﻿namespace VSTOLib
{
    public enum SetTextDirection
    {
        Column,
        Row
    }

    public enum xlOffset
    {
        Left, Right, Up, Down
    }

    public enum SortLevel { Level1, Level2 }
}
