﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSTOLib.Clipboard
{
    public static class ClipboardSafeInvoke
    {
        //https://stackoverflow.com/questions/17762037/current-thread-must-be-set-to-single-thread-apartment-sta-error-in-copy-stri
        public static void SetText(string text)
        {
            try
            {
                System.Threading.Thread STAThread = new System.Threading.Thread(
                          delegate ()
                          {
                              // Use a fully qualified name for Clipboard otherwise it
                              // will end up calling itself.
                              System.Windows.Forms.Clipboard.SetText(text);
                          });
                STAThread.SetApartmentState(System.Threading.ApartmentState.STA);
                STAThread.Start();
                STAThread.Join();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }

        public static string GetText()
        {
            string ReturnValue = string.Empty;
            System.Threading.Thread STAThread = new System.Threading.Thread(
                delegate ()
                {
                    // Use a fully qualified name for Clipboard otherwise it
                    // will end up calling itself.
                    ReturnValue = System.Windows.Forms.Clipboard.GetText();
                });
            STAThread.SetApartmentState(System.Threading.ApartmentState.STA);
            STAThread.Start();
            STAThread.Join();

            return ReturnValue;
        }
    }
}
