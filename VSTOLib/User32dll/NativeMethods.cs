﻿using System;
using System.Runtime.InteropServices;

namespace VSTOLib.User32dll
{
    public static class NativeMethods
    {
        [DllImport("user32.dll")]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);


        //https://stackoverflow.com/questions/971604/how-do-i-set-the-windows-default-printer-in-c
        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetDefaultPrinter(string Printer);



    }
}
