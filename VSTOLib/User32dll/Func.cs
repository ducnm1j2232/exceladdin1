﻿using System;

namespace VSTOLib.User32dll
{
    public static class Func
    {
        public static void SetWindowPos(System.IntPtr handle, bool topmost = true)
        {
            IntPtr HWND_TOPMOST = new IntPtr(-1);
            IntPtr HWND_NOTOPMOST = new IntPtr(-2);
            UInt32 SWP_NOSIZE = 0x0001;
            UInt32 SWP_NOMOVE = 0x0002;
            UInt32 SWP_SHOWWINDOW = 0x0040;
            if (topmost)
            {
                NativeMethods.SetWindowPos(handle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
            }
            else
            {
                NativeMethods.SetWindowPos(handle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
            }
        }
    }
}
