﻿using Microsoft.Office.Interop.Excel;
using DataTable = System.Data.DataTable;

namespace VSTOLib.Extensions
{
    public static class DataTableExt
    {
        public static bool IsEquals(this DataTable dataTable, DataTable dataTable2)
        {
            if (dataTable.Rows.Count != dataTable2.Rows.Count || dataTable.Columns.Count != dataTable2.Columns.Count)
                return false;

            for (int rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
            {
                for (int colIndex = 0; colIndex < dataTable.Columns.Count; colIndex++)
                {
                    if (!Equals(dataTable.Rows[rowIndex][colIndex], dataTable2.Rows[rowIndex][colIndex]))
                        return false;
                }
            }
            return true;
        }

        public static bool HasData(this System.Data.DataTable dataTable)
        {
            return dataTable != null && dataTable.Rows.Count > 0;
        }

        public static string[,] To2DStringArray(this DataTable dataTable)
        {
            string[,] result = new string[dataTable.Rows.Count, dataTable.Columns.Count];
            for (int rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
            {
                for (int colIndex = 0; colIndex < dataTable.Columns.Count; colIndex++)
                {
                    result[rowIndex, colIndex] = dataTable.Rows[rowIndex][colIndex].ToString();
                }
            }
            return result;
        }

        public static void CopyToRange(this DataTable dataTable, Range range, bool header = true)
        {
            if (dataTable == null || dataTable.Rows.Count == 0)
            {
                return;
            }

            if (header == true)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    range.Offset[0, i].Value2 = dataTable.Columns[i].ColumnName;
                }
                range.Offset[1, 0].Resize[dataTable.Rows.Count, dataTable.Columns.Count].Value2 = dataTable.To2DStringArray();
            }
            else
            {
                range.Resize[dataTable.Rows.Count, dataTable.Columns.Count].Value2 = dataTable.To2DStringArray();
            }
        }

        public static DataTable SelectColumn(this DataTable dataTable, params string[] columns)
        {
            DataTable output = null;

            output = dataTable.DefaultView.ToTable(false, columns);

            return output;
        }
    }
}
