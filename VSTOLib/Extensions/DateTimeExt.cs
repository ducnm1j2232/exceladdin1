﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSTOLib.Extensions
{
    public static class DateTimeExt
    {
        public static bool IsEqual(this DateTime date1, DateTime date2)
        {
            return date1.Year == date2.Year && date1.Month == date2.Month;
        }

        public static bool LessThan(this DateTime date1, DateTime date2)
        {
            if (date1.Year < date2.Year) return true;
            if (date1.Year == date2.Year && date1.Month < date2.Month) return true;
            return false;
        }

        public static bool IsLessThanOrEqualMonthAndYear(DateTime date1, DateTime date2)
        {
            if (date1.Year < date2.Year)
            {
                return true;
            }

            if (date1.Month <= date2.Month && date1.Year == date2.Year)
            {
                return true;
            }

            return false;
        }

        public static bool IsLessThanMonthAndYear(DateTime date1, DateTime date2)
        {
            if (date1.Month < date2.Month && date1.Year <= date2.Year)
            {
                return true;
            }

            return false;
        }
    }
}
