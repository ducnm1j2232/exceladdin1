﻿using System;
using System.Windows.Forms;


namespace VSTOLib.Extensions
{
    public static class ControlEx
    {
        public enum ShowType { Show, ShowDialog }

        public static void ShowAtCursorPosition(this System.Windows.Forms.Form form, ShowType showType)
        {
            form.StartPosition = FormStartPosition.Manual;
            System.Drawing.Point cursorPoint = Cursor.Position;
            form.Top = cursorPoint.Y - form.Height / 2;
            form.Left = cursorPoint.X - form.Width / 2;

            switch (showType)
            {
                case ShowType.Show:
                    form.Show();
                    break;
                case ShowType.ShowDialog:
                    form.ShowDialog();
                    break;
            }
        }

        public static void SafeInvoke(this Control control, Action action)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(action);
            }
            else
            {
                action();
            }
        }

        public static void SafeBeginInvoke(this Control control, Action action)
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke(action);
            }
            else
            {
                action();
            }
        }

        public static void BeginInvoke2(this Control control, MethodInvoker methodInvoker)
        {
            control.BeginInvoke(methodInvoker);

        }



        public static void InvokeSetEnable(this Control control, bool enable)
        {
            control.SafeBeginInvoke(() =>
            {
                control.Enabled = enable;
            });
        }
    }
}
