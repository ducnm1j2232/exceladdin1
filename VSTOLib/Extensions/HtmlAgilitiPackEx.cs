﻿namespace VSTOLib.Extensions
{
    public static class HtmlAgilitiPackEx
    {
        public static System.Data.DataTable ParseTable(string html, string headerHTML)
        {
            System.Data.DataTable dataTable = new System.Data.DataTable();

            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(html);

            var headers = document.DocumentNode.SelectNodes(headerHTML);
            foreach (var header in headers)
            {
                dataTable.Columns.Add(header.InnerText);
            }

            return dataTable;
        }

        public static void AddRowsToTable(System.Data.DataTable dataTable, string html, string rowsHTML)
        {
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(html);

            var rows = document.DocumentNode.SelectNodes(rowsHTML);
            foreach (var row in rows)
            {
                var cells = row.SelectNodes("td");
                System.Collections.Generic.List<string> itemArray = new System.Collections.Generic.List<string>();
                foreach (var cell in cells)
                {
                    itemArray.Add(cell.InnerText);
                }
                dataTable.Rows.Add(itemArray.ToArray());
            }
        }

    }
}
