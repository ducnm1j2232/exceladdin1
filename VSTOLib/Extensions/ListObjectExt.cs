﻿using Microsoft.Office.Interop.Excel;

namespace VSTOLib.Extensions
{
    public static class ListObjectExt
    {
        public static Range GetColumnDataBodyRange(this ListObject listObject, string columnName)
        {
            if (listObject == null)
            {
                throw new System.Exception("list object was null");
            }

            if (string.IsNullOrEmpty(columnName))
            {
                throw new System.Exception("column name was null or empty");
            }

            return listObject.ListColumns[columnName].DataBodyRange;
        }

        /// <summary>
        /// 1-base index
        /// </summary>
        /// <param name="listObject"></param>
        /// <param name="row"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static Range GetCell(this ListObject listObject, int row, string columnName)
        {
            return (Range)listObject.DataBodyRange[row, listObject.ListColumns[columnName].Index];
        }

        public static Range GetCell(this ListRow row, string columnName)
        {
            ListObject listObject = (ListObject)row.Parent;
            return listObject.GetCell(row.Index, columnName);

        }

        public static void DeleteRows(this ListObject listObject)
        {
            //foreach (ListRow listRow in listObject.ListRows)
            //{
            //    listRow.Delete();
            //}

            while (listObject.ListRows.Count > 0)
            {
                listObject.ListRows[1].Delete();
            }
        }

        public static void AddRows(this ListObject listObject, int rowCount = 1)
        {
            for (int i = 0; i < rowCount; i++)
            {
                listObject.ListRows.AddEx(AlwaysInsert: true);
            }
        }

        public static void Filter(this ListObject listObject, string columnName, object value)
        {
            AutoFilter autoFilter = listObject.AutoFilter;
            int fieldIndex = listObject.ListColumns[columnName].Index;
            listObject.Range.AutoFilter(fieldIndex, value);
        }

        public static void DeleteRowFilter(this ListObject listObject, string columnName, object value)
        {
            Filter(listObject, columnName, value);
            for (int i = listObject.ListRows.Count; i >= 1; i--)
            {
                ListRow row = listObject.ListRows[i];
                if (!row.Range.EntireRow.Hidden)
                {
                    row.Range.EntireRow.Delete();
                }
            }

            listObject.AutoFilter.ShowAllData();
        }

        public static void CopyFilterData(this ListObject listObject, string columnName, object value, Range destination, bool copyHeader)
        {
            Filter(listObject, columnName, value);
            if (copyHeader)
            {
                listObject.Range.SpecialCells(XlCellType.xlCellTypeVisible).Copy(destination);
            }

            if (!copyHeader)
            {
                listObject.DataBodyRange.SpecialCells(XlCellType.xlCellTypeVisible).Copy(destination);
            }
        }


        public static void Sort1(this ListObject listObject, string columnName, XlSortOrder order = XlSortOrder.xlAscending)
        {
            SortCore(listObject, SortLevel.Level1, columnName, order);
        }

        public static void Sort2(this ListObject listObject, string columnName, XlSortOrder order = XlSortOrder.xlAscending)
        {
            SortCore(listObject, SortLevel.Level2, columnName, order);
        }

        private static void SortCore(this ListObject listObject, VSTOLib.SortLevel sortLevel, string columnName, XlSortOrder order = XlSortOrder.xlAscending)
        {
            //Worksheet worksheet = (Worksheet)listObject.Parent;

            if (sortLevel == SortLevel.Level1)
            {
                listObject.Sort.SortFields.Clear();
            }

            Range key = listObject.HeaderRowRange[1, listObject.ListColumns[columnName].Index];
            listObject.Sort.SortFields.Add(Key: key, SortOn: XlSortOn.xlSortOnValues, Order: order, XlSortDataOption.xlSortNormal);
            //listObject.Sort.SetRange(listObject.Range);
            listObject.Sort.Header = XlYesNoGuess.xlYes;
            listObject.Sort.MatchCase = false;
            listObject.Sort.Orientation = XlSortOrientation.xlSortColumns;
            listObject.Sort.SortMethod = XlSortMethod.xlPinYin;
            listObject.Sort.Apply();
        }
    }
}
