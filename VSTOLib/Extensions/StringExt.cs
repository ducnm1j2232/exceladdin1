﻿using System;
using System.Globalization;
using System.Text;

namespace VSTOLib.Extensions
{
    public static class StringExt
    {
        public static bool IsEmpty(this string input)
        {
            return string.IsNullOrEmpty(input);
        }
        public static string GetNumberDigitOnly(this string source)
        {
            return System.Text.RegularExpressions.Regex.Match(source, @"\d+").Value;
            //return System.Text.RegularExpressions.Regex.Match(source, @"^-?[0-9]\d*(\.\d+)?$").Value; //positive and negative number
        }

        public static string Left(this string s, int length)
        {
            length = Math.Max(length, 0);

            if (s.Length > length)
            {
                return s.Substring(0, length);
            }
            else
            {
                return s;
            }
        }

        public static string Mid(this string s, int startIndex1Base, int length)
        {
            return s.Substring(startIndex1Base - 1, length);
        }

        public static string Right(this string s, int length)
        {
            length = Math.Max(length, 0);

            if (s.Length > length)
            {
                return s.Substring(s.Length - length, length);
            }
            else
            {
                return s;
            }
        }

        public static string GetStringBefore(this string input, string beforeString)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            int index = input.IndexOf(beforeString);

            if (index > 0)
            {
                return input.Substring(0, index);
            }

            return input;
        }

        public static string GetStringAfter(this string input, string afterString)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            int index = input.IndexOf(afterString);

            if (index > 0)
            {
                int startIndex = index + afterString.Length;
                return input.Substring(startIndex, input.Length - startIndex);
            }

            return input;
        }

        public static T ConvertFromVietnameseFormatToNumber<T>(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                input = "0";
            }

            input = input.Replace('.', '_');
            input = input.Replace(',', '.');
            input = input.Replace("_", string.Empty);

            return (T)System.Convert.ChangeType(input, typeof(T));
        }

        public static string CapitalizeFirstLetter(this string input)
        {
            if (input.Length == 0)
            {
                return input;
            }
            else if (input.Length == 1)
            {
                return Convert.ToString(char.ToUpper(input[0]));
            }
            else
            {
                return Convert.ToString(char.ToUpper(input[0]) + input.Substring(1).ToLowerInvariant());
            }
        }


        /// <summary>
        /// tiếng việt không dấu
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ConvertToUnSign(this string s)
        {
            string normalized = s.Normalize(NormalizationForm.FormD);
            StringBuilder result = new StringBuilder();

            foreach (char c in normalized)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                {
                    result.Append(c);
                }
            }

            result = result.Replace("Đ", "D").Replace("đ", "d");
            return result.ToString().Normalize(NormalizationForm.FormD);
        }

        public static string RemoveSpecialCharacters(this string input)
        {
            string output = "";

            foreach (char c in input)
            {
                if (Char.IsLetterOrDigit(c))
                {
                    output += c;
                }
            }

            return output;
        }

    }
}
