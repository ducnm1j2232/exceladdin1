﻿using System;
using System.Threading.Tasks;

namespace VSTOLib.Extensions
{
    public static class VoidExt
    {
        public static void RunWithTimeout(Action action, TimeSpan timeout)
        {
            Task t = Task.Run(action);
            if (t.Wait(timeout))
            {
                goto exit;
            }

        exit:;
        }
    }
}
