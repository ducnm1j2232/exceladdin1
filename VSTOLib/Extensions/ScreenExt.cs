﻿using System.Drawing;
using System.Windows.Forms;

namespace VSTOLib.Extensions
{
    public static class ScreenExt
    {
        public static string SreenShotFileName = System.IO.Path.Combine(@"D:\", "screenshot.png");
        public static Bitmap GetScreenShot()
        {
            Rectangle bounds = System.Windows.Forms.Screen.GetBounds(Point.Empty);
            Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height);

            using (Graphics g = Graphics.FromImage(bitmap))
            {
                g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
            }

            //bitmap.Save(FileName, System.Drawing.Imaging.ImageFormat.Png);

            return bitmap;
        }

        public static void TakeScreenShotAndSave()
        {
            Rectangle bounds = Screen.GetBounds(Point.Empty);
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                    bitmap.Save(SreenShotFileName, System.Drawing.Imaging.ImageFormat.Png);
                }
            }
        }
    }
}
