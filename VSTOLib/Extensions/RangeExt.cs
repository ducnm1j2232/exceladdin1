﻿using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using DataTable = System.Data.DataTable;

namespace VSTOLib.Extensions
{
    public static class RangeExt
    {
        public static void SetHeaderText(this Range range, params object[] array)
        {
            range.Resize[1, array.Length].Value2 = array;
        }

        public static void SetTextArray(this Range range, VSTOLib.SetTextDirection textDirection, params object[] array)
        {
            switch (textDirection)
            {
                case SetTextDirection.Row:
                    range.Resize[1, array.Length].Value2 = array;
                    break;
                case SetTextDirection.Column:
                    object[,] obj = new object[array.Length, 1];
                    for (int i = 0; i < obj.GetLength(0); i++)
                    {
                        obj[i, 0] = array[i];
                    }

                    range.Resize[array.Length, 1].Value2 = obj;
                    break;
            }
        }

        public static ListObject ConvertToListObject(this Range range, string tableName = "")
        {
            Worksheet sheet = range.Worksheet;
            foreach (ListObject obj in sheet.ListObjects)
            {
                if (obj.Range.Address == range.Address)
                {
                    return obj;
                }
            }

            //System.Runtime.InteropServices.COMException: 'This will change a filtered range on your worksheet. To complete this task, please remove AutoFilters.'
            if (sheet.AutoFilterMode)
            {
                sheet.AutoFilterMode = false;
            }

            ListObject listObject = sheet.ListObjects.AddEx(XlListObjectSourceType.xlSrcRange, range, null, XlYesNoGuess.xlYes);
            listObject.TableStyle = "";
            if (!string.IsNullOrEmpty(tableName))
            {
                listObject.Name = tableName;
            }
            return listObject;
        }

        public static List<string> GetUniqueValue(this Range range)
        {
            List<string> result = new List<string>();

            foreach (Range eachRange in range)
            {
                string cellValue = eachRange.GetValue<string>();
                if (!result.Contains(cellValue))
                {
                    result.Add(cellValue);
                }
            }

            return result;
        }

        public static T GetValue<T>(this Range range)
        {
            System.Type type = typeof(T);
            object value = range.Value == null ? string.Empty : range.Value;
            object result = null;

            if (type == typeof(string))
            {
                result = System.Convert.ToString(value);
            }

            if (type == typeof(int))
            {
                result = System.Convert.ToInt32(value);
            }

            if (type == typeof(double))
            {
                result = System.Convert.ToDouble(value);
            }

            //return (T)result;
            return (T)System.Convert.ChangeType(result, typeof(T));
        }

        public static void SetFilter(this Range range, string columnName, object criteria)
        {
            if (string.IsNullOrEmpty(columnName))
            {
                throw new System.Exception("columnName null");
            }

            if (criteria == null)
            {
                throw new System.Exception("criteria null");
            }

            range.Worksheet.RemoveAutoFilter();

            int columnIndex = 0;
            for (int colIndex = 1; colIndex <= range.Columns.Count; colIndex++)
            {
                Range cell = range.Cells[1, colIndex];
                if (cell.GetValue<string>() == columnName)
                {
                    columnIndex = colIndex;
                }
            }

            if (columnIndex == 0)
            {
                throw new System.Exception($"cannot find {columnName}");
            }

            System.Type type = criteria.GetType();
            XlAutoFilterOperator xlAutoFilterOperator = XlAutoFilterOperator.xlAnd;
            range.AutoFilter(columnIndex, criteria, xlAutoFilterOperator);



        }

        public static Range NewRangeAtColumn(this Worksheet worksheet, string columnName)
        {
            return worksheet.Range[columnName + worksheet.Rows.Count].End[XlDirection.xlUp].Offset[1, 0];
        }

        public static Range NewRightCellAtColumn(this Worksheet worksheet, int row)
        {
            Range lastCellAtColumn = worksheet.Cells[row, worksheet.Columns.Count];
            Range lastCol = lastCellAtColumn.End[XlDirection.xlToLeft];
            if (lastCol.Value == null)
            {
                return lastCol;
            }
            else
            {
                return lastCol.OffsetExt(xlOffset.Right, 1);
            }
        }

        public static Range OffsetExt(this Range range, xlOffset xloffset, int offsetStep = 1)
        {
            Range result = range;
            switch (xloffset)
            {
                case xlOffset.Left:
                    result = range.Offset[0, -offsetStep];
                    break;
                case xlOffset.Right:
                    result = range.Offset[0, offsetStep];
                    break;
                case xlOffset.Up:
                    result = range.Offset[-offsetStep, 0];
                    break;
                case xlOffset.Down:
                    result = range.Offset[offsetStep, 0];
                    break;
                default:
                    break;
            }
            return result;
        }

        public static bool IsSingleCell(this Range range)
        {
            if (range.Rows.Count == 1 && range.Columns.Count == 1)
            {
                return true;
            }
            return false;
        }
        public static void CopyValueTo(this Range range, Range destinationRange)
        {
            range.Copy();
            destinationRange.PasteSpecial(XlPasteType.xlPasteValues);
            range.Application.CutCopyMode = 0;
        }

        public static void CopyRawDataTo(this Range range, Range destinationRange)
        {
            //Range selection = Globals.ThisAddIn.Application.Selection;
            //Worksheet parentWorksheet = range.Worksheet;
            //Workbook parentWorkbook = (Workbook)selection.Worksheet.Parent;
            //Worksheet ws = parentWorkbook.Worksheets.Add(After: parentWorksheet);
            range.Copy(destinationRange);
            destinationRange.Worksheet.UsedRange.ClearAllBlankRowBlankColumnAndFormating();
        }

        public static void ClearAllBlankRowBlankColumnAndFormating(this Range range)
        {
            range.UnMerge();
            range.ClearFormats();
            range.ClearHyperlinks();
            range.WrapText = false;
            range.Worksheet.DeleteBlankRows();
            range.Worksheet.DeleteBlankColumns();
        }



        public static Range MinimumRange(this Range range)
        {
            if (range.Application.WorksheetFunction.CountA(range) == 0)
            {
                return range;
            }
            Worksheet ws = range.Worksheet;
            int lastRow = range.Cells.Find(What: "*", LookAt: XlLookAt.xlPart, LookIn: XlFindLookIn.xlFormulas, SearchOrder: XlSearchOrder.xlByRows, SearchDirection: XlSearchDirection.xlPrevious).Row;
            int lastColumn = range.Cells.Find(What: "*", LookAt: XlLookAt.xlPart, LookIn: XlFindLookIn.xlFormulas, SearchOrder: XlSearchOrder.xlByColumns, SearchDirection: XlSearchDirection.xlPrevious).Column;
            Range minimunRange = ws.Range[range.Cells[1, 1], ws.Cells[lastRow, lastColumn]];
            return minimunRange;
        }

        public static Range GetFirstColumn(this Range range)
        {
            Range cell1 = range.Cells[1, 1];
            Range cell2 = range.Cells[range.Rows.Count, 1];
            Worksheet worksheet = range.Worksheet;
            return worksheet.Range[cell1, cell2];
        }

        public static void PasteValue(this Range range)
        {
            if (range.IsSingleCell())
            {
                range.CopyValueTo(range);
            }
            else
            {
                Worksheet worksheet = range.Worksheet;
                if (worksheet.AutoFilter != null)// có mũi tên filter
                {
                    if (worksheet.FilterMode)// đang có dòng ẩn
                    {
                        Range firstColumn = range.MinimumRange().GetFirstColumn();
                        foreach (Range r in firstColumn.SpecialCells(XlCellType.xlCellTypeVisible))
                        {
                            r.Resize[1, range.Columns.Count].CopyValueTo(r);
                        }
                    }
                    else // không có dòng ẩn
                    {
                        range.CopyValueTo(range);
                    }
                }
                else
                {
                    range.CopyValueTo(range);
                }
            }
            range.Application.CutCopyMode = 0;
        }

        public static Range ConvertToValue(this Range range)
        {
            if (range.Areas.Count == 1)
            {
                range.Value = range.Value;
            }

            if (range.Areas.Count != 1)
            {
                for (int i = 1; i <= range.Areas.Count; i++)
                {
                    range.Areas[i].Value = range.Areas[i].Value;
                }
            }

            return range;
        }

        public static Range SetAccountingNumberFormat(this Range range, int numberAfterDot = 0)
        {
            //Selection.NumberFormat = "#,##0_);[Red](#,##0)"
            string fractionalPart = numberAfterDot == 0 ? string.Empty : $".".PadRight(numberAfterDot + 1, '0');
            //string format = $"#,##0{fractionalPart}";
            string format = $"#,##0{fractionalPart};[Red](#,##0{fractionalPart})";
            range.NumberFormat = format;

            return range;
        }

        public static Range Cell(this Range range, int rowIndex, int columnIndex)
        {
            return range.Cells[rowIndex, columnIndex];
        }

        public static void SetRowNum(this Range range, int rowCount)
        {
            //https://docs.microsoft.com/en-us/office/vba/api/excel.xlrowcol
            //const int xlRows = 1;
            const int xlColumns = 2;

            range.Value2 = 1;
            range.DataSeries(Rowcol: xlColumns, Step: 1, Stop: rowCount);
        }

        public static List<Range> GetSelection(this Worksheet worksheet)
        {
            List<Range> ranges = new List<Range>();
            Range selection = worksheet.Application.Selection;

            if (selection.IsSingleCell())
            {
                ranges.Add(selection);
            }




            return ranges;

        }

        public static DataTable ConvertToDataTable(this Range range)
        {
            DataTable dt = new DataTable();

            object[,] values = range.Value;

            //faster
            for (int colIndex = 1; colIndex <= values.GetLength(1); colIndex++)// loop over columns
            {
                System.Type columnType = typeof(string);
                if (values[2, colIndex] != null)
                {
                    columnType = values[2, colIndex].GetType();//first data row
                }
                dt.Columns.Add(values[1, colIndex].ToString(), columnType);
            }

            //faster
            for (int i = 2; i <= values.GetLength(0); i++) //rows 1-base
            {
                object[] rowValues = new object[values.GetLength(1)];
                for (int j = 1; j <= values.GetLength(1); j++) // columns 1-base
                {
                    rowValues[j - 1] = values[i, j];
                }

                dt.Rows.Add(rowValues);
            }

            return dt;
        }

        public static void SetValues (this Range range, object[,] values)
        {
            range.Resize[values.GetLength(0), values.GetLength(1)].Value = values;
        }
    }
}
