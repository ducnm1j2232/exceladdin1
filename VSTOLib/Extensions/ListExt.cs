﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace VSTOLib.Extensions
{
    public static class ListExt
    {
        public static void AddUnique<T>(this List<T> list, T item)
        {
            if (!list.Contains(item))
            {
                list.Add(item);
            }
        }

        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            //https://stackoverflow.com/questions/564366/convert-generic-list-enumerable-to-datatable
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }

                table.Rows.Add(row);
            }
            return table;
        }
    }
}
