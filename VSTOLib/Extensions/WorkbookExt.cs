﻿using Microsoft.Office.Interop.Excel;
using System;

namespace VSTOLib.Extensions
{
    public static class WorkbookExt
    {
        public static Worksheet GetSheet(this Workbook workbook, string name)
        {
            return GetWorksheetCore(workbook, name, false);
        }

        public static Worksheet GetNewSheet(this Workbook workbook, string name)
        {
            return GetWorksheetCore(workbook, name, true);
        }

        private static Worksheet GetWorksheetCore(Workbook workbook, string name, bool deleteOldData)
        {
            if (workbook == null)
            {
                throw new Exception("workbook was null");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("name was null");
            }

            foreach (Worksheet sheet in workbook.Worksheets)
            {
                if (sheet.Name == name)
                {
                    if (deleteOldData)
                    {
                        sheet.UnlistAllListObject();
                        sheet.Columns.Delete();
                    }

                    return sheet;
                }
            }

            Worksheet worksheet = (Worksheet)workbook.Worksheets.Add(After: workbook.Worksheets[workbook.Worksheets.Count]);
            worksheet.Name = name;

            return worksheet;
        }

        /// <summary>
        /// 56: .xls
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="fileName"></param>
        /// <param name="fileFormat"></param>
        public static void SaveOverWrite(this Workbook workbook, string fileName, object fileFormat)
        {
            SaveCore(workbook, fileName, fileFormat);
        }

        public static void SaveOverWrite(this Workbook workbook, string fileName)
        {
            SaveCore(workbook, fileName, XlFileFormat.xlOpenXMLWorkbook);
        }

        private static void SaveCore(this Workbook workbook, string fileName, object fileFormat)
        {
            //https://docs.microsoft.com/en-us/office/vba/api/excel.xlfileformat
            Microsoft.Office.Interop.Excel.Application application = workbook.Application;
            application.DisplayAlerts = false;//prevent from showing message file already exist
            workbook.SaveAs(Filename: fileName, FileFormat: fileFormat);
            application.DisplayAlerts = true;
        }

        public static void UnlistAllListObject(this Worksheet worksheet)
        {
            foreach (ListObject list in worksheet.ListObjects)
            {
                list.Unlist();
            }
        }


    }
}
