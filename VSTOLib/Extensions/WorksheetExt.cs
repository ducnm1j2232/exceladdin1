﻿using Microsoft.Office.Interop.Excel;

namespace VSTOLib.Extensions
{
    public static class WorksheetExt
    {
        public static int LastRow(this Worksheet worksheet, string column)
        {
            return worksheet.Range[column + worksheet.Rows.Count].End[XlDirection.xlUp].Row;
        }

        public static Range LastColumnRangeAtRow(this Worksheet worksheet, int rowIndex)
        {
            Range lastColumnRange = worksheet.Cells[rowIndex, worksheet.Columns.Count];
            return lastColumnRange.End[XlDirection.xlToLeft];
        }

        public static Range GetRange(this Worksheet worksheet, string address, string columnLastRow = null)
        {
            string[] columns = address.Split(':');
            string col1 = columns[0];
            string col2 = columns[1];

            int LR;
            if (columnLastRow == null)
            {
                LR = worksheet.Rows.Count;
            }
            else
            {
                LR = worksheet.LastRow(columnLastRow);
            }

            string fullAddress = address + LR.ToString();
            return worksheet.Range[fullAddress];
        }

        public static void RemoveAutoFilter(this Worksheet worksheet)
        {
            if (worksheet.AutoFilterMode)
            {
                worksheet.AutoFilterMode = false;
            }
        }

        public static Range GetRectangleRange(this Worksheet worksheet, Range firstCell, string lastRowColumnName)
        {
            Range LastColumnRange = worksheet.LastColumnRangeAtRow(firstCell.Row);
            int lastColumnIndex = LastColumnRange.Column;
            int lastRowIndex = worksheet.LastRow(lastRowColumnName);
            Range lastCell = worksheet.Cells[lastRowIndex, lastColumnIndex];
            return worksheet.Range[firstCell, lastCell];
        }

        public static Application GetApplication(this Worksheet worksheet)
        {
            return (worksheet.Parent as Workbook).Application;
        }

        public static void DeleteBlankRows(this Worksheet worksheet)
        {
            int lastRow = worksheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell).Row;

            worksheet.GetApplication().SpeedUpCode(true);
            for (int i = lastRow; i > 0; i--)
            {
                if (worksheet.GetApplication().WorksheetFunction.CountA(worksheet.Rows[i]) == 0)
                {
                    Range row = worksheet.Rows[i];
                    row.EntireRow.Delete();
                }
            }
            worksheet.GetApplication().SpeedUpCode(false);
        }

        public static void DeleteBlankColumns(this Worksheet worksheet)
        {
            int lastColumn = worksheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell).Column;

            worksheet.GetApplication().SpeedUpCode(true);
            for (int i = lastColumn; i > 0; i--)
            {
                if (worksheet.GetApplication().WorksheetFunction.CountA(worksheet.Columns[i]) == 0)
                {
                    Range col = worksheet.Columns[i];
                    col.EntireColumn.Delete();
                }
            }
            worksheet.GetApplication().SpeedUpCode(false);
        }

    }
}
