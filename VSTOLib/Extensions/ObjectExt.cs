﻿using System;

namespace VSTOLib.Extensions
{
    public static class ObjectExt
    {
        public static bool IsInteger(this object value)
        {
            int result;
            return Int32.TryParse(value.ToString(), out result);
        }
    }
}
