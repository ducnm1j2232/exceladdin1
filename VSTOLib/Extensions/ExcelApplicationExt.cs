﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSTOLib.Extensions
{
    public static class ExcelApplicationExt
    {
        public static void SpeedUpCode(this Application application, bool mode)
        {
            try
            {
                //Microsoft.Office.Interop.Excel.Application application = workbook.Application;
                if (mode == true) application.Calculation = XlCalculation.xlCalculationManual;
                else application.Calculation = XlCalculation.xlCalculationAutomatic;
                application.ScreenUpdating = !mode;
                application.EnableEvents = !mode;
                application.DisplayAlerts = !mode;
            }
            catch { }
        }
    }
}
