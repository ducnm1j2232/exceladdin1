﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using VSTOLib.Extensions;

namespace VSTOLib.Excel
{
    public static class Pivot
    {
        public static PivotTable CreatePivotTable(Range dataSource,string pivotTableName,string destination = "A3")
        {
            Worksheet sheet = dataSource.Worksheet;
            Workbook workbook = (Workbook)sheet.Parent;

            Worksheet pivotSheet = workbook.GetNewSheet(pivotTableName);

            PivotCache pivotCache = workbook.PivotCaches().Create(XlPivotTableSourceType.xlDatabase, dataSource);
            PivotTable pivotTable = pivotCache.CreatePivotTable(pivotSheet.Range[destination], pivotTableName);

            return pivotTable;
        }
    }
}
