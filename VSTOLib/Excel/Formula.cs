﻿using Microsoft.Office.Interop.Excel;
using VSTOLib.Extensions;

namespace VSTOLib.Excel
{
    public class Formula
    {
        public static string VNDBlueSoftWebservice(Range range)
        {
            string value = range.GetValue<string>();
            value = value.Replace(",", "");
            //return $"=WEBSERVICE(\"http://bluesofts.net:2019/acchelper?f=VND&m=\"&{value})";
            return $"=WEBSERVICE(\"https://script.google.com/macros/s/AKfycbzIcMWkiJI27IvhM7nVkUzkNJgxRySQorILAnEwKaxwhxgzfymCJFScxFbtCHLU2MEq/exec?heo.docso=\"&{value})";
        }
    }
}
