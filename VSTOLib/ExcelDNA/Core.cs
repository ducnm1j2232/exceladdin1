﻿using ExcelDna.Integration;
using System;
using System.Collections.Generic;


namespace VSTOLib.ExcelDNA
{
    public static class Core
    {
        public static T GetValue2<T>(this object excelReference)
        {
            object output = null;
            if (excelReference.GetType() == typeof(ExcelReference))
            {
                output = (excelReference as ExcelReference).GetValue();
            }
            else
            {
                output = excelReference;
            }

            if (typeof(T) == typeof(DateTime))
            {
                output = DateTime.FromOADate(Convert.ToDouble(output));
            }

            if (typeof(T) == typeof(double))
            {
                output = System.Convert.ToDouble(output);
            }

            return (T)Convert.ChangeType(output, typeof(T));

        }

        public static List<T> GetListValue<T>(this object input)
        {
            List<T> list = new List<T>();
            try
            {
                ExcelReference excelReference = (ExcelReference)input;

                foreach (var item in excelReference.InnerReferences)
                {
                    if (item.Is2DArrayReference())
                    {
                        object[,] array = (object[,])item.GetValue();

                        for (int i = 0; i < array.GetLength(0); i++)
                        {
                            for (int j = 0; j < array.GetLength(1); j++)
                            {
                                list.Add(VSTOLib.Converter.General.ConvertTo<T>(array[i, j]));
                            }
                        }
                    }
                    else
                    {
                        list.Add(VSTOLib.Converter.General.ConvertTo<T>(item.GetValue()));
                    }
                }
            }
            catch
            {

            }

            return list;
        }

        
        public static T GetValueCore<T>(this object input)
        {
            object output = null;

            if (typeof(T) == typeof(string))
            {
                output = System.Convert.ToString(input);
            }

            if (typeof(T) == typeof(double))
            {
                output = VSTOLib.Converter.General.ConvertTo<double>(input);
            }

            return (T)Convert.ChangeType(output, typeof(T));
        }

        public static double SumCore(object[,] input)
        {
            double sum = 0;
            int rows = input.GetLength(0);
            int cols = input.GetLength(1);

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    sum += VSTOLib.Converter.General.ConvertTo<double>(input[i, j]);
                }
            }

            return sum;
        }

        public static bool IsSingleCellReference(this object input)
        {
            if (input is ExcelReference)
            {
                if (((ExcelReference)input).GetValue() is object[,])
                {
                    return false;
                }
            }

            return true;
        }

        public static bool Is2DArrayReference(this object input)
        {
            if (input is ExcelReference)
            {
                if (((ExcelReference)input).GetValue() is object[,])
                {
                    return true;
                }
            }

            return false;
        }




    }
}
