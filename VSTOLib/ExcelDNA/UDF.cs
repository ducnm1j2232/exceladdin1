﻿using ExcelDna.Integration;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VSTOLib.Extensions;

namespace VSTOLib.ExcelDNA
{

    public class UDF
    {
        [ExcelFunction(Description = "Trả về số tháng khấu hao trong năm tính đến ngày target")]
        public static object F_SoThangPhanBoTrongNam(
            [ExcelArgument(Name = "Ngày mua", AllowReference = true)] object buyDate,
            [ExcelArgument(Name = "Ngày target", AllowReference = true)] object targetDate,
            [ExcelArgument(Name = "Số tháng phân bổ của TSCĐ", AllowReference = true)] object numOfMonth,
            [ExcelArgument(Name = "đúng hay sai", AllowReference = true)] bool num = true)
        {
            if (buyDate.Is2DArrayReference())
            {
                return $"tham số Ngày mua chọn 1 ô thôi, chọn gì lắm";
            }

            if (targetDate.Is2DArrayReference())
            {
                return $"tham số Ngày target chọn 1 ô thôi, chọn gì lắm";
            }


            DateTime buyDate1 = buyDate.GetValue2<DateTime>();
            DateTime targetDate1 = targetDate.GetValue2<DateTime>();
            int numOfMonth1 = numOfMonth.GetValue2<int>();

            if (!buyDate1.IsEqual(targetDate1) && !buyDate1.LessThan(targetDate1))
            {
                return 0;
            }

            DateTime currentDate = buyDate1;
            int output = 0;
            for (int i = 0; i < numOfMonth1; i++)
            {
                currentDate = currentDate.AddMonths(1);
                if (currentDate.Month == 1) output = 0;
                output++;

                if (currentDate.IsEqual(targetDate1)) break;
            }

            if (currentDate.Year < targetDate1.Year) output = 0;

            return output;
        }

        [ExcelFunction]
        public static object F_GETTYPE([ExcelArgument(AllowReference = true)] object input)
        {
            string type = input.GetType().ToString();
            ExcelReference excelReference = (ExcelReference)input;
            string col1 = excelReference.ColumnFirst.ToString();
            string col9 = excelReference.ColumnLast.ToString();
            string row1 = excelReference.RowFirst.ToString();
            string row9 = excelReference.RowLast.ToString();
            return $"col1: {col1}, col9: {col9}, row1: {row1}, row9: {row9}";
        }

        [ExcelFunction]
        public static object F_GETVALUE([ExcelArgument(AllowReference = true)] object input)
        {
            string type = input.GetType().ToString();
            ExcelReference excelReference = (ExcelReference)input;
            object output = excelReference.GetValue();
            return output;
        }

        [ExcelFunction]
        public static object F_SUM([ExcelArgument(AllowReference = true)] object input)
        {
            double sum = 0;

            try
            {
                List<double> values = input.GetListValue<double>();
                foreach (double value in values)
                {
                    sum += value;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            if ((input as ExcelReference).GetValue() is object[,])
            {
                sum = Core.SumCore((object[,])(input as ExcelReference).GetValue());
            }

            return sum;
        }

        [ExcelFunction(Description = "Looks up a value in the first column of a table array and returns the value in the same row from a specified column (column_index_num) in the table array.")]
        public static object VLOOKUP2(object lookupValue, object[,] tableArray, int columnIndexNum, bool rangeLookup)
        {
            int rowCount = tableArray.GetLength(0);
            int columnCount = tableArray.GetLength(1);

            if (columnIndexNum < 1 || columnIndexNum > columnCount)
            {
                return ExcelError.ExcelErrorValue;
            }

            for (int i = 0; i < rowCount; i++)
            {
                object tableValue = tableArray[i, 0];
                if (tableValue.Equals(lookupValue))
                {
                    return tableArray[i, columnIndexNum - 1];
                }
            }

            if (rangeLookup)
            {
                return ExcelError.ExcelErrorNA;
            }
            else
            {
                return ExcelError.ExcelErrorValue;
            }
        }

        [ExcelFunction(Description = "Chuyển loopkup_value của hàm VLOOKUP sang dạng tìm kiếm chính xác")]
        public static string F_LOOKUP_VALUE_WITH_WILDCARDS([ExcelArgument(AllowReference = true)] object lookupValue)
        {
            string output = string.Empty;
            string value = null;
            if (lookupValue is ExcelReference)
            {
                value = ((ExcelReference)lookupValue).GetValue().ToString();
            }
            else
            {
                value = lookupValue.ToString();
            }

            output = value.Replace("~", "~~").Replace("?", "~?").Replace("*", "~*");

            return output;
        }

        [ExcelFunction]
        public static string F_BO_DAU_TIENG_VIET([ExcelArgument(AllowReference = true)] object input)
        {
            string s = string.Empty;
            if (input is ExcelReference)
            {
                s = ((ExcelReference)input).GetValue().ToString();
            }
            else
            {
                s = input.ToString();
            }
            return VSTOLib.Extensions.StringExt.ConvertToUnSign(s);
        }

        [ExcelFunction(Description = "Chuyển text về dạng đơn giản (bỏ khoảng trắng, viết thường, bỏ dấu Tiếng Việt... để so sánh")]
        public static string F_SIMPLIFY_TEXT([ExcelArgument(AllowReference = true)] object text,
            [ExcelArgument(Name = "Trim", Description = "Loại bỏ khoảng trắng thừa. 1 Có, 0 Không", AllowReference = true)] int trim,
            [ExcelArgument(Name = "Lower", Description = "chuyển về dạng chữ viết thường. 1 Có, 0 Không", AllowReference = true)] int lower,
            [ExcelArgument(Name = "RemoveSpace", Description = "Loại bỏ hết khoảng trắng. 1 Có, 0 Không", AllowReference = true)] int remvoveSpace,
            [ExcelArgument(Name = "ReplaceWildCard", Description = "Thay thế các ký tự đặc biệt wild card ( ~ * ? ). 1 Có, 0 Không", AllowReference = true)] int replaceWildCard,
            [ExcelArgument(Name = "BoDauTiengViet", Description = "Bỏ dấu Tiếng Việt. 1 Có, 0 Không", AllowReference = true)] int boDauTiengViet)
        {
            string s = string.Empty;

            try
            {
                if (text is ExcelReference)
                {
                    s = ((ExcelReference)text).GetValue().ToString();
                }
                else
                {
                    s = text.ToString();
                }

                if (trim == 1)
                {
                    s = s.Trim();
                }

                if (lower == 1)
                {
                    s = s.ToLower();
                }

                if (remvoveSpace == 1)
                {
                    s = s.Replace(" ", "");
                }

                if (replaceWildCard == 1)
                {
                    s = s.Replace("*", "#");
                    s = s.Replace("?", "!");
                    s = s.Replace("~", "@");
                }

                if (boDauTiengViet == 1)
                {
                    s = VSTOLib.Extensions.StringExt.ConvertToUnSign(s);
                }
            }
            catch
            {

            }

            return s;
        }

        [ExcelFunction]
        public static string F_GET_NUMERIC_CHARACTERS([ExcelArgument(AllowReference = true)] object text)
        {
            string s = string.Empty;

            try
            {
                if (text is ExcelReference)
                {
                    s = ((ExcelReference)text).GetValue().ToString();
                }
                else
                {
                    s = text.ToString();
                }

                s = new string(s.Where(char.IsDigit).ToArray());
            }
            catch { }

            return s;
        }

        [ExcelFunction]
        public static string F_GET_FIRST_WORDS([ExcelArgument(AllowReference = true)] string text, int numWords = 1)
        {
            string[] words = text.Split(' ');
            return string.Join(" ", words.Take(numWords));
        }

        [ExcelFunction(Description = "Replaces old values with new values in a string.")]
        public static string F_REPLACES(
            [ExcelArgument(AllowReference = true)] string text,
            [ExcelArgument(Name = "oldValueAndNewValue", Description = "{\"oldText1\",\"newText1\",\"oldText2\",\"newText2\"}", AllowReference = true)] object[] oldValueAndNewValue)
        {
            if (oldValueAndNewValue.Length % 2 != 0)
            {
                return "Invalid arguments";
            }

            for (int i = 0; i < oldValueAndNewValue.Length; i += 2)
            {
                object oldValue = oldValueAndNewValue[i];
                object newValue = oldValueAndNewValue[i + 1];

                if (oldValue == null || newValue == null)
                {
                    return "Invalid arguments";
                }

                text = text.Replace(oldValue.ToString(), newValue.ToString());
            }

            return text;
        }

        [ExcelFunction(Description = "Replaces all alphabetic characters in a string with a single dash (-), while keeping all numeric characters unchanged.")]
        public static string F_REPLACE_ALPHA_WITH_SINGLE_DASH([ExcelArgument(AllowReference = true)] string text)
        {
            StringBuilder sb = new StringBuilder(text.Length);
            bool prevCharIsAlpha = false;
            foreach (char c in text)
            {
                if (char.IsLetter(c))
                {
                    if (!prevCharIsAlpha)
                    {
                        sb.Append('-');
                        prevCharIsAlpha = true;
                    }
                }
                else if (char.IsDigit(c))
                {
                    sb.Append(c);
                    prevCharIsAlpha = false;
                }
                else
                {
                    sb.Append(c);
                    prevCharIsAlpha = false;
                }
            }
            return sb.ToString();
        }

















    }
}
