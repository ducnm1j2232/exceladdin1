﻿using System;

namespace VSTOLib.Converter
{
    public static class General
    {
        public static T ConvertTo<T>(object input)
        {
            object output = null;

            if (typeof(T) == typeof(double))
            {
                try
                {
                    output = System.Convert.ToDouble(input);
                }
                catch
                {
                    output = 0;
                }
            }

            if (typeof(T) == typeof(int))
            {
                try
                {
                    output = System.Convert.ToInt32(input);
                }
                catch
                {
                    output = 0;
                }
            }

            if (typeof(T) == typeof(string))
            {
                try
                {
                    output = System.Convert.ToString(input);
                }
                catch
                {
                    output = string.Empty;
                }
            }

            return (T)Convert.ChangeType(output, typeof(T));
        }
    }
}
