﻿using System.Windows.Forms;

namespace VSTOLib.IO
{
    public static class FolderBrowser
    {
        public static string SelectFolder(string description)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.Description = description;
            DialogResult dialogResult = folderBrowserDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                return folderBrowserDialog.SelectedPath;
            }

            return string.Empty;
        }
    }
}
