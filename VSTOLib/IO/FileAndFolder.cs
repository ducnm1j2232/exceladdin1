﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSTOLib.IO
{
    public static class FileAndFolder
    {
        public static void DeleteAll(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            
            foreach (FileInfo file in di.EnumerateFiles())
            {
                file.Delete();
            }

            foreach (DirectoryInfo dir in di.EnumerateDirectories())
            {
                dir.Delete(true);
            }
        }

        public static void Delete(string path)
        {
            if(System.IO.File.Exists(path))
            {
                File.Delete(path);
            }
        }
    }
}
