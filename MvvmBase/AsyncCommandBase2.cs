﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MvvmBase
{
    public abstract class AsyncCommandBase2 : IAsyncCommand
    {
        protected AsyncCommandBase2()
        {
        }

        public event EventHandler CanExecuteChanged;

        protected void OnCanExecuteChange()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }

        private bool _isExecuting;
        public bool IsExecuting
        {
            get { return _isExecuting; }
            set { 
                _isExecuting = value;
                CanExecuteChanged?.Invoke(this, new EventArgs());
            }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return !IsExecuting && CanExecute();
        }

        async void ICommand.Execute(object parameter)
        {
            IsExecuting = true;
            try
            {
                await ExecuteAsync();
            }
            finally
            {
                IsExecuting = false;
            }
        }

        public abstract bool CanExecute();

        public abstract Task ExecuteAsync();

    }
}
