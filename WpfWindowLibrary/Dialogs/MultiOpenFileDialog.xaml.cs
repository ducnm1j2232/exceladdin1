﻿using Microsoft.Win32;
using System.Collections.Generic;
using System.Windows;

namespace WpfWindowLibrary.Dialogs
{
    /// <summary>
    /// Interaction logic for MultiOpenFileDialog.xaml
    /// </summary>
    public partial class MultiOpenFileDialog : Window
    {
        public MultiOpenFileDialog()
        {
            InitializeComponent();

        }


        public string Description
        {
            get { return textBlockDesCription.Text; }

            set
            {
                textBlockDesCription.Text = value;
            }
        }

        private bool _Show1 = false;
        public bool ShowWindow()
        {
            _Show1 = false;
            this.ShowDialog();
            return _Show1;
        }

        private List<string> _fileName = new List<string>();
        public List<string> FileNames
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private void buttonOpenFileDialog_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                Multiselect = true,
                Filter = "Excel Files|*.xls;*.xlsx;*.xlsm"
            };

            if (openFileDialog.ShowDialog() == false) { return; }


            foreach (var filename in openFileDialog.FileNames)
            {
                _fileName.Add(filename);
                listBoxFileNames.Items.Add(filename);
            }
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            _Show1 = true;
            this.Close();
        }
    }
}
