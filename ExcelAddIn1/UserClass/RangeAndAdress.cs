﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelAddIn1.MyClass;

namespace ExcelAddIn1.UserClass
{
    class RangeAndAdress
    {
        public  string Value { get; set; }
        public   string Address { get; set; }

        public void Add(Microsoft.Office.Interop.Excel.Range range)
        {
            Value = range.GetValue(ValueType.String);
            Address = range.Address;
        }
    }
}
