﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.UserClass
{
    public static  class DanhSachHoaDon
    {
        public const string STT = "STT";
        public const string KyHieuMauSo = "Ký hiệu mẫu số";
        public const string KyHieuHoaDon = "Ký hiệu hóa đơn";
        public const string SoHoaDon = "Số hóa đơn";
        public const string NgayLap = "Ngày lập";
        public const string MSTNguoiBan = "MST người bán/MST người xuất hàng";
        public const string TenNguoiBan = "Tên người bán/Tên người xuất hàng";
        public const string TongTienChuaThue = "Tổng tiền chưa thuế";
        public const string TongTienThue = "Tổng tiền thuế";
        public const string TongTienChietKhauThuongMai = "Tổng tiền chiết khấu thương mại";
        public const string TongTienPhi = "Tổng tiền phí";
        public const string TongTienThanhToan = "Tổng tiền thanh toán";
        public const string DonViTienTe = "Đơn vị tiền tệ";
        public const string TrangThaiHoaDon = "Trạng thái hóa đơn";
        public const string KetQuaKiemTraHoaDon = "Kết quả kiểm tra hóa đơn";
    }
}
