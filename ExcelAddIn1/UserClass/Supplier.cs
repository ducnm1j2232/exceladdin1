﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.UserClass
{
    public class Supplier
    {
        public string MST { get; set; }
        public string Name { get; set; }
        public string ID { get; set; }
    }
}
