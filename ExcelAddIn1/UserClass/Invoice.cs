﻿namespace ExcelAddIn1.UserClass
{
    public class Invoice
    {
        public string MST { get; set; }
        public string Date { get; set; }
        public string InvoiceNumber { get; set; }
        public string SerialNumber { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public double Subtotal { get; set; }
        public double VatAmount { get; set; }
        public double TotalAmount { get; set; }
        public string GhiChu { get; set; }
    }
}
