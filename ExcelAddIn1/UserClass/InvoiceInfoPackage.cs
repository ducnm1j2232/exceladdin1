﻿namespace ExcelAddIn1.UserClass
{
    public class InvoiceInfoPackage
    {
        public string InvoiceNumber { get; set; }
        public string InvoiceSerialNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string TranDate { get; set; }
        public string MaKhoNhap { get; set; }
        public string PrintCopies { get; set; }
        public string MaNCC { get; set; }
        public string VatRate { get; set; }
        public double InvoiceAmount { get; set; }
        public double InvoiceVat { get; set; }
        public string LocalExcelFileImport { get; set; }
        public string PrintType { get; set; }
    }
}
