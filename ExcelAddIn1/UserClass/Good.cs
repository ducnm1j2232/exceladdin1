﻿namespace ExcelAddIn1.UserClass
{
    public class Good
    {
        public string GoodID { get; set; }
        public string ShortName { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public string SupplierID { get; set; }
        public string CustomerName { get; set; }
        public string UnitName { get; set; }
        public int ConvertUnit { get; set; }
        public string LastImpPriceVat { get; set; }
        public string ExpRetailPriceVat { get; set; }
        public string Status { get; set; }
        public string Stock { get; set; }


    }
}
