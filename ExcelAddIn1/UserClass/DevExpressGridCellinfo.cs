﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.UserClass
{
    public class DevExpressGridCellinfo
    {
        public DevExpressGridCellinfo(int rowHandle,string columnName)
        {
            RowHandle = rowHandle;
            ColumnName = columnName;
        }
        public int RowHandle { get; set; }
        public string ColumnName { get; set; }
    }
}
