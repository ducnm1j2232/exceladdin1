﻿namespace ExcelAddIn1.ServerConnection
{
    internal class Server
    {
        static string xmlFileServerList = ExcelAddIn1.Public.Variables.AddinFolder + @"\ServerList.xml";

        public static System.Data.DataTable ServerList
        {
            get
            {
                using (System.Data.DataSet ds = new System.Data.DataSet("dsServerList"))
                {
                    if (System.IO.File.Exists(xmlFileServerList) == false)
                    {
                        using (System.Data.DataTable dt = new System.Data.DataTable { TableName = "ServerList" })
                        {
                            dt.Columns.Add("Name");
                            dt.Columns.Add("IP");
                            dt.Columns.Add("User");
                            dt.Columns.Add("Pass");

                            dt.Rows.Add(new string[] { "Tổng Kho", "192.168.0.9", "sa", "datphat@hanoi" });
                            dt.Rows.Add(new string[] { "Lê Trọng Tấn", "192.168.6.6", "sa", "123@Datphat" });
                            dt.Rows.Add(new string[] { "Bắc Giang", "192.168.1.200", "sa", "datphat@hanoi" });
                            dt.Rows.Add(new string[] { "Lào Cai", "192.168.5.6", "sa", "datphat@hanoi" });
                            dt.Rows.Add(new string[] { "Bảo Thắng", "192.168.11.6", "sa", "datphat@hanoi" });
                            dt.Rows.Add(new string[] { "Sa Pa", "192.168.12.6", "sa", "datphat@hanoi" });
                            dt.Rows.Add(new string[] { "Tổng kho cũ", "192.168.0.8", "sa", "datphat@hanoi" });

                            dt.WriteXml(xmlFileServerList, System.Data.XmlWriteMode.WriteSchema);
                        }
                    }
                    ds.ReadXml(xmlFileServerList, System.Data.XmlReadMode.ReadSchema);
                    return ds.Tables[0];
                }
            }
        }
    }
}
