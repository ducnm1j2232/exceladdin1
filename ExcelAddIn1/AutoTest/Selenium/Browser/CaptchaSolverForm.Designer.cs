﻿namespace ExcelAddIn1.AutoTest.Selenium.Browser
{
    partial class CaptchaSolverForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonSend = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(550, 130);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(12, 148);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(550, 20);
            this.textEdit1.TabIndex = 1;
            // 
            // simpleButtonSend
            // 
            this.simpleButtonSend.Location = new System.Drawing.Point(243, 199);
            this.simpleButtonSend.Name = "simpleButtonSend";
            this.simpleButtonSend.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSend.TabIndex = 2;
            this.simpleButtonSend.Text = "Chấp nhận";
            this.simpleButtonSend.Click += new System.EventHandler(this.simpleButtonSend_Click);
            // 
            // CaptchaSolverForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 269);
            this.Controls.Add(this.simpleButtonSend);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "CaptchaSolverForm";
            this.Text = "CaptchaSolverForm";
            this.Shown += new System.EventHandler(this.CaptchaSolverForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSend;
    }
}