﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace ExcelAddIn1.AutoTest.Selenium.Browser
{
    public class Element : ILocator
    {
        private OpenQA.Selenium.IWebDriver _driver { get; set; }

        private List<IWebElement> _webElementCollection;
        private OpenQA.Selenium.By _by { get; set; }

        private System.TimeSpan _timeout;


        public Element(List<IWebElement> webElementCollection, IWebDriver webDriver, System.TimeSpan timeout)
        {
            this._driver = webDriver;
            this._webElementCollection = webElementCollection;
            this._timeout = timeout;
        }

        public OpenQA.Selenium.IWebDriver ElementDriver
        {
            get { return _driver; }
            set { _driver = value; }
        }

        public List<Element> WebElementCollection
        {
            get
            {
                List<Element> list = new List<Element>();
                foreach (IWebElement webElement in _webElementCollection)
                {
                    List<IWebElement> elements = new List<IWebElement>();
                    elements.Add(webElement);
                    list.Add(new Element(elements, _driver, _timeout));
                }

                return list;
            }
        }

        public IWebElement GetIWebElement
        {
            get
            {
                return _webElementCollection[0];
            }
        }

        public System.TimeSpan TimeOut
        {
            get => _timeout;
            set
            {
                _timeout = value;
            }
        }


        private void FindElementsWithTimeout(List<IWebElement> collection, IWebElement element, By by, System.TimeSpan timeSpan)
        {
            bool timeout = false;
            Task t = Task.Run(() =>
            {
                System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> foundCollection = null;
                while (!timeout)
                {
                    Notification.Wait.Show($"element finding inside while {by}");
                    try
                    {
                        foundCollection = element.FindElements(by);
                        if (foundCollection != null && foundCollection.Count > 0)
                        {
                            break;
                        }
                    }
                    catch (OpenQA.Selenium.WebDriverException)
                    {
                        break;
                    }
                }

                if (foundCollection == null)
                {
                    return;
                }

                foreach (var item in foundCollection)
                {
                    collection.Add(item);
                }
            });

            if (!t.Wait(timeSpan))
            {
                Notification.Wait.Show("element find timeout");
                timeout = true;
            }
            Notification.Wait.Close();

        }
        public Element Locator(OpenQA.Selenium.By by, string containText = "")
        {
            return LocatorCore(by, containText, _timeout);
        }
        public Element Locator(OpenQA.Selenium.By by, System.TimeSpan timeSpan, string containText = "")
        {
            return LocatorCore(by, containText, timeSpan);
        }

        private Element LocatorCore(OpenQA.Selenium.By by, string containText, System.TimeSpan timeSpan)
        {
            List<IWebElement> collection = new List<IWebElement>();
            List<Task> findTaskList = new List<Task>();

            foreach (var element in _webElementCollection)
            {
                var findTask = Task.Run(() =>
                {
                   Common.FindElementsWithTimeout(collection, element, by, timeSpan);
                });
                findTaskList.Add(findTask);
            }

            Task.WaitAll(findTaskList.ToArray());

            if (collection.Count == 0)
            {
                return null;
            }

            List<IWebElement> newCollection = new List<IWebElement>();
            if (!string.IsNullOrEmpty(containText))
            {
                foreach (var element in collection)
                {
                    try
                    {
                        if (element.Text.Contains(containText))
                        {
                            newCollection.Add(element);
                        }
                    }
                    catch (OpenQA.Selenium.StaleElementReferenceException)
                    {

                    }
                }

                return new Element(newCollection, _driver, _timeout);
            }

            return new Element(collection, _driver, _timeout);
        }

        public Element Nth(int index)
        {
            if (_webElementCollection == null || _webElementCollection.Count == 0)
            {
                throw new System.Exception("collection count = 0");
            }

            List<IWebElement> collection = new List<IWebElement>();
            collection.Add(_webElementCollection[index]);
            return new Element(collection, _driver, _timeout);
        }

        public bool IsStable()
        {
            foreach (var element in _webElementCollection)
            {
                for (int i = 0; i < 3; i++)
                {
                    var oldLocation = element.Location;
                    System.Threading.Thread.Sleep(400);
                    var newLocation = element.Location;

                    if (oldLocation != newLocation)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public void Click(Enum.ClickType clickType = Enum.ClickType.Mouse)
        {
            if (_webElementCollection == null)
            {
                throw new System.Exception("_webElementCollection was null");
            }

            if (_webElementCollection.Count == 0)
            {
                throw new System.Exception("Không có element để click");
            }

            if (_webElementCollection.Count > 1)
            {
                throw new System.Exception("có nhiều hơn 1 element để click");
            }

            var element = _webElementCollection[0];
            if (!IsClickable())
            {
                //throw new System.Exception($"{element} is not clickable");
            }

            ScrollIntoView();

            switch (clickType)
            {
                case Enum.ClickType.JS:
                    IJavaScriptExecutor javaScriptExecutor = (IJavaScriptExecutor)_driver;
                    javaScriptExecutor.ExecuteScript("arguments[0].click();", element);
                    break;
                case Enum.ClickType.Mouse:
                    if (element.TagName.Equals("span") || element.TagName.Equals("i"))//cannot click on span, so get parent
                    {
                        IJavaScriptExecutor javaScriptExecutor2 = (IJavaScriptExecutor)_driver;
                        IWebElement parent = (IWebElement)javaScriptExecutor2.ExecuteScript("return arguments[0].parentNode;", element);
                        if (parent != null && parent.TagName.Equals("button"))
                        {
                            parent.Click();
                            break;
                        }
                    }

                    element.Click();
                    break;
            }

            System.Threading.Thread.Sleep(300);
            WaitDOMchange();
        }

        public void ScrollIntoView()
        {
            IWebElement element = this._webElementCollection[0];
            ((OpenQA.Selenium.IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].scrollIntoView();", element);
            WaitDOMchange();
        }

        public void Clear()
        {

            OpenQA.Selenium.Interactions.Actions actions = new OpenQA.Selenium.Interactions.Actions(_driver);
            actions.KeyDown(OpenQA.Selenium.Keys.Control)
                .SendKeys("a")
                .KeyUp(Keys.Control)
                .SendKeys(Keys.Delete)
                .Build()
                .Perform();
        }

        public void SetValue(string value)
        {
            if (_webElementCollection == null)
            {
                throw new System.Exception("_webElementCollection was null");
            }

            if (_webElementCollection.Count == 0)
            {
                throw new System.Exception("có 0 element để set value");
            }

            if (_webElementCollection.Count > 1)
            {
                throw new System.Exception("có nhiều hơn 1 element để set value");
            }

            var element = _webElementCollection[0];
            element.Clear();
            element.SendKeys(value);
        }
        public bool IsClickable()
        {
            if (_webElementCollection == null)
            {
                throw new System.Exception("_webElementCollection was null");
            }

            if (_webElementCollection.Count == 0)
            {
                throw new System.Exception("Không có element");
            }

            if (_webElementCollection.Count > 1)
            {
                throw new System.Exception("có nhiều hơn 1 element");
            }

            var element = _webElementCollection[0];

            Task t = Task.Run(() =>
            {
                while (true)
                {
                    try
                    {
                        if (element != null && element.Displayed && element.Enabled && element.Stable())
                        {
                            break;
                        }
                    }
                    catch (OpenQA.Selenium.WebDriverException)
                    {
                        break;
                    }
                }
            });

            if (!t.Wait(_timeout))
            {
                return false;
            }

            return true;

        }
        public void RemoveHighlight()
        {
            //OpenQA.Selenium.IJavaScriptExecutor javaScriptExecutor = (OpenQA.Selenium.IJavaScriptExecutor)_driver;
            //javaScriptExecutor.ExecuteScript("arguments[0].style.border='none'", _webElement);
        }
        public string Text
        {
            get
            {
                return _webElementCollection[0].Text;
            }
        }
        public string OuterHTML => _webElementCollection[0].GetAttribute("outerHTML");
        public System.Drawing.Bitmap GetScreenShot()
        {
            if (_webElementCollection.Count != 1)
            {
                throw new System.Exception($"element có {_webElementCollection.Count} cái lận");
            }

            var element = _webElementCollection[0];

            Screenshot myScreenShot = ((ITakesScreenshot)_driver).GetScreenshot();

            using (var img = new System.Drawing.Bitmap(new System.IO.MemoryStream(myScreenShot.AsByteArray)))
            {
                return img.Clone(new System.Drawing.Rectangle(element.Location, new System.Drawing.Size(element.Size.Width + 10, element.Size.Height + 10)), img.PixelFormat);
            }
        }

        public void Wait()
        {
            if (_webElementCollection == null)
            {
                throw new System.Exception("null");
            }

            if (_webElementCollection.Count == 0)
            {
                throw new System.Exception($"cannot wait because collection has 0 element");
            }




        }
        public void WaitDOMchange()
        {
            while (true)
            {
                try
                {
                    if ((string)((IJavaScriptExecutor)_driver).ExecuteScript("return document.readyState").ToString() == "complete"
                        && (bool)((IJavaScriptExecutor)_driver).ExecuteScript("return jQuery.active == 0") == true)
                    {
                        break;
                    }
                }
                catch
                {
                    break;
                }
            }
        }




    }
}
