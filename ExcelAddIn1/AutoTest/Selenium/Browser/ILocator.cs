﻿namespace ExcelAddIn1.AutoTest.Selenium.Browser
{
    public interface ILocator
    {
        Element Locator(OpenQA.Selenium.By by, string containText);

        //Element LocatorCore(OpenQA.Selenium.By by, string containText, System.TimeSpan timeSpan);

        System.TimeSpan TimeOut { get; set; }
    }
}
