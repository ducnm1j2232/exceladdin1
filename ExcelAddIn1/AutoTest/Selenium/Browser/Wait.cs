﻿using System;
using System.Threading.Tasks;

namespace ExcelAddIn1.AutoTest.Selenium.Browser
{
    public static class Wait
    {

        public const int DefaultTimeout = 30;
        public static void WaitAttached()
        {
            bool timeout = false;
            TimeSpan time = TimeSpan.FromSeconds(DefaultTimeout);

            Task task = Task.Run(() =>
            {
                while (!timeout)
                {

                }
            });

            if (!task.Wait(time))
            {
                timeout = true;
            }
        }


    }
}
