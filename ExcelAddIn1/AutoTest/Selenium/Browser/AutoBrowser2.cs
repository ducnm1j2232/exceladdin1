﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExcelAddIn1.AutoTest.Selenium.Browser
{
    public class AutoBrowser2 : ILocator
    {
        private TimeSpan _timeout;

        public AutoBrowser2(TimeSpan timeout, OpenQA.Selenium.Chrome.ChromeOptions options = null)
        {
            Setup.SetupWebDriver();
            OpenQA.Selenium.Chrome.ChromeOptions defaultOptions = new OpenQA.Selenium.Chrome.ChromeOptions();
            if (options != null)
            {
                defaultOptions = options;
            }

            defaultOptions.AddArgument("--start-maximized");
            defaultOptions.AddArgument("disable-blink-features=AutomationControlled");//https://stackoverflow.com/questions/60649179/chromedriver-with-selenium-displays-a-blank-page
            defaultOptions.AddArgument("--incognito");

            ChromeDriverCore = new OpenQA.Selenium.Chrome.ChromeDriver(defaultOptions);
            _timeout = timeout;
        }

        public OpenQA.Selenium.Chrome.ChromeDriver ChromeDriverCore { get; set; }

        public OpenQA.Selenium.Support.UI.WebDriverWait WaitCore { get; set; }

        public OpenQA.Selenium.Chrome.ChromeOptions Options { get; set; }

        public TimeSpan TimeOut { get; set; }

        public void WaitDOMchange()
        {
            while (true)
            {
                try
                {
                    System.Threading.Thread.Sleep(500);
                    if ((string)ChromeDriverCore.ExecuteScript("return document.readyState").ToString() == "complete"
                        && (bool)ChromeDriverCore.ExecuteScript("return jQuery.active == 0") == true)
                    {
                        break;
                    }
                }
                catch
                {
                    break;
                }
            }
        }

        public void Goto(string url)
        {
            while (true)
            {
                try
                {
                    ChromeDriverCore.Url = url;
                    if (ChromeDriverCore.Url == url)
                    {
                        break;
                    }
                }
                catch (OpenQA.Selenium.WebDriverException)
                {

                }
            }
        }

        public void Quit()
        {
            //ChromeDriverCore.Close();
            ChromeDriverCore.Quit();
        }

        public Element Locator(OpenQA.Selenium.By by, string containText = "")
        {
            return LocatorCore(by, containText, _timeout);
        }

        public Element Locator(OpenQA.Selenium.By by, System.TimeSpan timeSpan, string containText = "")
        {
            return LocatorCore(by, containText, timeSpan);
        }

        private Element LocatorCore(OpenQA.Selenium.By by, string containText, System.TimeSpan timeSpan)
        {
            List<IWebElement> collection = new List<IWebElement>();
            List<Task> findTaskList = new List<Task>();

            var findTask = Task.Run(() =>
            {
                Common.FindElementsWithTimeout(collection, ChromeDriverCore, by, timeSpan);
            });

            findTask.Wait();

            if (collection.Count == 0)
            {
                return null;
            }

            List<IWebElement> newCollection = new List<IWebElement>();
            if (!string.IsNullOrEmpty(containText))
            {
                foreach (var element in collection)
                {
                    if (element.Text.Contains(containText))
                    {
                        newCollection.Add(element);
                    }
                }

                return new Element(newCollection, ChromeDriverCore, _timeout);
            }

            return new Element(collection, ChromeDriverCore, _timeout);
        }

        private void FindElementsWithTimeout(List<IWebElement> collection, IWebDriver driver, By by)
        {
            bool timeout = false;
            Task t = Task.Run(() =>
            {
                //Notification.Wait.Show($"browser finding inside while {by}");
                while (!timeout)
                {
                    var foundCollection = driver.FindElements(by);
                    if (foundCollection != null && foundCollection.Count > 0)
                    {
                        foreach (var item in foundCollection)
                        {
                            collection.Add(item);
                        }
                        break;
                    }
                }
            });

            if (!t.Wait(_timeout))
            {
                timeout = true;
                //throw new System.Exception($"time out when finding {by}");
            }


        }

        public void WaitValueChange(Element element, string oldValue)
        {
            var stopWatch = System.Diagnostics.Stopwatch.StartNew();

            bool timeout = false;
            Task t = Task.Run(() =>
            {
                while (!timeout)
                {
                    System.Threading.Thread.Sleep(500);
                }
            });

            if (!t.Wait(_timeout))
            {
                timeout = true;
            }

            Notification.Wait.Close();
        }

        public void RunScript(string script, Element element)
        {
            ((IJavaScriptExecutor)ChromeDriverCore).ExecuteScript(script, element.GetIWebElement);
        }

    }
}
