﻿using System.Drawing;
using System.Windows.Forms;

namespace ExcelAddIn1.AutoTest.Selenium.Browser
{
    public partial class CaptchaSolverForm : DevExpress.XtraEditors.XtraForm
    {
        public CaptchaSolverForm(Bitmap bitmap)
        {
            InitializeComponent();

            pictureBox1.Image = bitmap;
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        internal string _captchaString = string.Empty;
        public string CaptchaString
        {
            get { return _captchaString; }
            set { _captchaString = value; }
        }

        private void simpleButtonSend_Click(object sender, System.EventArgs e)
        {
            _captchaString = textEdit1.EditValue.ToString();
            Close();
        }

        public string ShowForm()
        {
            this.TopMost = true;
            this.BringToFront();
            this.ShowDialog();
            //this.BringToFront();
            return _captchaString;
        }

        private void CaptchaSolverForm_Shown(object sender, System.EventArgs e)
        {
            this.BringToFront();
        }
    }
}