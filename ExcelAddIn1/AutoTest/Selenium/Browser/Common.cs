﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExcelAddIn1.AutoTest.Selenium.Browser
{
    public static class Common
    {
        public static void FindElementsWithTimeout(List<IWebElement> collection, object driverOrElement, By by, System.TimeSpan timeSpan)
        {
            Type type = driverOrElement.GetType();
            IWebDriver driver = null;
            IWebElement element = null;

            if (type.FullName == "OpenQA.Selenium.Chrome.ChromeDriver")
            {
                driver = (IWebDriver)driverOrElement;
            }

            if (type.FullName == "OpenQA.Selenium.Remote.RemoteWebElement")
            {
                element = (IWebElement)driverOrElement;
            }

            bool timeout = false;
            Task t = Task.Run(() =>
            {
                System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> foundCollection = null;
                while (!timeout)
                {
                    Notification.Wait.Show($"element finding inside while {by}");
                    try
                    {
                        if (driver != null)
                        {
                            foundCollection = driver.FindElements(by);
                        }

                        if (element != null)
                        {
                            foundCollection = element.FindElements(by);
                        }

                        if (foundCollection != null && foundCollection.Count > 0)
                        {
                            break;
                        }
                    }
                    catch (OpenQA.Selenium.WebDriverException)
                    {
                        break;
                    }
                }

                if (foundCollection == null)
                {
                    return;
                }

                foreach (var item in foundCollection)
                {
                    collection.Add(item);
                }
            });

            if (!t.Wait(timeSpan))
            {
                Notification.Wait.Show("element find timeout");
                timeout = true;
            }
            Notification.Wait.Close();
        }


    }
}
