﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.AutoTest.Selenium.Browser
{
    public static class Setup
    {
        public static void SetupWebDriver()
        {
            new WebDriverManager.DriverManager().SetUpDriver(new WebDriverManager.DriverConfigs.Impl.ChromeConfig(), new WebDriverManager.DriverConfigs.Impl.ChromeConfig().GetMatchingBrowserVersion());
        }
    }
}
