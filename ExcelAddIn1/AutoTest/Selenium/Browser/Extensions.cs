﻿
using OpenQA.Selenium;

namespace ExcelAddIn1.AutoTest.Selenium.Browser
{
    public static class Extensions
    {
        public static OpenQA.Selenium.IWebElement Locator(this OpenQA.Selenium.IWebElement element, string cssSelector, string containText = "")
        {
            var collections = element.FindElements(OpenQA.Selenium.By.CssSelector(cssSelector));
            if (collections != null && collections.Count > 0)
            {
                if (string.IsNullOrEmpty(containText))
                {
                    return collections[0];
                }
                else
                {
                    foreach (var item in collections)
                    {
                        try
                        {
                            if (item.Text.Contains(containText))
                            {
                                return item;
                            }
                        }
                        catch (OpenQA.Selenium.StaleElementReferenceException)
                        {

                        }
                    }
                }
            }

            return null;
        }

        public static void SetValue(this OpenQA.Selenium.IWebElement element, string value)
        {
            element.Clear();
            element.SendKeys(value);
        }

        public static void Wait(this OpenQA.Selenium.IWebElement element)
        {
            while (true)
            {
                if (element != null)
                {
                    break;
                }
            }
        }

        public static bool Stable(this OpenQA.Selenium.IWebElement element)
        {
            for (int i = 0; i < 4; i++)
            {
                var oldLocation = element.Location;
                System.Threading.Thread.Sleep(200);
                var newLocation = element.Location;

                if (oldLocation != newLocation)
                {
                    return false;
                }
            }
            return true;
        }

        public static void Click2(this OpenQA.Selenium.IWebElement element)
        {
            while (true)
            {
                if (element != null && element.Enabled)
                {
                    break;
                }
            }
            element.Click();
        }


    }
}
