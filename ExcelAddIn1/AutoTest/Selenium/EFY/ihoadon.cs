﻿using OpenQA.Selenium;
using System;
using System.Threading.Tasks;
using VSTOLib.Extensions;


namespace ExcelAddIn1.AutoTest.Selenium.EFY
{
    public static class ihoadon
    {
        private static AutoTest.Selenium.Browser.AutoBrowser2 _autoBrowser;
        private static string ihoadonUrl = "https://ihoadon.com.vn/login";

        private static void Login()
        {
            _autoBrowser.Goto(ihoadonUrl);

            _autoBrowser.Locator(By.CssSelector("input[placeholder='Mã số thuế *']")).SetValue(Config.Manager.GetValue(Config.ID.ihoadonMST));
            _autoBrowser.Locator(By.CssSelector("input[placeholder='Tên đăng nhập *']")).SetValue(Config.Manager.GetValue(Config.ID.ihoadonUsername));
            _autoBrowser.Locator(By.CssSelector("input[placeholder='Mật khẩu *']")).SetValue(Config.Manager.GetValue(Config.ID.ihoadonPassword));
            _autoBrowser.Locator(By.CssSelector("button[type='submit']")).Click();
        }

        public static void DownloadInvoicePDF(DateTime fromDate, DateTime toDate, string download_directory)
        {
            OpenQA.Selenium.Chrome.ChromeOptions chromeOptions = new OpenQA.Selenium.Chrome.ChromeOptions();
            chromeOptions.AddUserProfilePreference("download.default_directory", Config.Manager.GetValue(Config.ID.InvoicePDFSaveFolder));
            chromeOptions.AddUserProfilePreference("plugins.always_open_pdf_externally", true);
            chromeOptions.AddUserProfilePreference("download.prompt_for_download", false);
            chromeOptions.AddUserProfilePreference("download.directory_upgrade", true);
            _autoBrowser = new Browser.AutoBrowser2(TimeSpan.FromMinutes(15), chromeOptions);

            Login();
            _autoBrowser.Locator(By.CssSelector("div[class='col-md-4 article-group']"), "Lập hóa đơn").Click();

            for (DateTime date = fromDate; date <= toDate; date = date.AddDays(1))
            {
                ExtractData(date, download_directory);
            }

            _autoBrowser.Quit();
            System.Windows.Forms.MessageBox.Show("Tải hóa đơn PDF thành công");

        }

        private static void ExtractData(DateTime invoiceDate, string download_directory)
        {
            SetDate(invoiceDate);

            var gridNodata = _autoBrowser.Locator(By.CssSelector("div[class='dx-datagrid-nodata']"), TimeSpan.FromSeconds(5));
            if (gridNodata != null)
            {
                return;
            }

            var grid = _autoBrowser.Locator(By.CssSelector("div[class='dx-datagrid-content']"));
            var trCollection = grid.Locator(By.CssSelector("tr[class='dx-row dx-data-row dx-row-lines dx-column-lines'][role='row'][aria-selected='false']"), TimeSpan.FromSeconds(10));

            if (trCollection == null || trCollection.WebElementCollection.Count == 0)
            {
                return;
            }

            foreach (var eachTrRow in trCollection.WebElementCollection)
            {
                DownloadFile(eachTrRow, download_directory);
            }
        }

        private static void SetDate(DateTime invoiceDate)
        {
            _autoBrowser.Locator(By.CssSelector("div[class='column day']"), "Từ ngày")
                            .Locator(By.CssSelector("input[role='combobox']"))
                            .SetValue(invoiceDate.ToString("dd/MM/yyyy"));

            _autoBrowser.Locator(By.CssSelector("div[class='column day']"), "Từ ngày").Click();//fake click to lost focus

            _autoBrowser.Locator(By.CssSelector("div[class='column day']"), "Đến ngày")
                .Locator(By.CssSelector("input[role='combobox']"))
                .SetValue(invoiceDate.ToString("dd/MM/yyyy"));

            _autoBrowser.Locator(By.CssSelector("div[class='column day']"), "Từ ngày").Click();//fake click to lost focus
        }

        private static void DownloadFile(Browser.Element eachTrRow, string download_directory)
        {
            string invoiceNumber = eachTrRow.Locator(By.CssSelector("a[class='gach-chan']")).Text;
            invoiceNumber = invoiceNumber.GetStringBefore("-CĐ");
            string invoiceDate = eachTrRow.Locator(By.CssSelector("td[aria-colindex='4']")).Text;
            invoiceDate = invoiceDate.Replace('/', '-');
            string invoiceFullname = $"{invoiceNumber}_{invoiceDate}";

            eachTrRow.Locator(By.CssSelector("a[class='xemIcon actionIcon'][title='Xem hóa đơn']")).Click();

            string mainWindowHandle = _autoBrowser.ChromeDriverCore.CurrentWindowHandle;
            var handles = _autoBrowser.ChromeDriverCore.WindowHandles;
            while (true)
            {
                System.Threading.Thread.Sleep(500);
                handles = _autoBrowser.ChromeDriverCore.WindowHandles;
                if (handles.Count == 2) break;
                System.Threading.Thread.Sleep(500);
            }

            string popupHandle = string.Empty;
            foreach (var handle in handles)
            {
                if (handle != mainWindowHandle)
                {
                    popupHandle = handle;
                }
            }

            _autoBrowser.ChromeDriverCore.SwitchTo().Window(popupHandle);
            string linkDownload = _autoBrowser.Locator(By.CssSelector("iframe")).GetIWebElement.GetAttribute("src");

            System.Threading.Thread.Sleep(1000);
            _autoBrowser.ChromeDriverCore.SwitchTo().Window(popupHandle).Close();

            _autoBrowser.ChromeDriverCore.SwitchTo().Window(mainWindowHandle);
            _autoBrowser.ChromeDriverCore.Url = linkDownload;
            System.Threading.Thread.Sleep(2000);

            //Save
            string[] pdfFiles = null;
            string pdfPath = Config.Manager.GetValue(Config.ID.InvoicePDFSaveFolder);
            //VSTOLib.IO.FileAndFolder.DeleteAll(pdfPath);

            bool timeout = false;
            Task task = Task.Run(() =>
            {
                while (!timeout)
                {
                    pdfFiles = System.IO.Directory.GetFiles(pdfPath, "*.pdf");
                    if (pdfFiles.Length > 0) break;
                    System.Threading.Thread.Sleep(1000);
                }
            });

            if (!task.Wait(TimeSpan.FromSeconds(10)))
            {
                timeout = true;
            }

            if (pdfFiles.Length == 0) return;
            string pdf_file_name = pdfFiles[0];
            string newFilename = string.Format(@"{0}\{1}.pdf", download_directory, invoiceFullname);
            if (System.IO.File.Exists(newFilename))
            {
                System.IO.File.Delete(newFilename);
            }
            System.IO.File.Move(pdf_file_name, newFilename);

            VSTOLib.IO.FileAndFolder.DeleteAll(pdfPath);


        }
    }
}
