﻿namespace ExcelAddIn1.AutoTest.Selenium.EFY
{
    public partial class EFYAuto
    {
        private OpenQA.Selenium.IWebElement textboxMST
        {
            get
            {
                string xpath = "/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[1]/div/div/input";
                return autoBrowser.GetElementByXpath(xpath);
            }
        }
        private OpenQA.Selenium.IWebElement textboxUserName
        {
            get
            {
                string xpath = "/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[2]/div/div/input";
                return autoBrowser.GetElementByXpath(xpath);
            }
        }

        private OpenQA.Selenium.IWebElement textboxPassWord
        {
            get
            {
                string xpath = "/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[3]/div/div/input";
                return autoBrowser.GetElementByXpath(xpath);
            }
        }

        private OpenQA.Selenium.IWebElement buttonSignin
        {
            get
            {
                string xpath = "/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[6]/div[1]/button/i";
                return autoBrowser.GetElementByXpath(xpath);
            }
        }

        private OpenQA.Selenium.IWebElement buttonDongThongBaoTT78 => autoBrowser.GetWebElementWithTimeout("/html/body/modal-container/div/div/app-notify-warning/div[3]/dx-button/div/span", 10);

        private OpenQA.Selenium.IWebElement buttonLapHoaDon => autoBrowser.GetElementByCSSselector("article[_ngcontent-c1='']", "Lập hóa đơn");

        private OpenQA.Selenium.IWebElement buttonLapHD => autoBrowser.GetElementByCSSselector("span[class='dx-button-text']", "Lập HĐ");

        private OpenQA.Selenium.IWebElement labelNgayHD => autoBrowser.GetElementByCSSselector("span[class='dx-field-item-label-text']", "Ngày HĐ:");

        private OpenQA.Selenium.IWebElement comboboxHinhThucXuat => autoBrowser.GetElementByXpath("/html/body/app-root/app-system/div/main/div/app-create-invoice/form/div/vat-form-invoice/div/div[1]/dx-form[1]/div/div/div/div[1]/div/div/div/div/div/div/div/div/div[4]/div/div[1]/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/div/div/dx-select-box/div[1]/div/div[1]/input");

        private OpenQA.Selenium.IWebElement itemXuatChoKy => autoBrowser.GetElementByCSSselector("div[class='dx-item-content dx-list-item-content']", "Xuất chờ ký");

        private OpenQA.Selenium.IWebElement buttonInvoiceNumberSearchBox => autoBrowser.GetElementByXpath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input");
        private OpenQA.Selenium.IWebElement textboxMaKhachHang => autoBrowser.GetElementByXpath("/html/body/app-root/app-system/div/main/div/app-create-invoice/form/div/vat-form-invoice/div/div[1]/dx-form[1]/div/div/div/div[1]/div/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/div/div/dx-autocomplete/div/div/div[1]/input");
        private OpenQA.Selenium.IWebElement textboxMaKhachHangDropDownFistResult => autoBrowser.GetElementByXpath("/html/body/div/div/div/div/div[1]/div/div[1]/div[2]/div/div/span");
        private OpenQA.Selenium.IWebElement checkboxKhongTuTinhGiaTri => autoBrowser.GetElementByCSSselector("span[class='dx-checkbox-text']", "Không tự tính giá trị");
        private OpenQA.Selenium.IWebElement buttonNapTuExcel => autoBrowser.GetElementByXpath("/html/body/app-root/app-system/div/main/div/app-create-invoice/form/div/vat-product-grip/div[1]/div/div[2]/dx-menu/div/ul/li/div/div[1]/span[1]");
        private OpenQA.Selenium.IWebElement buttonNapDuLieu => autoBrowser.GetElementByXpath("/html/body/div/div/div[1]/ul/li[2]/div/div/span");
        private OpenQA.Selenium.IWebElement buttonThayTheToanBo => autoBrowser.GetElementByXpath("/html/body/div/div/div[2]/div/div/dx-button[1]/div/span");
        private OpenQA.Selenium.IWebElement buttonGhiTam => autoBrowser.GetElementByXpath("/html/body/app-root/app-system/div/main/div/app-create-invoice/form/div/div[3]/div/div[2]/dx-button[2]/div/span");
        private OpenQA.Selenium.IWebElement buttonXuatHoaDon => autoBrowser.GetElementByCSSselector("span[class='dx-button-text']", "Xuất hóa đơn");
        private OpenQA.Selenium.IWebElement labelDongY => autoBrowser.GetElementByXpath("/html/body/div[3]/a[1]");
        private OpenQA.Selenium.IWebElement buttonDongY => autoBrowser.GetElementByCSSselector("input[id='confirmYes']");
        private OpenQA.Selenium.IWebElement buttonYes => autoBrowser.GetElementByCSSselector("span[class='dx-button-text']","Yes");
        private OpenQA.Selenium.IWebElement toastNotification => autoBrowser.GetElementByXpath("/html/body/div/div/div/div[1]");
        private OpenQA.Selenium.IWebElement buttonDong => autoBrowser.GetElementByXpath("/html/body/app-root/app-system/div/main/div/app-create-invoice/form/div/div[3]/div/div[2]/dx-button[4]/div/span");

        //private OpenQA.Selenium.IWebElement buttonDong_ThongBaoTT78 => autoBrowser.GetWebElement("");










    }
}
