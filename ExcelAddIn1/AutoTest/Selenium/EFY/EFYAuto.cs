﻿using ExcelAddIn1.AutoTest.Selenium;

namespace ExcelAddIn1.AutoTest.Selenium.EFY
{
    public partial class EFYAuto
    {
        private const string TEN_KHACH_LE = "Khách lẻ không lấy hóa đơn";
        private const string MA_KHACH_LE = "KLE000001";
        private const string TEN_THANH_CONG = "Công ty TNHH Đầu tư Dịch vụ Thành Công";
        private const string MA_THANH_CONG = "5300680806";

        AutoBrowser autoBrowser;
        public EFYAuto()
        {
            OpenQA.Selenium.Chrome.ChromeOptions chromeOptions = new OpenQA.Selenium.Chrome.ChromeOptions();
            chromeOptions.AddUserProfilePreference("download.default_directory", PublicVar.Download_PDF_iHoaDon());
            //chromeOptions.AddUserProfilePreference("plugins.always_open_pdf_externally", true);
            chromeOptions.AddArgument("--no-sandbox");
            chromeOptions.AddArgument("--remote-debugging-port=9222");
            autoBrowser = new AutoBrowser(System.TimeSpan.FromMinutes(20), chromeOptions);
        }

        public void Exit()
        {
            autoBrowser.Exit();
        }

        public void Signin()
        {
            string mst = ExcelAddIn1.MyClass.AddinConfig.EFYmst;
            string username = ExcelAddIn1.MyClass.AddinConfig.EFYusername;
            string password = ExcelAddIn1.MyClass.AddinConfig.EFYpassword;

            SignIn(mst, username, password);
        }

        public void SignIn(string mst, string username, string password)
        {
            string url = "https://ihoadon.com.vn/login";
            ExcelAddIn1.Notification.Wait.Show($"Mở trang web: {url}");
            autoBrowser.ChromeDriverCore.Url = url;
            autoBrowser.ChromeDriverCore.Navigate();

            ExcelAddIn1.Notification.Wait.Show("Nhập MST");
            autoBrowser.SetValue(textboxMST, mst);

            ExcelAddIn1.Notification.Wait.Show("Nhập tên đăng nhập");
            autoBrowser.SetValue(textboxUserName, username);

            ExcelAddIn1.Notification.Wait.Show("Nhập mật khẩu");
            autoBrowser.SetValue(textboxPassWord, password);

            ExcelAddIn1.Notification.Wait.Show("Click đăng nhập");
            autoBrowser.MouseClick(buttonSignin);

            ExcelAddIn1.Notification.Wait.Close();

            //đóng thông báo thông tư 78
            //ExcelAddIn1.Notification.Wait.Show("Đóng thông báo thông tư 78");
            //autoBrowser.SmartClick(buttonDongThongBaoTT78);
        }

        public void CloseNotification78()
        {
            try
            {
                autoBrowser.MouseClick(buttonDongThongBaoTT78);
            }
            catch { }
        }

        public void ImportInvoicesDatPhat(System.Collections.Generic.List<UserClass.Invoice> invoiceInfoList, string path)
        {
            autoBrowser.MouseClick(buttonLapHoaDon);

            foreach (UserClass.Invoice invoiceInfo in invoiceInfoList)
            {
                #region main
                string invoiceNumber = invoiceInfo.InvoiceNumber;
                //buttonInvoiceNumberSearchBox.SetText(invoiceNumber);
                ExcelAddIn1.Notification.Wait.Show("Tìm kiếm số hóa đơn");
                autoBrowser.SetValue(buttonInvoiceNumberSearchBox, invoiceNumber);

                string firstResult = "/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[2]/div/a";
                while (true)//chờ 1 tí để số hóa đơn hiện lên đúng với số mong muốn
                {
                    autoBrowser.WaitDOMchange();
                    try
                    {
                        if (autoBrowser.GetElementByXpath(firstResult).Text == invoiceNumber)
                        {
                            break;
                        }
                    }
                    catch (OpenQA.Selenium.StaleElementReferenceException)
                    { }
                }

                ExcelAddIn1.Notification.Wait.Show("Click kết quả đầu tiên");
                autoBrowser.MouseClick(autoBrowser.GetElementByXpath(firstResult));

                //điền mã khách hàng
                string customer_MST = string.Empty;
                if (invoiceInfo.CustomerName == TEN_KHACH_LE) customer_MST = MA_KHACH_LE;
                if (invoiceInfo.CustomerName == TEN_THANH_CONG) customer_MST = MA_THANH_CONG;

                //textboxMaKhachHang.SetText(customer_MST);
                ExcelAddIn1.Notification.Wait.Show("Nhập mã khách hàng");
                autoBrowser.SetValue(textboxMaKhachHang, customer_MST);

                ExcelAddIn1.Notification.Wait.Show("Click mã khách hàng");
                autoBrowser.MouseClick(textboxMaKhachHangDropDownFistResult);

                //check không tự tính giá trị
                if (!checkboxKhongTuTinhGiaTri.IsCheckboxEFYChecked())
                {
                    ExcelAddIn1.Notification.Wait.Show("Tick không tính giá trị");
                    autoBrowser.MouseClick(checkboxKhongTuTinhGiaTri);
                }

                //nạp từ file Excel
                ExcelAddIn1.Notification.Wait.Show("Click nạp từ excel");
                autoBrowser.MouseClick(buttonNapTuExcel);

                ExcelAddIn1.Notification.Wait.Show("Click nạp dữ liệu");
                autoBrowser.MouseClick(buttonNapDuLieu);

                //select file excel
                string excelFile = System.IO.Path.Combine(path, $"iHoaDon {invoiceNumber}.xlsx");

                //select file excel
                string dialogTitle = "Open";
                AutoIt.AutoItX.WinWait(dialogTitle);
                AutoIt.AutoItX.WinWaitActive(dialogTitle);
                AutoIt.AutoItX.ControlSetText(dialogTitle, "", "Edit1", excelFile);
                System.Threading.Thread.Sleep(1000);
                AutoIt.AutoItX.ControlClick(dialogTitle, "", "Button1");
                System.Threading.Thread.Sleep(1000);
                AutoIt.AutoItX.WinWaitClose(dialogTitle);
                System.Threading.Thread.Sleep(1000);

                ExcelAddIn1.Notification.Wait.Show("Click thay thế toàn bộ");
                autoBrowser.MouseClick(buttonThayTheToanBo);

                ExcelAddIn1.Notification.Wait.Show("Click ghi tạm");
                autoBrowser.MouseClick(buttonGhiTam);

                //wait toast
                string xpath_toast = "/html/body/div/div/div/div[1]";
                while (true)
                {
                    try
                    {
                        ExcelAddIn1.Notification.Wait.Show("Chờ thông báo thành công");
                        OpenQA.Selenium.IWebElement element = autoBrowser.GetElementByXpath(xpath_toast);
                        System.Threading.Thread.Sleep(500);
                        if (element != null)
                        {
                            break;
                        }
                    }
                    catch { }
                }

                ExcelAddIn1.Notification.Wait.Show("Click đóng");
                autoBrowser.MouseClick(buttonDong);

                ExcelAddIn1.Notification.Wait.Close();

                #endregion
            }
        }

        public void ImportInvoiceThanhCong(System.Collections.Generic.List<UserClass.Invoice> invoiceInfoList, string path)
        {
            autoBrowser.MouseClick(buttonLapHoaDon);

            foreach (UserClass.Invoice invoiceInfo in invoiceInfoList)
            {
                #region main
                string invoiceNumber = invoiceInfo.InvoiceNumber;
                //buttonInvoiceNumberSearchBox.SetText(invoiceNumber);
                ExcelAddIn1.Notification.Wait.Show("Tìm kiếm số hóa đơn");
                autoBrowser.SetValue(buttonInvoiceNumberSearchBox, invoiceNumber);

                string firstResult = "/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[2]/div/a";
                while (true)//chờ 1 tí để số hóa đơn hiện lên đúng với số mong muốn
                {
                    autoBrowser.WaitDOMchange();
                    try
                    {
                        if (autoBrowser.GetElementByXpath(firstResult).Text == invoiceNumber)
                        {
                            break;
                        }
                    }
                    catch (OpenQA.Selenium.StaleElementReferenceException)
                    { }
                }

                ExcelAddIn1.Notification.Wait.Show("Click kết quả đầu tiên");
                autoBrowser.MouseClick(autoBrowser.GetElementByXpath(firstResult));

                //điền mã khách hàng
                //string customer_MST = string.Empty;
                //if (invoiceInfo.CustomerName == TEN_KHACH_LE) customer_MST = MA_KHACH_LE;
                //if (invoiceInfo.CustomerName == TEN_THANH_CONG) customer_MST = MA_THANH_CONG;

                //textboxMaKhachHang.SetText(customer_MST);
                //ExcelAddIn1.Notification.Wait.Show("Nhập mã khách hàng");
                //autoBrowser.SetValue(textboxMaKhachHang, customer_MST);

                //ExcelAddIn1.Notification.Wait.Show("Click mã khách hàng");
                //autoBrowser.SmartClick(textboxMaKhachHangDropDownFistResult);

                //check không tự tính giá trị
                if (!checkboxKhongTuTinhGiaTri.IsCheckboxEFYChecked())
                {
                    //System.Windows.Forms.MessageBox.Show("chưa check");
                    ExcelAddIn1.Notification.Wait.Show("Tick không tính giá trị");
                    autoBrowser.MouseClick(checkboxKhongTuTinhGiaTri);
                }

                //nạp từ file Excel
                ExcelAddIn1.Notification.Wait.Show("Click nạp từ excel");
                autoBrowser.MouseClick(buttonNapTuExcel);

                ExcelAddIn1.Notification.Wait.Show("Click nạp dữ liệu");
                autoBrowser.MouseClick(buttonNapDuLieu);

                //select file excel
                string excelFile = System.IO.Path.Combine(path, $"iHoaDon {invoiceNumber}.xlsx");

                //select file excel
                string dialogTitle = "Open";
                AutoIt.AutoItX.WinWait(dialogTitle);
                AutoIt.AutoItX.WinWaitActive(dialogTitle);
                AutoIt.AutoItX.ControlSetText(dialogTitle, "", "Edit1", excelFile);
                System.Threading.Thread.Sleep(1000);
                AutoIt.AutoItX.ControlClick(dialogTitle, "", "Button1");
                System.Threading.Thread.Sleep(1000);
                AutoIt.AutoItX.WinWaitClose(dialogTitle);
                System.Threading.Thread.Sleep(1000);

                ExcelAddIn1.Notification.Wait.Show("Click thay thế toàn bộ");
                autoBrowser.MouseClick(buttonThayTheToanBo);

                ExcelAddIn1.Notification.Wait.Show("Click ghi tạm");
                autoBrowser.MouseClick(buttonGhiTam);

                //wait toast
                string xpath_toast = "/html/body/div/div/div/div[1]";
                while (true)
                {
                    try
                    {
                        ExcelAddIn1.Notification.Wait.Show("Chờ thông báo thành công");
                        OpenQA.Selenium.IWebElement element = autoBrowser.GetElementByXpath(xpath_toast);
                        System.Threading.Thread.Sleep(500);
                        if (element != null)
                        {
                            break;
                        }
                    }
                    catch { }
                }

                ExcelAddIn1.Notification.Wait.Show("Click đóng");
                autoBrowser.MouseClick(buttonDong);

                ExcelAddIn1.Notification.Wait.Close();

                #endregion
            }
        }

        public void UploadInvoice(string ngayThang, string maKhachHang, string path, int invoiceNumber)
        {
            var mainWindow = autoBrowser.ChromeDriverCore.CurrentWindowHandle;

            ExcelAddIn1.Notification.Wait.Show($"Click lập hóa đơn");
            autoBrowser.MouseClick(buttonLapHoaDon);

            ExcelAddIn1.Notification.Wait.Show($"Click lập hóa đơn");
            autoBrowser.MouseClick(buttonLapHD);

            ExcelAddIn1.Notification.Wait.Show($"Điền ngày tháng");
            autoBrowser.SetValue(labelNgayHD, ngayThang);

            ExcelAddIn1.Notification.Wait.Show($"Chọn hình thức xuất");
            autoBrowser.MouseClick(comboboxHinhThucXuat);
            autoBrowser.MouseClick(itemXuatChoKy);

            ExcelAddIn1.Notification.Wait.Show($"Điền mã khách hàng");
            autoBrowser.SetValue(textboxMaKhachHang, maKhachHang);
            autoBrowser.MouseClick(textboxMaKhachHangDropDownFistResult);

            //check không tự tính giá trị
            if (!checkboxKhongTuTinhGiaTri.IsCheckboxEFYChecked())
            {
                ExcelAddIn1.Notification.Wait.Show("Tick không tính giá trị");
                autoBrowser.MouseClick(checkboxKhongTuTinhGiaTri);
            }

            //nạp từ file Excel
            ExcelAddIn1.Notification.Wait.Show("Click nạp từ excel");
            autoBrowser.MouseClick(buttonNapTuExcel);

            ExcelAddIn1.Notification.Wait.Show("Click nạp dữ liệu");
            autoBrowser.MouseClick(buttonNapDuLieu);

            //select file excel
            string excelFile = System.IO.Path.Combine(path, $"iHoaDon {invoiceNumber}.xlsx");

            //select file excel
            string dialogTitle = "Open";
            AutoIt.AutoItX.WinWait(dialogTitle);
            AutoIt.AutoItX.WinWaitActive(dialogTitle);
            AutoIt.AutoItX.ControlSetText(dialogTitle, "", "Edit1", excelFile);
            System.Threading.Thread.Sleep(1000);
            AutoIt.AutoItX.ControlClick(dialogTitle, "", "Button1");
            System.Threading.Thread.Sleep(1000);
            AutoIt.AutoItX.WinWaitClose(dialogTitle);
            System.Threading.Thread.Sleep(1000);

            ExcelAddIn1.Notification.Wait.Show("Click thay thế toàn bộ");
            autoBrowser.MouseClick(buttonThayTheToanBo);
            autoBrowser.WaitDOMchange();

            ExcelAddIn1.Notification.Wait.Show("Click xuất hóa đơn");
            autoBrowser.MouseClick(buttonXuatHoaDon);


            //var windowHandles = autoBrowser.ChromeDriverCore.WindowHandles;
            while (true)
            {
                if (autoBrowser.ChromeDriverCore.WindowHandles.Count == 2)
                {
                    Notification.Toast.Show("có 2 cửa sổ");
                    break;
                }
            }
            string popupWindowHandle = autoBrowser.ChromeDriverCore.WindowHandles[1];
            autoBrowser.ChromeDriverCore.SwitchTo().Window(popupWindowHandle);
            Notification.Toast.Show(autoBrowser.ChromeDriverCore.Title);
            ExcelAddIn1.Notification.Wait.Show("Click đồng ý");
            autoBrowser.ScrollToEnd();
            autoBrowser.ForceClick(labelDongY);

            var action = new OpenQA.Selenium.Interactions.Actions(autoBrowser.ChromeDriverCore);
            action.SendKeys(OpenQA.Selenium.Keys.Tab)
                .SendKeys(OpenQA.Selenium.Keys.Tab)
                .SendKeys(OpenQA.Selenium.Keys.Enter)
                .Build()
                .Perform();
            var windowHandles = autoBrowser.ChromeDriverCore.WindowHandles;

            //autoBrowser.SmartClick(buttonDongY);
            while (true)
            {
                if (autoBrowser.ChromeDriverCore.WindowHandles.Count == 1)
                {
                    break;
                }
            }
            autoBrowser.ChromeDriverCore.SwitchTo().Window(mainWindow);
            autoBrowser.MouseClick(buttonYes);



        }
    }
}
