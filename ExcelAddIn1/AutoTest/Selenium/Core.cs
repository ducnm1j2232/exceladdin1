﻿namespace ExcelAddIn1.AutoTest.Selenium
{
    public static class Core
    {
        public static void SetupWebDriver()
        {
            new WebDriverManager.DriverManager().SetUpDriver(new WebDriverManager.DriverConfigs.Impl.ChromeConfig(), new WebDriverManager.DriverConfigs.Impl.ChromeConfig().GetMatchingBrowserVersion());
        }

        public static void SetText(this OpenQA.Selenium.IWebElement element, string text)
        {
            ExcelAddIn1.MyClass.ClipboardSafeInvoke.SetText(text);
            element.Click();


            System.Windows.Forms.SendKeys.SendWait("{HOME}");
            System.Windows.Forms.SendKeys.SendWait("+{END}");
            System.Windows.Forms.SendKeys.SendWait("{DELETE}");

            //element.Clear();//lost focus after clear
            //element.Click();
            System.Windows.Forms.SendKeys.SendWait("^{v}");

        }

        public static bool IsCheckboxEFYChecked(this OpenQA.Selenium.IWebElement element)
        {
            if (element.Selected)
            {
                Notification.Toast.Show("đã check");
                return true;
            }
            Notification.Toast.Show("chưa check");
            return false;
        }


    }
}
