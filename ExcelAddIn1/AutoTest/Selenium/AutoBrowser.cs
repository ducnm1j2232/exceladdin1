﻿namespace ExcelAddIn1.AutoTest.Selenium
{
    public class AutoBrowser
    {
        private const double DEFAULT_TIMEOUT_SECONDS = 60;
        private static System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

        public OpenQA.Selenium.Chrome.ChromeDriver ChromeDriverCore
        {
            get;
            set;
        }

        public OpenQA.Selenium.Support.UI.WebDriverWait WaitCore
        {
            get;
            set;
        }

        public OpenQA.Selenium.Chrome.ChromeOptions Options
        {
            get;
            set;
        }



        public AutoBrowser(System.TimeSpan timeout, OpenQA.Selenium.Chrome.ChromeOptions chromeOptions = null)
        {
            ExcelAddIn1.AutoTest.Selenium.Core.SetupWebDriver();
            OpenQA.Selenium.Chrome.ChromeOptions chromeOptionsDefault = new OpenQA.Selenium.Chrome.ChromeOptions();
            if (chromeOptions != null)
            {
                chromeOptionsDefault = chromeOptions;
            }

            chromeOptionsDefault.AddArgument("--start-maximized");
            chromeOptionsDefault.AddArgument("disable-blink-features=AutomationControlled");//https://stackoverflow.com/questions/60649179/chromedriver-with-selenium-displays-a-blank-page
            chromeOptionsDefault.AddArgument("--incognito");

            OpenQA.Selenium.Chrome.ChromeDriver chromeDriver = new OpenQA.Selenium.Chrome.ChromeDriver(chromeOptionsDefault);
            OpenQA.Selenium.Support.UI.WebDriverWait wait = new OpenQA.Selenium.Support.UI.WebDriverWait(chromeDriver, timeout);
            ChromeDriverCore = chromeDriver;
            WaitCore = wait;

        }

        public void Exit()
        {
            ChromeDriverCore.Close();
            ChromeDriverCore.Quit();
        }

        public void Goto(string url)
        {
            while(true)
            {
                try
                {
                    ChromeDriverCore.Url = url;
                    break;
                }
                catch (OpenQA.Selenium.WebDriverException)
                {

                }
            }
        }

        public OpenQA.Selenium.IWebElement GetElementByXpath(string xPath)
        {
        Start:
            try
            {
                return WaitCore.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath(xPath)));
            }
            catch (OpenQA.Selenium.StaleElementReferenceException)
            {
                WaitDOMchange();
                goto Start;
            }
        }

        public OpenQA.Selenium.IWebElement GetWebElementWithTimeout(string xpath, double timeout)
        {
            OpenQA.Selenium.IWebElement result = null;
            bool found = false;
            System.TimeSpan time = System.TimeSpan.FromSeconds(timeout);
            stopwatch.Start();

            while (stopwatch.Elapsed <= time && found == false)
            {
                try
                {
                    result = ChromeDriverCore.FindElement(OpenQA.Selenium.By.XPath(xpath));
                    found = true;
                }
                catch { }
            }

            if (result == null)
            {
                throw new System.Exception("time out");
            }

            stopwatch.Stop();
            stopwatch.Reset();

            return result;
        }

        /// <summary>
        /// "input[class='tb8'][type='text'][name='signOnName']"
        /// </summary>
        /// <param name="cssSelector"></param>
        /// <param name="containsElementText"></param>
        /// <returns></returns>
        public OpenQA.Selenium.IWebElement GetElementByCSSselector(string cssSelector, string containsElementText = "")
        {
            while (true)
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.ThrowIfCancellationRequested();
                var collections = ChromeDriverCore.FindElements(OpenQA.Selenium.By.CssSelector(cssSelector));
                if (collections != null && collections.Count > 0)
                {
                    if (string.IsNullOrEmpty(containsElementText))
                    {
                        return collections[0];
                    }
                    else
                    {
                        foreach (var item in collections)
                        {
                            try
                            {
                                if (item.Text.Contains(containsElementText))
                                {
                                    return item;
                                }
                            }
                            catch (OpenQA.Selenium.StaleElementReferenceException)
                            {

                            }
                        }
                    }
                }
            }
        }

        public void WaitDOMchange()
        {
            while (true)
            {
                try
                {
                    if ((string)ChromeDriverCore.ExecuteScript("return document.readyState").ToString() == "complete"
                        && (bool)ChromeDriverCore.ExecuteScript("return jQuery.active == 0") == true)
                    {
                        break;
                    }
                }
                catch
                {
                    break;
                }
            }
        }

        public void MouseClick(OpenQA.Selenium.IWebElement element)
        {
            ((OpenQA.Selenium.IJavaScriptExecutor)ChromeDriverCore).ExecuteScript("arguments[0].scrollIntoView();", element);
            OpenQA.Selenium.IWebElement webElement = WaitCore.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
            WaitDOMchange();
            //System.Threading.Thread.Sleep(1000);
            webElement.Click();
            WaitDOMchange();
        }

        public void ForceClick(OpenQA.Selenium.IWebElement element)
        {
            ((OpenQA.Selenium.IJavaScriptExecutor)ChromeDriverCore).ExecuteScript("arguments[0].scrollIntoView();", element);
            OpenQA.Selenium.Interactions.Actions actions = new OpenQA.Selenium.Interactions.Actions(ChromeDriverCore);
            actions.MoveToElement(element).Build().Perform();
            actions.Click().Release().Build().Perform();

            //element.SendKeys(OpenQA.Selenium.Keys.Enter);
            //WaitDOMchange();
        }

        public void ScriptClick(OpenQA.Selenium.IWebElement element)
        {
            ((OpenQA.Selenium.IJavaScriptExecutor)ChromeDriverCore).ExecuteScript("arguments[0].click();", element);
            WaitDOMchange();
        }

        public void SetValue(OpenQA.Selenium.IWebElement element, string value)
        {
            ExcelAddIn1.MyClass.ClipboardSafeInvoke.SetText(value);

            MouseClick(element);

            OpenQA.Selenium.Interactions.Actions actions = new OpenQA.Selenium.Interactions.Actions(ChromeDriverCore);
            actions.SendKeys(OpenQA.Selenium.Keys.Home)
                .KeyDown(OpenQA.Selenium.Keys.Shift)
                .SendKeys(OpenQA.Selenium.Keys.End)
                .KeyUp(OpenQA.Selenium.Keys.Shift)
                .SendKeys(OpenQA.Selenium.Keys.Delete)
                .KeyDown(OpenQA.Selenium.Keys.Control)
                .SendKeys("v")
                .KeyUp(OpenQA.Selenium.Keys.Control)
                .Build()
                .Perform();
        }

        public void TypeValue(OpenQA.Selenium.IWebElement element, string value)
        {
            MouseClick(element);
            System.Windows.Forms.SendKeys.SendWait("^a");
            System.Windows.Forms.SendKeys.SendWait("{DEL}");
            System.Windows.Forms.SendKeys.SendWait(value);
            System.Windows.Forms.SendKeys.SendWait("{ENTER}");

        }

        public void SendKeys(OpenQA.Selenium.IWebElement element, string value)
        {
            element.SendKeys(value);
        }

        public void ScrollToEnd()
        {
            ((OpenQA.Selenium.IJavaScriptExecutor)ChromeDriverCore).ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
        }

        public string GetInnerHTML(OpenQA.Selenium.IWebElement element)
        {
            string result = string.Empty;
            result = element.GetAttribute("innerHTML");
            while (true)
            {
                if (string.IsNullOrEmpty(result))
                {
                    result = element.GetAttribute("innerHTML");
                }
                else
                {
                    break;
                }
            }

            return result;
        }





    }
}
