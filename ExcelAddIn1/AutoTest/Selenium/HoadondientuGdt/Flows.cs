﻿using OpenQA.Selenium;
using VSTOLib.Extensions;

namespace ExcelAddIn1.AutoTest.Selenium.HoadondientuGdt
{
    public static class Flows
    {
        //static AutoBrowser browser;
        static string url = "https://hoadondientu.gdt.gov.vn/";
        static string username = "0106188175";
        static string password = "jHCc1aA@";
        static AutoTest.Selenium.Browser.AutoBrowser2 browser;

        static int _dataRowCount = 0;

        public static System.Data.DataTable GetDataTable(System.DateTime startDate, System.DateTime endDate)
        {
            browser = new AutoTest.Selenium.Browser.AutoBrowser2(System.TimeSpan.FromSeconds(60));
            browser.Goto(url);

            browser.Locator(By.CssSelector("div[class='ant-col home-header-menu-item']"), "Đăng nhập").Click();

            var popupForm = browser.Locator(By.CssSelector("div[class='ant-modal-content']"));
            popupForm.Locator(By.CssSelector("input[id='username']")).SetValue(username);
            popupForm.Locator(By.CssSelector("input[id='password']")).SetValue(password);

            //var captchaImage = popupForm.Locator(By.CssSelector("div[class='Captcha__ImageWrapper-sc-1up1k1e-0 gkpSzC']")).GetScreenShot();
            //Browser.CaptchaSolverForm captchaSolverForm = new Browser.CaptchaSolverForm(captchaImage);
            //captchaSolverForm.ShowDialog();

            string captcha = DevExpress.XtraEditors.XtraInputBox.Show("Nhập mã captcha", "", "");
            //string captcha = captchaSolverForm.ShowForm();
            popupForm.Locator(By.CssSelector("input[id='cvalue']")).SetValue(captcha);
            popupForm.Locator(By.CssSelector("button[type='submit']")).Click();
            browser.WaitDOMchange();

            browser.Locator(By.CssSelector("div[class='ant-row-flex flex-space']"));
            browser.Goto("https://hoadondientu.gdt.gov.vn/tra-cuu/tra-cuu-hoa-don");

            browser.Locator(By.TagName("span"), "Tra cứu hóa đơn điện tử mua vào").Click();

            System.Data.DataTable table = InitDataTable();
            AddRowsToDataTable(table, startDate, endDate);

            browser.Quit();
            Notification.Toast.Show("OK");
            return table;
        }

        public static System.Data.DataTable InitDataTable()
        {
            System.Data.DataTable dataTable = new System.Data.DataTable();
            string theadSource = browser.Locator(By.CssSelector("thead[class='ant-table-thead']")).OuterHTML;
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(theadSource);
            var headers = document.DocumentNode.SelectNodes("//th");
            foreach (var head in headers)
            {
                dataTable.Columns.Add(head.InnerText);
            }
            return dataTable;
        }

        public static void AddRowsToDataTable(System.Data.DataTable dataTable, System.DateTime startDate, System.DateTime endDate)
        {
            browser.Locator(By.CssSelector("span[id='tngay']")).Nth(1).Click(Browser.Enum.ClickType.Mouse);
            browser.Locator(By.CssSelector("input[class='ant-calendar-input '][placeholder='Chọn thời điểm']")).Clear();
            browser.Locator(By.CssSelector("input[class='ant-calendar-input '][placeholder='Chọn thời điểm']")).SetValue(startDate.ToString("dd/MM/yyyy"));
            browser.Locator(By.TagName("label"), "MST người bán").Click(Browser.Enum.ClickType.Mouse);//fake click to close date picker

            browser.Locator(By.CssSelector("span[id='dngay']")).Nth(1).Click(Browser.Enum.ClickType.Mouse);
            browser.Locator(By.CssSelector("input[class='ant-calendar-input '][placeholder='Chọn thời điểm']")).Clear();
            browser.Locator(By.CssSelector("input[class='ant-calendar-input '][placeholder='Chọn thời điểm']")).SetValue(endDate.ToString("dd/MM/yyyy"));
            browser.Locator(By.TagName("label"), "MST người bán").Click(Browser.Enum.ClickType.Mouse);//fake click to close date picker

            browser.Locator(By.CssSelector("button[type='submit']"))
                .Locator(By.TagName("span"), "Tìm kiếm")
                .Click();

            string ketqua = browser.Locator(By.CssSelector("div[class='ant-col ant-col-24']"))
                .Locator(By.TagName("span"), "Có ").Text;
            _dataRowCount = System.Convert.ToInt32(ketqua.GetNumberDigitOnly());
            Notification.Toast.Show(_dataRowCount.ToString());

            var firstCell = browser.Locator(By.CssSelector("tbody[class='ant-table-tbody']"))
                .Nth(1)
                .Locator(By.TagName("span"), "1");
            //while (true)
            //{
            //    if (firstCell.Text == "1")
            //    {
            //        break;
            //    }
            //}

            AddDataRow(dataTable);




        }

        private static void AddDataRow(System.Data.DataTable dataTable)
        {
            string oldFirstCellValue = string.Empty;
            string newFirstCellValue = string.Empty;

            while (dataTable.Rows.Count < _dataRowCount)
            {
                oldFirstCellValue = browser.Locator(By.XPath("/html/body/div[1]/section/section/main/div/div/div/div/div[3]/div[2]/div[3]/div[2]/div[2]/div/div/div/div/div/div[2]/table/tbody/tr[1]/td[1]")).Text;
                if (!string.IsNullOrEmpty(oldFirstCellValue)
                    && !string.IsNullOrEmpty(newFirstCellValue)
                    && oldFirstCellValue.Equals(newFirstCellValue))
                {
                    break;
                }

                GetDataFromCurrentPage(dataTable);

                if (dataTable.Rows.Count >= _dataRowCount)
                {
                    break;
                }

                //click next page
                browser.Locator(By.XPath("/html/body/div[1]/section/section/main/div/div/div/div/div[3]/div[2]/div[3]/div[2]/div[1]/div[2]/div/div[3]/button"))
                    .Click();

                while (true)
                {
                    if (oldFirstCellValue != browser.Locator(By.XPath("/html/body/div[1]/section/section/main/div/div/div/div/div[3]/div[2]/div[3]/div[2]/div[2]/div/div/div/div/div/div[2]/table/tbody/tr[1]/td[1]")).Text)
                    {
                        break;
                    }
                    System.Threading.Thread.Sleep(500);
                }
                //browser.WaitUntil(oldFirstCellValue != browser.Locator(By.XPath("/html/body/div[1]/section/section/main/div/div/div/div/div[3]/div[2]/div[3]/div[2]/div[2]/div/div/div/div/div/div[2]/table/tbody/tr[1]/td[1]")).Text);

            }

        }

        private static void GetDataFromCurrentPage(System.Data.DataTable dataTable)
        {
            string bodySource = browser.Locator(By.CssSelector("tbody[class='ant-table-tbody']")).Nth(1).OuterHTML;
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(bodySource);
            var rows = document.DocumentNode.SelectNodes("//tr");
            foreach (var row in rows)
            {
                var cells = row.SelectNodes("td");
                System.Collections.Generic.List<string> itemArray = new System.Collections.Generic.List<string>();
                foreach (var cell in cells)
                {
                    itemArray.Add(cell.InnerText);
                }
                System.Data.DataRow dataRow = dataTable.NewRow();
                dataRow.ItemArray = itemArray.ToArray();
                dataTable.Rows.Add(dataRow);
            }
        }


    }
}
