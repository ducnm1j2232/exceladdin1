﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.AutoTest.Selenium.HoadondientuGdt
{
    public static class ColumnName
    {
        public const string STT = "STT";
        public const string KyHieuMauSo = "Ký hiệumẫu số";
        public const string KyHieuHoaDon = "Ký hiệuhóa đơn";
        public const string SoHoaDon = "Số hóa đơn";
        public const string NgayLap = "Ngày lập";
        public const string ThongTinHoaDon = "Thông tin hóa đơn";
        public const string TongTienChuaThue = "Tổng tiềnchưa thuế";
        public const string TongTienThue = "Tổng tiền thuế";
        public const string TongTienChietKhauThuongMai = "Tổng tiềnchiết khấuthương mại";
        public const string TongTienPhi = "Tổng tiền phí";
        public const string TongTienThanhToan = "Tổng tiềnthanh toán";
        public const string DonViTienTe = "Đơn vịtiền tệ";
        public const string TrangThaiHoaDon = "Trạng tháihóa đơn";
        public const string KetQuaKiemTraHoaDon = "Kết quảkiểm tra hóa đơn";
        public const string HoaDonLienQuan = "Hóa đơn liên quan";


    }
}
