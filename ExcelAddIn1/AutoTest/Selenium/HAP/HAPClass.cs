﻿using System;
using System.Linq;


namespace ExcelAddIn1.AutoTest.Selenium.HAP
{
    public static class HAPClass
    {
        public static System.Data.DataTable GetDataTableFromHTML(string htmlHeader, string htmlRow)
        {
            System.Data.DataTable dataTable = new System.Data.DataTable();

            HtmlAgilityPack.HtmlDocument htmlDocumentHeader = new HtmlAgilityPack.HtmlDocument();
            htmlDocumentHeader.LoadHtml(htmlHeader);
            var header = htmlDocumentHeader.DocumentNode.SelectNodes("//tr/th");
            foreach (HtmlAgilityPack.HtmlNode node in header)
            {
                dataTable.Columns.Add(node.InnerText);
            }

            if (!string.IsNullOrEmpty(htmlRow))
            {
                HtmlAgilityPack.HtmlDocument htmlDocumentRows = new HtmlAgilityPack.HtmlDocument();
                htmlDocumentRows.LoadHtml(htmlRow);
                var rows = htmlDocumentRows.DocumentNode.SelectNodes("tr");
                foreach (var row in rows)
                {
                    dataTable.Rows.Add(row.SelectNodes("td").Select(td => td.InnerText).ToArray());
                }
            }

            return dataTable;
        }

        public static void AddRowsToDataTable(System.Data.DataTable dataTable, string htmlRow)
        {
            if(dataTable == null)
            {
                throw new ArgumentNullException("Data table null");
            }

            if (string.IsNullOrEmpty(htmlRow))
            {
                throw new ArgumentNullException("htmlRow null");
            }

            HtmlAgilityPack.HtmlDocument htmlDocumentRows = new HtmlAgilityPack.HtmlDocument();
            htmlDocumentRows.LoadHtml(htmlRow);
            var rows = htmlDocumentRows.DocumentNode.SelectNodes("tr");
            foreach (var row in rows)
            {
                dataTable.Rows.Add(row.SelectNodes("td").Select(td => td.InnerText).ToArray());
            }
        }


    }
}
