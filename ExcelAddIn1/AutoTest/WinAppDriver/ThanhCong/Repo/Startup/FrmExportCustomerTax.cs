﻿using ExcelAddIn1.AutoTest.WinAppDriver;

namespace ExcelAddIn1.WinAppDriver.ThanhCong.Repo
{
    public static class FrmExportCustomerTax
    {
        public static void OpenForm()
        {
            if (Core.IsCancel)
            {
                Core.TearDown();
                return;
            }

            ExcelAddIn1.WinAppDriver.ThanhCong.Repo.Startup.frmStartup.BringToFront();//vì vừa show wait form sẽ bị mất focus

            ExcelAddIn1.Notification.Wait.Show("click menu giao dịch");
            ExcelAddIn1.WinAppDriver.ThanhCong.Repo.Startup.menuGiaoDich.MoveAndClick();

            ExcelAddIn1.Notification.Wait.Show("click xuất hàng");
            ExcelAddIn1.WinAppDriver.ThanhCong.Repo.Startup.menuGiaoDichDropDown.MoveAndClick(58, 35);//xuất hàng


            ExcelAddIn1.Notification.Wait.Show("click xuất bán");
            ExcelAddIn1.WinAppDriver.ThanhCong.Repo.Startup.menuGiaoDichXuatHangDropDown.MoveAndClick(48, 102);//xuất bán thuế

            var form = _frmExportCustomerTax;
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement _frmExportCustomerTax
        {
            get
            {
                OpenQA.Selenium.Appium.Windows.WindowsElement result =  Repo.Startup.frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "FrmExportCustomerTax");
                var windowState = result.GetAttribute("Window.WindowVisualState");
                if(windowState == "Normal")
                {
                    result.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "Maximize-Restore").MoveAndClick();
                }
                return result;
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement btnNew => Repo.Startup.frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btnNew");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement cboImpExpMethod => Repo.Startup.frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "cboImpExpMethod");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement cboStockID => Repo.Startup.frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "cboStockID");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement cboCustomerID => Repo.Startup.frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "cboCustomerID");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btnSynInner => Repo.Startup.frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btnSynInner");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement FrmExportCustomerSyncInner => Repo.Startup.frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "FrmExportCustomerSyncInner");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btnConnect => FrmExportCustomerSyncInner.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btnConnect");

        public static OpenQA.Selenium.Appium.Windows.WindowsElement frmChangeConnectionTax => Repo.Startup.frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "frmChangeConnectionTax");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement cboAccConnect => frmChangeConnectionTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "cboAccConnect");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement cboDatabase => frmChangeConnectionTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "cboDatabase");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btnConnectServer => frmChangeConnectionTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btnConnect");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement msgbox => frmChangeConnectionTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "#32770");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement msgboxButtonOK => msgbox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "OK");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement dtpStartDate => FrmExportCustomerSyncInner.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dtpStartDate");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement dtpEndDate => FrmExportCustomerSyncInner.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dtpEndDate");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btnSearch => FrmExportCustomerSyncInner.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btnSearch");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement panel_List => FrmExportCustomerSyncInner.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "panel_List");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement Mark => panel_List.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Mark");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement Mark0 => panel_List.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Mark row 0");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement Markfilterrow => panel_List.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Mark filter row");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btnFinish => FrmExportCustomerSyncInner.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btnFinish");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btnCancel => _frmExportCustomerTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btnCancel", 2);
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txtTotalImpPrice => _frmExportCustomerTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txtTotalImpPrice");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement showMes => Repo.Startup.frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Dữ liệu đã bị thay đổi. Bạn có chắc chắn hủy?", 3);
        public static OpenQA.Selenium.Appium.Windows.WindowsElement showMesCó => showMes.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Có", 3);
        public static OpenQA.Selenium.Appium.Windows.WindowsElement footerPanel => frmChangeConnectionTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "panelControlDetail")
            .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Footer Panel")
            .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Mã hàng");

        public static OpenQA.Selenium.Appium.Windows.WindowsElement cboInvoiceStatus => _frmExportCustomerTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "cboInvoiceStatus");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement tabHoaDon => _frmExportCustomerTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "tbcCommon").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Hóa đơn");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement cboTaxSerialNumber => _frmExportCustomerTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "cboTaxSerialNumber");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txtTaxSign => _frmExportCustomerTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txtTaxSign");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txtTaxSerialNumber => _frmExportCustomerTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txtTaxSerialNumber");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement tabThongTinPhieu => _frmExportCustomerTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "tbcCommon").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Thông tin phiếu");


    }
}
