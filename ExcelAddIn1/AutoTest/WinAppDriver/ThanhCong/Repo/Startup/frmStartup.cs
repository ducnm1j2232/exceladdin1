﻿using ExcelAddIn1.AutoTest.WinAppDriver;

namespace ExcelAddIn1.WinAppDriver.ThanhCong.Repo
{
    public static class Startup
    {
        public static OpenQA.Selenium.Appium.Windows.WindowsElement frmStartup
        {
            get
            {
                string id = "Startup";
                return Sessions.MainSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }
        public static OpenQA.Selenium.Appium.Windows.WindowsElement frmDateLock
        {
            get
            {
                string id = "frmDateLock";
                return frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }


        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuHeThong
        {
            get
            {
                string name = "Hệ thống";
                return frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuHeThongDropDown
        {
            get
            {
                string name = "Hệ thốngDropDown";
                return frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }




        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuGiaoDich
        {
            get
            {
                string name = "Giao dịch";
                return frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }
        
        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuGiaoDichXuatHang => Win32.Menu.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Xuất hàng");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuGiaoDichXuatHangXuatBan => Win32.Menu.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Xuất bán");



        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuGiaoDichDropDown
        {
            get
            {
                string name = "Giao dịchDropDown";
                return Sessions.MainSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name, 5);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuGiaoDichNhapHangDropDown
        {
            get
            {
                string name = "Nhập hàngDropDown";
                return Sessions.MainSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuGiaoDichXuatHangDropDown
        {
            get
            {
                string name = "Xuất hàngDropDown";
                return Sessions.MainSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name,5);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement frmImportProviderTax
        {
            get
            {
                string id = "frmImportProviderTax";
                return Sessions.MainSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }





        public static OpenQA.Selenium.Appium.Windows.WindowsElement buttonNew
        {
            get
            {
                string id = "btnNew";
                return frmImportProviderTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement comboboxStockID
        {
            get
            {
                string id = "cboStockID";
                return frmImportProviderTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement comboboxCustomerID
        {
            get
            {
                string id = "cboCustomerID";
                return frmImportProviderTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement gridRow1
        {
            get
            {
                return frmImportProviderTax
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "tbcDetail")
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "tpgGood");
            }
        }


        public static OpenQA.Selenium.Appium.Windows.WindowsElement popupGood
        {
            get
            {
                return (OpenQA.Selenium.Appium.Windows.WindowsElement)frmImportProviderTax
                    .FindElementByClassName("WindowsForms10.Window.20008.app.0.1983833_r7_ad1");
            }
        }

       






    }
}
