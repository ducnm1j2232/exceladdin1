﻿using ExcelAddIn1.AutoTest.WinAppDriver;

namespace ExcelAddIn1.WinAppDriver.ThanhCong.Repo
{
    public static class frmImportProviderTax
    {
        public static void OpenForm()
        {
            Repo.Startup.frmStartup.BringToFront();//vì vừa show wait form sẽ bị mất focus

            ExcelAddIn1.Notification.Wait.Show("click menu giao dịch");
            Repo.Startup.menuGiaoDich.MoveAndClick();

            ExcelAddIn1.Notification.Wait.Show("click nhập hàng");
            Repo.Startup.menuGiaoDichDropDown.MoveAndClick(44, 9);//nhập hàng
            
            ExcelAddIn1.Notification.Wait.Show("click nhập mua hàng");
            Repo.Startup.menuGiaoDichNhapHangDropDown.MoveAndClick(86, 51);//nhập mua hàng thuế
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement _frmImportProviderTax => Repo.Startup.frmStartup.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "frmImportProviderTax");

        public static OpenQA.Selenium.Appium.Windows.WindowsElement btnNew => _frmImportProviderTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btnNew");

        public static OpenQA.Selenium.Appium.Windows.WindowsElement cboStockID => _frmImportProviderTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "cboStockID");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement cboCustomerID => _frmImportProviderTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "cboCustomerID");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btnCancel => _frmImportProviderTax.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btnCancel");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement Row1 => _frmImportProviderTax
            .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "tbcDetail")
            .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "tpgGood");

        public static OpenQA.Selenium.Appium.Windows.WindowsElement popupGood => _frmImportProviderTax
            .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "WindowsForms10.Window.20008.app.0.1983833_r7_ad1")
            .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Header Panel")
            .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Good ID");


    }
}
