﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelAddIn1.AutoTest.WinAppDriver;

namespace ExcelAddIn1.WinAppDriver.ThanhCong.Repo
{
    public static class Login
    {
        public static OpenQA.Selenium.Appium.Windows.WindowsElement frmLogin
        {
            get
            {
                string id = "Login";
                return ThanhCong.Sessions.LoginSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement textboxUserName
        {
            get
            {
                string id = "txtUserName";
                return frmLogin.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement textboxPassword
        {
            get
            {
                string id = "txtPassword";
                return frmLogin.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement comboboxZone
        {
            get
            {
                string id = "cboZone";
                return frmLogin.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement buttonLogin
        {
            get
            {
                string id = "btnLogin";
                return frmLogin.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }
    }
}
