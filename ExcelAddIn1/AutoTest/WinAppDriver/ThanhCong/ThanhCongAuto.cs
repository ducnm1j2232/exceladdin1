﻿using ExcelAddIn1.MyClass;
using Microsoft.Office.Interop.Excel;
using ExcelAddIn1.AutoTest.WinAppDriver;
using VSTOLib.Extensions;

namespace ExcelAddIn1.WinAppDriver.ThanhCong
{
    public static class ThanhCongAuto
    {
        const string THANHCONG_PROCESS_NAME = "HMSys";
        public const string WAIT_CAPTION = "click vào đây để dừng";


        public static void OpenApplication(string userName, string password, string zoneID)
        {
            if (Core.IsCancel)
            {
                Core.TearDown();
                return;
            }

            var loginWindowHandle = "empty";

            System.TimeSpan timeout = System.TimeSpan.FromSeconds(60);
            if (!Method.IsRunning(THANHCONG_PROCESS_NAME))
            {
                Notification.Wait.Show(AddinConfig.TargetTHANHCONG);
                System.Diagnostics.Process.Start(ExcelAddIn1.MyClass.AddinConfig.TargetTHANHCONG);
                Sessions.LoginSession = Method.GetSesstion(THANHCONG_PROCESS_NAME, timeout);
                loginWindowHandle = Sessions.LoginSession.CurrentWindowHandle;
                SignInThanhCong(userName, password, zoneID);

            }

            while (true)//wait for change session. when login form close and main form open
            {
                Sessions.MainSession = Method.GetSesstion(THANHCONG_PROCESS_NAME, timeout);
                string mainWindowHandle = Sessions.MainSession.CurrentWindowHandle;
                if (loginWindowHandle != mainWindowHandle)
                {
                    break;
                }
            }

            Notification.Wait.Close();
            Sessions.MainSession.BringToFront();

        }


        public static void SignInThanhCong(string userName, string password, string zoneID)
        {
            ExcelAddIn1.AutoTest.WinAppDriver.Core.ThrowIfCancellationRequested();

            Notification.Wait.Show("nhập tên đăng nhập");
            Repo.Login.textboxUserName.SetTextByWM(userName);

            Notification.Wait.Show("nhập password");
            Repo.Login.textboxPassword.SetTextByWM(password);

            Notification.Wait.Show($"chọn khu vực {zoneID}");
            Repo.Login.comboboxZone.MSComboboxSelectItem(zoneID);

            Notification.Wait.Show("click đăng nhập");
            Repo.Login.buttonLogin.MoveAndClick();

            Notification.Wait.Show("click đăng nhập xong");
            //await System.Threading.Tasks.Task.Delay(0);
        }

        public static void KhoaSoChuyenNgay(string denNgay)
        {
            Repo.Startup.menuHeThong.MoveAndClick();
            Repo.Startup.menuHeThongDropDown.MoveAndClick(48, 17);//khóa ngày giao dịch
        }

        public static void TestNhapHangThuCong(string stockId, string customerID, System.Collections.Generic.List<UserClass.Good> goodInfo)
        {
            if (Core.IsCancel)
            {
                ExcelAddIn1.Notification.Wait.Close();
                return;
            }

            ExcelAddIn1.Notification.Wait.Show("Mở form nhập hàng");
            Repo.frmImportProviderTax.OpenForm();

            ExcelAddIn1.Notification.Wait.Show("Thêm mới");
            Repo.frmImportProviderTax.btnNew.MoveAndClick();

            ExcelAddIn1.Notification.Wait.Show($"Chọn kho {stockId}");
            Repo.frmImportProviderTax.cboStockID.MSComboboxSelectItem(stockId);

            ExcelAddIn1.Notification.Wait.Show($"Chọn nhà cung cấp {customerID}");
            Repo.frmImportProviderTax.cboCustomerID.MSComboboxSelectItem(customerID);

            ExcelAddIn1.Notification.Wait.Show("Click dòng đầu tiên");
            Repo.frmImportProviderTax.Row1.MoveAndClick(30, 40);

            System.Threading.Thread.Sleep(7000);
            //Repo.frmImportProviderTax.popupGood.WaitForDisplay();

            //Repo.frmImportProviderTax._frmImportProviderTax.GetElement(WADExtension.FindBy.ID, "txtDescription").MoveAndClick();
            //Repo.frmImportProviderTax.btnCancel.MoveAndClick();

            var stopwatch = System.Diagnostics.Stopwatch.StartNew();
            int sleep = 300;
            foreach (var item in goodInfo)
            {
                if (Core.IsCancel)
                {
                    Core.TearDown();
                    return;
                }

                ExcelAddIn1.Notification.Wait.Show($"Nhập mã hàng {item}");
                Method.SendKeys(item.GoodID);
                Method.SendKeys("{TAB 5}", sleep);

                ExcelAddIn1.Notification.Wait.Show($"Nhập số lượng");
                Method.SendKeys(System.Convert.ToString(item.Quantity), sleep);
                Method.SendKeys("{TAB}", sleep);

                ExcelAddIn1.Notification.Wait.Show($"Nhập đơn giá");
                Method.SendKeys(System.Convert.ToString(item.Price), sleep);
                Method.SendKeys("{DOWN}", sleep);
                Method.SendKeys("{LEFT 6}", sleep);
            }

            stopwatch.Elapsed.ToString();
            //Repo.frmImportProviderTax.btnCancel.MoveAndClick();
        }


        public static void TestXuatBan(Worksheet ws, int maxTrans, string ImpExpMethod, string stockID, string customerID, string startDate, string endDate)
        {
            if (Core.IsCancel)
            {
                Core.TearDown();
                return;
            }

            string testTime = System.DateTime.Now.ToString();

            Notification.Wait.Show("Mở form xuất hàng");
            Repo.FrmExportCustomerTax.OpenForm();

            Notification.Wait.Show($"Thêm mới");
            Repo.FrmExportCustomerTax.btnNew.MoveAndClick();

            //return;

            Notification.Wait.Show($"Chọn phương thức nhập xuất {ImpExpMethod}");
            Repo.FrmExportCustomerTax.cboImpExpMethod.MSComboboxSelectItem(ImpExpMethod);

            Notification.Wait.Show($"Chọn kho {stockID}");
            Repo.FrmExportCustomerTax.cboStockID.MSComboboxSelectItem(stockID);

            Notification.Wait.Show($"Chọn mã khách hàng {customerID}");
            Repo.FrmExportCustomerTax.cboCustomerID.MSComboboxSelectItem(customerID);


            Notification.Wait.Show($"Chọn nhận kèm hóa đơn");
            Repo.FrmExportCustomerTax.cboInvoiceStatus.MSComboboxSelectItem("1");

            Notification.Wait.Show($"Sang tab hóa đơn");
            Repo.FrmExportCustomerTax.tabHoaDon.MoveAndClick();

            Notification.Wait.Show($"Chọn mẫu số hóa đơn");
            Repo.FrmExportCustomerTax.cboTaxSerialNumber.MSComboboxSelectItem("01GTKT");

            Notification.Wait.Show($"Nhập ký hiệu hóa đơn");
            Repo.FrmExportCustomerTax.txtTaxSign.SetTextByWM("DP/21E"); 
            
            Notification.Wait.Show($"Nhập số hóa đơn");
            Repo.FrmExportCustomerTax.txtTaxSerialNumber.SetTextByWM("0001234");
            
            Notification.Wait.Show($"Về tab Thông tin phiếu");
            Repo.FrmExportCustomerTax.tabThongTinPhieu.MoveAndClick();



            Notification.Wait.Show($"Click đồng bộ");
            Repo.FrmExportCustomerTax.btnSynInner.MoveAndClick();

            Notification.Wait.Show($"Click kết nối dữ liệu");
            Repo.FrmExportCustomerTax.btnConnect.MoveAndClick();

            Notification.Wait.Show($"Chọn tài khoản kết nối");
            string dataBase = "Lê Trọng Tấn";
            Repo.FrmExportCustomerTax.cboAccConnect.MSComboboxSelectItem(dataBase);
            Repo.FrmExportCustomerTax.cboAccConnect.MSComboboxSelectItem(dataBase);

            Notification.Wait.Show($"Chọn tài khoản kết nối");
            Repo.FrmExportCustomerTax.cboDatabase.MSComboboxSelectItem("ACCOUNTING_LTT");

            Notification.Wait.Show($"Click kết nối trên form kết nối");
            Repo.FrmExportCustomerTax.btnConnectServer.MoveAndClick();

            Notification.Wait.Show($"Chờ thông báo kết nối thành công");
            Repo.FrmExportCustomerTax.msgboxButtonOK.MoveAndClick();

            Notification.Wait.Show($"Nhập từ ngày");
            Repo.FrmExportCustomerTax.dtpStartDate.DTPSetDate(startDate);

            Notification.Wait.Show($"Nhập đến ngày");
            Repo.FrmExportCustomerTax.dtpEndDate.DTPSetDate(endDate);

            Notification.Wait.Show($"Click tìm kiếm");
            Repo.FrmExportCustomerTax.btnSearch.MoveAndClick();

            Notification.Wait.Show($"Chọn mã giao dịch");
            //System.Threading.Thread.Sleep(3000);

            Repo.FrmExportCustomerTax.Mark0.MoveAndClick();//select all
            //if (maxTrans > 1)
            //{
            //    for (int i = 1; i <= maxTrans - 1; i++)
            //    {
            //        System.Windows.Forms.SendKeys.SendWait("{DOWN}");
            //        System.Threading.Thread.Sleep(100);
            //        System.Windows.Forms.SendKeys.SendWait(" ");
            //        System.Threading.Thread.Sleep(100);
            //    }

            //}

            Notification.Wait.Show($"Chọn đồng bộ");
            Repo.FrmExportCustomerTax.btnFinish.MoveAndClick();

            Notification.Wait.Show($"Đang tính toán dữ liệu");

            var stopwatch = System.Diagnostics.Stopwatch.StartNew();
            Repo.FrmExportCustomerTax.btnCancel.WaitForDisplay("Hủy", WAIT_CAPTION);
            string timeElapsed = stopwatch.Elapsed.ToString(@"hh\:mm\:ss");

            //Repo.FrmExportCustomerTax.btnCancel.MoveAndClick();
            //try
            //{
            //    Notification.Wait.Show($"Chờ thông báo hủy (nếu có)", ThanhCongAuto.WAIT_CAPTION);
            //    if (Repo.FrmExportCustomerTax.showMesCó != null)
            //    {
            //        Notification.Wait.Show($"Click hủy", ThanhCongAuto.WAIT_CAPTION);
            //        Repo.FrmExportCustomerTax.showMesCó.MoveAndClick();
            //    }
            //}
            //catch { }

            //Repo.FrmExportCustomerTax.btnNew.WaitEnable();

            ws.Range["a1"].SetHeaderText("Thời gian test", "dữ liệu", "mã kho", "mã khách", "từ ngày", "đến ngày", "số lượng mã GD đã chọn", "thời gian tính toán đồng bộ");
            ws.NewRangeAtColumn("a").SetHeaderText(testTime, dataBase, $"'{stockID}", $"'{customerID}", startDate, endDate, maxTrans, timeElapsed);

            ws.Columns.AutoFit();

            //Notification.Wait.Close();
        }




    }
}
