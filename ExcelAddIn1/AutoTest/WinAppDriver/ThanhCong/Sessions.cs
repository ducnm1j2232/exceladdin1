﻿namespace ExcelAddIn1.WinAppDriver.ThanhCong
{
    public static class Sessions
    {
        const string THANHCONG_PROCESS_NAME = "HMSys";

        public static int oldSessionID { get; set; }

        public static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> LoginSession
        {
            get;
            set;
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> MainSession
        {
            get;
            set;
        }
    }
}
