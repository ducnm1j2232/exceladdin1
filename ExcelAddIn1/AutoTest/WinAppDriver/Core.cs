﻿using System;

namespace ExcelAddIn1.AutoTest.WinAppDriver
{
    public static class Core
    {
        public const string DEFAULT_WINAPPDRIVER_PORT = "http://127.0.0.1:4723";
        private static System.Diagnostics.Process winAppDriverProcess = null;

        public static bool IsCancel = false;

        public static System.Threading.CancellationTokenSource cancellationTokenSource;
        public static System.Threading.CancellationToken cancellationToken;
        
        static KeyboardHook1 hook = new KeyboardHook1();

        public static void Start()
        {

            //https://robindotnet.wordpress.com/2010/07/11/how-do-i-programmatically-find-the-deployed-files-for-a-vsto-add-in/
            //Get the assembly information
            System.Reflection.Assembly assemblyInfo = System.Reflection.Assembly.GetExecutingAssembly();

            //Location is where the assembly is run from 
            string assemblyLocation = assemblyInfo.Location;

            //CodeBase is the location of the ClickOnce deployment files
            Uri uriCodeBase = new Uri(assemblyInfo.CodeBase);
            string ClickOnceLocation = System.IO.Path.GetDirectoryName(uriCodeBase.LocalPath.ToString());

            string winappdriverEXE = System.IO.Path.Combine(ClickOnceLocation, @"AutoTest\WinAppDriver\WindowsApplicationDriver\WinAppDriver.exe");

            ExcelAddIn1.Notification.Wait.Show("Start winappdriver.exe");
            System.Diagnostics.ProcessStartInfo processStartInfo = new System.Diagnostics.ProcessStartInfo(winappdriverEXE);
            processStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;
            winAppDriverProcess = System.Diagnostics.Process.Start(processStartInfo);
            
            while (true)
            {
                if (System.Diagnostics.Process.GetProcessesByName("WinAppDriver").Length > 0)
                {
                    break;
                }
            }

            InitToken();

            Notification.Wait.Close();

        }

        private static void Hook_KeyPressed(object sender, KeyPressedEventArgs e)
        {
            cancellationTokenSource.Cancel();
        }

        public static void TearDown()
        {
            Notification.Wait.Close();
            DisposeToken();
            hook.Dispose();

            try
            {
                foreach (var process in System.Diagnostics.Process.GetProcessesByName("WinAppDriver"))
                {
                    process.Kill();
                }
            }
            catch { }
        }

        public static void InitToken()
        {
            ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationTokenSource = new System.Threading.CancellationTokenSource();
            ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken = ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationTokenSource.Token;
        }

        public static void DisposeToken()
        {
            cancellationTokenSource?.Dispose();
        }

        public static void ThrowIfCancellationRequested()
        {
            if (cancellationToken.IsCancellationRequested)
            {
                cancellationToken.ThrowIfCancellationRequested();
            }
        }


    }
}
