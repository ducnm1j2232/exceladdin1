﻿namespace ExcelAddIn1.AutoTest.WinAppDriver
{
    public static class Win32
    {
        const string dialogClassName = "#32770";

        static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> _desktopSession = null;
        //static OpenQA.Selenium.Appium.Windows.WindowsElement _openDialog = null;
        public static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> DesktopSession
        {
            get
            {
                if (_desktopSession == null)
                {
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
                    OpenQA.Selenium.Appium.AppiumOptions appiumOptions = new OpenQA.Selenium.Appium.AppiumOptions();
                    appiumOptions.AddAdditionalCapability("app", "Root");
                    _desktopSession = new OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>(new System.Uri(Core.DEFAULT_WINAPPDRIVER_PORT), appiumOptions);
                }
                return _desktopSession;

                //ExcelAddIn1.AutomationTest.WinAppDriver.WinAppDriverCore.Start();
                //OpenQA.Selenium.Appium.AppiumOptions appiumOptions = new OpenQA.Selenium.Appium.AppiumOptions();
                //appiumOptions.AddAdditionalCapability("app", "Root");
                //return new OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>(new System.Uri(WinAppDriverCore.DEFAULT_WINAPPDRIVER_PORT), appiumOptions);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement Dialog => DesktopSession.GetElement(Extension.FindBy.ClassName, dialogClassName);
        //{
        //    get
        //    {
        //        if (_openDialog == null)
        //        {
        //            _openDialog = DesktopSession.GetElement(WADExtension.FindBy.ClassName, dialogClassName);
        //        }
        //        return _openDialog;

        //    }
        //}
        public static OpenQA.Selenium.Appium.Windows.WindowsElement OpenDialog_EditField => Dialog.GetElement(Extension.FindBy.ClassName, "Edit");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement OpenDialog_OpenButton => Dialog.GetElement(Extension.FindBy.Name, "Open");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement MsgboxStatic => Dialog.GetElement(Extension.FindBy.ClassName, "Static");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement Menu => DesktopSession.GetElement(Extension.FindBy.ClassName, "#32768");

    }
}
