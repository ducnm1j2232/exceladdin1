﻿namespace ExcelAddIn1.AutoTest.WinAppDriver
{
    public static class Method
    {
        public static bool IsRunning(string processName)
        {
            System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName(processName);
            if (processes.Length == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> StartNewProcess(string targetExe)
        {
            OpenQA.Selenium.Appium.AppiumOptions appiumOptions = new OpenQA.Selenium.Appium.AppiumOptions();
            appiumOptions.AddAdditionalCapability("app", targetExe);
            return new OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>(new System.Uri(Core.DEFAULT_WINAPPDRIVER_PORT), appiumOptions);
        }


        public static System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        public static System.Diagnostics.Process GetProcess(string processName, System.TimeSpan timeout)
        {
            System.Diagnostics.Process result = null;

            bool found = false;
            //System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            while (stopwatch.Elapsed <= timeout && found == false)
            {
                try
                {
                    System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName(processName);
                    if (processes.Length > 0)
                    {
                        result = processes[0];
                        found = true;
                    }
                }
                catch { }
            }

            if (result == null)
            {
                throw new System.Exception("time out");
            }

            stopwatch.Stop();
            stopwatch.Reset();

            return result;

        }

        //public static async System.Threading.Tasks.Task<System.Diagnostics.Process> taskGetProcess(string processName)
        //{
        //    await System.Threading.Tasks.Task.Delay(1);
        //    System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName(processName);
        //    if (processes.Length > 0)
        //    {
        //        return processes[0];
        //    }
        //}

        public static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> GetSesstion(string processName, System.TimeSpan timeOut)
        {
            System.IntPtr appTopLevelWindowHandle = new System.IntPtr();
            System.Diagnostics.Process process = GetProcess(processName, System.TimeSpan.FromSeconds(10));
            //System.Diagnostics.Process process = System.Diagnostics.Process.GetProcessesByName(processName)[0];

            while (true)//https://stackoverflow.com/questions/16185217/c-sharp-process-mainwindowhandle-always-returns-intptr-zero
            {
                process.Refresh();
                if (process.MainWindowHandle != System.IntPtr.Zero)
                {
                    break;
                }
            }
            appTopLevelWindowHandle = process.MainWindowHandle;
            var appTopLevelWindowHandleHex = appTopLevelWindowHandle.ToString("x");


            OpenQA.Selenium.Appium.AppiumOptions appiumOptions2 = new OpenQA.Selenium.Appium.AppiumOptions();
            appiumOptions2.AddAdditionalCapability("appTopLevelWindow", appTopLevelWindowHandleHex);
            var result = new OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>(new System.Uri(Core.DEFAULT_WINAPPDRIVER_PORT), appiumOptions2, timeOut);
            return result;
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> GetSession(System.Diagnostics.Process process, System.TimeSpan timeOut)
        {
            System.IntPtr appTopLevelWindowHandle = new System.IntPtr();
            appTopLevelWindowHandle = process.MainWindowHandle;
            var appTopLevelWindowHandleHex = appTopLevelWindowHandle.ToString("x");

            OpenQA.Selenium.Appium.AppiumOptions appiumOptions2 = new OpenQA.Selenium.Appium.AppiumOptions();
            appiumOptions2.AddAdditionalCapability("appTopLevelWindow", appTopLevelWindowHandleHex);
            var result = new OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>(new System.Uri(Core.DEFAULT_WINAPPDRIVER_PORT), appiumOptions2, timeOut);
            return result;
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> GetSessionFromHandle(System.IntPtr intPtr)
        {
            OpenQA.Selenium.Appium.AppiumOptions appiumOptions2 = new OpenQA.Selenium.Appium.AppiumOptions();
            appiumOptions2.AddAdditionalCapability("appTopLevelWindow", intPtr.ToString("x"));
            var result = new OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>(new System.Uri(Core.DEFAULT_WINAPPDRIVER_PORT), appiumOptions2);
            return result;
        }


        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(System.IntPtr hWnd, int Msg, System.IntPtr wParam, System.IntPtr lParam);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(System.IntPtr hWnd, int uMsg, int wParam, string lParam);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
        public static extern bool PostMessage(System.IntPtr hWnd, int msg, System.IntPtr wParam, System.IntPtr lParam);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(System.IntPtr hWnd);

        public static System.IntPtr MakeLParamFromXY(int x, int y)
        {
            return (System.IntPtr)(y << 16 | x & 0xFFFF);
        }

        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, CallingConvention = System.Runtime.InteropServices.CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);
        //Mouse actions
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        public static void MouseClickByWM(uint X, uint Y)
        {
            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, X, Y, 0, 0);
        }


        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool SetCursorPos(int X, int Y);

        public static void SendKeys(string keys, int milisecondSleep = 0)
        {
            if (Core.IsCancel) return;
            System.Threading.Thread.Sleep(milisecondSleep);
            System.Windows.Forms.SendKeys.SendWait(keys);
        }

        public static void CloseWindowByWM(System.IntPtr hWnd)
        {
            System.UInt32 WM_CLOSE = 0x0010;
            ExcelAddIn1.AutoTest.WinAppDriver.User32Func.SendMessage(hWnd, WM_CLOSE, System.IntPtr.Zero, System.IntPtr.Zero);
        }

        public static System.IntPtr FindWindow(string className, string caption)
        {
            while (true)
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.ThrowIfCancellationRequested();
                if (User32Func.FindWindow(className, caption) != System.IntPtr.Zero)
                {
                    return User32Func.FindWindow(className, caption);
                }
            }
        }
    }
}
