﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Appium.Windows;

namespace ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UI
{
    public static class FRM_MAIN_ERP
    {
        public static WAD.Element _FRM_MAIN_ERP => Flows.driver.Locator(WAD.WADEnum.FindBy.ID, "FRM_MAIN_ERP");
        public static WAD.Element MenuNghiepVu => _FRM_MAIN_ERP
            .Locator(WAD.WADEnum.FindBy.ID, "uiPanel_Top")
            .Locator(WAD.WADEnum.FindBy.ID, "uiPanel_TopContainer")
            .Locator(WAD.WADEnum.FindBy.Name, "Nghiệp vụ");

        public static WAD.Element MenuNghiepVuDropDown => _FRM_MAIN_ERP
            .Locator(WAD.WADEnum.FindBy.Name, "Nghiệp vụDropDown");

        public static WAD.Element MuaHangDropDown => _FRM_MAIN_ERP
            .Locator(WAD.WADEnum.FindBy.Name, "Mua hàngDropDown");

        public static WAD.Element buttonCloseThongTinMayChuLayDanhMucVatTu => _FRM_MAIN_ERP
            .Locator(WAD.WADEnum.FindBy.Name, "Thông tin máy chủ lấy danh mục vật tư")
            .Locator(WAD.WADEnum.FindBy.ID, "TitleBar")
            .Locator(WAD.WADEnum.FindBy.Name, "Close");

    }
}
