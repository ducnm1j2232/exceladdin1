﻿namespace ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UI
{
    public static class FRM_NHAPHANG
    {
        public static WAD.Element _FRM_NHAPHANG => FRM_MAIN_ERP._FRM_MAIN_ERP.Locator(WAD.WADEnum.FindBy.ID, "FRM_NHAPHANG");

        public static WAD.Element btn_THEMMOI => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "btn_THEMMOI");
        public static WAD.Element txt_MAKHACHHANG => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "txt_MAKHACHHANG");
        public static WAD.Element txt_MAKHOXUAT => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "txt_MAKHOXUAT");
        public static WAD.Element btn_import_order_solomon => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "btn_import_order_solomon");
        public static WAD.Element dtp_FROMDATE => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "dtp_FROMDATE");
        public static WAD.Element dtp_TODATE => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "dtp_TODATE");
        public static WAD.Element btn_SEARCH => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "btn_SEARCH");
        public static WAD.Element uiPanel1 => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "uiPanel1");
        public static WAD.Element txt_SOCHUNGTUGOC => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "txt_SOCHUNGTUGOC");
        public static WAD.Element btn_SUA => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "btn_SUA");
        public static WAD.Element txt_MACTU => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "txt_MACTU");

        public static WAD.Element dtp_NGAYHACHTOAN => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "dtp_NGAYHACHTOAN");
        public static WAD.Element txt_kyhieu_HD => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "txt_kyhieu_HD");
        public static WAD.Element txt_GHICHU => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "txt_GHICHU");
        public static WAD.Element txt_TENKHACHHANG => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "txt_TENKHACHHANG");
        public static WAD.Element cbb_COLUMNFILL => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "cbb_COLUMNFILL");
        public static WAD.Element txt_MAVAT => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "txt_MAVAT");
        public static WAD.Element btn_LUULAI_Tam => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "btn_LUULAI_Tam");
        
        public static WAD.Element frm_RpViewer
        {
            //=> ExcelAddIn1.AutomationTest.WinAppDriver.Win32.DesktopSession.Locator(WADWAD.WADEnum.FindBy.ID, "frm_RpViewer");
            get
            {
                //dùng cái này nhanh hơn là qua desktop session
                System.IntPtr handle = WAD.Win32Functions.FindWindow("WindowsForms10.Window.8.app.0.2004eee", "frm_RpViewer");
                var driver = new WAD.Driver(handle);
                return driver.Locator(WAD.WADEnum.FindBy.ID, "frm_RpViewer");
            }
        }
        public static WAD.Element frm_RpViewer_print => frm_RpViewer.Locator(WAD.WADEnum.FindBy.Name, "Print");

        public static WAD.Element frm_RpViewer_export => frm_RpViewer.Locator(WAD.WADEnum.FindBy.Name, "Export");
        public static WAD.Element frm_RpViewer_exportDropDown => frm_RpViewer.Locator(WAD.WADEnum.FindBy.Name, "ExportDropDown");

        public static WAD.Element frm_RpViewer_ExportDialog => frm_RpViewer.Locator(WAD.WADEnum.FindBy.ID, "ExportDialog");
        public static WAD.Element frm_RpViewer_ExportDialog_Filename =>
            frm_RpViewer_ExportDialog
            .Locator(WAD.WADEnum.FindBy.ID, "FileNameControlHost")
            .Locator(WAD.WADEnum.FindBy.ClassName, "Edit", "Name", "File name:");

        public static WAD.Element frm_RpViewer_ExportDialog_ButtonSave =>
            frm_RpViewer_ExportDialog
            .Locator(WAD.WADEnum.FindBy.ClassName, "Button", "Name", "Save");

        public static WAD.Element frm_RpViewer_Button_Close => frm_RpViewer
            .Locator(WAD.WADEnum.FindBy.ID, "TitleBar")
            .Locator(WAD.WADEnum.FindBy.Name, "Close");

        public static WAD.Element printDialog_buttonPrint => frm_RpViewer.Locator(WAD.WADEnum.FindBy.Name, "Print", "AccessKey", "Alt+p");

        public static WAD.Element printDialog_numberOfCopies => frm_RpViewer.Locator(WAD.WADEnum.FindBy.Name, "Number of copies:", "ClassName", "Edit");

        public static WAD.Element SavePrintOutputAsDialog
        {
            get
            {
                var desktopSession = new WAD.Driver(WAD.Enums.SessionType.Desktop);
                return desktopSession
                    .Locator(WAD.WADEnum.FindBy.Name, "Printing")
                    .Locator(WAD.WADEnum.FindBy.ClassName, "#32770");
            }
        }
            
        public static WAD.Element dtp_NGAYCT_GOC => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "dtp_NGAYCT_GOC");

        public static WAD.Element uiTabPage_VATTU => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "uiTabPage_VATTU");

        public static WAD.Element txt_SUM_TIEN_HANG => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "txt_SUM_TIEN_HANG");

        public static WAD.Element txt_TIEN_VAT => _FRM_NHAPHANG.Locator(WAD.WADEnum.FindBy.ID, "txt_TIEN_VAT");







    }
}
