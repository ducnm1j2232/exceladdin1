﻿using ExcelAddIn1.WinAppDriver;

namespace ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement
{
    public static class FRM_MAIN_ERP
    {
        public static OpenQA.Selenium.Appium.Windows.WindowsElement THIS => ExcelAddIn1.WinAppDriver.Vietsea.VietseaAuto.mainVietseaSession.GetElement(Extension.FindBy.ID, "FRM_MAIN_ERP");

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuNghiepVu => THIS
                    .GetElement(Extension.FindBy.ID, "uiPanel_Top")
                    .GetElement(Extension.FindBy.ID, "uiPanel_TopContainer")
                    .GetElement(Extension.FindBy.Name, "Nghiệp vụ");

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuNghiepVuDropDown => THIS.GetElement(Extension.FindBy.Name, "Nghiệp vụDropDown");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement MuaHangDropDown => THIS.GetElement(Extension.FindBy.Name, "Mua hàngDropDown");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement buttonCloseThongTinMayChuLayDanhMucVatTu => THIS
            .GetElement(Extension.FindBy.Name, "Thông tin máy chủ lấy danh mục vật tư")
            .GetElement(Extension.FindBy.Name, "Close");



    }
}
