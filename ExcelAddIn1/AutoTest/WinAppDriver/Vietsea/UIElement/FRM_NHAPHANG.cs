﻿namespace ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement
{
    public static class FRM_NHAPHANG
    {
        public static void OpenForm()
        {
            FRM_MAIN_ERP.menuNghiepVu.MoveAndClick();
            //FRM_MAIN_ERP.menuNghiepVu.MenuClick("Nghiệp vụ");
            FRM_MAIN_ERP.menuNghiepVuDropDown.MoveAndClick(54, 8);
            FRM_MAIN_ERP.MuaHangDropDown.MoveAndClick(85, 8);
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement THIS => FRM_MAIN_ERP.THIS.GetElement(Extension.FindBy.ID, "FRM_NHAPHANG");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btn_THEMMOI => THIS.GetElement(Extension.FindBy.ID, "btn_THEMMOI");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txt_MAKHACHHANG => THIS.GetElement(Extension.FindBy.ID, "txt_MAKHACHHANG");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txt_MAKHOXUAT => THIS.GetElement(Extension.FindBy.ID, "txt_MAKHOXUAT");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btn_import_order_solomon => THIS.GetElement(Extension.FindBy.ID, "btn_import_order_solomon");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement dtp_FROMDATE => THIS.GetElement(Extension.FindBy.ID, "dtp_FROMDATE");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement dtp_TODATE => THIS.GetElement(Extension.FindBy.ID, "dtp_TODATE");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btn_SEARCH => THIS.GetElement(Extension.FindBy.ID, "btn_SEARCH");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement leftPane => THIS.GetElement(Extension.FindBy.ID, "67878", 2);
        public static OpenQA.Selenium.Appium.Windows.WindowsElement autoHidePane => THIS.GetElement(Extension.FindBy.ID, "AutoHideContainer", 2).GetElement(Extension.FindBy.Name, "Danh sách", 2);
        public static OpenQA.Selenium.Appium.Windows.WindowsElement uiPanel1 => THIS.GetElement(Extension.FindBy.ID, "uiPanel1");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txt_SOCHUNGTUGOC => THIS.GetElement(Extension.FindBy.ID, "txt_SOCHUNGTUGOC");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btn_SUA => THIS.GetElement(Extension.FindBy.ID, "btn_SUA");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txt_MACTU => THIS.GetElement(Extension.FindBy.ID, "txt_MACTU");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txt_kyhieu_HD => THIS.GetElement(Extension.FindBy.ID, "txt_kyhieu_HD");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txt_GHICHU => THIS.GetElement(Extension.FindBy.ID, "txt_GHICHU");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txt_TENKHACHHANG => THIS.GetElement(Extension.FindBy.ID, "txt_TENKHACHHANG");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement cbb_COLUMNFILL => THIS.GetElement(Extension.FindBy.ID, "cbb_COLUMNFILL");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement txt_MAVAT => THIS.GetElement(Extension.FindBy.ID, "txt_MAVAT");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement btn_LUULAI_Tam => THIS.GetElement(Extension.FindBy.ID, "btn_LUULAI_Tam");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement frm_RpViewer
        {
            //=> ExcelAddIn1.AutomationTest.WinAppDriver.Win32.DesktopSession.GetElement(WADExtension.FindBy.ID, "frm_RpViewer");
            get
            {
                //dùng cái này nhanh hơn là qua desktop session
                System.IntPtr handle = Method.FindWindow("WindowsForms10.Window.8.app.0.2004eee", "frm_RpViewer");
                var session = Method.GetSessionFromHandle(handle);
                return session.GetElement(Extension.FindBy.ID, "frm_RpViewer");
            }
        }
        public static OpenQA.Selenium.Appium.Windows.WindowsElement frm_RpViewer_print => frm_RpViewer.GetElement(Extension.FindBy.Name, "Print");
        public static OpenQA.Selenium.Appium.Windows.WindowsElement printDialog_buttonPrint
        {
            get
            {
                //while (true)
                //{
                //    var list = ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.Repo.FRM_NHAPHANG.frm_RpViewer.FindElementsByName("Print");
                //    if (list.Count > 0)
                //    {
                //        foreach (var item in list)
                //        {
                //            if (item.GetAttribute("AccessKey") == "Alt+p")
                //            {
                //                return item as OpenQA.Selenium.Appium.Windows.WindowsElement;
                //            }
                //        }
                //    }
                //}
                return ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.frm_RpViewer.GetElementByNameAndClassName("Print", "AccessKey", "Alt+p");
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement printDialog_numberOfCopies
        {
            get
            {
                //while (true)
                //{
                //    var list = ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.Repo.FRM_NHAPHANG.frm_RpViewer.FindElementsByName("Number of copies:");
                //    if (list.Count > 0)
                //    {
                //        foreach (var item in list)
                //        {
                //            if (item.GetAttribute("ClassName") == "Edit")
                //            {
                //                return item as OpenQA.Selenium.Appium.Windows.WindowsElement;
                //            }
                //        }
                //    }
                //}
                return ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.frm_RpViewer.GetElementByNameAndClassName("Number of copies:", "ClassName", "Edit");
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement dtp_NGAYCT_GOC => THIS.GetElement(Extension.FindBy.ID, "dtp_NGAYCT_GOC");

        public static OpenQA.Selenium.Appium.Windows.WindowsElement uiTabPage_VATTU => THIS.GetElement(Extension.FindBy.ID, "uiTabPage_VATTU");


    }
}
