﻿using ExcelAddIn1.AutoTest.WinAppDriver;

namespace ExcelAddIn1.WinAppDriver.Vietsea
{
    public static  partial class VietseaAuto
    {
        public static readonly string vietseaEXEPath = ExcelAddIn1.MyClass.AddinConfig.TargetVIETSEA;
        //public OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> _loginSession;

        public static OpenQA.Selenium.Appium.Windows.WindowsElement FRM_MAIN_ERP
        {
            get
            {
                string id = "FRM_MAIN_ERP";
                //return GetDesktopSession().GetElement(WADExtension.FindBy.ID, id);
                return mainVietseaSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement FRM_CTKETOAN
        {
            get
            {
                string id = "FRM_CTKETOAN";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement FRM_XUATHANG
        {
            get
            {
                string id = "FRM_XUATHANG";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }
        public static OpenQA.Selenium.Appium.Windows.WindowsElement FRM_CHUYENNGAY
        {
            get
            {
                string id = "FRM_CHUYENNGAY";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement comboboxKhoaSoNgayHachToan
        {
            get
            {
                string id = "dtp_KHOASO";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }


        public static OpenQA.Selenium.Appium.Windows.WindowsElement textboxUserName
        {
            get
            {
                string id = "txt_TEN_DANG_NHAP";
                return loginSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement textboxPassword
        {
            get
            {
                string id = "txt_MAT_KHAU_DANG_NHAP";
                return loginSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement comboboxDonVi
        {
            get
            {
                string name = "Open";
                return loginSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement comboboxItem_TH0302
        {
            get
            {
                string name = "TH0302 - Công ty Cổ Phần Đạt Phát Hà Nội";
                return loginSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement buttonSignIn
        {
            get
            {
                string id = "btn_DANG_NHAP";
                return loginSession.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuHeThong
        {
            get
            {
                string uiPanel_Top_id = "uiPanel_Top";
                string uiPanel_TopContainer_id = "uiPanel_TopContainer";
                string name = "Hệ thống";

                return FRM_MAIN_ERP
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, uiPanel_Top_id)
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, uiPanel_TopContainer_id)
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuHeThong_dropdown
        {
            get
            {
                string name = "Hệ thốngDropDown";//49,190
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuNghiepVu
        {
            get
            {
                string uiPanel_Top_id = "uiPanel_Top";
                string uiPanel_TopContainer_id = "uiPanel_TopContainer";
                string name = "Nghiệp vụ";

                return FRM_MAIN_ERP
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, uiPanel_Top_id)
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, uiPanel_TopContainer_id)
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuNghiepVu_dropdown
        {
            get
            {
                string name = "Nghiệp vụDropDown";//62,12
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuNghiepVu_menuItemMuaHang_dropdown
        {
            get
            {
                string name = "Mua hàngDropDown";//64,14
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuNghiepVu_menuItemBanHang_dropdown
        {
            get
            {
                string name = "Bán hàngDropDown";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }


        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuNghiepVu_menuItemSoCai_dropdown
        {
            get
            {
                string name = "Sổ cáiDropDown";//80,187
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement menuNghiepVu_menuTMTGNH_dropdown
        {
            get
            {
                string name = "Tiền mặt - tiền gửi ngân hàngDropDown";//47,35
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }




        public static OpenQA.Selenium.Appium.Windows.WindowsElement buttonThemMoi
        {
            get
            {
                string id = "btn_THEMMOI";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement textboxMaKhachHang
        {
            get
            {
                string id = "txt_MAKHACHHANG";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement textboxMaKhoXuat
        {
            get
            {
                string id = "txt_MAKHOXUAT";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement buttonImportSolomon
        {
            get
            {
                string id = "btn_import_order_solomon";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }


        public static OpenQA.Selenium.Appium.Windows.WindowsElement windowPopupThongTinMayChuLayDanhMucVatTu
        {
            get
            {
                string name = "Thông tin máy chủ lấy danh mục vật tư";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement windowPopupThongTinMayChuLayDanhMucVatTu_buttonClose
        {
            get
            {
                string id = "Close";
                return windowPopupThongTinMayChuLayDanhMucVatTu.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);

            }
        }



        public static OpenQA.Selenium.Appium.Windows.WindowsElement Win32dialogBox
        {
            get
            {
                string className = "#32770";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, className);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement DialogOpen_textboxFileName
        {
            get
            {
                return Win32dialogBox
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "ComboBox")
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit");
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement DialogOpen_buttonOpen
        {
            get
            {
                string DialogOpen_buttonOpen_classname = "Button";
                string name = "Open";
                return Win32dialogBox
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, DialogOpen_buttonOpen_classname)
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement Win32MessageBox_buttonOK
        {
            get
            {
                string name = "OK";
                return Win32dialogBox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, name);
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement dtP_FROMDATE
        {
            get
            {
                return FRM_MAIN_ERP
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dtp_FROMDATE")
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit");
                //.GetElement(WinAppDriverExtension.findBy.Name, "Tìm theo");
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement dtp_TODATE
        {
            get
            {
                return FRM_MAIN_ERP
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dtp_TODATE")
                    .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit");
            }
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement btn_SEARCH
        {
            get
            {
                string id = "btn_SEARCH";
                return FRM_MAIN_ERP.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, id);
            }
        }






    }
}
