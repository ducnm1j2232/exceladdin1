﻿using VSTOLib.Extensions;
using System.Threading.Tasks;
using DevExpress.Printing.Native.PrintEditor;

namespace ExcelAddIn1.AutoTest.WinAppDriver.Vietsea
{
    public static class Flows
    {
        public static WAD.Driver driver = null;

        private static string _fileNameExcelImport = string.Empty;
        public const string _dumbDate = "01/01/2030";

        public static void OpenVietsea()
        {
            driver = new WAD.Driver(Const.VietseaProcessName);
            ActiveVietseaWindow();
        }

        public static void ImportInputInvoices(string tranDate, string invoiceNumber, string invoiceDate, string maNCC, string vat, double invoiceAmount, double invoiceVat, string copies, string maKho, string fileImport, string printType)
        {
            OpenFRM_NHAPHANG();
            ImportFormExcel(maNCC, vat, maKho, fileImport);
            EditInfo(tranDate, invoiceNumber, maNCC, invoiceDate, vat);
            EditAmount(invoiceAmount, invoiceVat);
            Save();
            Print(printType, copies);

            Notification.Toast.Show("Nhập phiếu thành công");


        }

        public static async Task ImportInputInvoicesByAutoWorker(string fileExcelImport)
        {
            _fileNameExcelImport = System.IO.Path.GetFileNameWithoutExtension(fileExcelImport);

            string q = "SELECT * FROM [info$]";
            System.Data.DataTable dt = await ADO.GetDataFromExcelFileAsync(fileExcelImport, q).ConfigureAwait(false);

            string tranDate = dt.Rows[0][VIETSEA.ExcelFile.ImportNhapHang.NGAYHACHTOAN].ToString();
            string invoiceNumber = dt.Rows[0][VIETSEA.ExcelFile.ImportNhapHang.SOHOADON].ToString();
            string maNCC = dt.Rows[0][VIETSEA.ExcelFile.ImportNhapHang.MANCC].ToString();
            string vat = dt.Rows[0][VIETSEA.ExcelFile.ImportNhapHang.VAT].ToString();
            double invoiceAmount = System.Convert.ToDouble(dt.Rows[0][VIETSEA.ExcelFile.ImportNhapHang.THANHTIEN].ToString());
            double invoiceVat = System.Convert.ToDouble(dt.Rows[0][VIETSEA.ExcelFile.ImportNhapHang.TIENVAT].ToString());
            string copies = dt.Rows[0][VIETSEA.ExcelFile.ImportNhapHang.SOBANIN].ToString();
            string maKho = dt.Rows[0][VIETSEA.ExcelFile.ImportNhapHang.KHO].ToString();
            string invoiceDate = dt.Rows[0][VIETSEA.ExcelFile.ImportNhapHang.NGAYHOADON].ToString();
            string printType = dt.Rows[0][VIETSEA.ExcelFile.ImportNhapHang.PRINTTYPE].ToString();

            //string tranDate = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(fileExcelImport, VIETSEA.ExcelFile.ImportNhapHang.NGAYHACHTOAN, "a1");
            //string invoiceNumber = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(fileExcelImport, VIETSEA.ExcelFile.ImportNhapHang.SOHOADON, "a1");
            //string maNCC = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(fileExcelImport, VIETSEA.ExcelFile.ImportNhapHang.MANCC, "a1");
            //string vat = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(fileExcelImport, VIETSEA.ExcelFile.ImportNhapHang.VAT, "a1");
            //int invoiceAmount = System.Convert.ToInt32(ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(fileExcelImport, VIETSEA.ExcelFile.ImportNhapHang.THANHTIEN, "a1"));
            //int invoiceVat = System.Convert.ToInt32(ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(fileExcelImport, VIETSEA.ExcelFile.ImportNhapHang.TIENVAT, "a1"));
            //string copies = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(fileExcelImport, VIETSEA.ExcelFile.ImportNhapHang.SOBANIN, "a1");
            //string maKho = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(fileExcelImport, VIETSEA.ExcelFile.ImportNhapHang.KHO, "a1");
            //string invoiceDate = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(fileExcelImport, VIETSEA.ExcelFile.ImportNhapHang.NGAYHOADON, "a1");
            //string printType = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(fileExcelImport, VIETSEA.ExcelFile.ImportNhapHang.PRINTTYPE, "a1");

            ImportInputInvoices(tranDate, invoiceNumber, invoiceDate, maNCC, vat, invoiceAmount, invoiceVat, copies, maKho, fileExcelImport, printType);

        }

        private static void ActiveVietseaWindow()
        {
            driver.BringToFront();
        }

        private static void OpenFRM_NHAPHANG()
        {
            ActiveVietseaWindow();
            UI.FRM_MAIN_ERP.MenuNghiepVu.Click();
            UI.FRM_MAIN_ERP.MenuNghiepVuDropDown.Click(73, 10);
            UI.FRM_MAIN_ERP.MuaHangDropDown.Click(85, 8);
        }

        private static void ImportFormExcel(string maNCC, string vat, string maKho, string fileImport)
        {
            UI.FRM_NHAPHANG.btn_THEMMOI.Click();
            UI.FRM_NHAPHANG.btn_THEMMOI.WaitFor(WAD.WADEnum.State.Disable);

            UI.FRM_NHAPHANG.txt_MAKHACHHANG.SetText(maNCC);

            UI.FRM_NHAPHANG.txt_MAKHOXUAT.SetText(maKho);

            UI.FRM_NHAPHANG.btn_import_order_solomon.Click();

            UI.FRM_MAIN_ERP.buttonCloseThongTinMayChuLayDanhMucVatTu.Click();

            UI.FRM_MAIN_ERP._FRM_MAIN_ERP
                .Locator(WAD.WADEnum.FindBy.ClassName, "#32770")
                .Locator(WAD.WADEnum.FindBy.ClassName, "Edit")
                .SetText(fileImport);

            UI.FRM_MAIN_ERP._FRM_MAIN_ERP
                .Locator(WAD.WADEnum.FindBy.ClassName, "#32770")
                .Locator(WAD.WADEnum.FindBy.ClassName, "Button")
                .Locator(WAD.WADEnum.FindBy.Name, "Open")
                .Click();


            UI.FRM_MAIN_ERP._FRM_MAIN_ERP.Locator(WAD.WADEnum.FindBy.ClassName, "#32770", "", "", 0)
                .Locator(WAD.WADEnum.FindBy.ClassName, "Button")
                .Locator(WAD.WADEnum.FindBy.Name, "OK")
                .Click();
        }

        private static void EditInfo(string tranDate, string invoiceNumber, string maNCC, string invoiceDate, string vat)
        {
            UI.FRM_NHAPHANG.cbb_COLUMNFILL.SetTextComboboxVietsea("1");
            //UI.FRM_NHAPHANG.dtp_FROMDATE.SetTextDateTimePickerVietsea(tranDate.Replace(@"/", ""));
            //UI.FRM_NHAPHANG.dtp_TODATE.SetTextDateTimePickerVietsea(tranDate.Replace(@"/", ""));
            UI.FRM_NHAPHANG.dtp_FROMDATE.SetTextDateTimePickerVietsea(_dumbDate.Replace(@"/", ""));
            UI.FRM_NHAPHANG.dtp_TODATE.SetTextDateTimePickerVietsea(_dumbDate.Replace(@"/", ""));
            UI.FRM_NHAPHANG.btn_SEARCH.Click();

            if (UI.FRM_NHAPHANG.txt_SOCHUNGTUGOC.Text != invoiceNumber)
            {
                //System.Windows.Forms.MessageBox.Show("Không đúng số hóa đơn");
                throw new System.Exception("Không đúng số hóa đơn");
            }

            UI.FRM_NHAPHANG.btn_SUA.Click();
            string oldSOCT = UI.FRM_NHAPHANG.txt_MACTU.Text;
            string s1 = oldSOCT.Left(7);
            string s2 = tranDate.Left(2) + "/" + tranDate.Mid(4, 2) + "/" + tranDate.Right(2);
            string s3 = oldSOCT.Mid(oldSOCT.LastIndexOf('-') + 1, oldSOCT.Length - oldSOCT.LastIndexOf('-'));
            string newSOCT = s1 + s2 + s3;
            UI.FRM_NHAPHANG.txt_MACTU.SetText(newSOCT, WAD.WADEnum.SetTextType.WindowMessage);

            UI.FRM_NHAPHANG.dtp_NGAYHACHTOAN.SetTextDateTimePickerVietsea(tranDate.Replace(@"/", ""));

            UI.FRM_NHAPHANG.txt_MAKHACHHANG.SetText(maNCC);
            UI.FRM_NHAPHANG.dtp_NGAYCT_GOC.SetTextDateTimePickerVietsea(invoiceDate);

            string tenNCC = UI.FRM_NHAPHANG.txt_TENKHACHHANG.Text;
            string sohoadon = UI.FRM_NHAPHANG.txt_SOCHUNGTUGOC.Text;
            string diengiai = $"Nhập hàng {tenNCC} hóa đơn {sohoadon} ({invoiceDate})";
            UI.FRM_NHAPHANG.txt_GHICHU.SetText(diengiai);

            UI.FRM_NHAPHANG.txt_MAVAT.SetText(vat);
            WAD.Actions.Keyboard.Tab();
        }

        private static void EditAmount(double invoiceAmount, double invoiceVat)
        {
            void EditNumber(double soChenhLech, int tabStep)
            {
                UI.FRM_NHAPHANG.uiTabPage_VATTU.Click(1, 1);
                WAD.Actions.Keyboard.Tab();
                WAD.Actions.Keyboard.SendKeys("{HOME}");
                WAD.Actions.Keyboard.SendKeys("{UP 20}");
                WAD.Actions.Keyboard.SendKeys("{RIGHT}", tabStep);
                WAD.Actions.Keyboard.SendKeys("{F2}");
                WAD.Actions.Keyboard.CtrlC();

                string clipboardText = VSTOLib.Clipboard.ClipboardSafeInvoke.GetText();
                string newClipboardText = clipboardText.Replace(",", "");
                double tienHang = System.Convert.ToDouble(newClipboardText);
                double newTienHang = tienHang + soChenhLech;
                WAD.Actions.Keyboard.SendKeys("{DELETE}");
                WAD.Actions.Keyboard.SendKeys(newTienHang.ToString());
                WAD.Actions.Keyboard.Tab();
            }

            string stringSumtienHang = UI.FRM_NHAPHANG.txt_SUM_TIEN_HANG.Text.Replace(",", "");
            int sumTienHang = System.Convert.ToInt32(stringSumtienHang);
            if (sumTienHang != invoiceAmount)
            {
                double chenhLech = invoiceAmount - sumTienHang;
                EditNumber(chenhLech, 5);
            }

            string stringSumTienVat = UI.FRM_NHAPHANG.txt_TIEN_VAT.Text.Replace(",", "");
            int sumTienVat = System.Convert.ToInt32(stringSumTienVat);
            if (sumTienVat != invoiceVat)
            {
                double chenhLech = invoiceVat - sumTienVat;
                EditNumber(chenhLech, 9);
            }
        }

        private static void Save()
        {
            UI.FRM_NHAPHANG.btn_LUULAI_Tam.Click();
            UI.FRM_NHAPHANG.btn_LUULAI_Tam.WaitFor(WAD.WADEnum.State.Disable);
        }

        private static void Print(string printType, string copies)
        {
            UI.FRM_MAIN_ERP._FRM_MAIN_ERP.Locator(WAD.WADEnum.FindBy.ClassName, "#32770")
                .Locator(WAD.WADEnum.FindBy.Name, "Bạn muốn in phiếu nhập kho?")
                .WaitFor(WAD.WADEnum.State.Display);

            if (printType == "None")
            {
                UI.FRM_MAIN_ERP._FRM_MAIN_ERP.Locator(WAD.WADEnum.FindBy.ClassName, "#32770")
               .Locator(WAD.WADEnum.FindBy.Name, "In phiếu")
               .Locator(WAD.WADEnum.FindBy.Name, "No")
               .Click();
                return;
            }

            UI.FRM_MAIN_ERP._FRM_MAIN_ERP.Locator(WAD.WADEnum.FindBy.ClassName, "#32770")
                .Locator(WAD.WADEnum.FindBy.ClassName, "Button")
                .Locator(WAD.WADEnum.FindBy.Name, "Yes")
                .Click();


            UI.FRM_NHAPHANG.frm_RpViewer.Active();
            UI.FRM_NHAPHANG.frm_RpViewer_print.WaitFor(WAD.WADEnum.State.Display);

            UI.FRM_NHAPHANG.frm_RpViewer_print.Click();
            UI.FRM_NHAPHANG.printDialog_numberOfCopies.SetText(copies);
            UI.FRM_NHAPHANG.printDialog_buttonPrint.Click();

            #region MyRegion
            //if (printType == Public.PrintType.Print)
            //{
            //}

            //System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
            //string currentPrinter = printerSettings.PrinterName;
            //VSTOLib.User32dll.NativeMethods.SetDefaultPrinter("Microsoft Print to PDF");

            //if (printType == Public.PrintType.PDF)
            //{

            //    string path = System.IO.Path.Combine(ExcelAddIn1.Config.Manager.GetValue(Config.ID.ImportPDFSaveFolder), _fileNameExcelImport + ".pdf");
            //    if (System.IO.File.Exists(path))
            //    {
            //        System.IO.File.Delete(path);
            //    }

            //    var savedialog = UI.FRM_NHAPHANG.SavePrintOutputAsDialog;
            //    savedialog.Locator(WAD.WADEnum.FindBy.ClassName, "DUIViewWndClassName")
            //        .Locator(WAD.WADEnum.FindBy.ID, "FileNameControlHost", "Edit", "File name:")
            //        .SetText(path);

            //    savedialog.Locator(WAD.WADEnum.FindBy.ClassName, "Button", "Name", "Save").Click();


            //}

            //System.Threading.Thread.Sleep(5000);
            //VSTOLib.User32dll.NativeMethods.SetDefaultPrinter(currentPrinter); 
            #endregion


            UI.FRM_NHAPHANG.frm_RpViewer_Button_Close.Click();
        }

        public static void TestExportPDF(string printType, string copies)
        {
            _fileNameExcelImport = "gg";
            UI.FRM_NHAPHANG.frm_RpViewer.Active();
            UI.FRM_NHAPHANG.frm_RpViewer_print.WaitFor(WAD.WADEnum.State.Display);

            UI.FRM_NHAPHANG.frm_RpViewer_print.Click();
            UI.FRM_NHAPHANG.printDialog_numberOfCopies.SetText(copies);
            UI.FRM_NHAPHANG.printDialog_buttonPrint.Click();

            if (printType == Public.PrintType.Print)
            {

            }

            System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
            string currentPrinter = printerSettings.PrinterName;
            VSTOLib.User32dll.NativeMethods.SetDefaultPrinter("Microsoft Print to PDF");

            if (printType == Public.PrintType.PDF)
            {

                string path = System.IO.Path.Combine(ExcelAddIn1.Config.Manager.GetValue(Config.ID.ImportPDFSaveFolder), _fileNameExcelImport + ".pdf");
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }

                var savedialog = UI.FRM_NHAPHANG.SavePrintOutputAsDialog;
                savedialog.Locator(WAD.WADEnum.FindBy.ClassName, "DUIViewWndClassName")
                    .Locator(WAD.WADEnum.FindBy.ID, "FileNameControlHost", "Edit", "File name:")
                    .SetText(path);

                savedialog.Locator(WAD.WADEnum.FindBy.ClassName, "Button", "Name", "Save").Click();
            }

            System.Threading.Thread.Sleep(5000);
            VSTOLib.User32dll.NativeMethods.SetDefaultPrinter(currentPrinter);
            UI.FRM_NHAPHANG.frm_RpViewer.Close();
        }

    }
}
