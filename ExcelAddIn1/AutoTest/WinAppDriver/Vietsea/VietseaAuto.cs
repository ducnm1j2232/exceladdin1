﻿using ExcelAddIn1.AutoTest.WinAppDriver;
using ExcelAddIn1.MyClass;
using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using System.Data;
using VSTOLib.Extensions;

namespace ExcelAddIn1.WinAppDriver.Vietsea
{
    public static partial class VietseaAuto
    {
        private static string VIETSEAProcessName = "TBNET.ERP";
        public static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> mainVietseaSession
        {
            get; set;
        }

        private static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> loginSession
        {
            get; set;
        }

        private static System.Diagnostics.Process[] currentVietseaProcesses;

        public static void OpenVietsea(string user, string pass, bool newInstance = true)
        {
            void startNewProcess()
            {
                currentVietseaProcesses = System.Diagnostics.Process.GetProcessesByName(VIETSEAProcessName);
                OpenQA.Selenium.Appium.AppiumOptions appiumOptions = new OpenQA.Selenium.Appium.AppiumOptions();
                appiumOptions.AddAdditionalCapability("app", vietseaEXEPath);
                loginSession = new OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>(new System.Uri("http://127.0.0.1:4723"), appiumOptions);

                LoginVietsea(user, pass);
                System.Threading.Thread.Sleep(2000);
            }

            if (newInstance)
            {
                startNewProcess();
            }
            else
            {
                if (!isRunning(VIETSEAProcessName))
                {
                    startNewProcess();
                }
            }

            mainVietseaSession = GetMainVietseaSesstion(System.TimeSpan.FromSeconds(60));
            mainVietseaSession.BringToFront();
        }

        private static bool isRunning(string processName)
        {
            System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName(processName);
            if (processes.Length == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private static OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> GetMainVietseaSesstion(System.TimeSpan timeOut)
        {
            System.IntPtr appTopLevelWindowHandle = new System.IntPtr();
            if (currentVietseaProcesses == null || currentVietseaProcesses.Length == 0)
            {
                appTopLevelWindowHandle = System.Diagnostics.Process.GetProcessesByName("TBNET.ERP")[0].MainWindowHandle;
            }
            else
            {
                foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcessesByName("TBNET.ERP"))
                {
                    foreach (System.Diagnostics.Process eachInCurrentProcess in currentVietseaProcesses)
                    {
                        if (eachInCurrentProcess.Id != process.Id)
                        {
                            appTopLevelWindowHandle = process.MainWindowHandle;
                        }
                    }
                }
            }

            var vietseaWindowHandle = appTopLevelWindowHandle.ToString("x");

            //OpenQA.Selenium.Appium.AppiumOptions appiumOptions2 = new OpenQA.Selenium.Appium.AppiumOptions();
            OpenQA.Selenium.Appium.AppiumOptions appiumOptions2 = new OpenQA.Selenium.Appium.AppiumOptions();
            //appiumOptions2.AddAdditionalCapability("appTopLevelWindow", vietseaWindowHandle);
            appiumOptions2.AddAdditionalCapability("appTopLevelWindow", vietseaWindowHandle);
            var result = new OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>(new System.Uri("http://127.0.0.1:4723"), appiumOptions2, timeOut);
            //result.Manage().Timeouts().ImplicitWait = System.TimeSpan.FromSeconds(15);
            return result;
        }

        public static void LoginVietsea(string user, string pass)
        {
            textboxUserName.SetTextByClipboard(user);
            textboxPassword.SetTextByClipboard(pass);
            comboboxDonVi.Click();
            comboboxItem_TH0302.MoveAndClick(ExcelAddIn1.AutoTest.WinAppDriver.Extension.ClickType.DoubleClick);
            System.Threading.Thread.Sleep(300);
            buttonSignIn.Click();


        }

        public static void ActiveVIETSEAWindow()
        {
            mainVietseaSession.BringToFront();
            //FRM_MAIN_ERP.BringToFront();
            //FRM_MAIN_ERP.GetElement(Extension.FindBy.ID, "uiPanel_Top").MoveAndClick(1,1);
            //mainVietseaSession.GetElement(Extension.FindBy.ID, "uiPanel_Top").MoveAndClick(1,1);
        }

        public static void OpenForm_ChungTuNghiepVuKhac()
        {
            //FRM_MAIN_ERP.BringToFront();
            ActiveVIETSEAWindow();
            menuNghiepVu.MoveAndClick();
            menuNghiepVu_dropdown.MoveAndClick(57, 121);//item Sổ cái
            menuNghiepVu_menuItemSoCai_dropdown.MoveAndClick(80, 187);//item chứng từ nghiệp vụ khác
        }

        public static void OpenForm_FRM_XUATHANG()
        {
            //FRM_MAIN_ERP.BringToFront();
            ActiveVIETSEAWindow();
            menuNghiepVu.MoveAndClick();
            menuNghiepVu_dropdown.MoveAndClick(52, 36);//item bán hàng
            menuNghiepVu_menuItemBanHang_dropdown.MoveAndClick(60, 11);//phiếu xuất bán
        }

        public static void OpenForm_FRM_PhieuChi()
        {
            //FRM_MAIN_ERP.BringToFront();
            ActiveVIETSEAWindow();
            menuNghiepVu.MoveAndClick();
            menuNghiepVu_dropdown.MoveAndClick(72, 79);//tiền mặt tiền gửi ngân hàng
            menuNghiepVu_menuTMTGNH_dropdown.MoveAndClick(47, 35);//phiếu chi
        }

        public static void OpenForm_FRM_PhieuThu()
        {
            //FRM_MAIN_ERP.BringToFront();
            ActiveVIETSEAWindow();
            menuNghiepVu.MoveAndClick();
            menuNghiepVu_dropdown.MoveAndClick(72, 79);//tiền mặt tiền gửi ngân hàng
            menuNghiepVu_menuTMTGNH_dropdown.MoveAndClick(58, 12);//phiếu thu
        }



        public static void ChuyenNgayHachToan(string ngayThang)
        {
            int day = System.Convert.ToInt32(ngayThang.Left(2));
            int month = System.Convert.ToInt32(ngayThang.Mid(4, 2));
            int year = System.Convert.ToInt32(ngayThang.Right(4));
            System.DateTime ngayhachtoan = new System.DateTime(year, month, day);

            //if (ngayhachtoan.Date < ngayKhoaSoHachToan.Date || ngayKhoaSoHachToan == System.DateTime.MinValue)
            //{
            menuHeThong.MoveAndClick();
            menuHeThong_dropdown.MoveAndClick(49, 190);//item khóa sổ ngày
            comboboxKhoaSoNgayHachToan.SetTextDateTimePickerVIETSEA(ngayThang);
            FRM_CHUYENNGAY.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_KHOASO").MoveAndClick();

            Win32dialogBox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Khoá sổ thành công!");
            Win32MessageBox_buttonOK.MoveAndClick();
            ngayKhoaSoHachToan = ngayhachtoan;
            //}
        }

        public static void Auto_HachToanGDThe(string nganHang, string ngaythang, string taiKhoan, string phi, string tienQuet)
        {
            ChuyenNgayHachToan(ngaythang);
            OpenForm_ChungTuNghiepVuKhac();

            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").MoveAndClick();
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dpk_NGAY_HT").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit").SetTextDateTimePickerVIETSEA(ngaythang);
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_MANGUOIGD").SetTextByClipboard("KLE000001");
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_GHICHU").SetTextByClipboard($"Giao dịch thẻ {nganHang} ngày {ngaythang}");

            System.Windows.Forms.SendKeys.SendWait("{TAB 5}");
            System.Windows.Forms.SendKeys.SendWait("{LEFT}");
            System.Windows.Forms.SendKeys.SendWait("6427");
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait(taiKhoan);
            System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
            System.Windows.Forms.SendKeys.SendWait(phi);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait($"Phí Giao dịch thẻ {nganHang} ngày {ngaythang}");
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait("00000009");
            System.Windows.Forms.SendKeys.SendWait("{ENTER}");
            System.Windows.Forms.SendKeys.SendWait(taiKhoan);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait("1311");
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait("{LEFT}");
            System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
            System.Windows.Forms.SendKeys.SendWait(tienQuet);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");

            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_LUULAI_Tam").MoveAndClick();
            Win32dialogBox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Chứng từ được ghi sổ!");
            Win32MessageBox_buttonOK.MoveAndClick();
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").WaitEnable();
        }

        public static void Auto_HachToanKhachHangTraTien(string ngaythang, string taiKhoan, string maKH, string tenKH, string soTien)
        {
            ChuyenNgayHachToan(ngaythang);
            OpenForm_ChungTuNghiepVuKhac();

            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").MoveAndClick();
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dpk_NGAY_HT").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit").SetTextByClipboard(ngaythang);
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_MANGUOIGD").SetTextByClipboard(maKH);
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_GHICHU").SetTextByClipboard($"{tenKH} trả tiền ngày {ngaythang}");

            System.Windows.Forms.SendKeys.SendWait("{TAB 5}");
            System.Windows.Forms.SendKeys.SendWait("{LEFT}");
            System.Windows.Forms.SendKeys.SendWait(taiKhoan);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait("1311");
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait("{LEFT}");
            System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
            System.Windows.Forms.SendKeys.SendWait(soTien);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");

            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_LUULAI_Tam").MoveAndClick();
            Win32dialogBox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Chứng từ được ghi sổ!");
            Win32MessageBox_buttonOK.MoveAndClick();
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").WaitEnable();

        }

        public static void Auto_HachToanIBVCB(string ngaythang, string taiKhoan, string maKH, string tenKH, string sotien, string phi)
        {
            ChuyenNgayHachToan(ngaythang);
            OpenForm_ChungTuNghiepVuKhac();

            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").MoveAndClick();
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dpk_NGAY_HT").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit").SetTextByClipboard(ngaythang);
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_MANGUOIGD").SetTextByClipboard(maKH);
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_GHICHU").SetTextByClipboard($"Chuyển khoản thanh toán cho {tenKH} ngày {ngaythang}");

            System.Windows.Forms.SendKeys.SendWait("{TAB 5}");
            System.Windows.Forms.SendKeys.SendWait("{LEFT}");
            System.Windows.Forms.SendKeys.SendWait("3311");
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait(taiKhoan);
            System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
            System.Windows.Forms.SendKeys.SendWait(sotien);
            System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
            System.Windows.Forms.SendKeys.SendWait("{ENTER}");

            System.Windows.Forms.SendKeys.SendWait("6427");
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait(taiKhoan);
            System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
            System.Windows.Forms.SendKeys.SendWait(phi);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait($"Phí Chuyển khoản thanh toán cho {tenKH} ngày {ngaythang}");
            System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
            System.Windows.Forms.SendKeys.SendWait("00000002");

            System.Windows.Forms.SendKeys.SendWait("{ENTER}");

            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_LUULAI_Tam").MoveAndClick();
            Win32dialogBox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Chứng từ được ghi sổ!");
            Win32MessageBox_buttonOK.MoveAndClick();
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").WaitEnable();

        }

        public static void Auto_HachToanPhiGiaiNgan(string ngaythang, string taiKhoanNo, string taiKhoanCo, string soTien)
        {
            ChuyenNgayHachToan(ngaythang);
            OpenForm_ChungTuNghiepVuKhac();

            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").MoveAndClick();
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dpk_NGAY_HT").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit").SetTextByClipboard(ngaythang);
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_MANGUOIGD").SetTextByClipboard("VCB");
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_GHICHU").SetTextByClipboard($"Phí giải ngân {Accounting.Banking.Name.Vietcombank} ngày {ngaythang}");

            System.Windows.Forms.SendKeys.SendWait("{TAB 5}");
            System.Windows.Forms.SendKeys.SendWait("{LEFT}");
            System.Windows.Forms.SendKeys.SendWait(taiKhoanNo);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait(taiKhoanCo);
            System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
            System.Windows.Forms.SendKeys.SendWait(soTien);
            System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
            System.Windows.Forms.SendKeys.SendWait("00000002");

            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_LUULAI_Tam").MoveAndClick();
            Win32dialogBox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Chứng từ được ghi sổ!");
            Win32MessageBox_buttonOK.MoveAndClick();
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").WaitEnable();
        }

        public static void Auto_HachToanPhieuChi(string ngayThang, string maNCC, string tenNCC, string hoaDon, string soTien)
        {
            ChuyenNgayHachToan(ngayThang);
            OpenForm_FRM_PhieuChi();

            Notification.Wait.Show("Thêm mới");
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").MoveAndClick();

            Notification.Wait.Show("Chọn ngày hạch toán");
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dpk_NGAY_HT").MoveAndClick();
            Win32MessageBox_buttonOK.MoveAndClick();

            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dpk_NGAY_HT").SetTextDateTimePickerVIETSEA(ngayThang);

            Notification.Wait.Show("Nhập mã nhà cung cấp");
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_MANGUOIGD").SetTextByClipboard(maNCC);

            Notification.Wait.Show("Nhập diễn giải");
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_GHICHU").SetTextByClipboard($"Thanh toán tiền cho {tenNCC} HĐ {hoaDon} ngày {ngayThang}");

            System.Windows.Forms.SendKeys.SendWait("{TAB 5}");
            System.Windows.Forms.SendKeys.SendWait("3311");
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
            System.Windows.Forms.SendKeys.SendWait("1111");
            System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
            System.Windows.Forms.SendKeys.SendWait(soTien);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");

            //lưu
            Notification.Wait.Show("Lưu lại");
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_LUULAI_Tam").MoveAndClick();

            Notification.Wait.Show("chờ thông báo Chứng từ được ghi sổ");
            Win32dialogBox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Chứng từ được ghi sổ!");
            Win32MessageBox_buttonOK.MoveAndClick();
            FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").WaitEnable();
            Notification.Wait.Close();
        }

        public static void Auto_HachToanPhieuThu()
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetNewSheet("dữ liệu làm phiếu thu");

            List<List<UserClass.Invoice>> list_invoiceInfo_date = new List<List<UserClass.Invoice>>();

            List<string> list_date = new List<string>();
            list_date = ws.EntireRow(1).GetHeaderRangeByText("NGAYHACHTOAN").FirstRow().ResizeToLastRow("a").ValueToList(true);

            System.Data.DataTable dataTable = ws.UsedRange.MinimumRange().ToDataTable();

            Notification.Wait.Show("Mở VIETSEA");
            WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);

            string convertDate(string inputFullLongDate)
            {
                System.DateTime dateTime = System.DateTime.Parse(inputFullLongDate);
                string result = $"{dateTime.Day.ToString().PadLeft(2, '0')}/{dateTime.Month.ToString().PadLeft(2, '0')}/{dateTime.Year.ToString().PadLeft(4, '0')}";
                return result;
            }
            Notification.Wait.Show($"Chuyển ngày hạch toán về {convertDate(list_date[0])}");
            ChuyenNgayHachToan(convertDate(list_date[0]));//chọn ngày đầu tiên và là nhỏ nhất

            Notification.Wait.Show("Mở form phiếu thu");
            OpenForm_FRM_PhieuThu();

            foreach (string ngayThang in list_date)
            {

                #region main
                Notification.Wait.Show("Thêm mới");
                FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").MoveAndClick();

                Notification.Wait.Show("Nhập ngày tháng");

                FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "dpk_NGAY_HT").SetTextDateTimePickerVIETSEA(convertDate(ngayThang));

                Notification.Wait.Show("Nhập đối tượng giao dịch");
                FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_MANGUOIGD").SetTextByClipboard(Vietsea.VietseaAuto.KHACHLE);

                Notification.Wait.Show("Nhập diễn giải");
                FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_GHICHU").SetTextByClipboard($"Thu tiền bán hàng ngày {convertDate(ngayThang)}");

                Notification.Wait.Show("Tab xuống chi tiết hạch toán");
                System.Windows.Forms.SendKeys.SendWait("{TAB 7}");

                //điền số tiền
                System.Data.DataTable filteredDataTable = dataTable.Select($"NGAYHACHTOAN = '{ngayThang}'").CopyToDataTable();
                int totalLoop = filteredDataTable.Rows.Count;
                int sleep = 300;
                foreach (DataRow dataRow in filteredDataTable.Rows)
                {
                    string sotien = dataRow["Số tiền"].ToString();
                    string makhachhang = dataRow["MAKHACHANG"].ToString();

                    System.Windows.Forms.SendKeys.SendWait("{F2}");
                    System.Threading.Thread.Sleep(sleep);
                    Notification.Wait.Show($"Nhập số tiền {sotien}");
                    System.Windows.Forms.SendKeys.SendWait(sotien);
                    System.Threading.Thread.Sleep(sleep);

                    System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
                    System.Threading.Thread.Sleep(sleep);

                    System.Windows.Forms.SendKeys.SendWait("{F2}");
                    System.Threading.Thread.Sleep(sleep);
                    Notification.Wait.Show($"Nhập mã khách hàng {makhachhang}");
                    System.Windows.Forms.SendKeys.SendWait(makhachhang);
                    System.Threading.Thread.Sleep(sleep);

                    System.Windows.Forms.SendKeys.SendWait("{ENTER}");
                    System.Threading.Thread.Sleep(sleep);

                    System.Windows.Forms.SendKeys.SendWait("1311");
                    System.Threading.Thread.Sleep(sleep);
                    System.Windows.Forms.SendKeys.SendWait("{TAB 2}");
                    System.Threading.Thread.Sleep(sleep);
                }

                Notification.Wait.Show($"Xóa hàng cuối cùng");
                System.Windows.Forms.SendKeys.SendWait("{F7}");//xóa bớt dòng cuối để lưu
                System.Threading.Thread.Sleep(sleep);

                //return;

                Notification.Wait.Show("Lưu lại");
                FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_LUULAI_Tam").MoveAndClick();

                Notification.Wait.Show("Chờ thông báo chứng từ được ghi sổ!");
                Win32dialogBox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Chứng từ được ghi sổ!").WaitForDisplay();

                Notification.Wait.Show("OK");
                Win32MessageBox_buttonOK.MoveAndClick();

                FRM_CTKETOAN.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").WaitEnable();
                #endregion
            }

            Notification.Wait.Close();
        }

        public static void Auto_HachToanBillXuatChoKhach(System.Collections.Generic.List<ExcelAddIn1.UserClass.Good> list)
        {
            OpenForm_FRM_XUATHANG();

            ExcelAddIn1.AutoTest.WinAppDriver.Core.ThrowIfCancellationRequested();
            Notification.Wait.Show("thêm mới");
            FRM_XUATHANG.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").MoveAndClick();

            ExcelAddIn1.AutoTest.WinAppDriver.Core.ThrowIfCancellationRequested();
            Notification.Wait.Show("nhập mã kho xuất");
            FRM_XUATHANG.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_MAKHOXUAT").SetTextByClipboard(VIETSEA.TBNETERP.BAN10);
            System.Windows.Forms.SendKeys.SendWait("{TAB 3}");

            foreach (var item in list)
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.ThrowIfCancellationRequested();

                Notification.Wait.Show($"nhập mã hàng {item.GoodID}");
                System.Windows.Forms.SendKeys.SendWait(item.GoodID);
                System.Windows.Forms.Application.DoEvents();

                System.Windows.Forms.SendKeys.SendWait("{TAB 4}");
                System.Windows.Forms.Application.DoEvents();

                System.Windows.Forms.SendKeys.SendWait(item.Quantity.ToString());
                System.Windows.Forms.Application.DoEvents();
                System.Windows.Forms.SendKeys.SendWait("{TAB 3}");
                System.Windows.Forms.Application.DoEvents();

                System.Windows.Forms.SendKeys.SendWait(item.Price.ToString());
                System.Windows.Forms.Application.DoEvents();
                System.Windows.Forms.SendKeys.SendWait("{TAB}");
                System.Windows.Forms.Application.DoEvents();
                System.Windows.Forms.SendKeys.SendWait("{DOWN}");
                System.Windows.Forms.Application.DoEvents();

                System.Windows.Forms.SendKeys.SendWait("{LEFT 8}");
                System.Windows.Forms.Application.DoEvents();
            }
        }

        public static void Auto_HachToanBill2(List<ExcelAddIn1.UserClass.Good> list)
        {
            OpenForm_FRM_XUATHANG();
            Core.ThrowIfCancellationRequested();
            Notification.Wait.Show("thêm mới");
            FRM_XUATHANG.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "btn_THEMMOI").MoveAndClick();

            ExcelAddIn1.AutoTest.WinAppDriver.Core.ThrowIfCancellationRequested();
            Notification.Wait.Show("nhập mã kho xuất");
            FRM_XUATHANG.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_MAKHOXUAT").SetTextByClipboard(VIETSEA.TBNETERP.BAN10);
            WAD.Actions.Keyboard.SendKeys("{TAB}");
            //System.Windows.Forms.SendKeys.SendWait("{TAB}");

            //var txt_TENKHOXUAT = FRM_XUATHANG.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ID, "txt_TENKHOXUAT");
            WAD.Actions.Keyboard.SendKeys("{TAB}", 2);
            //System.Windows.Forms.SendKeys.SendWait("{TAB 2}");

            foreach (var item in list)
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.ThrowIfCancellationRequested();

                Notification.Wait.Show($"nhập mã hàng {item.GoodID}");
                WAD.Actions.Keyboard.SendKeys(item.GoodID);

                WAD.Actions.Keyboard.SendKeys("{TAB}", 4);

                WAD.Actions.Keyboard.SendKeys(item.Quantity.ToString());
                WAD.Actions.Keyboard.SendKeys("{TAB}", 3);

                WAD.Actions.Keyboard.SendKeys(item.Price.ToString());
                WAD.Actions.Keyboard.SendKeys("{TAB}", 4);

                WAD.Actions.Keyboard.SendKeys(item.Stock.ToString());
                WAD.Actions.Keyboard.SendKeys("{TAB}");

                WAD.Actions.Keyboard.SendKeys("{DOWN}");

                WAD.Actions.Keyboard.SendKeys("{LEFT}", 12, 10);
            }



        }

        public static void Auto_ImportPhieuNhapHang(string tranDate, string invoiceNumber, string invoiceDate, string maNCC, string vat, string numberOfCopies = "1")
        {
            ActiveVIETSEAWindow();

            Notification.Wait.Show("mở form nhập hàng");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.OpenForm();

            Notification.Wait.Show("thêm mới");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_THEMMOI.MoveAndClick();
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_THEMMOI.WaitDisable();

            Notification.Wait.Show("nhập nhà cung cấp");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MAKHACHHANG.SetTextByClipboard(maNCC);

            Notification.Wait.Show("nhập kho");
            string maKho = "BAN" + vat;
            if (vat == "10") maKho = "BAN10";
            if (vat == "8") maKho = "BAN8";
            if (vat == "5") maKho = "BAN05";
            if (vat == "0") maKho = "K-HANG0";

            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MAKHOXUAT.SetTextByClipboard(maKho);

            Notification.Wait.Show("click import");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_import_order_solomon.MoveAndClick();

            Notification.Wait.Show("đóng form thông tin máy chủ");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_MAIN_ERP.buttonCloseThongTinMayChuLayDanhMucVatTu.MoveAndClick();

            // mẹ nó static nên nó execute trước, ko ra kết quả
            //dialog Open
            Notification.Wait.Show("open dialog");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_MAIN_ERP.THIS
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "#32770")
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit")
                .SetTextByClipboard(@"C:\ExcelVSTOAddin\FileImportNhapHang.xls");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_MAIN_ERP.THIS
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "#32770")
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Button")
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Open")
                .MoveAndClick();

            //message box
            Notification.Wait.Show("chờ thông báo import thành công");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_MAIN_ERP.THIS
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "#32770")
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Button")
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "OK").MoveAndClick();


            Notification.Wait.Show("chọn lưu tạm");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.cbb_COLUMNFILL.VSComboboxSetText("1");

            Notification.Wait.Show("nhập từ ngày");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.dtp_FROMDATE.SetTextDateTimePickerVIETSEA(tranDate.Replace(@"/", ""));

            Notification.Wait.Show("nhập đến ngày");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.dtp_TODATE.SetTextDateTimePickerVIETSEA(tranDate.Replace(@"/", ""));

            Notification.Wait.Show("click tìm kiếm");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_SEARCH.MoveAndClick();

            Notification.Wait.Show($"tìm đúng số hóa đơn {invoiceNumber}");
            if (ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_SOCHUNGTUGOC.Text != invoiceNumber)
            {
                MsgBox.Show("không đúng số hóa đơn");
                ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationTokenSource?.Cancel();
            }

            Notification.Wait.Show("click sửa");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_SUA.MoveAndClick();

            Notification.Wait.Show("sửa số chứng từ");
            string oldSOCT = ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MACTU.Text;
            string s1 = oldSOCT.Left(7);
            string s2 = tranDate.Left(2) + "/" + tranDate.Mid(4, 2) + "/" + tranDate.Right(2);
            string s3 = oldSOCT.Mid(oldSOCT.LastIndexOf('-') + 1, oldSOCT.Length - oldSOCT.LastIndexOf('-'));
            string newSOCT = s1 + s2 + s3;
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MACTU.SetTextByWM(newSOCT);

            Notification.Wait.Show("nhập nhà cung cấp");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MAKHACHHANG.SetTextByClipboard(maNCC);

            Notification.Wait.Show("Nhập ngày hóa đơn");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.dtp_NGAYCT_GOC.SetTextDateTimePickerVIETSEA(invoiceDate);

            Notification.Wait.Show("nhập diễn giải");
            string tenNCC = ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_TENKHACHHANG.Text;
            string sohoadon = ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_SOCHUNGTUGOC.Text;
            string diengiai = $"Nhập hàng {tenNCC} hóa đơn {sohoadon} ({invoiceDate})";
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_GHICHU.SetTextByClipboard(diengiai);

            Notification.Wait.Show("nhập mã VAT");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MAVAT.SetTextByClipboard(vat);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");

            //chỉnh sửa tổng tiền hàng và tiền vat cho khớp




            Notification.Wait.Show("chờ người dùng bấm lưu lại");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_LUULAI_Tam.WaitDisable();


            Notification.Wait.Show("chờ thông báo in phiếu");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_MAIN_ERP.THIS.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "#32770").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Bạn muốn in phiếu nhập kho?").WaitForDisplay();

            Notification.Wait.Show("chọn yes in phiếu nhập kho");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_MAIN_ERP.THIS.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "#32770").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Button").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Yes").MoveAndClick();

            Notification.Wait.Show("active phiếu nhập kho");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.frm_RpViewer.ActivateWindowByWM();

            Notification.Wait.Show("click print");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.frm_RpViewer_print.WaitForDisplay();
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.frm_RpViewer_print.MoveAndClick();

            Notification.Wait.Show("chọn số lượng bản in");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.printDialog_numberOfCopies.SetTextByClipboard(numberOfCopies);

            Notification.Wait.Show("print");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.printDialog_buttonPrint.MoveAndClick();

            Notification.Wait.Show("đóng in phiếu");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.frm_RpViewer.CloseWindowByWM();

        }


        public static void Auto_ImportPhieuNhapHang2(string tranDate, string invoiceNumber, string invoiceDate, string maNCC, string vat, string numberOfCopies = "1")
        {
            Import(maNCC, vat);
            FillInfo(tranDate, invoiceNumber, invoiceDate, maNCC, vat);
            EditAmount();
        }

        static void Import(string maNCC, string vat)
        {
            ActiveVIETSEAWindow();

            Notification.Wait.Show("mở form nhập hàng");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.OpenForm();

            Notification.Wait.Show("thêm mới");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_THEMMOI.MoveAndClick();
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_THEMMOI.WaitDisable();

            Notification.Wait.Show("nhập nhà cung cấp");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MAKHACHHANG.SetTextByClipboard(maNCC);

            Notification.Wait.Show("nhập kho");
            string maKho = "BAN" + vat;
            if (vat == "10") maKho = "BAN10";
            if (vat == "8") maKho = "BAN8";
            if (vat == "5") maKho = "BAN05";
            if (vat == "0") maKho = "K-HANG0";

            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MAKHOXUAT.SetTextByClipboard(maKho);

            Notification.Wait.Show("click import");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_import_order_solomon.MoveAndClick();

            Notification.Wait.Show("đóng form thông tin máy chủ");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_MAIN_ERP.buttonCloseThongTinMayChuLayDanhMucVatTu.MoveAndClick();

            // mẹ nó static nên nó execute trước, ko ra kết quả
            //dialog Open
            Notification.Wait.Show("open dialog");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_MAIN_ERP.THIS
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "#32770")
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit")
                .SetTextByClipboard(@"C:\ExcelVSTOAddin\FileImportNhapHang.xls");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_MAIN_ERP.THIS
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "#32770")
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Button")
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Open")
                .MoveAndClick();

            Notification.Wait.Show("chờ thông báo import thành công");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_MAIN_ERP.THIS
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "#32770")
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Button")
                .GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "OK")
                .MoveAndClick();
        }

        static void FillInfo(string tranDate, string invoiceNumber, string invoiceDate, string maNCC, string vat)
        {
            Notification.Wait.Show("chọn lưu tạm");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.cbb_COLUMNFILL.VSComboboxSetText("1");

            Notification.Wait.Show("nhập từ ngày");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.dtp_FROMDATE.SetTextDateTimePickerVIETSEA(tranDate.Replace(@"/", ""));

            Notification.Wait.Show("nhập đến ngày");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.dtp_TODATE.SetTextDateTimePickerVIETSEA(tranDate.Replace(@"/", ""));

            Notification.Wait.Show("click tìm kiếm");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_SEARCH.MoveAndClick();

            Notification.Wait.Show($"tìm đúng số hóa đơn {invoiceNumber}");
            if (ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_SOCHUNGTUGOC.Text != invoiceNumber)
            {
                MsgBox.Show("không đúng số hóa đơn");
                ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationTokenSource?.Cancel();
            }

            Notification.Wait.Show("click sửa");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.btn_SUA.MoveAndClick();

            Notification.Wait.Show("sửa số chứng từ");
            string oldSOCT = ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MACTU.Text;
            string s1 = oldSOCT.Left(7);
            string s2 = tranDate.Left(2) + "/" + tranDate.Mid(4, 2) + "/" + tranDate.Right(2);
            string s3 = oldSOCT.Mid(oldSOCT.LastIndexOf('-') + 1, oldSOCT.Length - oldSOCT.LastIndexOf('-'));
            string newSOCT = s1 + s2 + s3;
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MACTU.SetTextByWM(newSOCT);

            Notification.Wait.Show("nhập nhà cung cấp");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MAKHACHHANG.SetTextByClipboard(maNCC);

            Notification.Wait.Show("Nhập ngày hóa đơn");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.dtp_NGAYCT_GOC.SetTextDateTimePickerVIETSEA(invoiceDate);

            Notification.Wait.Show("nhập diễn giải");
            string tenNCC = ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_TENKHACHHANG.Text;
            string sohoadon = ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_SOCHUNGTUGOC.Text;
            string diengiai = $"Nhập hàng {tenNCC} hóa đơn {sohoadon} ({invoiceDate})";
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_GHICHU.SetTextByClipboard(diengiai);

            Notification.Wait.Show("nhập mã VAT");
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.txt_MAVAT.SetTextByClipboard(vat);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
        }

        static void EditAmount()
        {
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.uiTabPage_VATTU.MoveAndClick();
            ExcelAddIn1.AutoTest.WinAppDriver.Vietsea.UIElement.FRM_NHAPHANG.uiTabPage_VATTU.GetScreenshot();
        }
    }
}
