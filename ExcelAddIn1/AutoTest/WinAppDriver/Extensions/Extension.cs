﻿namespace ExcelAddIn1.AutoTest.WinAppDriver
{
    public static partial class Extension
    {
        public enum MouseClickPosition { TopLeft, Center }

        public static void WaitDisable(this OpenQA.Selenium.Appium.Windows.WindowsElement element)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            while (true)
            {
                WinAppDriver.Core.ThrowIfCancellationRequested();
                if (!element.Enabled)
                {
                    break;
                }
            }
        }

        public static void WaitForEditable(this OpenQA.Selenium.Appium.Windows.WindowsElement element, double timeout = DEFAULT_TIMEOUT_SECONDS)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            while (true)
            {
                WinAppDriver.Core.ThrowIfCancellationRequested();
                if (element.GetAttribute("Value.IsReadOnly") == "False")
                {
                    break;
                }
            }
        }

        public static void WaitEnable(this OpenQA.Selenium.Appium.Windows.WindowsElement element, double timeout = DEFAULT_TIMEOUT_SECONDS)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();

            if (element == null) return;

            bool isEnable(OpenQA.Selenium.Appium.Windows.WindowsElement ele)
            {
                while (true)
                {
                    if (ele.Enabled)
                    {
                        return true;
                    }
                }
                //return false;
            }

            System.Threading.Tasks.Task<bool> task = System.Threading.Tasks.Task<bool>.Run(() =>
            {
                return isEnable(element);
            });

            if (task.Wait(System.TimeSpan.FromSeconds(timeout)))
            {
                //return task.Result;
            }
            else
            {
                throw new System.Exception("Timeout");
            }
        }



        public static void BringToFront(this OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> windowsDriver)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            try
            {
                windowsDriver.Manage().Window.Maximize();
            }
            catch { }
            //string windowHandle = windowsDriver.CurrentWindowHandle;
            //System.IntPtr intPtr = System.Runtime.InteropServices.Marshal.StringToHGlobalAnsi(windowHandle);
            //Method.SetForegroundWindow(intPtr);
        }

        public static void BringToFront(this OpenQA.Selenium.Appium.Windows.WindowsElement element)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> windowsDriver = (OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>)element.WrappedDriver;
            windowsDriver.BringToFront();
        }

        public static void ActivateWindowByWM(this OpenQA.Selenium.Appium.Windows.WindowsElement element)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            Method.SetForegroundWindow(element.GetWindowHandle());
            element.WaitForDisplay();
        }


        public enum ClickType
        {
            Click, DoubleClick
        }

        public static void MoveAndClick(this OpenQA.Selenium.Appium.Windows.WindowsElement element, ClickType clickType = ClickType.Click)
        {

            WinAppDriver.Core.ThrowIfCancellationRequested();
            element.ClickCore(MouseClickPosition.Center, 0, 0, clickType);
        }

        public static void MoveAndClick(this OpenQA.Selenium.Appium.Windows.WindowsElement element, int offsetX, int offsetY, ClickType clickType = ClickType.Click)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            element.ClickCore(MouseClickPosition.TopLeft, offsetX, offsetY, clickType);
        }


        private static void ClickCore(this OpenQA.Selenium.Appium.Windows.WindowsElement element, MouseClickPosition mouseClickPosition, int offsetX = 0, int offsetY = 0, ClickType clickType = ClickType.Click)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();

            element.MoveMouseTo(offsetX, offsetY, mouseClickPosition);

            //WinAppDriverMethod.MouseClickWM(0, 0);
            //OpenQA.Selenium.Interactions.Actions actions = new OpenQA.Selenium.Interactions.Actions(element.WrappedDriver);
            switch (clickType)
            {
                case ClickType.Click:
                    //actions.Click();
                    Method.MouseClickByWM(0, 0);
                    break;
                case ClickType.DoubleClick:
                    //actions.DoubleClick();
                    Method.MouseClickByWM(0, 0);
                    Method.MouseClickByWM(0, 0);
                    break;
            }

            System.Windows.Forms.Application.DoEvents();

        }

        public static void MoveMouseTo(this OpenQA.Selenium.Appium.Windows.WindowsElement element, int offsetX, int offsetY, MouseClickPosition mouseClickPosition)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();

            //cách 1: lặp vì nếu dùng 2 màn hình mà kích thước không bằng nhau thì chuột chỉ di chuyển đến mép giữa 2 màn hình rồi dừng lại
            while (System.Windows.Forms.Cursor.Position != element.GetElementPoint(offsetX, offsetY, mouseClickPosition))
            {
                //System.Windows.Forms.MessageBox.Show($"mouse position: {System.Windows.Forms.Cursor.Position}");
                //System.Windows.Forms.MessageBox.Show($"element position: {element.GetElementPoint(offsetX, offsetY, mouseClickPosition)}");
                WinAppDriver.Core.ThrowIfCancellationRequested();
                System.Windows.Forms.Cursor.Position = element.GetElementPoint(offsetX, offsetY, mouseClickPosition);
            }

            ////cách 2: có animation

            //System.Drawing.Point targetPoint = element.GetElementPoint(offsetX, offsetY, mouseClickPosition);
            //while (System.Windows.Forms.Cursor.Position != targetPoint)
            //{
            //    int currentX = System.Windows.Forms.Cursor.Position.X;
            //    int currentY = System.Windows.Forms.Cursor.Position.Y;
            //    int targetX = targetPoint.X;
            //    int targetY = targetPoint.Y;

            //    int distanceX = System.Math.Abs(targetX - currentX);
            //    int distanceY = System.Math.Abs(targetY - currentY);
            //    //int rate = distanceX >= distanceY ? distanceX / distanceY : distanceY / distanceX;

            //    int jumpX = targetX - currentX == 0 ? 0 : distanceX / (targetX - currentX);
            //    //int jumpY = distanceY / (targetY - currentY);

            //    //phương trình bậc 1
            //    int x1 = System.Windows.Forms.Cursor.Position.X;
            //    int y1 = System.Windows.Forms.Cursor.Position.Y;
            //    int x2 = targetPoint.X;
            //    int y2 = targetPoint.Y;

            //    int a = y2 - y1;
            //    int b = x1 - x2;
            //    int c = a * x1 + b * y1;

            //    int newX = System.Windows.Forms.Cursor.Position.X + jumpX*40;
            //    int newY = (c - a * newX) / b;

            //    if (distanceX > 40)
            //    {
            //        System.Windows.Forms.Cursor.Position = new System.Drawing.Point(newX, newY);
            //        System.Threading.Thread.Sleep(1);

            //    }
            //    else
            //    {
            //        System.Windows.Forms.Cursor.Position = targetPoint;
            //    }

            //}
        }

        public static System.Drawing.Point GetElementPoint(this OpenQA.Selenium.Appium.Windows.WindowsElement element, int offsetX, int offsetY, MouseClickPosition mouseClickPosition)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            string rect = element.GetAttribute("BoundingRectangle");

            string[] array = rect.Split(' ');
            //int left = System.Convert.ToInt32(array[0].GetNumberDigitOnly());
            //int top = System.Convert.ToInt32(array[1].GetNumberDigitOnly());
            //int width = System.Convert.ToInt32(array[2].GetNumberDigitOnly());
            //int height = System.Convert.ToInt32(array[3].GetNumberDigitOnly());
            int left = System.Convert.ToInt32(array[0].Replace("Left:", ""));
            int top = System.Convert.ToInt32(array[1].Replace("Top:", ""));
            int width = System.Convert.ToInt32(array[2].Replace("Width:", ""));
            int height = System.Convert.ToInt32(array[3].Replace("Height:", ""));

            int centerX = 0;
            int centerY = 0;
            switch (mouseClickPosition)
            {
                case MouseClickPosition.TopLeft:
                    centerX = 0;
                    centerY = 0;
                    break;
                case MouseClickPosition.Center:
                    centerX = width / 2;
                    centerY = height / 2;
                    break;
            }

            int X = left + offsetX + centerX;
            int Y = top + offsetY + centerY;

            //System.Diagnostics.Debug.Print($"{rect}");
            //System.Windows.Forms.MessageBox.Show($"{rect}");

            return new System.Drawing.Point(X, Y);
        }

        public static void MenuClick(this OpenQA.Selenium.Appium.Windows.WindowsElement element, string name, int offsetX = 0, int offsetY = 0, int tries = 20)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();

            for (int i = 1; i <= tries; i++)
            {
                WinAppDriver.Core.ThrowIfCancellationRequested();
                try
                {
                    Notification.Wait.Show($"try to click {name}");

                    if (offsetX == 0 && offsetY == 0)
                    {
                        element.MoveAndClick();
                    }
                    else if (offsetX != 0 || offsetY != 0)
                    {
                        element.MoveAndClick(offsetX, offsetY);
                    }

                    OpenQA.Selenium.Appium.Windows.WindowsElement expectedDropDown = (element.WrappedDriver as OpenQA.Selenium.Appium.Windows.WindowsElement).GetElement(FindBy.Name, $"{name}DropDown", 5);
                    if (expectedDropDown != null)
                    {
                        break;
                    }

                }
                catch { }
            }

        }


        public static void WaitForDisplay(this OpenQA.Selenium.Appium.Windows.WindowsElement element, string elementName = "", string caption = "")
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();

            var sw = System.Diagnostics.Stopwatch.StartNew();
            while (true)
            {
                WinAppDriver.Core.ThrowIfCancellationRequested();
                try
                {
                    if (!string.IsNullOrEmpty(elementName))
                    {
                        Notification.Wait.Show($"Wait for display: {elementName} {sw.Elapsed.ToString()}");
                    }
                    if (element.Displayed)
                    {
                        break;
                    }
                }
                catch { }
            }
        }

        public static void WaitForValue(this OpenQA.Selenium.Appium.Windows.WindowsElement element, string value)
        {
            while (true)
            {
                WinAppDriver.Core.ThrowIfCancellationRequested();
                if (element.Text == value)
                {
                    return;
                }

            }
        }

        public static void SetTextByClipboard(this OpenQA.Selenium.Appium.Windows.WindowsElement element, string text)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();

            //var session = element.WrappedDriver;
            ExcelAddIn1.MyClass.ClipboardSafeInvoke.SetText(text);
            element.Click();
            element.Clear();
            System.Windows.Forms.SendKeys.SendWait("^{v}");
            System.Windows.Forms.Application.DoEvents();
        }

        public static void SetTextByWM(this OpenQA.Selenium.Appium.Windows.WindowsElement element, string text)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();

            element.WaitForEditable();

            int WM_SETTEXT = 0X000C;
            Method.SendMessage(element.GetWindowHandle(), WM_SETTEXT, 0, text);

        }

        public static void SetTextDateTimePickerVIETSEA(this OpenQA.Selenium.Appium.Windows.WindowsElement element, string text)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();

            //VIETSEA óc chó datetimepicker chỉ là cái combobox thuần

            //var session = element.WrappedDriver;
            //ExcelAddIn1.MyClass.ClipboardSafeInvoke.SetText(text);
            element.Click();
            element.SendKeys(text);
            //System.Windows.Forms.SendKeys.SendWait(text);
            //element.Clear();
        }


        public static System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        public const double DEFAULT_TIMEOUT_SECONDS = 10800;//3 hours

        public enum FindBy { ID, ClassName, Name }
        // dùng OpenQA.Selenium.By nó không ăn, phải dùng các hàm đích danh


        private static OpenQA.Selenium.Appium.Windows.WindowsElement GetElementCore(object mainElement, FindBy findBy, string valueToFind, double timeOut = DEFAULT_TIMEOUT_SECONDS)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();

            if (mainElement == null) return null;
            System.Type type = mainElement.GetType();
            dynamic element;

            if (type == typeof(OpenQA.Selenium.Appium.Windows.WindowsElement))
            {
                element = (OpenQA.Selenium.Appium.Windows.WindowsElement)mainElement;
            }
            else if (type == typeof(OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>))
            {
                element = (OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>)mainElement;
            }
            else
            {
                throw new System.Exception("Input not correct, element đầu vào sai");
            }

            #region main
            bool found = false;
            OpenQA.Selenium.Appium.Windows.WindowsElement result = null;

            if (timeOut == 0)
            {
                WinAppDriver.Core.ThrowIfCancellationRequested();
                try
                {
                    switch (findBy)
                    {
                        case FindBy.ID:
                            result = (OpenQA.Selenium.Appium.Windows.WindowsElement)element.FindElementByAccessibilityId(valueToFind);
                            break;
                        case FindBy.Name:
                            result = (OpenQA.Selenium.Appium.Windows.WindowsElement)element.FindElementByName(valueToFind);
                            break;
                        case FindBy.ClassName:
                            result = (OpenQA.Selenium.Appium.Windows.WindowsElement)element.FindElementByClassName(valueToFind);
                            break;
                    }
                }
                catch { }
            }
            else if (timeOut > 0)
            {
                System.TimeSpan time = System.TimeSpan.FromSeconds(timeOut);
                stopwatch.Start();
                while (stopwatch.Elapsed <= time && found == false)
                {
                    WinAppDriver.Core.ThrowIfCancellationRequested();
                    try
                    {
                        switch (findBy)
                        {
                            case FindBy.ID:
                                result = (OpenQA.Selenium.Appium.Windows.WindowsElement)element.FindElementByAccessibilityId(valueToFind);
                                break;
                            case FindBy.Name:
                                result = (OpenQA.Selenium.Appium.Windows.WindowsElement)element.FindElementByName(valueToFind);
                                break;
                            case FindBy.ClassName:
                                result = (OpenQA.Selenium.Appium.Windows.WindowsElement)element.FindElementByClassName(valueToFind);
                                break;
                        }
                        found = true;
                    }
                    catch { }
                }

            }



            if (result == null)
            {
                return null;
                //throw new System.Exception("null kìa");
            }

            stopwatch.Stop();
            stopwatch.Reset();
            return result;
            #endregion
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement GetElement(this OpenQA.Selenium.Appium.Windows.WindowsElement element, FindBy findBy, string valueToFind, double timeOut = DEFAULT_TIMEOUT_SECONDS)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            return GetElementCore(element, findBy, valueToFind, timeOut);
        }

        public static OpenQA.Selenium.Appium.Windows.WindowsElement GetElement(this OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> element, FindBy findBy, string valueToFind, double timeOut = DEFAULT_TIMEOUT_SECONDS)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            return GetElementCore(element, findBy, valueToFind, timeOut);
        }


        public static OpenQA.Selenium.Appium.Windows.WindowsElement GetElementByNameAndClassName(this OpenQA.Selenium.Appium.Windows.WindowsElement element, string name, string className, string classNameValue)
        {
            while (true)
            {
                var list = element.FindElementsByName(name);
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        if (item.GetAttribute(className) == classNameValue)
                        {
                            return item as OpenQA.Selenium.Appium.Windows.WindowsElement;
                        }
                    }
                }
            }
        }

        public static System.IntPtr GetWindowHandle(this OpenQA.Selenium.Appium.Windows.WindowsElement element)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            return new System.IntPtr(System.Convert.ToInt32(element.GetAttribute("NativeWindowHandle")));
        }

        public static void CloseWindowByWM(this OpenQA.Selenium.Appium.Windows.WindowsElement element)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            ExcelAddIn1.AutoTest.WinAppDriver.Method.CloseWindowByWM(element.GetWindowHandle());
        }




    }
}
