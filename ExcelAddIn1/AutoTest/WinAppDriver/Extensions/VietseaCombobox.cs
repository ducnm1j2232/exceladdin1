﻿namespace ExcelAddIn1.AutoTest.WinAppDriver
{
    public static partial class Extension
    {
        public static void VSComboboxSetText(this OpenQA.Selenium.Appium.Windows.WindowsElement combobox, string value)
        {
            combobox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit").SetTextByWM(value);
            combobox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Open").MoveAndClick(ClickType.DoubleClick);
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
        }



    }
}
