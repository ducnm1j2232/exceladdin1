﻿namespace ExcelAddIn1.AutoTest.WinAppDriver
{
    public static partial class Extension
    {
        public static bool IsExpand(this OpenQA.Selenium.Appium.Windows.WindowsElement combobox)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            var dropDown = combobox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "ComboLBox", 0);
            if (dropDown == null)
            {
                return false;
            }
            return true;
        }

        static void Expand(this OpenQA.Selenium.Appium.Windows.WindowsElement combobox)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            if (combobox.IsExpand())
            {
                return;
            }

            while (true)
            {
                WinAppDriver.Core.ThrowIfCancellationRequested();
                if (!combobox.IsExpand())
                {
                    OpenQA.Selenium.Appium.Windows.WindowsElement openButton = combobox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Open");
                    openButton.MoveAndClick();
                    break;
                }

            }
        }

        static void WaitColapse(this OpenQA.Selenium.Appium.Windows.WindowsElement combobox)
        {
            while (true)
            {
                WinAppDriver.Core.ThrowIfCancellationRequested();
                if (combobox.IsExpand() == false)
                {
                    break;
                }
            }
        }

        public static void MSComboboxSelectItem(this OpenQA.Selenium.Appium.Windows.WindowsElement combobox, string valueToFind)
        {
            while (true)
            {
                WinAppDriver.Core.ThrowIfCancellationRequested();
                combobox.Expand();

                OpenQA.Selenium.Appium.Windows.WindowsElement item = combobox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "ComboLBox").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, valueToFind, 5);
                if (item != null && item.Displayed)
                {
                    item.MoveAndClick();
                    break;
                }

                //System.Windows.Forms.SendKeys.SendWait(valueToFind);
                combobox.SendKeys(valueToFind);
                if (!combobox.IsExpand())//nếu điền phát đúng giá trị luôn
                {
                    System.Windows.Forms.SendKeys.SendWait("{TAB}");//tab để lost focus, để combobox nó áp dụng text changed
                    if (combobox.Text == valueToFind)
                    {
                        break;
                    }
                }


                item = combobox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "ComboLBox").GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, valueToFind, 5);
                if (item != null && item.Displayed)
                {
                    item.MoveAndClick();
                    break;
                }

                System.Windows.Forms.SendKeys.SendWait("{TAB}");//tab để lost focus, để combobox nó áp dụng text changed
                if (combobox.Text == valueToFind)
                {
                    break;
                }

                combobox.WaitColapse();
            }
        }

        public static void MSComboboxSetText(this OpenQA.Selenium.Appium.Windows.WindowsElement combobox, string value)
        {
            combobox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.ClassName, "Edit").SetTextByWM(value);
            combobox.GetElement(ExcelAddIn1.AutoTest.WinAppDriver.Extension.FindBy.Name, "Open").MoveAndClick();
            //combobox.MoveAndClick();
            System.Windows.Forms.SendKeys.SendWait("{TAB}");
        }



    }
}
