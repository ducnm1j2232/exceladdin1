﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelAddIn1.AutoTest.WinAppDriver;


namespace ExcelAddIn1.AutoTest.WinAppDriver
{
    public static partial class Extension
    {
        public static void DTPSetDate(this OpenQA.Selenium.Appium.Windows.WindowsElement dateTimePicker, string value)
        {
            WinAppDriver.Core.ThrowIfCancellationRequested();
            dateTimePicker.MoveAndClick(0,0);
            dateTimePicker.SendKeys(value);
        }
    }
}
