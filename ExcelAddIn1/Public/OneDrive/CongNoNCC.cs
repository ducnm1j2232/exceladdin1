﻿namespace ExcelAddIn1.Public.OneDrive
{
    public class CongNoNCC
    {
        public const string TableName = "CONGNONCC";

        public const string MANHACUNGCAP = "MANHACUNGCAP";
        public const string TENNHACUNGCAP = "TENNHACUNGCAP";
        public const string THOIGIANCHECK = "THOIGIANCHECK";
        public const string GHICHU = "GHICHU";
        public const string NGAYSODU = "NGAYSODU";
        public const string DUNO = "DUNO";
        public const string DUCO = "DUCO";

    }
}
