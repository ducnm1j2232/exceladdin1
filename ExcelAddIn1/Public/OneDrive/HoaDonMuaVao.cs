﻿namespace ExcelAddIn1.Public.OneDrive
{
    public class HoaDonMuaVao
    {
        public const string TableName = "HOADONMUAVAO";

        public const string NGAYHOADON = TableName + "." + "NGAYHOADON";
        public const string KYHIEU = TableName + "." + "KYHIEU";
        public const string SOHOADON = TableName + "." + "SOHOADON";
        public const string MST = TableName + "." + "MST";
        public const string TEN = TableName + "." + "TEN";
        public const string TIENHANG = TableName + "." + "TIENHANG";
        public const string TIENTHUE = TableName + "." + "TIENTHUE";
        public const string TONGTIEN = TableName + "." + "TONGTIEN";
        public const string TRANGTHAI = TableName + "." + "TRANGTHAI";
        public const string DANHAP = TableName + "." + "DANHAP";
        public const string GHICHU = TableName + "." + "GHICHU";
        public const string CHINHXAC = TableName + "." + "CHINHXAC";
    }
}
