﻿namespace ExcelAddIn1.Public
{
    public static class TableCheckInvoice
    {
        public const string NgayHoaDon = "NGAYHOADON";
        public const string KyHieu = "KYHIEU";
        public const string SoHD = "SOHOADON";
        public const string MST = "MST";
        public const string Ten = "TEN";
        public const string TienHang = "TIENHANG";
        public const string TienThue = "TIENTHUE";
        public const string TongTien = "TONGTIEN";
        public const string TrangThai = "TRANGTHAI";
        public const string DaNhap = "DANHAP";
        public const string GhiChu = "GHICHU";
        public const string ChinhXac = "CHINHXAC";
    }
}
