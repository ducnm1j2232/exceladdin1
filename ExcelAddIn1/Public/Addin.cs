﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.Public
{
    public static class Addin
    {
        public static string StartupPath
        {
            get
            {
                //https://robindotnet.wordpress.com/2010/07/11/how-do-i-programmatically-find-the-deployed-files-for-a-vsto-add-in/
                System.Reflection.Assembly assemblyInfo = System.Reflection.Assembly.GetExecutingAssembly();

                System.Uri uriCodeBase = new System.Uri(assemblyInfo.CodeBase);
                string ClickOnceLocation = System.IO.Path.GetDirectoryName(uriCodeBase.LocalPath.ToString());

                return ClickOnceLocation;
            }
        }
    }
}
