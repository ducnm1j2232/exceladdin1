﻿using System;

namespace ExcelAddIn1.Public
{
    public class Variables
    {
        public const string AddinName = "ExcelAddin1";

        public static string AddInStartupPath
        {
            get
            {
                System.Reflection.Assembly assemblyInfo = System.Reflection.Assembly.GetExecutingAssembly();
                //string assemblyLocation = assemblyInfo.Location;
                Uri uriCodeBase = new Uri(assemblyInfo.CodeBase);
                string ClickOnceLocation = System.IO.Path.GetDirectoryName(uriCodeBase.LocalPath.ToString());
                return ClickOnceLocation;
            }
        }

        public static string AddinFolder
        {
            get
            {
                string folder = @"C:\ExcelVSTOAddin";
                if (!System.IO.Directory.Exists(folder))
                {
                    _ = System.IO.Directory.CreateDirectory(folder);
                }
                return folder;
            }
        }
        public static string ImportNMUAFileName
        {
            get
            {
                return System.IO.Path.Combine(AddinFolder, "FileImportNhapHang");
            }
        }
    }
}
