﻿using System.Collections.Generic;

namespace ExcelAddIn1.Public
{
    public static class VatRate
    {
        static System.Data.DataTable _table = new System.Data.DataTable();
        public static ICollection<object> VatList
        {
            get
            {
                List<object> list = new List<object>
                {
                    10,
                    8,
                    5,
                    0
                };

                return list;
            }
        }

        public static System.Data.DataTable Table()
        {
            _table.Columns.Add("vat");
            foreach(int i in VatList)
            {
                _table.Rows.Add(i);
            }

            return _table;
        }

    }
}
