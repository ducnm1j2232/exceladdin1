﻿namespace ExcelAddIn1.Public
{
    public class PrintType
    {
        public const string Print = "Print";
        public const string PDF = "PDF";
        public const string None = "None";
    }
}
