﻿namespace ExcelAddIn1.Public.DataTables
{
    public static class DanhMucTaiKhoan
    {
        private static System.Data.DataTable _table = null;

        public static System.Data.DataTable GetTable()
        {
            if (_table != null && _table.Rows.Count > 0)
            {
                return _table;
            }

            _table = new System.Data.DataTable();

            _table.Columns.Add("matk", typeof(string));
            _table.Columns.Add("tentk", typeof(string));

            _table.Rows.Add("331", "Phải trả người bán");
            _table.Rows.Add("131", "Phải thu khách hàng");

            return _table;
        }
    }
}
