﻿using System;
using System.Windows.Forms;

namespace ExcelAddIn1.Reports
{
    public partial class FormReport : Form
    {
        public FormReport()
        {
            InitializeComponent();
        }

        public FormReport(Microsoft.Reporting.WinForms.ReportDataSource reportDataSource)
        {
            InitializeComponent();

            reportViewer2.LocalReport.ReportPath = System.IO.Path.Combine(Public.Addin.StartupPath, "Reports/Report1.rdlc");
            reportViewer2.LocalReport.DataSources.Clear();
            reportViewer2.LocalReport.DataSources.Add(reportDataSource);
            reportViewer2.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("ReportParameter1", "paramvalue"));
        }

        private void FormReport_Load(object sender, EventArgs e)
        {

            this.reportViewer2.RefreshReport();
        }
    }
}
