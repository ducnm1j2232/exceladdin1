﻿using ExcelAddIn1.MyClass;
using ExcelAddIn1.MyClass.Extensions;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Tools.Ribbon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using VSTOLib.Extensions;
using ListObject = Microsoft.Office.Interop.Excel.ListObject;
using Workbook = Microsoft.Office.Interop.Excel.Workbook;
using Worksheet = Microsoft.Office.Interop.Excel.Worksheet;

namespace ExcelAddIn1
{
    public partial class Ribbon1
    {
        ExcelAddIn1.Forms.FormCSDL _formCSDL = null;
        ExcelAddIn1.Forms.XtraFormNhapHang _xtraFormNhapHang = null;

        public static bool xllRegister = false;

        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {
            DevExpress.XtraEditors.WindowsFormsSettings.AllowRoundedWindowCorners = DevExpress.Utils.DefaultBoolean.True;

        }




        private void ButtonShowFormSetting_Click_1(object sender, RibbonControlEventArgs e)
        {
            //using (ExcelAddIn1.Forms.InputPassword.FormInputPassword formInputPassword = new Forms.InputPassword.FormInputPassword())
            //{
            //    if (!formInputPassword.ByPass)
            //    {
            //        return;
            //    }
            //}

            using (Forms.FormSetting formSetting = new Forms.FormSetting())
            {
                formSetting.ShowDialog();
            }

        }

        private void buttonExactNumberFromString_Click(object sender, RibbonControlEventArgs e)
        {
            Range selection = Module1.InputBoxRange("Chọn vùng dữ liệu:");
            if (selection == null) return;

            Range destination = Module1.InputBoxRange("Chọn ô chứa kết quả:");
            if (destination == null) return;

            object[,] source = selection.Get2DvalueArray();
            string[,] result = new string[source.GetLength(0), source.GetLength(1)];

            for (int i = 0; i < source.GetLength(0); i++)
            {
                for (int j = 0; j < source.GetLength(1); j++)
                {
                    if (source[i, j] != null)
                    {
                        string s = source[i, j].ToString();
                        string r = System.Text.RegularExpressions.Regex.Replace(s, @"\D", "");
                        result[i, j] = r;
                    }
                }
            }

            result.CopyToRange(destination);
        }

        private void buttonAddBlankRowAtValueChange_Click(object sender, RibbonControlEventArgs e)
        {
            Range selection = Module1.InputBoxRange("Chọn 1 cột dữ liệu mà có giá trị thay đổi:");
            if (selection == null) return;

            selection = selection.MinimumRange();

            try
            {
                Module1.SpeedUpCode(true);
                for (int i = selection.Rows.Count; i >= 2; i--)
                {
                    if (((Range)selection.Cells[i, 1]).Text != ((Range)selection.Cells[i - 1, 1]).Text)
                    {
                        ((Range)selection.Cells[i, 1]).EntireRow.Insert();
                    }
                }
            }
            catch { }
            Module1.SpeedUpCode(false);





        }

        private void buttonMergePDF2sidePrint_Click(object sender, RibbonControlEventArgs e)
        {
            string[] PdfFileNames = null;
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Multiselect = true;
                dialog.Filter = "Pdf Files|*.pdf";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    PdfFileNames = dialog.FileNames;
                }
                else { return; }
            }

            using (PdfSharp.Pdf.PdfDocument targetPDF = new PdfSharp.Pdf.PdfDocument())
            {
                foreach (string pdf in PdfFileNames)
                {
                    using (PdfSharp.Pdf.PdfDocument pdfDoc = PdfSharp.Pdf.IO.PdfReader.Open(pdf, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import))
                    {
                        if (!targetPDF.PageCount.IsEven())
                        {
                            //targetPDF.AddPage();
                            PdfSharp.Pdf.PdfPage p = targetPDF.AddPage();
                            p.Size = PdfSharp.PageSize.A4;
                        }
                        for (int i = 0; i < pdfDoc.PageCount; i++)
                        {
                            targetPDF.AddPage(pdfDoc.Pages[i]);
                        }
                    }
                }

                string firstPDFfullname = PdfFileNames[0].ToString();
                string path = System.IO.Path.GetDirectoryName(firstPDFfullname);
                if (targetPDF.PageCount > 0)
                {
                    targetPDF.Save($"{path}\\File-gộp (in 2 mặt).pdf");
                }
                else
                {
                    MessageBox.Show("Không có trang nào được gộp");
                    return;
                }
            }

            MessageBox.Show("Nối file PDF thành công!");

        }

        private void buttonMergePDFcontain1page_Click(object sender, RibbonControlEventArgs e)
        {
            string[] PdfFileNames = null;
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Multiselect = true;
                dialog.Filter = "Pdf Files|*.pdf";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    PdfFileNames = dialog.FileNames;
                }
                else { return; }
            }

            using (PdfSharp.Pdf.PdfDocument targetPDF = new PdfSharp.Pdf.PdfDocument())
            {
                foreach (string pdf in PdfFileNames)
                {
                    using (PdfSharp.Pdf.PdfDocument pdfDoc = PdfSharp.Pdf.IO.PdfReader.Open(pdf, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import))
                    {
                        if (pdfDoc.PageCount == 1)
                        {
                            targetPDF.AddPage(pdfDoc.Pages[0]);
                        }
                    }
                }

                string firstPDFfullname = PdfFileNames[0].ToString();
                string path = System.IO.Path.GetDirectoryName(firstPDFfullname);
                if (targetPDF.PageCount > 0)
                {
                    targetPDF.Save($"{path}\\File-gộp-1-mặt.pdf");
                }
                else
                {
                    MessageBox.Show("Không có trang nào được gộp");
                    return;
                }
            }

            MessageBox.Show("Nối file PDF thành công!");
        }

        private void buttonMergePDF2page2sidePrint_Click(object sender, RibbonControlEventArgs e)
        {
            string[] PdfFileNames = null;
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Multiselect = true;
                dialog.Filter = "Pdf Files|*.pdf";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    PdfFileNames = dialog.FileNames;
                }
                else { return; }
            }

            using (PdfSharp.Pdf.PdfDocument targetPDF = new PdfSharp.Pdf.PdfDocument())
            {
                foreach (string pdf in PdfFileNames)
                {
                    using (PdfSharp.Pdf.PdfDocument pdfDoc = PdfSharp.Pdf.IO.PdfReader.Open(pdf, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import))
                    {
                        if (pdfDoc.PageCount > 1)
                        {
                            if (!targetPDF.PageCount.IsEven())
                            {
                                //targetPDF.AddPage();
                                PdfSharp.Pdf.PdfPage p = targetPDF.AddPage();
                                p.Size = PdfSharp.PageSize.A4;
                            }
                            for (int i = 0; i < pdfDoc.PageCount; i++)
                            {
                                targetPDF.AddPage(pdfDoc.Pages[i]);
                            }
                        }
                    }
                }

                string firstPDFfullname = PdfFileNames[0].ToString();
                string path = System.IO.Path.GetDirectoryName(firstPDFfullname);
                if (targetPDF.PageCount > 0)
                {
                    targetPDF.Save($"{path}\\File-gộp-2-mặt.pdf");
                }
                else
                {
                    MessageBox.Show("Không có trang nào được gộp");
                    return;
                }
            }

            MessageBox.Show("Nối file PDF thành công!");
        }


        private void buttonViewAccountStatementTechcombank_Click_1(object sender, RibbonControlEventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;
            if (ws.Range["a1"].Value2 != "NGÂN HÀNG TMCP KỸ THƯƠNG VIỆT NAM")
            {
                MessageBox.Show("Đây không phải sao kê Techcombank nguyên gốc");
                return;
            }

            ws.Range["e16"].Value2 = "Phí quẹt thẻ:";
            ws.Range["e17"].Value2 = "Tiền quẹt thẻ:";

            ws.Range["f16"].Value2 = System.Math.Abs(Globals.ThisAddIn.Application.WorksheetFunction.SumIf(ws.GetRange("b19:b", "a"), "GD THE*", ws.GetRange("e19:e", "a"))
                                    + Globals.ThisAddIn.Application.WorksheetFunction.SumIf(ws.GetRange("b19:b", "a"), "*TT THE*", ws.GetRange("e19:e", "a")));

            ws.Range["f17"].Value2 = System.Math.Abs(Globals.ThisAddIn.Application.WorksheetFunction.SumIf(ws.GetRange("b19:b", "a"), "GD THE*", ws.GetRange("f19:f", "a"))
                                    + Globals.ThisAddIn.Application.WorksheetFunction.SumIf(ws.GetRange("b19:b", "a"), "*TT THE*", ws.GetRange("f19:f", "a")));

            ws.Range["f16:f17"].SetAccountingNumberFormat();
        }

        private void buttonViewAccountStatementVietcombank_Click_1(object sender, RibbonControlEventArgs e)
        {
            Worksheet activesheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (activesheet.Range["a3"].Value2 != "Chủ tài khoản:")
            {
                MessageBox.Show("Đây không phải sao kê Vietcombank nguyên gốc");
                return;
            }

            Worksheet ws = activesheet.Clone();

            ws.Range["a" + ws.NextRowIndexAtColumn("b"), "a" + ws.Rows.Count].EntireRow.Delete();
            Range sortRange = ws.GetRange("a11:e", "a");
            sortRange.Sort1(ws.Range["a1"], XlSortOrder.xlAscending);
            sortRange.Sort2(ws.Range["d1"], XlSortOrder.xlAscending);
            sortRange.Sort2(ws.Range["e1"], XlSortOrder.xlAscending);

            ws.Range["f11"].SetHeaderText(new[] { "Số tiền", "Phí", "Số dư", "Loại GD", "Tổng quẹt thẻ", "Tổng phí quẹt thẻ" });

            //số dư
            ws.Range["h12"].Formula = "=$B$10-SUM($C$12:C12)+SUM($D$12:D12)";
            ws.GetRange("h12:h", "a").FillDownFormula();


            //tính số tiền
            int LR = ws.LastRow("a");
            for (int i = 12; i <= LR; i++)
            {
                Range referenceNumberRange = ws.Range["b" + i];
                Range debitAmountRange = ws.Range["c" + i];
                Range creditAmountRange = ws.Range["d" + i];
                Range result_amount_range = ws.Range["f" + i];
                Range result_fee_range = ws.Range["g" + i];
                Range result_tranType_range = ws.Range["i" + i];

                #region credit card payment
                string description = ws.Range["e" + i].GetValue(ValueType.String);
                if (isCreditCardPayment(description))
                {
                    result_amount_range.Value2 = CreditCardPaymentAmount(description);
                    if (result_amount_range.Value2 != 0)
                    {
                        result_fee_range.Value2 = result_amount_range.GetValue(ValueType.Double) - creditAmountRange.GetValue(ValueType.Double);
                    }
                    else
                    {
                        result_fee_range.Value2 = "lỗi tính toán";
                    }

                    result_tranType_range.Value2 = Accounting.Banking.TransactionType.GiaoDichThe;
                    continue;
                }
                #endregion

                #region IBVCB internet banking
                if (debitAmountRange.Value2 != null && creditAmountRange.Value2 == null && description.Left(5).ToLower() == "ibvcb")//nếu là giao dịch chuyển khoản internet banking
                {
                    double fee = internetBankingFee(referenceNumberRange.GetValue(ValueType.String), debitAmountRange.GetValue(ValueType.Double));
                    result_fee_range.Value2 = fee;
                    result_amount_range.Value2 = debitAmountRange.Value2 - fee;
                    result_tranType_range.Value2 = Accounting.Banking.TransactionType.InternetBankingVCB;
                    continue;
                }
                #endregion

                #region tiền về
                bool isPayment(Range referenceRange, Range creditAmountRng)
                {
                    bool result = false;

                    if (!isCreditCardPayment(description) && creditAmountRng.Value2 != null)
                    {
                        result = true;
                    }
                    return result;
                }

                if (isPayment(referenceNumberRange, creditAmountRange))
                {
                    result_amount_range.Value2 = creditAmountRange.Value2;
                    result_tranType_range.Value2 = Accounting.Banking.TransactionType.KhachTraTien;
                    continue;
                }
                #endregion

                #region các loại phí còn lại
                result_amount_range.Value2 = debitAmountRange.GetValue(ValueType.Double) + creditAmountRange.GetValue(ValueType.Double);
                #endregion
            }

            //tổng tiền quẹt thẻ
            ws.Range["j12"].Formula = "=IF(I12=\"Giao dịch thẻ\",SUMIFS(F:F,A:A,A12,I:I,\"Giao dịch thẻ\"),\"\")";
            ws.Range["k12"].Formula = "=IF(I12=\"Giao dịch thẻ\",SUMIFS(G:G,A:A,A12,I:I,\"Giao dịch thẻ\"),\"\")";
            ws.GetRange("j12:k", "a").FillDownFormula();

            //add data validation
            System.Collections.Generic.List<string> listBankingType = new System.Collections.Generic.List<string>
            {
                Accounting.Banking.TransactionType.GiaoDichThe,
                Accounting.Banking.TransactionType.InternetBankingVCB,
                Accounting.Banking.TransactionType.KhachTraTien,
                Accounting.Banking.TransactionType.PhiGiaiNganVCB
            };


            string formula1 = string.Join(",", listBankingType);
            ws.GetRange("i12:i", "a").Validation.Add(XlDVType.xlValidateList, XlDVAlertStyle.xlValidAlertStop, XlFormatConditionOperator.xlBetween, formula1);

            //formatting
            ws.Rows.RowHeight = 30;
            ws.GetRange("f11:i", "a").Columns.AutoFit();
            ws.GetRange("f11:k", "a").SetAccountingNumberFormat();
            //sh.InsertBlankRowsAtValueChange(sh.GetRange("a12:a", "a"));



        }

        private bool isCreditCardPayment(string description)
        {
            if (description.ToLower().Contains("dvcnt") || description.ToLower().ToLower().Contains("merchno"))
            {
                return true;
            }
            return false;
        }

        private double CreditCardPaymentAmount(string description)
        {
            double result = 0;
            if (description.ToLower().Contains("dvcnt"))
            {
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"0\.22\*(.*)\.VAT");//string1 (.*) string2
                System.Text.RegularExpressions.Match match = regex.Match(description);
                if (match.Success)
                {
                    return Convert.ToDouble(match.Groups[1].Value);//https://stackoverflow.com/questions/5642315/regular-expression-to-get-a-string-between-two-strings-in-javascript
                    //When you get the result of the match, you need to extract the matched text of the first capturing group with matched[1], not the whole matched text with matched[0]
                }
            }

            if (description.ToLower().Contains("merchno"))
            {
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"VAT Amt:(.*)\*");//string1 (.*) string2
                System.Text.RegularExpressions.Match match = regex.Match(description);
                if (match.Success)
                {
                    return Convert.ToDouble(match.Groups[1].Value);//https://stackoverflow.com/questions/5642315/regular-expression-to-get-a-string-between-two-strings-in-javascript
                    //When you get the result of the match, you need to extract the matched text of the first capturing group with matched[1], not the whole matched text with matched[0]
                }
            }
            return result;
        }

        private double internetBankingFee(string referenceNumber, double debitamount)
        {
            double result = 0;
            if (new string[] { "5056", "5057" }.Any(s => referenceNumber.Left(4) == s))//cùng vietcombank 7.700
            {
                return 7000 * 1.1;
            }

            if (new string[] { "5058" }.Any(s => referenceNumber.Left(4) == s))//chuyển tiền thường khác vietcombank
            {
                string phiChuyenTienNgoaiVCB = Config.Manager.GetValue(Config.ID.PhiChuyenTienNgoaiVCB);
                double minFee = Convert.ToDouble(phiChuyenTienNgoaiVCB);
                double rate = debitamount < 500000000 ? 0.00015 : 0.00027;
                double maxFee = debitamount < 500000000 ? 75000 : 975000;
                return Math.Round(Math.Min(Math.Max(minFee * 1.1, debitamount - (debitamount / (1 + 1.1 * rate))), maxFee * 1.1), 0);
            }

            if (referenceNumber.Left(4) == "5059")
            {
                return 22000;
            }

            return result;
        }

        private void buttonCreateFilePulove_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Range selection = Module1.InputBoxRange("Chọn cột dữ liệu:", true);
                if (selection == null) return;

                Module1.SpeedUpCode(true);
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sh = wb.GetSheet("pulove");

                string path = PublicVar.ExcelVSTOAddinFolder() + "\\pulove.txt";
                if (System.IO.File.Exists(path)) System.IO.File.Delete(path);

                selection.CopyValueTo(sh.Range["a1"]);
                Range dataRange = sh.GetRange("a1:a", "a");
                System.Text.StringBuilder builder = new System.Text.StringBuilder();
                builder.AppendLine(dataRange.Rows.Count.ToString());
                foreach (Range rg in dataRange)
                {
                    builder.AppendLine(rg.GetValue(ValueType.String));
                }

                System.IO.File.AppendAllText(path, builder.ToString());
                wb.DeleteSheet("pulove");
                Module1.SpeedUpCode(false);
                Globals.ThisAddIn.Application.Goto(selection);
                Notification.Toast.Show("Tạo file pulove thành công!");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Module1.SpeedUpCode(false);
            }

        }

        private void button_ShowAdvanceFilterPane_Click(object sender, RibbonControlEventArgs e)
        {
            ExcelAddIn1.MyUserControl.CustomTaskPaneManager.Add(new MyUserControl.AdvanceFilter(), "Lọc nâng cao", 350);

        }

        private void buttonUpdateAddin_Click(object sender, RibbonControlEventArgs e)
        {
            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                string url = @"http://thanhcongtongkho.ddns.me/DUC/setup.exe";
                string filename = System.IO.Path.Combine(PublicVar.ExcelVSTOAddinFolder(), "setup.exe");
                if (System.IO.Directory.Exists(filename))
                {
                    System.IO.Directory.Delete(filename);
                }
                webClient.DownloadFile(url, filename);
                System.Diagnostics.Process.Start(filename);
            }
        }

        private void button1_Click_1(object sender, RibbonControlEventArgs e)
        {



            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("Sheet1");
            System.Collections.Generic.List<UserClass.Good> listGood = new System.Collections.Generic.List<UserClass.Good>();

            int LR = ws.LastRow("a");
            for (int i = 2; i <= LR; i++)
            {
                UserClass.Good good = new UserClass.Good();
                good.GoodID = ws.Range["b" + i].GetStringValue();
                good.Quantity = ws.Range["h" + i].Value2;
                good.Price = ws.Range["g" + i].Value2;

                listGood.Add(good);
            }

            ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
            WinAppDriver.ThanhCong.ThanhCongAuto.OpenApplication("ADMIN", "123456", "00");
            WinAppDriver.ThanhCong.ThanhCongAuto.TestNhapHangThuCong("00003", "000007", listGood);
            ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
        }

        private void button_Auto_HachToanGDTheTECH_Click_1(object sender, RibbonControlEventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;

            string ngay = (ws.Range["a20"].Text as string).Trim();
            string phi = ws.Range["f16"].GetValue(ValueType.String);
            string sotien = ws.Range["f17"].GetValue(ValueType.String);

            ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
            WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);
            WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanGDThe(Accounting.Banking.Name.Techcombank, ngay, Accounting.Banking.AccountNumber.Techcombank, phi, sotien);

            ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
        }

        private void button_Auto_HachToanGDTheVCB_Click_1(object sender, RibbonControlEventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;

            DateTime date = System.DateTime.Parse(ws.Range["a12"].GetValue(ValueType.String));
            string ngayThang = date.ToString("dd/MM/yyyy");
            string phi = ws.Range["k12"].GetValue(ValueType.String);
            string sotien = ws.Range["j12"].GetValue(ValueType.String);

            ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
            WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);
            WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanGDThe(Accounting.Banking.Name.Vietcombank, ngayThang, "11214", phi, sotien);
            ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
        }

        private void ButtonShowTaskPane_Click_1(object sender, RibbonControlEventArgs e)
        {
            TaskPane_NhapHoaDon taskPane_NhapHoaDon = new TaskPane_NhapHoaDon();
            ExcelAddIn1.MyUserControl.CustomTaskPaneManager.Add(taskPane_NhapHoaDon, "Nhập HĐ", taskPane_NhapHoaDon.ActualWidth);
        }

        private void ButtonShowFormCSDL_Click_1(object sender, RibbonControlEventArgs e)
        {
            if (_formCSDL == null)
            {
                _formCSDL = new ExcelAddIn1.Forms.FormCSDL();
            }
            if (_formCSDL.IsDisposed)
            {
                _formCSDL = new ExcelAddIn1.Forms.FormCSDL();
            }

            _formCSDL.Show();
            _formCSDL.BringToFront();
        }

        private void buttonTestXuatBanThanhCong_Click(object sender, RibbonControlEventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("Kết quả test");

            //await System.Threading.Tasks.Task.Delay(1);
            #region MAIN
            //string input = DevExpress.XtraEditors.XtraInputBox.Show("Nhập số lần lặp", "", "");
            //if (string.IsNullOrEmpty(input)) return;
            //int number = System.Convert.ToInt32(input);

            ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();

            WinAppDriver.ThanhCong.ThanhCongAuto.OpenApplication("ADMIN", "123456", "00");
            //System.Collections.Generic.List<string> list = new System.Collections.Generic.List<string>() { "01002481", "01002469", "01002254", "01001577", "01001765", "01008098", "01003759", "01002490", "01002465", "01002528" };


            if (ExcelAddIn1.AutoTest.WinAppDriver.Core.IsCancel)
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
                return;
            }
            WinAppDriver.ThanhCong.ThanhCongAuto.TestXuatBan(ws, 1, "XUATBB", "00005", "000786", "12082021", "12082021");


            //WinAppDriver.ThanhCong.ThanhCongAuto.TestXuatBan(ws, 7, "XUATBB", "00003", "000786", "11082021", "11082021");

            ExcelAddIn1.Notification.Wait.Close();
            ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            MsgBox.Show("Chạy test xong");
            #endregion




        }

        private void buttonStopWinAppDriver_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Notification.Wait.Close();
            }
            catch { }
            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationTokenSource?.Cancel();
            }
            catch { }

            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }
            catch { }
        }

        private void buttonTestSignInThanhCong_Click(object sender, RibbonControlEventArgs e)
        {
            ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
            WinAppDriver.ThanhCong.ThanhCongAuto.OpenApplication("ADMIN", "123456", "00");
            ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
        }

        private void buttonPasteToNewSheet_Click(object sender, RibbonControlEventArgs e)
        {
            Range selection = Globals.ThisAddIn.Application.Selection;
            Worksheet parentWorksheet = selection.Worksheet;
            Workbook parentWorkbook = (Workbook)selection.Worksheet.Parent;
            Worksheet ws = parentWorkbook.Worksheets.Add(After: parentWorksheet);
            selection.Copy(ws.Range["a1"]);
            ws.UsedRange.ClearAllBlankRowBlankColumnAndFormating();
        }

        private void buttonTestToastForm_Click(object sender, RibbonControlEventArgs e)
        {
            Notification.Toast.Show($"{System.DateTime.Now.ToString()}");
        }

        private async void buttonTestSignInVIETSEA_Click(object sender, RibbonControlEventArgs e)
        {
            ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();
            try
            {
                await System.Threading.Tasks.Task.Run(() =>
                {

                    ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
                    WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA);
                    Notification.Toast.Show("Đăng nhập thành công");
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
                }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);

            }
            catch (System.OperationCanceledException)
            {
                Notification.Toast.Show("Macro was canceled");
            }
            finally
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }
        }

        private async void buttonTestCancel_Click(object sender, RibbonControlEventArgs e)
        {

            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();
                await System.Threading.Tasks.Task.Run(() => { Loop100(1000); }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);
            }
            catch (System.OperationCanceledException)
            {
                MsgBox.Show("canceled");
            }
            finally
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }
        }

        void Loop100(int count)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;

            for (int i = 1; i <= count; i++)
            {
                Notification.Wait.Show($"current value is: {i}");
                ws.Range["a" + i].Value2 = i;
                System.Threading.Thread.Sleep(500);

                ExcelAddIn1.AutoTest.WinAppDriver.Core.ThrowIfCancellationRequested();
            }
        }

        void throwIfCancel(System.Threading.CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                token.ThrowIfCancellationRequested();
            }
        }

        private void buttonShowWADLocation_Click(object sender, RibbonControlEventArgs e)
        {
            System.Reflection.Assembly assemblyInfo = System.Reflection.Assembly.GetExecutingAssembly();

            string assemblyLocation = assemblyInfo.Location;

            Uri uriCodeBase = new Uri(assemblyInfo.CodeBase);
            string ClickOnceLocation = System.IO.Path.GetDirectoryName(uriCodeBase.LocalPath.ToString());

            string winappdriverEXE = System.IO.Path.Combine(ClickOnceLocation, @"AutoTest\WinAppDriver\WindowsApplicationDriver\WinAppDriver.exe");
            if (System.IO.File.Exists(winappdriverEXE))
            {
                System.Diagnostics.Process.Start(winappdriverEXE);
                MsgBox.Show($"Có tồn tại file {winappdriverEXE}");
            }
            else
            {
                MsgBox.Show($"Không tồn tại file {winappdriverEXE}");
            }

        }

        private async void buttonTestWriteDataToGoogleSheet_Click(object sender, RibbonControlEventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;

            //System.Collections.Generic.List<System.Collections.Generic.IList<object>> values = new System.Collections.Generic.List<System.Collections.Generic.IList<object>>();
            //values.Add(new System.Collections.Generic.List<object>() {"cell1 value" });

            var newValues = ((Range)Globals.ThisAddIn.Application.Selection).ToListList(false);
            //await ExcelAddIn1.GoogleDev.SheetsAPI.AppendValueAsync(GoogleDev.SheetsAPI._spreadSheetID, "KyHieuHoaDon!A1:B1", newValues);

            //var getvalue = await ExcelAddIn1.GoogleDev.SheetsAPI.GetValuesAsync(ExcelAddIn1.GoogleDev.SheetsAPI._spreadSheetID, "KyHieuHoaDon!A1:B");
            //getvalue.To2DArray().CopyToRange(ws.Range["a1"]);

            await ExcelAddIn1.GoogleApis.SheetsAPI.AppendValueAsync(ExcelAddIn1.GoogleApis.SheetsAPI.spreadSheetID, "MLnetData!A:B", newValues);
            Notification.Toast.Show("append thành công");
        }

        private void buttonTestWaitForm_Click(object sender, RibbonControlEventArgs e)
        {
            Notification.Wait.Show("This is a test");
        }

        private void buttonTurnOffSpeedUpCode_Click(object sender, RibbonControlEventArgs e)
        {
            Module1.SpeedUpCode(false);
        }

        private void buttonCapitalizeFirstChar_Click(object sender, RibbonControlEventArgs e)
        {
            Range selection = (Globals.ThisAddIn.Application.Selection as Range).MinimumRange();
            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            if (selection.IsSingleCell())
            {
                selection.Value = selection.GetStringValue().CapitalizeFirstLetter();
            }
            else
            {
                foreach (Range eachRange in selection)
                {
                    eachRange.Value = eachRange.GetStringValue().CapitalizeFirstLetter();
                }
            }
        }

        private void buttonCapitalizeFirstCharEachWord_Click(object sender, RibbonControlEventArgs e)
        {
            Range selection = (Globals.ThisAddIn.Application.Selection as Range).MinimumRange();
            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            foreach (Range eachRange in selection)
            {
                eachRange.Value = textInfo.ToLower(eachRange.GetStringValue());
                eachRange.Value = textInfo.ToTitleCase(eachRange.GetStringValue());
            }
        }

        private void buttonCapitalizeAll_Click(object sender, RibbonControlEventArgs e)
        {
            Range selection = (Range)Globals.ThisAddIn.Application.Selection;

            if (selection.IsSingleCell())
            {
                //selection = selection;
            }

            //selection = selection.MinimumRange();

            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            foreach (Range eachRange in selection)
            {
                eachRange.Value = textInfo.ToUpper(eachRange.GetStringValue());
            }
        }

        private void buttonLowerAll_Click(object sender, RibbonControlEventArgs e)
        {
            Range selection = (Globals.ThisAddIn.Application.Selection as Range).MinimumRange();
            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            foreach (Range eachRange in selection)
            {
                eachRange.Value = textInfo.ToLower(eachRange.GetStringValue());
            }
        }

        private async void buttonTestWaitingForm_Click(object sender, RibbonControlEventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                Notification.Wait.Show(System.DateTime.Now.ToString());
                await System.Threading.Tasks.Task.Delay(1000);
            }

            Notification.Wait.Close();
        }

        private void button2HachToanGDTheTechChiTietTungGD_Click(object sender, RibbonControlEventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("Account Statement");

            int firstDataRow = 20;
            int LR = ws.LastRow("a");

            System.Collections.Generic.List<string> listDateTime = ws.GetRange($"a{firstDataRow}:a", "a").ValueToList(true);

            foreach (string eachDateTime in listDateTime)
            {
                //Globals.ThisAddIn.Application. WorksheetFunction.sum
            }
        }



        private void buttonOpenFormNhap_Click(object sender, RibbonControlEventArgs e)
        {
            if (_xtraFormNhapHang == null || _xtraFormNhapHang.IsDisposed)
            {
                _xtraFormNhapHang = new Forms.XtraFormNhapHang();
            }

            if (_xtraFormNhapHang.WindowState == FormWindowState.Minimized)
            {
                _xtraFormNhapHang.WindowState = FormWindowState.Normal;
            }

            _xtraFormNhapHang.Show();
            _xtraFormNhapHang.BringToFront();
        }

        private void buttonCompareTwoRanges_Click(object sender, RibbonControlEventArgs e)
        {
            Range range1 = Module1.InputBoxRange("Chọn vùng dữ liệu 1:");
            if (range1 == null) return;

            Range range2 = Module1.InputBoxRange("Chọn vùng dữ liệu 2:");
            if (range2 == null) return;

            Task.Run(() =>
            {
                foreach (Range r1 in range1)
                {
                    if (Globals.ThisAddIn.Application.WorksheetFunction.CountIf(range2, r1.Value) == 0)
                    {
                        r1.Interior.Color = ExcelSystem.InteriorColorInteger.Orange;
                    }
                }
            });

            Task.Run(() =>
            {
                foreach (Range r2 in range2)
                {
                    if (Globals.ThisAddIn.Application.WorksheetFunction.CountIf(range1, r2.Value) > 0)
                    {
                        r2.Interior.Color = ExcelSystem.InteriorColorInteger.Orange;
                    }
                }
            });

        }

        private void button2_Click(object sender, RibbonControlEventArgs e)
        {
            ShowReport();

            void ShowReport()
            {
                System.Data.DataTable dt = new System.Data.DataTable("d1");
                dt.Columns.Add("date", typeof(string));
                dt.Columns.Add("name", typeof(string));

                dt.Rows.Add("r1", "n1");
                dt.Rows.Add("r2", "n2");
                dt.Rows.Add("r3", "n3");
                dt.Rows.Add("r4", "n4");

                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource = new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt);
                ExcelAddIn1.Reports.FormReport formReport = new Reports.FormReport(reportDataSource);
                formReport.Show();
            }

        }


        private void toggleButtonPinExcel_Click(object sender, RibbonControlEventArgs e)
        {

            bool isTopMost = ((RibbonToggleButton)sender).Checked;

            IntPtr excelHandle = new IntPtr(Globals.ThisAddIn.Application.Hwnd);
            VSTOLib.User32dll.Func.SetWindowPos(excelHandle, isTopMost);

        }

        private void buttonTranformExcelToMInvoiceTemplate_Click(object sender, RibbonControlEventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;

            ws.Range["b1"].EntireColumn.Insert();
            ws.Range["b1"].Value2 = "Tính chất(*)";
            //int LR = ws.LastRow("a");
            ws.GetRange("b2:b", "a").Value2 = 1;
            ws.GetRange("j2:j", "a").Value2 = ws.GetRange("h2:h", "a").Value2;
            ws.GetRange("h2:h", "a").ClearContents();

            Notification.Toast.Show("Thêm cột tính chất thành công!");
        }

        private void buttonAddCharacterToDisplayAsText_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Range selection = Globals.ThisAddIn.Application.Selection;
                selection = selection.MinimumRange();
                foreach (Range rng in selection)
                {
                    if (!rng.HasFormula)
                    {
                        rng.Value = "'" + rng.Text;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }

        private void buttonConvertRangeToValue_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Range selection = Globals.ThisAddIn.Application.Selection;
                selection = selection.MinimumRange();
                selection.PasteValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }

        private void buttonMergeInColumns_Click(object sender, RibbonControlEventArgs e)
        {
            Range selection = Globals.ThisAddIn.Application.Selection;

            foreach (Range column in selection.Columns)
            {
                System.Diagnostics.Debug.WriteLine(column.Address);
                column.MergeAndCenter();
            }
        }

        private void buttonShowXtraFormSetting_Click(object sender, RibbonControlEventArgs e)
        {
            //using (ExcelAddIn1.Forms.InputPassword.FormInputPassword formInputPassword = new Forms.InputPassword.FormInputPassword())
            //{
            //    if (!formInputPassword.ByPass)
            //    {
            //        return;
            //    }
            //}
            try
            {
                using (Forms.XtraFormSettings form = new Forms.XtraFormSettings())
                {
                    form.StartPosition = FormStartPosition.CenterScreen;
                    form.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonShowClipboard_Click(object sender, RibbonControlEventArgs e)
        {
            string clipboard = VSTOLib.Clipboard.ClipboardSafeInvoke.GetText();
            MessageBox.Show(clipboard.ToString());
        }

        private void Operation(Workbook workbook, string file)
        {
            try
            {
                Workbook wb = Globals.ThisAddIn.Application.Workbooks.Open(file);
                string filenameOnly = wb.Name;
                Worksheet ws = wb.GetSheet("MH_PNK");

                ws.UsedRange.Find("(Ban hành theo QĐ số 48/2006/QĐ-BTC").Value2 = "(Ban hành theo TT số 200/2014/TT-BTC";
                ws.UsedRange.Find("Ngày 14/09/2006 của Bộ trưởng BTC)").Value2 = "Ngày 22/12/2014 của Bộ trưởng BTC)";

                Range cell1 = ws.UsedRange.Find("Data Regions within table/matrix cells are ignored.");
                cell1.UnMerge();

                Range sltheochungtu = ws.Range[cell1, cell1.offset(ExtensionMethod.xlOffset.Right, 6)];

                Range cell2 = cell1.offset(ExtensionMethod.xlOffset.Right, 7);
                Range slthucnhap = ws.Range[cell2, cell2.offset(ExtensionMethod.xlOffset.Right, 6)];

                sltheochungtu.MergeAndCenter();
                slthucnhap.MergeAndCenter();

                sltheochungtu.Value2 = "SL theo chứng từ";
                slthucnhap.Value2 = "SL thực nhập";

                sltheochungtu.Font.Name = "Times New Roman";
                sltheochungtu.Font.Size = 9;
                sltheochungtu.Font.Bold = true;


                slthucnhap.Font.Name = "Times New Roman";
                slthucnhap.Font.Size = 9;
                slthucnhap.Font.Bold = true;

                ws.UsedRange.Find("Tiền VAT").EntireRow.Delete();
                ws.UsedRange.Find("Tiền CK").EntireRow.Delete();
                ws.UsedRange.Find("Tổng thanh toán").EntireRow.Delete();

                int rowHeader = ws.UsedRange.Find("Mã vật tư (HT)").Row;
                int rowCongTienHang = ws.UsedRange.Find("Cộng tiền hàng").Row;
                int rowTienBangChu = ws.UsedRange.Find("Tổng số tiền(viết bằng chữ): ").Row;

                Range amount = ws.Range["ca" + rowCongTienHang].End[XlDirection.xlToLeft];
                //MessageBox.Show(amount.Address);
                ws.Range["v" + rowTienBangChu].Formula = VSTOLib.Excel.Formula.VNDBlueSoftWebservice(amount);
                ws.Range["v" + rowTienBangChu].Value2 = ws.Range["v" + rowTienBangChu].Value2 + " đồng";

                ws.Range["a19", amount].Borders.LineStyle = XlLineStyle.xlContinuous;

                ws.Cells.Copy(workbook.GetNewSheet(filenameOnly).Range["a1"]);
                wb.Close(SaveChanges: false);
            }
            catch
            {

            }
        }

        private async void buttonTestExportPDF_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                await Task.Run(() =>
                {
                    AutoTest.WinAppDriver.Vietsea.Flows.OpenVietsea();
                    AutoTest.WinAppDriver.Vietsea.Flows.TestExportPDF(ExcelAddIn1.Public.PrintType.PDF, "1");
                });
            }
            catch
            {

            }
        }

        private void splitButtonSetNumberFomat_Click(object sender, RibbonControlEventArgs e)
        {
            Range selection = Globals.ThisAddIn.Application.Selection;
            //selection = selection.MinimumRange();
            selection.SetAccountingNumberFormat();
        }

        private void buttonGopFileExcel_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog()
                {
                    Title = "Chọn các file Excel cần gộp",
                    Multiselect = true,
                    Filter = "Excel Files|*.xls;*.xlsx;*.xlsm"
                };

                string[] filenames = null;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filenames = openFileDialog.FileNames;
                }
                else
                {
                    return;
                }

                Workbook newWorkbook = Globals.ThisAddIn.Application.Workbooks.Add();
                foreach (string filename in filenames)
                {
                    Workbook eachWorkbook = Globals.ThisAddIn.Application.Workbooks.Open(filename);
                    foreach (Worksheet eachWorksheet in eachWorkbook.Worksheets)
                    {
                        eachWorksheet.Copy(After: newWorkbook.Worksheets[newWorkbook.Worksheets.Count]);
                    }
                    eachWorkbook.Close(false);
                }

                MessageBox.Show("Gộp file Excel thành công!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }

        private void buttonOpenStartupPath_Click(object sender, RibbonControlEventArgs e)
        {
            string path = Public.Addin.StartupPath;
            if (System.IO.Directory.Exists(path))
            {
                System.Diagnostics.Process.Start("explorer.exe", path);
            }
        }

        private void buttonTestConvert_Click(object sender, RibbonControlEventArgs e)
        {
            Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet worksheet = workbook.GetSheet("Sheet1");
            var list = worksheet.GetRange("a1:a", "a").ValueToList();
            Worksheet worksheet2 = workbook.GetNewSheet("data source");
            foreach (string item in list)
            {
                //worksheet2.NewRangeAtColumn("a").Value2 = item;
                setSTT(item);

            }

            void setSTT(string value)
            {
                for (int i = 1; i <= 150; i++)
                {
                    worksheet2.NewRangeAtColumn("a").Value2 = value;
                    worksheet2.NewRangeAtColumn("b").Value2 = i;
                }
            }

            MessageBox.Show("xong");


        }

        private void buttonCreatePowerAutomateTxtFile_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Range selection = Module1.InputBoxRange("Chọn cột dữ liệu:", true);
                if (selection == null) return;

                Module1.SpeedUpCode(true);
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sh = wb.GetNewSheet("powerautomate");

                string path = PublicVar.ExcelVSTOAddinFolder() + "\\powerautomate.txt";
                if (System.IO.File.Exists(path)) System.IO.File.Delete(path);

                selection.CopyValueTo(sh.Range["a1"]);
                Range dataRange = sh.GetRange("a1:a", "a");
                System.Text.StringBuilder builder = new System.Text.StringBuilder();
                //builder.AppendLine(dataRange.Rows.Count.ToString());
                foreach (Range rg in dataRange)
                {
                    builder.AppendLine(rg.GetValue(ValueType.String));
                }

                System.IO.File.AppendAllText(path, builder.ToString());
                wb.DeleteSheet("powerautomate");
                Module1.SpeedUpCode(false);
                Globals.ThisAddIn.Application.Goto(selection);
                Notification.Toast.Show("Tạo file powerautomate thành công!");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Module1.SpeedUpCode(false);
            }
        }

        private void buttonUndo_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {

                Globals.ThisAddIn.Application.Undo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }

        private void buttonTCVNtoUnicode_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Range selection = Globals.ThisAddIn.Application.Selection;
                selection = selection.MinimumRange();
                foreach (Range rng in selection)
                {
                    rng.Value = VSTOLib.Converter.Text.TCVN3ToUnicode(rng.GetValue<string>());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }



        private void buttonHopNhatBangCanDoiPhatSinh_Click(object sender, RibbonControlEventArgs e)
        {
            const string sheetName = "200_FinancialBanlance";

            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                Multiselect = true
            };

            if (openFileDialog.ShowDialog() == DialogResult.Cancel) return;

            string[] fileNames = openFileDialog.FileNames;

            Workbook combinedWorkbook = Globals.ThisAddIn.Application.Workbooks.Add();
            foreach (var fileName in fileNames)
            {
                Workbook workbook = Globals.ThisAddIn.Application.Workbooks.Open(fileName);
                GetDataToCombinedWorkbook(combinedWorkbook, workbook);
            }


            combinedWorkbook.Activate();
            Worksheet sheetCombine = combinedWorkbook.GetSheet("Hopnhat");
            ListObject listObject = sheetCombine.UsedRange.ConvertToListObject();
            listObject.Sort1("TK");
            listObject.Sort2("ZONE");
            listObject.ListColumns["LEN"].Delete();

            listObject.GetColumnRange("NODAUKY").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("CODAUKY").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("NOTRONGKY").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("COTRONGKY").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("NOCUOIKY").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("COCUOIKY").ConvertToValue().SetAccountingNumberFormat();

            Range subtoatalRange = listObject.Range;
            listObject.Unlist();
            subtoatalRange.Subtotal(1, XlConsolidationFunction.xlSum, new int[] { 3, 4, 5, 6, 7, 8 }, true, false, XlSummaryRow.xlSummaryAbove);

            int LR = sheetCombine.LastRow("a");
            for (int i = 3; i <= LR; i++)
            {
                if (sheetCombine.Range["b" + i].Value2 == null)
                {
                    sheetCombine.Range["b" + i].Value2 = sheetCombine.Range["b" + (i + 1)].Value2;
                    sheetCombine.Range["b" + i].EntireRow.Font.Bold = true;
                }
            }

        }

        private void GetDataToCombinedWorkbook(Workbook combinedWorbook, Workbook dataWorkbook)
        {
            const string sheetName = "200_FinancialBanlance";

            Worksheet sheetFinalcialBalance = dataWorkbook.GetSheet(sheetName);
            string zone = sheetFinalcialBalance.Range["a1"].Value2;

            int LR = sheetFinalcialBalance.LastRow("i");
            Worksheet sheetTemp = dataWorkbook.GetNewSheet("Temp");
            sheetFinalcialBalance.Range["a15:a" + (LR - 1)].EntireRow.CopyRawDataTo(sheetTemp.Range["a1"]);
            sheetTemp.Range["A1"].EntireRow.Insert();
            sheetTemp.Range["A1"].SetHeaderText("TK", "TENTK", "NODAUKY", "CODAUKY", "NOTRONGKY", "COTRONGKY", "NOCUOIKY", "COCUOIKY");

            ListObject listObject = sheetTemp.UsedRange.ConvertToListObject();
            listObject.ListColumns.AddRange("LEN", "ZONE");
            listObject.GetColumnRange("LEN").Formula = "=LEN([@TK])";
            listObject.GetColumnRange("LEN").PasteValue();

            listObject.GetColumnRange("ZONE").Value2 = $"{zone}";
            //listObject.DeleteRowFilter("LEN", "<>3");

            Worksheet sheetCombine = combinedWorbook.GetSheet("Hopnhat");
            listObject.HeaderRowRange.Copy(sheetCombine.Range["a1"]);

            listObject.CopyFilterData("LEN", "3", sheetCombine.NewRangeAtColumn("a"), false);
            listObject.Unlist();
            dataWorkbook.Close(false);
        }

        private void buttonHopNhatBCKQKD_Click(object sender, RibbonControlEventArgs e)
        {
            WpfWindowLibrary.Dialogs.MultiOpenFileDialog multiOpenFileDialog = new WpfWindowLibrary.Dialogs.MultiOpenFileDialog();
            multiOpenFileDialog.Description = "Chọn các file báo cáo kết quả kinh doanh kết xuất từ phần mềm Thành Công";
            if (!multiOpenFileDialog.ShowWindow())
            {
                return;
            }

            var files = multiOpenFileDialog.FileNames;
            Workbook mainWorkbook = GetBlankKQKDWorkbook();

            foreach (string fileName in files)
            {
                CopyDataFromKQKD200ToMainWorkbook(fileName, mainWorkbook);
            }

            Worksheet KQKDSheet = mainWorkbook.GetSheet("KQKD");
            string lastDataColumnName = KQKDSheet.LastColumnRangeAtRow(1).GetColumnLetter();

            KQKDSheet.NewRightCellAtColumn(1).Value2 = "TỔNG CỘNG";
            KQKDSheet.NewRightCellAtColumn(1).Value2 = "TỔNG CỘNG";
            KQKDSheet.NewRightCellAtColumn(2).Value2 = "Kỳ này";
            KQKDSheet.NewRightCellAtColumn(2).Value2 = "kỲ trước";
            KQKDSheet.NewRightCellAtColumn(3).Value2 = 4;
            KQKDSheet.NewRightCellAtColumn(3).Value2 = 5;

            KQKDSheet.NewRightCellAtColumn(4).Formula = $"=SUMIF($D$3:${lastDataColumnName}$3,4,D4:{lastDataColumnName}4)";
            KQKDSheet.LastColumnRangeAtRow(4).Resize[19, 1].FillDownFormula(false);

            KQKDSheet.NewRightCellAtColumn(4).Formula = $"=SUMIF($D$3:${lastDataColumnName}$3,5,D4:{lastDataColumnName}4)";
            KQKDSheet.LastColumnRangeAtRow(4).Resize[19, 1].FillDownFormula(false);

            string lastColumnName = KQKDSheet.LastColumnRangeAtRow(1).GetColumnLetter();
            KQKDSheet.GetRange($"D1:{lastColumnName}", "a").SetAccountingNumberFormat().EntireColumn.AutoFit();



        }

        private Workbook GetBlankKQKDWorkbook()
        {
            string baseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
            string KQKD200 = System.IO.Path.Combine(baseDirectory, "Resources", "KQKD200.xlsx");
            Workbook resourceWorkbook = Globals.ThisAddIn.Application.Workbooks.Open(KQKD200);
            Workbook blankWorkbook = Globals.ThisAddIn.Application.Workbooks.Add();

            resourceWorkbook.GetSheet("KQKD").Copy(After: blankWorkbook.GetSheet("Sheet1"));
            resourceWorkbook.Close(false);
            return blankWorkbook;
        }

        private void CopyDataFromKQKD200ToMainWorkbook(string filename, Workbook mainWorkbook)
        {
            Workbook workbook = Globals.ThisAddIn.Application.Workbooks.Open(filename);
            string sheetName = "200_BusinessResult";
            Worksheet worksheet = workbook.GetSheet(sheetName);
            string zone = worksheet.Range["a1"].Value2;

            Worksheet rawSheet = workbook.GetNewSheet("raw");
            worksheet.GetRange("c12:aa", "m").CopyRawDataTo(rawSheet.Range["a1"]);
            rawSheet.UsedRange.ConvertToValue();

            Worksheet KQKDSheet = mainWorkbook.GetSheet("KQKD");
            KQKDSheet.NewRightCellAtColumn(1).Resize[1, 2].Value2 = zone;
            rawSheet.GetRange("d1:e", "a").Copy(KQKDSheet.NewRightCellAtColumn(2));

            workbook.Close(false);
        }

        private void buttonDeleteAllNamedRange_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                foreach (Name namedRange in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
                {
                    try
                    {
                        namedRange.Delete();
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }

        private void buttonDeleteAllShapes_Click(object sender, RibbonControlEventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn xóa tất cả các Shapes trong sheet hiện tại?" + Environment.NewLine + "Thao tác một khi đã thực thi sẽ không thể Undo được!", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            Worksheet activeSheet = Globals.ThisAddIn.Application.ActiveSheet;
            foreach (Shape shape in activeSheet.Shapes)
            {
                try
                {
                    shape.Delete();
                }
                catch { }
            }

            MessageBox.Show("Xóa các shapes thành công!");
        }

        private void buttonChangeVLOOKUPFunction_Click(object sender, RibbonControlEventArgs e)
        {
            Range selection = Globals.ThisAddIn.Application.Selection;

            if (!selection.IsSingleCell())
            {
                MessageBox.Show("Chọn 1 ô thôi", Public.Variables.AddinName);
                return;
            }

            string currentFormula = (string)selection.Formula;
            if (!currentFormula.Contains("VLOOKUP"))
            {
                MessageBox.Show($"Ô {selection.Address} không chứa hàm VLOOKUP", Public.Variables.AddinName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //=VLOOKUP("Apple*",A1:D24,4,FALSE)
            //=VLOOKUP(SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(F2,"~","~~"),"?","~?"),"*","~*"),A1:D24,4,FALSE)
            //string lookupValue = currentFormula.GetStringBetween("VLOOKUP(", ",");

            //chat gpt 
            string lookupValue = ExtractLookupValue(currentFormula);

            string substitute1 = "SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(";
            string substitute2 = ",\"~\",\"~~\"),\"?\",\"~?\"),\"*\",\"~*\")";
            if (currentFormula.Contains(substitute1) || lookupValue.Contains(substitute1) || lookupValue.Contains(substitute2))
            {
                DialogResult dialogResult = MessageBox.Show($"Trong hàm VLOOKUP đã có SUBSTITUE để tìm kiếm chính xác rồi"
                    + Environment.NewLine + "Bạn có muốn tiếp tục?",
                    Public.Variables.AddinName,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button2);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }

            string newLookupValue = "SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(" + lookupValue + ",\"~\",\"~~\"),\"?\",\"~?\"),\"*\",\"~*\")";
            string newFormula = currentFormula.Replace(lookupValue, newLookupValue);
            selection.Formula = newFormula;
            //MessageBox.Show(looupValue);

            string ExtractLookupValue(string formula)
            {
                string pattern = @"VLOOKUP\(([^,]+),";
                Match match = Regex.Match(formula, pattern);
                return match.Groups[1].Value;
            }
        }

        private void buttonCheckNumberArrayIsContinousNumericalSeries_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Range selection = Globals.ThisAddIn.Application.Selection;
                selection = selection.MinimumRange();
                selection.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                object[,] values = selection.Value;
                List<object> listInteger = new List<object>();
                List<object> listString = new List<object>();

                for (int i = 0; i < values.GetLength(0); i++)
                {
                    for (int j = 0; j < values.GetLength(1); j++)
                    {
                        object cellValue = values[i + 1, j + 1];
                        if (cellValue == null)
                        {
                            continue;
                        }

                        if (cellValue.IsInteger())
                        {
                            listInteger.Add(cellValue);
                        }
                        else
                        {
                            listString.Add(cellValue);
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Public.Variables.AddinName);
            }

            //local

        }

        private void buttonCheckInstallStatus_Click(object sender, RibbonControlEventArgs e)
        {
            string msg = "Cài đặt XLL64: " + xllRegister;
            MessageBox.Show(msg);
        }

        private void buttonDeleteAllBlankColumns_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet worksheet = Globals.ThisAddIn.Application.ActiveSheet;
            Range lastCell = worksheet.Cells.Find(
                "*",
                Type.Missing,
                XlFindLookIn.xlFormulas,
                XlLookAt.xlPart,
                XlSearchOrder.xlByColumns,
                XlSearchDirection.xlPrevious,
                Type.Missing,
                Type.Missing,
                Type.Missing);

            int lastColumn = lastCell.Column;
            for (int i = lastColumn; i >= 1; i--)
            {
                Range column = worksheet.Columns[i];
                if (Globals.ThisAddIn.Application.WorksheetFunction.CountA(column) == 0)
                {
                    column.Delete();
                }
            }
        }
    }
}

