﻿
namespace ExcelAddIn1
{
    partial class Ribbon1 : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon1()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ribbon1));
            this.tab2 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.ButtonShowTaskPane = this.Factory.CreateRibbonButton();
            this.buttonOpenFormNhap = this.Factory.CreateRibbonButton();
            this.ButtonShowFormCSDL = this.Factory.CreateRibbonButton();
            this.group5 = this.Factory.CreateRibbonGroup();
            this.menu4 = this.Factory.CreateRibbonMenu();
            this.buttonViewAccountStatementTechcombank = this.Factory.CreateRibbonButton();
            this.button_Auto_HachToanGDTheTECH = this.Factory.CreateRibbonButton();
            this.button2HachToanGDTheTechChiTietTungGD = this.Factory.CreateRibbonButton();
            this.menu5 = this.Factory.CreateRibbonMenu();
            this.buttonViewAccountStatementVietcombank = this.Factory.CreateRibbonButton();
            this.button_Auto_HachToanGDTheVCB = this.Factory.CreateRibbonButton();
            this.group3 = this.Factory.CreateRibbonGroup();
            this.menu9 = this.Factory.CreateRibbonMenu();
            this.buttonGopFileExcel = this.Factory.CreateRibbonButton();
            this.buttonDeleteAllNamedRange = this.Factory.CreateRibbonButton();
            this.menu15 = this.Factory.CreateRibbonMenu();
            this.buttonDeleteAllShapes = this.Factory.CreateRibbonButton();
            this.menu2 = this.Factory.CreateRibbonMenu();
            this.buttonPasteToNewSheet = this.Factory.CreateRibbonButton();
            this.buttonConvertRangeToValue = this.Factory.CreateRibbonButton();
            this.buttonAddCharacterToDisplayAsText = this.Factory.CreateRibbonButton();
            this.buttonAddBlankRowAtValueChange = this.Factory.CreateRibbonButton();
            this.buttonCompareTwoRanges = this.Factory.CreateRibbonButton();
            this.buttonMergeInColumns = this.Factory.CreateRibbonButton();
            this.menu1 = this.Factory.CreateRibbonMenu();
            this.buttonExactNumberFromString = this.Factory.CreateRibbonButton();
            this.menuChangeCapital = this.Factory.CreateRibbonMenu();
            this.buttonCapitalizeFirstChar = this.Factory.CreateRibbonButton();
            this.buttonCapitalizeFirstCharEachWord = this.Factory.CreateRibbonButton();
            this.buttonCapitalizeAll = this.Factory.CreateRibbonButton();
            this.buttonLowerAll = this.Factory.CreateRibbonButton();
            this.buttonTCVNtoUnicode = this.Factory.CreateRibbonButton();
            this.splitButtonSetNumberFomat = this.Factory.CreateRibbonSplitButton();
            this.buttonCheckNumberArrayIsContinousNumericalSeries = this.Factory.CreateRibbonButton();
            this.group7 = this.Factory.CreateRibbonGroup();
            this.button_ShowAdvanceFilterPane = this.Factory.CreateRibbonButton();
            this.menu16 = this.Factory.CreateRibbonMenu();
            this.buttonChangeVLOOKUPFunction = this.Factory.CreateRibbonButton();
            this.group4 = this.Factory.CreateRibbonGroup();
            this.menu3 = this.Factory.CreateRibbonMenu();
            this.buttonMergePDF2sidePrint = this.Factory.CreateRibbonButton();
            this.buttonMergePDFcontain1page = this.Factory.CreateRibbonButton();
            this.buttonMergePDF2page2sidePrint = this.Factory.CreateRibbonButton();
            this.group9 = this.Factory.CreateRibbonGroup();
            this.menu11 = this.Factory.CreateRibbonMenu();
            this.menu13 = this.Factory.CreateRibbonMenu();
            this.buttonTranformExcelToMInvoiceTemplate = this.Factory.CreateRibbonButton();
            this.menu14 = this.Factory.CreateRibbonMenu();
            this.buttonHopNhatBangCanDoiPhatSinh = this.Factory.CreateRibbonButton();
            this.buttonHopNhatBCKQKD = this.Factory.CreateRibbonButton();
            this.group6 = this.Factory.CreateRibbonGroup();
            this.menu10 = this.Factory.CreateRibbonMenu();
            this.buttonCreateFilePulove = this.Factory.CreateRibbonButton();
            this.buttonCreatePowerAutomateTxtFile = this.Factory.CreateRibbonButton();
            this.menu8 = this.Factory.CreateRibbonMenu();
            this.buttonTurnOffSpeedUpCode = this.Factory.CreateRibbonButton();
            this.buttonStopWinAppDriver = this.Factory.CreateRibbonButton();
            this.buttonOpenStartupPath = this.Factory.CreateRibbonButton();
            this.buttonTestToastForm = this.Factory.CreateRibbonButton();
            this.buttonTestWaitForm = this.Factory.CreateRibbonButton();
            this.buttonTestCancel = this.Factory.CreateRibbonButton();
            this.menu7 = this.Factory.CreateRibbonMenu();
            this.buttonTestSignInVIETSEA = this.Factory.CreateRibbonButton();
            this.menu6 = this.Factory.CreateRibbonMenu();
            this.buttonTestSignInThanhCong = this.Factory.CreateRibbonButton();
            this.button1 = this.Factory.CreateRibbonButton();
            this.buttonTestXuatBanThanhCong = this.Factory.CreateRibbonButton();
            this.buttonTestWriteDataToGoogleSheet = this.Factory.CreateRibbonButton();
            this.buttonShowWADLocation = this.Factory.CreateRibbonButton();
            this.buttonTestWaitingForm = this.Factory.CreateRibbonButton();
            this.buttonTestExportPDF = this.Factory.CreateRibbonButton();
            this.buttonUndo = this.Factory.CreateRibbonButton();
            this.buttonShowClipboard = this.Factory.CreateRibbonButton();
            this.button2 = this.Factory.CreateRibbonButton();
            this.buttonTestConvert = this.Factory.CreateRibbonButton();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.ButtonShowFormSetting = this.Factory.CreateRibbonButton();
            this.buttonShowXtraFormSetting = this.Factory.CreateRibbonButton();
            this.toggleButtonPinExcel = this.Factory.CreateRibbonToggleButton();
            this.group8 = this.Factory.CreateRibbonGroup();
            this.buttonUpdateAddin = this.Factory.CreateRibbonButton();
            this.menu12 = this.Factory.CreateRibbonMenu();
            this.buttonCheckInstallStatus = this.Factory.CreateRibbonButton();
            this.buttonDeleteAllBlankColumns = this.Factory.CreateRibbonButton();
            this.tab2.SuspendLayout();
            this.group1.SuspendLayout();
            this.group5.SuspendLayout();
            this.group3.SuspendLayout();
            this.group7.SuspendLayout();
            this.group4.SuspendLayout();
            this.group9.SuspendLayout();
            this.group6.SuspendLayout();
            this.group2.SuspendLayout();
            this.group8.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab2
            // 
            this.tab2.Groups.Add(this.group1);
            this.tab2.Groups.Add(this.group5);
            this.tab2.Groups.Add(this.group3);
            this.tab2.Groups.Add(this.group7);
            this.tab2.Groups.Add(this.group4);
            this.tab2.Groups.Add(this.group9);
            this.tab2.Groups.Add(this.group6);
            this.tab2.Groups.Add(this.group2);
            this.tab2.Groups.Add(this.group8);
            this.tab2.Label = "ExcelAddin1";
            this.tab2.Name = "tab2";
            // 
            // group1
            // 
            this.group1.Items.Add(this.ButtonShowTaskPane);
            this.group1.Items.Add(this.buttonOpenFormNhap);
            this.group1.Items.Add(this.ButtonShowFormCSDL);
            this.group1.Label = "CSDL";
            this.group1.Name = "group1";
            // 
            // ButtonShowTaskPane
            // 
            this.ButtonShowTaskPane.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.ButtonShowTaskPane.Image = global::ExcelAddIn1.Properties.Resources.icons8_open_pane_100px;
            this.ButtonShowTaskPane.Label = "Form nhập";
            this.ButtonShowTaskPane.Name = "ButtonShowTaskPane";
            this.ButtonShowTaskPane.ShowImage = true;
            this.ButtonShowTaskPane.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ButtonShowTaskPane_Click_1);
            // 
            // buttonOpenFormNhap
            // 
            this.buttonOpenFormNhap.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonOpenFormNhap.Image = global::ExcelAddIn1.Properties.Resources.icons8_form_80px;
            this.buttonOpenFormNhap.Label = "Form nhập 2.0";
            this.buttonOpenFormNhap.Name = "buttonOpenFormNhap";
            this.buttonOpenFormNhap.ShowImage = true;
            this.buttonOpenFormNhap.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonOpenFormNhap_Click);
            // 
            // ButtonShowFormCSDL
            // 
            this.ButtonShowFormCSDL.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.ButtonShowFormCSDL.Image = global::ExcelAddIn1.Properties.Resources.icons8_form_64px;
            this.ButtonShowFormCSDL.Label = "Form xuất";
            this.ButtonShowFormCSDL.Name = "ButtonShowFormCSDL";
            this.ButtonShowFormCSDL.ShowImage = true;
            this.ButtonShowFormCSDL.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ButtonShowFormCSDL_Click_1);
            // 
            // group5
            // 
            this.group5.Items.Add(this.menu4);
            this.group5.Items.Add(this.menu5);
            this.group5.Label = "Ngân hàng";
            this.group5.Name = "group5";
            // 
            // menu4
            // 
            this.menu4.Image = global::ExcelAddIn1.Properties.Resources.techcombank;
            this.menu4.Items.Add(this.buttonViewAccountStatementTechcombank);
            this.menu4.Items.Add(this.button_Auto_HachToanGDTheTECH);
            this.menu4.Items.Add(this.button2HachToanGDTheTechChiTietTungGD);
            this.menu4.Label = "TECH";
            this.menu4.Name = "menu4";
            this.menu4.ShowImage = true;
            // 
            // buttonViewAccountStatementTechcombank
            // 
            this.buttonViewAccountStatementTechcombank.Label = "Tính giao dịch thẻ";
            this.buttonViewAccountStatementTechcombank.Name = "buttonViewAccountStatementTechcombank";
            this.buttonViewAccountStatementTechcombank.ShowImage = true;
            this.buttonViewAccountStatementTechcombank.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonViewAccountStatementTechcombank_Click_1);
            // 
            // button_Auto_HachToanGDTheTECH
            // 
            this.button_Auto_HachToanGDTheTECH.Label = "Hạch toán giao dịch thẻ TECH một ngày tự động (VIETSEA)";
            this.button_Auto_HachToanGDTheTECH.Name = "button_Auto_HachToanGDTheTECH";
            this.button_Auto_HachToanGDTheTECH.ShowImage = true;
            this.button_Auto_HachToanGDTheTECH.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button_Auto_HachToanGDTheTECH_Click_1);
            // 
            // button2HachToanGDTheTechChiTietTungGD
            // 
            this.button2HachToanGDTheTechChiTietTungGD.Label = "button2";
            this.button2HachToanGDTheTechChiTietTungGD.Name = "button2HachToanGDTheTechChiTietTungGD";
            this.button2HachToanGDTheTechChiTietTungGD.ShowImage = true;
            this.button2HachToanGDTheTechChiTietTungGD.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button2HachToanGDTheTechChiTietTungGD_Click);
            // 
            // menu5
            // 
            this.menu5.Image = global::ExcelAddIn1.Properties.Resources.vietcombank;
            this.menu5.Items.Add(this.buttonViewAccountStatementVietcombank);
            this.menu5.Items.Add(this.button_Auto_HachToanGDTheVCB);
            this.menu5.Label = "VCB";
            this.menu5.Name = "menu5";
            this.menu5.ShowImage = true;
            // 
            // buttonViewAccountStatementVietcombank
            // 
            this.buttonViewAccountStatementVietcombank.Label = "Sao kê VCB";
            this.buttonViewAccountStatementVietcombank.Name = "buttonViewAccountStatementVietcombank";
            this.buttonViewAccountStatementVietcombank.ShowImage = true;
            this.buttonViewAccountStatementVietcombank.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonViewAccountStatementVietcombank_Click_1);
            // 
            // button_Auto_HachToanGDTheVCB
            // 
            this.button_Auto_HachToanGDTheVCB.Label = "Hạch toán giao dịch thẻ VCB một ngày tự động (VIETSEA)";
            this.button_Auto_HachToanGDTheVCB.Name = "button_Auto_HachToanGDTheVCB";
            this.button_Auto_HachToanGDTheVCB.ShowImage = true;
            this.button_Auto_HachToanGDTheVCB.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button_Auto_HachToanGDTheVCB_Click_1);
            // 
            // group3
            // 
            this.group3.Items.Add(this.menu9);
            this.group3.Items.Add(this.menu15);
            this.group3.Items.Add(this.menu2);
            this.group3.Items.Add(this.menu1);
            this.group3.Items.Add(this.splitButtonSetNumberFomat);
            this.group3.Label = "Tools";
            this.group3.Name = "group3";
            // 
            // menu9
            // 
            this.menu9.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.menu9.Image = global::ExcelAddIn1.Properties.Resources.microsoft_excel_2013_icon_10;
            this.menu9.Items.Add(this.buttonGopFileExcel);
            this.menu9.Items.Add(this.buttonDeleteAllNamedRange);
            this.menu9.Label = "Workbook";
            this.menu9.Name = "menu9";
            this.menu9.ShowImage = true;
            // 
            // buttonGopFileExcel
            // 
            this.buttonGopFileExcel.Label = "Gộp file Excel";
            this.buttonGopFileExcel.Name = "buttonGopFileExcel";
            this.buttonGopFileExcel.ShowImage = true;
            this.buttonGopFileExcel.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonGopFileExcel_Click);
            // 
            // buttonDeleteAllNamedRange
            // 
            this.buttonDeleteAllNamedRange.Label = "Xóa tất cả NamedRange";
            this.buttonDeleteAllNamedRange.Name = "buttonDeleteAllNamedRange";
            this.buttonDeleteAllNamedRange.ShowImage = true;
            this.buttonDeleteAllNamedRange.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonDeleteAllNamedRange_Click);
            // 
            // menu15
            // 
            this.menu15.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.menu15.Items.Add(this.buttonDeleteAllShapes);
            this.menu15.Items.Add(this.buttonDeleteAllBlankColumns);
            this.menu15.Label = "Worksheet";
            this.menu15.Name = "menu15";
            this.menu15.ShowImage = true;
            // 
            // buttonDeleteAllShapes
            // 
            this.buttonDeleteAllShapes.Label = "Xóa tất cả Shapes";
            this.buttonDeleteAllShapes.Name = "buttonDeleteAllShapes";
            this.buttonDeleteAllShapes.ShowImage = true;
            this.buttonDeleteAllShapes.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonDeleteAllShapes_Click);
            // 
            // menu2
            // 
            this.menu2.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.menu2.Image = global::ExcelAddIn1.Properties.Resources.icons8_data_sheet_40px;
            this.menu2.Items.Add(this.buttonPasteToNewSheet);
            this.menu2.Items.Add(this.buttonConvertRangeToValue);
            this.menu2.Items.Add(this.buttonAddCharacterToDisplayAsText);
            this.menu2.Items.Add(this.buttonAddBlankRowAtValueChange);
            this.menu2.Items.Add(this.buttonCompareTwoRanges);
            this.menu2.Items.Add(this.buttonMergeInColumns);
            this.menu2.Label = "Range";
            this.menu2.Name = "menu2";
            this.menu2.ShowImage = true;
            // 
            // buttonPasteToNewSheet
            // 
            this.buttonPasteToNewSheet.Label = "Paste sang sheet mới";
            this.buttonPasteToNewSheet.Name = "buttonPasteToNewSheet";
            this.buttonPasteToNewSheet.ShowImage = true;
            this.buttonPasteToNewSheet.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonPasteToNewSheet_Click);
            // 
            // buttonConvertRangeToValue
            // 
            this.buttonConvertRangeToValue.Label = "Paste Value";
            this.buttonConvertRangeToValue.Name = "buttonConvertRangeToValue";
            this.buttonConvertRangeToValue.ShowImage = true;
            this.buttonConvertRangeToValue.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonConvertRangeToValue_Click);
            // 
            // buttonAddCharacterToDisplayAsText
            // 
            this.buttonAddCharacterToDisplayAsText.Label = "Thêm \' vào trước dữ liệu";
            this.buttonAddCharacterToDisplayAsText.Name = "buttonAddCharacterToDisplayAsText";
            this.buttonAddCharacterToDisplayAsText.ShowImage = true;
            this.buttonAddCharacterToDisplayAsText.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonAddCharacterToDisplayAsText_Click);
            // 
            // buttonAddBlankRowAtValueChange
            // 
            this.buttonAddBlankRowAtValueChange.Label = "Thêm dòng trống khi dữ liệu thay đổi";
            this.buttonAddBlankRowAtValueChange.Name = "buttonAddBlankRowAtValueChange";
            this.buttonAddBlankRowAtValueChange.ShowImage = true;
            this.buttonAddBlankRowAtValueChange.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonAddBlankRowAtValueChange_Click);
            // 
            // buttonCompareTwoRanges
            // 
            this.buttonCompareTwoRanges.Label = "Đối chiếu 2 dữ liệu";
            this.buttonCompareTwoRanges.Name = "buttonCompareTwoRanges";
            this.buttonCompareTwoRanges.ShowImage = true;
            this.buttonCompareTwoRanges.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonCompareTwoRanges_Click);
            // 
            // buttonMergeInColumns
            // 
            this.buttonMergeInColumns.Label = "Merge theo từng cột";
            this.buttonMergeInColumns.Name = "buttonMergeInColumns";
            this.buttonMergeInColumns.ShowImage = true;
            this.buttonMergeInColumns.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonMergeInColumns_Click);
            // 
            // menu1
            // 
            this.menu1.Image = global::ExcelAddIn1.Properties.Resources.cubes;
            this.menu1.Items.Add(this.buttonExactNumberFromString);
            this.menu1.Items.Add(this.menuChangeCapital);
            this.menu1.Items.Add(this.buttonTCVNtoUnicode);
            this.menu1.Label = "Xử lý chuỗi";
            this.menu1.Name = "menu1";
            this.menu1.ShowImage = true;
            // 
            // buttonExactNumberFromString
            // 
            this.buttonExactNumberFromString.Label = "Tách số khỏi chuỗi";
            this.buttonExactNumberFromString.Name = "buttonExactNumberFromString";
            this.buttonExactNumberFromString.ShowImage = true;
            this.buttonExactNumberFromString.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonExactNumberFromString_Click);
            // 
            // menuChangeCapital
            // 
            this.menuChangeCapital.Items.Add(this.buttonCapitalizeFirstChar);
            this.menuChangeCapital.Items.Add(this.buttonCapitalizeFirstCharEachWord);
            this.menuChangeCapital.Items.Add(this.buttonCapitalizeAll);
            this.menuChangeCapital.Items.Add(this.buttonLowerAll);
            this.menuChangeCapital.Label = "Đổi chữ hoa - thường";
            this.menuChangeCapital.Name = "menuChangeCapital";
            this.menuChangeCapital.ShowImage = true;
            // 
            // buttonCapitalizeFirstChar
            // 
            this.buttonCapitalizeFirstChar.Label = "Viết hoa chữ cái đầu tiên";
            this.buttonCapitalizeFirstChar.Name = "buttonCapitalizeFirstChar";
            this.buttonCapitalizeFirstChar.ShowImage = true;
            this.buttonCapitalizeFirstChar.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonCapitalizeFirstChar_Click);
            // 
            // buttonCapitalizeFirstCharEachWord
            // 
            this.buttonCapitalizeFirstCharEachWord.Label = "Viết hoa chữ cái đầu tiên mỗi từ";
            this.buttonCapitalizeFirstCharEachWord.Name = "buttonCapitalizeFirstCharEachWord";
            this.buttonCapitalizeFirstCharEachWord.ShowImage = true;
            this.buttonCapitalizeFirstCharEachWord.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonCapitalizeFirstCharEachWord_Click);
            // 
            // buttonCapitalizeAll
            // 
            this.buttonCapitalizeAll.Label = "Viết hoa tất cả";
            this.buttonCapitalizeAll.Name = "buttonCapitalizeAll";
            this.buttonCapitalizeAll.ShowImage = true;
            this.buttonCapitalizeAll.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonCapitalizeAll_Click);
            // 
            // buttonLowerAll
            // 
            this.buttonLowerAll.Label = "Viết thường tất cả";
            this.buttonLowerAll.Name = "buttonLowerAll";
            this.buttonLowerAll.ShowImage = true;
            this.buttonLowerAll.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonLowerAll_Click);
            // 
            // buttonTCVNtoUnicode
            // 
            this.buttonTCVNtoUnicode.Label = "Chuyển mã TCVN sang Unicode";
            this.buttonTCVNtoUnicode.Name = "buttonTCVNtoUnicode";
            this.buttonTCVNtoUnicode.ShowImage = true;
            this.buttonTCVNtoUnicode.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTCVNtoUnicode_Click);
            // 
            // splitButtonSetNumberFomat
            // 
            this.splitButtonSetNumberFomat.Items.Add(this.buttonCheckNumberArrayIsContinousNumericalSeries);
            this.splitButtonSetNumberFomat.Label = "Định dạng số";
            this.splitButtonSetNumberFomat.Name = "splitButtonSetNumberFomat";
            this.splitButtonSetNumberFomat.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.splitButtonSetNumberFomat_Click);
            // 
            // buttonCheckNumberArrayIsContinousNumericalSeries
            // 
            this.buttonCheckNumberArrayIsContinousNumericalSeries.Label = "button3";
            this.buttonCheckNumberArrayIsContinousNumericalSeries.Name = "buttonCheckNumberArrayIsContinousNumericalSeries";
            this.buttonCheckNumberArrayIsContinousNumericalSeries.ShowImage = true;
            this.buttonCheckNumberArrayIsContinousNumericalSeries.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonCheckNumberArrayIsContinousNumericalSeries_Click);
            // 
            // group7
            // 
            this.group7.Items.Add(this.button_ShowAdvanceFilterPane);
            this.group7.Items.Add(this.menu16);
            this.group7.Label = "Data";
            this.group7.Name = "group7";
            // 
            // button_ShowAdvanceFilterPane
            // 
            this.button_ShowAdvanceFilterPane.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.button_ShowAdvanceFilterPane.Image = global::ExcelAddIn1.Properties.Resources.icons8_filter_edit_40px_2;
            this.button_ShowAdvanceFilterPane.Label = "Lọc nâng cao";
            this.button_ShowAdvanceFilterPane.Name = "button_ShowAdvanceFilterPane";
            this.button_ShowAdvanceFilterPane.ShowImage = true;
            this.button_ShowAdvanceFilterPane.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button_ShowAdvanceFilterPane_Click);
            // 
            // menu16
            // 
            this.menu16.Items.Add(this.buttonChangeVLOOKUPFunction);
            this.menu16.Label = "Hàm Excel";
            this.menu16.Name = "menu16";
            // 
            // buttonChangeVLOOKUPFunction
            // 
            this.buttonChangeVLOOKUPFunction.Label = "Convert hàm VLOOKUP sang tìm kiếm chính xác";
            this.buttonChangeVLOOKUPFunction.Name = "buttonChangeVLOOKUPFunction";
            this.buttonChangeVLOOKUPFunction.ShowImage = true;
            this.buttonChangeVLOOKUPFunction.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonChangeVLOOKUPFunction_Click);
            // 
            // group4
            // 
            this.group4.Items.Add(this.menu3);
            this.group4.Label = "PDF Tools";
            this.group4.Name = "group4";
            // 
            // menu3
            // 
            this.menu3.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.menu3.Image = ((System.Drawing.Image)(resources.GetObject("menu3.Image")));
            this.menu3.Items.Add(this.buttonMergePDF2sidePrint);
            this.menu3.Items.Add(this.buttonMergePDFcontain1page);
            this.menu3.Items.Add(this.buttonMergePDF2page2sidePrint);
            this.menu3.Label = "Ghép PDF";
            this.menu3.Name = "menu3";
            this.menu3.ShowImage = true;
            // 
            // buttonMergePDF2sidePrint
            // 
            this.buttonMergePDF2sidePrint.Label = "Ghép file PDF in 2 mặt";
            this.buttonMergePDF2sidePrint.Name = "buttonMergePDF2sidePrint";
            this.buttonMergePDF2sidePrint.ShowImage = true;
            this.buttonMergePDF2sidePrint.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonMergePDF2sidePrint_Click);
            // 
            // buttonMergePDFcontain1page
            // 
            this.buttonMergePDFcontain1page.Label = "Ghép PDF 1 trang";
            this.buttonMergePDFcontain1page.Name = "buttonMergePDFcontain1page";
            this.buttonMergePDFcontain1page.ShowImage = true;
            this.buttonMergePDFcontain1page.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonMergePDFcontain1page_Click);
            // 
            // buttonMergePDF2page2sidePrint
            // 
            this.buttonMergePDF2page2sidePrint.Label = "Ghép PDF nhiều trang in 2 mặt";
            this.buttonMergePDF2page2sidePrint.Name = "buttonMergePDF2page2sidePrint";
            this.buttonMergePDF2page2sidePrint.ShowImage = true;
            this.buttonMergePDF2page2sidePrint.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonMergePDF2page2sidePrint_Click);
            // 
            // group9
            // 
            this.group9.Items.Add(this.menu11);
            this.group9.Items.Add(this.menu14);
            this.group9.Label = "Khác";
            this.group9.Name = "group9";
            // 
            // menu11
            // 
            this.menu11.Items.Add(this.menu13);
            this.menu11.Label = "DN Khác";
            this.menu11.Name = "menu11";
            this.menu11.ShowImage = true;
            // 
            // menu13
            // 
            this.menu13.Items.Add(this.buttonTranformExcelToMInvoiceTemplate);
            this.menu13.Label = "Minh Quang";
            this.menu13.Name = "menu13";
            this.menu13.ShowImage = true;
            // 
            // buttonTranformExcelToMInvoiceTemplate
            // 
            this.buttonTranformExcelToMInvoiceTemplate.Label = "Chuyển file import M-Invoice";
            this.buttonTranformExcelToMInvoiceTemplate.Name = "buttonTranformExcelToMInvoiceTemplate";
            this.buttonTranformExcelToMInvoiceTemplate.ShowImage = true;
            this.buttonTranformExcelToMInvoiceTemplate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTranformExcelToMInvoiceTemplate_Click);
            // 
            // menu14
            // 
            this.menu14.Items.Add(this.buttonHopNhatBangCanDoiPhatSinh);
            this.menu14.Items.Add(this.buttonHopNhatBCKQKD);
            this.menu14.Label = "Báo cáo nội bộ";
            this.menu14.Name = "menu14";
            this.menu14.ShowImage = true;
            // 
            // buttonHopNhatBangCanDoiPhatSinh
            // 
            this.buttonHopNhatBangCanDoiPhatSinh.Label = "Hợp nhất bảng cân đối phát sinh";
            this.buttonHopNhatBangCanDoiPhatSinh.Name = "buttonHopNhatBangCanDoiPhatSinh";
            this.buttonHopNhatBangCanDoiPhatSinh.ShowImage = true;
            this.buttonHopNhatBangCanDoiPhatSinh.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonHopNhatBangCanDoiPhatSinh_Click);
            // 
            // buttonHopNhatBCKQKD
            // 
            this.buttonHopNhatBCKQKD.Label = "Hợp nhất báo cáo KQKD";
            this.buttonHopNhatBCKQKD.Name = "buttonHopNhatBCKQKD";
            this.buttonHopNhatBCKQKD.ShowImage = true;
            this.buttonHopNhatBCKQKD.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonHopNhatBCKQKD_Click);
            // 
            // group6
            // 
            this.group6.Items.Add(this.menu10);
            this.group6.Items.Add(this.menu8);
            this.group6.Label = "Auto Test";
            this.group6.Name = "group6";
            // 
            // menu10
            // 
            this.menu10.Items.Add(this.buttonCreateFilePulove);
            this.menu10.Items.Add(this.buttonCreatePowerAutomateTxtFile);
            this.menu10.Label = "menu10";
            this.menu10.Name = "menu10";
            // 
            // buttonCreateFilePulove
            // 
            this.buttonCreateFilePulove.Image = global::ExcelAddIn1.Properties.Resources.pulovers_macro_creator100_100;
            this.buttonCreateFilePulove.Label = "Tạo file pulove.txt";
            this.buttonCreateFilePulove.Name = "buttonCreateFilePulove";
            this.buttonCreateFilePulove.ShowImage = true;
            this.buttonCreateFilePulove.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonCreateFilePulove_Click);
            // 
            // buttonCreatePowerAutomateTxtFile
            // 
            this.buttonCreatePowerAutomateTxtFile.Label = "Tạo file power automate";
            this.buttonCreatePowerAutomateTxtFile.Name = "buttonCreatePowerAutomateTxtFile";
            this.buttonCreatePowerAutomateTxtFile.ShowImage = true;
            this.buttonCreatePowerAutomateTxtFile.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonCreatePowerAutomateTxtFile_Click);
            // 
            // menu8
            // 
            this.menu8.Items.Add(this.buttonTurnOffSpeedUpCode);
            this.menu8.Items.Add(this.buttonStopWinAppDriver);
            this.menu8.Items.Add(this.buttonOpenStartupPath);
            this.menu8.Items.Add(this.buttonTestToastForm);
            this.menu8.Items.Add(this.buttonTestWaitForm);
            this.menu8.Items.Add(this.buttonTestCancel);
            this.menu8.Items.Add(this.menu7);
            this.menu8.Items.Add(this.menu6);
            this.menu8.Items.Add(this.buttonTestWriteDataToGoogleSheet);
            this.menu8.Items.Add(this.buttonShowWADLocation);
            this.menu8.Items.Add(this.buttonTestWaitingForm);
            this.menu8.Items.Add(this.buttonTestExportPDF);
            this.menu8.Items.Add(this.buttonUndo);
            this.menu8.Items.Add(this.buttonShowClipboard);
            this.menu8.Items.Add(this.button2);
            this.menu8.Items.Add(this.buttonTestConvert);
            this.menu8.Label = "Testing";
            this.menu8.Name = "menu8";
            this.menu8.ShowImage = true;
            // 
            // buttonTurnOffSpeedUpCode
            // 
            this.buttonTurnOffSpeedUpCode.Label = "Fix lỗi đơ màn hình";
            this.buttonTurnOffSpeedUpCode.Name = "buttonTurnOffSpeedUpCode";
            this.buttonTurnOffSpeedUpCode.ShowImage = true;
            this.buttonTurnOffSpeedUpCode.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTurnOffSpeedUpCode_Click);
            // 
            // buttonStopWinAppDriver
            // 
            this.buttonStopWinAppDriver.Image = global::ExcelAddIn1.Properties.Resources.icons8_stop_sign_80px_2;
            this.buttonStopWinAppDriver.Label = "Stop";
            this.buttonStopWinAppDriver.Name = "buttonStopWinAppDriver";
            this.buttonStopWinAppDriver.ShowImage = true;
            this.buttonStopWinAppDriver.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonStopWinAppDriver_Click);
            // 
            // buttonOpenStartupPath
            // 
            this.buttonOpenStartupPath.Label = "Open Startup Path";
            this.buttonOpenStartupPath.Name = "buttonOpenStartupPath";
            this.buttonOpenStartupPath.ShowImage = true;
            this.buttonOpenStartupPath.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonOpenStartupPath_Click);
            // 
            // buttonTestToastForm
            // 
            this.buttonTestToastForm.Label = "Test Toast";
            this.buttonTestToastForm.Name = "buttonTestToastForm";
            this.buttonTestToastForm.ShowImage = true;
            this.buttonTestToastForm.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTestToastForm_Click);
            // 
            // buttonTestWaitForm
            // 
            this.buttonTestWaitForm.Label = "Test wait";
            this.buttonTestWaitForm.Name = "buttonTestWaitForm";
            this.buttonTestWaitForm.ShowImage = true;
            this.buttonTestWaitForm.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTestWaitForm_Click);
            // 
            // buttonTestCancel
            // 
            this.buttonTestCancel.Label = "Test cancel";
            this.buttonTestCancel.Name = "buttonTestCancel";
            this.buttonTestCancel.ShowImage = true;
            this.buttonTestCancel.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTestCancel_Click);
            // 
            // menu7
            // 
            this.menu7.Items.Add(this.buttonTestSignInVIETSEA);
            this.menu7.Label = "Test Vietsea";
            this.menu7.Name = "menu7";
            this.menu7.ShowImage = true;
            // 
            // buttonTestSignInVIETSEA
            // 
            this.buttonTestSignInVIETSEA.Label = "Test đăng nhập Vietsea";
            this.buttonTestSignInVIETSEA.Name = "buttonTestSignInVIETSEA";
            this.buttonTestSignInVIETSEA.ShowImage = true;
            this.buttonTestSignInVIETSEA.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTestSignInVIETSEA_Click);
            // 
            // menu6
            // 
            this.menu6.Items.Add(this.buttonTestSignInThanhCong);
            this.menu6.Items.Add(this.button1);
            this.menu6.Items.Add(this.buttonTestXuatBanThanhCong);
            this.menu6.Label = "Test Thành Công";
            this.menu6.Name = "menu6";
            this.menu6.ShowImage = true;
            // 
            // buttonTestSignInThanhCong
            // 
            this.buttonTestSignInThanhCong.Label = "Test đăng nhập Thành Công";
            this.buttonTestSignInThanhCong.Name = "buttonTestSignInThanhCong";
            this.buttonTestSignInThanhCong.ShowImage = true;
            this.buttonTestSignInThanhCong.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTestSignInThanhCong_Click);
            // 
            // button1
            // 
            this.button1.Label = "Test nhập hàng Thành Công";
            this.button1.Name = "button1";
            this.button1.ShowImage = true;
            this.button1.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button1_Click_1);
            // 
            // buttonTestXuatBanThanhCong
            // 
            this.buttonTestXuatBanThanhCong.Label = "Test dồng bộ xuất hàng";
            this.buttonTestXuatBanThanhCong.Name = "buttonTestXuatBanThanhCong";
            this.buttonTestXuatBanThanhCong.ShowImage = true;
            this.buttonTestXuatBanThanhCong.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTestXuatBanThanhCong_Click);
            // 
            // buttonTestWriteDataToGoogleSheet
            // 
            this.buttonTestWriteDataToGoogleSheet.Label = "test ghi dữ liệu lên google sheet";
            this.buttonTestWriteDataToGoogleSheet.Name = "buttonTestWriteDataToGoogleSheet";
            this.buttonTestWriteDataToGoogleSheet.ShowImage = true;
            this.buttonTestWriteDataToGoogleSheet.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTestWriteDataToGoogleSheet_Click);
            // 
            // buttonShowWADLocation
            // 
            this.buttonShowWADLocation.Label = "Show WAD folder";
            this.buttonShowWADLocation.Name = "buttonShowWADLocation";
            this.buttonShowWADLocation.ShowImage = true;
            this.buttonShowWADLocation.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonShowWADLocation_Click);
            // 
            // buttonTestWaitingForm
            // 
            this.buttonTestWaitingForm.Label = "Test waiting form";
            this.buttonTestWaitingForm.Name = "buttonTestWaitingForm";
            this.buttonTestWaitingForm.ShowImage = true;
            this.buttonTestWaitingForm.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTestWaitingForm_Click);
            // 
            // buttonTestExportPDF
            // 
            this.buttonTestExportPDF.Label = "Test export PDF";
            this.buttonTestExportPDF.Name = "buttonTestExportPDF";
            this.buttonTestExportPDF.ShowImage = true;
            this.buttonTestExportPDF.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTestExportPDF_Click);
            // 
            // buttonUndo
            // 
            this.buttonUndo.Image = global::ExcelAddIn1.Properties.Resources.undo_48;
            this.buttonUndo.Label = "Undo";
            this.buttonUndo.Name = "buttonUndo";
            this.buttonUndo.ShowImage = true;
            this.buttonUndo.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonUndo_Click);
            // 
            // buttonShowClipboard
            // 
            this.buttonShowClipboard.Label = "Xem clipboard";
            this.buttonShowClipboard.Name = "buttonShowClipboard";
            this.buttonShowClipboard.ShowImage = true;
            this.buttonShowClipboard.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonShowClipboard_Click);
            // 
            // button2
            // 
            this.button2.Label = "Test report";
            this.button2.Name = "button2";
            this.button2.ShowImage = true;
            this.button2.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button2_Click);
            // 
            // buttonTestConvert
            // 
            this.buttonTestConvert.Label = "Test convert";
            this.buttonTestConvert.Name = "buttonTestConvert";
            this.buttonTestConvert.ShowImage = true;
            this.buttonTestConvert.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonTestConvert_Click);
            // 
            // group2
            // 
            this.group2.Items.Add(this.ButtonShowFormSetting);
            this.group2.Items.Add(this.buttonShowXtraFormSetting);
            this.group2.Items.Add(this.toggleButtonPinExcel);
            this.group2.Label = "Cài đặt";
            this.group2.Name = "group2";
            // 
            // ButtonShowFormSetting
            // 
            this.ButtonShowFormSetting.Image = global::ExcelAddIn1.Properties.Resources.icons8_maintenance_40px_3;
            this.ButtonShowFormSetting.Label = "Cài đặt";
            this.ButtonShowFormSetting.Name = "ButtonShowFormSetting";
            this.ButtonShowFormSetting.ShowImage = true;
            this.ButtonShowFormSetting.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ButtonShowFormSetting_Click_1);
            // 
            // buttonShowXtraFormSetting
            // 
            this.buttonShowXtraFormSetting.Image = global::ExcelAddIn1.Properties.Resources.icons8_maintenance_40px_3;
            this.buttonShowXtraFormSetting.Label = "Cài đặt 2";
            this.buttonShowXtraFormSetting.Name = "buttonShowXtraFormSetting";
            this.buttonShowXtraFormSetting.ShowImage = true;
            this.buttonShowXtraFormSetting.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonShowXtraFormSetting_Click);
            // 
            // toggleButtonPinExcel
            // 
            this.toggleButtonPinExcel.Label = "Ghim Excel";
            this.toggleButtonPinExcel.Name = "toggleButtonPinExcel";
            this.toggleButtonPinExcel.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.toggleButtonPinExcel_Click);
            // 
            // group8
            // 
            this.group8.Items.Add(this.buttonUpdateAddin);
            this.group8.Items.Add(this.menu12);
            this.group8.Label = "Cập nhật";
            this.group8.Name = "group8";
            // 
            // buttonUpdateAddin
            // 
            this.buttonUpdateAddin.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.buttonUpdateAddin.Enabled = false;
            this.buttonUpdateAddin.Image = global::ExcelAddIn1.Properties.Resources.icons8_downloading_updates_64;
            this.buttonUpdateAddin.Label = "Cập nhật bản mới";
            this.buttonUpdateAddin.Name = "buttonUpdateAddin";
            this.buttonUpdateAddin.ShowImage = true;
            this.buttonUpdateAddin.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonUpdateAddin_Click);
            // 
            // menu12
            // 
            this.menu12.Items.Add(this.buttonCheckInstallStatus);
            this.menu12.Label = "Status";
            this.menu12.Name = "menu12";
            // 
            // buttonCheckInstallStatus
            // 
            this.buttonCheckInstallStatus.Label = "Kiểm tra cài đặt";
            this.buttonCheckInstallStatus.Name = "buttonCheckInstallStatus";
            this.buttonCheckInstallStatus.ShowImage = true;
            this.buttonCheckInstallStatus.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonCheckInstallStatus_Click);
            // 
            // buttonDeleteAllBlankColumns
            // 
            this.buttonDeleteAllBlankColumns.Label = "Xóa cột trống";
            this.buttonDeleteAllBlankColumns.Name = "buttonDeleteAllBlankColumns";
            this.buttonDeleteAllBlankColumns.ShowImage = true;
            this.buttonDeleteAllBlankColumns.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.buttonDeleteAllBlankColumns_Click);
            // 
            // Ribbon1
            // 
            this.Name = "Ribbon1";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab2);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.tab2.ResumeLayout(false);
            this.tab2.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.group5.ResumeLayout(false);
            this.group5.PerformLayout();
            this.group3.ResumeLayout(false);
            this.group3.PerformLayout();
            this.group7.ResumeLayout(false);
            this.group7.PerformLayout();
            this.group4.ResumeLayout(false);
            this.group4.PerformLayout();
            this.group9.ResumeLayout(false);
            this.group9.PerformLayout();
            this.group6.ResumeLayout(false);
            this.group6.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            this.group8.ResumeLayout(false);
            this.group8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab2;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonExactNumberFromString;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu1;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonAddBlankRowAtValueChange;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group4;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonMergePDF2sidePrint;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonMergePDFcontain1page;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonMergePDF2page2sidePrint;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu3;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group6;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonCreateFilePulove;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group7;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button_ShowAdvanceFilterPane;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group8;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonUpdateAddin;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ButtonShowTaskPane;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ButtonShowFormCSDL;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group5;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu4;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonViewAccountStatementTechcombank;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button_Auto_HachToanGDTheTECH;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu5;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonViewAccountStatementVietcombank;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button_Auto_HachToanGDTheVCB;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ButtonShowFormSetting;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTestXuatBanThanhCong;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonStopWinAppDriver;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTestSignInThanhCong;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonPasteToNewSheet;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTestToastForm;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu6;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu7;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTestSignInVIETSEA;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTestCancel;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonShowWADLocation;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTestWriteDataToGoogleSheet;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTestWaitForm;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTurnOffSpeedUpCode;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu8;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menuChangeCapital;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonCapitalizeFirstChar;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonCapitalizeFirstCharEachWord;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonCapitalizeAll;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonLowerAll;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTestWaitingForm;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button2HachToanGDTheTechChiTietTungGD;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonOpenFormNhap;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonCompareTwoRanges;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button2;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton toggleButtonPinExcel;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group9;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTranformExcelToMInvoiceTemplate;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonAddCharacterToDisplayAsText;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonConvertRangeToValue;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonMergeInColumns;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonShowXtraFormSetting;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonShowClipboard;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTestExportPDF;
        internal Microsoft.Office.Tools.Ribbon.RibbonSplitButton splitButtonSetNumberFomat;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu9;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonGopFileExcel;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonOpenStartupPath;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTestConvert;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonCreatePowerAutomateTxtFile;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu10;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonUndo;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonTCVNtoUnicode;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu11;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu13;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu14;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonHopNhatBangCanDoiPhatSinh;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonHopNhatBCKQKD;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonDeleteAllNamedRange;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu15;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonDeleteAllShapes;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu16;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonChangeVLOOKUPFunction;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonCheckNumberArrayIsContinousNumericalSeries;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu12;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonCheckInstallStatus;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton buttonDeleteAllBlankColumns;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon1 Ribbon1
        {
            get { return this.GetRibbon<Ribbon1>(); }
        }
    }
}
