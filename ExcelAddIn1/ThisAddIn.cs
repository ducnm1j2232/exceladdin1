﻿namespace ExcelAddIn1
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            Notification.Wait._waitingForm = new Notification.Waitform.MyWaitingForm();

            try
            {
                string accHelperPath = System.IO.Path.Combine(Public.Addin.StartupPath, @"Bluesoft\AccHelperx64\AccHelper.xll");
                Globals.ThisAddIn.Application.RegisterXLL(accHelperPath);


                string xllPath64 = System.IO.Path.Combine(Public.Addin.StartupPath, @"Files\DNA\VSTOLib-AddIn64-packed.xll");
                //bool xllPath64Exist = System.IO.File.Exists(xllPath64);
               Ribbon1.xllRegister = Globals.ThisAddIn.Application.RegisterXLL(xllPath64);
                
                //System.Windows.Forms.MessageBox.Show("xll64 file exist: " + xllPath64.ToString() + System.Environment.NewLine + xll64.ToString());
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            finally
            {
            }
        }


        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }


        





        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
