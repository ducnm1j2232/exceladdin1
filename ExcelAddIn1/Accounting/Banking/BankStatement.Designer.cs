﻿
namespace ExcelAddIn1.Accounting.Banking
{
    partial class BankStatement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl_bankStatement = new DevExpress.XtraGrid.GridControl();
            this.gridView_bankStatement = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.button_analyseBankStatement = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_bankStatement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_bankStatement)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl_bankStatement
            // 
            this.gridControl_bankStatement.Location = new System.Drawing.Point(12, 49);
            this.gridControl_bankStatement.MainView = this.gridView_bankStatement;
            this.gridControl_bankStatement.Name = "gridControl_bankStatement";
            this.gridControl_bankStatement.Size = new System.Drawing.Size(776, 389);
            this.gridControl_bankStatement.TabIndex = 0;
            this.gridControl_bankStatement.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_bankStatement});
            // 
            // gridView_bankStatement
            // 
            this.gridView_bankStatement.GridControl = this.gridControl_bankStatement;
            this.gridView_bankStatement.Name = "gridView_bankStatement";
            // 
            // button_analyseBankStatement
            // 
            this.button_analyseBankStatement.Location = new System.Drawing.Point(12, 12);
            this.button_analyseBankStatement.Name = "button_analyseBankStatement";
            this.button_analyseBankStatement.Size = new System.Drawing.Size(124, 23);
            this.button_analyseBankStatement.TabIndex = 1;
            this.button_analyseBankStatement.Text = "Phân tích sao kê";
            this.button_analyseBankStatement.UseVisualStyleBackColor = true;
            this.button_analyseBankStatement.Click += new System.EventHandler(this.button_analyseBankStatement_Click);
            // 
            // BankStatement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button_analyseBankStatement);
            this.Controls.Add(this.gridControl_bankStatement);
            this.Name = "BankStatement";
            this.Text = "BankStatement";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_bankStatement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_bankStatement)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl_bankStatement;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_bankStatement;
        private System.Windows.Forms.Button button_analyseBankStatement;
    }
}