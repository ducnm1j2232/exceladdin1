﻿using ExcelAddIn1.MyClass;
using Microsoft.Office.Interop.Excel;
using VSTOLib.Extensions;

namespace ExcelAddIn1.Accounting.Banking
{
    public static class BankLib
    {
        public static string colDateTime = "ngaythang";
        public static string colType = "loaigd";
        public static string colDescription = "diengiai";
        public static string colAmount = "sotien";
        public static string colFee = "phi";
        public static string colCustomerID = "makhachhang";
        public static string colCustomerName = "tenkhachhang";




        public static bool IsVietcombankStatement(Worksheet worksheet)
        {
            if (worksheet.Range["a3"].Value2 == "Chủ tài khoản:")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsTechcombankStatement(Worksheet worksheet)
        {
            if (worksheet.Range["a1"].Value2 == "NGÂN HÀNG TMCP KỸ THƯƠNG VIỆT NAM")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static System.Data.DataTable AnalyzeVietcombankStatement(Worksheet ws)
        {
            //Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            //Worksheet ws = wb.ActiveSheet;

            int LR = ws.LastRow("a");
            System.Data.DataTable dataTable = new System.Data.DataTable();

            dataTable.Columns.Add(colDateTime);
            dataTable.Columns.Add(colType);
            dataTable.Columns.Add(colDescription);
            dataTable.Columns.Add(colAmount);
            dataTable.Columns.Add(colFee);
            dataTable.Columns.Add(colCustomerID);
            dataTable.Columns.Add(colCustomerName);


            int firstDataRow = 12;
            for (int i = firstDataRow; i <= LR; i++)
            {
                System.DateTime dateTime = (System.DateTime)ws.Range["a" + i].Value;
                string date = dateTime.ToString("dd/MM/yyyy");

                string type = ws.Range["i" + i].GetStringValue();
                string description = ws.Range["e" + i].GetStringValue();
                double amount = ws.Range["f" + i].GetValue(ValueType.Double);
                double fee = ws.Range["g" + i].GetValue(ValueType.Double);

                System.Data.DataRow newRow = dataTable.NewRow();
                double newAmount = 0;
                double newFee = 0;
                System.Data.DataRow[] dataRows = dataTable.Select($"{colType} = '{TransactionType.GiaoDichThe}' and {colDateTime} = '{date}'");
                if (type == TransactionType.GiaoDichThe && dataRows.Length == 1)
                {
                    newAmount = amount + System.Convert.ToInt32(dataRows[0].ItemArray[3]);
                    newFee = fee + System.Convert.ToInt32(dataRows[0].ItemArray[4]);
                    newRow.ItemArray = new[] { date, type, description, newAmount.ToString(), newFee.ToString() };
                    dataRows[0].Delete();
                }
                else
                {
                    newRow.ItemArray = new[] { date, type, description, amount.ToString(), fee.ToString() };
                }

                dataTable.Rows.Add(newRow);
            }

            return dataTable;

        }


    }
}
