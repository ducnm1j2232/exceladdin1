﻿namespace ExcelAddIn1.ThanhCong.Database.Tables
{
    public static class Customers
    {
        public const string TableName = "Customers";

        public const string CustomerID = "CustomerID";
        public const string CustomerName = "CustomerName";
        public const string Taxcode = "Taxcode";

    }
}
