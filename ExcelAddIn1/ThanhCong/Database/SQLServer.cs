﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.ThanhCong.Database
{
    public static class SQLServer
    {
        public static string DefaultConnectionStringTONGKHO
        {
            get
            {
                System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
                builder.DataSource = "192.168.0.9";
                builder.InitialCatalog = "ACCOUNTING";
                builder.UserID = "sa";
                builder.Password = "datphat@hanoi";

                return builder.ToString();
            }
        }

        public static string DefaultConnectionStringLETRONGTAN
        {
            get
            {
                System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
                builder.DataSource = "192.168.6.6";
                builder.InitialCatalog = "ACCOUNTING_LTT";
                builder.UserID = "sa";
                builder.Password = "123@Datphat";

                return builder.ToString();
            }
        }

        public static string ConvertDateToTransDate(DateTime dateTime)
        {
            return dateTime.ToString("yyyyMMdd");
        }

        private static async Task<DataTable> GetDetailByOneAccountAsyncCore(string connectionString, string accountNumber, DateTime fromDate, DateTime toDate, List<string> customerIDs)
        {
            if (customerIDs is null)
            {
                return null;
            }

            string IDs = string.Empty;
            foreach (string item in customerIDs)
            {
                IDs += "," + item;
            }

            IDs = IDs.Trim(',');

            List<System.Data.SqlClient.SqlParameter> sqlParameters = new List<System.Data.SqlClient.SqlParameter>();
            sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@FromDate", ConvertDateToTransDate(fromDate)));
            sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@EndDate", ConvertDateToTransDate(toDate)));
            sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@AccountNumber", accountNumber));
            sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@SupplyID", IDs));

            DataTable dataTable = await ADO.GetDataTableFromStoredProcedureAsync(connectionString, ThanhCong.Database.StoredProcedure.DetailByOneAccount, sqlParameters).ConfigureAwait(false);
            if (!dataTable.HasData())
            {
                return null;
            }

            return dataTable;
        }

        public static async Task<DataTable> GetDetailByOneAccountAsync(string connectionString, string accountNumber, DateTime fromDate, DateTime toDate, List<string> customerIDs)
        {
            return await GetDetailByOneAccountAsyncCore(connectionString, accountNumber, fromDate, toDate, customerIDs);
        }

        public static async Task<DataTable> GetDetailByOneAccountAsync(string connectionString, string accountNumber, DateTime fromDate, DateTime toDate, string customerID)
        {
            List<string> customerIDs = new List<string>();
            customerIDs.Add(customerID);
            return await GetDetailByOneAccountAsyncCore(connectionString, accountNumber, fromDate, toDate, customerIDs);
        }

        public static async Task<DataTable> GetDetailByOneAccountByMSTAsync(string connectionString, string accountNumber, DateTime fromDate, DateTime toDate, string mst)
        {
            List<string> customerIDs = await GetCustomerIDs(connectionString, mst);
            return await GetDetailByOneAccountAsyncCore(connectionString, accountNumber, fromDate, toDate, customerIDs);
        }

        public static async Task<List<string>> GetCustomerIDs(string connectionString, string mst)
        {
            List<string> customerIDs = new List<string>();

            string q = $"Select Customers.CustomerID From Customers Where Customers.Taxcode = '{mst}'";
            DataTable dataTable = await ADO.GetDataTableFromSQLServerAsync(connectionString, q).ConfigureAwait(false);
            if (!dataTable.HasData())
            {
                return null;
            }

            foreach (System.Data.DataRow row in dataTable.Rows)
            {
                customerIDs.Add(row["CustomerID"].ToString());
            }

            return customerIDs;
        }


    }
}
