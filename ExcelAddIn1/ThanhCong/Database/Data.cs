﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.ThanhCong.Database
{
    public static class Data
    {
        public static async Task<List<string>> GetCustomerIDList(string connectionString, string mst)
        {
            string q = $"select {ThanhCong.Database.Tables.Customers.CustomerID} From {ThanhCong.Database.Tables.Customers.TableName} where {ThanhCong.Database.Tables.Customers.Taxcode} = '{mst}'";
            DataTable dt = await ADO.GetDataTableFromSQLServerAsync(connectionString, q).ConfigureAwait(false);

            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }

            List<string> list = new List<string>();
            foreach (DataRow dataRow in dt.Rows)
            {
                list.Add(dataRow[ThanhCong.Database.Tables.Customers.CustomerID].ToString());
            }

            return list;
        }

    }
}
