﻿using ExcelAddIn1.MyClass;
using Microsoft.Office.Interop.Excel;
using System.Threading.Tasks;
using VSTOLib.Extensions;

namespace ExcelAddIn1
{
    class ExcelObject
    {

        public static Worksheet CopyRangeToNewSheetAndClearAllFormat(Range range, string sheetname = null)
        {
            Workbook wb = range.Worksheet.Parent;
            Worksheet newSheet;

            if (sheetname != null)
            {
                newSheet = wb.GetNewSheet(sheetname);
            }
            else
            {
                newSheet = wb.Worksheets.Add(After: range.Worksheet);
            }

            range.Copy(newSheet.Range["a1"]);
            newSheet.UsedRange.ClearAllBlankRowBlankColumnAndFormating();
            return newSheet;
        }

        public static async Task<Worksheet> GetWorksheetAsync(Workbook workbook, string sheetName, bool clearOldData = true)
        {
            foreach (Worksheet sh in workbook.Worksheets)
            {
                if (sh.Name == sheetName)
                {
                    if (clearOldData == true)
                    {
                        sh.AutoFilterMode = false;
                        sh.Columns.Delete();
                    }
                    return sh;
                }
            }

            Worksheet sheet = workbook.Worksheets.Add(After: workbook.Worksheets[workbook.Worksheets.Count]);
            //Worksheet sheet = wb.Worksheets.Add();
            sheet.Name = sheetName;
            await Task.Delay(0);
            return sheet;
        }

        public static Worksheet Barcode_Table1()
        {
            Workbook wb = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet ws = wb.Worksheets[1];

            ws.Name = "Table1";
            Range cells = ws.Cells;
            cells.Font.Name = "MS Sans Serif";
            cells.Font.Size = 10;

            ws.Range["a1", "i1"].Value = new string[] { "'Tensieuthi", "'Mancc", "'Tennganhhang", "'Masieuthi", "'Tenmathang", "'Barcode", "'Soluong", "'Giabanlecovat", "'Thutuin" };

            return ws;
        }


        public static async Task<Worksheet> GetSheetBangMaFromGoogleDriveAsync(Workbook workbook)
        {
            foreach (Worksheet sheet in workbook.Worksheets)
            {
                if (sheet.Name == PublicVar.BangMaSheetName)
                {
                    return sheet;
                }
            }

            //Cách mới: lấy từ google sheet
            object[,] dataFromGoogleSheet = await GoogleAPI.Get2dArrayFromGoogleSheetAsync($"{PublicVar.BangMaSheetName}!A1:C").ConfigureAwait(false);
            Worksheet wsBangMa = workbook.GetNewSheet(PublicVar.BangMaSheetName);
            dataFromGoogleSheet.CopyToRange(wsBangMa.Range["a1"]);


            //Cách cũ: lấy từ file
            //string BangMaPath = BangMa_FullName();
            //if (!string.IsNullOrEmpty(BangMaPath))
            //{
            //    using (DataTable dtBangMa = await ADO.GetDataTableFromExcelFileAsync(BangMaPath, "select * from [bangma10$]").ConfigureAwait(false))
            //    {
            //        if (dtBangMa.Rows.Count > 0)
            //        {
            //            //CopyDataTableToRange(dtBangMa, wsBangMa.Range["a1"], true);
            //            wsBangMa = workbook.GetWorksheet("bangma10", false);
            //            dtBangMa.CopyToRange(wsBangMa.Range["a1"]);
            //        }
            //    }
            //}

            return wsBangMa;
        }

    }
}
