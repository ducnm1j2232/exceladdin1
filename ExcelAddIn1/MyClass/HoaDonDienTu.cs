﻿using ExcelAddIn1.MyClass;
using Microsoft.Office.Interop.Excel;
using System;
using System.Windows.Forms;
using VSTOLib.Extensions;

namespace ExcelAddIn1
{
    class HoaDonDienTu
    {
        public static bool createSuccessfully = false;
        public static string SoHoaDon = string.Empty;

        public static void MoFileBangKeBanLeVaTaoFileImportHoaDonDienTu(string fileName)
        {
            Globals.ThisAddIn.Application.ScreenUpdating = false;
            createSuccessfully = false;
            SoHoaDon = "0";
            try
            {
                Workbook BangKeBanLeWorkbook = Globals.ThisAddIn.Application.Workbooks.Open(fileName);
                Worksheet sheetBangKe = BangKeBanLeWorkbook.Worksheets[1];
                if (sheetBangKe.Range["G3"].Value2 != "BẢNG KÊ BÁN LẺ")
                {
                    MessageBox.Show("File không đúng định dạng" + Environment.NewLine + BangKeBanLeWorkbook.FullName, Module1.errorTitle);
                    BangKeBanLeWorkbook.Close(false);
                    return;
                }
                Workbook importFile = NewImportHoaDonDienTuTemplate();
                Worksheet ImportSheet = importFile.Worksheets[1];

                //chuyển định dạng số
                int bangkeLR = sheetBangKe.LastRow("I");
                ConvertVietnameseNumberFormatToDouble(sheetBangKe.Range["J12", "P" + sheetBangKe.Rows.Count]);
                sheetBangKe.Range["J12", "j" + sheetBangKe.Rows.Count].Value2 = sheetBangKe.Range["J12", "j" + sheetBangKe.Rows.Count].Value2;
                double thueSuat = Math.Round(Convert.ToDouble(sheetBangKe.Range["P" + (bangkeLR + 2)].Value2) / Convert.ToDouble(sheetBangKe.Range["P" + (bangkeLR + 1)].Value2) * 100, 0);
                double soTienThue = Convert.ToDouble(sheetBangKe.Range["P" + (bangkeLR + 2)].Value2);

                int firstDataRowNumber = 11;
                sheetBangKe.Range["A12", "A" + bangkeLR].Copy(ImportSheet.Range["A" + firstDataRowNumber]);//stt
                sheetBangKe.Range["B12", "B" + bangkeLR].Copy(ImportSheet.Range["B" + firstDataRowNumber]);//mã hàng
                sheetBangKe.Range["C12", "C" + bangkeLR].Copy(ImportSheet.Range["C" + firstDataRowNumber]);//tên hàng
                sheetBangKe.Range["I12", "I" + bangkeLR].Copy(ImportSheet.Range["D" + firstDataRowNumber]);//đơn vị tính
                sheetBangKe.Range["J12", "J" + bangkeLR].Copy(ImportSheet.Range["E" + firstDataRowNumber]);//số lượng
                sheetBangKe.Range["K12", "K" + bangkeLR].Copy(ImportSheet.Range["F" + firstDataRowNumber]);//đơn giá

                int importLR = ImportSheet.LastRow("A");
                for (int i = firstDataRowNumber; i <= importLR; i++)
                {
                    ImportSheet.Range["G" + i].Value2 = Math.Round(Convert.ToDouble(ImportSheet.Range["E" + i].Value2) * Convert.ToDouble(ImportSheet.Range["F" + i].Value2), 0);//thành tiền
                    ImportSheet.Range["I" + i].Value2 = Math.Round(Convert.ToDouble(ImportSheet.Range["G" + i].Value2) * thueSuat / 100, 0);//tiền thuế
                }

                ImportSheet.Range["H" + firstDataRowNumber, "H" + importLR].Value2 = thueSuat;//thuế suất
                ImportSheet.UsedRange.ClearFormats();

                //tự động điều chỉnh số liệu
                double thanhTien = Convert.ToDouble(sheetBangKe.Range["P" + (bangkeLR + 1)].Value2);
                double sumAmount = Globals.ThisAddIn.Application.WorksheetFunction.Sum(ImportSheet.Range["G" + firstDataRowNumber, "G" + importLR]);
                double vatAmount = Globals.ThisAddIn.Application.WorksheetFunction.Sum(ImportSheet.Range["I" + firstDataRowNumber, "I" + importLR]);

                //sort lấy mã hàng có số lượng nhỏ nhất, rồi đến đơn giá nhỏ nhất. Khi đó thay đổi đơn giá sẽ làm thành tiền thay đổi từ từ
                Range sortRange = ImportSheet.Range["A" + firstDataRowNumber, "J" + importLR];
                sortRange.Sort(Key1: sortRange.Columns[5], Order1: XlSortOrder.xlAscending, Key2: sortRange.Columns[6], Order2: XlSortOrder.xlAscending, Header: XlYesNoGuess.xlYes);
                ImportSheet.Range["G" + firstDataRowNumber].Value2 = Convert.ToDouble(ImportSheet.Range["G" + firstDataRowNumber].Value2) + thanhTien - sumAmount;//thêm bớt đơn vị vào 1 mã hàng nhỏ nhất
                ImportSheet.Range["F" + firstDataRowNumber].Value2 = Math.Round(Convert.ToDouble(ImportSheet.Range["G" + firstDataRowNumber].Value2) / Convert.ToDouble(ImportSheet.Range["E" + firstDataRowNumber].Value2), 2);//tính lại đơn giá
                ImportSheet.Range["I" + firstDataRowNumber].Value2 = Math.Round(Convert.ToDouble(ImportSheet.Range["I" + firstDataRowNumber].Value2) + soTienThue - vatAmount, 0);//chỉnh lại tiền thuế
                sortRange.Sort(Key1: sortRange.Columns[1], Order1: XlSortOrder.xlAscending, Header: XlYesNoGuess.xlYes);

                string soHD = System.Text.RegularExpressions.Regex.Replace(sheetBangKe.Range["O4"].Text, @"[^\d]", "");
                SoHoaDon = soHD;
                string ngayHD = sheetBangKe.Range["H5"].Value2;
                ngayHD = "(" + ngayHD.Substring(5, 2) + "-" + ngayHD.Substring(14, 2) + "-" + ngayHD.Substring(21, 4) + ")";
                string tenKH = GetLast4Word(sheetBangKe.Range["e10"].Text);

                //kiểm tra mã hàng có đơn giá bằng 0
                Range rangeContainZero = ImportSheet.Range["f:f"].Find(What: "0", LookAt: XlLookAt.xlWhole);
                if (rangeContainZero != null)
                {
                    rangeContainZero = ImportSheet.Range["f:f"].Find(What: "0", LookAt: XlLookAt.xlPart);//reset Find vì dùng Whole ở trên
                    createSuccessfully = false;
                    importFile.Close(false);
                    BangKeBanLeWorkbook.Close(false);
                    Globals.ThisAddIn.Application.ScreenUpdating = true;
                    return;
                }

                //save
                string path = BangKeBanLeWorkbook.Path;
                Globals.ThisAddIn.Application.DisplayAlerts = false;
                importFile.SaveAs($"{path}\\iHoaDon {soHD} {tenKH} {ngayHD}");
                Globals.ThisAddIn.Application.DisplayAlerts = true;
                importFile.Close();
                BangKeBanLeWorkbook.Close(false);
                createSuccessfully = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Module1.errorTitle);
            }
            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        /// <summary>
        /// Chọn file báo cáo chi tiết xuất kho để tạo file import hóa đơn điện tử;
        /// sử dụng để tạo file import nhiều hóa đơn
        /// </summary>
        public static void MoFileBaoCaoChiTietXuatKhoVaTaoFileImport()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Chọn file báo cáo chi tiết xuất kho";
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "Excel Files|*.xls*";

            if (openFileDialog.ShowDialog() == DialogResult.Cancel) return;

            Workbook wbFileBaoCaoXuatKho = null;
            Worksheet wsBaoCaoXuatKho = null;
            try
            {
                wbFileBaoCaoXuatKho = Globals.ThisAddIn.Application.Workbooks.Open(openFileDialog.FileName);
                wsBaoCaoXuatKho = wbFileBaoCaoXuatKho.Worksheets[1];
                if (wsBaoCaoXuatKho.Name != "KTVTHH_XUAT_MORE_COLUM_SL_GT")
                {
                    wbFileBaoCaoXuatKho.Close(false);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            Worksheet sheetData = ExcelObject.CopyRangeToNewSheetAndClearAllFormat(wsBaoCaoXuatKho.Range["a10", "av" + wsBaoCaoXuatKho.LastRow("a")]);
            int LR = sheetData.LastRow("a");
            sheetData.Range["d1", "t" + LR].ConvertToValue();

            ExcelVoid.DeleteRowContainText(sheetData.Range["a1", "t" + LR], 1, "cộng");

            LR = sheetData.LastRow("a");//tính lại LR vì vừa xóa dòng
            sheetData.Range["b3"].Formula = "=IF(ISNUMBER(SEARCH(\"số ct\",A2))=TRUE,MID(A2,9,7),B2)";//lấy số hóa đơn
            sheetData.Range["i3"].Formula = "=IF(ISNUMBER(SEARCH(\"số ct\",A2))=TRUE,RIGHT(A2,10),I2)";//lấy ngày hạch toán
            sheetData.Range["j3"].Formula = "=ROUND(K3/O3*100, 0)";

            sheetData.Range["b3", "b" + LR].FillDown();
            sheetData.Range["i3", "i" + LR].FillDown();
            sheetData.Range["j3", "j" + LR].FillDown();

            ExcelVoid.PasteValue(sheetData.Range["b3", "b" + LR]);
            ExcelVoid.PasteValue(sheetData.Range["i3", "i" + LR]);
            ExcelVoid.PasteValue(sheetData.Range["j3", "j" + LR]);

            ExcelVoid.DeleteRowContainText(sheetData.Range["a1", "t" + LR], 1, "số ct");

            Workbook wbImport = New_Import_List_EInvoice_Template();
            Worksheet sheetImport = wbImport.Worksheets[1];

            #region update value to import file
            sheetData.Range["b2", "b" + LR].Copy(sheetImport.Range["a6"]);//số hóa đơn
            sheetData.Range["i2", "i" + LR].Copy(sheetImport.Range["b6"]);//ngày hóa đơn
            sheetImport.Range["f6", "f" + sheetImport.LastRow("a")].Value2 = "Khách lẻ không lấy hóa đơn";//tên
            sheetImport.Range["h6", "h" + sheetImport.LastRow("a")].Value2 = "Hà Nội";//dịa chỉ
            sheetImport.Range["j6", "j" + sheetImport.LastRow("a")].Value2 = "hoadondientu.datphathn@gmail.com";//mail nhận hóa đơn
            sheetImport.Range["k6", "k" + sheetImport.LastRow("a")].Value2 = "TM/CK";//hình thức thanh toán
            sheetData.Range["c2", "c" + LR].Copy(sheetImport.Range["o6"]);//tên hàng hóa
            sheetData.Range["g2", "g" + LR].Copy(sheetImport.Range["s6"]);//đơn vị tính
            sheetData.Range["d2", "d" + LR].Copy(sheetImport.Range["t6"]);//số lượng
            sheetData.Range["e2", "e" + LR].Copy(sheetImport.Range["u6"]);//đơn giá
            sheetData.Range["o2", "o" + LR].Copy(sheetImport.Range["v6"]);//thành tiền
            sheetData.Range["j2", "j" + LR].Copy(sheetImport.Range["w6"]);//%VAT
            sheetData.Range["k2", "k" + LR].Copy(sheetImport.Range["x6"]);//tiền VAT
            sheetData.Range["q2", "q" + LR].Copy(sheetImport.Range["y6"]);//tổng tiền
            #endregion

            string hoaDonDauTien = sheetImport.Range["a6"].Text;
            string hoaDonCuoiCung = sheetImport.Range["a" + sheetImport.LastRow("a")].Text;

            string fileName = $"import iHoaDon từ {hoaDonDauTien} đến {hoaDonCuoiCung}";
            fileName = Module1.ConvertToValidFileName(fileName);
            Globals.ThisAddIn.Application.DisplayAlerts = false;
            wbImport.SaveAs($"{ wbFileBaoCaoXuatKho.Path}\\{fileName}");
            Globals.ThisAddIn.Application.DisplayAlerts = true;

            wbFileBaoCaoXuatKho.Close(false);
            wbImport.Close();

        }

        static string GetLast4Word(string text)
        {
            string result = string.Empty;
            string[] stringArray = text.Trim().Split(' ');
            if (stringArray.Length <= 4)
            {
                result = text.Trim();
            }
            else
            {
                int index = stringArray.GetUpperBound(0);
                for (int i = 1; i <= 4; i++)
                {
                    result = stringArray[index].ToString() + " " + result;
                    index--;
                }
            }

            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
            foreach (char c in specialChar)
            {
                result = result.Replace(c, '_');
            }

            //không dùng Regex được vì nó replace hết cả chữ tiếng việt
            //result =  Regex.Replace(result,  @"[^0-9a-zA-Z]+", "");
            return result.Trim();
        }

        public static Workbook NewImportHoaDonDienTuTemplate()
        {
            Workbook wb = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet ws = wb.Worksheets[1];
            ws.Name = "Bang_ke_hang_hoa_dich_vu";

            string[] header = new string[] { "view_order", "product_code", "product_name", "unit_name", "quantity", "price", "amount", "vat", "amount_vat", "note" };
            ExcelVoid.Write1DArrayToRange(header, ws.Range["a10"]);
            return wb;
        }

        public static Workbook New_Import_List_EInvoice_Template()
        {
            Workbook wb = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet ws = wb.Worksheets[1];
            ws.Name = "Dữ Liệu Hóa Đơn";

            ws.Range["a5", "z5"].Value2 = new string[] { "number_of_vouchers", "invoice_date", "customer_code", "customer_object_code", "buyer_tax_code", "customer_name", "buyer_name", "buyer_address", "buyer_mobile", "buyer_email", "payment_method_name", "bank_account_number", "bank_name", "product_code", "product_name", "is_promotion", "commercial_discount", "provisions_on_accounts", "unit_name", "quantity", "price", "amount", "vat", "amount_vat", "amount_after_vat", "note" };
            return wb;
        }

        public static void ConvertVietnameseNumberFormatToDouble(Range range)
        {
            range.Replace(".", "", XlLookAt.xlPart);
            range.Replace(",", ".", XlLookAt.xlPart);
        }

        public static void Create_workbook_import_iHoaDon(System.Data.DataTable dataSource, string name, string savePath)
        {
            Workbook wb = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet ws = wb.Worksheets[1];
            ws.Name = "Bang_ke_hang_hoa_dich_vu";
            dataSource.CopyToRange(ws.Range["a10"]);
            ws.GetRange("a10:j", "a").ConvertToValue();
            wb.SaveOverWrite($"{savePath}\\{name}.xlsx", 51);
            wb.Close();
        }

        public static System.Threading.Tasks.Task Task_Create_workbook_import_iHoaDon(System.Data.DataTable dataSource, string name, string savePath)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                Workbook wb = Globals.ThisAddIn.Application.Workbooks.Add();
                Worksheet ws = wb.Worksheets[1];
                ws.Name = "Bang_ke_hang_hoa_dich_vu";
                dataSource.CopyToRange(ws.Range["a10"]);
                wb.SaveOverWrite($"{savePath}\\{name}.xlsx", 51);
                wb.Close();
            });
        }



    }
}
