﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;

public enum ValueType { String, Int, Double }

namespace ExcelAddIn1.MyClass
{

    public static class ExtensionMethod
    {





        public static int NextRowIndexAtColumn(this Worksheet worksheet, string columnName)
        {
            return worksheet.Range[columnName + worksheet.Rows.Count].End[XlDirection.xlUp].Row + 1;
        }

        public static Range NextColumnRangeAtRow(this Worksheet worksheet, int row)
        {
            return ((Range)worksheet.Cells[row, worksheet.Columns.Count]).End[XlDirection.xlToLeft].Offset[0, 1];
        }

        public static int NewColumn(this Worksheet worksheet, int row)
        {
            return ((Range)worksheet.Cells[row, worksheet.Columns.Columns]).End[XlDirection.xlToLeft].Column + 1;
        }

        public static Range NewColumnAtRow(this Worksheet worksheet, int row)
        {
            return ((Range)worksheet.Cells[row, worksheet.Columns.Count]).End[XlDirection.xlToLeft].Offset[0, 1];
        }



        public static Range LastRange(this Worksheet worksheet, string columnName)
        {
            return worksheet.Range[columnName + worksheet.Rows.Count].End[XlDirection.xlUp];
        }



        /// <summary>
        /// if ColumnName == null then return lastrow of current range
        /// </summary>
        /// <param name="range"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static Range ResizeToLastRow(this Range range, string columnName = null)
        {
            Worksheet sheet = range.Worksheet;
            Range firstCell = range.Cells[1, 1];
            int row1 = firstCell.Row;
            int row9 = 0;
            if (columnName != null)
            {
                row9 = sheet.LastRow(columnName);
            }
            else
            {
                //row9 = sheet.LastRow(range.GetColumnLetter());
                row9 = range.EntireColumn.Cells.Find(What: "*", SearchOrder: XlSearchOrder.xlByRows, SearchDirection: XlSearchDirection.xlPrevious).Row;

            }
            //int lastRow = range.Cells.Find(What: "*", SearchOrder: XlSearchOrder.xlByRows, SearchDirection: XlSearchDirection.xlPrevious).Row;

            return firstCell.Resize[row9 - row1 + 1, range.Columns.Count];
        }

        public static Range ResizeToRow(this Range range, int row)
        {
            int row1 = range.Row;
            return range.Resize[row - row1 + 1, range.Columns.Count];
        }

        public static void SetBorder(this Range range)
        {
            range.Borders.LineStyle = XlLineStyle.xlContinuous;
        }


        //public static Range ConvertToValue(this Range range)
        //{
        //    if (range.Areas.Count == 1)
        //    {
        //        range.Value = range.Value;
        //    }
        //    if (range.Areas.Count != 1)
        //    {
        //        for (int i = 1; i <= range.Areas.Count; i++)
        //        {
        //            range.Areas[i].Value = range.Areas[i].Value;
        //        }
        //    }

        //    return range;
        //}




        public static bool IsSingleColumn(this Range range)
        {
            if (range.Columns.Count == 1)
            {
                return true;
            }
            return false;
        }



        //public static void CopyToRange(this DataTable dataTable, Range range, bool header = true)
        //{
        //    if (dataTable == null || dataTable.Rows.Count == 0)
        //    {
        //        return;
        //    }

        //    if (header == true)
        //    {
        //        for (int i = 0; i < dataTable.Columns.Count; i++)
        //        {
        //            range.Offset[0, i].Value2 = dataTable.Columns[i].ColumnName;
        //        }
        //        range.Offset[1, 0].Resize[dataTable.Rows.Count, dataTable.Columns.Count].Value2 = dataTable.To2dStringArray();
        //    }
        //    else
        //    {
        //        range.Resize[dataTable.Rows.Count, dataTable.Columns.Count].Value2 = dataTable.To2dStringArray();
        //    }
        //}

        //public static string[,] To2dStringArray(this DataTable dataTable)
        //{
        //    string[,] result = new string[dataTable.Rows.Count, dataTable.Columns.Count];
        //    for (int rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
        //    {
        //        for (int colIndex = 0; colIndex < dataTable.Columns.Count; colIndex++)
        //        {
        //            result[rowIndex, colIndex] = dataTable.Rows[rowIndex][colIndex].ToString();
        //        }
        //    }
        //    return result;
        //}



        public static void SaveOverwriteOld(this Workbook workbook, string fileName, object fileFormat)
        {
            Globals.ThisAddIn.Application.DisplayAlerts = false;
            workbook.SaveAs(Filename: fileName, FileFormat: fileFormat);
            Globals.ThisAddIn.Application.DisplayAlerts = true;
            //if (CloseAfterSave) workbook.Close();
        }

        public static void DeleteSheet(this Workbook workbook, string sheetName)
        {
            try
            {
                Globals.ThisAddIn.Application.DisplayAlerts = false;
                workbook.GetSheet(sheetName).Delete();
                Globals.ThisAddIn.Application.DisplayAlerts = true;
            }
            catch
            {
            }
            finally
            {
                Globals.ThisAddIn.Application.DisplayAlerts = true;
            }
        }



        public static DataTable ToDataTable(this Range range, bool firstRowAsHeaders = true)
        {
            DataTable dt = new DataTable();


            Worksheet ws = range.Worksheet;
            Range firstCell = firstRowAsHeaders ? range.Cells[2, 1] : range.Cells[1, 1];
            Range lastCell = range.Cells[range.Rows.Count, range.Columns.Count];
            object[,] array = ws.Range[firstCell, lastCell].Value;


            Range headersRange = ws.Range[range.Cells[1, 1], range.Cells[1, range.Columns.Count]];
            int colIndex = 1;
            foreach (Range rng in headersRange)
            {
                dt.Columns.Add(firstRowAsHeaders ? rng.Value2 : $"Column{colIndex}");
                colIndex++;
            }

            for (var i = 1; i <= array.GetLength(0); i++)
            {
                DataRow row = dt.NewRow();
                for (var j = 1; j <= array.GetLength(1); j++)
                {
                    row[j - 1] = array[i, j];
                }
                dt.Rows.Add(row);
            }

            return dt;
        }

        public static DataTable ToDataTableSelectedColumns(this DataTable dataTable, params string[] selectedColumns)
        {
            return new DataView(dataTable).ToTable(false, selectedColumns);
        }

        /// <summary>
        /// "=0" ; "=muối*" always start with "="
        /// </summary>
        /// <param name="range"></param>
        /// <param name="ColumnNumber"></param>
        /// <param name="Criteria"></param>
        public static void DeleteRowContainTextCore(this Range range, int ColumnNumber, object Criteria)
        {
            Worksheet ws = range.Worksheet;

            Range firstCellIncludeHeader = range.Cells[1, 1];
            Range firstCellExceptHeader = firstCellIncludeHeader.Offset[1, 0];
            Range lastCell = range.Cells[range.Rows.Count, range.Columns.Count];

            try//bắt lỗi No cell was found.
            {
                //xlFilterValues dùng khi criteria là mảng
                //xlAnd dùng khi có not equal "<>"
                XlAutoFilterOperator xlAutoFilterOperator = XlAutoFilterOperator.xlFilterValues;
                if (Criteria is string)
                {
                    xlAutoFilterOperator = XlAutoFilterOperator.xlAnd;
                }

                range.AutoFilter(Field: ColumnNumber, Criteria1: Criteria, Operator: xlAutoFilterOperator);
                ws.Range[firstCellExceptHeader, lastCell].SpecialCells(XlCellType.xlCellTypeVisible).EntireRow.Delete();
            }
            catch { }

            //không dùng Offset bình thường vì Offset bình thường dịch chuyển cả vùng dữ liệu đi xuống, nghĩa là dòng cuối cùng sẽ bị đẩy xuống 1 dòng
            //range.Offset[1, 0].SpecialCells(XlCellType.xlCellTypeVisible).EntireRow.Delete();
            ws.AutoFilterMode = false;
        }

        /// <summary>
        /// blank: "="
        /// </summary>
        /// <param name="range"></param>
        /// <param name="columnHeaderText"></param>
        /// <param name="Criteria"></param>
        public static void DeleteRowContainText(this Range range, string columnHeaderText, object Criteria)
        {
            //Worksheet ws = range.Worksheet;

            Range firstCellIncludeHeader = range.Cells[1, 1];
            //Range firstCellExceptHeader = firstCellIncludeHeader.Offset[1, 0];
            //Range lastCell = range.Cells[range.Rows.Count, range.Columns.Count];

            int ColumnNumber = 0;
            for (int colIndex = 1; colIndex <= range.Columns.Count; colIndex++)
            {
                Range colRange = (Range)firstCellIncludeHeader.Cells[1, colIndex];
                if (colRange.Value == columnHeaderText)
                {
                    ColumnNumber = colIndex;
                    break;
                }
            }
            if (ColumnNumber == 0) throw new Exception("Không tìm được tên trường để xóa dữ liệu");

            range.DeleteRowContainTextCore(ColumnNumber, Criteria);

            //try//bắt lỗi No cell was found.
            //{
            //    //xlFilterValues dùng khi criteria là mảng
            //    //xlAnd dùng khi có not equal "<>"
            //    XlAutoFilterOperator xlAutoFilterOperator = XlAutoFilterOperator.xlFilterValues;
            //    if ((Criteria as string).Contains("<>"))
            //    {
            //        xlAutoFilterOperator = XlAutoFilterOperator.xlAnd;
            //    }
            //    range.AutoFilter(Field: ColumnNumber, Criteria1: Criteria, Operator: xlAutoFilterOperator);
            //    ws.Range[firstCellExceptHeader, lastCell].SpecialCells(XlCellType.xlCellTypeVisible).EntireRow.Delete();
            //}
            //catch { }

            //không dùng Offset bình thường vì Offset bình thường dịch chuyển cả vùng dữ liệu đi xuống, nghĩa là dòng cuối cùng sẽ bị đẩy xuống 1 dòng
            //range.Offset[1, 0].SpecialCells(XlCellType.xlCellTypeVisible).EntireRow.Delete();
            //ws.AutoFilterMode = false;
        }

        public static void CopyFilterData(this Range range, string columnName, object Criteria, Range Destination)
        {
            Worksheet ws = range.Worksheet;

            Range firstCellIncludeHeader = range.Cells[1, 1];
            Range firstCellExceptHeader = firstCellIncludeHeader.Offset[1, 0];
            Range lastCell = range.Cells[range.Rows.Count, range.Columns.Count];

            Range header = range.Rows[1];
            int ColumnNumber = header.GetHeaderRangeByText(columnName).Column;

            try//bắt lỗi No cell was found.
            {
                range.AutoFilter(Field: ColumnNumber, Criteria, XlAutoFilterOperator.xlFilterValues);
                range.SpecialCells(XlCellType.xlCellTypeVisible).Copy(Destination);
            }
            catch { }

            //không dùng Offset bình thường vì Offset bình thường dịch chuyển cả vùng dữ liệu đi xuống, nghĩa là dòng cuối cùng sẽ bị đẩy xuống 1 dòng
            //range.Offset[1, 0].SpecialCells(XlCellType.xlCellTypeVisible).EntireRow.Delete();
            ws.AutoFilterMode = false;
        }



        public static void FillDownFormula(this Range range, bool pasteValue = true)
        {
            Globals.ThisAddIn.Application.Calculation = XlCalculation.xlCalculationManual;
            if (range.Rows.Count > 1)//nếu chỉ có 1 dòng thì dữ liệu sẽ bằng dòng trên liền kề
            {
                range.FillDown();
            }
            Globals.ThisAddIn.Application.Calculation = XlCalculation.xlCalculationAutomatic;
            WaitCalculationComplete();

            if (pasteValue) range.PasteValue();

        }






        //public static void SafeInvoke(this Control control, Action action)
        //{
        //    if (control.InvokeRequired)
        //    {
        //        control.Invoke(action);
        //    }
        //    else
        //    {
        //        action();
        //    }
        //}

        //public static void SafeBeginInvoke(this Control control, Action action)
        //{
        //    if (control.InvokeRequired)
        //    {
        //        control.BeginInvoke(action);
        //    }
        //    else
        //    {
        //        action();
        //    }
        //}

        //public static string[,] To2DArray<T>(this IList<IList<T>> source)
        //{
        //    if (source == null)
        //    {
        //        throw new ArgumentNullException("source null");
        //    }

        //    int max = source.Select(l => l).Max(l => l.Count());
        //    var result = new string[source.Count, max];

        //    for (int i = 0; i < source.Count; i++)
        //    {
        //        for (int j = 0; j < source[i].Count(); j++)
        //        {
        //            result[i, j] = source[i][j].ToString();
        //        }
        //    }

        //    return result;
        //}

        public static string[] To1DArray<T>(this IList<IList<T>> source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            int max = source.Select(l => l).Max(l => l.Count());
            List<string> result = new List<string>();

            for (int i = 0; i < source.Count; i++)
            {
                for (int j = 0; j < source[i].Count(); j++)
                {
                    result.Add(source[i][j].ToString());
                }
            }

            return result.ToArray();
        }

        public static void CopyToRange(this object[,] object2D, Range range)
        {
            range.Resize[object2D.GetLength(0), object2D.GetLength(1)].Value = object2D;
        }

        //public static void ClearAllBlankRowBlankColumnAndFormating(this Range range)
        //{
        //    range.UnMerge();
        //    range.ClearFormats();
        //    range.ClearHyperlinks();
        //    range.WrapText = false;
        //    range.Worksheet.DeleteBlankRows();
        //    range.Worksheet.DeleteBlankColumns();
        //}

        public static void DeleteBlankRows(this Worksheet worksheet)
        {
            int lastRow = worksheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell).Row;

            Module1.SpeedUpCode(true);
            for (int i = lastRow; i > 0; i--)
            {
                if (Globals.ThisAddIn.Application.WorksheetFunction.CountA(worksheet.Rows[i]) == 0)
                {
                    Range row = worksheet.Rows[i];
                    row.EntireRow.Delete();
                }
            }
            Module1.SpeedUpCode(false);
        }

        public static void DeleteBlankColumns(this Worksheet worksheet)
        {
            int lastColumn = worksheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell).Column;

            Module1.SpeedUpCode(true);
            for (int i = lastColumn; i > 0; i--)
            {
                if (Globals.ThisAddIn.Application.WorksheetFunction.CountA(worksheet.Columns[i]) == 0)
                {
                    Range col = worksheet.Columns[i];
                    col.EntireColumn.Delete();
                }
            }
            Module1.SpeedUpCode(false);
        }

        /// <summary>
        /// remember to check items.count
        /// cannot find if number format diffent
        /// </summary>
        /// <param name="range"></param>
        /// <param name="listText"></param>
        /// <returns></returns>
        public static List<Range> FindAllRangeEqualToValue(this Range range, IList<string> listText, bool matchCase = false)
        {
            List<Range> result = new List<Range>();

            foreach (string eachText in listText)
            {
                Range foundRange = range.Find(What: eachText, LookIn: XlFindLookIn.xlValues, LookAt: XlLookAt.xlWhole, SearchOrder: XlSearchOrder.xlByRows, MatchCase: matchCase);
                if (foundRange != null)
                {
                    string firstAddress = foundRange.Address;
                    while (foundRange != null)
                    {
                        result.Add(foundRange);
                        foundRange = range.FindNext(foundRange);
                        if (foundRange != null && foundRange.Address == firstAddress) break;
                    }
                }
            }
            return result;
        }

        public static List<object> GetAllValueFoundInRange(this Range range, List<string> textToFind)
        {
            List<object> result = new List<object>();

            foreach (string eachText in textToFind)
            {
                Range foundRange = range.Find(What: eachText, LookIn: XlFindLookIn.xlValues, LookAt: XlLookAt.xlPart, SearchOrder: XlSearchOrder.xlByRows);
                if (foundRange != null)
                {
                    string firstAddress = foundRange.Address;
                    while (foundRange != null)
                    {
                        if (!result.Contains(foundRange.Value))
                        {
                            result.Add(foundRange.Value);
                        }
                        foundRange = range.FindNext(foundRange);
                        if (foundRange != null && foundRange.Address == firstAddress) break;
                    }
                }
            }
            return result;
        }

        public static List<object> GetAllValueFoundInRange(this Range range, IEnumerable<object> values)
        {
            List<object> result = new List<object>();

            foreach (string eachValue in values)
            {
                Range foundRange = range.Find(What: eachValue.Trim(), LookIn: XlFindLookIn.xlValues, LookAt: XlLookAt.xlPart, SearchOrder: XlSearchOrder.xlByRows);
                if (foundRange != null)
                {
                    string firstAddress = foundRange.Address;
                    while (foundRange != null)
                    {
                        if (!result.Contains(foundRange.Value))
                        {
                            result.Add(foundRange.Value);
                        }
                        foundRange = range.FindNext(foundRange);
                        if (foundRange != null && foundRange.Address == firstAddress) break;
                    }
                }
            }

            return result;
        }


        /// <summary>
        /// <para>Range should be a single entire row.</para>
        /// <para>Use to find Header Range, create a new Header Range 'offset-right-step-1' if not found</para> 
        /// </summary>
        /// <param name="range"></param>
        /// <param name="text"></param>
        /// <param name="xlLookAt"></param>
        /// <param name="matchCase"></param>
        /// <returns></returns>
        public static Range GetHeaderRangeByText(this Range range, string text, XlLookAt xlLookAt = XlLookAt.xlPart, bool matchCase = false)
        {
            Range foundRange = range.Find(What: text, LookIn: XlFindLookIn.xlValues, LookAt: xlLookAt, SearchOrder: XlSearchOrder.xlByRows, MatchCase: matchCase);
            if (foundRange != null)
            {
                return foundRange;
            }
            else
            {
                Worksheet sheet = range.Worksheet;
                Range rangeCreated = sheet.NextColumnRangeAtRow(range.Row);
                rangeCreated.Value2 = text;
                return rangeCreated;
            }
        }

        public static Range GetRangeByText(this Worksheet worksheet, string text, XlLookAt xlLookAt = XlLookAt.xlPart, bool matchCase = false)
        {
            Range foundRange = worksheet.Cells.Find(What: text, LookIn: XlFindLookIn.xlValues, LookAt: xlLookAt, SearchOrder: XlSearchOrder.xlByRows, MatchCase: matchCase);
            if (foundRange != null) return foundRange;
            else throw new Exception("GetRangeByText cannot find range " + text);
        }

        /// <summary>
        /// return Range that offset down 1 step
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public static Range FirstRow(this Range range)
        {
            return range.offset(xlOffset.Down);
        }

        public static string GetColumnLetter(this Range range)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"\d");
            return regex.Replace(range.Address[false, false], string.Empty);
        }


        public static void MergeAndCenter(this Range range)
        {
            range.Merge();
            range.VerticalAlignment = XlVAlign.xlVAlignCenter;
            range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
        }

        /// <summary>
        /// Dùng cho chức năng tìm kiếm ký tự theo thứ tự
        /// </summary>
        /// <param name="str"></param>
        /// <param name="charArray"></param>
        /// <returns></returns>
        public static bool ContainsInList(this string str, string[] charArray)
        {
            bool result = true;

            string previousChar = string.Empty;
            int substringStartIndex = 0;

            for (int i = 0; i < charArray.Length; i++)
            {
                string currentChar = charArray[i];
                if (str.IndexOf(currentChar, StringComparison.OrdinalIgnoreCase) == -1) return false;//cái đầu mà ko có thì false luôn

                int addPosition = 0; // nếu tìm 2 ký tự trùng nhau và liền nhau thì sẽ bị lặp lại cái cữ, tìm "aa" cũng chỉ như tìm "a" mà thôi
                if (currentChar == previousChar) addPosition = 1; //tăng thêm 1 để Start Position nó liền kề sau đó
                str = str.Substring(substringStartIndex + addPosition).ToLower();
                substringStartIndex = str.IndexOf(currentChar.ToLower());

                if (!str.Contains(currentChar.ToLower())) return false;
                previousChar = currentChar;
            }

            return result;
        }

        public static void InsertBlankRowsAtValueChange(this Worksheet worksheet, string columnName, int numberOfRowsToAdd = 1)
        {
            int LR = worksheet.LastRow(columnName);
            for (int rowIndex = LR; rowIndex > 1; rowIndex--)
            {
                //Range range1 = range.Cells[rowIndex, 1];
                //Range range0 = range.Cells[rowIndex - 1, 1];
                if (worksheet.Range[columnName + rowIndex].Text != worksheet.Range[columnName + (rowIndex - 1)].Text)
                {
                    for (int i = 1; i <= numberOfRowsToAdd; i++)
                    {
                        worksheet.Range[columnName + rowIndex].EntireRow.Insert();
                    }
                }
            }
        }

        public static void InsertBlankRowsAtValueChange(this Worksheet worksheet, Range range, int numberOfRowsToAdd = 1)
        {
            int FR = ((Range)range.Cells[1, 1]).Row;
            int LR = ((Range)range.Cells[range.Rows.Count, 1]).Row;

            for (int rowIndex = range.Rows.Count; rowIndex > 1; rowIndex--)
            {
                Range range1 = range.Cells[rowIndex, 1];
                Range range0 = range.Cells[rowIndex - 1, 1];
                if (range1.Text != range0.Text)
                {
                    for (int i = 1; i <= numberOfRowsToAdd; i++)
                    {
                        range1.EntireRow.Insert();
                    }
                }
            }
        }

        public static string[] AddWildCard(this string[] source)
        {
            List<string> list = new List<string>();
            foreach (string s in source)
            {
                list.Add('*' + s + '*');
            }
            return list.ToArray();
        }

        public static HashSet<T> ToHashset<T>(this Range source)
        {
            HashSet<T> hs = new HashSet<T>();
            foreach (Range r in source)
            {
                hs.Add(r.Value);
            }

            return hs;

        }

        public static bool IsNumeric(this string str)
        {
            return double.TryParse(str, out _);
        }

        public static bool ContainsAtLeast1Letter(this string str)
        {
            return str.Any(x => char.IsLetter(x));
        }

        public static List<string> ValueToList(this Range range, bool unique = false, bool ignoreEmptyCell = true)
        {
            List<string> result = new List<string>();

            void addValue(List<string> list, Range rng, bool ignoreEmpty)
            {
                if (ignoreEmpty)
                {
                    if (rng.Value != null)
                    {
                        list.Add(rng.GetStringValue());
                    }
                }
            }

            foreach (Range eachRange in range)
            {
                if (unique)
                {
                    if (!result.Contains(eachRange.GetStringValue()))
                    {
                        addValue(result, eachRange, ignoreEmptyCell);
                    }
                }
                else
                {
                    addValue(result, eachRange, ignoreEmptyCell);
                }
            }

            return result;
        }

        public static string ToSQLWhereIn(this IList<string> list)
        {
            string[] array = list.ToArray();
            return "'" + string.Join("','", array) + "'";
        }

        public static bool IsEven(this int value)
        {
            return value % 2 == 0;
        }

        public static dynamic GetValue(this Range range, ValueType type)
        {
            switch (type)
            {
                case ValueType.String:
                    if (range.Value == null) return string.Empty;
                    return Convert.ToString(range.Value);
                case ValueType.Int:
                    if (range.Value == null) return default(int);
                    bool success = int.TryParse(Convert.ToString(range.Value), out int result);
                    if (success) return result;
                    else return default(int);
                case ValueType.Double:
                    if (range.Value == null) return default(double);
                    bool success2 = double.TryParse(Convert.ToString(range.Value), out double result2);
                    if (success2) return result2;
                    else return default(double);
                default:
                    return range.Value;
            }
        }
        public static string GetStringValue(this Range range)
        {
            return (string)range.GetValue(ValueType.String);
        }


        public static object[] To1DArray(this object[,] input)
        {
            // Step 1: get total size of 2D array, and allocate 1D array.
            int size = input.Length;
            object[] result = new object[size];

            // Step 2: copy 2D array elements into a 1D array.
            int write = 0;
            for (int i = 0; i <= input.GetUpperBound(0); i++)
            {
                for (int z = 0; z <= input.GetUpperBound(1); z++)
                {
                    result[write++] = input[i, z];
                }
            }
            // Step 3: return the new array.
            return result;
        }

        public static object[] To1DArray(this Range range)
        {
            object[,] arr = range.Value;

            List<object> result = new List<object>();

            for (int i = 1; i <= arr.GetUpperBound(0); i++)
            {
                for (int z = 1; z <= arr.GetUpperBound(1); z++)
                {
                    if (arr[i, z] == null) break;
                    result.Add(arr[i, z]);
                }
            }

            return result.ToArray();
        }


        public static dynamic GetText(this Control control, ValueType type = ValueType.String)
        {
            dynamic result = null;
            control.SafeInvoke(() =>
            {
                switch (type)
                {
                    case ValueType.String:
                        if (string.IsNullOrEmpty(control.Text))
                        {
                            result = string.Empty;
                        }
                        else
                        {
                            result = control.Text;
                        }
                        break;
                    case ValueType.Int:
                        if (string.IsNullOrEmpty(control.Text))
                        {
                            result = 0;
                        }
                        else
                        {
                            result = Convert.ToInt32(control.Text);
                        }
                        break;
                    case ValueType.Double:
                        if (string.IsNullOrEmpty(control.Text))
                        {
                            result = 0;
                        }
                        else
                        {
                            result = Convert.ToDouble(control.Text);
                        }
                        break;
                    default:
                        result = control.Text;
                        break;
                }
            });
            return result;
        }

        public static void SetTextSafeThread(this Control control, string text)
        {
            control.SafeInvoke(() =>
            {
                control.Text = text;
            });
        }

        //public static void SetAccountingNumberFormat(this Range range, int numberAfterDot = 0)
        //{
        //    string fractionalPart = numberAfterDot == 0 ? string.Empty : $".".PadRight(numberAfterDot + 1, '0');
        //    string format = $"#,##0{fractionalPart}";
        //    range.NumberFormat = format;
        //}

        public static dynamic[,] Get2DvalueArray(this Range range)
        {
            if (range.IsSingleCell())
            {
                object[,] res = new object[1, 1];
                res[0, 0] = range.GetValue(ValueType.String);
                return res;
            }

            object[,] source = range.Value;//range.value thì mảng sẽ là 1-base, không phải zero-base
            object[,] result = new object[source.GetLength(0), source.GetLength(1)];

            for (int i = 1; i <= source.GetUpperBound(0); i++)
            {
                for (int j = 1; j <= source.GetUpperBound(1); j++)
                {
                    result[i - 1, j - 1] = source[i, j];
                }
            }
            return result;
        }


        public static Worksheet Clone(this Worksheet worksheet, string sheetName = null)
        {
            Workbook workbook = (Workbook)worksheet.Parent;
            worksheet.Copy(After: worksheet);
            Worksheet result = workbook.ActiveSheet;
            if (sheetName != null)
            {
                workbook.DeleteSheet(sheetName);
                result.Name = sheetName;
            }
            return result;
        }

        public static void CustomSort(this Range range, Range key, XlSortOrder order)
        {
            Worksheet worksheet = range.Worksheet;
            worksheet.Sort.SortFields.Clear();
            worksheet.Sort.SortFields.Add(Key: key, SortOn: XlSortOn.xlSortOnValues, Order: order, XlSortDataOption.xlSortNormal);
            worksheet.Sort.SetRange(range);
            worksheet.Sort.Header = XlYesNoGuess.xlYes;
            worksheet.Sort.MatchCase = false;
            worksheet.Sort.Orientation = XlSortOrientation.xlSortColumns;
            worksheet.Sort.SortMethod = XlSortMethod.xlPinYin;
            worksheet.Sort.Apply();
        }

        public static void Sort1(this Range range, Range key, XlSortOrder order = XlSortOrder.xlAscending)
        {
            Worksheet worksheet = range.Worksheet;
            worksheet.Sort.SortFields.Clear();
            worksheet.Sort.SortFields.Add(Key: key, SortOn: XlSortOn.xlSortOnValues, Order: order, XlSortDataOption.xlSortNormal);
            worksheet.Sort.SetRange(range);
            worksheet.Sort.Header = XlYesNoGuess.xlYes;
            worksheet.Sort.MatchCase = false;
            worksheet.Sort.Orientation = XlSortOrientation.xlSortColumns;
            worksheet.Sort.SortMethod = XlSortMethod.xlPinYin;
            worksheet.Sort.Apply();

            WaitCalculationComplete();
        }

        public static void Sort2(this Range range, Range key, XlSortOrder order = XlSortOrder.xlAscending)
        {
            Worksheet worksheet = range.Worksheet;
            //worksheet.Sort.SortFields.Clear();
            worksheet.Sort.SortFields.Add(Key: key, SortOn: XlSortOn.xlSortOnValues, Order: order, XlSortDataOption.xlSortNormal);
            worksheet.Sort.SetRange(range);
            worksheet.Sort.Header = XlYesNoGuess.xlYes;
            worksheet.Sort.MatchCase = false;
            worksheet.Sort.Orientation = XlSortOrientation.xlSortColumns;
            worksheet.Sort.SortMethod = XlSortMethod.xlPinYin;
            worksheet.Sort.Apply();

            WaitCalculationComplete();
        }



        public static int GetRandomNumber(this System.Windows.Forms.TextBox textBox)
        {
            string text = (string)textBox.GetText();
            if (!text.Contains("-"))
            {
                return (int)textBox.GetText(ValueType.Int);
            }
            else
            {
                string[] array = text.Split('-');
                int s1 = Convert.ToInt32(array[0].ToString());
                int s2 = Convert.ToInt32(array[1].ToString());
                Random random = new Random();
                return random.Next(s1, s2);
            }
        }

        public static void AddDataSource(this ListView listView, DataTable dataTable)
        {
            listView.Columns.Clear();
            listView.Items.Clear();

            foreach (DataColumn dataColumn in dataTable.Columns)
            {
                listView.Columns.Add(dataColumn.ColumnName);
            }

            for (int row = 0; row < dataTable.Rows.Count; row++)
            {
                ListViewItem listViewItem = new ListViewItem(dataTable.Rows[row][0].ToString());
                listView.Items.Add(listViewItem);
                for (int col = 1; col < dataTable.Columns.Count; col++)
                {
                    listViewItem.SubItems.Add(dataTable.Rows[row][col].ToString());
                }
            }


        }

        public enum xlOffset
        {
            Left, Right, Up, Down
        }
        public static Range offset(this Range range, xlOffset xloffset, int offsetStep = 1)
        {
            Range result = range;
            switch (xloffset)
            {
                case xlOffset.Left:
                    result = range.Offset[0, -offsetStep];
                    break;
                case xlOffset.Right:
                    result = range.Offset[0, offsetStep];
                    break;
                case xlOffset.Up:
                    result = range.Offset[-offsetStep, 0];
                    break;
                case xlOffset.Down:
                    result = range.Offset[offsetStep, 0];
                    break;
                default:
                    break;
            }
            return result;
        }


        public static void WaitCalculationComplete()
        {
            Globals.ThisAddIn.Application.Calculation = XlCalculation.xlCalculationAutomatic;
            Globals.ThisAddIn.Application.Calculate();
            System.Windows.Forms.Application.DoEvents();

            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;

            while (true)
            {
                if (Globals.ThisAddIn.Application.CalculationState == XlCalculationState.xlPending)
                {
                    Globals.ThisAddIn.Application.Calculate();
                }

                //ws.Range["a1"].Select();//trick tự nghĩ, kiểu như nếu không active hoặc click linh tinh đi đâu đó, thì sheet không recalculate
                System.Windows.Forms.Application.DoEvents();

                if (Globals.ThisAddIn.Application.CalculationState == XlCalculationState.xlDone)
                {
                    Globals.ThisAddIn.Application.StatusBar = false;
                    break;
                }

            }
        }



        public static Range EntireRow(this Worksheet worksheet, int rowNumber)
        {
            return worksheet.Rows[rowNumber];
        }

        public static Range EntireColumn(this Worksheet worksheet, string columnName)
        {
            return worksheet.Range[columnName + "1"].EntireColumn;
        }








    }
}


