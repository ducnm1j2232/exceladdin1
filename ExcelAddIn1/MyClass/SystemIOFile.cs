﻿namespace ExcelAddIn1.MyClass
{
    public static class SystemIOFile
    {
        public static string DownloadFolder
        {
            get
            {
                string userRoot = System.Environment.GetEnvironmentVariable("USERPROFILE");
                string downloadFolder = System.IO.Path.Combine(userRoot, "Downloads");
                return downloadFolder;
            }
        }

        public static void Delete(string fileName)
        {
            if (System.IO.File.Exists(fileName))
            {
                System.IO.File.Delete(fileName);
            }
        }

        public static void Rename(string oldPath, string newPath)
        {
            Delete(newPath);
            System.IO.File.Move(oldPath, newPath);
        }
    }
}

