﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace ExcelAddIn1.MyClass
{
    public static class TableRange
    {
        public static ListObject GetTableRange(Range sourceRange)
        {
            Worksheet sheet = sourceRange.Worksheet;
            ListObject listObject = sheet.ListObjects.AddEx(XlListObjectSourceType.xlSrcRange, sourceRange,null,XlYesNoGuess.xlYes);
            listObject.TableStyle = "";
            return listObject;
        }
    }
}
