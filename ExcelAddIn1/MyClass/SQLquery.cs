﻿using System.Collections.Generic;

namespace ExcelAddIn1.MyClass
{
    class SQLquery
    {
        public static string getRetailTranDetail(string tranWhereIn)
        {
            return string.Format(@"Select
                                RetailTranDetail.GoodID As [Mã hàng],
                                Goods.ShortName As [Tên hàng],
                                '' As [Mã vietsea],
                                Sum(RetailTranDetail.OrginalQty) As [Số lượng],
                                Round(Sum((RetailTranDetail.TotalPriceVATOrg - IsNull(TransactionDiscountDetails.DiscountAmount, 0)) / 1.1),0) /
                                Sum(RetailTranDetail.OrginalQty) As [Đơn giá],
                                Round(Sum((RetailTranDetail.TotalPriceVATOrg - IsNull(TransactionDiscountDetails.DiscountAmount, 0)) / 1.1),
                                0) As [Thành tiền],
                                Round(Sum((RetailTranDetail.TotalPriceVATOrg - IsNull(TransactionDiscountDetails.DiscountAmount, 0))),
                                0) As [Thành tiền VAT]
                            From
                                RetailTranDetail Inner Join
                                Goods On Goods.GoodID = RetailTranDetail.GoodID Inner Join
                                RetailTrans On RetailTranDetail.TransactionID = RetailTrans.TransactionID left Join
                                TransactionDiscountDetails On TransactionDiscountDetails.TransactionID = RetailTranDetail.TransactionID
                                And TransactionDiscountDetails.FullGoodID = RetailTranDetail.GoodID
                            Where
                                RetailTrans.TransactionID In ({0})
                            Group By
                                RetailTranDetail.GoodID,
                                Goods.ShortName", tranWhereIn);
        }

        public static string GetRetailTranDetail2(string tranIDsWhereIn, int vatRate = 8)
        {
            string q = string.Format(@"select
RetailTranDetail.GoodID,
Goods.ShortName,
'' as [Mã vietsea],
'' as [Kho],
0 as [SL tồn],
Sum(RetailTranDetail.OrginalQty) As [Số lượng],
Sum((RetailTranDetail.TotalPriceVATOrg - IsNull(TransactionDiscountDetails.DiscountAmount, 0)) / (1+{1}/100.00000000)) /Sum(RetailTranDetail.OrginalQty) As [Đơn giá],
Sum((RetailTranDetail.TotalPriceVATOrg - IsNull(TransactionDiscountDetails.DiscountAmount, 0)) / (1+{1}/100.00000000)) As [Thành tiền],
{1} as VAT,
Sum((RetailTranDetail.TotalPriceVATOrg - IsNull(TransactionDiscountDetails.DiscountAmount, 0))) As [Thành tiền VAT]

from
RetailTranDetail
inner join Goods on Goods.GoodID = RetailTranDetail.GoodID
inner join RetailTrans on RetailTranDetail.TransactionID = RetailTrans.TransactionID
left join TransactionDiscountDetails on TransactionDiscountDetails.TransactionID = RetailTranDetail.TransactionID
and TransactionDiscountDetails.FullGoodID = RetailTranDetail.GoodID

where
RetailTrans.TransactionID in ({0})
group by
RetailTranDetail.GoodID,
Goods.ShortName", tranIDsWhereIn, vatRate);
            return q;
        }

        public static string getRetailTranDetailTotal(string tranWhereIn)
        {
            return string.Format(@"Select
                            '' As[Mã hàng],
                            '' As[Tên hàng],
                            '' As[Mã vietsea],
                            '' As[Số lượng],
                            '' As[Đơn giá],
                            round(Sum((RetailTranDetail.TotalPriceVATOrg - IsNull(TransactionDiscountDetails.DiscountAmount, 0)) / 1.1), 0) As
                            [Thành tiền],
                            round(Sum((RetailTranDetail.TotalPriceVATOrg - IsNull(TransactionDiscountDetails.DiscountAmount, 0))), 0) As[Thành tiền VAT]
                        From
                            RetailTranDetail Left Join
                            TransactionDiscountDetails On TransactionDiscountDetails.TransactionID = RetailTranDetail.TransactionID
                                And TransactionDiscountDetails.FullGoodID = RetailTranDetail.GoodID
                        Where 
                            RetailTranDetail.TransactionID In ({0})", tranWhereIn);
        }

        public static string getXNTVIETSEA(string kho, string date)
        {
            string s = $"select MAVTU as \"Mã vật tư\"," +
                $" TENVTU as \"Tên vật tư\"," +
                $" ROUND(TONCUOIKYSL) as \"Số lượng\"," +
                $" ROUND(GIAVON) as \"Giá vốn\"," +
                $" ROUND(TONCUOIKYGT) as \"Thành tiền\"" +
                $" from XNT_{date}" +
                $" where MAKHOHANG ='{kho}'" +
                $" and TONCUOIKYSL <> 0" +
                $" order by MAVTU";
            return s;
        }

        public static string GetNegativeStockVIETSEA(string date)
        {
            //string s = $"select MAKHOHANG as KHO," +
            //    $"MAVTU as \"Mã vật tư\"," +
            //    $" TENVTU as \"Tên vật tư\"," +
            //    $" ROUND(TONCUOIKYSL) as \"Số lượng\"" +
            //    $" from XNT_{date}" +
            //    //$" where MAKHOHANG ='{kho}'" +
            //    $" where TONCUOIKYSL < 0" +
            //    $" order by MAKHOHANG, MAVTU";

            string s = $"select MAKHOHANG as KHO," +
                $"MAVTU ," +
                $" TENVTU ," +
                $" TONCUOIKYSL " +
                $" from XNT_{date}" +
                //$" where MAKHOHANG ='{kho}'" +
                $" where TONCUOIKYSL < 0";
            return s;
        }

        public static string getXNTVIETSEA(string kho, string date, List<string> mavattu)
        {
            string listMavtu = mavattu.ToSQLWhereIn();
            string s = $"select MAVTU as \"Mã vật tư\"," +
                $" TENVTU as \"Tên vật tư\"," +
                $" ROUND(TONCUOIKYSL) as \"Số lượng\"," +
                $" ROUND(GIAVON) as \"Giá vốn\"," +
                $" ROUND(TONCUOIKYGT) as \"Thành tiền\"" +
                $" from XNT_{date}" +
                $" where MAKHOHANG ='{kho}'" +
                $" and MAVTU in ({listMavtu})" +
                $" and TONCUOIKYSL <> 0" +
                $" order by MAVTU";
            return s;
        }

        public static string getNXTVIETSEA_all(string kho, string date)
        {
            string s = $"select *" +
                        $" from XNT_{date}" +
                        $" where MAKHOHANG ='{kho}'" +
                        $" order by MAVTU";
            return s;
        }

        public static string getExport(string fromdate, string todate)
        {
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            stringBuilder.Append("select * from");

            string s = string.Empty;
            s += "select * from";

            return stringBuilder.ToString();
        }

        public static string getBarcode(List<string> GoodIDs)
        {
            string goodIDwherein = GoodIDs.ToSQLWhereIn();
            //            return string.Format(@"Select
            //    N'''Siêu thị Thành Công' As Tensieuthi,
            //    ''''+Goods.SupplierID As Mancc,
            //    N'''Bán lẻ siêu thị' as Tennganhhang,
            //    ''''+Barcodes.GoodID As Masieuthi,
            //    ''''+Goods.ShortName as Tenmathang,
            //    ''''+Barcodes.Barcode as Barcode,
            //    N'1' as Soluong,
            //    Goods.ExpRetailPriceVat as Giabanlecovat,
            //    N'0' as Thutuin
            //From
            //    Barcodes Inner Join
            //    Goods On Goods.GoodID = Barcodes.GoodID
            //Where Barcodes.GoodID in ({0})
            //Order By
            //    Barcodes.GoodID,
            //    Len(Barcodes.Barcode)", goodIDwherein);

            return string.Format(@"Select
    N'''Siêu thị Thành Công' As Tensieuthi,
    ''''+Goods.SupplierID As Mancc,
    N'''Bán lẻ siêu thị' as Tennganhhang,
    ''''+Barcodes.GoodID As Masieuthi,
    ''''+dbo.Unicode2TCVN(Goods.ShortName) as Tenmathang,
    ''''+Barcodes.Barcode as Barcode,
    N'1' as Soluong,
    FORMAT(Goods.ExpRetailPriceVat,'#,##')+' VND' as Giabanlecovat,
    N'0' as Thutuin
From
    Barcodes Inner Join
    Goods On Goods.GoodID = Barcodes.GoodID
Where Barcodes.GoodID in ({0})
Order By
    Barcodes.GoodID,
    Len(Barcodes.Barcode)", goodIDwherein);

        }

        public static string getHangXuatTra_trancode_05(string trandateStart, string trandateEnd)
        {
            return string.Format(@"Select
                                        TransDetail.GoodID,
                                        '' as {0},
                                        Sum(TransDetail.Quantity) As SumQuantity
                                    From
                                        TransDetail Inner Join
                                        Transactions On TransDetail.TransactionID = Transactions.TransactionID
                                    Where
                                        Transactions.TransCode = '05' And
                                        Transactions.TransDate >= '{1}' And
                                        Transactions.TransDate <= '{2}'
                                    Group By
                                        TransDetail.GoodID", Forms.FormCSDL.XNT_COLUMN_MAVATTU, trandateStart, trandateEnd);
        }

        public static string GetData_ExportInner_Transcode_23(System.DateTime fromDate, System.DateTime toDate, string importID)
        {
            string fromdate = fromDate.ToString("yyyyMMdd");
            string todate = toDate.ToString("yyyyMMdd");
            return string.Format(@"Select
    '' as {0},
    TransDetail.GoodID,
    Sum(TransDetail.Quantity) As SumQuantity
From
    TransDetail Inner Join
    Transactions On TransDetail.TransactionID = Transactions.TransactionID
Where
    Transactions.TransCode = '23' And
    Transactions.TransDate >= '{1}' And
    Transactions.TransDate <= '{2}' And
    Transactions.Status = '1' And
    Transactions.ImportID = '{3}'
Group By
    TransDetail.GoodID
Order By
    TransDetail.GoodID", Forms.FormCSDL.XNT_COLUMN_MAVATTU, fromdate, todate, importID);
        }

        public static string GetData_Retail_Transcode_03(System.DateTime fromDate, System.DateTime toDate)
        {
            string fromdate = fromDate.ToString("yyyyMMdd");
            string todate = toDate.ToString("yyyyMMdd");
            return string.Format(@"Select
    '' as {0},
    RetailTranDetail.GoodID,
    Sum(RetailTranDetail.OrginalQty) As SumQuantity
From
    RetailTranDetail Inner Join
    RetailTrans On RetailTranDetail.TransactionID = RetailTrans.TransactionID
Where
    RetailTranDetail.TransDate >= '{1}' And
    RetailTranDetail.TransDate <= '{2}' And
    RetailTranDetail.Status = '1' And
    RetailTrans.TransCode = '03'
Group By
    RetailTranDetail.GoodID
Order By
    RetailTranDetail.GoodID", Forms.FormCSDL.XNT_COLUMN_MAVATTU, fromdate, todate);

        }

        public static string GetData_HangDaXuat_XBANSL_XTRALAISL(string kho, System.DateTime startDate)
        {
            string date = startDate.ToString("ddMMyyyy");
            return string.Format(@"select * from 
(select makhohang, mavtu, xbansl + xtralaisl as xuat
from XNT_{0})
where makhohang = '{1}' and xuat > 0", date, kho);
        }


        public static string Oracle_Get_XBAN(System.DateTime startDate, System.DateTime endDate)
        {
            string fromDate = startDate.ToString("dd-MMM-yy");
            string toDate = endDate.ToString("dd-MMM-yy");
            string q = string.Format(@"select gd.sohoadongd ""Số hóa đơn"",
                                        gd.ngayhachtoan ""Ngày tháng"",
                                        kh.makhachhang ""Mã KH"",
                                        kh.tenkhachhang ""Tên khách hàng"",
                                        sum(ct.tienhang + ct.tienvat) ""Số tiền"",
                                        gd.mavattugdpk

                                        from vattugd gd
                                        Join Vattugdct Ct
                                        on gd.mavattugdpk= ct.mavattugdpk
                                        join dmkhachhang kh
                                        on gd.makhachang = kh.makhachhang
                                        where gd.ngayhachtoan >= '{0}'
                                        and gd.ngayhachtoan <= '{1}'
                                        and gd.manhomptnx like 'X_%'
                                        and gd.manhomptnx not in ('X_CHUYENKHO','X_KHAC')
                                        and gd.trangthai = '10'
                                        group by gd.sohoadongd, gd.ngayhachtoan,kh.makhachhang, Kh.Tenkhachhang, gd.mavattugdpk
                                        order by gd.ngayhachtoan,cast(gd.sohoadongd as int)", fromDate, toDate);

            return q;


        }

        public static string Oracle_Get_XBAN_Detail(string MAVATTUGDPK)
        {
            string q = string.Format(@"select rownum as ""view_order"",
mavtu as ""product_code"",
tenvtu as ""product_name"",
donvile as ""unit_name"",
soluong as ""quantity"",
dongia as ""price"",
tienhang as ""amount"",
vat as ""vat"",
tienvat as ""amount_vat""
from vattugdct
where mavattugdpk = '{0}'
order by vat", MAVATTUGDPK);
            return q;
        }


        public static string Oracle_XNT_xuatBill(System.DateTime date, List<string> listMaVatTu)
        {
            string mavattu_whereIn = listMaVatTu.ToSQLWhereIn();
            string q = $"select MAVTU as \"Mã vật tư\"," +
                //$" TENVTU as \"Tên vật tư\"," +
                $" ROUND(TONCUOIKYSL) as \"Số lượng\"," +
                //$" ROUND(GIAVON) as \"Giá vốn\"," +
                //$" ROUND(TONCUOIKYGT) as \"Thành tiền\"" +
                $" from XNT_{date}" +
                //$" where MAKHOHANG ='{kho}'" +
                $" and MAVTU in ({mavattu_whereIn})" +
                $" and TONCUOIKYSL <> 0" +
                $" order by MAVTU";
            return q;
        }

        public static string Oracle_Get_CreditCardPayment(System.DateTime startDate, System.DateTime endDate)
        {
            string fromDate = startDate.ToString("dd-MMM-yy");
            string toDate = endDate.ToString("dd-MMM-yy");
            string q = string.Format(@"select 
concat(th.ngayhachtoan, ct.makhachhang) as id, sum(ct.sotien) as sotien
from quyctktct ct
join quyctkt th on ct.mactktpk = th.mactktpk
where
ct.matkno like '112%'
and ct.matkco = '1311'
and ct.makhachhang = 'KLE000001'
and th.ngayhachtoan >= '{0}'
and th.ngayhachtoan <= '{1}'

group by th.ngayhachtoan, ct.makhachhang
order by th.ngayhachtoan", fromDate, toDate);

            return q;
        }

        public static string Orale_get_XBAN_only(System.DateTime startDate, System.DateTime endDate)
        {
            string fromDate = startDate.ToString("dd-MMM-yy");
            string toDate = endDate.ToString("dd-MMM-yy");
            string q = string.Format(@"select
concat(ngayhachtoan, makhachang), sum(sumtienhang + sumtienvat), makhachang, tenkhachhang
  from vattugd
  where ngayhachtoan >= '01-JAN-21'
and ngayhachtoan <= '31-JAN-21'
and manhomptnx = 'X_BAN'
and makhachang not in ('KH-CTY000717', 'KH-CTY000808')

group by concat(ngayhachtoan, makhachang), makhachang, tenkhachhang
--order by ngayhachtoan");
            return q;
        }

        public static string Oracle_AllinOne_Create_Receipt(System.DateTime startDate, System.DateTime endDate)
        {
            string fromDate = startDate.ToString("dd-MMM-yy");
            string toDate = endDate.ToString("dd-MMM-yy");
            string q = string.Format(@"with hoadonbanra as
(select
th.ngayhachtoan,
concat(th.ngayhachtoan, th.makhachang) as id,
sum(ct.tienhang + ct.tienvat) as sotien,
th.makhachang,
dmkh.tenkhachhang
from vattugd th
join vattugdct ct
on th.mavattugdpk = ct.mavattugdpk
Join Dmkhachhang Dmkh
on th.makhachang = dmkh.makhachhang
where th.ngayhachtoan >= '{0}'
and th.ngayhachtoan <= '{1}'
and th.manhomptnx = 'X_BAN'
and th.makhachang not in ('KH-CTY000717', 'KH-CTY000808','KH-CTY00590','KH-CTY00602')
group by th.ngayhachtoan, concat(th.ngayhachtoan, th.makhachang), th.makhachang, dmkh.tenkhachhang
order by th.ngayhachtoan)
,
cardpayment as 
(select 
concat(th.ngayhachtoan, ct.makhachhang) as id, sum(ct.sotien) as sotien
from quyctktct ct
join quyctkt th on ct.mactktpk = th.mactktpk
where
ct.matkno like '112%'
and ct.matkco = '1311'
and ct.makhachhang = 'KLE000001'
and th.ngayhachtoan >= '{0}'
and th.ngayhachtoan <= '{1}'
and th.trangthaict = '1'
group by th.ngayhachtoan, ct.makhachhang
order by th.ngayhachtoan)

select 
hoadonbanra.ngayhachtoan,
hoadonbanra.id,
hoadonbanra.sotien - NVL(cardpayment.sotien,0) as ""Số tiền"",
hoadonbanra.makhachang,
hoadonbanra.tenkhachhang
from hoadonbanra
left join cardpayment
on hoadonbanra.id = cardpayment.id
order by hoadonbanra.ngayhachtoan,hoadonbanra.makhachang", fromDate, toDate);
            return q;
        }

        public static string Oracle_product_detail(System.DateTime date, List<string> productCode)
        {
            string d = date.ToString("ddMMyyyy");
            string productCode_wherein = productCode.ToSQLWhereIn();
            string q = string.Format(@"Select 
makhohang,
Mavtu,
Tenvtu,
Tondaukysl,
nhapmuasl,
nchuyenkhosl,
Xbansl,
Xtralaisl,
xchuyenkhosl,
Xkhacsl,
Toncuoikysl
--Tondaukysl+nhapmuasl+Xbansl+Xkhacsl+Toncuoikysl
From Xnt_{0}
Where Mavtu In ({1})
and Tondaukysl+nhapmuasl+nchuyenkhosl+Xbansl+Xtralaisl+xchuyenkhosl+Xkhacsl+Toncuoikysl <> 0
order by 1,2", d, productCode_wherein);
            return q;


        }


        public static string MSSQL_GetDataExportSTLTTAN(System.DateTime startDate, System.DateTime endDate)
        {
            string q = string.Format(@"select
	                                        TransDetail.GoodID,
	                                        '' as MAVATTU,
	                                        sum(TransDetail.Quantity) as SumQuantity
                                        from
	                                        TransDetail
                                        join
	                                        Transactions
                                        on
	                                        TransDetail.TransactionID = Transactions.TransactionID
                                        where
	                                        Transactions.TransDate >= '{0}'
	                                        and Transactions.TransDate <= '{1}'
	                                        and Transactions.TransCode = '23'
	                                        and Transactions.Status = '1'
	                                        and Transactions.ImportID = 'STLTTAN'
                                        group by
	                                        TransDetail.GoodID", startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd"));
            return q;
        }






    }

}
