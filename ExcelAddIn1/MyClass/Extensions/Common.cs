﻿namespace ExcelAddIn1.MyClass.Extensions
{
    public static class Common
    {
        public static T ConvertTo<T>(object obj)
        {
            return (T)System.Convert.ChangeType(obj, typeof(T));
        }
    }
}
