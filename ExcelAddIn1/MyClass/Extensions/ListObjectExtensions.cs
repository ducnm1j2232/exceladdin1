﻿using Microsoft.Office.Interop.Excel;

namespace ExcelAddIn1.MyClass.Extensions
{
    public static class ListObjectExtensions
    {
        

        

        public static void AddRange(this ListColumns listColumn, params string[] columnNames)
        {
            foreach (string name in columnNames)
            {
                listColumn.Add().Name = name;
            }
        }

        /// <summary>
        /// Return Data body range only
        /// </summary>
        /// <param name="listObject"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public static Range GetColumnRange(this ListObject listObject, string columnName)
        {
            if (listObject == null)
            {
                throw new System.Exception("list object was null");
            }

            if (string.IsNullOrEmpty(columnName))
            {
                throw new System.Exception("column name was null or empty");
            }

            return listObject.ListColumns[columnName].DataBodyRange;
        }





    }
}
