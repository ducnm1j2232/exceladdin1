﻿using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using VSTOLib.Extensions;

namespace ExcelAddIn1.MyClass.Extensions
{
    public static class RangeExtensions
    {
        public static IList<IList<object>> ToListList(this Range range, bool firstRowAsHeader = true)
        {
            if (range.IsSingleCell())
            {
                List<IList<object>> result = new List<IList<object>>();
                result.Add(new List<object>() { range.Value });
                return result;
            }
            else
            {
                return range.ToDataTable(firstRowAsHeader).ToListList();
            }
        }

        public static T ToType<T>(this Range range)
        {
            return (T)Common.ConvertTo<T>(range.Value);
        }

        public static ListObject ToListObject(this Range range, string tableName = "")
        {
            Worksheet sheet = range.Worksheet;
            foreach (ListObject obj in sheet.ListObjects)
            {
                if (obj.Range.Address == range.Address)
                {
                    return obj;
                }
            }

            ListObject listObject = sheet.ListObjects.AddEx(XlListObjectSourceType.xlSrcRange, range, null, XlYesNoGuess.xlYes);
            listObject.TableStyle = "";
            if (!string.IsNullOrEmpty(tableName))
            {
                listObject.Name = tableName;
            }
            return listObject;
        }

        /// <summary>
        /// absolute = false
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public static string Address2(this Range range)
        {
            return range.Address[false, false];
        }

    }
}
