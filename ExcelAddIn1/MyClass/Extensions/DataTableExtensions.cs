﻿using System;
using System.Collections.Generic;

namespace ExcelAddIn1.MyClass.Extensions
{
    public static class DataTableExtensions
    {
        //public static bool HasData(this System.Data.DataTable dataTable)
        //{
        //    return dataTable != null && dataTable.Rows.Count > 0;
        //}

        public static IList<IList<object>> ToListList(this System.Data.DataTable dataTable)
        {
            //IList<object> eachRow = new List<object>();
            IList<IList<object>> result = new List<IList<object>>();

            for (int rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
            {
                IList<object> eachRow = new List<object>();
                for (int columnIndex = 0; columnIndex < dataTable.Columns.Count; columnIndex++)
                {
                    eachRow.Add(dataTable.Rows[rowIndex][columnIndex].ToString());
                }
                result.Add(eachRow);
            }

            return result;
        }

        public static void ConvertColumns(this System.Data.DataTable dataTable, string columnName, Type toType)
        {
            for (int rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
            {
                if (toType == typeof(string))
                {
                    dataTable.Rows[rowIndex][columnName] = Convert.ToString(dataTable.Rows[rowIndex][columnName]);
                }

                if (toType == typeof(int))
                {
                    dataTable.Rows[rowIndex][columnName] = Convert.ToInt32(dataTable.Rows[rowIndex][columnName]);
                }

                if (toType == typeof(double))
                {
                    dataTable.Rows[rowIndex][columnName] = Convert.ToDouble(dataTable.Rows[rowIndex][columnName]);
                }
            }
        }
    }
}
