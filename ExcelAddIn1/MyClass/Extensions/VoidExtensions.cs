﻿using System;
using System.Threading.Tasks;

namespace ExcelAddIn1.MyClass.Extensions
{
    public static class VoidExtensions
    {
        public static bool RunSuccessfully(this Action action, System.TimeSpan timeout)
        {
            System.Threading.Tasks.Task task = Task.Run(action);
            if (!task.Wait(timeout))
            {
                return false;
            }
            return true;
        }
    }
}
