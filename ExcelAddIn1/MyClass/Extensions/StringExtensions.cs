﻿using System;

namespace ExcelAddIn1.MyClass.Extensions
{
    public static class StringExtensions
    {
        public static bool StartWithList(this string s, params string[] listValue)
        {
            bool result = false;
            foreach (string value in listValue)
            {
                if (s.StartsWith(value))
                {
                    result = true;
                }
            }
            return result;
        }

        public static string GetStringBetween(this string source, string string1, string string2)
        {
            string result = string.Empty;
            try
            {
                //    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex($"{string1}(.*){string2}");//string1 (.*) string2
                //    System.Text.RegularExpressions.Match match = regex.Match(source);
                //    if (match.Success)
                //    {
                //        result = Convert.ToString(match.Groups[1].Value);
                //        //https://stackoverflow.com/questions/5642315/regular-expression-to-get-a-string-between-two-strings-in-javascript
                //        //When you get the result of the match, you need to extract the matched text of the first capturing group with matched[1], not the whole matched text with matched[0]
                //    }

                int fromPos = source.IndexOf(string1) + string1.Length;
                int toPos = source.IndexOf(string2);
                result = source.Substring(fromPos, toPos - fromPos);
            }


            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, Public.Variables.AddinName);
            }
            return result;
        }

        public static string GetNumberDigitOnly(this string source)
        {
            return System.Text.RegularExpressions.Regex.Match(source, @"\d+").Value;
            //return System.Text.RegularExpressions.Regex.Match(source, @"^-?[0-9]\d*(\.\d+)?$").Value; //positive and negative number
        }



        public static string CapitalizeAllLetter(this string str)
        {
            string result = string.Empty;
            foreach (char c in str)
            {
                result += char.ToUpper(c);
            }
            return result;
        }
        public static string LowerAllLetter(this string str)
        {
            string result = string.Empty;
            foreach (char c in str)
            {
                result += char.ToLower(c);
            }
            return result;
        }

        public static string CapitalizeFirstLetterEachWord(this string str)
        {
            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            return textInfo.ToTitleCase(str);
        }

    }
}
