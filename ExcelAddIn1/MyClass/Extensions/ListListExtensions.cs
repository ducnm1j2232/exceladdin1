﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExcelAddIn1.MyClass.Extensions
{
    public static class ListListExtensions
    {
        public static System.Data.DataTable ToDataTale(this IList<System.Collections.Generic.IList<object>> listList, bool firstRowAsHeader = true)
        {
            System.Data.DataTable result = new System.Data.DataTable();
            List<string> columns = listList.First().ToList().Select(x => x.ToString()).ToList();
            columns.ForEach(x => result.Columns.Add(x.ToString()));

            foreach (List<object> row in listList.Skip(1))
            {
                result.Rows.Add(row.ToArray());
            }
            return result;
        }

        public static string[,] To2DArray<T>(this IList<IList<T>> source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source null");
            }

            int max = source.Select(l => l).Max(l => l.Count());
            var result = new string[source.Count, max];

            for (int i = 0; i < source.Count; i++)
            {
                for (int j = 0; j < source[i].Count(); j++)
                {
                    result[i, j] = source[i][j].ToString();
                }
            }

            return result;
        }
    }
}
