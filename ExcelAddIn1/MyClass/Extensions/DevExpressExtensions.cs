﻿using System.Collections.Generic;
using System.Linq;
using VSTOLib.Extensions;

namespace ExcelAddIn1.MyClass.Extensions
{
    public static class DevExpressExtensions
    {
        /// <summary>
        /// <para>because some devexpress control return system.DBNull, not null</para>
        /// <para>Return string.Empty instead of System.DBNull</para>
        /// </summary>
        /// <param name="valueToGet"></param>
        /// <returns></returns>
        public static object GetControlValue(object valueToGet)
        {
            if (valueToGet == System.DBNull.Value)
            {
                return string.Empty;
            }
            else return valueToGet;
        }

        public static int[] GetCheckedRows(this DevExpress.XtraGrid.Views.Grid.GridView gridView)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < gridView.DataRowCount; i++)
            {
                if (gridView.IsRowSelected(i))
                {
                    result.Add(i);
                }
            }
            return result.ToArray();
        }

        /// <summary>
        ///with = 0 : BestFit
        /// </summary>
        /// <param name="gridView"></param>
        /// <param name="width"></param>
        /// <param name="columnsNames"></param>
        public static void SetBestFitColumns(this DevExpress.XtraGrid.Views.Grid.GridView gridView, int width = 0, params string[] columnsNames)
        {
            foreach (string columnName in columnsNames)
            {
                if (width == 0)
                {
                    //gridView.Columns[columnName].BestFit();
                    gridView.Columns[columnName].Width = gridView.Columns[columnName].GetBestWidth();
                }
                else
                {
                    gridView.Columns[columnName].Width = width;
                }
                gridView.Columns[columnName].OptionsColumn.FixedWidth = true;
            }
            gridView.LayoutChanged();

        }

        public static List<string> GetSelectedValue(this DevExpress.XtraGrid.Views.Grid.GridView gridView, string columnName)
        {
            List<string> result = new List<string>();
            foreach (int rowHandle in gridView.GetSelectedRows())
            {
                result.Add(gridView.GetRowCellValue(rowHandle, columnName).ToString());
            }
            return result;

        }

        public static void SetRowColor(this DevExpress.XtraGrid.Views.Grid.GridView gridView, System.Drawing.Color color, params int[] rows)
        {
            gridView.RowStyle += (sender, e) =>
            {
                foreach (int row in rows)
                {
                    if (e.RowHandle == row)
                    {
                        e.Appearance.BackColor = color;
                        e.HighPriority = true;
                    }
                }
            };
            gridView.LayoutChanged();
        }

        public static void SetCellColor(this DevExpress.XtraGrid.Views.Grid.GridView gridView, System.Drawing.Color color, int rowHandle, string columnName)
        {
            gridView.RowCellStyle += (sender, e) =>
            {
                if (e.RowHandle == rowHandle && e.Column.FieldName == columnName)
                {
                    e.Appearance.BackColor = color;
                }
            };
            gridView.LayoutChanged();
        }

        public static System.Drawing.Color GetCellBackColor(this DevExpress.XtraGrid.Views.Grid.GridView gridView, int rowHandle, string columnName)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo gridViewInfo = gridView.GetGridViewInfo();
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridCellInfo gridCellInfo = gridViewInfo.GetGridCellInfo(rowHandle, gridView.Columns[columnName]);
            return gridCellInfo.Appearance.BackColor;

        }

        public static void AddButtonEdit(this DevExpress.XtraGrid.Views.Grid.GridView gridView, string columnName, out DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit itemButtonEdit, params DevExpress.XtraEditors.Controls.EditorButton[] editorButtons)
        {
            DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repoItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            itemButtonEdit = repoItemButtonEdit;
            itemButtonEdit.Buttons.Clear();

            foreach (DevExpress.XtraEditors.Controls.EditorButton editorButton in editorButtons)
            {
                itemButtonEdit.Buttons.Add(editorButton);
            }

            DevExpress.XtraGrid.GridControl gridControl = gridView.GridControl;
            DevExpress.XtraGrid.Columns.GridColumn column = gridView.Columns[columnName];
            column.ColumnEdit = itemButtonEdit;
            column.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;

            gridView.GridControl.RepositoryItems.Add(itemButtonEdit);

            gridControl.SafeInvoke(() =>
            {
                column.Width = column.GetBestWidth();
                column.OptionsColumn.FixedWidth = true;
            });

        }

        public static DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit AddButtonEdit(this DevExpress.XtraGrid.Views.Grid.GridView gridView, string columnName, params DevExpress.XtraEditors.Controls.EditorButton[] editorButtons)
        {
            DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repoItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            repoItemButtonEdit.Buttons.Clear();

            foreach (DevExpress.XtraEditors.Controls.EditorButton editorButton in editorButtons)
            {
                repoItemButtonEdit.Buttons.Add(editorButton);
            }

            DevExpress.XtraGrid.GridControl gridControl = gridView.GridControl;
            DevExpress.XtraGrid.Columns.GridColumn column = gridView.Columns[columnName];
            column.ColumnEdit = repoItemButtonEdit;
            column.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;

            gridView.GridControl.RepositoryItems.Add(repoItemButtonEdit);

            gridControl.SafeInvoke(() =>
            {
                column.Width = column.GetBestWidth();
                column.OptionsColumn.FixedWidth = true;
            });

            return repoItemButtonEdit;

        }

        public static DevExpress.XtraEditors.Repository.RepositoryItemComboBox AddComboboxButton(this DevExpress.XtraGrid.Views.Grid.GridView gridView, string columnName, ICollection<object> items)
        {
            DevExpress.XtraEditors.Controls.EditorButton editorButton = new DevExpress.XtraEditors.Controls.EditorButton();
            editorButton.Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Combo;

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox comboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            comboBox.Buttons.Clear();
            comboBox.Buttons.Add(editorButton);
            comboBox.Items.AddRange(items.ToArray<object>());

            DevExpress.XtraGrid.Columns.GridColumn column = gridView.Columns[columnName];
            column.ColumnEdit = comboBox;
            gridView.GridControl.RepositoryItems.Add(comboBox);
            column.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;

            return comboBox;
        }

        public static DevExpress.XtraGrid.Views.Grid.GridView GetGridView(object sender)
        {
            DevExpress.XtraEditors.BaseEdit editor = sender as DevExpress.XtraEditors.BaseEdit;
            DevExpress.XtraGrid.GridControl grid = editor.Parent as DevExpress.XtraGrid.GridControl;
            DevExpress.XtraGrid.Views.Grid.GridView gridView = grid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            return gridView;
        }

        public static DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo GetGridViewInfo(this DevExpress.XtraGrid.Views.Grid.GridView gridView)
        {
            System.Reflection.FieldInfo fieldInfo = typeof(DevExpress.XtraGrid.Views.Grid.GridView).GetField("fViewInfo", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo gridViewInfo = fieldInfo.GetValue(gridView) as DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo;
            return gridViewInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gridView"></param>
        /// <param name="formatType"></param>
        /// <param name="formatString">number "{0:N0}"</param>
        public static void SetNumberFormat(this DevExpress.XtraGrid.Columns.GridColumn gridColumn, DevExpress.Utils.FormatType formatType, string formatString)
        {
            if (gridColumn != null)
            {
                gridColumn.DisplayFormat.FormatType = formatType;
                gridColumn.DisplayFormat.FormatString = formatString;
            }
        }

        /// <summary>
        /// Set number format default is numeric, format string is "{0:N0}"
        /// </summary>
        /// <param name="gridColumn"></param>
        public static void SetNumberFormat(this DevExpress.XtraGrid.Columns.GridColumn gridColumn)
        {
            SetNumberFormat(gridColumn, DevExpress.Utils.FormatType.Numeric, "{0:N0}");
        }

        public static object GetColumnValueFromLookupEdit(this DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit, int rowHandle, string columnName)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gridView = searchLookUpEdit.Properties.View;
            object value = gridView.GetRowCellValue(rowHandle, columnName);
            return value;
        }

        public static void ApplyDuplicateFormating(this DevExpress.XtraGrid.Views.Grid.GridView gridView, string columnName, System.Drawing.Color color)
        {
            DevExpress.XtraGrid.GridFormatRule formatRule = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleUniqueDuplicate formatConditionRuleUniqueDuplicate = new DevExpress.XtraEditors.FormatConditionRuleUniqueDuplicate();
            formatConditionRuleUniqueDuplicate.Appearance.BackColor = color;
            formatRule.Rule = formatConditionRuleUniqueDuplicate;
            formatRule.ColumnApplyTo = formatRule.Column = gridView.Columns[columnName];
            gridView.FormatRules.Add(formatRule);
        }

        public static void SetMultiSelectCheckbox(this DevExpress.XtraGrid.Views.Grid.GridView gridView)
        {
            gridView.OptionsSelection.MultiSelect = true;
            gridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            gridView.OptionsSelection.CheckBoxSelectorColumnWidth = 25;
        }

        public static void AddSumaryItem(this DevExpress.XtraGrid.Columns.GridColumn gridColumn, DevExpress.Data.SummaryItemType type)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gridView = (DevExpress.XtraGrid.Views.Grid.GridView)gridColumn.View;
            gridView.OptionsView.ShowFooter = true;
            string columnName = gridColumn.FieldName;
            DevExpress.XtraGrid.GridColumnSummaryItem summaryItem = new DevExpress.XtraGrid.GridColumnSummaryItem(type, columnName, "{0:N0}");

            if (type == DevExpress.Data.SummaryItemType.Sum)
            {
                //summaryItem.SetSummary(type, "{0:N0}");
                summaryItem.Mode = DevExpress.Data.SummaryMode.Mixed;
                //summaryItem = new DevExpress.XtraGrid.GridColumnSummaryItem(type, DevExpress.Data.SummaryMode.Mixed, columnName, "{0:N0}");
            }

            gridColumn.Summary.Clear();
            gridColumn.Summary.Add(summaryItem);
        }

    }
}
