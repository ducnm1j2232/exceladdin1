﻿using Microsoft.Office.Interop.Excel;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1
{
    public class ADO
    {
        public static async Task<bool> TestSQLConnection(string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    await conn.OpenAsync().ConfigureAwait(false);
                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }

        public static async Task<bool> TestOracleConnection(string connectionString)
        {
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    await conn.OpenAsync();
                    return true;
                }
                catch (OracleException)
                {
                    return false;
                }
            }
        }


        public static string ConvertDateToTransDate(DateTime dateTime)
        {
            string day = dateTime.Day.ToString("00");
            string month = dateTime.Month.ToString("00");
            string year = dateTime.Year.ToString("0000");
            return year + month + day;
        }

        public static void CopyDataTableToRange(DataTable dataTable, Range range, bool header)
        {

            if (header == true)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    range.Offset[0, i].Value2 = dataTable.Columns[i].ColumnName;
                }
                range.Offset[1, 0].Resize[dataTable.Rows.Count, dataTable.Columns.Count].Value2 = ConvertDatatableTo2dStringArray(dataTable);
            }
            else
            {
                range.Resize[dataTable.Rows.Count, dataTable.Columns.Count].Value2 = ConvertDatatableTo2dStringArray(dataTable);
            }
        }

        public static void AddDataBaseNameToComboBox(string ip, string user, string pass, ComboBox combobox)
        {
            string connectionString = $"Server={ip};" +
                                    $"User Id={user};" +
                                    $"Password = {pass};";
            combobox.SelectedIndex = -1;
            combobox.Items.Clear();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand("select name from sys.databases", connection))
                    {
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                combobox.Items.Add(dr[0].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static DataTable GetDataTableFromStoredProcedure(string connectionString, string StoredProcedureName, List<SqlParameter> sqlParameters)
        {
            DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(StoredProcedureName, connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddRange(sqlParameters.ToArray());
                command.CommandTimeout = 300;//5 phút
                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(ds);
                return ds.Tables[0];
            }

        }

        public static async Task<DataTable> GetDataTableFromStoredProcedureAsync(string connectionString, string StoredProcedureName, List<SqlParameter> sqlParameters)
        {
            DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(StoredProcedureName, connection))
                {
                    await Task.Delay(0).ConfigureAwait(false);
                    command.CommandType = CommandType.StoredProcedure;
                    if (sqlParameters != null)
                    {
                        command.Parameters.AddRange(sqlParameters.ToArray());
                    }
                    command.CommandTimeout = 300;//5 phút, đợi lâu lâu tí. tránh trường hợp chưa tính toán xong đã hết thời gian

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(ds);
                        return ds.Tables[0];
                    }
                }
            }

        }

        public static string[,] ConvertDatatableTo2dStringArray(DataTable dataTable)
        {
            string[,] result = new string[dataTable.Rows.Count, dataTable.Columns.Count];
            for (int rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
            {
                for (int colIndex = 0; colIndex < dataTable.Columns.Count; colIndex++)
                {
                    result[rowIndex, colIndex] = dataTable.Rows[rowIndex][colIndex].ToString();
                }
            }
            return result;
        }

        public static DataTable GetDataTableFromSQLServer(string connectionString, string SQLquery)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    try
                    {
                        connection.Open();

                        DataTable dt = new DataTable();
                        da.SelectCommand = new SqlCommand(SQLquery, connection);
                        da.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return null;
                    }
                }
            }
        }

        public static async Task<DataTable> GetDataTableFromSQLServerAsync(string connectionString, string SQLquery)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    using (DataTable dt = new DataTable())
                    {
                        try
                        {
                            await connection.OpenAsync().ConfigureAwait(false);
                            da.SelectCommand = new SqlCommand(SQLquery, connection);
                            da.Fill(dt);
                            return dt;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            return null;
                        }
                    }
                }
            }


        }

        public static async Task<DataTable> GetDataTableFromSQLServerUsingDataReaderAsync(string connectionString, string SQLquery)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (DataTable dt = new DataTable())
                {
                    try
                    {
                        using (SqlCommand sqlCommand = new SqlCommand(SQLquery, connection))
                        {
                            await connection.OpenAsync().ConfigureAwait(false);
                            SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync().ConfigureAwait(false);
                            dt.Load(sqlDataReader);
                            return dt;
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return null;
                    }
                }

            }


        }

        public static async Task<DataTable> GetDataTableFromSQLServerAsync(string IPserver, string database, string username, string password, string SQLquery)
        {
            string connectionString = $"Data Source = {IPserver};" +
                                    $"Initial Catalog = {database};" +
                                    $"User ID = {username};" +
                                    $"Password = {password}";
            DataTable dataTable = await GetDataTableFromSQLServerAsync(connectionString, SQLquery).ConfigureAwait(false);
            return dataTable;

        }

        public static void WriteDataToExcelFile(DataTable dt, string fileName)
        {

            string connectionString = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + fileName + "; Extended Properties = \"Excel 12.0 Xml;HDR=YES\";";
            //string connectionString = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = " + fileName + "; Extended Properties = \"Excel 8.0;HDR=YES\";";

            if (System.IO.File.Exists(fileName)) System.IO.File.Delete(fileName);

            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionString))
            {
                oleDbConnection.Open();

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendFormat("CREATE TABLE [{0}] (", dt.TableName);
                foreach (DataColumn dc in dt.Columns)
                {
                    stringBuilder.AppendFormat("[{0}] {1}{2}", dc.ColumnName, GetColumnType(dc), (dc.Ordinal == (dt.Columns.Count - 1)) ? ")" : ",");
                }

                using (OleDbCommand oleDbCommand = new OleDbCommand())
                {
                    oleDbCommand.Connection = oleDbConnection;
                    oleDbCommand.CommandText = stringBuilder.ToString();
                    oleDbCommand.ExecuteNonQuery();

                    using (OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter(string.Format("select * from [{0}]", dt.TableName), oleDbConnection))
                    {
                        using (OleDbCommandBuilder oleDbCommandBuilder = new OleDbCommandBuilder(oleDbDataAdapter))
                        {
                            oleDbCommandBuilder.QuotePrefix = "[";
                            oleDbCommandBuilder.QuoteSuffix = "]";
                            try
                            {
                                oleDbDataAdapter.InsertCommand = oleDbCommandBuilder.GetInsertCommand();
                                oleDbDataAdapter.Update(dt);
                            }
                            catch { }
                        }
                    }
                }
            }



        }

        private static string GetColumnType(DataColumn dc)
        {
            string colType = dc.DataType.ToString();
            if ((colType != null) && (((colType == "System.Int64") || (colType == "System.Double")) || (colType == "System.Int32")))
            {
                return "NUMERIC";
            }
            return "TEXT";
        }

        public static async Task<DataTable> GetDataFromExcelFileAsync(string excel_file_fullname, string SQL_statement)
        {
            using (OleDbConnection connection = new OleDbConnection())
            {
                DataTable dt = new DataTable();
                string excelFileName = excel_file_fullname;
                connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                                            + excelFileName
                                            + @";Extended Properties=""Excel 12.0;HDR=YES;IMEX=1"";";
                using (OleDbCommand command = new OleDbCommand())
                {
                    command.CommandText = SQL_statement;
                    command.Connection = connection;
                    using (OleDbDataAdapter da = new OleDbDataAdapter())
                    {
                        await connection.OpenAsync().ConfigureAwait(false);
                        da.SelectCommand = command;
                        da.Fill(dt);
                        //await Task.Delay(0);
                        return dt;
                    }
                }
            }
        }

        public static async Task ExecuteQueryExcelFileAsync(string excel_file_fullname, string SQL_statement)
        {
            using (OleDbConnection connection = new OleDbConnection())
            {
                string excelFileName = excel_file_fullname;
                connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                                            + excelFileName
                                            + @";Extended Properties=""Excel 12.0;HDR=YES"";";
                using (OleDbCommand command = new OleDbCommand())
                {
                    command.CommandText = SQL_statement;
                    command.Connection = connection;
                    await connection.OpenAsync().ConfigureAwait(false);
                    await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                }
            }
        }

        public static async Task ExecuteQueryAccessFileAsync(string fileName, string query)
        {
            using (OleDbConnection connection = new OleDbConnection())
            {
                connection.ConnectionString = $"Provider=Microsoft.ACE.OLEDB.12.0;" +
                                                $"Data Source={fileName};" +
                                                $"Persist Security Info=False;";
                using (OleDbCommand command = new OleDbCommand())
                {
                    command.CommandText = query;
                    command.Connection = connection;
                    await connection.OpenAsync().ConfigureAwait(false);
                    await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                }
            }
        }


        public static async Task<DataTable> GetDataFromAccessFileAsync(string fileName, string query)
        {
            using (OleDbConnection connection = new OleDbConnection())
            {
                DataTable dt = new DataTable();
                connection.ConnectionString = $"Provider=Microsoft.ACE.OLEDB.12.0;" +
                                                $"Data Source={fileName};" +
                                                $"Persist Security Info=False;";
                using (OleDbCommand command = new OleDbCommand())
                {
                    //command.CommandType = CommandType.Text;
                    command.CommandText = query;
                    command.Connection = connection;
                    using (OleDbDataAdapter da = new OleDbDataAdapter())
                    {
                        await connection.OpenAsync().ConfigureAwait(false);
                        da.SelectCommand = command;
                        da.Fill(dt);
                        return dt;
                    }
                }
            }
        }




        public static async Task<DataTable> GetDataTableFromOracleServerAsync(string query)
        {
            OracleConnectionStringBuilder stringBuilder = new OracleConnectionStringBuilder();
            stringBuilder.DataSource = "192.168.0.50:1522/TBNETERP";
            stringBuilder.UserID = "TBNETERP";
            stringBuilder.Password = "TBNETERP";

            using (OracleConnection oracleConnection = new OracleConnection(stringBuilder.ConnectionString))
            {
                try
                {
                    await Task.Delay(0);
                    using (OracleCommand oracleCommand = new OracleCommand(query, oracleConnection))
                    {
                        using (OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(oracleCommand))
                        {
                            DataTable dataTable = new DataTable();
                            oracleDataAdapter.Fill(dataTable);
                            return dataTable;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return null;
                }
            }

        }

        public static async Task<DataTable> GetDataTableFromOracleServerAsync(string connectionString, string query)
        {
            using (OracleConnection oracleConnection = new OracleConnection(connectionString))
            {
                try
                {
                    using (OracleCommand oracleCommand = new OracleCommand(query, oracleConnection))
                    {
                        using (OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(oracleCommand))
                        {
                            await oracleConnection.OpenAsync().ConfigureAwait(false);
                            DataTable dataTable = new DataTable();
                            oracleDataAdapter.Fill(dataTable);
                            return dataTable;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return null;
                }
            }

        }






    }
}
