﻿using System;
using System.Windows.Forms;

namespace ExcelAddIn1.MyClass
{
    public class MsgBox
    {
        public static void Show(string text)
        {
            NativeWindow xlMain = new NativeWindow();
            System.IntPtr intPtr = new IntPtr(Globals.ThisAddIn.Application.Hwnd);
            xlMain.AssignHandle(intPtr);
            ExcelAddIn1.AutoTest.WinAppDriver.Method.SetForegroundWindow(intPtr);
            MessageBox.Show(xlMain, text);
            xlMain.ReleaseHandle();
        }
    }
}
