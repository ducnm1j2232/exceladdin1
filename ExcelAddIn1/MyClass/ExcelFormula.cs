﻿using Microsoft.Office.Interop.Excel;

namespace ExcelAddIn1
{
    public static class ExcelFormula
    {
        public static string FullAddress(this Range range, bool containWorbook = true,bool containWorksheet = true)
        {
            Worksheet worksheet = range.Worksheet;
            Workbook workbook = (Workbook)worksheet.Parent;

            string workbookPart = containWorbook ? $"[{workbook.Name}]" : string.Empty;
            string worksheetPart = containWorksheet ? $"{worksheet.Name}" : string.Empty;
            string rangePart = $"{range.Address[false, false]}";

            string result = $"'{workbookPart}{worksheetPart}'!{rangePart}";
            return result;

            //return $"'[{workbook.Name}]{worksheet.Name}'!{range.Address[false, false]}";
        }

        public static string VLOOKUP(Range lookup_value, Range table_array, int col_index, int range_lookup = 0, dynamic valueIfError = null)
        {
            // '[Workbook_name]Sheet_name'!Cell_address

            //Lookup Value
            string lookup_value_workbookname = ((Workbook)lookup_value.Worksheet.Parent).Name;
            string lookup_value_worksheetname = lookup_value.Worksheet.Name;
            string lookup_value_address = lookup_value.Address[false, false];
            string LookupValue = $"'[{lookup_value_workbookname}]{lookup_value_worksheetname}'!{lookup_value_address}";
            //string LookupValue = $"'{lookup_value_worksheetname}'!{lookup_value_address }";

            //Table Array
            string table_array_workbookname = ((Workbook)table_array.Worksheet.Parent).Name;
            string table_array_worksheetname = table_array.Worksheet.Name;
            string table_array_address = table_array.Address;
            string TableArray = $"'[{table_array_workbookname}]{table_array_worksheetname}'!{table_array_address}";
            //string TableArray = $"'{table_array_worksheetname}'!{table_array_address}";

            if (valueIfError != null)
            {
                return $"IFERROR(VLOOKUP({LookupValue},{TableArray},{col_index},{range_lookup}),{valueIfError})";
            }
            else
            {
                return $"VLOOKUP({LookupValue},{TableArray},{col_index},{range_lookup})";
            }
        }


        public static string SUMIF(Range range, dynamic criteria, Range sum_range = null)
        {
            if (criteria is Range) criteria = ((Range)criteria).FullAddress();
            return $"SUMIF({range.FullAddress()},{criteria},{sum_range.FullAddress()})";
        }














    }
}
