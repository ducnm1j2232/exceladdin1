﻿using System.Linq;
using System.Text;

namespace ExcelAddIn1.MyClass
{
    class Converter
    {
        private static char[] tcvnchars = {
        'µ', '¸', '¶', '·', '¹',
        '¨', '»', '¾', '¼', '½', 'Æ',
        '©', 'Ç', 'Ê', 'È', 'É', 'Ë',
        '®', 'Ì', 'Ð', 'Î', 'Ï', 'Ñ',
        'ª', 'Ò', 'Õ', 'Ó', 'Ô', 'Ö',
        '×', 'Ý', 'Ø', 'Ü', 'Þ',
        'ß', 'ã', 'á', 'â', 'ä',
        '«', 'å', 'è', 'æ', 'ç', 'é',
        '¬', 'ê', 'í', 'ë', 'ì', 'î',
        'ï', 'ó', 'ñ', 'ò', 'ô',
        '­', 'õ', 'ø', 'ö', '÷', 'ù',
        'ú', 'ý', 'û', 'ü', 'þ',
        '¡', '¢', '§', '£', '¤', '¥', '¦'
    };

        private static char[] unichars = {
        'à', 'á', 'ả', 'ã', 'ạ',
        'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ',
        'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ',
        'đ', 'è', 'é', 'ẻ', 'ẽ', 'ẹ',
        'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ',
        'ì', 'í', 'ỉ', 'ĩ', 'ị',
        'ò', 'ó', 'ỏ', 'õ', 'ọ',
        'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ',
        'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ',
        'ù', 'ú', 'ủ', 'ũ', 'ụ',
        'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự',
        'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ',
        'Ă', 'Â', 'Đ', 'Ê', 'Ô', 'Ơ', 'Ư'
    };

        private static char[] convertTable;

        static Converter()
        {
            convertTable = new char[256];
            for (int i = 0; i < 256; i++)
                convertTable[i] = (char)i;
            for (int i = 0; i < tcvnchars.Length; i++)
                convertTable[tcvnchars[i]] = unichars[i];
        }

        public static bool ContainTCVN3(string value)
        {
            try// bắt lỗi value bị null do Cell rỗng
            {
                char[] chars = value.ToCharArray();
                for (int i = 0; i < chars.Length; i++)
                {
                    if (tcvnchars.Contains(chars[i])) return true;
                }
            }
            catch { }
            return false;

        }

        public static string TCVN3ToUnicode(string value)
        {
            char[] chars = value.ToCharArray();
            for (int i = 0; i < chars.Length; i++)
                if (chars[i] < (char)256)
                    chars[i] = convertTable[chars[i]];
            return new string(chars);
        }

        public static string UnicodetoTCVN_old(string strUnicode)
        {
            string str1 = string.Empty;
            string str2 = "a,à,á,ả,ã,ạ,â,ầ,ấ,ẩ,ẫ,ậ,ă,ằ,ắ,ẳ,ẵ,ặ,e,è,é,ẻ,ẽ,ẹ,ê,ề,ế,ể,ễ,ệ,i,ì,í,ỉ,ĩ,ị,o,ò,ó,ỏ,õ,ọ,ơ,ờ,ớ,ở,ỡ,ợ,ô,ồ,ố,ổ,ỗ,ộ,u,ù,ú,ủ,ũ,ụ,ư,ừ,ứ,ử,ữ,ự,y,ỳ,ý,ỷ,ỹ,ỵ,đ";
            for (int index1 = 0; index1 < strUnicode.Length; ++index1)
            {
                string str3 = str2;
                char ch = strUnicode[index1];
                string lower = ch.ToString().ToLower();
                if (str3.Contains(lower))
                {
                    ch = strUnicode[index1];
                    StringBuilder stringBuilder1 = new StringBuilder(ch.ToString().ToLower());
                    ch = strUnicode[index1];
                    StringBuilder stringBuilder2 = new StringBuilder(ch.ToString().ToLower());
                    char[] chArray1 = new char[68]
                    {
            'ỳ',
            'ý',
            'ỷ',
            'ỹ',
            'ỵ',
            'ư',
            'ừ',
            'ứ',
            'ử',
            'ữ',
            'ự',
            'ò',
            'ó',
            'ỏ',
            'õ',
            'ọ',
            'ơ',
            'ờ',
            'ớ',
            'ở',
            'ỡ',
            'ợ',
            'ô',
            'ồ',
            'ố',
            'ổ',
            'ỗ',
            'ộ',
            'ì',
            'í',
            'ỉ',
            'ĩ',
            'ị',
            'ê',
            'ề',
            'ế',
            'ể',
            'ễ',
            'ệ',
            'è',
            'é',
            'ẻ',
            'ẽ',
            'ẹ',
            'ă',
            'ằ',
            'ắ',
            'ẳ',
            'ẵ',
            'ặ',
            'à',
            'á',
            'ả',
            'ã',
            'ạ',
            'â',
            'ầ',
            'ấ',
            'ẩ',
            'ẫ',
            'ậ',
            'ù',
            'ú',
            'ủ',
            'ũ',
            'ụ',
            'Ð',
            'đ'
                    };
                    char[] chArray2 = new char[68]
                    {
            'ú',
            'ý',
            'û',
            'ü',
            'þ',
            '\x00AD',
            'õ',
            'ø',
            'ö',
            '÷',
            'ù',
            'ß',
            'ã',
            'á',
            'â',
            'ä',
            '¬',
            'ê',
            'í',
            'ë',
            'ì',
            'î',
            '«',
            'å',
            'è',
            'æ',
            'ç',
            'é',
            '×',
            'Ý',
            'Ø',
            'Ü',
            'Þ',
            'ª',
            'Ò',
            'Õ',
            'Ó',
            'Ô',
            'Ö',
            'Ì',
            'Ð',
            'Î',
            'Ï',
            'Ñ',
            '¨',
            '»',
            '\x00BE',
            '\x00BC',
            '\x00BD',
            'Æ',
            'µ',
            '¸',
            '¶',
            '·',
            '\x00B9',
            '©',
            'Ç',
            'Ê',
            'È',
            'É',
            'Ë',
            'ï',
            'ó',
            'ñ',
            'ò',
            'ô',
            '§',
            '®'
                    };
                    for (int index2 = 0; index2 < chArray1.Length; ++index2)
                    {
                        char oldChar = chArray1[index2];
                        char newChar = chArray2[index2];
                        stringBuilder1.Replace(oldChar, newChar);
                        if (stringBuilder2.ToString() != stringBuilder1.ToString())
                            break;
                    }
                    str1 += stringBuilder1.ToString();
                }
                else
                {
                    string str4 = str1;
                    ch = strUnicode[index1];
                    string str5 = ch.ToString();
                    str1 = str4 + str5;
                }
            }
            return str1;
        }


        public static int toInt(object o)
        {
            return int.Parse(o.ToString());
        }

        public static string UnicodetoTCVN(string strUnicode)
        {
            int num;
            StringBuilder builder = new StringBuilder(strUnicode);
            char[] chArray = new char[] {
            '\x00fc', '\x00fb', '\x00fe', '\x00fa', '\x00f9', '\x00f7', '\x00f6', '\x00f5', '\x00f8', '\x00f1', '\x00f4', '\x00ee', '\x00ec', '\x00eb', '\x00ea', '\x00ed',
            '\x00e9', '\x00e7', '\x00e6', '\x00e5', '\x00e8', '\x00e1', '\x00e4', '\x00de', '\x00d8', '\x00d6', '\x00d4', '\x00d3', '\x00d2', '\x00d5', '\x00cf', '\x00ce',
            '\x00d1', '\x00c6', '\x00bd', '\x00bc', '\x00ab', '\x00be', '\x00cb', '\x00c9', '\x00c8', '\x00c7', '\x00ca', '\x00b6', '\x00b9', '\x00ad', '\x00a6', '\x00ac',
            '\x00a5', '\x00f2', '\x00dc', '\x00ae', '\x00a8', '\x00a1', '\x00f3', '\x00ef', '\x00e2', '\x00bb', '\x00e3', '\x00df', '\x00dd', '\x00d7', '\x00aa', '\x00d0',
            '\x00cc', '\x00b7', '\x00a9', '\x00b8', '\x00b5', '\x00a4', '\x00a7', '\x00a3', '\x00a2'
         };
            char[] chArray2 = new char[] {
            'ỹ', 'ỷ', 'ỵ', 'ỳ', 'ự', 'ữ', 'ử', 'ừ', 'ứ', 'ủ', 'ụ', 'ợ', 'ỡ', 'ở', 'ờ', 'ớ',
            'ộ', 'ỗ', 'ổ', 'ồ', 'ố', 'ỏ', 'ọ', 'ị', 'ỉ', 'ệ', 'ễ', 'ể', 'ề', 'ế', 'ẽ', 'ẻ',
            'ẹ', 'ặ', 'ẵ', 'ẳ', '\x00f4', 'ắ', 'ậ', 'ẫ', 'ẩ', 'ầ', 'ấ', 'ả', 'ạ', 'ư', 'Ư', 'ơ',
            'Ơ', 'ũ', 'ĩ', 'đ', 'ă', 'Ă', '\x00fa', '\x00f9', '\x00f5', 'ằ', '\x00f3', '\x00f2', '\x00ed', '\x00ec', '\x00ea', '\x00e9',
            '\x00e8', '\x00e3', '\x00e2', '\x00e1', '\x00e0', '\x00d4', 'Đ', '\x00ca', '\x00c2'
         };
            for (num = 0; num < chArray.Length; num++)
            {
                builder.Replace(chArray2[num], chArray[num]);
            }
            string[] strArray = new string[] {
            "A\x00e0", "Aả", "A\x00e3", "A\x00e1", "Aạ", "E\x00e8", "Eẻ", "Eẽ", "E\x00e9", "Eẹ", "I\x00ec", "Iỉ", "Iĩ", "I\x00ed", "Iị", "O\x00f2",
            "Oỏ", "O\x00f5", "O\x00f3", "Oọ", "U\x00f9", "Uủ", "Uũ", "U\x00fa", "Uụ", "Yỳ", "Yỷ", "Yỹ", "Y\x00fd", "Yỵ", "Ăằ", "Ăẳ",
            "Ăẵ", "Ăắ", "Ăặ", "\x00c2ầ", "\x00c2ẩ", "\x00c2ẫ", "\x00c2ấ", "\x00c2ậ", "\x00caề", "\x00caể", "\x00caễ", "\x00caế", "\x00caệ", "\x00d4ồ", "\x00d4ổ", "\x00d4ỗ",
            "\x00d4ố", "\x00d4ộ", "Ơờ", "Ơở", "Ơỡ", "Ơớ", "Ơợ", "Ưừ", "Ưử", "Ưữ", "Ưứ", "Ưự"
         };
            string[] strArray2 = new string[] {
            "\x00c0", "Ả", "\x00c3", "\x00c1", "Ạ", "\x00c8", "Ẻ", "Ẽ", "\x00c9", "Ẹ", "\x00cc", "Ỉ", "Ĩ", "\x00cd", "Ị", "\x00d2",
            "Ỏ", "\x00d5", "\x00d3", "Ọ", "\x00d9", "Ủ", "Ũ", "\x00da", "Ụ", "Ỳ", "Ỷ", "Ỹ", "\x00dd", "Ỵ", "Ằ", "Ẳ",
            "Ẵ", "Ắ", "Ặ", "Ầ", "Ẩ", "Ẫ", "Ấ", "Ậ", "Ề", "Ể", "Ễ", "Ế", "Ệ", "Ồ", "Ổ", "Ỗ",
            "Ố", "Ộ", "Ờ", "Ở", "Ỡ", "Ớ", "Ợ", "Ừ", "Ử", "Ữ", "Ứ", "Ự"
         };
            for (num = 0; num < strArray.Length; num++)
            {
                builder.Replace(strArray2[num], strArray[num]);
            }
            return builder.ToString();
        }
    }
}
