﻿namespace ExcelAddIn1.MyClass
{
    public class AddinConfig
    {
        private static string default_config_json_path => System.IO.Path.Combine(PublicVar.ExcelVSTOAddinFolder() + @"\config.json");

        private static string _saveLocationImportPurchaseInvoiceVIETSEA = "SaveLocationImportPurchaseInvoiceVIETSEA";
        private static string _saveLocationSaleInvoiceImportFileVIETSEA = "SaveLocationSaleInvoiceImportFileVIETSEA";
        private static string _saveLocationImport_iHoadon = "SaveLocation_Import_iHoadon";
        
        private static string _transferFeeSameBankSystem = "TransferFeeSameBankSystem";
        private static string _transferFeeDifferentBankSystem = "TransferFeeDifferentBankSystem";
        
        private static string _startDateSaleReturn = "StartDateSaleReturn";
        
        private static string _targetVIETSEA = "TargetVIETSEA";
        private static string _oracleIP = "OracleIP";
        private static string _oraclePort = "OraclePort";
        private static string _usernameVIETSEA = "usernameVIETSEA";
        private static string _passwordVIETSEA = "passwordVIETSEA";

        private static string _targetTHANHCONG = "TargetTHANHCONG";
        
        private static string _EFYmst = "EFYmst";
        private static string _EFYusername = "EFYusername";
        private static string _EFYpassword = "EFYpassword";



        private static System.Collections.Generic.Dictionary<string, string> default_keyValuePairs()
        {
            System.Collections.Generic.Dictionary<string, string> dic = new System.Collections.Generic.Dictionary<string, string>();
            dic.Add(_saveLocationImportPurchaseInvoiceVIETSEA, PublicVar.ExcelVSTOAddinFolder());
            dic.Add(_saveLocationSaleInvoiceImportFileVIETSEA, PublicVar.ExcelVSTOAddinFolder());
            dic.Add(_saveLocationImport_iHoadon, PublicVar.ExcelVSTOAddinFolder());
            dic.Add(_transferFeeSameBankSystem, "7000");
            dic.Add(_transferFeeDifferentBankSystem, "19000");
            dic.Add(_startDateSaleReturn, new System.DateTime(System.DateTime.Now.Year, 1, 1).ToString());
            dic.Add(_oracleIP, "192.168.0.50");
            dic.Add(_oraclePort, "1522");
            dic.Add(_targetVIETSEA, "");
            dic.Add(_targetTHANHCONG, "");
            dic.Add(_usernameVIETSEA, "nguyenminhduc");
            dic.Add(_passwordVIETSEA, "123456");
            dic.Add(_EFYmst, "0106188175");
            dic.Add(_EFYusername, "0106188175");
            dic.Add(_EFYpassword, "thanhcong86");


            //.....
            //add more here

            return dic;
        }



        private static string configJsonFullname()
        {
            if (System.IO.File.Exists(default_config_json_path))
            {
                return default_config_json_path;
            }
            else
            {
                Newtonsoft.Json.Linq.JObject config = new Newtonsoft.Json.Linq.JObject();

                System.Collections.Generic.Dictionary<string, string> dic = default_keyValuePairs();

                //add default value
                foreach (var item in dic)
                {
                    config.Add(item.Key, item.Value);
                }

                System.IO.File.WriteAllText(default_config_json_path, config.ToString());
                return default_config_json_path;
            }
        }

        public static string SaveLocationImportPurchaseInvoiceVIETSEA
        {
            get
            {
                return GetValue(_saveLocationImportPurchaseInvoiceVIETSEA);
            }
            set
            {
                SetValue(_saveLocationImportPurchaseInvoiceVIETSEA, value);
            }
        }

        public static string SaveLocationSaleInvoiceImportFileVIETSEA
        {
            get
            {
                return GetValue(_saveLocationSaleInvoiceImportFileVIETSEA);
            }
            set
            {
                SetValue(_saveLocationSaleInvoiceImportFileVIETSEA, value);
            }
        }

        public static string TransferFeeSameBankSystem
        {
            get
            {
                return GetValue(_transferFeeSameBankSystem);
            }
            set
            {
                SetValue(_transferFeeSameBankSystem, value);
            }
        }

        public static string TransferFeeDifferentBankSystem
        {
            get
            {
                return GetValue(_transferFeeDifferentBankSystem);
            }
            set
            {
                SetValue(_transferFeeDifferentBankSystem, value);
            }
        }

        public static string SaveLocation_Import_iHoadon
        {
            get
            {
                return GetValue(_saveLocationImport_iHoadon);
            }
            set
            {
                SetValue(_saveLocationImport_iHoadon, value);
            }
        }

        public static string StartDateSaleReturn
        {
            get
            {
                return GetValue(_startDateSaleReturn);
            }
            set
            {
                SetValue(_startDateSaleReturn, value);
            }
        }

        public static string Oralce_IP
        {
            get { return GetValue(_oracleIP); }
            set { SetValue(_oracleIP, value); }
        }

        public static string Oralce_Port
        {
            get { return GetValue(_oraclePort); }
            set { SetValue(_oraclePort, value); }
        

        } 
        public static string TargetVIETSEA
        {
            get { return GetValue(_targetVIETSEA); }
            set { SetValue(_targetVIETSEA, value); }
        }
        
        public static string TargetTHANHCONG
        {
            get { return GetValue(_targetTHANHCONG); }
            set { SetValue(_targetTHANHCONG, value); }
        }
        
        public static string UsernameVIETSEA
        {
            get { return GetValue(_usernameVIETSEA); }
            set { SetValue(_usernameVIETSEA, value); }
        }
        
        public static string PasswordVIETSEA
        {
            get { return GetValue(_passwordVIETSEA); }
            set { SetValue(_passwordVIETSEA, value); }
        }

        public static string EFYmst
        {
            get { return GetValue(_EFYmst); }
            set { SetValue(_EFYmst, value); }
        }
        public static string EFYusername
        {
            get { return GetValue(_EFYusername); }
            set { SetValue(_EFYusername, value); }
        }
        
        public static string EFYpassword
        {
            get { return GetValue(_EFYpassword); }
            set { SetValue(_EFYpassword, value); }
        }





        #region Methods

        private static string GetValue(string key)
        {

            Newtonsoft.Json.Linq.JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(System.IO.File.ReadAllText(configJsonFullname()));
            
            if(jObject[key] == null)
            {
                jObject.Add(key, default_keyValuePairs()[key]);
                string output = Newtonsoft.Json.JsonConvert.SerializeObject(jObject, Newtonsoft.Json.Formatting.Indented);
                System.IO.File.WriteAllText(configJsonFullname(), output);
            }
            
            return jObject[key].ToString();
        }

        private static void SetValue(string key, string theValueOfKey)
        {
            string jsonText = System.IO.File.ReadAllText(configJsonFullname());
            Newtonsoft.Json.Linq.JObject jObject = Newtonsoft.Json.JsonConvert.DeserializeObject(jsonText) as Newtonsoft.Json.Linq.JObject;
            jObject[key] = theValueOfKey;
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jObject, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText(configJsonFullname(), output);
        }
        #endregion





    }
}
