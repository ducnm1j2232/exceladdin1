﻿using ExcelAddIn1.MyClass;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataTable = System.Data.DataTable;
using VSTOLib.Extensions;

namespace ExcelAddIn1
{

    public static class Module1
    {
        //public variables
        public static string title = "Nguyễn Minh Đức";
        public static string errorTitle = "Lỗi!";


        public static void SpeedUpCode(bool mode)
        {
            try
            {
                if (mode == true) Globals.ThisAddIn.Application.Calculation = XlCalculation.xlCalculationManual;
                else Globals.ThisAddIn.Application.Calculation = XlCalculation.xlCalculationAutomatic;
                Globals.ThisAddIn.Application.ScreenUpdating = !mode;
                Globals.ThisAddIn.Application.EnableEvents = !mode;
                Globals.ThisAddIn.Application.DisplayAlerts = !mode;
            }
            catch { }
        }

        public static void EndVoid()
        {
            SpeedUpCode(false);
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;
            Range range = ws.Range["a1"].Find(What: "", LookIn: XlFindLookIn.xlValues, LookAt: XlLookAt.xlPart, SearchOrder: XlSearchOrder.xlByRows, SearchDirection: XlSearchDirection.xlNext, MatchCase: false);
            Globals.ThisAddIn.Application.CutCopyMode = 0;
            Notification.Wait.Close();
        }


        public static double TachPhiVCB(string descriptionText)
        {
            double result = 0;
            try
            {
                int startIndex;
                int endIndex;
                if (descriptionText.ToUpper().Contains("MERCHNO") == true) //chứa MerchNo
                {
                    startIndex = descriptionText.IndexOf("VAT Amt:", 0, comparisonType: StringComparison.CurrentCultureIgnoreCase) + 8;
                    endIndex = descriptionText.IndexOf("*", 0, comparisonType: StringComparison.CurrentCultureIgnoreCase) - 1;
                }
                else //không chứa MerchNo
                {
                    startIndex = descriptionText.IndexOf("*", 0, comparisonType: StringComparison.CurrentCultureIgnoreCase) + 1;
                    endIndex = descriptionText.IndexOf(".vat:", 0, comparisonType: StringComparison.CurrentCultureIgnoreCase);
                }

                int lengthIndex = endIndex - startIndex;
                string amount = descriptionText.Substring(startIndex: startIndex, length: lengthIndex);
                double.TryParse(amount, out result);
            }
            catch (Exception ex) { MessageBox.Show(text: ex.Message); }
            return result;
        }

        public static string ConvertToValidNameForWorksheet(object stringToConvert)
        {
            string resultString;
            if (stringToConvert == null)
            {
                resultString = "(null)";
            }
            else if ((string)stringToConvert == string.Empty)
            {
                resultString = "(blank)";
            }
            else
            {
                resultString = stringToConvert.ToString();
            }

            if (resultString.Length >= 25)
            {
                resultString = resultString.Substring(0, 25);
            }
            char[] invalidChar = new char[] { '/', '\\', '[', ']', '*', ':', '?' };

            foreach (char ch in invalidChar)
            {
                resultString = resultString.Replace(oldChar: ch, newChar: '-');
            }
            return resultString;
        }

        public static DataTable GetDataTableFromExcelFile(string excel_file_fullname, string SQL_statement)
        {
            using (OleDbConnection connection = new OleDbConnection())
            {
                DataTable dt = new DataTable();
                string excelFileName = excel_file_fullname;
                connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                                            + excelFileName
                                            + @";Extended Properties=""Excel 12.0;HDR=YES;IMEX=1"";";
                using (OleDbCommand command = new OleDbCommand())
                {
                    command.CommandText = SQL_statement;
                    command.Connection = connection;
                    using (OleDbDataAdapter da = new OleDbDataAdapter())
                    {
                        da.SelectCommand = command;
                        da.Fill(dt);
                        return dt;
                    }
                }
            }
        }

        public static string ConvertDateToTransDate(DateTime dateTime)
        {
            string day = dateTime.Day.ToString("00");
            string month = dateTime.Month.ToString("00");
            string year = dateTime.Year.ToString("0000");
            return year + month + day;
        }

        public static DataTable GetDataTableFromSQLServer(string IPserver, string database, string username, string password, string SQLquery)
        {
            string connectionString = $"Data Source = {IPserver};" +
                                    $"Initial Catalog = {database};" +
                                    $"User ID = {username};" +
                                    $"Password = {password}";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Module1.errorTitle);
                    return null;
                }
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    DataTable dt = new DataTable();
                    da.SelectCommand = new SqlCommand(SQLquery, connection);
                    da.Fill(dt);
                    return dt;
                }
            }

        }

        public static DataTable GetDataTableFromSQLServer(string connectionString, string SQLquery)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        DataTable dt = new DataTable();
                        da.SelectCommand = new SqlCommand(SQLquery, connection);
                        da.Fill(dt);
                        return dt;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Module1.errorTitle);
                    return null;
                }
            }
        }

        public static System.Data.DataSet GetDataSetFromSQLServer(string connectionString, string SQLquery)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        System.Data.DataSet ds = new System.Data.DataSet();
                        da.SelectCommand = new SqlCommand(SQLquery, connection);
                        da.Fill(ds);
                        return ds;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Module1.errorTitle);
                    return null;
                }
            }
        }

        public static void VlookupMAVIETSEA(Range lookupValueRange, Range destinationFirstCell, Worksheet sheetBangMa, bool pasteValue = true)
        {
            if (sheetBangMa == null)
            {
                Notification.Toast.Show("Đường dẫn File Bảng Mã chưa được thiết lập!\r\nViệc Vlookup sẽ không được thực thi");
                return;
            }

            //dùng địa chỉ tuyệt đối đến tận workbook, vì có khả năng phải vlookup giữa các workbook khác nhau
            //cấu trúc:
            // '[Workbook_name]Sheet_name'!Cell_address

            //param 1: Lookup Value
            string lookupValueWorkbookName = ((Workbook)lookupValueRange.Worksheet.Parent).Name;
            string lookupValueWorksheetName = lookupValueRange.Worksheet.Name;
            string lookupValueCellAddress = ((Range)lookupValueRange.Cells[1, 1]).Address[false, false];// Cell{1,1] là chỉ lấy ô đầu tiên, sau đó FillDown xuống
            string lookupValue = $"'[{ lookupValueWorkbookName }]{lookupValueWorksheetName}'!{lookupValueCellAddress}";

            //param 2: Table array
            string bangMaWorbookName = ((Workbook)sheetBangMa.Parent).Name;
            string bangMaWorsheetName = sheetBangMa.Name;
            string columnsInTableArray = "A:C";
            string tableArray = $"'[{bangMaWorbookName}]{bangMaWorsheetName}'!{columnsInTableArray}";

            destinationFirstCell.Resize[lookupValueRange.Rows.Count, 1].NumberFormat = "General";
            destinationFirstCell.Formula = $"=VLOOKUP({lookupValue},{tableArray},3,0)";
            destinationFirstCell.Resize[lookupValueRange.Rows.Count, 1].FillDownFormula(pasteValue);
        }

        public static Workbook ImportWorkbookTemplate()
        {
            Workbook importWB = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet importWS = importWB.Worksheets[1];
            importWS.Name = "4";
            importWS.Range["a1"].SetHeaderText(new string[] { "So_ct", "Ma_kh", "Ma_vt", "So_luong", "Gia2", "Tien2", "Thue" });
            //ExcelVoid.Write1DArrayToRange(header, importWS.Range["a1"]);
            importWS.Range["f2", "f5000"].FormulaR1C1 = "=RC[-2]*RC[-1]";
            importWS.Range["h1"].Formula = "=SUM(F2:F5000)";
            return importWB;
        }

        public static Workbook ImportWorkbookTemplate(string So_ct = null, string Ma_kh = null, Range Ma_vt = null, Range So_luong = null, Range Gia2 = null, Range Tien2 = null, int Thue = 10)
        {
            Workbook importWB = ImportWorkbookTemplate();
            Worksheet importWS = importWB.Worksheets[1];
            //importWS.Name = "4";
            //importWS.Range["a1"].SetHeaderText(new string[] { "So_ct", "Ma_kh", "Ma_vt", "So_luong", "Gia2", "Tien2", "Thue" });

            int rowsCount = Ma_vt.Rows.Count;
            if (So_ct != null) importWS.Range["a2"].Resize[rowsCount].Value2 = So_ct;
            if (Ma_kh != null) importWS.Range["b2"].Resize[rowsCount].Value2 = Ma_kh;
            if (Ma_vt != null) Ma_vt.CopyValueTo(importWS.Range["c2"]);
            if (So_luong != null) So_luong.CopyValueTo(importWS.Range["d2"]);
            if (Gia2 != null) Gia2.CopyValueTo(importWS.Range["e2"]);
            if (Tien2 != null) Tien2.CopyValueTo(importWS.Range["f2"]);
            importWS.Range["g2"].Resize[rowsCount].Value2 = Thue;

            return importWB;
        }

        public static Workbook ImportWorkbookTemplate(string So_ct = null, string Ma_kh = null, Range Ma_vt = null, int So_luong = 1, Range Gia2 = null, Range Tien2 = null, int Thue = 10)
        {
            Workbook importWB = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet importWS = importWB.Worksheets[1];
            importWS.Name = "4";
            string[] header = new string[] { "So_ct", "Ma_kh", "Ma_vt", "So_luong", "Gia2", "Tien2", "Thue" };
            ExcelVoid.Write1DArrayToRange(header, importWS.Range["a1"]);

            int rowsCount = Ma_vt.Rows.Count;
            if (So_ct != null) importWS.Range["a2"].Resize[rowsCount].Value2 = So_ct;
            if (Ma_kh != null) importWS.Range["b2"].Resize[rowsCount].Value2 = Ma_kh;
            if (Ma_vt != null) Ma_vt.Copy(importWS.Range["c2"]);
            if (So_luong != 0) importWS.Range["d2"].Resize[rowsCount].Value2 = So_luong;
            if (Gia2 != null) Gia2.Copy(importWS.Range["e2"]);
            if (Tien2 != null) Tien2.Copy(importWS.Range["f2"]);
            importWS.Range["g2"].Resize[rowsCount].Value2 = Thue;

            return importWB;
        }

        public static Workbook ImportWorkbookTemplateRandom(string So_ct = null, string Ma_kh = null, Range Ma_vt = null, Range So_luong_random = null, int Thue = 10)
        {
            Workbook importWB = ImportWorkbookTemplate();
            Worksheet importWS = importWB.Worksheets[1];
            //importWS.Name = "4";
            //string[] header = new string[] { "So_ct", "Ma_kh", "Ma_vt", "So_luong", "Gia2", "Tien2", "Thue" };
            //ExcelVoid.Write1DArrayToRange(header, importWS.Range["a1"]);

            int rowsCount = Ma_vt.Rows.Count;
            if (So_ct != null) importWS.Range["a2"].Resize[rowsCount].Value2 = So_ct;
            if (Ma_kh != null) importWS.Range["b2"].Resize[rowsCount].Value2 = Ma_kh;
            if (Ma_vt != null) Ma_vt.CopyValueTo(importWS.Range["c2"]);
            if (So_luong_random != null)
            {
                So_luong_random.CopyValueTo(importWS.Range["d2"]);
                //object[,] soluong = So_luong_random.Value;
                //importWS.Range["d2"].Resize[rowsCount, 1].Value = soluong;
            }

            //ExcelVoid.PasteValue(importWS.Range["d2"].Resize[rowsCount, 1]);
            importWS.Range["g2"].Resize[rowsCount].Value2 = Thue;

            return importWB;
        }

        //public static string ImportFilesSaveLocation()
        //{
        //    string saveFolder;
        //    while (true)
        //    {
        //        saveFolder = Properties.Settings.Default.ImportInvoiceSaveLocation;
        //        if (saveFolder == string.Empty)
        //        {
        //            using (fmSettings fm = new fmSettings())
        //            {
        //                fm.ShowDialog();
        //            }
        //        }
        //        else break;
        //    }
        //    return saveFolder;
        //}


        public static string ConvertToValidFileName(string s)
        {
            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,";
            foreach (char c in specialChar)
            {
                s = s.Replace(c, '_');
            }
            return s;
        }

        /// <summary>
        /// check null
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static string FolderBrowser(string description = "")
        {
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
            {
                folderBrowserDialog.Description = description;
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    return folderBrowserDialog.SelectedPath;
                }
            }
            return null;
        }


        public static string ConvertTransdateToDateString(string transdate)
        {
            string day = transdate.Substring(6, 2);
            string month = transdate.Substring(4, 2);
            string year = transdate.Substring(0, 4);
            return day + "/" + month + "/" + year;
        }

        /// <summary>
        /// Remember to check null
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="defaultCurrentRegion"></param>
        /// <returns></returns>
        public static Range InputBoxRange(string prompt, bool defaultCurrentRegion = false)
        {
            Range selection = Globals.ThisAddIn.Application.Selection;
            if (defaultCurrentRegion == true)
            {
                if (selection.IsSingleCell())
                {
                    selection = selection.CurrentRegion;
                }
            }

            try
            {
                selection = Globals.ThisAddIn.Application.InputBox(prompt, PublicVar.title, Default: selection.Address, Type: 8);
            }
            catch
            {
                return null;
            }

            return selection;

        }


        public static dynamic InputBox(string prompt, bool defaultCurrentRegion = false)
        {
            dynamic result = null;
            try
            {
                result = Globals.ThisAddIn.Application.InputBox(Prompt: prompt, Type: 1 + 2 + 4 + 8 + 16 + 64);
                if (result == string.Empty|| result == false) return null;
            }
            catch
            {
                return null;
            }

            return result;

        }

        public static bool IsAnyControlEmpty(List<Control> listControl)
        {
            foreach (Control control in listControl)
            {
                if (string.IsNullOrEmpty(control.Text))
                {
                    control.Focus();
                    MessageBox.Show($"{control.Name} chưa có dữ liệu");
                    return true;
                }
            }
            return false;
        }

        public static void WriteDataGridViewToXML(DataGridView dataGridView, string xml)
        {
            dataGridView.Update();
            dataGridView.Refresh();

            DataTable dataTable = (DataTable)((BindingSource)dataGridView.DataSource).DataSource;
            dataTable.WriteXml(xml, System.Data.XmlWriteMode.WriteSchema);

        }









    }
}

