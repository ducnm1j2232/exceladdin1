﻿using OpenQA.Selenium;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ExcelAddIn1.MyClass
{
    public static class Selenium
    {

        public static string GetAbsoluteXPath(this IWebElement element, OpenQA.Selenium.Chrome.ChromeDriver driver)
        {
            return (string)((IJavaScriptExecutor)driver).ExecuteScript(
                    "function absoluteXPath(element) {" +
                            "var comp, comps = [];" +
                            "var parent = null;" +
                            "var xpath = '';" +
                            "var getPos = function(element) {" +
                            "var position = 1, curNode;" +
                            "if (element.nodeType == Node.ATTRIBUTE_NODE) {" +
                            "return null;" +
                            "}" +
                            "for (curNode = element.previousSibling; curNode; curNode = curNode.previousSibling) {" +
                            "if (curNode.nodeName == element.nodeName) {" +
                            "++position;" +
                            "}" +
                            "}" +
                            "return position;" +
                            "};" +

                            "if (element instanceof Document) {" +
                            "return '/';" +
                            "}" +

                            "for (; element && !(element instanceof Document); element = element.nodeType == Node.ATTRIBUTE_NODE ? element.ownerElement : element.parentNode) {" +
                            "comp = comps[comps.length] = {};" +
                            "switch (element.nodeType) {" +
                            "case Node.TEXT_NODE:" +
                            "comp.name = 'text()';" +
                            "break;" +
                            "case Node.ATTRIBUTE_NODE:" +
                            "comp.name = '@' + element.nodeName;" +
                            "break;" +
                            "case Node.PROCESSING_INSTRUCTION_NODE:" +
                            "comp.name = 'processing-instruction()';" +
                            "break;" +
                            "case Node.COMMENT_NODE:" +
                            "comp.name = 'comment()';" +
                            "break;" +
                            "case Node.ELEMENT_NODE:" +
                            "comp.name = element.nodeName;" +
                            "break;" +
                            "}" +
                            "comp.position = getPos(element);" +
                            "}" +

                            "for (var i = comps.length - 1; i >= 0; i--) {" +
                            "comp = comps[i];" +
                            "xpath += '/' + comp.name.toLowerCase();" +
                            "if (comp.position !== null) {" +
                            "xpath += '[' + comp.position + ']';" +
                            "}" +
                            "}" +

                            "return xpath;" +

                            "} return absoluteXPath(arguments[0]);", element);
        }



        public static IWebElement GetWebElement(this OpenQA.Selenium.Support.UI.WebDriverWait wait, string xPath)
        {
            //OpenQA.Selenium.Support.UI.WebDriverWait wait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver , System.TimeSpan.FromMinutes(15));
            IWebElement result = null;
            try
            {
                for (int i = 0; i < 2; i++)
                {
                    try
                    {
                        result = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath(xPath)));
                        return result;
                    }
                    catch
                    {
                        result = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath(xPath)));
                        //return null;
                    }
                }
            }
            catch
            {
                return null;
                //throw new Exception("Cannot get webElement");
            }
            return result;
        }

        public static void ScrollAndClick(this IWebElement webElement, OpenQA.Selenium.Chrome.ChromeDriver driver, OpenQA.Selenium.Support.UI.WebDriverWait wait)
        {
            string xpath = webElement.GetAbsoluteXPath(driver);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView();", webElement);
            webElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath(xpath)));
            webElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(OpenQA.Selenium.By.XPath(xpath)));
            webElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(OpenQA.Selenium.By.XPath(xpath)));
            webElement.Click();

            //WaitDOMchange(driver);
        }
        public static void ScrollAndClick(this IWebElement webElement, SeleniumBrowser browser)
        {
            OpenQA.Selenium.Chrome.ChromeDriver driver = browser.ChromeDriverCore;
            OpenQA.Selenium.Support.UI.WebDriverWait wait = browser.WaitCore;

            string xpath = webElement.GetAbsoluteXPath(driver);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView();", webElement);
            webElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath(xpath)));
            webElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(OpenQA.Selenium.By.XPath(xpath)));
            webElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(OpenQA.Selenium.By.XPath(xpath)));
            webElement.Click();

            //WaitDOMchange(browerCore);
        }



        public static bool IsElementPresent(this SeleniumBrowser browerCore, string XPath)
        {
            try
            {
                //browerCore.WaitDOMchange().Wait();
                browerCore.ChromeDriverCore.FindElement(OpenQA.Selenium.By.XPath(XPath));
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }




        public static async Task SignIn_IHoaDon(SeleniumBrowser browser)
        {
            OpenQA.Selenium.Chrome.ChromeDriver driver = browser.ChromeDriverCore;
            OpenQA.Selenium.Support.UI.WebDriverWait wait = browser.WaitCore;

            driver.Url = "https://ihoadon.com.vn/login";
            driver.Navigate();

            wait.GetWebElement("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[1]/div/div/input").SendKeys("0106188175");//mst
            wait.GetWebElement("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[2]/div/div/input").SendKeys("0106188175");//tên đăng nhập
            wait.GetWebElement("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[3]/div/div/input").SendKeys("thanhcong86");//pass
            await browser.SmartClick("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[6]/div[1]/button/i");//button đăng nhập

            //await Task.Delay(1000);
            await browser.WaitDOMchange();


            //wait.GetWebElement("/html/body/app-root/app-home/div[1]/div[2]/div/div[1]/div[2]/div[2]/a/article");

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        public static System.Drawing.Bitmap GetElementScreenShot(this SeleniumBrowser browser, IWebElement element)
        {
            OpenQA.Selenium.Chrome.ChromeDriver driver = browser.ChromeDriverCore;
            var sc = ((ITakesScreenshot)driver).GetScreenshot();
            using (var img = System.Drawing.Image.FromStream(new MemoryStream(sc.AsByteArray)) as System.Drawing.Bitmap)
            {
                return img.Clone(new System.Drawing.Rectangle(element.Location, element.Size), img.PixelFormat);
            }
        }







    }








    /// <summary>
    /// Add chrome driver and wait into one class
    /// </summary>
    public class SeleniumBrowser
    {

        public SeleniumBrowser(OpenQA.Selenium.Chrome.ChromeDriver chromeDriver, OpenQA.Selenium.Support.UI.WebDriverWait wait)
        {

            ChromeDriverCore = chromeDriver;
            WaitCore = wait;
        }

        public SeleniumBrowser(TimeSpan timeSpanWait, OpenQA.Selenium.Chrome.ChromeOptions chromeOptions = null)
        {
            new WebDriverManager.DriverManager().SetUpDriver(new WebDriverManager.DriverConfigs.Impl.ChromeConfig(), new WebDriverManager.DriverConfigs.Impl.ChromeConfig().GetMatchingBrowserVersion());
            OpenQA.Selenium.Chrome.ChromeOptions chromeOptionsDefault = new OpenQA.Selenium.Chrome.ChromeOptions();
            if (chromeOptions != null)
            {
                chromeOptionsDefault = chromeOptions;
            }

            chromeOptionsDefault.AddArgument("--start-maximized");
            chromeOptionsDefault.AddArgument("disable-blink-features=AutomationControlled");//https://stackoverflow.com/questions/60649179/chromedriver-with-selenium-displays-a-blank-page


            OpenQA.Selenium.Chrome.ChromeDriver chromeDriver = new OpenQA.Selenium.Chrome.ChromeDriver(chromeOptionsDefault);
            chromeDriver.Manage().Timeouts().ImplicitWait = TimeSpan.Zero;
            OpenQA.Selenium.Support.UI.WebDriverWait wait = new OpenQA.Selenium.Support.UI.WebDriverWait(chromeDriver, timeSpanWait);
            ChromeDriverCore = chromeDriver;
            WaitCore = wait;
        }

        public OpenQA.Selenium.Chrome.ChromeDriver ChromeDriverCore
        {
            get;
            set;
        }

        public OpenQA.Selenium.Support.UI.WebDriverWait WaitCore
        {
            get;
            set;
        }


        public IWebElement GetWebElement(string xPath)
        {

            IWebElement result = null;
            try
            {
                result = WaitCore.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath(xPath)));
                return result;
            }
            //catch (OpenQA.Selenium.ElementNotInteractableException)
            //{
            //    result = WaitCore.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath(xPath)));
            //    result = WaitCore.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(OpenQA.Selenium.By.XPath(xPath)));
            //    return result;
            //}
            catch
            {
                return null;
                //throw new Exception(ex.Message + Environment.NewLine + "Cannot get webElement " + xPath);
            }
        }


        public async Task WaitDOMchange()
        {
            while (true)
            {
                await Task.Delay(500);
                if ((string)ChromeDriverCore.ExecuteScript("return document.readyState").ToString() == "complete"
                    && (bool)ChromeDriverCore.ExecuteScript("return jQuery.active == 0") == true)
                    break;
            }
            await Task.Delay(500);
        }

        public async Task SmartClick(string XPath)
        {
            OpenQA.Selenium.Chrome.ChromeDriver driver = ChromeDriverCore;
            OpenQA.Selenium.Support.UI.WebDriverWait wait = WaitCore;

            IWebElement webElement = GetWebElement(XPath);

            //((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView();", webElement);
            //webElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath(XPath)));
            //webElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(OpenQA.Selenium.By.XPath(XPath)));

            OpenQA.Selenium.Interactions.Actions actions = new OpenQA.Selenium.Interactions.Actions(ChromeDriverCore);
            actions.MoveToElement(webElement);

            try
            {
                webElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(OpenQA.Selenium.By.XPath(XPath)));
                webElement.Click();

            }
            catch (OpenQA.Selenium.ElementClickInterceptedException)
            {
                ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView();", webElement);
                webElement = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(OpenQA.Selenium.By.XPath(XPath)));
                webElement.Click();
            }

            await WaitDOMchange();
        }

        public async Task SendKeys(string xPath, string keys)
        {
            IWebElement webElement = GetWebElement(xPath);
            try
            {
                webElement.SendKeys(keys);
            }
            catch (OpenQA.Selenium.ElementNotInteractableException)
            {
                webElement = WaitCore.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath(xPath)));
                webElement = WaitCore.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(OpenQA.Selenium.By.XPath(xPath)));
                //goto ssend;
            }
            await WaitDOMchange();
        }

        public void CloseAndQuit()
        {
            ChromeDriverCore.Close();
            ChromeDriverCore.Quit();
        }












    }







    public class iHoaDon
    {
        public const string button_LapHoaDon = "/html/body/app-root/app-home/div[1]/div[2]/div/div[1]/div[2]/div[2]/a/article";

        public const string headerRow = "/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[1]";
        public const string rowView = "/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]";
        public const string dataGrid_header = "/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[1]";


    }

}
