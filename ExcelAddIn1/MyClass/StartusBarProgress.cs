﻿using System;

namespace ExcelAddIn1.MyClass
{
    public static class StartusBarProgress
    {
        static System.Threading.CancellationTokenSource tokenSource = new System.Threading.CancellationTokenSource();
        static System.Threading.CancellationToken token = tokenSource.Token;
        static System.Timers.Timer timer = new System.Timers.Timer();

        public static void ShowStatus()
        {
            timer.Interval = 1000;
            timer.Enabled = true;
            timer.Elapsed += Timer_Elapsed;
            startTime = DateTime.Now;
        }

        private static DateTime startTime;
        private static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string timePass = TimeSpan.FromSeconds((DateTime.Now - startTime).TotalSeconds).ToString("hh\\:mm\\:ss");
            Globals.ThisAddIn.Application.StatusBar = $"Running . . . {timePass}";
            //throw new NotImplementedException();
        }

        public static void TurnOffStatus()
        {
            timer.Enabled = false;
            Globals.ThisAddIn.Application.StatusBar = false;
        }
    }
}
