﻿using ExcelAddIn1.MyClass.Extensions;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace ExcelAddIn1
{
    class GoogleAPI
    {
        static string[] Scopes = { SheetsService.Scope.SpreadsheetsReadonly };
        static string applicationName = "VSTOaddin";
        static string spreadSheetID = "1o9N0xb1FdSWLQS_rlPcwp1bocYU4Z1R6p3-HyzNxjKw";
        //static string credentialsjson_path = PublicVar.ExcelVSTOAddinFolder() + @"\credentials.json";

        // dùng service account để tự động đăng nhập chẳng hạn
        // vì nếu dùng credential cũ thì người nào được share thì mới có thể access được file
        static string serviceAccount_CredentialsJson_path = PublicVar.ExcelVSTOAddinFolder() + @"\serviceAccount.json";

        // credential của drive, dùng để truy cập file trên google drive
        static string drive_credentialsjson_path = PublicVar.ExcelVSTOAddinFolder() + @"\driveCredentials.json";

        static string VSTOaddin_credentialsjson_path = PublicVar.ExcelVSTOAddinFolder() + @"\VSTOaddin_credentials.json";



        public static string ServiceAccount_Credentials_FullName()
        {
            if (!System.IO.File.Exists(serviceAccount_CredentialsJson_path))
            {
                //string s = @"{""installed"":{""client_id"":""264960917366-0prv058rurrb3g16r2k3qb1hk1jjmvp7.apps.googleusercontent.com"",""project_id"":""vstoaddin-1601210271267"",""auth_uri"":""https://accounts.google.com/o/oauth2/auth"",""token_uri"":""https://oauth2.googleapis.com/token"",""auth_provider_x509_cert_url"":""https://www.googleapis.com/oauth2/v1/certs"",""client_secret"":""7j60njVz9zgRyzMwa3PnsTJn"",""redirect_uris"":[""urn:ietf:wg:oauth:2.0:oob"",""http://localhost""]}}";

                //using service account
                string s = @"{""type"": ""service_account"",""project_id"": ""vstoaddin-1601210271267"",""private_key_id"": ""5d7f67312af0fb5738b3f85efd389b91bee94829"",""private_key"": ""-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDTabUeXtfUhu59\nGeTYMhtBjy8SDeZR0hMlj2kiwvuLSA80o/vgixTXnSRse+bpDT5Nzql1P+0bl/yF\nQzOAyhnu52mN1PtOFKszuNFe/FeN/PANDWv/zzunxFo05PSRE+q0Tp4+okKLEMRy\ntMm/4qV8om6Zzh5WmgqWR2BJEY5ylv1+pWKCfbUEpfwohtJ5OLlbvTB9X/XRC3ID\nITyNWNjZbmB+Fnr2A16C3T/NMdXpwVuM4J4wXzIbVwuoti9ShGTWb+N+J9sftvpC\nbbDPyA4lXqRq3E5hsj7+1De8IfIPZ8soIWLqZ+pcbImpgYKOKGGlNh1eU7XdGxHq\n3QdHdY/jAgMBAAECggEAIeh1w5XpVA2tjqRqjz+/c2JoZaLpFVJ2U7M5ZPk/qHU5\noHMfYO0DE29Wz4zdL5/k0EycjVEK0emB96BxpZnEqGFhYqg4WQD4/JC2CWARGEBA\nrYyWN9n1yCYMDykcY3B9rRxhm8t2lMtvAV7RxP8Fv9m67NxMWA4uMzA7gedY9jPK\nqUlDbe9OaqGS6ROdi+e43hbSxlRVix9xJkmDAdKGm2LNe1fplRsXUJxd3LDPPgRN\nZ+bZbFtps5BBmOCRsPPww2uRm7g3qcFv/vPeNssAe7Ll+JQ29wF41KAVwz7svhxe\nT7oK6Sk0bPq4XnVd96djxDw4rNuOTwsIHstklaGJ4QKBgQD3+/2l2vukVn/IKAHt\nZIzneUtFoOAttecYnf/zRrQXBDNx+8eJ+LTWxeuDQR0cmSHt5Zb1pEd7Y260/+ts\nfd5crRSJG3ukxpjksZd7jXaAjZmtsligym7LBY2nPKB9c6NTLaiqdgZkLHU5HO0T\nfDIEjkRKR79sPn7Ro2TYGPUUewKBgQDaPxjjM/OdQcVtybVUR5Lxa5oMkpSAoVQT\n5LdUAhrNSCI3BHVNQKvtoQZ9zNDGqEtevlIuxunDYBHHMvGiLLEH5q6Sf+s1X+TO\n4QgnyG837bUseeLJOmFhemG8G/WkzjQJBCf2DxAPTLWKWsoA9lPNX68m27NFUY1R\nVOirVUFZuQKBgEwJgUHcvnIqb63wzn566VWEdJcZ+AMsFnsO308567hu+lNF724X\ny2NbdpZdXc7KCYRHdJ1HTP/Bgk2PdyFiCs+B5p0Pxz7EAleFOLySqLZMNMX0vIY4\n6/XmcwPHbDJrRbYYdvIllFoc46iqfOsPlsqc0MWx8DkBhKNFJYO5S1SHAoGAaFKm\n89wehDovRGaZ3dqEvBAiSAY9JCt5xDSdCXQkGaJ0cHYaGD5ksKc7MiEYXw2NUZGK\nOT/t9+1RMjYA2POh/9Pd+Lta1F2tQtLAkju2JjbjVopAxM26RN06ILAAwLGsn+6z\np2PV2mphkOwLCWC1+ELXt6xnSihMc+cxfMy4+0kCgYEAyisHcp1hJ3DFt55FnJE5\noUHhlNsRJbESrVSol28NB50HLo1Ps4YHdaXkmn+OSnowYZKGUHtQrIgvO6jtwvuq\nO1Km00Hf+vSQm5qM7Mfwy+V1+TBmhJzuswuirqKKlSyhmXP4XZiVr/++9iRvuao9\nXHa0r5SRWN2iFSI8rcDc5AU=\n-----END PRIVATE KEY-----\n"",""client_email"": ""serviceaccount@vstoaddin-1601210271267.iam.gserviceaccount.com"",""client_id"": ""100771244059551248345"",""auth_uri"": ""https://accounts.google.com/o/oauth2/auth"",""token_uri"": ""https://oauth2.googleapis.com/token"",""auth_provider_x509_cert_url"": ""https://www.googleapis.com/oauth2/v1/certs"",""client_x509_cert_url"": ""https://www.googleapis.com/robot/v1/metadata/x509/serviceaccount%40vstoaddin-1601210271267.iam.gserviceaccount.com""}";
                System.IO.File.WriteAllText(serviceAccount_CredentialsJson_path, s);
            }
            return serviceAccount_CredentialsJson_path;
        }

        public static string Drive_Credentials_Fullname()
        {
            if (!System.IO.File.Exists(drive_credentialsjson_path))
            {
                string s = @"{""installed"":{""client_id"":""93821380818 - o1r81al7vmkobo1qqpk9egkceqth1ckk.apps.googleusercontent.com"",""project_id"":""vstoaddin - 1602902870942"",""auth_uri"":""https://accounts.google.com/o/oauth2/auth"",""token_uri"":""https://oauth2.googleapis.com/token"",""auth_provider_x509_cert_url"":""https://www.googleapis.com/oauth2/v1/certs"",""client_secret"":""1r6L1G2g5xk6RO-xvnvW9Xvg"",""redirect_uris"":[""urn:ietf:wg:oauth:2.0:oob"",""http://localhost""]}}";
                System.IO.File.WriteAllText(drive_credentialsjson_path, s);
            }
            return drive_credentialsjson_path;
        }

        public static string VSTOaddin_Credentials_Fullname()
        {
            if (!System.IO.File.Exists(VSTOaddin_credentialsjson_path))
            {
                string s = @"{""installed"":{""client_id"":""264960917366 - 0prv058rurrb3g16r2k3qb1hk1jjmvp7.apps.googleusercontent.com"",""project_id"":""vstoaddin - 1601210271267"",""auth_uri"":""https://accounts.google.com/o/oauth2/auth"",""token_uri"":""https://oauth2.googleapis.com/token"",""auth_provider_x509_cert_url"":""https://www.googleapis.com/oauth2/v1/certs"",""client_secret"":""7j60njVz9zgRyzMwa3PnsTJn"",""redirect_uris"":[""urn:ietf:wg:oauth:2.0:oob"",""http://localhost""]}}";
                s = s.Replace(" ", "");
                System.IO.File.WriteAllText(VSTOaddin_credentialsjson_path, s);
            }
            return VSTOaddin_credentialsjson_path;
        }



        public static async Task<GoogleCredential> GetServiceAccountCredentialAsync()
        {
            return await GoogleCredential.FromFileAsync(ServiceAccount_Credentials_FullName(), CancellationToken.None).ConfigureAwait(false);
        }

        public static async Task<IList<IList<object>>> GetDataFromGoogleSheetAsync(string rangeAddress)
        {
            GoogleCredential credential = await GetServiceAccountCredentialAsync();
            if (credential.IsCreateScopedRequired)
            {
                credential = credential.CreateScoped(SheetsService.Scope.Spreadsheets);
            }

            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = applicationName
            });

            SpreadsheetsResource.ValuesResource.GetRequest request = service.Spreadsheets.Values.Get(spreadSheetID, rangeAddress);
            ValueRange response = await request.ExecuteAsync().ConfigureAwait(false);
            IList<IList<object>> values = response.Values;
            return values;
        }


        public static async Task<object[,]> Get2dArrayFromGoogleSheetAsync(string rangeAddress)
        {
            IList<IList<object>> values = await GetDataFromGoogleSheetAsync(rangeAddress).ConfigureAwait(false);
            return values.To2DArray();
        }

        public static async Task<DriveService> GetDriveServiceAsync()
        {
            string[] Scopes = { DriveService.Scope.DriveReadonly };
            UserCredential userCredential;
            using (var stream = new FileStream(VSTOaddin_Credentials_Fullname(), FileMode.Open, FileAccess.Read))
            {
                string credPath = PublicVar.ExcelVSTOAddinFolder();
                userCredential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.FromStream(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true));
            }

            DriveService driveService = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = userCredential,
                ApplicationName = applicationName
            });
            return driveService;
        }


        public static async Task<SheetsService> GetSheetsServiceAsync()
        {
            GoogleCredential googleCredential = await GetServiceAccountCredentialAsync().ConfigureAwait(false);
            if (googleCredential.IsCreateScopedRequired)
            {
                googleCredential = googleCredential.CreateScoped(SheetsService.Scope.Spreadsheets);
            }
            SheetsService sheetsService = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = googleCredential,
                ApplicationName = applicationName
            });

            return sheetsService;
        }



        public static async Task AppendValueAsync(string range, List<List<object>> _values)
        {
            SheetsService sheetsService = await GetSheetsServiceAsync();
            SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum valueInputOptionEnum = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum insertDataOptionEnum = SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;
            ValueRange requestBody = new ValueRange();
            requestBody.MajorDimension = "ROWS";
            var oblist = new List<object>() { "My Cell Text" };
            requestBody.Values = new List<IList<object>> { oblist };
            requestBody.Range = range;

            ValueRange valueRange = new ValueRange();
            

            SpreadsheetsResource.ValuesResource.AppendRequest request = sheetsService.Spreadsheets.Values.Append(requestBody, spreadSheetID, range);
            request.ValueInputOption = valueInputOptionEnum;
            request.InsertDataOption = insertDataOptionEnum;

            AppendValuesResponse response = await request.ExecuteAsync().ConfigureAwait(false);
            System.Windows.Forms.MessageBox.Show("append thành công");

        }








        public static async Task DownLoadFileFromGoogleDrive(string fileID, string folderPath)
        {
            DriveService driveService = await GetDriveServiceAsync();
            FilesResource.GetRequest request = driveService.Files.Get(fileID);
            var ggFile = request.Execute();
            await DownloadFolderFromGoogleDrive(driveService, ggFile, folderPath);

        }

        private static void SaveStream(MemoryStream stream, string FilePath)
        {
            using (FileStream file = new FileStream(FilePath, FileMode.Create, FileAccess.ReadWrite))
            {
                stream.WriteTo(file);
            }
        }


        public static async Task DownloadFolderFromGoogleDrive(DriveService driveService, global::Google.Apis.Drive.v3.Data.File googleFile, string path)
        {
            if (googleFile.MimeType != "application/vnd.google-apps.folder") // nếu không phải là folder, tải luôn
            {
                using (var stream = new MemoryStream())
                {
                    await driveService.Files.Get(googleFile.Id).DownloadAsync(stream).ConfigureAwait(false);
                    using (FileStream file = new FileStream(path + $"/{googleFile.Name}", FileMode.Create, FileAccess.Write))
                    {
                        stream.WriteTo(file);
                    }
                }
            }
            else // nếu là folder, tạo folder mới và tải file chứa trong đó
            {
                string NewPath = path + @"/" + googleFile.Name;

                Directory.CreateDirectory(NewPath);
                var SubFolderItems = RessInFolder(driveService, googleFile.Id);

                foreach (var Item in SubFolderItems)
                {
                    // đệ quy
                    await DownloadFolderFromGoogleDrive(driveService, Item, NewPath);
                }
            }
        }

        public static List<global::Google.Apis.Drive.v3.Data.File> RessInFolder(DriveService service, string folderId)
        {
            List<global::Google.Apis.Drive.v3.Data.File> filesList = new List<global::Google.Apis.Drive.v3.Data.File>();
            var listRequest = service.Files.List();
            listRequest.Q = $"'{folderId}' in parents";

            var files = listRequest.Execute().Files;
            foreach (global::Google.Apis.Drive.v3.Data.File file in files)
            {
                filesList.Add(service.Files.Get(file.Id).Execute());
            }

            return filesList;
        }


        public static DriveService GetDriveService(string serviceAccountEmail, string keyFilePath)
        {
            if (!System.IO.File.Exists(keyFilePath))
            {
                Notification.Toast.Show("Key File does not exist!");
                return null;
            }

            string[] scopes = new string[] { DriveService.Scope.Drive };
            var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
            try
            {
                ServiceAccountCredential credential = new ServiceAccountCredential(
                    new ServiceAccountCredential.Initializer(serviceAccountEmail)
                    {
                        Scopes = scopes
                    }.FromCertificate(certificate));
                DriveService driveService = new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = applicationName
                });
                return driveService;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return null;
            }






        }


        internal static class ChromeLauncher
        {
            private const string ChromeAppKey = @"\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe";

            private static string ChromeAppFileName
            {
                get
                {
                    return (string)(Registry.GetValue("HKEY_LOCAL_MACHINE" + ChromeAppKey, "", null) ??
                                        Registry.GetValue("HKEY_CURRENT_USER" + ChromeAppKey, "", null));
                }
            }

            public static void OpenLink(string url)
            {
                string chromeAppFileName = ChromeAppFileName;
                if (string.IsNullOrEmpty(chromeAppFileName))
                {
                    throw new Exception("Could not find chrome.exe!");
                }
                Process.Start(chromeAppFileName, url);
            }
        }



    }
}
