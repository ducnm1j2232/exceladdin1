﻿using ExcelAddIn1.MyClass;
using Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using VSTOLib.Extensions;

namespace ExcelAddIn1
{

    class ExcelVoid
    {

        public static void Write1DArrayToRange(object[] array, Range range)
        {
            for (int i = 0; i < array.Length; i++)
            {
                range.Offset[0, i].Value = array[i].ToString();
            }
        }


        public static void DeleteBlankRows(Worksheet ws, string columnName)
        {
            int lastRow = ws.LastRow( columnName);

            Module1.SpeedUpCode(true);
            for (int i = lastRow; i > 0; i--)
            {
                if (string.IsNullOrWhiteSpace(ws.Range[columnName + i].Text))
                {
                    ws.Range[columnName + i].EntireRow.Delete();
                }
            }
            Module1.SpeedUpCode(false);
        }


        public static void InsertBlankRowsAtValueChange(Worksheet worksheet, string columnName, int numberOfRowsToAdd = 1)
        {
            int LR = worksheet.LastRow( columnName);
            for (int rowIndex = LR; rowIndex > 1; rowIndex--)
            {
                //Range range1 = range.Cells[rowIndex, 1];
                //Range range0 = range.Cells[rowIndex - 1, 1];
                if (worksheet.Range[columnName + rowIndex].Text != worksheet.Range[columnName + (rowIndex - 1)].Text)
                {
                    for (int i = 1; i <= numberOfRowsToAdd; i++)
                    {
                        worksheet.Range[columnName + rowIndex].EntireRow.Insert();
                    }
                }
            }
        }

        public static void AddAllSheetNameToComboBox(ComboBox comboBox, Workbook workbook)
        {
            comboBox.Items.Clear();
            foreach (Worksheet sh in workbook.Worksheets)
            {
                comboBox.Items.Add(sh.Name);
            }
        }

        public static void DeleteRowContainText(Range range, int columnNumber, object criteria)
        {
            Worksheet ws = range.Worksheet;
            ws.AutoFilterMode = false;

            range = range.MinimumRange();
            range.AutoFilter(columnNumber, criteria);

            Range firstCellIncludeHeader = range.Cells[1, 1];
            Range firstCellExceptHeader = firstCellIncludeHeader.Offset[1, 0];
            Range lastCell = range.Cells[range.Rows.Count, range.Columns.Count];

            try//bắt lỗi No cell was found.
            {
                ws.Range[firstCellExceptHeader, lastCell].SpecialCells(XlCellType.xlCellTypeVisible).EntireRow.Delete();
            }
            catch { }

            //không dùng Offset bình thường vì Offset bình thường dịch chuyển cả vùng dữ liệu đi xuống, nghĩa là dòng cuối cùng sẽ bị ảnh hưởng
            //range.Offset[1, 0].SpecialCells(XlCellType.xlCellTypeVisible).EntireRow.Delete();
            ws.AutoFilterMode = false;
        }

        public static void PasteValue(Range range)
        {
            Range destinationRange = range.Cells[1, 1];
            range.Copy();
            destinationRange.PasteSpecial(XlPasteType.xlPasteValues);
            Globals.ThisAddIn.Application.CutCopyMode = 0;
            //destinationRange.Resize[range.Rows.Count, range.Columns.Count].Cells.Value = range.Cells.Value;
        }

        public static void DeleteWorksheet(Workbook workbook, string worksheetName)
        {
            Globals.ThisAddIn.Application.DisplayAlerts = false;
            try
            {
                ((Worksheet)workbook.Worksheets[worksheetName]).Delete();
            }
            catch { }
            Globals.ThisAddIn.Application.DisplayAlerts = true;
        }

        public static void WorksheetToExcelFile(string path, Worksheet worksheet)
        {
            Workbook wb = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet ws = wb.Worksheets[1];
            ws.Name = worksheet.Name;
            worksheet.Cells.Copy(ws.Range["a1"]);

            string validName = Module1.ConvertToValidFileName(worksheet.Name);
            wb.SaveOverWrite(path + "\\" + "_" + validName, XlFileFormat.xlOpenXMLWorkbook);
            //Globals.ThisAddIn.Application.DisplayAlerts = false;
            //wb.SaveAs(path + "\\" + validName);
            //Globals.ThisAddIn.Application.DisplayAlerts = true;
            wb.Close();
        }


        
    }

}
