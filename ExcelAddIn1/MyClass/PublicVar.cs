﻿using ExcelAddIn1.MyClass;
using Microsoft.Office.Interop.Excel;
using System;
using System.Data;
using System.Security;
using System.Threading.Tasks;
using DataTable = System.Data.DataTable;
using VSTOLib.Extensions;

namespace ExcelAddIn1
{
    class PublicVar
    {
        public static string title = "Nguyễn Minh Đức";
        public static string errorTitle = "Lỗi!!!";

        public static string companyName = "Công ty Cổ Phần Đạt Phát Hà Nội";

        public static string sheetNameStockInfo = "StockInfo";
        public static string sheetNameChuyenKho = "chuyenkho";
        public static string sheetNameHangThua = "hangthua";
        public static string sheetGiaoDichBanLe = "banle";
        public static string BangMaSheetName = "bangma10";

        public static string xmlFileServerList = ExcelVSTOAddinFolder() + @"\ServerList.xml";
        public static string xmlMaHangLoaiTru = ExcelVSTOAddinFolder() + @"\MaHangLoaiTru.xml";
        public static string xmlNganhHang = ExcelVSTOAddinFolder() + @"\NganhHang.xml";
        public static string xmlTiLeLai = ExcelVSTOAddinFolder() + @"\TiLeLai.xml";
        public static string xmlAutoKeTay = ExcelVSTOAddinFolder() + @"\AutoKeTay.xml";
        public static string xmlDanhSachSieuThiThanhVien = ExcelVSTOAddinFolder() + @"\DanhSachSieuThiThanhVien.xml";
        public static string credentialsjson_path = ExcelVSTOAddinFolder() + @"\credentials.json";

        public static string microsoftAccountUsername = "ducdoom@gmail.com";
        public static string microsoftAccountPassword = "password.text=1122";

        public static string last_KhoHang;
        public static string last_file_import_xuatban_fullname;

        public static string fileImportNhapHang_fullPath = ExcelVSTOAddinFolder() + @"\FileImportNhapHang.xls";



        public static bool cancelWinappdriver = false;

        public static string fileConfig()
        {
            string folder = System.IO.Path.Combine(ExcelVSTOAddinFolder(), "config");
            if (!System.IO.Directory.Exists(folder))
            {
                _ = System.IO.Directory.CreateDirectory(folder);
            }
            return folder;
        }


        public static string ExcelVSTOAddinFolder()
        {
            string folder = @"C:\ExcelVSTOAddin";
            if (!System.IO.Directory.Exists(folder))
            {
                _ = System.IO.Directory.CreateDirectory(folder);
            }
            return folder;
        }

        public static string MinhQuang_MinvoiceFolder()
        {
            string folder = ExcelVSTOAddinFolder() + "\\" + "MinhQuang M-invoice";
            if (!System.IO.Directory.Exists(folder))
            {
                _ = System.IO.Directory.CreateDirectory(folder);
            }
            return folder;
        }

        public static string Download_PDF_iHoaDon()
        {
            string folder = ExcelVSTOAddinFolder() + "\\" + "Download_PDF_iHoaDon";
            if (!System.IO.Directory.Exists(folder))
            {
                _ = System.IO.Directory.CreateDirectory(folder);
            }
            return folder;
        }

        public static DataTable DT_ServerList()
        {
            using (System.Data.DataSet ds = new System.Data.DataSet("dsServerList"))
            {
                if (System.IO.File.Exists(xmlFileServerList) == false)
                {
                    using (DataTable dt = new DataTable { TableName = "ServerList" })
                    {
                        dt.Columns.Add("Name");
                        dt.Columns.Add("IP");
                        dt.Columns.Add("User");
                        dt.Columns.Add("Pass");

                        dt.Rows.Add(new string[] { "Tổng Kho", "192.168.0.9", "sa", "datphat@hanoi" });
                        dt.Rows.Add(new string[] { "Lê Trọng Tấn", "192.168.6.6", "sa", "123@Datphat" });
                        dt.Rows.Add(new string[] { "Bắc Giang", "192.168.1.200", "sa", "datphat@hanoi" });
                        dt.Rows.Add(new string[] { "Lào Cai", "192.168.5.6", "sa", "datphat@hanoi" });
                        dt.Rows.Add(new string[] { "Bảo Thắng", "192.168.11.6", "sa", "datphat@hanoi" });
                        dt.Rows.Add(new string[] { "Sa Pa", "192.168.12.6", "sa", "datphat@hanoi" });
                        dt.Rows.Add(new string[] { "Tổng kho cũ", "192.168.0.8", "sa", "datphat@hanoi" });

                        dt.WriteXml(xmlFileServerList, System.Data.XmlWriteMode.WriteSchema);
                    }
                }
                ds.ReadXml(xmlFileServerList, System.Data.XmlReadMode.ReadSchema);
                return ds.Tables[0];
            }
        }

        public static DataTable DT_ImportOrExportTranCode()
        {
            DataTable dt = new DataTable { TableName = "TranCode" };
            dt.Columns.Add("Loại giao dịch");
            dt.Columns.Add("TranCode");
            dt.Columns.Add("ExportIDImportID");
            dt.Columns.Add("Price");
            dt.Columns.Add("Total");

            //dt.Rows.Add(new string[] { "Nhập hàng", "01", "ExportID", "ImpPriceVAT", "TotalImpPriceVAT" });
            //dt.Rows.Add(new string[] { "Xuất trả", "05", "ImportID", "ExpPriceVAT", "TotalExpPriceVAT" });

            dt.Rows.Add(new string[] { "Nhập hàng", "01", "ExportID", "ImpPriceVAT", "TotalImpPriceVAT" });
            dt.Rows.Add(new string[] { "Xuất trả", "05", "ImportID", "ExpPriceVAT", "TotalExpPriceVAT" });
            return dt;
        }

        public static DataTable DT_BranchIDs()
        {
            using (DataSet ds = new DataSet())
            {
                if (System.IO.File.Exists(xmlNganhHang) == false)
                {
                    using (DataTable dt = new DataTable { TableName = "NganhHang" })
                    {
                        dt.Columns.Add("Ngành hàng");
                        dt.Columns.Add("Mã ngành");

                        dt.Rows.Add(new string[] { "Thực phẩm", "01,11,12,13,14,15" });
                        dt.Rows.Add(new string[] { "Mỹ phẩm, bách hóa", "02,05,06,17" });
                        dt.Rows.Add(new string[] { "Gia dụng, đồ chơi, dụng cụ học tập", "03,04" });
                        dt.Rows.Add(new string[] { "Thời trang", "07,08,09,10" });

                        dt.WriteXml(xmlNganhHang, XmlWriteMode.WriteSchema);
                    }
                }
                ds.ReadXml(xmlNganhHang, XmlReadMode.ReadSchema);
                return ds.Tables[0];
            }
        }


        public static DataTable DT_TiLeLai()
        {
            using (DataSet ds = new DataSet())
            {
                if (System.IO.File.Exists(xmlTiLeLai) == false)
                {
                    using (DataTable dt = new DataTable { TableName = "TiLeLai" })
                    {
                        dt.Columns.Add("Ngành");
                        dt.Columns.Add("Mã loại vật tư");
                        dt.Columns.Add("Tỉ lệ lãi (%)");

                        dt.Rows.Add(new[] { "Mỹ phẩm bách hóa", "A,B,C,D,E,F,G,Y", "13" });
                        dt.Rows.Add(new[] { "Thực phẩm", "1,3,H,HH,I,J,L", "10" });
                        dt.Rows.Add(new[] { "Sữa, kem, sữa chua", "K", "5" });
                        dt.Rows.Add(new[] { "Thời trang", "M,N,O,P,Q,R,S,T,U,V", "20" });
                        dt.Rows.Add(new[] { "Gia dụng đồ chơi", "W", "13" });

                        dt.WriteXml(xmlTiLeLai, XmlWriteMode.WriteSchema);
                    }
                }
                ds.ReadXml(xmlTiLeLai, XmlReadMode.ReadSchema);
                return ds.Tables[0];
            }
        }

        public static DataTable DT_AutoKeTay()
        {
            using (DataSet ds = new DataSet())
            {
                if (System.IO.File.Exists(xmlAutoKeTay) == false)
                {
                    using (DataTable dt = new DataTable { TableName = "AutoKeTay" })
                    {
                        dt.Columns.Add("Tên phương thức");
                        dt.Columns.Add("code");

                        dt.Rows.Add(new[] { "Tất cả", "" });
                        dt.Rows.Add(new[] { "Kê tay", "dbo.Transactions.TransactionID Not Like 'A%' And" });
                        dt.Rows.Add(new[] { "Tự động", "dbo.Transactions.TransactionID Like 'A%' And" });

                        dt.WriteXml(xmlAutoKeTay, XmlWriteMode.WriteSchema);
                    }
                }
                ds.ReadXml(xmlAutoKeTay, XmlReadMode.ReadSchema);
                return ds.Tables[0];
            }
        }

        public static DataTable DT_DanhSachSieuThiThanhVien()
        {
            string xmlFile = xmlDanhSachSieuThiThanhVien;
            using (DataSet ds = new DataSet())
            {
                if (System.IO.File.Exists(xmlFile) == false)
                {
                    using (DataTable dt = new DataTable { TableName = "DanhSachSieuThiThanhVien" })
                    {
                        dt.Columns.Add("Tên siêu thị");
                        dt.Columns.Add("IP");

                        dt.Rows.Add(new[] { "Lê Trọng Tấn", "LETRONGTAN" });
                        dt.Rows.Add(new[] { "Lào Cai", "LAOCAI" });
                        dt.Rows.Add(new[] { "Sa Pa", "SAPA" });
                        dt.Rows.Add(new[] { "Yên Dũng", "YDACCOUNTING" });

                        dt.WriteXml(xmlFile, XmlWriteMode.WriteSchema);
                    }
                }
                ds.ReadXml(xmlFile, XmlReadMode.ReadSchema);
                return ds.Tables[0];
            }
        }


        public static string ImportNhapHangSaveLocation()
        {
            ////string setting = Properties.Settings.Default.ImportInvoiceSaveLocation;
            //if (string.IsNullOrEmpty(Properties.Settings.Default.ImportInvoiceSaveLocation))
            //{
            //    Properties.Settings.Default.ImportInvoiceSaveLocation = ExcelVSTOAddinFolder();
            //    Properties.Settings.Default.Save();
            //}
            return ExcelVSTOAddinFolder();
        }

        public static SecureString MicrosoftAcountPassword()
        {
            SecureString pass = new SecureString();

            string password = PublicVar.microsoftAccountPassword;
            foreach (char c in password)
            {
                pass.AppendChar(c);
            }

            return pass;
        }

        public static string AddinLocation
        {
            get
            {
                System.Reflection.Assembly assemblyInfo = System.Reflection.Assembly.GetExecutingAssembly();
                Uri uriCodeBase = new Uri(assemblyInfo.CodeBase);
                string ClickOnceLocation = System.IO.Path.GetDirectoryName(uriCodeBase.LocalPath.ToString());
                return ClickOnceLocation;
            }
        }

        public static string CompanyName = "Công ty Cổ Phần Đạt Phát Hà Nội";

        public static string ConnectionStringOracleDatPhat
        {
            get
            {
                Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder stringBuilder = new Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder();
                stringBuilder.DataSource = $"192.168.0.50:1522/TBNETERP";
                stringBuilder.UserID = "TBNETERP";
                stringBuilder.Password = "TBNETERP";
                return stringBuilder.ConnectionString;
            }
        }

    }
}
