﻿namespace ExcelAddIn1.MyClass
{
    public static class ClipboardSafeInvoke
    {
        //https://stackoverflow.com/questions/17762037/current-thread-must-be-set-to-single-thread-apartment-sta-error-in-copy-stri

        public static void SetText(string p_Text)
        {
            System.Threading.Thread STAThread = new System.Threading.Thread(
                  delegate ()
                  {
                    // Use a fully qualified name for Clipboard otherwise it
                    // will end up calling itself.
                    System.Windows.Forms.Clipboard.SetText(p_Text);
                  });
            STAThread.SetApartmentState(System.Threading.ApartmentState.STA);
            STAThread.Start();
            STAThread.Join();
        }
        public static string GetText()
        {
            string ReturnValue = string.Empty;
            System.Threading.Thread STAThread = new System.Threading.Thread(
                delegate ()
                {
                    // Use a fully qualified name for Clipboard otherwise it
                    // will end up calling itself.
                    ReturnValue = System.Windows.Forms.Clipboard.GetText();
                });
            STAThread.SetApartmentState(System.Threading.ApartmentState.STA);
            STAThread.Start();
            STAThread.Join();

            return ReturnValue;
        }
    }
}
