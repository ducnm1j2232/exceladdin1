﻿using Microsoft.Office.Interop.Excel;
using VSTOLib.Extensions;

namespace ExcelAddIn1.MyClass.ExcelGodClass
{
    class Func
    {
        public static string GetValueFromWorkbookWithoutOpening(string fileName, string sheetName, string rangeAddress)
        {
            Globals.ThisAddIn.Application.ScreenUpdating = false;
            Workbook workbook = Globals.ThisAddIn.Application.Workbooks.Open(fileName);
            string result = workbook.GetSheet(sheetName).Range[rangeAddress].GetValue<string>();
            workbook.Close(false);
            Globals.ThisAddIn.Application.ScreenUpdating = true;
            return result;

        }
    }
}
