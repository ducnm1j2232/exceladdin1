﻿using System.Collections.Generic;


namespace ExcelAddIn1.MyClass
{
    class Mathematics
    {
        public static bool isSubsetSum(int[] set, int n, int sum)
        {
            // Base Cases 
            if (sum == 0)
                return true;
            if (n == 0)
                return false;

            // If last element is greater than sum, 
            // then ignore it 
            if (set[n - 1] > sum)
                return isSubsetSum(set, n - 1, sum);

            /* else, check if sum can be obtained  
            by any of the following 
            (a) including the last element 
            (b) excluding the last element */
            return isSubsetSum(set, n - 1, sum) || isSubsetSum(set, n - 1, sum - set[n - 1]);
        }



        // The vector v stores current subset.  
        public static void printAllSubsetsRec(int[] inputSet, int n, List<int> currentSubset, int sum, List<int> outputSubset)
        {
            // If remaining sum is 0, then print all elements of current subset. 
            if (outputSubset.Count == 0)//vì chỉ lấy kết quả đầu tiên nên check Count = 0
            {
                if (sum == 0)
                {
                    for (int i = 0; i < currentSubset.Count; i++)
                    {
                        outputSubset.Add(currentSubset[i]);
                    }  
                }
            }
            else
            {
                return;
            }

            // If no remaining elements,  
            if (n == 0) return;

            printAllSubsetsRec(inputSet, n - 1, currentSubset, sum, outputSubset);
            List<int> currentSubset1 = new List<int>(currentSubset);
            currentSubset1.Add(inputSet[n - 1]);
            printAllSubsetsRec(inputSet, n - 1, currentSubset1, sum - inputSet[n - 1], outputSubset);
        }

        // Wrapper over printAllSubsetsRec()  
        public static void printAllSubsets(int[] inputSet, int n, int sum, List<int> outputSubset)
        {
            List<int> v = new List<int>();
            printAllSubsetsRec(inputSet, n, v, sum, outputSubset);
        }

        /// <summary>
        /// Remember to check List.Count > 0
        /// </summary>
        /// <param name="inputSet"></param>
        /// <param name="sum"></param>
        /// <returns></returns>
        public static List<int> SubsetEqualToGivenSum(int[] inputSet,int n, int sum)
        {
            List<int> output = new List<int>();
            Mathematics.printAllSubsets(inputSet, n, sum, output);
            return output;
        }


    }
}
