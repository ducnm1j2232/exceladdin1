﻿namespace ExcelAddIn1.MyClass
{

    public static class Noti
    {
        public static ExcelAddIn1.MyUserControl.FakeForm fakeForm = new MyUserControl.FakeForm();


        public static void ShowToast(string text, string caption = "")
        {
            if (fakeForm == null)
            {
                fakeForm = new MyUserControl.FakeForm();
            }
            fakeForm.ShowAlertCore(text, caption);
        }


    }

    
}
