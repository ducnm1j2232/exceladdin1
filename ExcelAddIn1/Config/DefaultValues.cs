﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ExcelAddIn1.Config
{
    public static class DefaultValues
    {
        private static string _ExcelVSTOAddinFolder = @"C:\ExcelVSTOAddin";
        private static string _PublicFolder = @"C:\Users\Public";

        private static string GetPath(string path)
        {
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            return path;
        }

        public static string ExcelVSTOAddinFolder => GetPath(_ExcelVSTOAddinFolder);
        public static string PublicFolder => GetPath(_PublicFolder);
        public static string PublicImportFolder => GetPath(Path.Combine(PublicFolder, "ImportFiles"));
        public static string EInvoiceSaveFolder => GetPath(Path.Combine(ExcelVSTOAddinFolder, "DownloadiHoaDonPDF"));
        public  static string ImportPDFSaveFolder => GetPath(Path.Combine(ExcelVSTOAddinFolder, "ImportPDFSaveFolder"));
    }
}
