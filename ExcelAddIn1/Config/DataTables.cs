﻿using System.Data;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.Config
{
    public static class DataTables
    {
        public const string dataSetName = "dsSettings";
        public const string dataTableName = "dtSettings";



        public static DataTable GlobalSettings
        {
            get
            {
                using (DataSet ds = new DataSet(dataSetName))
                {
                    DataTable defaultDatatable = new DataTable(dataTableName);
                    defaultDatatable.Columns.Add("ID");
                    defaultDatatable.Columns.Add("Description");
                    defaultDatatable.Columns.Add("Value");

                    defaultDatatable.Rows.Add(new string[] { ID.PublicFolder, "Đường dẫn folder public", DefaultValues.PublicFolder });
                    defaultDatatable.Rows.Add(new string[] { ID.PublicImportFolder, "Đường dẫn folder public chứa file import ", DefaultValues.PublicImportFolder });
                    defaultDatatable.Rows.Add(new string[] { ID.InvoicePDFSaveFolder, "Đường dẫn folder lưu file pdf hóa đơn điện tử tải về", DefaultValues.EInvoiceSaveFolder });
                    defaultDatatable.Rows.Add(new string[] { ID.ImportPDFSaveFolder, "Đường dẫn folder lưu file pdf phiếu nhập kho", DefaultValues.ImportPDFSaveFolder });
                    
                    defaultDatatable.Rows.Add(new string[] { ID.OneDriveDatabaseAccessFile, "OneDriveDatabaseAccessFile", ""});
                    
                    defaultDatatable.Rows.Add(new string[] { ID.ihoadonMST, "Mã số thuế ihoadon", "0106188175" });
                    defaultDatatable.Rows.Add(new string[] { ID.ihoadonUsername, "Tài khoản ihoadon", "0106188175" });
                    defaultDatatable.Rows.Add(new string[] { ID.ihoadonPassword, "Mật khẩu ihoadon", "thanhcong86" });

                    defaultDatatable.Rows.Add(new string[] { ID.PhiChuyenTienTrongVCB, "Phí chuyển tiền trong VCB", "7000" });
                    defaultDatatable.Rows.Add(new string[] { ID.PhiChuyenTienNgoaiVCB, "Phí chuyển tiền ngoài VCB", "20000" });


                    if (System.IO.File.Exists(Config.XmlFilename.GlobalSettings))
                    {
                        DataSet ds2 = new DataSet("local");
                        ds2.ReadXml(Config.XmlFilename.GlobalSettings, XmlReadMode.ReadSchema);
                        DataTable localData = ds2.Tables[dataTableName];
                        foreach (DataRow dataRow in defaultDatatable.Rows)
                        {
                            string defaultID = dataRow["ID"].ToString();
                            string defaultValue = dataRow["Value"].ToString();

                            DataRow[] localDataRows = localData.Select($"ID ='{defaultID}'");
                            if (localDataRows.Length > 0)
                            {
                                string localValue = localDataRows[0]["Value"].ToString();
                                if (defaultValue != localValue)
                                {
                                    dataRow["Value"] = localValue;
                                }
                            }
                        }

                        if (System.IO.File.Exists(Config.XmlFilename.GlobalSettings))
                        {
                            System.IO.File.Delete(Config.XmlFilename.GlobalSettings);
                        }

                        defaultDatatable.WriteXml(Config.XmlFilename.GlobalSettings, System.Data.XmlWriteMode.WriteSchema);
                    }

                    if (!System.IO.File.Exists(Config.XmlFilename.GlobalSettings))
                    {
                        defaultDatatable.WriteXml(Config.XmlFilename.GlobalSettings, System.Data.XmlWriteMode.WriteSchema);
                    }

                    ds.ReadXml(Config.XmlFilename.GlobalSettings, XmlReadMode.ReadSchema);
                    DataTable result = ds.Tables[dataTableName];
                    return result;
                }

            }

        }
    }
}
