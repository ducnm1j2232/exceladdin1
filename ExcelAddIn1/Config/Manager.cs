﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ExcelAddIn1.Config
{
    public static class Manager
    {
        public static string GetValue(string ID)
        {
            //DataSet dataSet = new DataSet(Config.DataTables.dataSetName);
            //dataSet.ReadXml(Config.XmlFilename.GlobalSettings,XmlReadMode.ReadSchema);
            DataTable dt = DataTables.GlobalSettings;
            DataRow[] dataRow = dt.Select($"ID ='{ID}'");
            if (dataRow.Length > 0)
            {
                return dataRow[0]["Value"].ToString();
            }
            else
            {
                throw new Exception($"cannot get setting value of ID:{ID}");
            }
        }

    }
}
