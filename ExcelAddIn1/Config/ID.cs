﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.Config
{
    public class ID
    {
        

        public const string PublicFolder = "PublicFolder";
        public const string PublicImportFolder = "PublicImportFolder";
        public const string InvoicePDFSaveFolder = "InvoicePDFSaveFolder";
        public const string ImportPDFSaveFolder = "ImportPDFSaveFolder";
        public const string OneDriveDatabaseAccessFile = "OneDriveDatabaseAccessFile";

        public const string ihoadonMST = "ihoadonMST";
        public const string ihoadonUsername = "ihoadonUsername";
        public const string ihoadonPassword = "ihoadonPassword";
        
        public const string PhiChuyenTienTrongVCB = "PhiChuyenTienTrongVCB";
        public const string PhiChuyenTienNgoaiVCB = "PhiChuyenTienNgoaiVCB";

    }
}
