﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelAddIn1.MyUserControl
{
    public partial class TextBoxOpenFileDialog : TextBox
    {
        public TextBoxOpenFileDialog()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }



        protected override void OnDoubleClick(EventArgs e)
        {
            base.OnDoubleClick(e);
            using(OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    base.Text = openFileDialog.FileName;
                }
            }
        }
    }
}
