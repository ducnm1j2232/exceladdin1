﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using VSTOLib.Extensions;

namespace ExcelAddIn1
{

    public partial class Popup : Form
    {
        public enum PopupType { Success, Error }


        public Popup(string message, PopupType popupType = PopupType.Success)
        {
            InitializeComponent();

            RichTextBoxMessage.Text = message;

            #region color       
            Color controlColor = Color.FromArgb(98, 0, 238);
            switch (popupType)
            {
                case PopupType.Success:
                    controlColor = Color.FromArgb(101, 31, 255);
                    break;
                case PopupType.Error:
                    controlColor = Color.FromArgb(204, 0, 0);
                    break;
            }

            BackColor = controlColor;

            RichTextBoxMessage.BackColor = controlColor;
            RichTextBoxMessage.ForeColor = Color.White;

            #endregion



        }


        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                //make sure Top Most property on form is set to false
                //otherwise this doesn't work
                int WS_EX_TOPMOST = 0x00000008;
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= WS_EX_TOPMOST;
                return cp;
            }
        }


        public static List<Popup> popups = new System.Collections.Generic.List<Popup>();

        private static int newPopupHeight = 0;

        public static void Show(string message, PopupType popupType = PopupType.Success)
        {
            //Task.Run(() =>
            //{
            //    #region MAIN
            //    MoveUpAllCurrentPopup();


            //    Popup newPopup = new Popup(message, popupType);

            //    popups.Add(newPopup);
            //    newPopup.ShowDialog();

            //    #endregion

            //}).ConfigureAwait(false);


            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                #region MAIN
                MoveUpAllCurrentPopup();
                Popup newPopup = new Popup(message, popupType);
                popups.Add(newPopup);
                newPopup.ShowDialog();
                #endregion
            }));
            thread.Start();
            //thread.Join();

        }


        private static void Fly(int px)
        {
            for (int i = 0; i < px; i++)
            {
                //Top--;
            }

        }

        private static void MoveUpAllCurrentPopup()
        {
            //List<Popup> currentPopups = popups;
            for (int index = 0; index < popups.Count; index++)
            {
                if (!popups[index].IsDisposed)
                {
                    try
                    {
                        popups[index].MoveUp(newPopupHeight + 5);
                    }
                    catch { }
                }
            }
            //for (int i = 0; i < newPopupHeight + 5; i++)
            //{

            //    List<Popup> currentPopups = popups;
            //    for (int index = 0; index < currentPopups.Count; index++)
            //    {
            //        if (!popups[index].IsDisposed)
            //        {
            //            try
            //            {
            //                popups[index].MoveUp(newPopupHeight + 5);
            //            }
            //            catch { }
            //        }
            //    }
            //}
        }


        private void MoveUp(int point = 1)
        {
            try
            {
                this.SafeInvoke(() =>
                {
                    Top -= point;
                });
            }
            catch { }
        }


        private void ButtonClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }


        //event để tự động resize kích thước của richtextbox
        private void RichTextBoxMessage_ContentsResized(object sender, ContentsResizedEventArgs e)
        {
            ((RichTextBox)sender).Height = e.NewRectangle.Height;
            //Height = Math.Min(RichTextBoxMessage.Height + ButtonClose.Height + 12, Screen.PrimaryScreen.WorkingArea.Height);
            Height = Math.Min(RichTextBoxMessage.Height + ((RichTextBox)sender).Location.X * 2, Screen.PrimaryScreen.WorkingArea.Height);
            newPopupHeight = Height;

            //sau khi xác định được Height thì mới set vị trí Top Left được, vì nó theo trình tự event
            #region position
            StartPosition = FormStartPosition.Manual;
            //Random random = new Random();
            //Top = random.Next(1, Math.Max(Screen.PrimaryScreen.WorkingArea.Height - Height, 1));
            Top = Screen.PrimaryScreen.WorkingArea.Height - Height;
            Left = Screen.PrimaryScreen.WorkingArea.Width - Width;
            #endregion
        }


        int disposeFormTimer = 5;
        private void Popup_Load(object sender, EventArgs e)
        {
            //Timer timer = new Timer();
            //timer.Interval = 1000;
            //timer.Enabled = true;
            //timer.Start();
            //timer.Tick += new System.EventHandler(timer_tick);
        }


        private void timer_tick(object sender, EventArgs e)
        {
            disposeFormTimer--;

            if (disposeFormTimer >= 0)
            {
                //newMessageBox.lblTimer.Text = disposeFormTimer.ToString();
            }
            else
            {
                //popups.Remove(this);
                //Dispose();
                CloseAndDispose();
            }

        }


        private void CloseAndDispose()
        {

            //    for (int i = 0; i < 1000; i++)
            //    {
            //        Opacity -= 0.001;
            //        Left += 1;
            //    }
            popups.Remove(this);
            Dispose();


        }

        private void Popup_Shown(object sender, EventArgs e)
        {
            Timer timer = new Timer();
            timer.Interval = 1000;
            timer.Enabled = true;
            timer.Start();
            timer.Tick += new System.EventHandler(timer_tick);
        }
    }



}

