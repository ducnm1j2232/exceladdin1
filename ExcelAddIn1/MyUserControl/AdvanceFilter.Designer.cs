﻿namespace ExcelAddIn1.MyUserControl
{
    partial class AdvanceFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxFilterRange = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonSelectFilterRange = new System.Windows.Forms.Button();
            this.dataGridViewAdvanceFilter = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonApplyFilter = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonSmartFind = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdvanceFilter)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBoxFilterRange
            // 
            this.TextBoxFilterRange.Location = new System.Drawing.Point(3, 34);
            this.TextBoxFilterRange.Name = "TextBoxFilterRange";
            this.TextBoxFilterRange.Size = new System.Drawing.Size(287, 20);
            this.TextBoxFilterRange.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Vùng dữ liệu";
            // 
            // ButtonSelectFilterRange
            // 
            this.ButtonSelectFilterRange.Location = new System.Drawing.Point(296, 32);
            this.ButtonSelectFilterRange.Name = "ButtonSelectFilterRange";
            this.ButtonSelectFilterRange.Size = new System.Drawing.Size(31, 23);
            this.ButtonSelectFilterRange.TabIndex = 2;
            this.ButtonSelectFilterRange.Text = "...";
            this.ButtonSelectFilterRange.UseVisualStyleBackColor = true;
            this.ButtonSelectFilterRange.Click += new System.EventHandler(this.ButtonSelectFilterRange_Click);
            // 
            // dataGridViewAdvanceFilter
            // 
            this.dataGridViewAdvanceFilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAdvanceFilter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dataGridViewAdvanceFilter.Location = new System.Drawing.Point(3, 61);
            this.dataGridViewAdvanceFilter.Name = "dataGridViewAdvanceFilter";
            this.dataGridViewAdvanceFilter.Size = new System.Drawing.Size(324, 264);
            this.dataGridViewAdvanceFilter.TabIndex = 3;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Tiêu đề";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "Giá trị lọc";
            this.Column2.Name = "Column2";
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ButtonApplyFilter
            // 
            this.ButtonApplyFilter.Location = new System.Drawing.Point(252, 331);
            this.ButtonApplyFilter.Name = "ButtonApplyFilter";
            this.ButtonApplyFilter.Size = new System.Drawing.Size(75, 23);
            this.ButtonApplyFilter.TabIndex = 4;
            this.ButtonApplyFilter.Text = "Lọc";
            this.ButtonApplyFilter.UseVisualStyleBackColor = true;
            this.ButtonApplyFilter.Click += new System.EventHandler(this.ButtonApplyFilter_Click);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(3, 333);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(243, 84);
            this.textBox1.TabIndex = 5;
            this.textBox1.Text = "Lọc với nhiều hơn 2 điều kiện bằng cách sử dụng dấu phẩy ( , )\r\n\r\nDấu bằng ( = ) " +
    ": ô trống (Blanks)\r\nDấu khác ( <> ) : ô không trống (nonblank)";
            // 
            // buttonSmartFind
            // 
            this.buttonSmartFind.Location = new System.Drawing.Point(252, 360);
            this.buttonSmartFind.Name = "buttonSmartFind";
            this.buttonSmartFind.Size = new System.Drawing.Size(75, 23);
            this.buttonSmartFind.TabIndex = 6;
            this.buttonSmartFind.Text = "Tìm kiếm";
            this.buttonSmartFind.UseVisualStyleBackColor = true;
            this.buttonSmartFind.Click += new System.EventHandler(this.buttonSmartFind_Click);
            // 
            // AdvanceFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonSmartFind);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.ButtonApplyFilter);
            this.Controls.Add(this.dataGridViewAdvanceFilter);
            this.Controls.Add(this.ButtonSelectFilterRange);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxFilterRange);
            this.Name = "AdvanceFilter";
            this.Size = new System.Drawing.Size(330, 430);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdvanceFilter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxFilterRange;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonSelectFilterRange;
        private System.Windows.Forms.DataGridView dataGridViewAdvanceFilter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Button ButtonApplyFilter;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonSmartFind;
    }
}
