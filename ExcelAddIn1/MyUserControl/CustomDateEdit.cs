﻿using System.Windows.Forms;

namespace ExcelAddIn1.MyUserControl
{
    public partial class CustomDateEdit : DevExpress.XtraEditors.DateEdit
    {
        public CustomDateEdit()
        {
            InitializeComponent();
            
            base.Properties.MaskSettings.Configure<DevExpress.XtraEditors.Mask.MaskSettings.DateTime>().UseAdvancingCaret = true;
            base.Properties.MaskSettings.Configure<DevExpress.XtraEditors.Mask.MaskSettings.DateTime>().MaskExpression = "dd/MM/yyyy";
            base.Properties.MaskSettings.UseMaskAsDisplayFormat = true;
            base.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            base.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            base.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            base.Properties.EditFormat.FormatString = "dd/MM/yyyy";


        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }


    }
}
