﻿namespace ExcelAddIn1
{
    partial class Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RichTextBoxMessage = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // RichTextBoxMessage
            // 
            this.RichTextBoxMessage.BackColor = System.Drawing.Color.Gray;
            this.RichTextBoxMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RichTextBoxMessage.Cursor = System.Windows.Forms.Cursors.Default;
            this.RichTextBoxMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RichTextBoxMessage.ForeColor = System.Drawing.SystemColors.Window;
            this.RichTextBoxMessage.Location = new System.Drawing.Point(12, 12);
            this.RichTextBoxMessage.Name = "RichTextBoxMessage";
            this.RichTextBoxMessage.ReadOnly = true;
            this.RichTextBoxMessage.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.RichTextBoxMessage.Size = new System.Drawing.Size(276, 30);
            this.RichTextBoxMessage.TabIndex = 1;
            this.RichTextBoxMessage.Text = "Sample text";
            this.RichTextBoxMessage.ContentsResized += new System.Windows.Forms.ContentsResizedEventHandler(this.RichTextBoxMessage_ContentsResized);
            // 
            // Popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(300, 54);
            this.Controls.Add(this.RichTextBoxMessage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Popup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Popup";
            this.Load += new System.EventHandler(this.Popup_Load);
            this.Shown += new System.EventHandler(this.Popup_Shown);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RichTextBox RichTextBoxMessage;
    }
}