﻿namespace ExcelAddIn1.MyUserControl
{
    partial class UCShowAllSheetNames
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewAllSheetNames = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ButtonUnhideSheets = new System.Windows.Forms.Button();
            this.buttonWorksheetToFiles = new System.Windows.Forms.Button();
            this.buttonCheckAll = new System.Windows.Forms.Button();
            this.buttonRemoveCheckAll = new System.Windows.Forms.Button();
            this.ButtonPrint = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listViewAllSheetNames
            // 
            this.listViewAllSheetNames.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewAllSheetNames.CheckBoxes = true;
            this.listViewAllSheetNames.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listViewAllSheetNames.FullRowSelect = true;
            this.listViewAllSheetNames.HideSelection = false;
            this.listViewAllSheetNames.Location = new System.Drawing.Point(3, 3);
            this.listViewAllSheetNames.Name = "listViewAllSheetNames";
            this.listViewAllSheetNames.Size = new System.Drawing.Size(174, 263);
            this.listViewAllSheetNames.TabIndex = 2;
            this.listViewAllSheetNames.UseCompatibleStateImageBehavior = false;
            this.listViewAllSheetNames.View = System.Windows.Forms.View.Details;
            this.listViewAllSheetNames.SelectedIndexChanged += new System.EventHandler(this.listViewAllSheetNames_SelectedIndexChanged);
            this.listViewAllSheetNames.VisibleChanged += new System.EventHandler(this.listViewAllSheetNames_VisibleChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Tên sheet";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "";
            // 
            // ButtonUnhideSheets
            // 
            this.ButtonUnhideSheets.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonUnhideSheets.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ButtonUnhideSheets.Location = new System.Drawing.Point(3, 307);
            this.ButtonUnhideSheets.Name = "ButtonUnhideSheets";
            this.ButtonUnhideSheets.Size = new System.Drawing.Size(172, 23);
            this.ButtonUnhideSheets.TabIndex = 3;
            this.ButtonUnhideSheets.Text = "Hiện sheet";
            this.ButtonUnhideSheets.UseVisualStyleBackColor = true;
            this.ButtonUnhideSheets.Click += new System.EventHandler(this.ButtonUnhideSheets_Click);
            // 
            // buttonWorksheetToFiles
            // 
            this.buttonWorksheetToFiles.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonWorksheetToFiles.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonWorksheetToFiles.Location = new System.Drawing.Point(3, 335);
            this.buttonWorksheetToFiles.Name = "buttonWorksheetToFiles";
            this.buttonWorksheetToFiles.Size = new System.Drawing.Size(172, 23);
            this.buttonWorksheetToFiles.TabIndex = 4;
            this.buttonWorksheetToFiles.Text = "Sheets -> Files";
            this.buttonWorksheetToFiles.UseVisualStyleBackColor = true;
            this.buttonWorksheetToFiles.Click += new System.EventHandler(this.buttonWorksheetToFiles_Click);
            // 
            // buttonCheckAll
            // 
            this.buttonCheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonCheckAll.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonCheckAll.Location = new System.Drawing.Point(3, 271);
            this.buttonCheckAll.Name = "buttonCheckAll";
            this.buttonCheckAll.Size = new System.Drawing.Size(81, 23);
            this.buttonCheckAll.TabIndex = 5;
            this.buttonCheckAll.Text = "Chọn tất cả";
            this.buttonCheckAll.UseVisualStyleBackColor = true;
            this.buttonCheckAll.Click += new System.EventHandler(this.buttonCheckAll_Click);
            // 
            // buttonRemoveCheckAll
            // 
            this.buttonRemoveCheckAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonRemoveCheckAll.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonRemoveCheckAll.Location = new System.Drawing.Point(90, 271);
            this.buttonRemoveCheckAll.Name = "buttonRemoveCheckAll";
            this.buttonRemoveCheckAll.Size = new System.Drawing.Size(85, 23);
            this.buttonRemoveCheckAll.TabIndex = 6;
            this.buttonRemoveCheckAll.Text = "Bỏ chọn tất cả";
            this.buttonRemoveCheckAll.UseVisualStyleBackColor = true;
            this.buttonRemoveCheckAll.Click += new System.EventHandler(this.buttonRemoveCheckAll_Click);
            // 
            // ButtonPrint
            // 
            this.ButtonPrint.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonPrint.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ButtonPrint.Location = new System.Drawing.Point(3, 362);
            this.ButtonPrint.Name = "ButtonPrint";
            this.ButtonPrint.Size = new System.Drawing.Size(172, 23);
            this.ButtonPrint.TabIndex = 7;
            this.ButtonPrint.Text = "Print";
            this.ButtonPrint.UseVisualStyleBackColor = true;
            this.ButtonPrint.Click += new System.EventHandler(this.ButtonPrint_Click);
            // 
            // UCShowAllSheetNames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ButtonPrint);
            this.Controls.Add(this.buttonRemoveCheckAll);
            this.Controls.Add(this.buttonCheckAll);
            this.Controls.Add(this.buttonWorksheetToFiles);
            this.Controls.Add(this.ButtonUnhideSheets);
            this.Controls.Add(this.listViewAllSheetNames);
            this.Name = "UCShowAllSheetNames";
            this.Size = new System.Drawing.Size(180, 400);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListView listViewAllSheetNames;
        private System.Windows.Forms.Button ButtonUnhideSheets;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button buttonWorksheetToFiles;
        private System.Windows.Forms.Button buttonCheckAll;
        private System.Windows.Forms.Button buttonRemoveCheckAll;
        private System.Windows.Forms.Button ButtonPrint;
    }
}
