﻿using System;
using System.Windows.Forms;

namespace ExcelAddIn1.MyUserControl
{
    public partial class FolderBroswerTextBox : UserControl
    {
        public FolderBroswerTextBox()
        {
            InitializeComponent();
        }

        public enum EnumFolderType { Folder, File }

        public string FolderPath
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        public EnumFolderType FolderType { get; set; }


        private void buttonFolderBroswer_Click(object sender, EventArgs e)
        {
            SelectFolder(FolderType);
        }

        public void SelectFolder(EnumFolderType folderType, string description = "")
        {

            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
            {
                folderBrowserDialog.Description = description;
                DialogResult dialogResult = folderBrowserDialog.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {
                    textBox1.Text = folderBrowserDialog.SelectedPath;
                }
            }
        }



    }
}
