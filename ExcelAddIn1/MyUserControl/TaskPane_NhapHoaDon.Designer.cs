﻿namespace ExcelAddIn1
{
    partial class TaskPane_NhapHoaDon
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxSearch = new System.Windows.Forms.TextBox();
            this.bnSearch = new System.Windows.Forms.Button();
            this.Lview1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label4 = new System.Windows.Forms.Label();
            this.bnGetSelectedTrans = new System.Windows.Forms.Button();
            this.bnSelectRows = new System.Windows.Forms.Button();
            this.bnReCalculateTotalAmount = new System.Windows.Forms.Button();
            this.bnCreateImportFile = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.bnNew = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxServerList = new System.Windows.Forms.ComboBox();
            this.comboBoxDataBase = new System.Windows.Forms.ComboBox();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.ComboboxImportOrExport = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBoxSoHoaDon = new ExcelAddIn1.CustomTextBox();
            this.textBoxKyHieuHoaDon = new ExcelAddIn1.CustomTextBox();
            this.textBoxSoTien = new ExcelAddIn1.CustomTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDownNumberOfCopies = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.gridLookUpEditTenNCC = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEditTenNCCView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.button_FlauUI_AutoImport = new System.Windows.Forms.Button();
            this.DTPNgayHoaDon = new ExcelAddIn1.CustomDateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.ButtonPopupStockInfo = new System.Windows.Forms.Button();
            this.ButtonReportImportProvider = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.buttonViewReport = new System.Windows.Forms.Button();
            this.buttonLoadCustomersToDataTable = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.comboBoxSheetName1 = new ExcelAddIn1.MyUserControl.ComboBoxSheetName();
            this.ButtonTimKiemBill = new System.Windows.Forms.Button();
            this.TextBoxMaGD = new ExcelAddIn1.CustomTextBox();
            this.Button1_ChuanBiDuLieu = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.gridLookUpEditSearch = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dtpFromDate = new ExcelAddIn1.CustomDateTimePicker();
            this.dtpToDate = new ExcelAddIn1.CustomDateTimePicker();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumberOfCopies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTenNCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTenNCCView)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Từ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(179, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Đến";
            // 
            // TextBoxSearch
            // 
            this.TextBoxSearch.Location = new System.Drawing.Point(28, 10);
            this.TextBoxSearch.Name = "TextBoxSearch";
            this.TextBoxSearch.Size = new System.Drawing.Size(180, 20);
            this.TextBoxSearch.TabIndex = 1;
            this.TextBoxSearch.Click += new System.EventHandler(this.TboxSearch_Enter);
            this.TextBoxSearch.Enter += new System.EventHandler(this.TboxSearch_Enter);
            this.TextBoxSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TboxSearch_KeyDown);
            // 
            // bnSearch
            // 
            this.bnSearch.Location = new System.Drawing.Point(214, 8);
            this.bnSearch.Name = "bnSearch";
            this.bnSearch.Size = new System.Drawing.Size(84, 23);
            this.bnSearch.TabIndex = 2;
            this.bnSearch.Text = "2. Tìm kiếm";
            this.bnSearch.UseVisualStyleBackColor = false;
            this.bnSearch.Click += new System.EventHandler(this.BnSearch_Click);
            this.bnSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BnSearch_KeyDown);
            // 
            // Lview1
            // 
            this.Lview1.CheckBoxes = true;
            this.Lview1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.Lview1.FullRowSelect = true;
            this.Lview1.GridLines = true;
            this.Lview1.HideSelection = false;
            this.Lview1.Location = new System.Drawing.Point(10, 37);
            this.Lview1.Name = "Lview1";
            this.Lview1.Size = new System.Drawing.Size(288, 166);
            this.Lview1.TabIndex = 3;
            this.Lview1.UseCompatibleStateImageBehavior = false;
            this.Lview1.View = System.Windows.Forms.View.Details;
            this.Lview1.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.Lview1_ItemChecked);
            this.Lview1.SelectedIndexChanged += new System.EventHandler(this.Lview1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "3. Ngày";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Số tiền";
            this.columnHeader2.Width = 200;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Mã giao dịch";
            this.columnHeader3.Width = 102;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "0";
            // 
            // bnGetSelectedTrans
            // 
            this.bnGetSelectedTrans.Location = new System.Drawing.Point(182, 209);
            this.bnGetSelectedTrans.Name = "bnGetSelectedTrans";
            this.bnGetSelectedTrans.Size = new System.Drawing.Size(116, 23);
            this.bnGetSelectedTrans.TabIndex = 6;
            this.bnGetSelectedTrans.Text = "4.2. Chọn cả phiếu";
            this.bnGetSelectedTrans.UseVisualStyleBackColor = false;
            this.bnGetSelectedTrans.Click += new System.EventHandler(this.BnGetSelectedTrans_Click);
            // 
            // bnSelectRows
            // 
            this.bnSelectRows.Location = new System.Drawing.Point(79, 209);
            this.bnSelectRows.Name = "bnSelectRows";
            this.bnSelectRows.Size = new System.Drawing.Size(97, 23);
            this.bnSelectRows.TabIndex = 5;
            this.bnSelectRows.Text = "4.1. Chọn mã";
            this.bnSelectRows.UseVisualStyleBackColor = false;
            this.bnSelectRows.Click += new System.EventHandler(this.BnSelectRows_Click);
            // 
            // bnReCalculateTotalAmount
            // 
            this.bnReCalculateTotalAmount.Location = new System.Drawing.Point(182, 235);
            this.bnReCalculateTotalAmount.Name = "bnReCalculateTotalAmount";
            this.bnReCalculateTotalAmount.Size = new System.Drawing.Size(116, 23);
            this.bnReCalculateTotalAmount.TabIndex = 17;
            this.bnReCalculateTotalAmount.Text = "5.2. Tính lại tổng tiền";
            this.bnReCalculateTotalAmount.UseVisualStyleBackColor = false;
            this.bnReCalculateTotalAmount.Click += new System.EventHandler(this.BnReCalculateTotalAmount_Click);
            this.bnReCalculateTotalAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BnReCalculateTotalAmount_KeyDown);
            // 
            // bnCreateImportFile
            // 
            this.bnCreateImportFile.Location = new System.Drawing.Point(182, 261);
            this.bnCreateImportFile.Name = "bnCreateImportFile";
            this.bnCreateImportFile.Size = new System.Drawing.Size(116, 23);
            this.bnCreateImportFile.TabIndex = 18;
            this.bnCreateImportFile.Text = "10. Tạo file import";
            this.bnCreateImportFile.UseVisualStyleBackColor = false;
            this.bnCreateImportFile.Click += new System.EventHandler(this.BnCreateImportFile_Click);
            this.bnCreateImportFile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BnCreateImportFile_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 241);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "5.1. Số tiền";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 344);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "9. Số HĐ";
            // 
            // bnNew
            // 
            this.bnNew.Location = new System.Drawing.Point(182, 313);
            this.bnNew.Name = "bnNew";
            this.bnNew.Size = new System.Drawing.Size(116, 23);
            this.bnNew.TabIndex = 20;
            this.bnNew.Text = "12. Phiếu mới";
            this.bnNew.UseVisualStyleBackColor = false;
            this.bnNew.Click += new System.EventHandler(this.BnNew_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Server";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "DataBase";
            // 
            // comboBoxServerList
            // 
            this.comboBoxServerList.FormattingEnabled = true;
            this.comboBoxServerList.Location = new System.Drawing.Point(67, 8);
            this.comboBoxServerList.Name = "comboBoxServerList";
            this.comboBoxServerList.Size = new System.Drawing.Size(128, 21);
            this.comboBoxServerList.TabIndex = 1;
            this.comboBoxServerList.SelectedIndexChanged += new System.EventHandler(this.comboBoxServerList_SelectedIndexChanged);
            // 
            // comboBoxDataBase
            // 
            this.comboBoxDataBase.FormattingEnabled = true;
            this.comboBoxDataBase.Location = new System.Drawing.Point(67, 30);
            this.comboBoxDataBase.Name = "comboBoxDataBase";
            this.comboBoxDataBase.Size = new System.Drawing.Size(128, 21);
            this.comboBoxDataBase.TabIndex = 4;
            this.comboBoxDataBase.DropDown += new System.EventHandler(this.comboBoxDataBase_DropDown);
            // 
            // textBoxIP
            // 
            this.textBoxIP.Location = new System.Drawing.Point(201, 8);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(110, 20);
            this.textBoxIP.TabIndex = 2;
            // 
            // ComboboxImportOrExport
            // 
            this.ComboboxImportOrExport.FormattingEnabled = true;
            this.ComboboxImportOrExport.Location = new System.Drawing.Point(201, 30);
            this.ComboboxImportOrExport.Name = "ComboboxImportOrExport";
            this.ComboboxImportOrExport.Size = new System.Drawing.Size(110, 21);
            this.ComboboxImportOrExport.TabIndex = 5;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(3, 79);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(312, 445);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBoxSoHoaDon);
            this.tabPage1.Controls.Add(this.textBoxKyHieuHoaDon);
            this.tabPage1.Controls.Add(this.textBoxSoTien);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.numericUpDownNumberOfCopies);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.gridLookUpEditTenNCC);
            this.tabPage1.Controls.Add(this.button_FlauUI_AutoImport);
            this.tabPage1.Controls.Add(this.DTPNgayHoaDon);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.ButtonPopupStockInfo);
            this.tabPage1.Controls.Add(this.ButtonReportImportProvider);
            this.tabPage1.Controls.Add(this.Lview1);
            this.tabPage1.Controls.Add(this.TextBoxSearch);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.bnSearch);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.bnGetSelectedTrans);
            this.tabPage1.Controls.Add(this.bnSelectRows);
            this.tabPage1.Controls.Add(this.bnNew);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.bnReCalculateTotalAmount);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.bnCreateImportFile);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(304, 419);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Nhập hàng";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBoxSoHoaDon
            // 
            this.textBoxSoHoaDon.Location = new System.Drawing.Point(79, 341);
            this.textBoxSoHoaDon.Name = "textBoxSoHoaDon";
            this.textBoxSoHoaDon.Size = new System.Drawing.Size(97, 20);
            this.textBoxSoHoaDon.TabIndex = 16;
            this.textBoxSoHoaDon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxSoHoaDon_KeyDown);
            // 
            // textBoxKyHieuHoaDon
            // 
            this.textBoxKyHieuHoaDon.Location = new System.Drawing.Point(79, 315);
            this.textBoxKyHieuHoaDon.Name = "textBoxKyHieuHoaDon";
            this.textBoxKyHieuHoaDon.Size = new System.Drawing.Size(97, 20);
            this.textBoxKyHieuHoaDon.TabIndex = 14;
            // 
            // textBoxSoTien
            // 
            this.textBoxSoTien.Location = new System.Drawing.Point(79, 237);
            this.textBoxSoTien.Name = "textBoxSoTien";
            this.textBoxSoTien.Size = new System.Drawing.Size(97, 20);
            this.textBoxSoTien.TabIndex = 8;
            this.textBoxSoTien.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxSoTien_KeyDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 368);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Số bản in";
            // 
            // numericUpDownNumberOfCopies
            // 
            this.numericUpDownNumberOfCopies.Location = new System.Drawing.Point(79, 366);
            this.numericUpDownNumberOfCopies.Name = "numericUpDownNumberOfCopies";
            this.numericUpDownNumberOfCopies.Size = new System.Drawing.Size(97, 20);
            this.numericUpDownNumberOfCopies.TabIndex = 23;
            this.numericUpDownNumberOfCopies.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "1.";
            // 
            // gridLookUpEditTenNCC
            // 
            this.gridLookUpEditTenNCC.Location = new System.Drawing.Point(79, 263);
            this.gridLookUpEditTenNCC.Name = "gridLookUpEditTenNCC";
            this.gridLookUpEditTenNCC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditTenNCC.Properties.PopupView = this.gridLookUpEditTenNCCView;
            this.gridLookUpEditTenNCC.Properties.SearchMode = DevExpress.XtraEditors.Repository.GridLookUpSearchMode.AutoSearch;
            this.gridLookUpEditTenNCC.Size = new System.Drawing.Size(97, 20);
            this.gridLookUpEditTenNCC.TabIndex = 10;
            this.gridLookUpEditTenNCC.Popup += new System.EventHandler(this.gridLookUpEditTenNCC_Popup);
            this.gridLookUpEditTenNCC.EditValueChanged += new System.EventHandler(this.gridLookUpEditTenNCC_EditValueChanged);
            this.gridLookUpEditTenNCC.Click += new System.EventHandler(this.gridLookUpEditTenNCC_Click);
            this.gridLookUpEditTenNCC.Enter += new System.EventHandler(this.gridLookUpEditTenNCC_Enter);
            // 
            // gridLookUpEditTenNCCView
            // 
            this.gridLookUpEditTenNCCView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEditTenNCCView.Name = "gridLookUpEditTenNCCView";
            this.gridLookUpEditTenNCCView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEditTenNCCView.OptionsView.ShowGroupPanel = false;
            // 
            // button_FlauUI_AutoImport
            // 
            this.button_FlauUI_AutoImport.Location = new System.Drawing.Point(182, 287);
            this.button_FlauUI_AutoImport.Name = "button_FlauUI_AutoImport";
            this.button_FlauUI_AutoImport.Size = new System.Drawing.Size(116, 23);
            this.button_FlauUI_AutoImport.TabIndex = 19;
            this.button_FlauUI_AutoImport.Text = "11. Import tự động";
            this.button_FlauUI_AutoImport.UseVisualStyleBackColor = true;
            this.button_FlauUI_AutoImport.Click += new System.EventHandler(this.button_AutoImportNhapHoaDon_Click);
            // 
            // DTPNgayHoaDon
            // 
            this.DTPNgayHoaDon.CustomFormat = "dd/MM/yyyy";
            this.DTPNgayHoaDon.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPNgayHoaDon.Location = new System.Drawing.Point(79, 288);
            this.DTPNgayHoaDon.Name = "DTPNgayHoaDon";
            this.DTPNgayHoaDon.Size = new System.Drawing.Size(97, 20);
            this.DTPNgayHoaDon.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 318);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "8. Ký hiệu";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 266);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "6. NCC";
            // 
            // ButtonPopupStockInfo
            // 
            this.ButtonPopupStockInfo.Location = new System.Drawing.Point(182, 339);
            this.ButtonPopupStockInfo.Name = "ButtonPopupStockInfo";
            this.ButtonPopupStockInfo.Size = new System.Drawing.Size(116, 23);
            this.ButtonPopupStockInfo.TabIndex = 21;
            this.ButtonPopupStockInfo.Text = "Thông tin vật tư";
            this.ButtonPopupStockInfo.UseVisualStyleBackColor = true;
            this.ButtonPopupStockInfo.Click += new System.EventHandler(this.ButtonPopupStockInfo_Click);
            // 
            // ButtonReportImportProvider
            // 
            this.ButtonReportImportProvider.Location = new System.Drawing.Point(194, 385);
            this.ButtonReportImportProvider.Name = "ButtonReportImportProvider";
            this.ButtonReportImportProvider.Size = new System.Drawing.Size(104, 23);
            this.ButtonReportImportProvider.TabIndex = 24;
            this.ButtonReportImportProvider.Text = "Offline";
            this.ButtonReportImportProvider.UseVisualStyleBackColor = true;
            this.ButtonReportImportProvider.Visible = false;
            this.ButtonReportImportProvider.Click += new System.EventHandler(this.ButtonReportImportProvider_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 292);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "7. Ngày HĐ";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridControl1);
            this.tabPage2.Controls.Add(this.buttonViewReport);
            this.tabPage2.Controls.Add(this.buttonLoadCustomersToDataTable);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(304, 419);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Công nợ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(8, 36);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(290, 377);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 25;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            // 
            // buttonViewReport
            // 
            this.buttonViewReport.Location = new System.Drawing.Point(205, 7);
            this.buttonViewReport.Name = "buttonViewReport";
            this.buttonViewReport.Size = new System.Drawing.Size(93, 23);
            this.buttonViewReport.TabIndex = 3;
            this.buttonViewReport.Text = "Xem báo cáo";
            this.buttonViewReport.UseVisualStyleBackColor = true;
            this.buttonViewReport.Click += new System.EventHandler(this.buttonViewReport_Click);
            // 
            // buttonLoadCustomersToDataTable
            // 
            this.buttonLoadCustomersToDataTable.Location = new System.Drawing.Point(8, 7);
            this.buttonLoadCustomersToDataTable.Name = "buttonLoadCustomersToDataTable";
            this.buttonLoadCustomersToDataTable.Size = new System.Drawing.Size(140, 23);
            this.buttonLoadCustomersToDataTable.TabIndex = 0;
            this.buttonLoadCustomersToDataTable.Text = "Tải dữ liệu nhà cung cấp";
            this.buttonLoadCustomersToDataTable.UseVisualStyleBackColor = true;
            this.buttonLoadCustomersToDataTable.Click += new System.EventHandler(this.buttonLoadCustomersToDataTable_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.comboBoxSheetName1);
            this.tabPage3.Controls.Add(this.ButtonTimKiemBill);
            this.tabPage3.Controls.Add(this.TextBoxMaGD);
            this.tabPage3.Controls.Add(this.Button1_ChuanBiDuLieu);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(304, 419);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Xuất bill";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // comboBoxSheetName1
            // 
            this.comboBoxSheetName1.FormattingEnabled = true;
            this.comboBoxSheetName1.Location = new System.Drawing.Point(6, 45);
            this.comboBoxSheetName1.Name = "comboBoxSheetName1";
            this.comboBoxSheetName1.Size = new System.Drawing.Size(125, 21);
            this.comboBoxSheetName1.TabIndex = 3;
            // 
            // ButtonTimKiemBill
            // 
            this.ButtonTimKiemBill.Location = new System.Drawing.Point(236, 79);
            this.ButtonTimKiemBill.Name = "ButtonTimKiemBill";
            this.ButtonTimKiemBill.Size = new System.Drawing.Size(62, 23);
            this.ButtonTimKiemBill.TabIndex = 2;
            this.ButtonTimKiemBill.Text = "Tìm kiếm";
            this.ButtonTimKiemBill.UseVisualStyleBackColor = true;
            this.ButtonTimKiemBill.Click += new System.EventHandler(this.ButtonTimKiemBill_Click);
            // 
            // TextBoxMaGD
            // 
            this.TextBoxMaGD.Location = new System.Drawing.Point(6, 81);
            this.TextBoxMaGD.Name = "TextBoxMaGD";
            this.TextBoxMaGD.Size = new System.Drawing.Size(224, 20);
            this.TextBoxMaGD.TabIndex = 1;
            // 
            // Button1_ChuanBiDuLieu
            // 
            this.Button1_ChuanBiDuLieu.Location = new System.Drawing.Point(6, 6);
            this.Button1_ChuanBiDuLieu.Name = "Button1_ChuanBiDuLieu";
            this.Button1_ChuanBiDuLieu.Size = new System.Drawing.Size(125, 23);
            this.Button1_ChuanBiDuLieu.TabIndex = 0;
            this.Button1_ChuanBiDuLieu.Text = "1. Chuẩn bị dữ liệu";
            this.Button1_ChuanBiDuLieu.UseVisualStyleBackColor = true;
            this.Button1_ChuanBiDuLieu.Click += new System.EventHandler(this.Button1_ChuanBiDuLieu_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.gridLookUpEditSearch);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(304, 419);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Nhập hàng 2";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // gridLookUpEditSearch
            // 
            this.gridLookUpEditSearch.EditValue = "m";
            this.gridLookUpEditSearch.Location = new System.Drawing.Point(6, 6);
            this.gridLookUpEditSearch.Name = "gridLookUpEditSearch";
            this.gridLookUpEditSearch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSearch.Properties.PopupView = this.gridLookUpEdit1View;
            this.gridLookUpEditSearch.Size = new System.Drawing.Size(211, 20);
            this.gridLookUpEditSearch.TabIndex = 0;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(67, 56);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(85, 20);
            this.dtpFromDate.TabIndex = 7;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(221, 56);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(90, 20);
            this.dtpToDate.TabIndex = 9;
            // 
            // TaskPane_NhapHoaDon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.ComboboxImportOrExport);
            this.Controls.Add(this.textBoxIP);
            this.Controls.Add(this.comboBoxDataBase);
            this.Controls.Add(this.comboBoxServerList);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "TaskPane_NhapHoaDon";
            this.Size = new System.Drawing.Size(318, 534);
            this.Load += new System.EventHandler(this.ControlimportInvoice_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumberOfCopies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTenNCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTenNCCView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxSearch;
        private System.Windows.Forms.Button bnSearch;
        private System.Windows.Forms.ListView Lview1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button bnGetSelectedTrans;
        private System.Windows.Forms.Button bnSelectRows;
        private System.Windows.Forms.Button bnReCalculateTotalAmount;
        private System.Windows.Forms.Button bnCreateImportFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bnNew;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxServerList;
        private System.Windows.Forms.ComboBox comboBoxDataBase;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.ComboBox ComboboxImportOrExport;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonLoadCustomersToDataTable;
        private System.Windows.Forms.Button buttonViewReport;
        private System.Windows.Forms.Button ButtonReportImportProvider;
        private System.Windows.Forms.Button ButtonPopupStockInfo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private CustomDateTimePicker DTPNgayHoaDon;
        private CustomDateTimePicker dtpFromDate;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button Button1_ChuanBiDuLieu;
        private CustomTextBox TextBoxMaGD;
        private System.Windows.Forms.Button ButtonTimKiemBill;
        private MyUserControl.ComboBoxSheetName comboBoxSheetName1;
        private CustomDateTimePicker dtpToDate;
        private System.Windows.Forms.Button button_FlauUI_AutoImport;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditTenNCC;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEditTenNCCView;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDownNumberOfCopies;
        private CustomTextBox textBoxSoTien;
        private CustomTextBox textBoxKyHieuHoaDon;
        private CustomTextBox textBoxSoHoaDon;
        private System.Windows.Forms.TabPage tabPage4;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSearch;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
    }
}
