﻿using System;
using System.Windows.Forms;

namespace ExcelAddIn1
{
    public partial class CustomTextBox : TextBox
    {
        public CustomTextBox()
        {
            InitializeComponent();
        }

        public bool SelectAllOnClick { get; set; } = false;

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);

            if (SelectAllOnClick)
            {
                SelectAll();
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            //SelectAll();
        }

        



    }
}
