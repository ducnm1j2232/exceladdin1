﻿using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace ExcelAddIn1.MyUserControl
{
    public partial class CustomListView : ListView
    {
        ListViewItemComparer _lvwItemComparer = new ListViewItemComparer();
        

        public CustomListView()
        {
            InitializeComponent();

            View = View.Details;
            FullRowSelect = true;
            AllowColumnReorder = true;
            GridLines = true;

            //The ListViewItemSorter property allows you to specify the
            // object that performs the sorting of items in the ListView.
            // You can use the ListViewItemSorter property in combination
            // with the Sort method to perform custom sorting.
            base.ListViewItemSorter = _lvwItemComparer;

            //base.ListViewItemSorter = new Sorter();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }


        //This class is an implementation of the 'IComparer' interface.
        public class ListViewItemComparer : IComparer
        {
            // Specifies the column to be sorted
            private int ColumnToSort;

            // Specifies the order in which to sort (i.e. 'Ascending').
            private SortOrder OrderOfSort;

            // Case insensitive comparer object
            private System.Collections.CaseInsensitiveComparer ObjectCompare;

            // Class constructor, initializes various elements
            public ListViewItemComparer()
            {
                // Initialize the column to '0'
                ColumnToSort = 0;

                // Initialize the sort order to 'none'
                OrderOfSort = SortOrder.None;

                // Initialize the CaseInsensitiveComparer object
                ObjectCompare = new System.Collections.CaseInsensitiveComparer();
            }

            // This method is inherited from the IComparer interface.
            // It compares the two objects passed using a case
            // insensitive comparison.
            //
            // x: First object to be compared
            // y: Second object to be compared
            //
            // The result of the comparison. "0" if equal,
            // negative if 'x' is less than 'y' and
            // positive if 'x' is greater than 'y'
            public int Compare(object x, object y)
            {
                int compareResult;
                ListViewItem listviewX, listviewY;

                // Cast the objects to be compared to ListViewItem objects
                listviewX = (ListViewItem)x;
                listviewY = (ListViewItem)y;

                // Case insensitive Compare
                compareResult = ObjectCompare.Compare(
                    listviewX.SubItems[ColumnToSort].Text,
                    listviewY.SubItems[ColumnToSort].Text
                );

                // Calculate correct return value based on object comparison
                if (OrderOfSort == SortOrder.Ascending)
                {
                    // Ascending sort is selected, return normal result of compare operation
                    return compareResult;
                }
                else if (OrderOfSort == SortOrder.Descending)
                {
                    // Descending sort is selected, return negative result of compare operation
                    return (-compareResult);
                }
                else
                {
                    // Return '0' to indicate they are equal
                    return 0;
                }
            }

            // Gets or sets the number of the column to which to
            // apply the sorting operation (Defaults to '0').
            public int SortColumn
            {
                set
                {
                    ColumnToSort = value;
                }
                get
                {
                    return ColumnToSort;
                }
            }

            // Gets or sets the order of sorting to apply
            // (for example, 'Ascending' or 'Descending').
            public SortOrder Order
            {
                set
                {
                    OrderOfSort = value;
                }
                get
                {
                    return OrderOfSort;
                }
            }
        }



        private void AddDataSource(DataTable dataTable)
        {
            Columns.Clear();
            Items.Clear();

            foreach (DataColumn dataColumn in dataTable.Columns)
            {
                Columns.Add(dataColumn.ColumnName);
            }

            for (int row = 0; row < dataTable.Rows.Count; row++)
            {
                ListViewItem listViewItem = new ListViewItem(dataTable.Rows[row][0].ToString());
                for (int col = 1; col < dataTable.Columns.Count; col++)
                {
                    listViewItem.SubItems.Add(dataTable.Rows[row][col].ToString());
                }
                Items.Add(listViewItem);
            }
        }


        //class Sorter : System.Collections.IComparer
        //{
        //    public int Column = 0;
        //    public System.Windows.Forms.SortOrder Order = SortOrder.Ascending;
        //    public int Compare(object x, object y) // IComparer Member
        //    {
        //        if (!(x is ListViewItem))
        //            return (0);
        //        if (!(y is ListViewItem))
        //            return (0);

        //        ListViewItem l1 = (ListViewItem)x;
        //        ListViewItem l2 = (ListViewItem)y;

        //        if (l1.ListView.Columns[Column].Tag == null)
        //        {
        //            l1.ListView.Columns[Column].Tag = "Text";
        //        }

        //        if (l1.ListView.Columns[Column].Tag.ToString() == "Numeric")
        //        {
        //            float fl1 = float.Parse(l1.SubItems[Column].Text);
        //            float fl2 = float.Parse(l2.SubItems[Column].Text);

        //            if (Order == SortOrder.Ascending)
        //            {
        //                return fl1.CompareTo(fl2);
        //            }
        //            else
        //            {
        //                return fl2.CompareTo(fl1);
        //            }
        //        }
        //        else
        //        {
        //            string str1 = l1.SubItems[Column].Text;
        //            string str2 = l2.SubItems[Column].Text;

        //            if (Order == SortOrder.Ascending)
        //            {
        //                return str1.CompareTo(str2);
        //            }
        //            else
        //            {
        //                return str2.CompareTo(str1);
        //            }
        //        }
        //    }
        //}


        protected override void OnColumnClick(ColumnClickEventArgs e)
        {
            base.OnColumnClick(e);

            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == _lvwItemComparer.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (_lvwItemComparer.Order == SortOrder.Ascending)
                {
                    _lvwItemComparer.Order = SortOrder.Descending;
                }
                else
                {
                    _lvwItemComparer.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                _lvwItemComparer.SortColumn = e.Column;
                _lvwItemComparer.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            base.Sort();


            //Sorter s = (Sorter)base.ListViewItemSorter;
            //s.Column = e.Column;

            //if (s.Order == System.Windows.Forms.SortOrder.Ascending)
            //{
            //    s.Order = System.Windows.Forms.SortOrder.Descending;
            //}
            //else
            //{
            //    s.Order = System.Windows.Forms.SortOrder.Ascending;
            //}
            //base.Sort();
        }

    }
}
