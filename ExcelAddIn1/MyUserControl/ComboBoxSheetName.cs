﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;

namespace ExcelAddIn1.MyUserControl
{
    public partial class ComboBoxSheetName : ComboBox
    {
        public ComboBoxSheetName()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        protected override void OnDropDown(EventArgs e)
        {
            base.OnDropDown(e);
            
            //add item
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;           
            base.SelectedIndex = -1;
            base.Items.Clear();
            base.BeginUpdate();
            foreach (Worksheet sheet in wb.Worksheets)
            {
                base.Items.Add(sheet.Name);
            }
            base.EndUpdate();

            //autosize font
            //ComboBox senderComboBox = (ComboBox)sender;
            int width = base.DropDownWidth;
            Graphics g = base.CreateGraphics();
            System.Drawing.Font font = base.Font;
            int vertScrollBarWidth =
                (base.Items.Count > base.MaxDropDownItems)
                ? SystemInformation.VerticalScrollBarWidth : 0;

            int newWidth;
            foreach (string s in base.Items)
            {
                newWidth = (int)g.MeasureString(s, font).Width
                    + vertScrollBarWidth;
                if (width < newWidth)
                {
                    width = newWidth;
                }
            }
            base.DropDownWidth = width;



            Cursor.Current = Cursors.Default;
        }

    }
}
