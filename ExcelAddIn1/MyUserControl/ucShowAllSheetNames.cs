﻿using ExcelAddIn1.MyClass;
using Microsoft.Office.Interop.Excel;
using System;
using System.Windows.Forms;
using VSTOLib.Extensions;

namespace ExcelAddIn1.MyUserControl
{
    public partial class UCShowAllSheetNames : UserControl
    {
        public UCShowAllSheetNames()
        {
            InitializeComponent();
        }

        private void listViewAllSheetNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            if (listViewAllSheetNames.SelectedItems.Count == 0) return;
            ListViewItem item = listViewAllSheetNames.SelectedItems[0];
            string sheetName = item.SubItems[0].Text;
            ((Worksheet)wb.Worksheets[sheetName]).Activate();
        }

        public void listViewAllSheetNames_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (listViewAllSheetNames.Visible)
                {
                    Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                    listViewAllSheetNames.Items.Clear();
                    foreach (Worksheet ws in wb.Worksheets)
                    {
                        listViewAllSheetNames.Items.Add(ws.Name);
                    }
                }
            }
            catch { }
        }

        private void ButtonUnhideSheets_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            foreach (Worksheet ws in wb.Worksheets)
            {
                ws.Visible = XlSheetVisibility.xlSheetVisible;
            }
        }

        private void buttonWorksheetToFiles_Click(object sender, EventArgs e)
        {
            try
            {
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                string path = Module1.FolderBrowser("Chọn folder lưu các file Excel");
                if (string.IsNullOrEmpty(path)) return;
                Module1.SpeedUpCode(true);
                foreach (ListViewItem item in listViewAllSheetNames.Items)
                {
                    if (item.Checked)
                    {
                        Worksheet ws = wb.GetSheet(item.Text);
                        ExcelVoid.WorksheetToExcelFile(path, ws);
                    }
                }
                Module1.SpeedUpCode(false);
                Noti.ShowToast($"Chia các sheet thành từng File thành công!\r\nThư mục lưu: {path}");
            }
            catch (Exception ex)
            {
                Module1.SpeedUpCode(false);
                Noti.ShowToast($"{ex.Message}");
            }
        }

        private void buttonCheckAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem listViewItem in listViewAllSheetNames.Items)
            {
                if (!listViewItem.Checked) listViewItem.Checked = true;
            }
        }

        private void buttonRemoveCheckAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem listViewItem in listViewAllSheetNames.Items)
            {
                if (listViewItem.Checked) listViewItem.Checked = false;
            }
        }

        private void ButtonPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                foreach (ListViewItem item in listViewAllSheetNames.Items)
                {
                    if (item.Checked)
                    {
                        Worksheet ws = wb.GetSheet(item.Text);
                        PageSetup pageSetup = ws.PageSetup;
                        pageSetup.Zoom = false;
                        pageSetup.FitToPagesWide = 1;
                        pageSetup.FitToPagesTall = false;
                        ws.PrintOutEx();
                    }
                }

            }
            catch (Exception ex)
            {
                Noti.ShowToast(ex.Message);
                
            }
        }
    }
}
