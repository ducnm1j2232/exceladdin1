﻿using Microsoft.Office.Core;
using System.Windows.Forms;

namespace ExcelAddIn1.MyUserControl
{
    public static class CustomTaskPaneManager
    {


        public static void Add(UserControl userControl, string name, int userControlWidth, MsoCTPDockPosition dockPosition = MsoCTPDockPosition.msoCTPDockPositionRight)
        {
            Microsoft.Office.Tools.CustomTaskPane customTaskPane = Globals.ThisAddIn.CustomTaskPanes.Add(userControl, name);
            customTaskPane.DockPosition = dockPosition;
            customTaskPane.Width = userControlWidth + 40;
            customTaskPane.Visible = true;
        }

    }
}
