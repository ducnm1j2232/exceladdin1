﻿using ExcelAddIn1.MyClass;
using ExcelAddIn1.MyClass.Extensions;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataTable = System.Data.DataTable;
using VSTOLib.Extensions;

namespace ExcelAddIn1
{
    public partial class TaskPane_NhapHoaDon : UserControl
    {
        public int width;

        public TaskPane_NhapHoaDon()
        {
            InitializeComponent();
            width = this.Width;

            LoadDanhMucNhaCungCapVIETSEA();

        }

        public int ActualWidth
        {
            get
            {
                return width;
            }
            set { }
        }

        private async void LoadDanhMucNhaCungCapVIETSEA()
        {
            try
            {
                DataTable dt = await ADO.GetDataTableFromOracleServerAsync(ExcelAddIn1.VIETSEA.TBNETERP.ConnectionString_DATPHAT, "select makhachhang,tentochu from dmkhachhang where manhomkhachang like '%NCC%'");
                if (dt.HasData())
                {
                    gridLookUpEditTenNCC.Properties.SearchMode = DevExpress.XtraEditors.Repository.GridLookUpSearchMode.AutoSearch;
                    gridLookUpEditTenNCC.Enabled = true;
                    gridLookUpEditTenNCC.Properties.DataSource = dt;
                    gridLookUpEditTenNCC.Text = "Nhập tên NCC";
                    gridLookUpEditTenNCC.Properties.DisplayMember = "MAKHACHHANG";
                    gridLookUpEditTenNCC.Properties.ValueMember = "MAKHACHHANG";

                    gridLookUpEditTenNCC.Properties.PopulateViewColumns();
                    gridLookUpEditTenNCCView.Columns["MAKHACHHANG"].Width = 15;
                }
            }
            catch { }
        }

        async Task LoadCustomerIDs()
        {
            string sql = "";
            DataTable table = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString(), sql);
            if (table.HasData())
            {
                gridLookUpEditSearch.Properties.DataSource = table;
                gridLookUpEditSearch.Properties.DisplayMember = "";
            }
        }



        string GetConnectionString()
        {
            string ip = textBoxIP.Text;
            string database = comboBoxDataBase.Text;
            string user = PublicVar.DT_ServerList().Rows[comboBoxServerList.SelectedIndex][2].ToString();
            string pass = PublicVar.DT_ServerList().Rows[comboBoxServerList.SelectedIndex][3].ToString();

            return $"Server={ip};" +
                    $"Database={database};" +
                    $"User Id={user};" +
                    $"Password = {pass}; ";
        }

        private async void BnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                #region MAIN
                Module1.SpeedUpCode(true);
                textBoxSoTien.Clear();
                textBoxKyHieuHoaDon.Clear();
                textBoxSoHoaDon.Clear();

                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet ws = wb.GetNewSheet("Giao dịch");
                SetParameters();

                wb.DeleteSheet("Temp");

                Worksheet shBangMa = await ExcelObject.GetSheetBangMaFromGoogleDriveAsync(wb);

                #region fill data listview
                /*không dùng threading được vì để truy cập vào control listview phải là thread chính tạo ra nó */
                //try
                //{
                using (DataTable dtTransactions = await GetTransactionsAsync())
                {
                    Lview1.SafeInvoke(() => { Lview1.Items.Clear(); });
                    if (dtTransactions.Rows.Count > 0)
                    {
                        for (int row = 0; row < dtTransactions.Rows.Count; row++)
                        {
                            ListViewItem item = new ListViewItem
                            {
                                Text = Module1.ConvertTransdateToDateString(dtTransactions.Rows[row][0].ToString())
                            };
                            item.SubItems.Add(((decimal)dtTransactions.Rows[row][1]).ToString("N0"));
                            item.SubItems.Add(dtTransactions.Rows[row][2].ToString());
                            Lview1.SafeInvoke(() => { Lview1.Items.Add(item); });
                        }
                    }
                }
                //}
                //catch (Exception ex)
                //{
                //    Module1.Speed_up_code(false);
                //    MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace, Module1.errorTitle);
                //    return;
                //}
                #endregion

                #region get transactions to range

                //mới, database cũ bảng transdetail không có cột importID và exportTD

                //try
                //{
                using (DataTable dtTransdetail = await GetTranDetailAsync())
                {
                    if (dtTransdetail.HasData())
                    {

                        dtTransdetail.CopyToRange(ws.Range["A2"], false);
                        ws.Range["C1"].EntireColumn.Insert();

                        ws.Range["a1"].Value2 = "Mã giao dịch";
                        ws.Range["b1"].Value2 = "GoodID";
                        ws.Range["c1"].Value2 = "Mã hàng";
                        ws.Range["d1"].Value2 = "Tên hàng";
                        ws.Range["e1"].Value2 = "Số lượng";
                        ws.Range["f1"].Value2 = "Đơn giá";
                        ws.Range["g1"].Value2 = "Thành tiền";
                        ws.Range["h1"].Value2 = "Thành tiền VAT";
                        ws.Range["i1"].Value2 = "SortOrder";

                        int LR = ws.LastRow("A");
                        Module1.VlookupMAVIETSEA(ws.Range["B2", "B" + LR], ws.Range["C2"], shBangMa);

                        //dòng tổng cộng
                        DataTable dtSubtotal = await GetSubtotalAsync();
                        if (dtSubtotal != null || dtSubtotal.Rows.Count > 0)
                        {
                            Range lastPreviousRange = ws.NewRangeAtColumn("a");
                            dtSubtotal.CopyToRange(ws.NewRangeAtColumn("a"), false);
                            ws.Range[lastPreviousRange, ws.LastRange("a")].EntireRow.Font.Bold = true;
                            Range formatRange = ws.GetRange("e1:h", "a");
                            formatRange.ConvertToValue();
                            formatRange.NumberFormat = "#,##0";
                        }
                        ws.Columns.AutoFit();


                        //dòng tên nhà cung cấp đầu tiên
                        using (DataTable dtCustomerName = await GetCustomerNameAsync())
                        {
                            int newRow = ws.LastRow("A") + 1;
                            dtCustomerName.CopyToRange(ws.Range["A" + newRow], false);
                            ws.Range["A" + newRow, "H" + ws.LastRow("A")].Font.Bold = true;
                        }
                    }
                    else
                    {
                        Module1.SpeedUpCode(false);
                        ws.Activate();
                        ws.Cells.ClearContents();
                        ws.Range["A1"].Select();
                        MsgBox.Show("Không có giao dịch thỏa mãn!");
                        TextBoxSearch.SafeInvoke(() => { TextBoxSearch.SelectAll(); });
                        return;
                    }
                }
                //}
                //catch (Exception ex)
                //{
                //    Module1.Speed_up_code(false);
                //    MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace, Module1.errorTitle);
                //    return;
                //}
                #endregion

                #region change font color to red if cannot find mã hàng
                //change font color to red if cannot find mã hàng
                Range rangeToFind = ws.Range["C1", "C" + ws.LastRow("C")];
                Range NArange = rangeToFind.Find(@"#N/A");
                if (NArange != null)
                {
                    string firstAddress = NArange.Address;
                    while (NArange != null)
                    {
                        string transID = ws.Range["A" + NArange.Row].Text;
                        Lview1.SafeInvoke(() =>
                        {
                            foreach (ListViewItem item in Lview1.Items)
                            {
                                if (item.SubItems[2].Text == transID)
                                {
                                    item.ForeColor = System.Drawing.Color.Red;
                                    break;
                                }
                            }
                        });

                        NArange = rangeToFind.FindNext(NArange);
                        if (NArange != null && NArange.Address == firstAddress) break;
                    }
                }

                textBoxSoTien.SafeInvoke(() => { textBoxSoTien.Clear(); });
                textBoxSoHoaDon.SafeInvoke(() => { textBoxSoHoaDon.Clear(); });
                #endregion


                int endRow = ws.LastRow("A");
                Range sortRange = ws.Range["A1", "I" + endRow];
                sortRange.Sort(Key1: ws.Columns[9], Order1: XlSortOrder.xlAscending, Header: XlYesNoGuess.xlYes);
                sortRange.Sort(Key1: ws.Columns[1], Order1: XlSortOrder.xlAscending, Header: XlYesNoGuess.xlYes);
                ws.Range["I1", "I" + endRow].ClearContents();//clear cột sort order
                ws.InsertBlankRowsAtValueChange("a", 1);
                //set borders
                Range rangeCollection = ws.UsedRange.SpecialCells(XlCellType.xlCellTypeConstants);
                rangeCollection.SetBorder();

                Module1.SpeedUpCode(false);
                Globals.ThisAddIn.Application.Goto(ws.Range["A1"], true);
                #endregion
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
            finally
            {
                Module1.SpeedUpCode(false);
            }


            ////-----------BẢN OFFLINE KHÔNG DÙNG SQL
            //Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            //Worksheet ws = wb.GetWorksheet("Giao dịch", false);
            //wb.GetWorksheet("Temp", false).DeleteWithoutPrompt();
            //ws.Activate();

            //Lview1.Items.Clear();
            //string supplierName = TextBoxSearch.Text;
            //List<Range> ranges = ws.Range["b:b"].FindAllRange(supplierName);
            //if (ranges.Count > 0)
            //{
            //    foreach (Range r in ranges)
            //    {
            //        string date = GetDateString(r.Text);
            //        string totalVAT = GetTotalVAT(r);
            //        string tranID = GetTransactionID(r.Text);
            //        ListViewItem item = new ListViewItem();
            //        item.Text = date;
            //        item.SubItems.Add(totalVAT);
            //        item.SubItems.Add(tranID);
            //        Lview1.Items.Add(item);
            //    }
            //}

            //TboxTotalAmount.SafeInvoke(() => { TboxTotalAmount.Clear(); });
            //TboxInvoiceNumber.SafeInvoke(() => { TboxInvoiceNumber.Clear(); });


        }

        private void BnSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BnSearch_Click(sender, e);
            }
        }

        private void TboxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BnSearch_Click(sender, e);
            }
        }

        private void BnClose_Click(object sender, EventArgs e)
        {
            for (int i = Globals.ThisAddIn.CustomTaskPanes.Count; i > 0; i--)
            {
                try
                {
                    Globals.ThisAddIn.CustomTaskPanes.Remove(Globals.ThisAddIn.CustomTaskPanes[i - 1]);
                }
                catch { }
            }
        }

        private void Lview1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.Worksheets["Giao dịch"];

            if (Lview1.SelectedItems.Count == 0) return;
            ListViewItem item = Lview1.SelectedItems[0];
            string tranID = item.SubItems[2].Text;
            Range findRange = ws.Range["A:A"].Find(What: tranID, LookAt: XlLookAt.xlPart);

            if (findRange != null)
            {
                Globals.ThisAddIn.Application.Goto(findRange, true);
            }
        }

        private void Lview1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (Lview1.SelectedItems.Count == 0)
            {
                label4.Text = "0";
            }
            else
            {
                double amount = 0;
                foreach (ListViewItem item in Lview1.CheckedItems)
                {
                    amount += double.Parse(item.SubItems[1].Text);
                }

                label4.Text = amount.ToString("N0");
            }
        }

        private void TboxSearch_Enter(object sender, EventArgs e)
        {
            TextBoxSearch.SelectAll();
        }

        private void BnGetSelectedTrans_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet transSheet = wb.Worksheets["Giao dịch"];

            Worksheet tempSheet = wb.GetSheet("Temp");
            tempSheet.Move(After: transSheet);

            tempSheet.Range["G1"].Formula = "=SUM(G2:G1000)";
            tempSheet.Range["G1"].Font.Bold = true;
            tempSheet.Range["G1"].NumberFormat = "#,##0";

            Range findRange = transSheet.Range["A1", "A" + transSheet.LastRow("A")];
            foreach (ListViewItem item in Lview1.CheckedItems)
            {
                string tranID = item.SubItems[2].Text;
                Range foundRange = findRange.Find(tranID);
                if (foundRange != null)
                {
                    Range rangeToCopy = transSheet.Range[foundRange.Offset[1, 0], foundRange.Offset[1, 0].Offset[0, 7].End[XlDirection.xlDown].Offset[-1, 0]];
                    rangeToCopy.Copy();
                    tempSheet.Range["A" + (tempSheet.LastRow("A") + 1)].PasteSpecial(XlPasteType.xlPasteColumnWidths);
                    tempSheet.Range["A" + (tempSheet.LastRow("A") + 1)].PasteSpecial(XlPasteType.xlPasteAllExceptBorders);
                    rangeToCopy.EntireRow.Interior.Color = System.Drawing.Color.LightGoldenrodYellow;
                }
            }

            //add data validation
            Validation validation = tempSheet.Range["i2:i1000"].Validation;
            validation.Delete();
            validation.Add(Type: XlDVType.xlValidateList, AlertStyle: XlDVAlertStyle.xlValidAlertWarning, Formula1: "KM");
            validation.InCellDropdown = true;
            validation.IgnoreBlank = true;

            Globals.ThisAddIn.Application.CutCopyMode = 0;
            tempSheet.Activate();
        }

        private void BnSelectRows_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet transSheet = wb.ActiveSheet;
            try
            {
                transSheet = wb.Worksheets["Giao dịch"];
            }
            catch { return; }

            Range selectedRange = Globals.ThisAddIn.Application.Selection;
            if (selectedRange.Worksheet != transSheet) return;

            Globals.ThisAddIn.Application.ScreenUpdating = false;
            Worksheet tempSheet = wb.GetSheet("Temp");
            tempSheet.Move(After: transSheet);

            tempSheet.Range["G1"].Formula = "=SUM(G2:G1000)";
            tempSheet.Range["G1"].Font.Bold = true;
            tempSheet.Range["G1"].NumberFormat = "#,##0";

            selectedRange.EntireRow.Copy();
            tempSheet.Range["A" + tempSheet.NextRowIndexAtColumn("A")].PasteSpecial(XlPasteType.xlPasteColumnWidths);
            tempSheet.Range["A" + tempSheet.NextRowIndexAtColumn("A")].PasteSpecial(XlPasteType.xlPasteAllExceptBorders);
            selectedRange.EntireRow.Interior.Color = System.Drawing.Color.LightGoldenrodYellow;

            //add data validation
            Validation validation = tempSheet.Range["i2:i1000"].Validation;
            validation.Delete();
            validation.Add(XlDVType.xlValidateList, XlDVAlertStyle.xlValidAlertWarning, XlFormatConditionOperator.xlBetween, "KM");
            validation.InCellDropdown = true;
            validation.IgnoreBlank = true;

            transSheet.Activate();
            Globals.ThisAddIn.Application.CutCopyMode = 0;
            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        private void BnReCalculateTotalAmount_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxSoTien.Text)) return;
            if (double.TryParse(textBoxSoTien.Text, out double dumb))
            {
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet tempSheet;
                try { tempSheet = wb.Worksheets["Temp"]; } catch { return; }

                int LR = tempSheet.LastRow("g");

                double giatriThuong = Globals.ThisAddIn.Application.WorksheetFunction.SumIf(tempSheet.Range["i2:i" + LR], "", tempSheet.Range["g2:g" + LR]);
                double giaTriKM = Globals.ThisAddIn.Application.WorksheetFunction.SumIf(tempSheet.Range["i2:i" + LR], "KM", tempSheet.Range["g2:g" + LR]);
                double oldAmount = giatriThuong;
                double newAmount = double.Parse(textBoxSoTien.Text) - giaTriKM;
                double rate = newAmount / oldAmount;

                //chia lại đơn giá và thành tiền
                for (int i = 2; i <= LR; i++)
                {
                    if (tempSheet.Range["i" + i].Value2 != "KM")//nếu không phải là khuyến mại thì mới tính lại giá
                    {
                        tempSheet.Range["g" + i].Value2 = Math.Round(Convert.ToDouble(tempSheet.Range["g" + i].Value2) * rate, 0);//thanh tiền mới làm tròn
                        tempSheet.Range["f" + i].Value2 = Math.Round(Convert.ToDouble(tempSheet.Range["g" + i].Value2) / Convert.ToDouble(tempSheet.Range["e" + i].Value2), 0);//đơn giá mới làm tròn
                        tempSheet.Range["g" + i].Value2 = tempSheet.Range["e" + i].Value2 * tempSheet.Range["f" + i].Value2;//thành tiền mới nhân ngược lại
                    }
                }

                //object[,] data;
                //Range range = tempSheet.Range["E2", "G" + tempSheet.LastRowAtColumn("G")];
                //data = range.Value2;

                //for (int i = data.GetLowerBound(0); i <= data.GetUpperBound(0); i++)
                //{
                //    data[i, 3] = Math.Round((double)data[i, 3] * rate, 0); //thành tiền mới
                //    data[i, 2] = Math.Round((double)data[i, 3] / (double)data[i, 1], 0); //đơn giá mới
                //    data[i, 3] = (double)data[i, 1] * (double)data[i, 2];//thành tiền mới làm tròn
                //}
                //range.Value2 = data;

                //điều chỉnh cho khớp, nguyên tắc là tất cả đều làm tròn, còn lại 1 cái duy nhất thì để lẻ
                double tongTienDatDuoc = Convert.ToDouble(tempSheet.Range["g1"].Value2);
                double tongTienMongMuon = Convert.ToDouble(textBoxSoTien.Text);
                for (int i = 2; i <= LR; i++)
                {
                    if (tempSheet.Range["i" + i].Value2 != "KM")//nếu không phải là khuyến mại thì mới tính lại giá
                    {
                        tempSheet.Range["G" + i].Value2 = Convert.ToDouble(tempSheet.Range["G" + i].Value2) + tongTienMongMuon - tongTienDatDuoc;
                        tempSheet.Range["F" + i].Value2 = Convert.ToDouble(tempSheet.Range["G" + i].Value2) / Convert.ToDouble(tempSheet.Range["E" + i].Value2);
                        break;
                    }
                }
            }
            else { MessageBox.Show("Số tiền không hợp lệ!", Module1.errorTitle); }
        }

        private void TboxTotalAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BnReCalculateTotalAmount_Click(sender, e);
            }
        }

        private void BnReCalculateTotalAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BnReCalculateTotalAmount_Click(sender, e);
            }
        }

        private void BnNew_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (ListViewItem item in Lview1.CheckedItems)
                {
                    item.Checked = false;
                }

                textBoxSoTien.Clear();
                //textBoxKyHieuHoaDon.Clear();
                textBoxSoHoaDon.Clear();
                // textBoxTenNCC.Clear();


                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Globals.ThisAddIn.Application.DisplayAlerts = false;
                ((Worksheet)wb.Worksheets["Temp"]).Columns.Delete();
                Globals.ThisAddIn.Application.DisplayAlerts = true;
                ((Worksheet)wb.Worksheets["Giao dịch"]).Activate();
            }
            catch { }
        }

        private void BnCreateImportFile_Click(object sender, EventArgs e)
        {
            //update data to MLnetData google sheet
            Task.Run(async () =>
           {
               System.Collections.Generic.IList<System.Collections.Generic.IList<object>> newValues = new System.Collections.Generic.List<System.Collections.Generic.IList<object>>();
               List<object> value = new List<object>();
               value.Add(TextBoxSearch.GetText());
               value.Add(gridLookUpEditTenNCC.GetText());
               newValues.Add(value);
               await ExcelAddIn1.GoogleApis.SheetsAPI.AppendValueAsync(ExcelAddIn1.GoogleApis.SheetsAPI.spreadSheetID, "MLnetData!A:B", (IList<IList<object>>)newValues);
               Notification.Toast.Show("Gửi dữ liệu MLnet thành công!");
           });


            bool checkInvoiceExist(string MaNCC, string kyhieuHD, string SoHoaDon)
            {
                bool result = false;
                string query = string.Format(@"Select *
                                From Vattugd
                                Where Maptnx ='NMUA'
                                And Soctugoc = '{0}'
                                And Makhachang = '{1}'
                                and kyhieuhoadongd = '{2}'", SoHoaDon, MaNCC, kyhieuHD);
                System.Data.DataTable dataTable = ADO.GetDataTableFromOracleServerAsync(ExcelAddIn1.VIETSEA.TBNETERP.ConnectionString_DATPHAT, query).Result;
                if (dataTable.HasData())
                {
                    result = true;
                }
                return result;
            }

            string maNCC = gridLookUpEditTenNCC.Text;
            string kyhieu = textBoxKyHieuHoaDon.Text;
            string soHoaDon = kyhieu.Contains("/") ? textBoxSoHoaDon.Text.PadLeft(7, '0') : textBoxSoHoaDon.Text;
            //string sohoadon = textBoxSoHoaDon.Text.PadLeft(7, '0');
            if (checkInvoiceExist(maNCC, kyhieu, soHoaDon))
            {
                MsgBox.Show($"Đã tồn tại hóa đơn {soHoaDon} ký hiệu {kyhieu} của nhà cung cấp {maNCC}");
                return;
            }


            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet temp = wb.GetSheet("Temp");


            try
            {
                if (string.IsNullOrEmpty(textBoxSoHoaDon.Text) == false)
                {
                    Module1.SpeedUpCode(true);
                    Workbook importFile = Globals.ThisAddIn.Application.Workbooks.Add();
                    Worksheet sheet1 = importFile.Worksheets[1];
                    sheet1.Name = "MAUHOADON";
                    sheet1.Range["a1"].SetHeaderText(new string[] { "SOHOADON", "MAVTU", "TENVTU", "TONCUOIKYSL", "DON GIA", "TONCUOIKYGT", "MANCC", "NGAYHD", "KYHIEUHD" });

                    //copy mã hàng, tên, số lượng, đơn giá
                    Range copyRange = temp.Range["C2", "F" + temp.LastRow("C")];
                    copyRange.Copy();
                    sheet1.Range["B2"].PasteSpecial(XlPasteType.xlPasteValues);


                    //số hóa đơn
                    //string soHoaDon = string.Format("{0:0000000}", int.Parse(textBoxSoHoaDon.Text));
                    sheet1.Range["A2"].Resize[copyRange.Rows.Count, 1].NumberFormat = "@";
                    sheet1.Range["A2"].Resize[copyRange.Rows.Count, 1].Value2 = soHoaDon;

                    //Mã ncc
                    string MaNhaCungCap = gridLookUpEditTenNCC.Text;
                    sheet1.GetRange("g2:g", "a").Value2 = MaNhaCungCap;

                    //ngày hóa đơn
                    string ngayHoaDon = DTPNgayHoaDon.Text;
                    sheet1.GetRange("h2:h", "a").Value = "'" + ngayHoaDon;

                    //ký hiệu hóa đơn
                    string kyHieuHoaDon = Convert.ToString(textBoxKyHieuHoaDon.Text);
                    sheet1.GetRange("i2:i", "a").Value2 = kyHieuHoaDon;

                    //show comment
                    foreach (Comment cmt in temp.Comments) { cmt.Delete(); }
                    Comment comment = temp.Range["G1"].AddComment();
                    comment.Text($"Xuất file import thành công" + Environment.NewLine + $"Số hóa đơn: {soHoaDon}");
                    comment.Visible = true;
                    comment.Shape.TextFrame.AutoSize = true;

                    //save
                    string saveFolder = PublicVar.ImportNhapHangSaveLocation();
                    importFile.SaveOverWrite(saveFolder + @"\FileImportNhapHang", 56);
                    importFile.Close();

                    Module1.SpeedUpCode(false);
                }
                else
                {
                    MessageBox.Show("Chưa có số tiền");
                    textBoxSoHoaDon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Module1.SpeedUpCode(false);
            }
            finally
            {
                Module1.SpeedUpCode(false);
                temp.Activate();
                temp.Range["G1"].Select();
            }

        }

        private void TboxInvoiceNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BnCreateImportFile_Click(sender, e);
            }
        }

        private void BnCreateImportFile_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BnCreateImportFile_Click(sender, e);
            }
        }

        private void ControlimportInvoice_Load(object sender, EventArgs e)
        {
            comboBoxServerList.DataSource = PublicVar.DT_ServerList().DefaultView;
            comboBoxServerList.DisplayMember = "Name";


            ComboboxImportOrExport.DataSource = PublicVar.DT_ImportOrExportTranCode().DefaultView;
            ComboboxImportOrExport.DisplayMember = "Loại giao dịch";

        }

        private void comboBoxServerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                textBoxIP.Text = PublicVar.DT_ServerList().Rows[comboBoxServerList.SelectedIndex][1].ToString();
                comboBoxDataBase.Text = string.Empty;
            }
            catch { }

        }

        private void comboBoxDataBase_DropDown(object sender, EventArgs e)
        {
            string ip = textBoxIP.Text;
            string user = PublicVar.DT_ServerList().Rows[comboBoxServerList.SelectedIndex][2].ToString();
            string pass = PublicVar.DT_ServerList().Rows[comboBoxServerList.SelectedIndex][3].ToString();
            ADO.AddDataBaseNameToComboBox(textBoxIP.Text, user, pass, comboBoxDataBase);
        }


        private void buttonLoadCustomersToDataTable_Click(object sender, EventArgs e)
        {
            DataTable DT_NhaCungCap = ADO.GetDataTableFromSQLServer(GetConnectionString(), "select CustomerID, CustomerName from Customers");

            gridControl1.DataSource = DT_NhaCungCap;
            gridView1.Columns[1].Width = gridView1.Columns[1].GetBestWidth();
            gridView1.Columns[0].Visible = false;


        }



        private void GetReportByOneAccount()
        {

            List<string> supplyIDList = new List<string>();//chọn nhiều nhà cùng lúc
            foreach (var item in gridView1.GetSelectedRows())
            {
                supplyIDList.Add(gridView1.GetRowCellValue(item, "CustomerID").ToString());
            }
            string supplyIDs = string.Join(",", supplyIDList);

            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet reportSheet = wb.GetSheet("TK331 bội bộ");

            List<SqlParameter> parameters = new List<SqlParameter>
                    {
                        new SqlParameter("@FromDate",ADO.ConvertDateToTransDate( dtpFromDate.Value)),
                        new SqlParameter("@EndDate",ADO.ConvertDateToTransDate( dtpToDate.Value)),
                        new SqlParameter("@AccountNumber","331"),
                        new SqlParameter("@SupplyID", supplyIDs)
                    };
            DataTable dt = ADO.GetDataTableFromStoredProcedure(GetConnectionString(), "[dbo].[Accounting.Report.DetailByOneAccount]", parameters);
            string[] selectedColumns = new string[] { "Transdate", "Description", "AppAccount", "DebitValue", "CreditValue", "SurplusDebit", "SurplusCredit" };
            DataTable data = new DataView(dt).ToTable(false, selectedColumns);

            if (data == null || data.Rows.Count == 0)
            {
                Notification.Toast.Show("Không có dữ liệu");
                return;
            }
            //tính toán số liệu
            double DebitStartValue = Convert.ToDouble(dt.Compute("AVG(DebitStartValue)", ""));
            double CreditStartValue = Convert.ToDouble(dt.Compute("AVG(CreditStartValue)", ""));
            double DebitEndValue = Convert.ToDouble(dt.Compute("AVG(DebitEndValue)", ""));
            double CreditEndValue = Convert.ToDouble(dt.Compute("AVG(CreditEndValue)", ""));

            double SumDebitValue = Convert.ToDouble(dt.Compute("SUM(DebitValue)", ""));
            double SumCrebitValue = Convert.ToDouble(dt.Compute("SUM(CreditValue)", ""));

            //ghi dữ liệu ra sheet
            ADO.CopyDataTableToRange(data, reportSheet.Range["a3"], false);
            reportSheet.Range["a1", "g1"].Value2 = new[] { "Ngày tháng", "Diễn giải", "TK đối ứng", "PS Nợ", "PS Có", "Dư Nợ", "Dự Có" };

            int LR = reportSheet.LastRow("a");
            reportSheet.Range["f2", "g2"].Value2 = new[] { DebitStartValue, CreditStartValue };//số dư dầu kỳ
            reportSheet.Range["d" + (LR + 1), "e" + (LR + 1)].Value2 = new[] { SumDebitValue, SumCrebitValue };//tổng phát sinh
            reportSheet.Range["f" + (LR + 2), "g" + (LR + 2)].Value2 = new[] { DebitEndValue, CreditEndValue };//tổng phát sinh

            reportSheet.Range["a2"].EntireRow.Font.Bold = true;
            reportSheet.Range["a" + (LR + 1), "a" + (LR + 2)].EntireRow.Font.Bold = true;
            reportSheet.Range["d1", "g" + (LR + 2)].Value2 = reportSheet.Range["d1", "g" + (LR + 2)].Value2;
            reportSheet.Range["d1", "g" + (LR + 2)].NumberFormat = @"#,##";
            reportSheet.UsedRange.Borders.LineStyle = XlLineStyle.xlContinuous;
            reportSheet.Columns.AutoFit();
            reportSheet.Range["b1"].EntireColumn.ColumnWidth = 55;
            reportSheet.Range["d1", "g1"].EntireColumn.ColumnWidth = 13;
            reportSheet.Rows.RowHeight = 20;

            Globals.ThisAddIn.Application.Goto(reportSheet.Range["a1"], true);
        }

        private void buttonViewReport_Click(object sender, EventArgs e)
        {
            GetReportByOneAccount();
        }


        private async void ButtonReportImportProvider_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("frmRptImportProvider");
            ws.Name = "Giao dịch";
            if (ws.Range["a1"].Value2 == "Mã giao dịch") return;

            Worksheet sheetBangMa = await ExcelObject.GetSheetBangMaFromGoogleDriveAsync(wb);

            ws.Range["A" + ws.NextRowIndexAtColumn("n"), "A" + ws.Rows.Count].EntireRow.Delete();
            ws.Range["a1:a13"].EntireRow.Delete();
            ws.Range["a1:n" + ws.LastRow("n")].ClearAllBlankRowBlankColumnAndFormating();
            ws.Range["d:g"].MinimumRange().ConvertToValue();

            ws.Range["e1"].EntireColumn.Insert();
            int LR = ws.LastRow("h"); //cột cuối là H
            ws.Range["g3"].Formula = @"=IF(H3/1.1=0,"""",H3/1.1)";
            ws.Range["g3:g" + LR].FillDownFormula();
            ws.Range["e3"].Formula = @"=IF(F3/1.1=0,"""",F3/1.1)";
            ws.Range["e3:e" + LR].FillDownFormula();

            ws.Range["f1"].EntireColumn.Delete();
            ws.Range["c1"].EntireColumn.Delete();
            ws.Range["b1"].EntireColumn.Insert();
            ws.Range["a1"].EntireColumn.Insert();

            ws.Range["a2"].Formula = @"=IF(ISNUMBER(SEARCH(""Giao"",B2)),B2,"""")";
            ws.Range["a2:a" + LR].FillDownFormula();

            (ws.Range["e1:h1"].EntireColumn).MinimumRange().NumberFormat = "#,##0";
            ws.Range["a1"].SetHeaderText(new string[] { "Mã giao dịch", "GoodID", "Mã hàng", "Tên hàng", "Số lượng", "Đơn giá", "Thành tiền", "Thành tiền VAT" });

            Module1.VlookupMAVIETSEA(ws.Range["b3:b" + ws.LastRow("b")], ws.Range["c3"], sheetBangMa);

            //sau khi vlookup cột C sẽ có N/A do lookupvalue cột B là rỗng và "giao dịch"
            Range filterRange = ws.Range["a1:h" + LR];
            filterRange.AutoFilter(2, "=");
            ws.Range["a2:f" + LR].SpecialCells(XlCellType.xlCellTypeVisible).ClearContents();
            ws.Range["c2:h" + LR].SpecialCells(XlCellType.xlCellTypeVisible).EntireRow.Font.Bold = true;
            ws.ShowAllData();
            filterRange.AutoFilter(2, "Giao*");
            ws.Range["c2:h" + LR].SpecialCells(XlCellType.xlCellTypeVisible).EntireRow.Font.Bold = true;
            ws.Range["c2:h" + LR].SpecialCells(XlCellType.xlCellTypeVisible).ClearContents();
            ws.ShowAllData();
            ws.AutoFilterMode = false;

            ws.Cells.SpecialCells(XlCellType.xlCellTypeConstants).Borders.LineStyle = XlLineStyle.xlContinuous;
            ws.Rows.RowHeight = 15;
            ws.Range["c:h"].Columns.AutoFit();
            ws.Range["d1"].EntireColumn.ColumnWidth = 50;

            Globals.ThisAddIn.Application.Goto(ws.Range["a1"], true);
            Globals.ThisAddIn.Application.ActiveWindow.DisplayGridlines = true;

            Notification.Toast.Show($"Hoàn thành!\r\nThời gian: {stopwatch.Elapsed.TotalSeconds:N2}");
        }

        private async void ButtonPopupStockInfo_Click(object sender, EventArgs e)
        {

            try
            {
                Range range = Module1.InputBoxRange("Chọn vùng dữ liệu chứa mã hàng");
                if (range == null) return;

                List<string> list = range.ValueToList(true);
                string sqlWhereIn = list.ToSQLWhereIn();

                string query = @"Select
    Goods.GoodID,
    Goods.ShortName,
    Goods.SupplierID,
    Customers.CustomerName,
    Units.UnitName,
    Goods.ConvertUnit,
    Goods.LastImpPriceVat,
    Goods.ExpRetailPriceVat,
    Goods.Status
From
    Goods Inner Join
    Units On Goods.UnitID = Units.UnitID Inner Join
    Customers On Goods.SupplierID = Customers.CustomerID
Where
    Goods.GoodID In(" + sqlWhereIn + ")";

                DataTable dataTable = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString(), query);
                if (dataTable == null || dataTable.Rows.Count == 0)
                {
                    Notification.Toast.Show("Không có dữ liệu");
                    return;
                }

                List<UserClass.Good> goods = new List<UserClass.Good>();
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    UserClass.Good good = new UserClass.Good();

                    good.GoodID = dataRow["GoodID"].ToString();
                    good.ShortName = dataRow["ShortName"].ToString();
                    good.SupplierID = dataRow["SupplierID"].ToString();
                    good.CustomerName = dataRow["CustomerName"].ToString();
                    good.UnitName = dataRow["UnitName"].ToString();
                    good.ConvertUnit = int.Parse(dataRow["ConvertUnit"].ToString());
                    good.LastImpPriceVat = dataRow["LastImpPriceVat"].ToString();
                    good.ExpRetailPriceVat = dataRow["ExpRetailPriceVat"].ToString();
                    good.Status = dataRow["Status"].ToString();

                    goods.Add(good);
                }


                foreach (Range r in range)
                {
                    Comment cmt = r.Comment;
                    if (cmt != null) cmt.Delete();

                    //DataRow row = dataTable.Select("GoodID = "+r.Value);
                    var g = goods.Find(x => x.GoodID == r.Value);
                    if (g == null) break;

                    string commentText = $"Mã vật tư: {g.GoodID}" + Environment.NewLine
                                        + $"Tên vật tư: {g.ShortName}" + Environment.NewLine
                                        + $"Mã NCC: {g.SupplierID}" + Environment.NewLine
                                        + $"Tên NCC: {g.CustomerName}" + Environment.NewLine
                                        + $"Đơn vị tính: {g.UnitName}" + Environment.NewLine
                                        + $"Quy cách: {g.ConvertUnit}" + Environment.NewLine
                                        + $"Giá nhập gần nhất: {Convert.ToDouble(g.LastImpPriceVat).ToString("N2")}" + Environment.NewLine
                                        + $"Giá bán lẻ: {Convert.ToDouble(g.ExpRetailPriceVat).ToString("N2")}" + Environment.NewLine
                                        + $"Tình trạng: {g.Status}";

                    Comment comment = r.AddComment(commentText);
                    comment.Shape.TextFrame.AutoSize = true;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }




        string connectionString = null;
        string fromDate = null;
        string toDate = null;
        string supplierName = null;
        string IP = null;
        string dataBase = null;
        string tranCode = null;
        string exportIDimportID = null;
        string priceVAT = null;
        string totalVAT = null;


        private void SetParameters()
        {
            connectionString = GetConnectionString();
            fromDate = Module1.ConvertDateToTransDate(dtpFromDate.Value);
            toDate = Module1.ConvertDateToTransDate(dtpToDate.Value);
            supplierName = TextBoxSearch.Text;
            IP = textBoxIP.Text;
            dataBase = comboBoxDataBase.Text;
            tranCode = PublicVar.DT_ImportOrExportTranCode().Rows[ComboboxImportOrExport.SelectedIndex][1].ToString();
            exportIDimportID = PublicVar.DT_ImportOrExportTranCode().Rows[ComboboxImportOrExport.SelectedIndex][2].ToString();
            priceVAT = PublicVar.DT_ImportOrExportTranCode().Rows[ComboboxImportOrExport.SelectedIndex][3].ToString();
            totalVAT = PublicVar.DT_ImportOrExportTranCode().Rows[ComboboxImportOrExport.SelectedIndex][4].ToString();

            //fromDate = dtpFromDate.SafeInvokeFunction(() => { fromDate = Module1.ConvertDateToTransDate(dtpFromDate.Value); });
            //toDate = Module1.ConvertDateToTransDate(dtpToDate.Value);
            //supplierName = tboxSearch.Text;
            //IP = textBoxIP.Text;
            //dataBase = comboBoxDataBase.Text;
            //tranCode = PublicVar.DT_ImportOrExportTranCode().Rows[ComboboxImportOrExport.SelectedIndex][1].ToString();
            //exportIDimportID = PublicVar.DT_ImportOrExportTranCode().Rows[ComboboxImportOrExport.SelectedIndex][2].ToString();
            //priceVAT = PublicVar.DT_ImportOrExportTranCode().Rows[ComboboxImportOrExport.SelectedIndex][3].ToString();
            //totalVAT = PublicVar.DT_ImportOrExportTranCode().Rows[ComboboxImportOrExport.SelectedIndex][4].ToString();
        }

        private async Task<DataTable> GetTransactionsAsync()
        {
            //đang bị lỗi ở chỗ on trs.ExportID = ctm.CustomerID
            //phải là importID thì sql xuất trả mới đúng
            /*ExportID : phiếu nhập hàng
             *ImportID : phiếu xuất trả
             */
            string sqlTransactions = @"select td.TransDate as [Ngày tháng],SUM(td." + totalVAT + @") as [Thành tiền], td.TransactionID as [Mã giao dịch]
                        from [dbo].[Transactions] ts
                        join [dbo].[TransDetail] td on td.TransactionID = ts.TransactionID
                        join [dbo].[Customers] ctm on ts." + exportIDimportID + @" = ctm.CustomerID
                        where ts.TransCode = '" + tranCode + @"'
                        and ts.TransDate >= '" + fromDate + "' and ts.TransDate <= '" + toDate + @"'
                        and ctm.CustomerName like N'%" + supplierName + @"%'
                        and ts.Status ='1'
                        group by td.TransDate, td.TransactionID
                        order by td.TransDate, td.TransactionID";
            DataTable dt = await ADO.GetDataTableFromSQLServerAsync(connectionString, sqlTransactions).ConfigureAwait(false);
            return dt;
        }


        private async Task<DataTable> GetTranDetailAsync()
        {
            string sqlTransdetail = @"select td.TransactionID, td.GoodID,gd.ShortName, td.Quantity, td." + priceVAT + @" / 1.1, td." + totalVAT + @" / 1.1, td." + totalVAT + @", 1
                                    from TransDetail as td
                                    join Goods as gd on td.GoodID = gd.GoodID
                                    join Transactions as ts on td.TransactionID = ts.TransactionID
                                    join Customers as ctm on ts." + exportIDimportID + @" = ctm.CustomerID
                                    where td.TransDate >= '" + fromDate + "' and td.TransDate <= '" + toDate + @"'
                                    and ctm.CustomerName like N'%" + supplierName + @"%'
                                    and ts.TransCode ='" + tranCode + @"'
                                    and ts.Status ='1'
                                    order by td.TransDate, td.TransactionID";
            DataTable dt = await ADO.GetDataTableFromSQLServerAsync(connectionString, sqlTransdetail).ConfigureAwait(false);
            return dt;
        }

        private async Task<DataTable> GetCustomerNameAsync()
        {
            string sqlCustomerName = @"select ts.TransactionID,'Ngày ' + RIGHT(ts.TransDate, 2) + '/' + SUBSTRING(ts.TransDate, 5, 2) + '/' + LEFT(ts.TransDate, 4) + ' - ' + N'NCC: ' + ts." + exportIDimportID + @" + ' - ' + ctm.CustomerName,'','','','','','', 0
                                    from[dbo].Transactions as ts
                                    join[dbo].Customers as ctm
                                    on ts." + exportIDimportID + @" = ctm.CustomerID
                                    where ts.TransCode = '" + tranCode + @"'
                                    and ts.TransDate >= '" + fromDate + @"' and ts.TransDate <= '" + toDate + @"'
                                    and ctm.CustomerName like N'%" + supplierName + @"%'
                                    and ts.Status = '1'";
            DataTable dt = await ADO.GetDataTableFromSQLServerAsync(connectionString, sqlCustomerName).ConfigureAwait(false);
            return dt;
        }

        private async Task<DataTable> GetSubtotalAsync()
        {
            //phải dùng cộng của trandetail vì số tổng của transaction là số đã tính lại giá vốn, có chênh lệch

            //string sqlSubtotal = @"select td.TransactionID,'','','','','',SUM(td." + totalVAT + @"/1.1),SUM(td." + totalVAT + @"), 2
            //                    from[dbo].TransDetail as td
            //                    join[dbo].Transactions ts on td.TransactionID = ts.TransactionID
            //                    join[dbo].Customers as ctm on td.CustomerID = ctm.CustomerID                            
            //                    where ts.TransCode = '" + tranCode + @"'
            //                    and ts.TransDate >= '" + fromDate + @"' and ts.TransDate <= '" + toDate + @"'
            //                    and ctm.CustomerName like N'%" + supplierName + @"%'
            //                    and ts.Status ='1'
            //                    group by td.TransactionID";

            //string sqlSubtotal_old = @"Select
            //                        dbo.TransDetail.TransactionID,
            //                        '' As Column1,
            //                        '' As Column2,
            //                        '' As Column3,
            //                        '' As Column4,
            //                        '' As Column5,
            //                        Sum(dbo.TransDetail." + totalVAT + @" / 1.1) As TotalExpPriceVAT,
            //                        Sum(dbo.TransDetail." + totalVAT + @") As TotalExpPriceVAT1,
            //                        '2' As Column6
            //                    From
            //                        dbo.TransDetail Inner Join
            //                        dbo.Transactions On dbo.TransDetail.TransactionID = dbo.Transactions.TransactionID
            //                        Inner Join
            //                        dbo.Customers On dbo.TransDetail." + exportIDimportID + @" = dbo.Customers.CustomerID
            //                    Where
            //                        dbo.Transactions.TransCode = '" + tranCode + @"' And
            //                        dbo.Transactions.TransDate >= '" + fromDate + @"' And
            //                        dbo.Transactions.TransDate <= '" + toDate + @"' And
            //                        dbo.Customers.CustomerName Like N'%" + supplierName + @"%' And
            //                        dbo.Transactions.Status = '1'
            //                    Group By
            //                        dbo.TransDetail.TransactionID";

            string sqlsubtotal_new = string.Format(@"select abc.TransactionID,'','','','','',SUM(abc.totalimprice),SUM(abc.totalimpricevat),2
                                            from
                                            (
                                            select td.TransactionID,
                                            td.TransDate,
                                            td.{5} / 1.1 as totalimprice,
                                            td.{5} as totalimpricevat

                                            from TransDetail as td
                                            join Transactions as ts on td.TransactionID = ts.TransactionID
                                            join Customers as ctm on ts.{4} = ctm.CustomerID

                                            where td.TransDate >= '{0}' and td.TransDate <= '{1}'
                                            and ctm.CustomerName like N'%{2}%'
                                            and ts.TransCode ='{3}'
                                            and ts.Status ='1'

                                            ) abc

                                            group by abc.TransactionID,abc.TransDate
                                            order by abc.TransDate, abc.TransactionID", fromDate, toDate, supplierName, tranCode, exportIDimportID, totalVAT);

            DataTable dt = await ADO.GetDataTableFromSQLServerAsync(connectionString, sqlsubtotal_new).ConfigureAwait(false);
            return dt;
        }


        /// <summary>
        /// lấy ngày tháng từ diễn giải giao dịch
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        string GetDateString(string s)
        {
            return s.Substring(29, 10);
        }


        /// <summary>
        /// lấy tổng tiền VAT từ range
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        string GetTotalVAT(Range range)
        {
            return range.Offset[1, 0].Offset[0, 6].End[XlDirection.xlDown].Text;
        }


        /// <summary>
        /// lấy mã giao dịch từ diễn giải giao dịch
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        string GetTransactionID(string s)
        {
            return s.Substring(11, 15);
        }


        private void ButtonCreateImport2_Click(object sender, EventArgs e)
        {
            //Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            //Worksheet temp = wb.GetWorksheet("Temp", false);

            //try
            //{
            //    if (string.IsNullOrEmpty(textBoxSoHoaDon.Text) == false)
            //    {
            //        Module1.SpeedUpCode(true);
            //        Workbook importFile = Globals.ThisAddIn.Application.Workbooks.Add();
            //        Worksheet sheet1 = importFile.Worksheets[1];
            //        sheet1.Name = "MAUHOADON";
            //        sheet1.Range["a1"].SetHeaderText(new string[] { "SOHOADON", "MAVTU", "TENVTU", "TONCUOIKYSL", "DON GIA", "TONCUOIKYGT", "MANCC", "NGAYHD", "KYHIEUHD" });

            //        //copy mã hàng, tên, số lượng, đơn giá
            //        Range copyRange = temp.Range["C2", "F" + temp.LastRow("C")];
            //        copyRange.Copy();
            //        sheet1.Range["B2"].PasteSpecial(XlPasteType.xlPasteValues);


            //        //số hóa đơn
            //        string soHoaDon = string.Format("{0:0000000}", int.Parse(textBoxSoHoaDon.Text));
            //        sheet1.Range["A2"].Resize[copyRange.Rows.Count, 1].NumberFormat = "@";
            //        sheet1.Range["A2"].Resize[copyRange.Rows.Count, 1].Value2 = soHoaDon;

            //        //Tên ncc
            //        string tenNCC = Convert.ToString(textBoxTenNCC.Text);

            //        //ngày hóa đơn
            //        string ngayHoaDon = DTPNgayHoaDon.Text;
            //        sheet1.GetRange("h2:h", "a").Value = "'" + ngayHoaDon;

            //        //ký hiệu hóa đơn: dùng để ghi toàn bộ nội dung diễn giải
            //        string kyHieuHoaDon = Convert.ToString(textBoxKyHieuHoaDon.Text);

            //        string noiDungDienGiai1 = string.Empty;
            //        string noiDungDienGiai2 = string.Empty;
            //        //noiDungDienGiai1 = $"{tienTruocThue};{tienVAT};{thanhTien}";
            //        noiDungDienGiai2 = $"{tenNCC};{soHoaDon};{kyHieuHoaDon}";

            //        //ghi nội dung vào cột ký hiệu hóa đơn trong file import
            //        sheet1.GetRange("i2:i", "a").Value2 = noiDungDienGiai1;//cột ký hiệu hóa đơn max length 50
            //        sheet1.GetRange("a2:a", "a").Value2 = noiDungDienGiai2;//cột số hóa đơn , max length 50

            //        foreach (Comment cmt in temp.Comments) { cmt.Delete(); }
            //        Comment comment = temp.Range["G1"].AddComment();
            //        comment.Text($"Xuất file import thành công" + Environment.NewLine + $"Số hóa đơn: {soHoaDon}");
            //        comment.Visible = true;
            //        comment.Shape.TextFrame.AutoSize = true;

            //        //save
            //        string saveFolder = PublicVar.ImportNhapHangSaveLocation();
            //        importFile.SaveOverwrite(saveFolder + @"\FileImportNhapHang", 56);
            //        importFile.Close();

            //        //write text file chứa các thông tin
            //        string ngay = DTPNgayHoaDon.Value.Day.ToString("00");
            //        string thang = DTPNgayHoaDon.Value.Month.ToString("00");
            //        string nam = DTPNgayHoaDon.Value.Year.ToString("0000");
            //        string ngaythangnam = $"{ngay}/{thang}/{nam}";
            //        string[] text = new string[] { tenNCC, ngay, thang, nam, ngaythangnam, kyHieuHoaDon, soHoaDon, tienTruocThue, tienVAT, thanhTien };

            //        System.IO.File.WriteAllLines(@"C:\ExcelVSTOAddin\TenNCC.txt", text);


            //        Module1.SpeedUpCode(false);
            //    }
            //    else
            //    {
            //        MessageBox.Show("Chưa có số tiền");
            //        textBoxSoHoaDon.Select();
            //    }
            //}
            //catch { Module1.SpeedUpCode(false); }
            //Module1.SpeedUpCode(false);
            //temp.Activate();
            //temp.Range["G1"].Select();
        }

        private async void Button1_ChuanBiDuLieu_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;

            if (ws.Range["a4"].GetValue(ValueType.String) != "BÁO CÁO XUẤT HÀNG BÁN LẺ")
            {
                MessageBox.Show("Đây không phải sheet báo cáo xuất hàng bán lẻ nguyên gốc", PublicVar.title);
                return;
            }

            Worksheet bangma = await ExcelObject.GetSheetBangMaFromGoogleDriveAsync(wb);
            Worksheet dulieubanle = ExcelObject.CopyRangeToNewSheetAndClearAllFormat(ws.GetRange("A14:P", "a"), "_Chi tiết bán lẻ");
            dulieubanle.GetRange("c1:f", "a").ConvertToValue();
            dulieubanle.Range["e1"].Value2 = "Đơn giá chưa VAT";
            dulieubanle.Activate();
        }

        private async void ButtonTimKiemBill_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet bangma = await ExcelObject.GetSheetBangMaFromGoogleDriveAsync(wb);
            Worksheet ws = wb.GetSheet("_Chi tiết bán lẻ");
            Worksheet bill = wb.GetSheet("bill");
            bill.Range["a1"].SetHeaderText(new string[] { "mã vật tư", "tên vật tư", "số lượng", "đơn giá", "đơn giá chưa VAT", "thành tiền VAT", "mã vietsea", "tồn" });

            string maGD = TextBoxMaGD.GetText();
            List<string> ListMaGD = new List<string>();
            if (maGD.Contains(","))
            {
                ListMaGD = maGD.Split(',').ToList();
            }
            else
            {
                ListMaGD.Add(maGD);
            }

            List<Range> maGDRange = ws.GetRange("a1:a", "a").FindAllRangeEqualToValue(ListMaGD);
            if (maGDRange.Count == 0)
            {
                MessageBox.Show("Không tìm thấy mã giao dịch nào phù hợp");
                return;
            }

            foreach (Range rng in maGDRange)//cop những giao dịch mà tìm được
            {
                ws.Range[rng.Offset[1, 0], rng.Offset[1, 0].End[XlDirection.xlToRight].End[XlDirection.xlDown].Offset[-1, 0]].Copy(bill.NewRangeAtColumn("a"));
            }

            Module1.VlookupMAVIETSEA(bill.GetRange("a2:a", "a"), bill.Range["g2"], bangma);
            bill.Columns.AutoFit();
            bill.Activate();
        }

        private async void button_AutoImportNhapHoaDon_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;

            string date = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(ExcelAddIn1.PublicVar.fileImportNhapHang_fullPath, "MAUHOADON", "H2");
            string sohoadon = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(ExcelAddIn1.PublicVar.fileImportNhapHang_fullPath, "MAUHOADON", "A2");
            string defaultMaNCC = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(ExcelAddIn1.PublicVar.fileImportNhapHang_fullPath, "MAUHOADON", "G2");

            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();

                await Task.Run(() =>
                {
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
                    WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);
                    WinAppDriver.Vietsea.VietseaAuto.Auto_ImportPhieuNhapHang(date, sohoadon, date, defaultMaNCC, "10", numericUpDownNumberOfCopies.Value.ToString());
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();

                    ws.Activate();
                    ExcelAddIn1.AutoTest.WinAppDriver.Method.SetForegroundWindow(new IntPtr(Globals.ThisAddIn.Application.Hwnd));
                    Notification.Toast.Show("Nhập phiếu thành công");
                }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);
            }
            catch (System.OperationCanceledException)
            {
                Notification.Toast.Show("Macro was canceled");
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }








        }

        private void gridLookUpEditTenNCC_Click(object sender, EventArgs e)
        {
            gridLookUpEditTenNCC.SelectAll();
        }

        private void gridLookUpEditTenNCC_Popup(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.GridLookUpEdit edit = sender as DevExpress.XtraEditors.GridLookUpEdit;
            if (edit != null)
            {
                DevExpress.XtraEditors.Popup.PopupGridLookUpEditForm f = edit.GetPopupEditForm();
                if (f != null)
                {
                    int newPopupFormWidth = 500;
                    f.Width = newPopupFormWidth;
                    f.Left -= newPopupFormWidth - 100;
                }
            }
        }

        private void gridLookUpEditTenNCC_Enter(object sender, EventArgs e)
        {
            gridLookUpEditTenNCC.SelectAll();
        }

        private async void gridLookUpEditTenNCC_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                string MAKH = gridLookUpEditTenNCC.EditValue as string;
                string querry = string.Format(@"Select * From
(Select kyhieuhoadongd
From Vattugd
Where 
Maptnx = 'NMUA'
And Makhachang = '{0}'
Order By Ngayhachtoan Desc)
where rownum = 1", MAKH);
                var kyhieuHD = await ADO.GetDataTableFromOracleServerAsync(PublicVar.ConnectionStringOracleDatPhat, querry);
                if (kyhieuHD.HasData())
                {
                    textBoxKyHieuHoaDon.SetTextSafeThread(kyhieuHD.Rows[0][0].ToString());
                }
            }
            catch { }
        }

        private void textBoxSoTien_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BnReCalculateTotalAmount_Click(sender, e);
            }
        }

        private void textBoxSoHoaDon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BnCreateImportFile_Click(sender, e);
            }
        }
    }



}

