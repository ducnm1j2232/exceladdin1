﻿using ExcelAddIn1.MyClass;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ExcelAddIn1.MyUserControl
{
    public partial class AdvanceFilter : UserControl
    {
        public AdvanceFilter()
        {
            InitializeComponent();
        }

        private Range filterRange;

        private string[] GetHeaderText(Range range)
        {
            Worksheet worksheet = range.Worksheet;
            Range firstCell = range.Cells[1, 1];
            Range lastCellAtRow1 = range.Cells[1, range.Columns.Count];
            Range headerRange = worksheet.Range[firstCell, lastCellAtRow1];

            List<string> headerTexts = new List<string>();
            foreach (Range r in headerRange)
            {
                headerTexts.Add(Convert.ToString(r.Value));
            }

            return headerTexts.ToArray();
        }

        private void ButtonSelectFilterRange_Click(object sender, EventArgs e)
        {
            filterRange = null;
            filterRange = Module1.InputBoxRange("Chọn vùng dữ liệu cần lọc\r\nBao gồm cả 1 dòng tiêu đề", true);
            if (filterRange == null) return;
            string worksheetName = filterRange.Worksheet.Name;
            TextBoxFilterRange.Text = $"'{worksheetName}'!{filterRange.Address}";

            //điền các trường tiêu đề vào grid
            string[] headers = GetHeaderText(filterRange);
            dataGridViewAdvanceFilter.Rows.Clear();
            foreach (string s in headers)
            {
                if (string.IsNullOrEmpty(s))
                {
                    MsgBox.Show("Có trường chưa được đặt tên");
                    dataGridViewAdvanceFilter.Rows.Clear();
                    return;
                }
                dataGridViewAdvanceFilter.Rows.Add(s);
            }
        }

        private void ButtonApplyFilter_Click(object sender, EventArgs e)
        {
            Worksheet ws = filterRange.Worksheet;

            AutoFilter autoFilter = ws.AutoFilter;
            if (autoFilter != null) autoFilter.ShowAllData();

            for (int i = 0; i < dataGridViewAdvanceFilter.Rows.Count; i++)//lặp qua từng dòng tiêu đề để lấy điều kiện
            {
                int field = i + 1;
                if (dataGridViewAdvanceFilter.Rows[i].Cells[1].Value != null)//nếu điều kiện tiêu đề khác rỗng
                {
                    string criteriaValue = dataGridViewAdvanceFilter.Rows[i].Cells[1].Value.ToString();
                    Range criteriaRange = ws.Range[filterRange.Cells[2, field], filterRange.Cells[filterRange.Rows.Count, field]];

                    object criteriaParam;
                    XlAutoFilterOperator xlAutoFilterOperator = XlAutoFilterOperator.xlAnd;
                    if (criteriaValue.ContainsAtLeast1Letter())//nếu chứa ít nhất 1 chữ cái => coi như là text
                    {
                        criteriaParam = "*" + criteriaValue + "*";//lọc kiểu contains

                        xlAutoFilterOperator = XlAutoFilterOperator.xlAnd;
                        if (criteriaValue.Contains(","))
                        {
                            object[] criteriaArray = criteriaValue.Split(',');
                            criteriaParam = criteriaRange.GetAllValueFoundInRange(criteriaArray).ToArray();
                            xlAutoFilterOperator = XlAutoFilterOperator.xlFilterValues;//muốn lọc theo array thì phải có Operator theo Values mới được
                        }
                    }
                    else//còn lại coi như là số
                    {
                        criteriaParam = criteriaValue;
                    }

                    filterRange.AutoFilter(field, criteriaParam, xlAutoFilterOperator);
                }



            }
        }

        private void buttonSmartFind_Click(object sender, EventArgs e)
        {

        }
    }
}
