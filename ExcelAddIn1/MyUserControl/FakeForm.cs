﻿namespace ExcelAddIn1.MyUserControl
{
    public partial class FakeForm : DevExpress.XtraEditors.XtraForm
    {
        public FakeForm()
        {
            InitializeComponent();
            var intptr = this.Handle;

            alertControl1.AutoHeight = true;
            alertControl1.AllowHotTrack = false;

            alertControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            alertControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            //alertControl1.LookAndFeel.SetSkinStyle(DevExpress.LookAndFeel.SkinStyle.VisualStudio2010);
            //string skinName = getSkinName();
            alertControl1.LookAndFeel.SetSkinStyle(DevExpress.LookAndFeel.SkinStyle.VisualStudio2013Dark);

            alertControl1.FormShowingEffect = DevExpress.XtraBars.Alerter.AlertFormShowingEffect.SlideHorizontal;
        }


        public void ShowAlertCore(string text, string caption)
        {
            BeginInvoke(new System.Windows.Forms.MethodInvoker(delegate ()
            {
                alertControl1.Show(this, caption, text);
            }));
        }

        private void alertControl1_BeforeFormShow(object sender, DevExpress.XtraBars.Alerter.AlertFormEventArgs e)
        {
            e.AlertForm.Width = 350;
            e.AlertForm.OpacityLevel = 1;
        }




    }
}