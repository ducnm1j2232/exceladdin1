﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelAddIn1
{
    public partial class CustomDateTimePicker : DateTimePicker
    {
        public CustomDateTimePicker()
        {
            InitializeComponent();
            this.CustomFormat = "dd/MM/yyyy";
            this.Format = DateTimePickerFormat.Custom;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        //https://stackoverflow.com/questions/2399954/datetimepicker-automatically-move-to-next-datepart
        private bool selectionComplete = false;
        private bool numberKeyPressed = false;

        private const int WM_KEYUP = 0x0101;
        private const int WM_KEYDOWN = 0x0100;
        private const int WM_REFLECT = 0x2000;
        private const int WM_NOTIFY = 0x004e;

        //[StructLayout(LayoutKind.Sequential)]
        private struct NMHDR
        {
#pragma warning disable CS0649 // Field 'CustomDateTimePicker.NMHDR.hwndFrom' is never assigned to, and will always have its default value
            public IntPtr hwndFrom;
#pragma warning restore CS0649 // Field 'CustomDateTimePicker.NMHDR.hwndFrom' is never assigned to, and will always have its default value
#pragma warning disable CS0649 // Field 'CustomDateTimePicker.NMHDR.idFrom' is never assigned to, and will always have its default value
            public IntPtr idFrom;
#pragma warning restore CS0649 // Field 'CustomDateTimePicker.NMHDR.idFrom' is never assigned to, and will always have its default value
#pragma warning disable CS0649 // Field 'CustomDateTimePicker.NMHDR.Code' is never assigned to, and will always have its default value 0
            public int Code;
#pragma warning restore CS0649 // Field 'CustomDateTimePicker.NMHDR.Code' is never assigned to, and will always have its default value 0
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            numberKeyPressed = (e.Modifiers == Keys.None && ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode != Keys.Back && e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9)));
            selectionComplete = false;
            base.OnKeyDown(e);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_REFLECT + WM_NOTIFY)
            {
                var hdr = (NMHDR)m.GetLParam(typeof(NMHDR));
                if (hdr.Code == -759) //date chosen (by keyboard)
                    selectionComplete = true;
            }
            base.WndProc(ref m);
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (numberKeyPressed && selectionComplete &&
                (e.Modifiers == Keys.None && ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode != Keys.Back && e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9))))
            {
                Message m = new Message();
                m.HWnd = this.Handle;
                m.LParam = IntPtr.Zero;
                m.WParam = new IntPtr((int)Keys.Right); //right arrow key
                m.Msg = WM_KEYDOWN;
                base.WndProc(ref m);
                m.Msg = WM_KEYUP;
                base.WndProc(ref m);
                numberKeyPressed = false;
                selectionComplete = false;
            }
        }


    }
}
