﻿namespace ExcelAddIn1.Notification
{
    public static class Toast
    {
        private static ToastForm.ToastForm1 _toastForm;

        public static ToastForm.ToastForm1 toastForm
        {
            get
            {
                if (_toastForm == null)
                {
                    _toastForm = new ToastForm.ToastForm1();
                    return _toastForm;
                }
                else
                {
                    return _toastForm;
                }

            }
        }

        public static void Show(string text)
        {
            new System.Threading.Thread(new System.Threading.ThreadStart(() => { new ToastForm.ToastForm1().ShowToastCore(text); })).Start();
            //new System.Threading.Thread(new System.Threading.ThreadStart(() => { toastForm.ShowToastCore(text); })).Start();
            //var d = toastForm.Handle;
            //toastForm.BeginInvoke((System.Windows.Forms.MethodInvoker)delegate { toastForm.ShowToastCore(text); });
        }
    }
}
