﻿using System;
using System.Windows.Forms;

namespace ExcelAddIn1.Notification.ToastForm
{
    public partial class ToastForm1 : Form
    {
        public ToastForm1()
        {
            InitializeComponent();

            StartPosition = FormStartPosition.Manual;
        }

        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                //make sure Top Most property on form is set to false
                //otherwise this doesn't work
                int WS_EX_TOPMOST = 0x00000008;
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= WS_EX_TOPMOST;
                return cp;
            }
        }

        public void ShowToastCore(string text)
        {
            label1.Text = text;

            this.Width = label1.Width + 24;
            //this.Height = 35;

            //label1.Top = (this.Height - label1.Height) / 2;
            //label1.Left = (this.Width - label1.Width) / 2;
            label1.Location = new System.Drawing.Point(this.Left + (this.Width - label1.Width) / 2, this.Top + (this.Height - label1.Height) / 2);

            SetPosition();
            ShowDialog();
        }


        private void SetPosition()
        {
            //this.Top = Cursor.Position.Y + 15;
            //this.Left = Cursor.Position.X + 15;

            Screen screen = Screen.FromPoint(Cursor.Position);
            //this.Location = new System.Drawing.Point((screen.WorkingArea.Right - this.Width) / 2, screen.WorkingArea.Height - this.Height);
            this.Top = screen.WorkingArea.Top + screen.WorkingArea.Height - this.Height;
            this.Left = screen.WorkingArea.Left + ((screen.WorkingArea.Width - this.Width) / 2);
        }

        private void ToastForm1_Shown(object sender, EventArgs e)
        {
            timer1.Interval = 4000;
            timer1.Enabled = true;
            timer1.Tick += Timer1_Tick;
            timer1.Start();
        }


        private void Timer1_Tick(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
