﻿namespace ExcelAddIn1.Notification
{
    public static class Wait
    {
        public static Waitform.MyWaitingForm _waitingForm = null;
        //static WaitFormParent waitFormParent = new WaitFormParent();
        private delegate void dlg(string str);

        public static void Show(string text)
        {
            dlg delegateShow = DelegateMethod;
            delegateShow(text);
        }

        private static void DelegateMethod(string text)
        {
            WaitingForm.ShowCore(text);
        }


        public static void Close()
        {
            //waitFormParent.CloseWaitFormCore();
            WaitingForm.CloseCore();
        }

        static Waitform.MyWaitingForm WaitingForm
        {
            get
            {
                if (_waitingForm == null)
                {
                    _waitingForm = new Waitform.MyWaitingForm();
                }
                return _waitingForm;
            }
        }


    }
}
