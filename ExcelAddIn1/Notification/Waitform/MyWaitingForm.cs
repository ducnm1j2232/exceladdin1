﻿using System.Windows.Forms;

namespace ExcelAddIn1.Notification.Waitform
{
    public partial class MyWaitingForm : Form
    {
        public delegate void delegateVoidSetPos();
        public delegateVoidSetPos setPos;

        public delegate void delegateSetText(string text);
        public delegateSetText setText;

        public delegate void delegateSetVisible(bool value);
        public delegateSetVisible setVisible;


        public MyWaitingForm()
        {
            InitializeComponent();

            StartPosition = FormStartPosition.Manual;
            setPos = new delegateVoidSetPos(SetPositionMethod);
            setText = new delegateSetText(SetTextMethod);
            setVisible = new delegateSetVisible(SetVisibleMethod);
            var hnd = this.Handle;
            TopMost = true;
            Height = 31;
            pictureBox1.Height = Height;
            pictureBox1.Width = Height;
        }

        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        public void SetTextMethod(string text)
        {
            label1.Text = text;
            Width = pictureBox1.Width + label1.Width + pictureBox1.Width;
            label1.Location = new System.Drawing.Point(pictureBox1.Width, (this.Height - label1.Height) / 2+2);
        }

        public void SetPositionMethod()
        {
            Screen screen = Screen.FromPoint(Cursor.Position);
            Location = new System.Drawing.Point(screen.WorkingArea.Left + (screen.WorkingArea.Width - Width) / 2, screen.WorkingArea.Top);

        }

        public void SetVisibleMethod(bool value)
        {
            if (value)
            {
                this.Show();
            }
            else
            {
                this.Hide();
            }
        }

        public void ShowCore(string text)
        {
            this.Invoke(setText, text);
            this.Invoke(setPos);
            this.Invoke(setVisible, true);
            this.Invoke((MethodInvoker)delegate
             {
                 //SetTextMethod(text);
                 //SetPositionMethod();
                 //SetVisibleMethod(true);
                 Refresh();
             });
        }

        public void CloseCore()
        {
            this.Invoke(setVisible, false);
        }

        private void label1_Click(object sender, System.EventArgs e)
        {
            try
            {
                Notification.Wait.Close();
            }
            catch { }
            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationTokenSource?.Cancel();
            }
            catch { }

            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }
            catch { }
        }

        private void MyWaitingForm_Click(object sender, System.EventArgs e)
        {
            try
            {
                Notification.Wait.Close();
            }
            catch { }
            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationTokenSource?.Cancel();
            }
            catch { }

            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }
            catch { }
        }
    }

}
