﻿using System.Windows.Forms;

namespace ExcelAddIn1.Forms.InputPassword
{
    public partial class FormInputPassword : Form
    {
        private string pass = "singonceagainwithme";

        public FormInputPassword()
        {
            InitializeComponent();

            //InputPasswordViewModel viewModel = new InputPasswordViewModel();
        }

        private bool _IsPasswordValid = false;
        public bool IsPasswordValid { get { return _IsPasswordValid; } set { _IsPasswordValid = value; } }

        private bool _ByPass = false;
        public bool ByPass
        {
            get
            {
                this.ShowDialog();
                return _ByPass;
            }
            set { _ByPass = value; }
        }

        private void buttonSubmit_Click(object sender, System.EventArgs e)
        {
            string input = textBoxInputPassword.Text;
            if (input == pass)
            {
                _ByPass = true;
                this.Close();
            }
            else
            {
                MessageBox.Show(this,"Sai mật khẩu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                textBoxInputPassword.Focus();
                textBoxInputPassword.SelectAll();
            }
        }
    }
}
