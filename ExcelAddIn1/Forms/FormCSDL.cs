﻿using ExcelAddIn1.AutoTest.WinAppDriver;
using ExcelAddIn1.MyClass;
using ExcelAddIn1.MyClass.Extensions;
using Microsoft.Office.Interop.Excel;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.Forms
{
    public partial class FormCSDL : Form
    {
        public const string XNT_COLUMN_MAVATTU = "MAVATTU";
        public const string XNT_COLUMN_TENVATTU = "TENVATTU";
        public const string XNT_COLUMN_SOLUONG = "SOLUONG";
        public const string XNT_COLUMN_GIAVON = "GIAVON";
        public const string XNT_COLUMN_GIATRI = "GIATRI";

        public const string XNT_NAME_TYPE_FULL = "-F";
        public const string XNT_NAME_TYPE_AVAILABLE = "-A";
        public const string XNT_NAME_TYPE_KHA_DUNG = "-KD";// KHẢ DỤNG
        public const string XNT_NAME_TYPE_THUA = "-TH";// THỪA
        public const string XNT_NAME_TYPE_XUATTRA = "-XT";

        public const string SHEETNAME_STOCKINFO = "stockinfo";
        public const string SHEETNAME_XUATTRA = "XUATTRA";

        public const string HEADER_TONTHUCTE = "tồn thực tế";
        public const string HEADER_TONKHADUNG = "tồn khả dụng";



        public FormCSDL()
        {
            InitializeComponent();

            CenterToScreen();

            comboBoxZone.DataSource = PublicVar.DT_ServerList();
            comboBoxZone.DisplayMember = "Name";
            textBoxIP.Text = PublicVar.DT_ServerList().Rows[comboBoxZone.SelectedIndex][1].ToString();

            radioButtonKHACHLE.Checked = true;

            customComboBoxVATrate.Text = "10";
            textBoxNumberOfRowToGet.Text = "10";

            gridLookUpEditMaNCC.Enabled = false;
            //LoadData_DANH_MUC_KHACH_HANG().Wait();

            checkBox_RoundExpPrice.Checked = true;

        }

        private System.Data.DataTable DANH_MUC_KHACH_HANG;

        private async Task LoadData_DANH_MUC_KHACH_HANGAsync()
        {
            DANH_MUC_KHACH_HANG = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), "select makhachhang,tentochu from dmkhachhang where manhomkhachang like '%NCC%'");
        }


        private bool DataBaseIsNull()
        {
            if (string.IsNullOrEmpty(comboBoxDBName.GetText()))
            {
                MsgBox.Show("Chưa chọn tên cơ sở dữ liệu!");
                comboBoxDBName.Focus();
                return true;
            }
            return false;
        }


        private void ButtonHideForm_Click(object sender, EventArgs e)
        {
            Hide();
        }


        private string GetConnectionString()
        {
            string ip = string.Empty;
            string DBname = string.Empty;
            string user = string.Empty;
            string pass = string.Empty;

            comboBoxZone.SafeInvoke(() =>
            {
                ip = textBoxIP.GetText();
                DBname = comboBoxDBName.GetText();
                user = PublicVar.DT_ServerList().Rows[comboBoxZone.SelectedIndex][2].ToString();
                pass = PublicVar.DT_ServerList().Rows[comboBoxZone.SelectedIndex][3].ToString();
            });

            string output = $"Server={ip}; Database={DBname}; User Id={user}; Password ={pass}";
            return output;
        }

        private string GetConnectionString_TONGKHO()
        {
            return $"Server=192.168.0.9; Database=ACCOUNTING; User Id=sa; Password =datphat@hanoi; ";
        }

        public string GetConnectionStringOracle()
        {
            string ip = textBox_IP_Oracle.GetText();
            string port = textBox_Port_Oracle.GetText();

            OracleConnectionStringBuilder stringBuilder = new OracleConnectionStringBuilder();
            stringBuilder.DataSource = $"{ip}:{port}/TBNETERP";
            stringBuilder.UserID = "TBNETERP";
            stringBuilder.Password = "TBNETERP";
            return stringBuilder.ConnectionString;
        }

        private async void buttonDownloadBANGMA_Click(object sender, EventArgs e)
        {
            try
            {
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet BANGMA = await ExcelObject.GetSheetBangMaFromGoogleDriveAsync(wb);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBoxDBName_DropDown(object sender, EventArgs e)
        {
            string ip = textBoxIP.GetText();
            string DBname = comboBoxDBName.GetText();
            string user = PublicVar.DT_ServerList().Rows[comboBoxZone.SelectedIndex][2].ToString();
            string pass = PublicVar.DT_ServerList().Rows[comboBoxZone.SelectedIndex][3].ToString();
            ADO.AddDataBaseNameToComboBox(ip, user, pass, comboBoxDBName);
        }

        private void comboBoxZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxIP.Text = PublicVar.DT_ServerList().Rows[comboBoxZone.SelectedIndex][1].ToString();
        }

        private async void buttonGetRetailTranDetail_Click(object sender, EventArgs e)
        {
            try
            {
                #region main
                if (DataBaseIsNull()) return;

                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sh = wb.GetNewSheet("Bán lẻ");
                Worksheet bangma = await ExcelObject.GetSheetBangMaFromGoogleDriveAsync(wb);

                string typedTranIDs = textBoxTypeTranID.GetText();
                string[] tranIDs = typedTranIDs.Split(',');
                string tranIDWhereIn = tranIDs.ToSQLWhereIn();
                string sqlGetRetailTranDetail = SQLquery.getRetailTranDetail(tranIDWhereIn);
                string sqlGetRetailTranDetailTotal = SQLquery.getRetailTranDetailTotal(tranIDWhereIn);

                DataTable dtRetailTranDetail = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString(), sqlGetRetailTranDetail);
                DataTable dtRetailTranDetailTotal = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString(), sqlGetRetailTranDetailTotal);

                if (dtRetailTranDetail.HasData())
                {
                    dtRetailTranDetail.CopyToRange(sh.Range["a1"]);

                    if (dtRetailTranDetailTotal.HasData())
                    {
                        dtRetailTranDetailTotal.CopyToRange(sh.NewRangeAtColumn("a"), false);
                    }
                }
                else
                {
                    MsgBox.Show("Không có dữ liệu");
                    return;
                }

                Module1.VlookupMAVIETSEA(sh.GetRange("a2:a", "a"), sh.Range["c2"], bangma);
                sh.GetRange("d2:g", "g").ConvertToValue();
                sh.GetRange("d2:d", "g").SetAccountingNumberFormat(0);
                sh.GetRange("e2:g", "g").SetAccountingNumberFormat(2);
                sh.Range["a1"].EntireRow.Font.Bold = true;
                sh.LastRange("g").EntireRow.Font.Bold = true;
                sh.Columns.AutoFit();

                //áp tồn kho
                if (checkBoxHienTonVIETSEA.Checked)
                {
                    sh.Range["h1"].SetHeaderText("BAN10", "K-HANG01", "BAN05", "K-HANG5");

                    List<string> listMAVTU = new List<string>();
                    listMAVTU = sh.GetRange("c2:c", "c").ValueToList();
                    DataTable data = await VIETSEA.TBNETERP.DATATABLE_XNT_VIETSEA_CORE(dtpToDateVietsea.Value, VIETSEA.TBNETERP.MAKHOHANG.ALL, VIETSEA.TBNETERP.XNTtype.Available, listMAVTU);
                    if (data != null && data.Rows.Count > 0)
                    {
                        foreach (Range rangeMavattu in sh.GetRange("c2:c", "c"))
                        {
                            foreach (Range rangeKho in sh.Range["h1:k1"])
                            {
                                string kho = rangeKho.GetValue(ValueType.String);
                                string mavattu = rangeMavattu.GetValue(ValueType.String);
                                Range cellToWriteValue = sh.Cells[rangeMavattu.Row, rangeKho.Column];

                                DataRow[] dataRows = data.Select($"[mavattu] = '{mavattu}' and kho = '{kho}'");
                                if (dataRows.Length > 0)
                                {
                                    string soluongTon = dataRows[0][3].ToString();
                                    cellToWriteValue.Value2 = soluongTon;
                                }
                            }
                        }
                    }

                    sh.GetRange("a1:k", "a").Sort1(sh.Range["h1"], XlSortOrder.xlDescending);//sort từng loại kho một để mã nào có nó dồn lên trên
                    sh.GetRange("a1:k", "a").Sort2(sh.Range["i1"], XlSortOrder.xlDescending);
                    sh.GetRange("a1:k", "a").Sort2(sh.Range["j1"], XlSortOrder.xlDescending);
                    sh.GetRange("a1:k", "a").Sort2(sh.Range["k1"], XlSortOrder.xlDescending);
                }
                sh.Activate();
                sh.Columns.AutoFit();
                Notification.Toast.Show("Tải dữ liệu thành công");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, PublicVar.errorTitle);
            }
        }


        private async void buttonGetStockInfo_Click(object sender, EventArgs e)
        {
            //await Task.Delay(0);
            try
            {
                #region MAIN
                if (DataBaseIsNull()) return;
                Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
                ExcelAddIn1.Notification.Wait.Show("Tải bangma");
                Worksheet bangma = await ExcelObject.GetSheetBangMaFromGoogleDriveAsync(workbook);

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("@Fromdate", dtpToDate.Value.ToString("yyyyMMdd")));
                sqlParameters.Add(new SqlParameter("@Todate", dtpToDate.Value.ToString("yyyyMMdd")));
                ExcelAddIn1.Notification.Wait.Show("Tải data");
                DataTable dt_stockinfoTONGKHO = await ADO.GetDataTableFromStoredProcedureAsync(GetConnectionString(), "[dbo].[System.Stock.Stockinfo]", sqlParameters);

                if (dt_stockinfoTONGKHO != null && dt_stockinfoTONGKHO.Rows.Count > 0)
                {
                    DataTable dtGreaterThanZero = dt_stockinfoTONGKHO.Select("EndQty > 0").CopyToDataTable();
                    DataTable dataTable = dtGreaterThanZero.ToDataTableSelectedColumns(new string[] { "GoodID", "FullName", "EndQty" });

                    Worksheet stockInfoSheet = workbook.GetNewSheet(SHEETNAME_STOCKINFO);
                    dataTable.CopyToRange(stockInfoSheet.Range["a1"]);
                    stockInfoSheet.GetRange("c1:c", "c").ConvertToValue();
                    stockInfoSheet.Range["b1"].EntireColumn.Insert();
                    stockInfoSheet.Range["b1"].Value2 = XNT_COLUMN_MAVATTU;
                    Module1.VlookupMAVIETSEA(stockInfoSheet.GetRangeByText("GoodID").offset(ExtensionMethod.xlOffset.Down).ResizeToLastRow("a"), stockInfoSheet.GetRangeByText(XNT_COLUMN_MAVATTU).offset(ExtensionMethod.xlOffset.Down), bangma);
                    //stockInfoSheet.GetRange("a1:d", "a").DeleteRowContainTextCore(2, "#n/a");
                    stockInfoSheet.Columns.AutoFit();

                    Notification.Toast.Show("Load tồn kho thành công!");
                }
                #endregion
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
            finally
            {
                this.SafeInvoke(() => BringToFront());
                ExcelAddIn1.Notification.Wait.Close();
            }
        }


        private void comboBoxZone_DropDown(object sender, EventArgs e)
        {
            comboBoxZone.DisplayMember = PublicVar.DT_ServerList().Columns[0].ToString();
            comboBoxZone.ValueMember = PublicVar.DT_ServerList().Columns[1].ToString();
            comboBoxZone.DataSource = PublicVar.DT_ServerList();


        }

        private void FormCSDL_VisibleChanged(object sender, EventArgs e)
        {
            //load data grid view
            BindingSource bs_BranchID = new BindingSource();
            bs_BranchID.DataSource = PublicVar.DT_BranchIDs();
            dgvBranchIDs.DataSource = bs_BranchID;
            dgvBranchIDs.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvBranchIDs.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            BindingSource bs_InterestRate = new BindingSource();
            bs_InterestRate.DataSource = PublicVar.DT_TiLeLai();
            dgvTiLeLai.DataSource = bs_InterestRate;
            dgvTiLeLai.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvTiLeLai.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvTiLeLai.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;


            folderBroswerTextBoxSaveImportFileLocation.FolderPath = AddinConfig.SaveLocationSaleInvoiceImportFileVIETSEA;
            folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderPath = AddinConfig.SaveLocation_Import_iHoadon;

            textBox_IP_Oracle.Text = AddinConfig.Oralce_IP;
            textBox_Port_Oracle.Text = AddinConfig.Oralce_Port;

        }

        private async void buttonFastReportVCB_NXT_Click(object sender, EventArgs e)
        {
            if (DataBaseIsNull()) return;

            try
            {
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet ws = wb.GetNewSheet((string)comboBoxZone.GetText());

                string fromDate = dtpFromDate.Value.ToString("yyyyMMdd");
                string toDate = dtpToDate.Value.ToString("yyyyMMdd");

                for (int rowIndex = 0; rowIndex < dgvBranchIDs.Rows.Count - 1; rowIndex++)
                {
                    double startValue = 0;
                    double impValue = 0;
                    double expValue = 0;
                    double endValue = 0;

                    string branchName = dgvBranchIDs.Rows[rowIndex].Cells[0].Value.ToString();
                    string paramBranchID = dgvBranchIDs.Rows[rowIndex].Cells[1].Value.ToString();

                    List<SqlParameter> parameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@FromDate",fromDate),
                        new SqlParameter("@ToDate",toDate),
                        new SqlParameter("@BranchID",paramBranchID)
                    };

                    DataTable dt_sumaryNXT = await ADO.GetDataTableFromStoredProcedureAsync(GetConnectionString(), "[dbo].[System.Stock.SummaryXNT]", parameters);
                    if (dt_sumaryNXT.HasData())
                    {
                        startValue = SumOfSumaryNXT(dt_sumaryNXT, "AMOUNTSTART");
                        endValue = SumOfSumaryNXT(dt_sumaryNXT, "AMOUNTEND");
                        impValue = Convert.ToDouble(dt_sumaryNXT.Compute("SUM(AMOUNTIMP)", string.Empty));
                        expValue = Convert.ToDouble(dt_sumaryNXT.Compute("SUM(AMOUNTEXP)", string.Empty));
                    }

                    ws.NewRangeAtColumn("a").Value2 = branchName;
                    ws.NewRangeAtColumn("b").Value2 = startValue;
                    ws.NewRangeAtColumn("c").Value2 = impValue;
                    ws.NewRangeAtColumn("d").Value2 = expValue;
                    ws.NewRangeAtColumn("e").Value2 = endValue;

                }

                ws.Activate();
                ws.Columns.AutoFit();
                ws.GetRange("b1:e", "e").SetAccountingNumberFormat();
                MsgBox.Show("Thành công!");
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
        }

        private double SumOfSumaryNXT(DataTable datatableSumaryNXT, string columnName)
        {
            double result = Convert.ToDouble(datatableSumaryNXT.Rows[0][columnName]);
            for (int index = 0; index < datatableSumaryNXT.Rows.Count; index++)
            {
                if (index > 0 && datatableSumaryNXT.Rows[index]["StockID"].ToString() != datatableSumaryNXT.Rows[index - 1]["StockID"].ToString())
                {
                    result += Convert.ToDouble(datatableSumaryNXT.Rows[index][columnName]);
                }
            }
            return result;
        }

        private async void buttonFastReportVCB_RevenueGroupByBranch_Click(object sender, EventArgs e)
        {
            if (DataBaseIsNull()) return;

            try
            {
                string fromDate = dtpFromDate.Value.ToString("yyyyMMdd");
                string toDate = dtpToDate.Value.ToString("yyyyMMdd");

                List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@FromDate", fromDate),
                new SqlParameter("@ToDate",  toDate),
                new SqlParameter("@Groupby",  "BRANCH")
            };
                DataTable data = await ADO.GetDataTableFromStoredProcedureAsync(GetConnectionString(), "[dbo].[Reports.Export.RetailSummary]", parameters);
                DataTable data1 = null;
                if (data.HasData())
                {
                    data1 = data.ToDataTableSelectedColumns("MainID", "Name", "TotalExpPriceVAT", "TotalImpPriceVAT", "ProfitSale");
                }
                else
                {
                    MsgBox.Show("Không có dữ liệu!");
                    return;
                }

                //report worksheet
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet ws = wb.GetSheet($"DT {comboBoxZone.GetText()}");
                ws.Cells.Font.Name = "Times New Roman";
                ws.Cells.Font.Size = 14;
                ws.Cells.RowHeight = 25;

                ws.Cells.VerticalAlignment = XlVAlign.xlVAlignCenter;
                ws.Range["a1"].Value2 = "Công ty Cổ Phần Đạt Phát Hà Nội";
                ws.Range["a1"].Font.Bold = true;

                ws.Range["a3"].Value2 = $"BÁO CÁO DOANH THU {((string)comboBoxZone.GetText()).ToUpper()}";
                ws.Range["a3"].Font.Size = 14;
                ws.Range["a3"].Font.Bold = true;
                ws.Range["a3:e3"].MergeAndCenter();
                ws.Range["a4"].Value2 = $"Từ ngày {dtpFromDate.Value.ToString("dd/MM/yyyy")} đến ngày {dtpToDate.Value.ToString("dd/MM/yyyy")}";
                ws.Range["a4:e4"].MergeAndCenter();

                data1.CopyToRange(ws.Range["a6"]);
                ws.Range["a6"].SetHeaderText("Mã ngành hàng", "Tên ngành hàng", "Doanh thu", "Giá vốn", "Lãi");
                ws.Range["a6"].EntireRow.Font.Bold = true;
                ws.Range["a6"].EntireRow.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                ws.NewRangeAtColumn("b").Value2 = "TỔNG CỘNG:";
                ws.LastRange("b").EntireRow.Font.Bold = true;

                ws.NewRangeAtColumn("c").Formula = $"=SUM({ws.GetRange("c7:c", "a").Address[false, false]})";
                ws.NewRangeAtColumn("d").Formula = $"=SUM({ws.GetRange("d7:d", "a").Address[false, false]})";
                ws.NewRangeAtColumn("e").Formula = $"=SUM({ws.GetRange("e7:e", "a").Address[false, false]})";


                ws.GetRange("a6:e", "e").SetBorder();
                ws.GetRange("c6:e", "e").SetAccountingNumberFormat();
                ws.GetRange("c6:e", "a").ConvertToValue();

                int LR2 = ws.LastRow("b") + 2;
                ws.Range[$"a{LR2}"].Value2 = "Người lập biểu";
                ws.Range[$"d{LR2}"].Value2 = "Kế toán trưởng";
                ws.Range[$"a{LR2}"].EntireRow.Font.Bold = true;
                ws.Range[$"a{LR2}:b{LR2}"].MergeAndCenter();
                ws.Range[$"d{LR2}:e{LR2}"].MergeAndCenter();

                ws.Columns.AutoFit();
                ws.Range["a:a"].ColumnWidth = 20;
                ws.Range["c:e"].ColumnWidth = 20;

                Notification.Toast.Show("Thành công!");
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
        }


        private async void buttonExportFileBa_Click(object sender, EventArgs e)
        {
            Range selection = Module1.InputBoxRange("Chọn 1 cột mã hàng");
            if (selection == null) return;

            if (DataBaseIsNull()) return;

            List<string> listGoodID = selection.ValueToList(true);
            DataTable data = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString(), SQLquery.getBarcode(listGoodID));

            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("barcode");

            if (data.HasData())
            {
                data.CopyToRange(ws.Range["a1"]);
            }
            else
            {
                MsgBox.Show("Không có dữ liệu!");
                return;
            }

            ws.Range["j1"].Value2 = "truefalse";
            //ws.Range["k1"].Value2 = "sotien";
            ws.Range["j2"].Formula = "=IF(D2<>D3,1,0)";//xác định barcode cuối cùng
            ws.GetRange("j2:j", "a").FillDownFormula();

            ws.GetRange("a1:j", "a").DeleteRowContainTextCore(10, "=0");
            //ws.Range["k2"].Formula = "=TEXT(VALUE(H2),\"#,##\")& \" VND\"";
            //ws.GetRange("k2:k", "a").FillDownFormula();
            ws.GetRange("i1:i", "a").ConvertToValue();

            Range rng = ws.GetRange("e2:e", "a");
            foreach (Range r in rng)
            {
                r.Value2 = Converter.UnicodetoTCVN_old(r.Value2);
            }

            DataTable emptyBarcodeDatatable = EmptyBarcodeDataTable(ws.LastRange("a").Row - 1);
            System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo("C:\\Barcode\\");
            if (!directory.Exists) directory.Create();
            string fileName = "C:\\Barcode\\Barcode.xls";
            ADO.WriteDataToExcelFile(emptyBarcodeDatatable, fileName);//write 1 data trắng vào file để lấy cấu trúc

            try
            {
                Globals.ThisAddIn.Application.ScreenUpdating = false;
                Workbook wbBarcode = Globals.ThisAddIn.Application.Workbooks.Open(fileName);
                Worksheet sheetTable1 = wbBarcode.GetSheet("Table1");
                ws.GetRange("a2:i", "a").Copy(sheetTable1.Range["a2"]);
                //ws.GetRange("k2:k", "a").Copy(sheetTable1.Range["h2"]);
                wbBarcode.SaveOverWrite(fileName, 56);
                wbBarcode.Close();
            }
            catch { }
            finally { Globals.ThisAddIn.Application.ScreenUpdating = true; }

            MsgBox.Show("Xuất file Barcode.xls thành công!");


        }

        private DataTable EmptyBarcodeDataTable(int rowCount)
        {
            DataTable dataTable = new DataTable("Table1");
            dataTable.Columns.Add("Tensieuthi", typeof(string));
            dataTable.Columns.Add("Mancc", typeof(string));
            dataTable.Columns.Add("Tennganhhang", typeof(string));
            dataTable.Columns.Add("Masieuthi", typeof(string));
            dataTable.Columns.Add("Tenmathang", typeof(string));
            dataTable.Columns.Add("Barcode", typeof(string));
            dataTable.Columns.Add("Soluong", typeof(string));
            dataTable.Columns.Add("Giabanlecovat", typeof(string));
            dataTable.Columns.Add("Thutuin", typeof(int));
            for (int index = 1; index <= rowCount; ++index)
            {
                dataTable.Rows.Add();
            }
            return dataTable;
        }



        private void buttonCalculateGoodsAvailable_Click(object sender, EventArgs e)
        {

            try
            {
                #region MAIN
                if (string.IsNullOrEmpty(comboBoxSheetName2.GetText()))
                {
                    MsgBox.Show("Chưa chọn sheet tồn vietsea!");
                    comboBoxSheetName2.Focus();
                    this.SafeInvoke(() => BringToFront());
                    return;
                }

                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sheetTonKho = wb.GetSheet((string)comboBoxSheetName2.GetText());

                //string header_TonThucTe = "tồn thực tế";
                //string header_TonKhaDung = "tồn khả dụng";
                ((Range)sheetTonKho.Rows[1]).GetHeaderRangeByText(HEADER_TONTHUCTE).SetHeaderText(new string[] { HEADER_TONTHUCTE, HEADER_TONKHADUNG });

                Range tonThucTeHeaderRange = sheetTonKho.GetRangeByText(HEADER_TONTHUCTE);
                Range tonKhaDungHeader = sheetTonKho.GetRangeByText(HEADER_TONKHADUNG);
                Range SOLUONG_header = sheetTonKho.GetRangeByText(XNT_COLUMN_SOLUONG);

                Worksheet sheetStockInfo = wb.GetSheet(SHEETNAME_STOCKINFO);
                //tonThucTeHeaderRange.GetFirstDataRow().Formula = $"=SUMIF({SHEETNAME_STOCKINFO}!B:B,'{sheetTonKho.Name}'!{sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).Offset(ExtensionMethod.xlOffset.Down).Address[false, false]},{SHEETNAME_STOCKINFO}!D:D)";

                Range sumif_range = sheetStockInfo.GetRangeByText(XNT_COLUMN_MAVATTU).EntireColumn;
                Range sumif_criteria = sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).FirstRow();
                Range sumif_sum_range = sheetStockInfo.GetRangeByText("EndQty").EntireColumn;
                tonThucTeHeaderRange.FirstRow().Formula = "=" + ExcelFormula.SUMIF(sumif_range, sumif_criteria, sumif_sum_range);

                tonKhaDungHeader.FirstRow().Formula = $"=MAX({SOLUONG_header.offset(ExtensionMethod.xlOffset.Down).Address[false, false]}-{tonThucTeHeaderRange.offset(ExtensionMethod.xlOffset.Down).Address[false, false]},0)";//0 nghĩa là không có header_TonThucTe, nghĩa là hàng thừa
                sheetTonKho.Range[tonThucTeHeaderRange.offset(ExtensionMethod.xlOffset.Down), tonKhaDungHeader.offset(ExtensionMethod.xlOffset.Down)].ResizeToLastRow("a").FillDownFormula();

                Worksheet sheetAvailable = sheetTonKho.Clone($"{sheetTonKho.Name} {XNT_NAME_TYPE_KHA_DUNG}");
                //sheetTonKho.GetRange("a1:k", "a").CopyFilterData(7, ">0", sheetAvailable.Range["a1"]);
                sheetAvailable.Columns.AutoFit();
                sheetAvailable.GetRangeByText(HEADER_TONKHADUNG).FirstRow().ResizeToLastRow("a").Copy(sheetAvailable.GetRangeByText(XNT_COLUMN_SOLUONG).FirstRow());
                sheetAvailable.UsedRange.DeleteRowContainTextCore(sheetAvailable.GetRangeByText(HEADER_TONKHADUNG).Column, "=0");
                sheetAvailable.GetRangeByText(HEADER_TONKHADUNG).EntireColumn.ClearContents();
                Notification.Toast.Show("Tính tồn kho khả dụng thành công");

                this.SafeInvoke(() => BringToFront());
                #endregion
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
            finally
            {
                this.SafeInvoke(() => BringToFront());
            }
        }

        private async void buttonCheckNegativeStockEveryDay_Click(object sender, EventArgs e)
        {
            try
            {
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                bool hasNegative = false;
                DateTime todate = dtpToDateVietsea.Value;
                Worksheet ws = wb.GetNewSheet($"Hàng âm");

                for (DateTime d = dtpFromDateVietsea.Value; d <= todate; d = d.AddDays(1))
                {
                    Notification.Wait.Show($"Check âm ngày {d.ToString("dd/MM/yyyy")}");
                    string date = d.ToString("ddMMyyyy");
                    DataTable dt = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), SQLquery.GetNegativeStockVIETSEA(date));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string title = $"Ngày {d.ToString("dd-MM-yyyy")}";
                        ws.NewRangeAtColumn("a").Value2 = title;
                        ws.LastRange("a").Font.Bold = true;
                        dt.CopyToRange(ws.NewRangeAtColumn("a"));
                        hasNegative = true;
                    }

                }

                if (hasNegative == false)
                {
                    Notification.Wait.Close();
                    wb.DeleteSheet(ws.Name);
                    MsgBox.Show("Không có hàng âm!");
                }
                else
                {
                    Notification.Wait.Close();
                    ws.Columns.AutoFit();
                    ws.Activate();
                    MsgBox.Show("Check âm xong!");
                }

            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
            finally
            {
                Notification.Wait.Close();
                this.SafeInvoke(() => BringToFront());
            }
        }




        private Worksheet SheetInterestRate(System.Windows.Forms.DataGridView dataGridView, Workbook workbook)
        {
            Worksheet sheet = workbook.GetNewSheet("TiLeLai");
            sheet.Range["a1"].SetHeaderText(new string[] { "loaivattu", "tilelai" });
            foreach (System.Windows.Forms.DataGridViewRow row in dataGridView.Rows)
            {
                if (row.Cells[0].Value != null)
                {
                    string listStock = row.Cells[1].Value.ToString();
                    string[] array = listStock.Split(',');
                    foreach (string s in array)
                    {
                        Range rng = sheet.NewRangeAtColumn("a");
                        rng.NumberFormat = "@";
                        rng.Value2 = s;
                        sheet.NewRangeAtColumn("b").Value2 = row.Cells[2].Value.ToString();
                    }
                }
            }
            return sheet;
        }

        private void buttonExportImportFileRandom_Click(object sender, EventArgs e)
        {
            try
            {
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sheetTonKho = wb.GetSheet((string)comboBox_sheet_tồn.GetText());
                int maxRandom = sheetTonKho.LastRow("a");

                int foundIndex = 1;
                double givenSum = textBox_số_tiền_random.GetText(ValueType.Double);
                for (int i = 2; i < maxRandom; i++)
                {
                    double sum = sheetTonKho.Range["k" + i].GetValue(ValueType.Double);
                    if (sum >= givenSum)
                    {
                        foundIndex = i;
                        break;
                    }
                }

                string soCT = textBox_SoCT.GetText();
                string maKH = textBox_CustomerID.GetText();
                Worksheet sheetImport = VIETSEA.TBNETERP.FileImportXUATBAN(
                                        soCT,
                                        maKH,
                                        sheetTonKho.Range["a2:a" + foundIndex],
                                        sheetTonKho.Range["g2:g" + foundIndex],
                                        sheetTonKho.Range["h2:h" + foundIndex],
                                        customComboBoxVATrate.GetText(ValueType.Int));
                Workbook workbookImport = (Workbook)sheetImport.Parent;
                if (string.IsNullOrEmpty(folderBroswerTextBoxSaveImportFileLocation.FolderPath))
                {
                    folderBroswerTextBoxSaveImportFileLocation.SelectFolder(MyUserControl.FolderBroswerTextBox.EnumFolderType.Folder);
                }
                string savePath = folderBroswerTextBoxSaveImportFileLocation.FolderPath;
                workbookImport.SaveOverWrite($"{savePath}\\Import Random {maKH} - {soCT}.xls", 56);
                workbookImport.Close();

                //tính tồn còn lại
                sheetTonKho.GetRange("j2:j", "a").CopyValueTo(sheetTonKho.Range["c2"]);
                sheetTonKho.GetRange("a1:k", "a").DeleteRowContainTextCore(3, "=0");

                MsgBox.Show("Xuất file import thành công!");
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
            finally
            {
                this.SafeInvoke(() => BringToFront());
            }
        }

        private void dgvTiLeLai_Leave(object sender, EventArgs e)
        {
            Module1.WriteDataGridViewToXML(dgvTiLeLai, PublicVar.xmlTiLeLai);
        }



        private void buttonLocRaMaHangThua_Click(object sender, EventArgs e)
        {
            try
            {
                string tên_kho = comboBoxSheetName2.GetText();
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sheet_hàng_thừa = wb.GetSheet($"{tên_kho} -Th");
                Worksheet sheet_tồn = wb.GetSheet($"{comboBoxSheetName2.GetText()}");
                Worksheet sheet_tồn_nội_bộ = wb.GetSheet(SHEETNAME_STOCKINFO);

                string COUNTIF_HEADER = "countif";
                Range countif_Range = sheet_tồn.EntireRow(1).GetHeaderRangeByText(COUNTIF_HEADER);

                countif_Range.FirstRow().Formula = $"=COUNTIF({SHEETNAME_STOCKINFO}!B:B,'{tên_kho}'!{sheet_tồn.EntireRow(1).GetHeaderRangeByText(XNT_COLUMN_MAVATTU).GetColumnLetter()}2)";
                countif_Range.FirstRow().ResizeToLastRow("a").FillDownFormula(false);
                sheet_tồn.UsedRange.CopyFilterData(COUNTIF_HEADER, "=0", sheet_hàng_thừa.Range["a1"]);
                //sheet_tồn.UsedRange.DeleteRowContainTextCore(6, "=0");
                sheet_hàng_thừa.Columns.AutoFit();
                Notification.Toast.Show("Lọc ra mã hàng thừa thành công!");

            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
            finally
            {
                this.SafeInvoke(() => BringToFront());
            }
        }

        private async void buttonGetDataExportInner_Click(object sender, EventArgs e)
        {
            //try
            //{
            #region MAIN
            if (DataBaseIsNull()) return;
            if (string.IsNullOrEmpty(comboBox_sheet_tồn.GetText()))
            {
                MsgBox.Show("Chưa chọn sheet tồn kho");
                comboBox_sheet_tồn.Focus();
                return;
            }

            if (radioButtonMember.Checked)
            {
                if (string.IsNullOrEmpty(comboBoxCustomersID.GetText()))
                {
                    MsgBox.Show("Chưa chọn mã đơn vị");
                    comboBoxCustomersID.Focus();
                    return;
                }
            }

            if (!System.IO.Directory.Exists(folderBroswerTextBoxSaveImportFileLocation.FolderPath) || string.IsNullOrEmpty(folderBroswerTextBoxSaveImportFileLocation.FolderPath))
            {
                folderBroswerTextBoxSaveImportFileLocation.SelectFolder(MyUserControl.FolderBroswerTextBox.EnumFolderType.Folder, "Chọn folder lưu file import");
                if (string.IsNullOrEmpty(folderBroswerTextBoxSaveImportFileLocation.FolderPath))
                {
                    return;
                }
            }


            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet bangma = await ExcelObject.GetSheetBangMaFromGoogleDriveAsync(wb);
            DataTable dt_export = null;
            if (radioButtonKHACHLE.Checked)
            {
                dt_export = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString(), SQLquery.GetData_Retail_Transcode_03(dtpFromDate.Value, dtpToDate.Value));
            }
            else if (radioButtonMember.Checked)
            {
                dt_export = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString(), SQLquery.GetData_ExportInner_Transcode_23(dtpFromDate.Value, dtpToDate.Value, comboBoxCustomersID.GetText()));
            }

            Worksheet sheet_data_export = wb.GetSheet("data_export");
            if (dt_export.HasData())
            {
                dt_export.CopyToRange(sheet_data_export.Range["a1"]);
                sheet_data_export.GetRangeByText("SumQuantity").ResizeToLastRow(sheet_data_export.GetRangeByText("GoodID").GetColumnLetter()).ConvertToValue();
                Module1.VlookupMAVIETSEA(sheet_data_export.GetRangeByText("GoodID").ResizeToLastRow(), sheet_data_export.GetRangeByText(XNT_COLUMN_MAVATTU).FirstRow(), bangma);

                Worksheet sheetTonKho = wb.GetSheet((string)comboBox_sheet_tồn.GetText());
                PublicVar.last_KhoHang = sheetTonKho.Range["a2"].Value2;


                sheet_data_export.GetRange("a1:c", "a").DeleteRowContainText(XNT_COLUMN_MAVATTU, "#N/A");
                string EXPORT_header = "EXPORT";
                Range EXPORT_headerRange = (sheetTonKho.Rows[1] as Range).GetHeaderRangeByText(EXPORT_header);

                //EXPORT_headerRange.GetFirstDataRow().Formula = $"=IFERROR(MIN(VLOOKUP({sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).GetFirstDataRow().Address[false, false]},data_export!A:C,3,0),{sheetTonKho.GetRangeByText(XNT_COLUMN_SOLUONG).GetFirstDataRow().Address[false, false]}),MIN({sheetTonKho.GetRangeByText(XNT_COLUMN_SOLUONG).GetFirstDataRow().Address[false, false]},RANDBETWEEN(1,10)))";
                Range loolupValue = sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).FirstRow();
                Range tableArray = sheet_data_export.Range["a1", "c1"].EntireColumn;
                EXPORT_headerRange.FirstRow().Formula = $"=MIN({ExcelFormula.VLOOKUP(loolupValue, tableArray, 3, 0, 0)},{sheetTonKho.GetRangeByText(XNT_COLUMN_SOLUONG).FirstRow().Address[false, false]})";
                EXPORT_headerRange.FirstRow().ResizeToLastRow(sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).GetColumnLetter()).FillDownFormula(true);


                string RANDOM_header = "RANDOM";
                Range RANDOM_headerRange = (sheetTonKho.Rows[1] as Range).GetHeaderRangeByText(RANDOM_header);
                RANDOM_headerRange.FirstRow().Formula = $"=IF({EXPORT_headerRange.FirstRow().Address[false, false]}>0,0,MIN({sheetTonKho.GetRangeByText(XNT_COLUMN_SOLUONG).FirstRow().Address[false, false]},RANDBETWEEN(1,10)))";
                RANDOM_headerRange.FirstRow().ResizeToLastRow(sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).GetColumnLetter()).FillDownFormula(true);

                string XBAN_header = "XBAN";
                Range XBAN_headerRange = (sheetTonKho.Rows[1] as Range).GetHeaderRangeByText(XBAN_header);
                XBAN_headerRange.FirstRow().Formula = $"={EXPORT_headerRange.FirstRow().Address[false, false]}+{RANDOM_headerRange.FirstRow().Address[false, false]}";
                XBAN_headerRange.FirstRow().ResizeToLastRow(sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).GetColumnLetter()).FillDownFormula(true);

                ApplyExpPrice(wb, sheetTonKho, sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).FirstRow().ResizeToLastRow(), customComboBoxVATrate.GetText(ValueType.Int), ((Range)sheetTonKho.Rows[1]).GetHeaderRangeByText("GIABAN").FirstRow(), checkBox_RoundExpPrice.Checked);

                Range amountHeaderRange = (sheetTonKho.Rows[1] as Range).GetHeaderRangeByText("THANHTIEN VAT");
                amountHeaderRange.FirstRow().Formula = $"=({XBAN_headerRange.FirstRow().Address[false, false]})*{sheetTonKho.GetRangeByText("GIABAN").FirstRow().Address[false, false]}*(1+{customComboBoxVATrate.GetText()}/100)";
                amountHeaderRange.FirstRow().ResizeToLastRow(sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).GetColumnLetter()).FillDownFormula(false);

                Range LUYKE_headerRange = (sheetTonKho.Rows[1] as Range).GetHeaderRangeByText("LUYKE");
                LUYKE_headerRange.FirstRow().Formula = $"=SUM({amountHeaderRange.FirstRow().Address[true, true]}:{amountHeaderRange.FirstRow().Address[false, false]})";
                LUYKE_headerRange.FirstRow().ResizeToLastRow(sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).GetColumnLetter()).FillDownFormula(false);

                string RANDOM_SORT_ORDER_header = "sortOder";
                Range RANDOM_SORT_ORDER_range = (sheetTonKho.Rows[1] as Range).GetHeaderRangeByText(RANDOM_SORT_ORDER_header);
                RANDOM_SORT_ORDER_range.FirstRow().Formula = "=RAND()";
                RANDOM_SORT_ORDER_range.FirstRow().ResizeToLastRow(sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).GetColumnLetter()).FillDownFormula(true);


                sheetTonKho.UsedRange.SetAccountingNumberFormat();
                sheetTonKho.Columns.AutoFit();
                sheetTonKho.Activate();

                sheetTonKho.UsedRange.Sort1(sheetTonKho.GetRangeByText(EXPORT_header), XlSortOrder.xlDescending);
                //sheetTonKho.UsedRange.Sort2(sheetTonKho.GetRangeByText(RANDOM_header), XlSortOrder.xlDescending);
                sheetTonKho.UsedRange.Sort2(RANDOM_SORT_ORDER_range, XlSortOrder.xlDescending);


                //create import
                string saveLocation = folderBroswerTextBoxSaveImportFileLocation.FolderPath;

                int sheetTonKhoRowCount = sheetTonKho.LastRow("a") - 1;
                int maxRowEnd = Math.Min(new Random().Next(900, 1200), sheetTonKhoRowCount);

                int rowEnd = 0;
                if (!string.IsNullOrEmpty(textBox_số_tiền_random.GetText()))
                {
                    rowEnd = 2;
                    int givenAmountVAT = (int)textBox_số_tiền_random.GetText(ValueType.Int);
                    int LR = sheetTonKho.LastRow("a");
                    string LUYKEcolname = sheetTonKho.GetRangeByText("LUYKE").GetColumnLetter();

                    for (int i = 2; i <= LR; i++)
                    {
                        if (sheetTonKho.Range[LUYKEcolname + i].Value2 > givenAmountVAT)
                        {
                            rowEnd = i;
                            break;
                        }
                    }

                    if (rowEnd > maxRowEnd)//nếu số lượng hàng nhiều quá mới đạt đủ tiền mong muốn thì thêm 1 vào mỗi mặt hàng đã có
                    {
                        //int fixDesireAmount = sheetTonKho.Range[LUYKEcolname + maxRowEnd].Value2;
                        string XBANcolname = sheetTonKho.GetRangeByText("XBAN").GetColumnLetter();
                        string TONcolname = sheetTonKho.GetRangeByText(XNT_COLUMN_SOLUONG).GetColumnLetter();

                    startagain:
                        for (int j = 2; j <= maxRowEnd; j++)
                        {
                            if (sheetTonKho.Range[XBANcolname + j].Value2 < sheetTonKho.Range[TONcolname + j].Value2)
                            {
                                //Globals.ThisAddIn.Application.Goto(sheetTonKho.Range[XBANcolname + j]);
                                sheetTonKho.Range[XBANcolname + j].Value2 += 1;
                            }

                            System.Windows.Forms.Application.DoEvents();
                            if (sheetTonKho.Range[LUYKEcolname + maxRowEnd].Value2 > givenAmountVAT)
                            {
                                rowEnd = maxRowEnd;
                                break;
                            }
                        }

                        if (sheetTonKho.Range[LUYKEcolname + maxRowEnd].Value2 < givenAmountVAT)
                        {
                            goto startagain;
                        }

                    }

                }

                if (string.IsNullOrEmpty(textBox_số_tiền_random.GetText()))
                {
                    rowEnd = sheetTonKho.GetRangeByText(EXPORT_header).EntireColumn.Find(What: 0, LookAt: XlLookAt.xlWhole).Row - 1;
                }

                string amount = sheetTonKho.Range[sheetTonKho.GetRangeByText("LUYKE").GetColumnLetter() + rowEnd].Text;
                VIETSEA.TBNETERP.Create_workbook_import_XuatBan(saveLocation, textBox_SoCT.GetText(), textBox_CustomerID.GetText(), sheetTonKho.GetRangeByText(XNT_COLUMN_MAVATTU).FirstRow().ResizeToRow(rowEnd), XBAN_headerRange.FirstRow().ResizeToRow(rowEnd), sheetTonKho.GetRangeByText("GIABAN").FirstRow().ResizeToRow(rowEnd), customComboBoxVATrate.GetText(ValueType.Int));
                MsgBox.Show("Tạo file import thành công!"
                    + Environment.NewLine + "Số mã hàng import: " + (rowEnd - 1)
                    + Environment.NewLine + "Số tiền: " + amount);

            }
            #endregion

            this.SafeInvoke(() => BringToFront());

            //}
            //catch (Exception ex)
            //{
            //    MsgBox.Show(ex.Message);
            //}
            //finally
            //{
            //}
        }

        /// <summary>
        /// Giá bán mà không vlookup được sẽ trả về 0
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="sheetTonKho"></param>
        /// <param name="vietseaProductCode"></param>
        /// <param name="VAT"></param>
        /// <param name="RangeGIABAN"></param>
        private void ApplyExpPrice(Workbook workbook, Worksheet sheetTonKho, Range vietseaProductCode, int VAT, Range RangeGIABAN, bool lamTron)
        {
            Worksheet sheetInterestRate = SheetInterestRate(dgvTiLeLai, workbook);
            Range destination = RangeGIABAN.Cells[1, 1];
            string lookupValue = ((Range)vietseaProductCode.Cells[1, 1]).Address[false, false];

            string firstColumnLookupTable = sheetTonKho.Range["a1"].EntireRow.GetHeaderRangeByText("MAVATTU").GetColumnLetter();
            string columnGIAVON = sheetTonKho.Range["a1"].EntireRow.GetHeaderRangeByText("GIAVON").GetColumnLetter();
            string tableArray = $"'{sheetTonKho.Name}'!{firstColumnLookupTable}:{columnGIAVON}";
            int GIAVON_col_index_num = sheetTonKho.Range["a1"].EntireRow.GetHeaderRangeByText("GIAVON").Column - sheetTonKho.Range["a1"].EntireRow.GetHeaderRangeByText("MAVATTU").Column + 1;

            //string giaVon = $"(VLOOKUP({lookupValue},{tableArray},{GIAVON_col_index_num},0))";
            int firstRow = destination.Row;
            string giaVon = $"({columnGIAVON + firstRow})";

            string tiLeLai = $"(1+IFERROR(VLOOKUP(LEFT({lookupValue},1),TiLeLai!A:B,2,0),10)/100)";
            string giaBanChuaThue_chuaLamTron = $"({giaVon}*{tiLeLai})";
            string thue = $"(1+{VAT}/100)";
            //string giaBanCoThue = $"({giaBanChuaThue1}*{thue})";
            string add = $"(IF({giaVon}<20000,50,250))";//add là để nâng giá trị lên, để khi làm tròn thì sẽ làm tròn lên ra xa số 0
            string multiple = $"({add}*2)";// x2
            string giaBanCoThue = $"({giaBanChuaThue_chuaLamTron}*{thue}+{add})";
            string giaBanCoThueLamTron = $"(MROUND({giaBanCoThue},{multiple}))";
            string giaBanChuaThueTinhNguocLai = $"({giaBanCoThueLamTron}/{thue})";

            if (lamTron)
            {
                destination.Formula = $"=IFERROR({giaBanChuaThueTinhNguocLai},0)";
            }
            else
            {
                destination.Formula = $"=IFERROR({giaBanChuaThue_chuaLamTron},0)";
            }

            //old
            //cell1.Formula = $"=IFERROR(MROUND(VLOOKUP(A2,'{sheetTonKho.Name}'!A:D,4,0)*(1+IFERROR(VLOOKUP(LEFT(A2,1),TiLeLai!A:B,2,0),10)/100)*(1+{VAT}/100),CHOOSE(RANDBETWEEN(1,2),1000,500))/(1+{VAT}/100),0)";

            destination.Resize[vietseaProductCode.Rows.Count, 1].FillDownFormula(false);
        }

        private async void buttonExecute_Click(object sender, EventArgs e)
        {
            try
            {
                string q = textBoxOracleQuery.GetText();
                DataTable dt = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), q);
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sh = wb.GetSheet("result");
                int numberOfRecords = textBoxNumberOfRowToGet.GetText(ValueType.Int);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows.Count > numberOfRecords)
                    {
                        DataTable dt_clone = dt.Clone();
                        for (int i = 0; i < numberOfRecords; i++)
                        {
                            dt_clone.ImportRow(dt.Rows[i]);
                        }
                        dt_clone.CopyToRange(sh.Range["a1"]);
                    }
                    else
                    {
                        dt.CopyToRange(sh.Range["a1"]);
                    }

                }
                else
                {
                    MsgBox.Show("Không có dữ liệu");
                    this.SafeInvoke(() => BringToFront());
                }



            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
                this.SafeInvoke(() => BringToFront());
            }
        }

        private async void comboBoxCustomersID_DropDown(object sender, EventArgs e)
        {
            try
            {
                DataTable data = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString(), "select customerid from customers");
                if (data.Rows.Count > 0)
                {
                    comboBoxCustomersID.SafeInvoke(() =>
                    {
                        comboBoxCustomersID.DataSource = data;
                        comboBoxCustomersID.DisplayMember = "customerid";
                    });

                }
            }
            catch (Exception)
            {

            }


        }

        private async void button_Load_XBAN_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt_XBAN_list = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), SQLquery.Oracle_Get_XBAN(dtpFromDateVietsea.Value, dtpToDateVietsea.Value));
                if (dt_XBAN_list != null && dt_XBAN_list.Rows.Count > 0)
                {
                    gridControl_EinvoiceList.SafeBeginInvoke(() =>
                    {
                        gridControl_EinvoiceList.DataSource = dt_XBAN_list;
                        gridView_EinvoiceList.Columns["Ngày tháng"].SetNumberFormat(DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy");
                        gridView_EinvoiceList.Columns["Số tiền"].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                        gridView_EinvoiceList.Columns["MAVATTUGDPK"].Visible = false;
                        gridView_EinvoiceList.Columns["Mã KH"].Visible = false;
                        gridView_EinvoiceList.OptionsView.ShowGroupPanel = false;
                        gridView_EinvoiceList.SetBestFitColumns(0, "Số hóa đơn", "Ngày tháng", "Số tiền");
                    });

                }
                else
                {
                    MsgBox.Show("Không có dữ liệu!");
                    return;
                }
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message + ex.StackTrace);
            }
            finally
            {
                this.SafeInvoke(() => { BringToFront(); });
            }


        }

        private async void button_CreateFileImport_Einvoice_Click(object sender, EventArgs e)
        {
            try
            {
                int[] selectedRows = gridView_EinvoiceList.GetSelectedRows();
                if (selectedRows.Length == 0) return;

                if (string.IsNullOrEmpty(folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderPath))
                {
                    folderBroswerTextBox_SaveLocation_import_iHoaDon.SelectFolder(MyUserControl.FolderBroswerTextBox.EnumFolderType.Folder);
                }
                string saveLocation = folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderPath;

                //if (gridView2.GetSelectedRows().Length == 0)
                //{
                //    MsgBox.Show("Chưa chọn phiếu, vui lòng tích chọn phiếu và thử lại!");
                //    this.SafeInvoke(() => { BringToFront(); });
                //    return;
                //}

                Module1.SpeedUpCode(true);
                foreach (var row in selectedRows)
                {
                    string sohoadon = gridView_EinvoiceList.GetRowCellValue(row, "Số hóa đơn").ToString();
                    string MAVATTUGDPK = gridView_EinvoiceList.GetRowCellValue(row, "MAVATTUGDPK").ToString();
                    DataTable dt_detail = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), SQLquery.Oracle_Get_XBAN_Detail(MAVATTUGDPK));
                    if (dt_detail != null && dt_detail.Rows.Count > 0)
                    {
                        HoaDonDienTu.Create_workbook_import_iHoaDon(dt_detail, "iHoaDon " + sohoadon, saveLocation);
                    }
                }
                Module1.SpeedUpCode(false);
                Notification.Toast.Show("Kết xuất file thành công!");

            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
            finally
            {
                Module1.SpeedUpCode(false);
                this.SafeInvoke(() => { BringToFront(); });
            }


        }

        private void folderBroswerTextBox_SaveLocation_import_iHoaDon_Leave(object sender, EventArgs e)
        {
            //Properties.Settings.Default.SaveLocation_file_import_iHoaDon = folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderPath;
            //Properties.Settings.Default.Save();

            AddinConfig.SaveLocation_Import_iHoadon = folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderPath;
        }

        private void folderBroswerTextBoxSaveImportFileLocation_Leave(object sender, EventArgs e)
        {
            //Properties.Settings.Default.FolderSaveImport = folderBroswerTextBoxSaveImportFileLocation.FolderPath;
            //Properties.Settings.Default.Save();

            AddinConfig.SaveLocationSaleInvoiceImportFileVIETSEA = folderBroswerTextBoxSaveImportFileLocation.FolderPath;
        }

        private async void button_download_PDF_einvoice_by_selenium_Click(object sender, EventArgs e)
        {
            string download_directory = folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderPath;

            //#region Get invoice number
            //var startNumber = 0;
            //var endNumber = 0;
            //try
            //{
            //    startNumber = (int)Globals.ThisAddIn.Application.InputBox("Số hóa đơn đầu:", Type: 1);
            //    endNumber = (int)Globals.ThisAddIn.Application.InputBox("Số hóa đơn cuối:", Type: 1);
            //}
            //catch
            //{
            //    this.SafeInvoke(() => BringToFront());
            //    return;
            //}

            //List<string> listNumber = new List<string>();

            //for (int i = startNumber; i <= endNumber; i++)
            //{
            //    listNumber.Add(Convert.ToString(i).PadLeft(7, '0'));
            //}
            //#endregion

            //try
            //{
            //    await Task_download_invoice_pdf(listNumber, download_directory);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            //this.SafeInvoke(() => BringToFront());


            //v2
            OpenQA.Selenium.Chrome.ChromeOptions chromeOptions = new OpenQA.Selenium.Chrome.ChromeOptions();
            chromeOptions.AddUserProfilePreference("download.default_directory", PublicVar.Download_PDF_iHoaDon());
            chromeOptions.AddUserProfilePreference("plugins.always_open_pdf_externally", true);
            chromeOptions.AddArgument("--no-sandbox");
            chromeOptions.AddArgument("--remote-debugging-port=9222");
            SeleniumBrowser browser = new SeleniumBrowser(TimeSpan.FromMinutes(20), chromeOptions);

            string mainWindow = browser.ChromeDriverCore.CurrentWindowHandle;

            await Selenium.SignIn_IHoaDon(browser);
            await browser.SmartClick(iHoaDon.button_LapHoaDon);

            //lọc ngày tháng
            string fromDate = dtpFromDateVietsea.Value.ToString("dd/MM/yyyy");
            string toDate = dtpToDateVietsea.Value.ToString("dd/MM/yyyy");
            string xpath_tuNgay = "/html/body/app-root/app-system/div/main/div/app-index/div/div[1]/app-search-advanced/div[1]/div[2]/div/div[2]/dx-date-box/div/div/div[1]/input";
            string xpath_denNgay = "/html/body/app-root/app-system/div/main/div/app-index/div/div[1]/app-search-advanced/div[1]/div[3]/div/div[2]/dx-date-box/div/div/div[1]/input";
            await browser.SendKeys(xpath_tuNgay, fromDate);
            await browser.SendKeys(xpath_denNgay, toDate);
            string xpath_timkiem = "/html/body/app-root/app-system/div/main/div/app-index/div/div[1]/app-search-advanced/div[2]/div[1]/div/div[2]/dx-text-box/div/div[1]/input";
            await browser.SmartClick(xpath_timkiem);

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            List<string> invoiceNumberList = new List<string>();

            while (true) //get invoice numbers 
            {
                doc.LoadHtml(browser.GetWebElement("/html").GetAttribute("outerHTML"));

                var allRows = doc.DocumentNode.SelectNodes("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr");
                foreach (var row in allRows)
                {
                    HtmlAgilityPack.HtmlDocument doc_cells = new HtmlAgilityPack.HtmlDocument();
                    doc_cells.LoadHtml(row.OuterHtml);
                    var cells = doc_cells.DocumentNode.SelectNodes("//td[2]");
                    foreach (var cell in cells)
                    {
                        string cellValue = cell.InnerText.Left(7);//hóa đơn đã chuyển đổi sẽ là: 0001900 - CĐ(1)
                        invoiceNumberList.Add(cellValue);
                    }

                }

                //click next page
                doc.LoadHtml(browser.GetWebElement("/html").GetAttribute("outerHTML"));
                var pages = doc.DocumentNode.SelectNodes("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[11]/div[2]/div");
                var lastPage = pages.Last();

                if (lastPage.GetAttributeValue("class", "") == "dx-navigate-button dx-next-button dx-button-disable")
                {
                    break;
                }
                else
                {
                    string xpath_lastPage = lastPage.XPath;
                    await browser.SmartClick(xpath_lastPage);
                }

            }

            foreach (var invoiceNumber in invoiceNumberList)
            {
                browser.GetWebElement("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input").Clear();
                await browser.SendKeys("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input", invoiceNumber);//điền số hóa đơn

                //đợi số hóa đơn nó nhảy về đúng số mình cần
                while (true)
                {
                    string invoiceNumberFromWeb = browser.GetWebElement("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[2]/div/a").Text;
                    if (invoiceNumberFromWeb.Contains(invoiceNumber))
                    {
                        break;//contains vì hóa đơn đã chuyển đổi sẽ là: 0001900 - CĐ(1)
                    }
                    await browser.WaitDOMchange();
                }

                //tăng chiều cao cho cái grid lên 400px để khi bấm mở rộng thì nó vẫn hiện đầy đủ các mục, thì mới click được
                browser.ChromeDriverCore.ExecuteScript("document.getElementById(\"gridContainerVat\").style.height = \"400px\";");

                //click mở rộng
                await browser.SmartClick("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[9]/div/div/a");

                //click in chuyển đổi
                OpenQA.Selenium.IWebElement element = browser.GetWebElement("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[9]/div/div/div/a[2]");
                OpenQA.Selenium.Support.UI.PopupWindowFinder finder = new OpenQA.Selenium.Support.UI.PopupWindowFinder(browser.ChromeDriverCore, System.TimeSpan.FromMinutes(1), System.TimeSpan.FromMilliseconds(500));
                string popupWindow = finder.Click(element);

                //wait for download complete
                string[] pdfFiles = null;
                while (true)
                {
                    pdfFiles = System.IO.Directory.GetFiles(PublicVar.Download_PDF_iHoaDon(), "*.pdf");
                    if (pdfFiles.Length == 1) break;
                    await Task.Delay(1000);
                }

                string pdf_file_name = pdfFiles[0];
                string newFilename = string.Format(@"{0}\{1}.pdf", download_directory, invoiceNumber);
                if (System.IO.File.Exists(newFilename))
                {
                    System.IO.File.Delete(newFilename);
                }
                System.IO.File.Move(pdf_file_name, newFilename);

                browser.ChromeDriverCore.SwitchTo().Window(popupWindow).Close();
                browser.ChromeDriverCore.SwitchTo().Window(mainWindow);

            }

            MsgBox.Show("Tải file PDF hóa đơn điện tử thành công!");
            browser.CloseAndQuit();


        }

        private void Download_PDF_invoice(List<string> listInvoiceNumber, string download_directory)
        {
            _ = new WebDriverManager.DriverManager().SetUpDriver(new WebDriverManager.DriverConfigs.Impl.ChromeConfig());
            OpenQA.Selenium.Chrome.ChromeOptions chromeOptions = new OpenQA.Selenium.Chrome.ChromeOptions();
            chromeOptions.AddArgument("--start-maximized");
            chromeOptions.AddUserProfilePreference("download.default_directory", PublicVar.Download_PDF_iHoaDon());
            //chromeOptions.AddUserProfilePreference("download.promt_for_download", false);
            //chromeOptions.AddUserProfilePreference("download.directory_upgrade", true) ;
            //chromeOptions.AddUserProfilePreference("plugins.plugins_disabled", "Chrome PDF Viewer");
            chromeOptions.AddUserProfilePreference("plugins.always_open_pdf_externally", true);
            chromeOptions.AddArgument("--no-sandbox");
            chromeOptions.AddArgument("--remote-debugging-port=9222");
            //chromeOptions.AddArgument("--headless");

            OpenQA.Selenium.Chrome.ChromeDriver chromeDriver = new OpenQA.Selenium.Chrome.ChromeDriver(chromeOptions);

            OpenQA.Selenium.Support.UI.WebDriverWait wait = new OpenQA.Selenium.Support.UI.WebDriverWait(chromeDriver, System.TimeSpan.FromMinutes(15));
            string mainWindow = chromeDriver.CurrentWindowHandle;

            chromeDriver.Url = "https://ihoadon.com.vn/login";
            chromeDriver.Navigate();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[1]/div/div/input"))).SendKeys("0106188175");//mst
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[2]/div/div/input"))).SendKeys("0106188175");//tên đăng nhập
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[3]/div/div/input"))).SendKeys("thanhcong86");//pass
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[6]/div[1]/button/i"))).Click();//button đăng nhập
            Task.Delay(4000).Wait();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-home/div[1]/div[2]/div/div[1]/div[2]/div[2]/a/article")));//button lập hóa đơn
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(OpenQA.Selenium.By.XPath("/html/body/app-root/app-home/div[1]/div[2]/div/div[1]/div[2]/div[2]/a/article"))).Click();//button lập hóa đơn              
            Task.Delay(4000).Wait();

            foreach (string invoiceNumber in listInvoiceNumber)
            {
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"))).Clear();
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"))).SendKeys(invoiceNumber);//điền số hóa đơn
                                                                                                                                                                                                                                                                                       //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[4]/div/div[2]/div/div/div/div[1]/input"))).Clear();                                                                                                                                                                                                                                                                                   //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[4]/div/div[2]/div/div/div/div[1]/input"))).SendKeys(ngayHoaDon.Replace("/", string.Empty));//điền ngày hóa đơn
                Task.Delay(5000).Wait();

                //tăng chiều cao cho cái grid
                chromeDriver.ExecuteScript("document.getElementById(\"gridContainerVat\").style.height = \"400px\";");

                //click mở rộng
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[9]/div/div/a"))).Click();

                //click in chuyển đổi
                OpenQA.Selenium.IWebElement element = chromeDriver.FindElement(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[9]/div/div/div/a[2]"));
                OpenQA.Selenium.Support.UI.PopupWindowFinder finder = new OpenQA.Selenium.Support.UI.PopupWindowFinder(chromeDriver, System.TimeSpan.FromMinutes(1), System.TimeSpan.FromMilliseconds(500));
                string popupWindow = finder.Click(element);

                //wait for download complete
                string[] pdfFiles = null;
                while (true)
                {
                    pdfFiles = System.IO.Directory.GetFiles(PublicVar.Download_PDF_iHoaDon(), "*.pdf");
                    if (pdfFiles.Length == 1) break;
                    Task.Delay(1000).Wait();
                }

                string pdf_file_name = pdfFiles[0];
                string newFilename = string.Format(@"{0}\{1}.pdf", download_directory, invoiceNumber);
                if (System.IO.File.Exists(newFilename))
                {
                    System.IO.File.Delete(newFilename);
                }
                System.IO.File.Move(pdf_file_name, newFilename);

                chromeDriver.SwitchTo().Window(popupWindow).Close();
                chromeDriver.SwitchTo().Window(mainWindow);
            }

            chromeDriver.Close();
            chromeDriver.Quit();
        }

        private async Task Task_download_invoice_pdf(List<string> listInvoiceNumber, string download_directory)
        {
            await System.Threading.Tasks.Task.Run(async () =>
            {
                _ = new WebDriverManager.DriverManager().SetUpDriver(new WebDriverManager.DriverConfigs.Impl.ChromeConfig());
                OpenQA.Selenium.Chrome.ChromeOptions chromeOptions = new OpenQA.Selenium.Chrome.ChromeOptions();
                chromeOptions.AddArgument("--start-maximized");
                chromeOptions.AddUserProfilePreference("download.default_directory", PublicVar.Download_PDF_iHoaDon());
                chromeOptions.AddUserProfilePreference("plugins.always_open_pdf_externally", true);
                chromeOptions.AddArgument("--no-sandbox");
                chromeOptions.AddArgument("--remote-debugging-port=9222");

                OpenQA.Selenium.Chrome.ChromeDriver chromeDriver = new OpenQA.Selenium.Chrome.ChromeDriver(chromeOptions);

                OpenQA.Selenium.Support.UI.WebDriverWait wait = new OpenQA.Selenium.Support.UI.WebDriverWait(chromeDriver, System.TimeSpan.FromMinutes(15));
                string mainWindow = chromeDriver.CurrentWindowHandle;

                try
                {
                    chromeDriver.Url = "https://ihoadon.com.vn/login";
                    chromeDriver.Navigate();

                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[1]/div/div/input"))).SendKeys("0106188175");//mst
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[2]/div/div/input"))).SendKeys("0106188175");//tên đăng nhập
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[3]/div/div/input"))).SendKeys("thanhcong86");//pass
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[6]/div[1]/button/i"))).Click();//button đăng nhập
                    await Task.Delay(5000);

                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-home/div[1]/div[2]/div/div[1]/div[2]/div[2]/a/article")));//button lập hóa đơn
                    await Task.Delay(5000);
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(OpenQA.Selenium.By.XPath("/html/body/app-root/app-home/div[1]/div[2]/div/div[1]/div[2]/div[2]/a/article"))).Click();//button lập hóa đơn              
                    await Task.Delay(5000);

                    foreach (string invoiceNumber in listInvoiceNumber)
                    {
                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"))).Clear();
                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"))).SendKeys(invoiceNumber);//điền số hóa đơn
                        await Task.Delay(7000);

                        //đợi số hóa đơn nó nhảy về đúng số mình cần
                        while (true)
                        {
                            string invoiceNumberFromWeb = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[2]/div/a"))).Text;
                            //MessageBox.Show(invoiceNumberFromWeb);
                            if (invoiceNumberFromWeb.Contains(invoiceNumber)) break;//contains vì hóa đơn đã chuyển đổi sẽ là: 0001900 - CĐ(1)
                            await Task.Delay(5000);
                        }

                        //tăng chiều cao cho cái grid lên 400px để khi bấm mở rộng thì nó vẫn hiện đầy đủ các mục, thì mới click được
                        chromeDriver.ExecuteScript("document.getElementById(\"gridContainerVat\").style.height = \"400px\";");

                        //click mở rộng
                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[9]/div/div/a"))).Click();
                        await Task.Delay(1000);

                        //click in chuyển đổi
                        OpenQA.Selenium.IWebElement element = chromeDriver.FindElement(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[9]/div/div/div/a[2]"));
                        OpenQA.Selenium.Support.UI.PopupWindowFinder finder = new OpenQA.Selenium.Support.UI.PopupWindowFinder(chromeDriver, System.TimeSpan.FromMinutes(1), System.TimeSpan.FromMilliseconds(500));
                        string popupWindow = finder.Click(element);

                        //wait for download complete
                        string[] pdfFiles = null;
                        while (true)
                        {
                            pdfFiles = System.IO.Directory.GetFiles(PublicVar.Download_PDF_iHoaDon(), "*.pdf");
                            if (pdfFiles.Length == 1) break;
                            await Task.Delay(1000);
                        }

                        string pdf_file_name = pdfFiles[0];
                        string newFilename = string.Format(@"{0}\{1}.pdf", download_directory, invoiceNumber);
                        if (System.IO.File.Exists(newFilename))
                        {
                            System.IO.File.Delete(newFilename);
                        }
                        System.IO.File.Move(pdf_file_name, newFilename);

                        chromeDriver.SwitchTo().Window(popupWindow).Close();
                        chromeDriver.SwitchTo().Window(mainWindow);
                    }

                    chromeDriver.Close();
                    chromeDriver.Quit();
                    MessageBox.Show("Tải file PDF hóa đơn-điện tử-chuyển đổi thành công!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    chromeDriver.Close();
                    chromeDriver.Quit();
                }
            });
        }

        private async Task Download_invoice_pdf_v2(List<string> listInvoiceNumber, string download_directory)
        {
            await System.Threading.Tasks.Task.Run(async () =>
            {
                _ = new WebDriverManager.DriverManager().SetUpDriver(new WebDriverManager.DriverConfigs.Impl.ChromeConfig());
                OpenQA.Selenium.Chrome.ChromeOptions chromeOptions = new OpenQA.Selenium.Chrome.ChromeOptions();
                chromeOptions.AddArgument("--start-maximized");
                chromeOptions.AddUserProfilePreference("download.default_directory", PublicVar.Download_PDF_iHoaDon());
                chromeOptions.AddUserProfilePreference("plugins.always_open_pdf_externally", true);
                chromeOptions.AddArgument("--no-sandbox");
                chromeOptions.AddArgument("--remote-debugging-port=9222");

                OpenQA.Selenium.Chrome.ChromeDriver chromeDriver = new OpenQA.Selenium.Chrome.ChromeDriver(chromeOptions);

                OpenQA.Selenium.Support.UI.WebDriverWait wait = new OpenQA.Selenium.Support.UI.WebDriverWait(chromeDriver, System.TimeSpan.FromMinutes(15));
                string mainWindow = chromeDriver.CurrentWindowHandle;

                try
                {
                    chromeDriver.Url = "https://ihoadon.com.vn/login";
                    chromeDriver.Navigate();

                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[1]/div/div/input"))).SendKeys("0106188175");//mst
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[2]/div/div/input"))).SendKeys("0106188175");//tên đăng nhập
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[3]/div/div/input"))).SendKeys("thanhcong86");//pass
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-login/div/div[1]/div/div[1]/div[2]/form/div[6]/div[1]/button/i"))).Click();//button đăng nhập
                    await Task.Delay(5000);

                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-home/div[1]/div[2]/div/div[1]/div[2]/div[2]/a/article")));//button lập hóa đơn
                    await Task.Delay(5000);
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(OpenQA.Selenium.By.XPath("/html/body/app-root/app-home/div[1]/div[2]/div/div[1]/div[2]/div[2]/a/article"))).Click();//button lập hóa đơn              
                    await Task.Delay(5000);

                    foreach (string invoiceNumber in listInvoiceNumber)
                    {
                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"))).Clear();
                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"))).SendKeys(invoiceNumber);//điền số hóa đơn
                        await Task.Delay(7000);

                        //đợi số hóa đơn nó nhảy về đúng số mình cần
                        while (true)
                        {
                            string invoiceNumberFromWeb = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[2]/div/a"))).Text;
                            //MessageBox.Show(invoiceNumberFromWeb);
                            if (invoiceNumberFromWeb.Contains(invoiceNumber)) break;//contains vì hóa đơn đã chuyển đổi sẽ là: 0001900 - CĐ(1)
                            await Task.Delay(5000);
                        }

                        //tăng chiều cao cho cái grid lên 400px để khi bấm mở rộng thì nó vẫn hiện đầy đủ các mục, thì mới click được
                        chromeDriver.ExecuteScript("document.getElementById(\"gridContainerVat\").style.height = \"400px\";");

                        //click mở rộng
                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[9]/div/div/a"))).Click();
                        await Task.Delay(1000);

                        //click in chuyển đổi
                        OpenQA.Selenium.IWebElement element = chromeDriver.FindElement(OpenQA.Selenium.By.XPath("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[9]/div/div/div/a[2]"));
                        OpenQA.Selenium.Support.UI.PopupWindowFinder finder = new OpenQA.Selenium.Support.UI.PopupWindowFinder(chromeDriver, System.TimeSpan.FromMinutes(1), System.TimeSpan.FromMilliseconds(500));
                        string popupWindow = finder.Click(element);

                        //wait for download complete
                        string[] pdfFiles = null;
                        while (true)
                        {
                            pdfFiles = System.IO.Directory.GetFiles(PublicVar.Download_PDF_iHoaDon(), "*.pdf");
                            if (pdfFiles.Length == 1) break;
                            await Task.Delay(1000);
                        }

                        string pdf_file_name = pdfFiles[0];
                        string newFilename = string.Format(@"{0}\{1}.pdf", download_directory, invoiceNumber);
                        if (System.IO.File.Exists(newFilename))
                        {
                            System.IO.File.Delete(newFilename);
                        }
                        System.IO.File.Move(pdf_file_name, newFilename);

                        chromeDriver.SwitchTo().Window(popupWindow).Close();
                        chromeDriver.SwitchTo().Window(mainWindow);
                    }

                    chromeDriver.Close();
                    chromeDriver.Quit();
                    MessageBox.Show("Tải file PDF hóa đơn-điện tử-chuyển đổi thành công!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    chromeDriver.Close();
                    chromeDriver.Quit();
                }
            });
        }

        private async void buttonGetDataToMakeReceipt_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("dữ liệu làm phiếu thu");

            DataTable data_creditCardPayment = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), SQLquery.Oracle_AllinOne_Create_Receipt(dtpFromDateVietsea.Value, dtpToDateVietsea.Value));
            if (data_creditCardPayment.HasData())
            {
                data_creditCardPayment.CopyToRange(ws.Range["a1"]);
                ws.Columns.AutoFit();
                ws.GetRange("c1:c", "c").ConvertToValue();
                ws.GetRange("c1:c", "c").SetAccountingNumberFormat();
                //ws.InsertBlankRowsAtValueChange("a");
                Globals.ThisAddIn.Application.Goto(ws.Range["a1"]);
            }


        }

        private async void buttonViewProductDetail_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet sh = wb.GetSheet("Chi tiết vật tư");
            string[] arr = (((string)textBoxProduct_codes.GetText()).Split(','));
            List<string> list = arr.ToList();

            for (DateTime d1 = dtpFromDateVietsea.Value; d1 <= dtpToDateVietsea.Value; d1 = d1.AddDays(1))
            {
                DataTable dt = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), SQLquery.Oracle_product_detail(d1, list));
                if (dt != null && dt.Rows.Count > 0)
                {
                    sh.NewRangeAtColumn("a").Value2 = "ngày " + d1.ToString("dd-MM-yyyy");
                    sh.LastRange("a").Font.Bold = true;
                    dt.CopyToRange(sh.NewRangeAtColumn("a"), true);
                }
            }

            sh.Columns.AutoFit();
            Globals.ThisAddIn.Application.Goto(sh.Range["a1"], true);
            Notification.Toast.Show("Thành công");

        }

        private async void buttonLoadInvoice_Click(object sender, EventArgs e)
        {
            DataTable data = await VIETSEA.TBNETERP.DATATABLE_HOADON_NHAPMUA_NHAPDICHVU(GetConnectionStringOracle(), dtpFromDateVietsea.Value, dtpToDateVietsea.Value, gridLookUpEditMaNCC.Text);

            if (data.HasData())
            {
                gridControl_DS_Hoadon_vietsea.DataSource = data;
            }
            else
            {
                MsgBox.Show("Không có dữ liệu");
                this.SafeInvoke(() => BringToFront());
            }

        }



        private async void buttonGetDataExportSTLTTAN_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(comboBoxSheetName2.GetText()))
            {
                MsgBox.Show("Chưa chọn sheet tồn kho");
                this.SafeInvoke(() => BringToFront());
                return;
            }

            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet bangma = await ExcelObject.GetSheetBangMaFromGoogleDriveAsync(wb);
            DataTable dt = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString(), SQLquery.MSSQL_GetDataExportSTLTTAN(dtpFromDate.Value, dtpToDate.Value));

            Worksheet sheetTonKho = wb.GetSheet(comboBoxSheetName2.GetText() as string);
            //Worksheet sh = wb.GetWorksheet("xuất nội bộ");

            if (dt.Rows.Count > 0)
            {
                Worksheet sh = wb.GetSheet("xuất nội bộ");
                dt.CopyToRange(sh.Range["a1"]);
                Module1.VlookupMAVIETSEA(sh.GetRange("a2:a", "a"), sh.Range["b2"], bangma);
                sh.GetRange("c1:c", "a").ConvertToValue();
                sh.GetRange("a1:c", "a").DeleteRowContainTextCore(2, "#N/A");

                string header_chuyenkho = "chuyển kho";
                Range range_chuyenkho = sh.EntireRow(1).GetHeaderRangeByText(header_chuyenkho);

                Range lookupValue = sh.EntireRow(1).GetHeaderRangeByText(XNT_COLUMN_MAVATTU).FirstRow();
                Range tableArray = sheetTonKho.Range[((Range)sheetTonKho.Rows[1]).GetHeaderRangeByText(XNT_COLUMN_MAVATTU), ((Range)sheetTonKho.Rows[1]).GetHeaderRangeByText(XNT_COLUMN_SOLUONG)].EntireColumn;
                int col_index = sheetTonKho.EntireRow(1).GetHeaderRangeByText(XNT_COLUMN_SOLUONG).Column - sheetTonKho.EntireRow(1).GetHeaderRangeByText(XNT_COLUMN_MAVATTU).Column + 1;

                string vlookupFormula = ExcelFormula.VLOOKUP(lookupValue, tableArray, col_index, 0, 0);

                range_chuyenkho.FirstRow().Formula = $"=MIN(C2,{vlookupFormula})";
                range_chuyenkho.FirstRow().ResizeToLastRow("a").FillDownFormula(false);
                //return;
                sh.UsedRange.DeleteRowContainTextCore(4, "=0");

            }
            else
            {
                MsgBox.Show("Không có dữ liệu");
                this.SafeInvoke(() => BringToFront());
            }

        }

        private void buttonbuttonApplyPrice_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(comboBox_sheet_tồn.GetText()))
            {
                MsgBox.Show("Chưa chọn sheet tôn kho");
                comboBox_sheet_tồn.Focus();
                return;
            }

            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet sheetTonKho = wb.GetSheet(comboBox_sheet_tồn.GetText() as string);
            sheetTonKho.Activate();

            Range range1 = sheetTonKho.Range["a1"].EntireRow.GetHeaderRangeByText("GIATRI").offset(ExtensionMethod.xlOffset.Right);
            range1.SetHeaderText(new string[] { "GIABAN", "CHENHLECH", "TILELAI" });

            Range giaVonFirstRow = sheetTonKho.Range["a1"].EntireRow.GetHeaderRangeByText("GIAVON").offset(ExtensionMethod.xlOffset.Down);
            Range giaBanFirstRow = sheetTonKho.Range["a1"].EntireRow.GetHeaderRangeByText("GIABAN").offset(ExtensionMethod.xlOffset.Down);
            Range chenhLechFirstRow = sheetTonKho.Range["a1"].EntireRow.GetHeaderRangeByText("CHENHLECH").offset(ExtensionMethod.xlOffset.Down);
            Range tiLeLaiFirstRow = sheetTonKho.Range["a1"].EntireRow.GetHeaderRangeByText("TILELAI").offset(ExtensionMethod.xlOffset.Down);

            Range mavattuFirstRow = sheetTonKho.Range["a1"].EntireRow.GetHeaderRangeByText("MAVATTU").offset(ExtensionMethod.xlOffset.Down);
            ApplyExpPrice(wb, sheetTonKho, mavattuFirstRow.ResizeToLastRow("a"), customComboBoxVATrate.GetText(ValueType.Int), giaBanFirstRow, checkBox_RoundExpPrice.Checked);

            chenhLechFirstRow.Formula = $"={giaBanFirstRow.Address[false, false]}-{giaVonFirstRow.Address[false, false]}";
            tiLeLaiFirstRow.Formula = $"={giaBanFirstRow.Address[false, false]}/{giaVonFirstRow.Address[false, false]}";

            sheetTonKho.Range[chenhLechFirstRow, tiLeLaiFirstRow].ResizeToLastRow("a").FillDownFormula(false);
            //sheetTonKho.GetRange("a1:h", "a").Sort1(sheetTonKho.Range["g1"], XlSortOrder.xlAscending);
            sheetTonKho.Activate();
            this.SafeInvoke(() => BringToFront());
        }


        private void button3_UploadEinvoiceSelenium_Click(object sender, EventArgs e)
        {
            List<string> listInvoiceNumber = new List<string>();
            List<UserClass.Invoice> listInvoiceInfo = new List<UserClass.Invoice>();

            int[] checkedRows = gridView_EinvoiceList.GetCheckedRows();

            foreach (int item in checkedRows)
            {
                UserClass.Invoice info = new UserClass.Invoice();
                info.InvoiceNumber = gridView_EinvoiceList.GetRowCellValue(item, "Số hóa đơn").ToString();
                info.CustomerName = gridView_EinvoiceList.GetRowCellValue(item, "Tên khách hàng").ToString();
                listInvoiceInfo.Add(info);
            }

            Task.Run(() =>
            {
                ExcelAddIn1.AutoTest.Selenium.EFY.EFYAuto efy = new AutoTest.Selenium.EFY.EFYAuto();
                string mst = AddinConfig.EFYmst;
                string username = AddinConfig.EFYusername;
                string password = AddinConfig.EFYpassword;
                efy.SignIn(mst, username, password);

                string path = folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderPath;
                efy.ImportInvoicesDatPhat(listInvoiceInfo, path);
                efy.Exit();
                MsgBox.Show("Upload hóa đơn điện tử thành công!");
            });



        }



        private async void buttonLoadTonCuoi_v2_Click(object sender, EventArgs e)
        {
            try
            {
                #region MAIN
                //await Task.Delay(0);
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;

                List<string> khohang = null;
                khohang = gridViewKHO.GetSelectedValue("MAKHO");

                string sheetname = $"XNT_{dtpToDateVietsea.Value.ToString("dd-MM-yyyy")} {XNT_NAME_TYPE_FULL}";

                VIETSEA.TBNETERP.XNTtype type = VIETSEA.TBNETERP.XNTtype.Full;
                if (checkBoxBoMaDaXuat.Checked)
                {
                    type = VIETSEA.TBNETERP.XNTtype.Available;
                    sheetname = sheetname.Replace(XNT_NAME_TYPE_FULL, XNT_NAME_TYPE_AVAILABLE);
                }

                Worksheet sheetTonkho = wb.GetNewSheet(sheetname);


                DataTable dt = await VIETSEA.TBNETERP.DATATABLE_XNT_VIETSEA_CORE(GetConnectionStringOracle(), dtpToDateVietsea.Value, khohang, type);
                if (dt != null && dt.Rows.Count > 0)
                {
                    dt.CopyToRange(sheetTonkho.Range["a1"]);
                    sheetTonkho.GetRange("d1:f", "a").ConvertToValue();
                    sheetTonkho.GetRange("d1:f", "a").SetAccountingNumberFormat();
                    sheetTonkho.Columns.AutoFit();
                }
                else
                {
                    MsgBox.Show("không có dữ liệu");
                    goto endd;
                }


                if (checkBoxBoHangXuatTra.Checked)
                {
                    wb.DeleteSheet(sheetTonkho.Name.Replace(XNT_NAME_TYPE_XUATTRA, string.Empty) + XNT_NAME_TYPE_XUATTRA);
                    sheetTonkho.Name = sheetTonkho.Name.Replace(XNT_NAME_TYPE_XUATTRA, string.Empty) + XNT_NAME_TYPE_XUATTRA;
                    string startDate = System.DateTime.Parse(AddinConfig.StartDateSaleReturn).ToString("yyyyMMdd");
                    string endDate = System.DateTime.Now.ToString("yyyyMMdd");
                    DataTable hangXuatTra = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString_TONGKHO(), SQLquery.getHangXuatTra_trancode_05(startDate, endDate));
                    if (hangXuatTra != null && hangXuatTra.Rows.Count > 0)
                    {
                        Worksheet sheetHangXuatTra = wb.GetSheet(SHEETNAME_XUATTRA);
                        hangXuatTra.CopyToRange(sheetHangXuatTra.Range["a1"]);
                        Module1.VlookupMAVIETSEA(sheetHangXuatTra.GetRangeByText("GoodID").ResizeToLastRow(), sheetHangXuatTra.GetRangeByText(XNT_COLUMN_MAVATTU).FirstRow(), await ExcelObject.GetSheetBangMaFromGoogleDriveAsync(wb)); ;

                        sheetHangXuatTra.UsedRange.DeleteRowContainText(XNT_COLUMN_MAVATTU, "#N/A");
                        sheetHangXuatTra.GetRangeByText("SumQuantity").ResizeToLastRow().ConvertToValue();

                        Range KhaDung_headerRange = ((Range)sheetTonkho.Rows[1]).GetHeaderRangeByText("KHADUNG");
                        KhaDung_headerRange.FirstRow().Formula = $"=MAX({sheetTonkho.GetRangeByText(XNT_COLUMN_SOLUONG).FirstRow().Address[false, false]}-IFERROR(VLOOKUP({sheetTonkho.GetRangeByText(XNT_COLUMN_MAVATTU).FirstRow().Address[false, false]},'{sheetHangXuatTra.Name}'!{sheetHangXuatTra.GetRangeByText(XNT_COLUMN_MAVATTU).GetColumnLetter()}:{sheetHangXuatTra.GetRangeByText("SumQuantity").GetColumnLetter()},{sheetHangXuatTra.GetRangeByText("SumQuantity").Column}-{sheetHangXuatTra.GetRangeByText(XNT_COLUMN_MAVATTU).Column}+1,0),0),0)";
                        KhaDung_headerRange.FirstRow().ResizeToLastRow(sheetTonkho.GetRangeByText(XNT_COLUMN_MAVATTU).GetColumnLetter()).FillDownFormula();
                        sheetTonkho.UsedRange.DeleteRowContainText("KHADUNG", "0");
                        sheetTonkho.GetRangeByText(XNT_COLUMN_SOLUONG).FirstRow().ResizeToLastRow().Value2 = sheetTonkho.GetRangeByText("KHADUNG").FirstRow().ResizeToLastRow().Value2;
                        KhaDung_headerRange.EntireColumn.ClearContents();
                        wb.DeleteSheet(SHEETNAME_XUATTRA);
                    }
                }

                if (checkBoxGetRetailPrice.Checked)
                {

                }

                sheetTonkho.Activate();
                Notification.Toast.Show("Load tồn kho VIETSEA thành công");
            #endregion



            endd:;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.SafeInvoke(() => BringToFront());
            }
        }

        private void radioButtonKHACHLE_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonKHACHLE.Checked)
            {

            }
        }

        private async void button_DownloadInvoiceInfoToCheck_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet sh = wb.GetSheet("ihoadon");

            SeleniumBrowser browser = new SeleniumBrowser(TimeSpan.FromSeconds(10));
            await Selenium.SignIn_IHoaDon(browser);
            await browser.SmartClick(iHoaDon.button_LapHoaDon);

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(browser.GetWebElement("/html").GetAttribute("outerHTML"));

            DataTable table = new DataTable();
            //add columns
            var headers = doc.DocumentNode.SelectNodes("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[5]/div/table/tbody/tr[1]/td");
            foreach (var header in headers)
            {
                table.Columns.Add(header.InnerText);
            }

            //lọc ngày tháng
            string fromDate = dtpFromDateVietsea.Value.ToString("dd/MM/yyyy");
            string toDate = dtpToDateVietsea.Value.ToString("dd/MM/yyyy");
            string xpath_tuNgay = "/html/body/app-root/app-system/div/main/div/app-index/div/div[1]/app-search-advanced/div[1]/div[2]/div/div[2]/dx-date-box/div/div/div[1]/input";
            string xpath_denNgay = "/html/body/app-root/app-system/div/main/div/app-index/div/div[1]/app-search-advanced/div[1]/div[3]/div/div[2]/dx-date-box/div/div/div[1]/input";
            await browser.SendKeys(xpath_tuNgay, fromDate);
            await browser.SendKeys(xpath_denNgay, toDate);
            string xpath_timkiem = "/html/body/app-root/app-system/div/main/div/app-index/div/div[1]/app-search-advanced/div[2]/div[1]/div/div[2]/dx-text-box/div/div[1]/input";
            await browser.SmartClick(xpath_timkiem);
            //await Task.Delay(1000);

            while (true)
            {
                #region crawl current page
                doc.LoadHtml(browser.GetWebElement("/html").GetAttribute("outerHTML"));

                //add cell value
                var allRows = doc.DocumentNode.SelectNodes("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr");
                foreach (var row in allRows)
                {
                    HtmlAgilityPack.HtmlDocument doc_cells = new HtmlAgilityPack.HtmlDocument();
                    doc_cells.LoadHtml(row.OuterHtml);

                    var cells = doc_cells.DocumentNode.SelectNodes("//td");
                    List<string> valueArray = new List<string>();
                    foreach (var cell in cells)
                    {
                        string cellValue = cell.InnerText;
                        valueArray.Add(cellValue);
                    }
                    DataRow dataRow = table.NewRow();
                    dataRow.ItemArray = valueArray.ToArray();
                    table.Rows.Add(dataRow);
                    valueArray.Clear();
                }
                #endregion

                //click next page
                doc.LoadHtml(browser.GetWebElement("/html").GetAttribute("outerHTML"));
                var pages = doc.DocumentNode.SelectNodes("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[11]/div[2]/div");
                var lastPage = pages.Last();

                if (lastPage.GetAttributeValue("class", "") == "dx-navigate-button dx-next-button dx-button-disable")
                {
                    break;
                }
                else
                {
                    string xpath_lastPage = lastPage.XPath;
                    await browser.SmartClick(xpath_lastPage);
                }
            }

            table.CopyToRange(sh.Range["a1"]);
            sh.Activate();
            sh.Columns.AutoFit();
            sh.UsedRange.DeleteRowContainText(table.Columns[1].ToString(), "=");
            sh.EntireRow(1).GetHeaderRangeByText(" Tổng tiền (đ) ").ResizeToLastRow().ConvertToValue();
            Notification.Toast.Show("xong");
            browser.CloseAndQuit();
        }



        private async Task GetAndWriteData(SeleniumBrowser browser, Worksheet sheet)
        {

            #region main
            OpenQA.Selenium.Chrome.ChromeDriver driver = browser.ChromeDriverCore;
            OpenQA.Selenium.Support.UI.WebDriverWait wait = browser.WaitCore;

            int page = 3;//3 vì element thứ 3 mới là page 1

            //string endingElement = "p";

            while (true)
            {
                await browser.WaitDOMchange();
                await browser.SmartClick($"/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[11]/div[2]/div[{page}]");
                var pageElement = browser.GetWebElement($"/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[11]/div[2]/div[{page}]");

                #region wait for Page changed
                string attributeClass = pageElement.GetAttribute("class");
                if (attributeClass == "dx-navigate-button dx-prev-button dx-button-disable")//button next page cuối cùng
                {
                    break;
                }
                #endregion

                string attributePage = pageElement.GetAttribute("aria-label");//page
                if (attributePage.Contains("Page "))
                {
                    #region Loop Data Row
                    int row = 0;
                    string endingElement = "p";

                    while (true)//loop data row
                    {
                        row++;
                        if (browser.IsElementPresent($"/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[{row}]/td[2]/div/a"))//check invoice number
                        {
                            string invoiceNumber = browser.GetWebElement($"/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[{row}]/td[2]/div/a").Text;
                            string invoiceDate = browser.GetWebElement($"/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[{row}]/td[4]").Text;
                            string invoiceCustomerName = browser.GetWebElement($"/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[{row}]/td[6]/div/p").Text;
                            string invoiveAmount = browser.GetWebElement($"/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[{row}]/td[7]").Text;

                            if (endingElement == "p")
                            {
                                if (browser.IsElementPresent($"/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[{row}]/td[8]/div/{endingElement}") == true)//status
                                {
                                    endingElement = "p";//chờ ký
                                }
                                else
                                {
                                    endingElement = "a";//đã ký (đã nhận hoặc chưa gửi khách hàng)
                                }
                            }
                            string invoiceStatus = browser.GetWebElement($"/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[{row}]/td[8]/div/{endingElement}").Text;//status

                            Range newRange = sheet.NewRangeAtColumn("a");
                            //Globals.ThisAddIn.Application.Goto(newRange);
                            newRange.SetHeaderText(invoiceNumber, invoiceDate, invoiceCustomerName, invoiveAmount, invoiceStatus);
                        }
                        else
                        {
                            break;
                        }
                    }
                    #endregion

                    page++;
                    Notification.Toast.Show(page.ToString());
                }
                else
                {
                    break;
                }
            }

            //string invoiceNumber = wait.GetWebElement("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[2]/div/a").Text;
            //string invoiceDate = wait.GetWebElement("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[4]").Text;
            //string invoiceCustomerName = wait.GetWebElement("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[6]/div/p").Text;
            //string invoiveAmount = wait.GetWebElement("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[7]").Text;
            //string invoiceStatus = wait.GetWebElement("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[8]/div/p").Text;

            sheet.Columns.AutoFit();
            sheet.Activate();
            #endregion


        }


        private async Task GetAndWriteData_HAM(SeleniumBrowser browser, Worksheet sheet)
        {
            await browser.WaitDOMchange();
            //await Task.Delay(1000);

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(browser.ChromeDriverCore.PageSource);

            var str = doc.DocumentNode.SelectSingleNode("/html/body/app-root/app-system/div/main/div/app-index/dx-data-grid/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[2]/div/a");
            MessageBox.Show("inner text: " + str.InnerText);
            MessageBox.Show("inner html: " + str.InnerHtml);
            MessageBox.Show("outer text: " + str.OuterHtml);




        }

        private async void buttonQuickViewRetail_Click(object sender, EventArgs e)
        {
            if (DataBaseIsNull()) return;

            try
            {
                string fromDate = dtpFromDate.Value.ToString("yyyyMMdd");
                string toDate = dtpToDate.Value.ToString("yyyyMMdd");

                List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@FromDate", fromDate),
                new SqlParameter("@ToDate",  toDate),

            };
                DataTable data = await ADO.GetDataTableFromStoredProcedureAsync(GetConnectionString(), "[dbo].[Reports.Export.RetailSummary]", parameters);
                var dataRows = data.Select("TransCode = 03");
                var result = dataRows[0]["TotalExpPrice"];
                label24.SetTextSafeThread(Convert.ToDecimal(result).ToString("#,##0"));
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
        }



        private void button_TachBill_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet sh = wb.GetSheet("Sheet1");





        }

        private async void button_LoadBangKeBanRa_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet sheetBangKeBanRa = wb.GetSheet("bangkebanra");

            DataTable bangke = await VIETSEA.TBNETERP.DATATABLE_BangKeBanRa(GetConnectionStringOracle(), dtpFromDateVietsea.Value, dtpToDateVietsea.Value);
            if (bangke != null)
            {
                bangke.CopyToRange(sheetBangKeBanRa.Range["a1"]);
                Range range_thanhtien = sheetBangKeBanRa.EntireRow(1).GetHeaderRangeByText("THANHTIEN").EntireColumn;
                range_thanhtien.ConvertToValue();
                range_thanhtien.SetAccountingNumberFormat();
                sheetBangKeBanRa.Columns.AutoFit();
            }

        }

        private void textBox_IP_Oracle_Leave(object sender, EventArgs e)
        {
            AddinConfig.Oralce_IP = textBox_IP_Oracle.GetText();
        }

        private void textBox_Port_Oracle_Leave(object sender, EventArgs e)
        {
            AddinConfig.Oralce_Port = textBox_Port_Oracle.GetText();
        }

        private async void button_LoadDSKhoVietsea_Click(object sender, EventArgs e)
        {
            DataTable dt_kho = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), "select makho from dmkhodv order by 1");
            if (dt_kho.HasData())
            {
                gridControlKHO.SafeBeginInvoke(() =>
                {
                    gridControlKHO.DataSource = dt_kho;
                    gridViewKHO.SetMultiSelectCheckbox();
                    gridViewKHO.OptionsView.ShowGroupPanel = false;
                    gridViewKHO.OptionsView.ShowAutoFilterRow = true;
                });
                Notification.Toast.Show("Load danh sách kho thành công");
            }

            //customListViewKHOHANG.AddDataSource(dt_kho);
            //customListViewKHOHANG.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }


        private async void button_BC_S38_Click(object sender, EventArgs e)
        {
            Module1.SpeedUpCode(true);
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("TK331 thuế");
            try
            {
                #region MAIN
                string tu_ngay = dtpFromDateVietsea.Value.ToString("dd/MM/yyyy");
                string den_ngay = dtpToDateVietsea.Value.ToString("dd/MM/yyyy");
                int p_nam = dtpFromDateVietsea.Value.Year;

                //string ncc = gridView1.GetRowCellValue(gridView1.GetSelectedRows().First(), "MAKHACHHANG").ToString();
                string ncc = gridLookUpEditMaNCC.Text;

                DataTable dt = await VIETSEA.TBNETERP.BC_SKT_S38DN_BCMAIN(GetConnectionStringOracle(), "", p_nam, tu_ngay, den_ngay, "MATK LIKE '331%' ", $"CTDOITUONG LIKE '%DMNHACUNGCAP:{ncc};%' ", "", "", "");
                if (dt == null && dt.Rows.Count == 0)
                {
                    Notification.Toast.Show("Không có dữ liệu");
                    return;
                }

                #region transform data
                dt.CopyToRange(ws.Range["a1"]);
                Range NGAYHACHTOAN = ws.EntireRow(1).GetHeaderRangeByText("NGAYHACHTOAN");
                Range NGAYCTKT = ws.EntireRow(1).GetHeaderRangeByText("NGAYCTKT");
                Range NOIDUNG = ws.EntireRow(1).GetHeaderRangeByText("NOIDUNG");
                Range SPSTK_NO = ws.EntireRow(1).GetHeaderRangeByText("SPSTK_NO");
                Range SPSTK_CO = ws.EntireRow(1).GetHeaderRangeByText("SPSTK_CO");

                Range SDDK_NO = ws.EntireRow(1).GetHeaderRangeByText("SDDK_NO");
                Range SDDK_CO = ws.EntireRow(1).GetHeaderRangeByText("SDDK_CO");
                Range SOCTUGOC = ws.EntireRow(1).GetHeaderRangeByText("SOCTUGOC");

                ws.Range[SPSTK_NO, SDDK_CO].ResizeToLastRow().ConvertToValue();


                double DUNO_DK = Globals.ThisAddIn.Application.WorksheetFunction.SumIf(NOIDUNG.ResizeToLastRow(), "Số dư đầu kỳ ", SDDK_NO.ResizeToLastRow());
                double DUCO_DK = Globals.ThisAddIn.Application.WorksheetFunction.SumIf(NOIDUNG.ResizeToLastRow(), "Số dư đầu kỳ ", SDDK_CO.ResizeToLastRow());

                ws.UsedRange.DeleteRowContainText("NOIDUNG", "Số dư đầu kỳ ");
                ws.EntireRow(2).Insert();
                NOIDUNG.FirstRow().Value2 = "Số dư đầu kỳ ";
                SDDK_NO.FirstRow().Value2 = Math.Max(0, DUNO_DK - DUCO_DK);
                SDDK_CO.FirstRow().Value2 = Math.Max(0, DUCO_DK - DUNO_DK);

                int LR = ws.LastRow("a");
                if (LR <= 2) return;
                for (int i = LR; i >= 2; i--)//dòng đâu header, 2 dòng tiếp là dư đầu kỳ
                {
                    //cộng dồn số tiền các dòng trùng nhau
                    if (ws.Range[SOCTUGOC.GetColumnLetter() + i].Value2 == ws.Range[SOCTUGOC.GetColumnLetter() + (i + 1)].Value2)
                    {
                        ws.Range[SPSTK_NO.GetColumnLetter() + i].Value2 += ws.Range[SPSTK_NO.GetColumnLetter() + (i + 1)].Value2;
                        ws.Range[SPSTK_CO.GetColumnLetter() + i].Value2 += ws.Range[SPSTK_CO.GetColumnLetter() + (i + 1)].Value2;
                        ws.Range[SPSTK_CO.GetColumnLetter() + (i + 1)].EntireRow.Delete();
                    }

                    //định dạng lại ngày tháng
                    System.DateTime date1;
                    if (DateTime.TryParse(ws.Range[NGAYHACHTOAN.GetColumnLetter() + i].Value2, out date1))
                    {
                        ws.Range[NGAYHACHTOAN.GetColumnLetter() + i].Value2 = "'" + date1.ToString("dd/MM/yyyy");
                    }

                    System.DateTime date2;
                    if (DateTime.TryParse(ws.Range[NGAYCTKT.GetColumnLetter() + i].Value2, out date2))
                    {
                        ws.Range[NGAYCTKT.GetColumnLetter() + i].Value2 = "'" + date2.ToString("dd/MM/yyyy");
                    }

                }
                #endregion

                //tính dư cuối kỳ lũy kế
                SDDK_NO.offset(ExtensionMethod.xlOffset.Down, 2).Formula = "=MAX(0,H2-I2+F3-G3)";
                SDDK_CO.offset(ExtensionMethod.xlOffset.Down, 2).Formula = "=MAX(0,I2-H2+G3-F3)";
                ws.Range[SDDK_NO.offset(ExtensionMethod.xlOffset.Down, 2), SDDK_CO.offset(ExtensionMethod.xlOffset.Down, 2)].ResizeToLastRow().FillDownFormula(false);

                ws.Columns.AutoFit();
                ws.Rows.RowHeight = 20;
                NOIDUNG.ColumnWidth = 25;
                SOCTUGOC.ColumnWidth = 10;
                ws.Range[SPSTK_NO, SPSTK_CO].EntireColumn.Replace("0", string.Empty, LookAt: XlLookAt.xlWhole, SearchOrder: XlSearchOrder.xlByRows);

                //tô màu để tìm ra các đơn thanh toán ứng với hóa đơn nào
                List<Range> GetSet(Range range)
                {
                    List<Range> result = new List<Range>();
                    foreach (Range eachRange in range)
                    {
                        if (eachRange.Value2 != null && eachRange.Interior.Color == ExcelSystem.InteriorColorInteger.White)
                        {
                            result.Add(eachRange);
                        }
                    }
                    return result;
                }

                Range spsNO = SPSTK_NO.FirstRow().ResizeToLastRow("a");
                Range spsCO = SPSTK_CO.FirstRow().ResizeToLastRow("a");

                void setColor(Range columns, int color, params Range[] rangeContainValues)
                {
                    //List<Range> ranges = columns.FindAllRangeEqualToValue(rangeContainValues);
                    //List<double> dupRange = new List<double>();
                    foreach (Range r in rangeContainValues)
                    {
                        r.Interior.Color = color;
                        //if (r.Interior.Color == ExcelSystem.InteriorColorInteger.White)//white
                        //{
                        //    //if (!dupRange.Contains(r.Value2))
                        //    //{
                        //    //dupRange.Add(r.Value2);
                        //    //}
                        //}
                    }
                }

                Range getRemainRange(List<Range> source, double value, params Range[] exceptRanges)
                {
                    Range result = null;
                    bool flag = true;

                    foreach (Range range in source)
                    {
                        if (range.Value2 == value)
                        {
                            result = range;
                            if (exceptRanges.Length > 0)
                            {
                                foreach (Range exceptRange in exceptRanges)
                                {
                                    if (exceptRange.Address == range.Address)
                                    {
                                        flag = false;
                                    }
                                }
                            }

                            if (flag)
                            {
                                return range;
                            }
                        }
                    }
                    return null;
                }
                #region subset 1     

                List<Range> setThanhToan1 = GetSet(spsNO);
                foreach (Range thanhToan1 in setThanhToan1)
                {
                    List<Range> setHoaDon1 = GetSet(spsCO);
                    foreach (Range hoaDon1 in setHoaDon1)
                    {
                        if (thanhToan1.Value2 == hoaDon1.Value2)
                        {
                            setColor(spsNO, ExcelSystem.InteriorColorInteger.Green, thanhToan1);
                            setColor(spsCO, ExcelSystem.InteriorColorInteger.Green, hoaDon1);
                            break;
                        }
                    }
                }
                #endregion


                #region subset2
                List<Range> setThanhToan2 = GetSet(spsNO);

                foreach (Range sumThanhToan in setThanhToan2)
                {
                    List<Range> setHoaDon = GetSet(spsCO);

                    //https://www.geeksforgeeks.org/given-an-array-a-and-a-number-x-check-for-pair-in-a-with-sum-as-x/
                    for (int i = 0; i < setHoaDon.Count; ++i)
                    {
                        double remainValue = Convert.ToDouble(sumThanhToan.Value2) - Convert.ToDouble(setHoaDon[i].Value2);

                        // checking for condition
                        Range remainRange = getRemainRange(setHoaDon, remainValue, setHoaDon[i]);
                        if (remainRange != null)
                        {
                            setColor(spsNO, ExcelSystem.InteriorColorInteger.Blue, sumThanhToan);
                            setColor(spsCO, ExcelSystem.InteriorColorInteger.Blue, remainRange, setHoaDon[i]);
                            break;//tô màu xong 2 cái thoát luôn, tránh bị tô màu trùng
                        }
                    }
                }

                #endregion


                #region subset3
                List<Range> setThanhToan3 = GetSet(spsNO);

                foreach (Range sumThanhToan in setThanhToan3)
                {
                    List<Range> setHoaDon3 = GetSet(spsCO);
                    bool isColorSet = false;

                    //https://www.geeksforgeeks.org/given-an-array-a-and-a-number-x-check-for-pair-in-a-with-sum-as-x/
                    for (int i = 0; i < setHoaDon3.Count - 2; i++)
                    {
                        double currentSum = Convert.ToDouble(sumThanhToan.Value2) - Convert.ToDouble(setHoaDon3[i].Value2);

                        for (int j = i + 1; j < setHoaDon3.Count; j++)
                        {
                            double remainValue = Convert.ToDouble(currentSum - Convert.ToDouble(setHoaDon3[j].Value2));
                            Range remainRange = getRemainRange(setHoaDon3, remainValue, setHoaDon3[i], setHoaDon3[j]);
                            if (remainRange != null)
                            {
                                // Console.Write("Triplet is {0}, {1}, {2}", A[i], A[j], curr_sum - A[j]);
                                setColor(spsNO, ExcelSystem.InteriorColorInteger.Orange, sumThanhToan);
                                setColor(spsCO, ExcelSystem.InteriorColorInteger.Orange, setHoaDon3[i], setHoaDon3[j], remainRange);
                                isColorSet = true;
                                break;//tô màu xong 3 cái thoát luôn, tránh bị tô màu trùng
                            }
                        }
                        if (isColorSet) break;
                    }
                }
                #endregion



                #endregion
                ws.Range[SPSTK_NO, SDDK_CO].ResizeToLastRow("a").SetAccountingNumberFormat();
            }
            finally
            {
                Module1.SpeedUpCode(false);
                Globals.ThisAddIn.Application.Goto(ws.Range["a1"], true);
            }


        }

        private async void button_load_makhachhang_vietsea_Click(object sender, EventArgs e)
        {
            DataTable dt = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), "select makhachhang,tentochu from dmkhachhang where manhomkhachang like '%NCC%'");
            if (dt.HasData())
            {
                gridLookUpEditMaNCC.Enabled = true;
                gridLookUpEditMaNCC.Properties.DataSource = dt;
                gridLookUpEditMaNCC.Text = "Gõ tên nhà cung cấp để tìm kiếm";
                gridLookUpEditMaNCC.Properties.DisplayMember = "MAKHACHHANG";
                gridLookUpEditMaNCC.Properties.ValueMember = "MAKHACHHANG";
                textBoxMAKHACHHANG.Clear();

                //https://supportcenter.devexpress.com/ticket/details/t119128/gridlookup-how-to-adjust-column-widths#
                //gridLookUpEditMaNCC.Properties.PopulateViewColumns();
                //gridLookUpEditMaNCCView.Columns["MAKHACHHANG"].Width =25;
                //gridLookUpEditMaNCCView.Columns["TENTOCHU"].Width = 500;

            }

        }



        private void checkBox_pinForm_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_pinForm.Checked)
            {
                TopMost = true;
            }
            else
            {
                TopMost = false;
            }
        }

        private async void button_download_saoke_techcombank_Click(object sender, EventArgs e)
        {
            try
            {
                AutoTest.WinAppDriver.Core.InitToken();
                await Task.Run(() =>
                {
                    #region MAIN
                    System.TimeSpan timeout = System.TimeSpan.FromSeconds(120);
                    AutoTest.Selenium.AutoBrowser browser = new AutoTest.Selenium.AutoBrowser(timeout);

                    string url = "https://www.techcombank.com.vn/trang-chu";
                    Notification.Wait.Show($"Truy cập {url}");
                    browser.ChromeDriverCore.Url = url;

                    Notification.Wait.Show($"Click doanh nghiệp");
                    var buttonDoanhNghiep = browser.GetElementByCSSselector(@"a[class='button button_login_box']", "DOANH NGHIỆP");
                    browser.MouseClick(buttonDoanhNghiep);

                    Notification.Wait.Show($"Nhập tên đăng nhập");
                    var textboxUsername = browser.GetElementByCSSselector("input[class='tb8'][type='text'][name='signOnName']");
                    browser.SendKeys(textboxUsername, "LETRONGTAN86");

                    Notification.Wait.Show($"Nhập mật khẩu");
                    var textboxPassword = browser.GetElementByCSSselector("input[class='tb8'][type='password'][name='password']");
                    browser.SendKeys(textboxPassword, "Datphat8689@@");

                    Notification.Wait.Show($"Click đăng nhập");
                    var buttonLogin = browser.GetElementByCSSselector("input[type='submit'][name='reset_btn'][class='btn_grey'][value='Đăng Nhập']");
                    browser.MouseClick(buttonLogin);

                    Notification.Wait.Show($"Báo cáo tài khoản");
                    var buttonBaoCaoTaiKhoan = browser.GetElementByCSSselector("span[onclick='ProcessMouseClick(event)']", "  Báo cáo tài khoản");
                    browser.MouseClick(buttonBaoCaoTaiKhoan);

                    Notification.Wait.Show($"Tải thông tin giao dịch");
                    var buttonTaiThongTinGiaoDich = browser.GetElementByCSSselector("a[class='MenuLink']", "Tải thông tin giao dịch");
                    browser.MouseClick(buttonTaiThongTinGiaoDich);

                    Notification.Wait.Show($"Nhập số tài khoản Tech");
                    var textboxTaiTuNguon = browser.GetElementByCSSselector("input[class='dealbox'][id='fieldName:ACCOUNT']");
                    browser.SendKeys(textboxTaiTuNguon, "19027201729889");

                    string startDate = dtpFromDate.Value.ToString("dd/MM/yyyy");
                    string endDate = dtpToDate.Value.ToString("dd/MM/yyyy");

                    var textboxStartDate = browser.GetElementByCSSselector("input[class='dealbox'][id='fieldName:START.DATE']");
                    var textboxEndDate = browser.GetElementByCSSselector("input[class='dealbox'][id='fieldName:END.DATE']");
                    browser.SetValue(textboxStartDate, startDate);
                    browser.SetValue(textboxEndDate, endDate);

                    Notification.Wait.Show($"Click thực hiện");
                    var buttonThucHien = browser.GetElementByCSSselector("a[alt='Thực hiện'][title='Thực hiện']");
                    browser.MouseClick(buttonThucHien);

                    Notification.Wait.Show($"Tìm tên file excel");
                    string originalFileName = string.Empty;
                    OpenQA.Selenium.IWebElement table = browser.GetElementByCSSselector("tr[onmouseover=\"raiseEnquiryRowMouseOver( event, this, 'AI.DOWNLOAD.FILE.CORP');\"]");
                    var rows = table.FindElements(OpenQA.Selenium.By.TagName("td"));
                    foreach (OpenQA.Selenium.IWebElement row in rows)
                    {
                        if (row.Text.Contains(".xls"))
                        {
                            originalFileName = row.Text;
                            break;
                        }
                    }
                    if (string.IsNullOrEmpty(originalFileName))
                    {
                        MessageBox.Show("Không tìm thấy file name");
                        browser.Exit();
                        return;
                    }

                    Notification.Wait.Show($"Xóa file cũ (nếu có)");
                    string localFilePath = System.IO.Path.Combine(SystemIOFile.DownloadFolder, originalFileName);
                    SystemIOFile.Delete(localFilePath);

                    Notification.Wait.Show($"Tải file");
                    var buttonDownloadFile = browser.GetElementByCSSselector("img[alt='Tải File']");
                    browser.MouseClick(buttonDownloadFile);

                    while (true)
                    {
                        if (System.IO.File.Exists(localFilePath))
                        {
                            //rename
                            string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(localFilePath);
                            string fromDate = dtpFromDate.Value.ToString("dd-MM-yyyy");
                            string toDate = dtpToDate.Value.ToString("dd-MM-yyyy");
                            string newFileNameWithoutExtension = $"Sao kê {Accounting.Banking.Name.Techcombank} {fromDate}--{toDate}";
                            string newLocalFilePath = localFilePath.Replace(fileNameWithoutExtension, newFileNameWithoutExtension);
                            SystemIOFile.Rename(localFilePath, newLocalFilePath);

                            Notification.Wait.Show($"Mở file sao kê {newLocalFilePath}");
                            System.Diagnostics.Process.Start(newLocalFilePath);
                            break;
                        }
                    }

                    browser.Exit();
                    Notification.Wait.Close();
                    Notification.Toast.Show("Tải sao kê Techcombank thành công!");
                    #endregion

                }, AutoTest.WinAppDriver.Core.cancellationToken);
            }
            catch (OperationCanceledException)
            {
                Notification.Toast.Show("Đã hủy");
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
            finally
            {
                Core.TearDown();
            }


        }


        private async void button_download_saoke_vietcombank_Click(object sender, EventArgs e)
        {
            SeleniumBrowser browser = new SeleniumBrowser(TimeSpan.FromSeconds(5));
            browser.ChromeDriverCore.Url = "https://www.vietcombank.com.vn/Ibanking2020/";
            //string button_doanhnghiep = "/html/body/div[2]/div/div/div[2]/div/div[3]/div/div[2]/a/span";
            //await browser.SmartClick(button_doanhnghiep);

            string textbox_username = "/html/body/div/div/main/div/div/div/div/section/div/div/form/div[3]/input";
            string textbox_password = "/html/body/div/div/main/div/div/div/div/section/div/div/form/div[4]/input";


        entercaptcha:
            browser.GetWebElement(textbox_username).Clear();
            browser.GetWebElement(textbox_password).Clear();

            await browser.SendKeys(textbox_username, "12954486g58");
            await browser.SendKeys(textbox_password, "Dp123456@");

            try
            {
                string captcha_key = DevExpress.XtraEditors.XtraInputBox.Show("Nhập mã bảo mật:", "", "");
                if (string.IsNullOrEmpty(captcha_key))
                {
                    return;
                }

                string textbox_captcha = "/html/body/div/div/main/div/div/div/div/section/div/div/form/div[5]/div[1]/input";
                browser.GetWebElement(textbox_captcha).Clear();
                await browser.SendKeys(textbox_captcha, captcha_key);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            string button_signin = "/html/body/div/div/main/div/div/div/div/section/div/div/form/div[7]/input";
            await browser.SmartClick(button_signin);

            string text_nhapkhongdungdayso = "/html/body/div/div/main/div/div/div/div/section/div/div/form/div[6]/div[2]/span";
            if (browser.GetWebElement(text_nhapkhongdungdayso) != null && browser.GetWebElement(text_nhapkhongdungdayso).Displayed)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Mã bảo mật không đúng, hãy nhập lại");
                goto entercaptcha;
            }

            string button_danhsachtafikhoan = "/html/body/div/main/div/form/div[3]/aside/div/div/div/div[2]/div[2]/section/div/div/ul[1]/a[5]";
            await browser.SmartClick(button_danhsachtafikhoan);

            string button_0451000556868 = "/html/body/div/main/div/form/div[3]/div/div/div[1]/div[2]/div/table/tbody/tr/td[1]/a";
            await browser.SmartClick(button_0451000556868);

            string fromdate = dtpFromDate.Value.ToString("dd/MM/yyyy");
            string todate = dtpToDate.Value.ToString("dd/MM/yyyy");

            string textbox_fromdate = "/html/body/div/main/div/form/div[3]/div/div/div/div[4]/div[2]/div[2]/div[1]/input[1]";
            string textbox_todate = "/html/body/div/main/div/form/div[3]/div/div/div/div[4]/div[2]/div[2]/div[1]/input[2]";

            browser.ChromeDriverCore.ExecuteScript($"arguments[0].value='{fromdate}';", browser.GetWebElement(textbox_fromdate));
            browser.ChromeDriverCore.ExecuteScript($"arguments[0].value='{todate}';", browser.GetWebElement(textbox_todate));

            //await browser.SmartClick(textbox_fromdate);
            //await browser.SendKeys(textbox_fromdate, fromdate);


            //await browser.SmartClick(textbox_todate);
            //await browser.SendKeys(textbox_todate, todate);

            string button_xemsaoke = "/html/body/div/main/div/form/div[3]/div/div/div/div[4]/div[2]/div[2]/div[1]/a";
            await browser.SmartClick(button_xemsaoke);

            string download_folder = System.IO.Path.Combine(Environment.GetEnvironmentVariable("USERPROFILE"), "Downloads");
            string file_fullpath = System.IO.Path.Combine(download_folder, "Vietcombank_Account_Statement.xls");
            MyClass.SystemIOFile.Delete(file_fullpath);

            string button_xuatfile = "/html/body/div/main/div/form/div[3]/div/div/div/div[4]/div[2]/div[2]/div[1]/input[5]";
            await browser.SmartClick(button_xuatfile);

            while (true)
            {
                if (System.IO.File.Exists(file_fullpath))
                {
                    //rename
                    string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(file_fullpath);
                    string fromDate = dtpFromDate.Value.ToString("dd-MM-yyyy");
                    string toDate = dtpToDate.Value.ToString("dd-MM-yyyy");
                    string newFileNameWithoutExtension = $"Sao kê {Accounting.Banking.Name.Vietcombank} {fromDate}--{toDate}";
                    string newLocalFilePath = file_fullpath.Replace(fileNameWithoutExtension, newFileNameWithoutExtension);
                    SystemIOFile.Rename(file_fullpath, newLocalFilePath);

                    System.Diagnostics.Process.Start(newLocalFilePath);
                    break;
                }
            }
            browser.CloseAndQuit();


        }

        private async void button_check_xnt_vietsea_Click(object sender, EventArgs e)
        {
            //string fromdate = dtpFromDateVietsea.Value.ToString("ddMMyyyy");
            //string todate = dtpToDateVietsea.Value.ToString("ddMMyyyy");

            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetNewSheet("BC_XNT_4_COT");

            async Task<DataTable> Getdata_xnt_tonghop_4cot(DateTime fromdate, DateTime todate)
            {
                string sql_dauky_cuoiky = string.Format(@"Select
                                            Sum(Tondaukysl) As Tondaukysl,
                                            Sum(Tondaukygt) As Tondaukygt,
                                            0 As Nhapmuasl,
                                            0 As Nhapmuagt,
                                            0 As Xbansl,
                                            0 As Xbangt,
                                            0 As Toncuoikysl,
                                            0 As Toncuoikygt
                                            From Xnt_{0} GROUP BY 0, 0, 0, 0, 0, 0

                                            Union All

                                            Select
                                            0 As Tondaukysl,
                                            0 As Tondaukygt,
                                            0 As Nhapmuasl,
                                            0 As Nhapmuagt,
                                            0 As Xbansl,
                                            0 As Xbangt,
                                            Sum(Toncuoikysl) As Toncuoikysl,
                                            Sum(Toncuoikygt) As Toncuoikygt
                                            From Xnt_{1}", fromdate.ToString("ddMMyyyy"), todate.ToString("ddMMyyyy"));

                List<string> someSQLstatements = new List<string>();
                for (DateTime eachDate = fromdate; eachDate <= todate; eachDate = eachDate.AddDays(1))
                {
                    string table = $"XNT_{eachDate.ToString("ddMMyyyy")}";
                    string sql_sum = string.Format(@"select 0 as tondaukysl,
                                                    0 as tondaukygt,
                                                    SUM((NHAPMUASL) +(NKHAUSL) +(NKHACSL) +(NCHUYENKHOSL) +(NTHANHPHAMSL)+(NDODANGSL) + (NTRALAISL) + (NDIEUCHINHSL)) AS NHAPMUASL,
                                                    SUM((NHAPMUAGT) + (NKHAUGT) + (NKHACGT) + (NCHUYENKHOGT) + (NTHANHPHAMGT) + (NDODANGGT) + (NTRALAIGT) + (NDIEUCHINHGT)) AS NHAPMUAGT,
                                                    SUM((XCHUYENKHOSL) + (XKHACSL) + (XBANSL) + (XSXSL) + (XTAICHESL) + (XDICHDANHSL) + (XTRALAISL) + (XDIEUCHINHSL)) as XBANSL,
                                                    SUM((XCHUYENKHOGT) + (XKHACGT) + (XBANGT) + (XSXGT) + (XTAICHEGT) + (XDICHDANHGT) + (XTRALAIGT) + (XDIEUCHINHGT)) AS XBANGT,
                                                    0 as toncuoikysl,
                                                    0 as toncuoikygt
                                                    FROM {0}", table);
                    someSQLstatements.Add(sql_sum);
                }
                string sql_sum_trongky = string.Join(" union all ", someSQLstatements);
                string join_query = sql_dauky_cuoiky + " union all " + sql_sum_trongky;
                string final_query = string.Format(@"Select Sum(Tondaukysl) as Tondaukysl,
                                        Sum(Tondaukygt) as Tondaukygt,
                                        Sum(Nhapmuasl) as Nhapmuasl,
                                        Sum(Nhapmuagt) as Nhapmuagt,
                                        Sum(Xbansl) as Xbansl,
                                        Sum(Xbangt) as Xbangt,
                                        Sum(Toncuoikysl) As Toncuoikysl,
                                        Sum(Toncuoikygt) As Toncuoikygt
                                        From ({0})", join_query);
                //return await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), final_query).ConfigureAwait(false);
                return await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), join_query).ConfigureAwait(false);
            }
            DataTable dt = await Getdata_xnt_tonghop_4cot(dtpFromDateVietsea.Value, dtpToDateVietsea.Value);
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.CopyToRange(ws.Range["a1"]);
                ws.UsedRange.ConvertToValue();
                ws.UsedRange.SetAccountingNumberFormat();
                ws.Columns.AutoFit();
                Notification.Toast.Show("Xuất báo cáo XNT thành công!");
            }
            else
            {
                Notification.Toast.Show("Không có dữ liệu");
            }

        }

        private async void button_TH_CONGNO_331_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet sheetTH = wb.GetSheet("TH_CONGNO_331");
            sheetTH.Range["a1"].SetHeaderText("Mã NCC", "Tên NCC", "Dư Nợ", "Dư Có", "Tình trạng");

            string p_tungay = dtpFromDateVietsea.Value.ToString("dd/MM/yyyy");
            string p_denngay = dtpToDateVietsea.Value.ToString("dd/MM/yyyy");

            string tungay = dtpFromDateVietsea.Value.ToString("dd/MM/yyyy");
            string denngay = dtpToDateVietsea.Value.ToString("dd/MM/yyyy");
            int pnam = dtpFromDateVietsea.Value.Year;

            bool hasData = true;
            async void create_SKT_S38DN_BCMAIN(Worksheet ws, string ncc, int p_nam, string tu_ngay, string den_ngay)
            {

                DataTable dt = await VIETSEA.TBNETERP.BC_SKT_S38DN_BCMAIN(GetConnectionStringOracle(), "", p_nam, tu_ngay, den_ngay, "MATK LIKE '331%' ", $"CTDOITUONG LIKE '%DMNHACUNGCAP:{ncc};%' ", "", "", "");
                if (!dt.HasData())
                {
                    hasData = false;
                    return;
                }


                #region transform data
                dt.CopyToRange(ws.Range["a1"]);
                Range NGAYHACHTOAN = ws.EntireRow(1).GetHeaderRangeByText("NGAYHACHTOAN");
                Range NGAYCTKT = ws.EntireRow(1).GetHeaderRangeByText("NGAYCTKT");
                Range NOIDUNG = ws.EntireRow(1).GetHeaderRangeByText("NOIDUNG");
                Range SPSTK_NO = ws.EntireRow(1).GetHeaderRangeByText("SPSTK_NO");
                Range SPSTK_CO = ws.EntireRow(1).GetHeaderRangeByText("SPSTK_CO");

                Range SDDK_NO = ws.EntireRow(1).GetHeaderRangeByText("SDDK_NO");
                Range SDDK_CO = ws.EntireRow(1).GetHeaderRangeByText("SDDK_CO");
                Range SOCTUGOC = ws.EntireRow(1).GetHeaderRangeByText("SOCTUGOC");

                ws.Range[SPSTK_NO, SDDK_CO].ResizeToLastRow().ConvertToValue();


                double DUNO_DK = Globals.ThisAddIn.Application.WorksheetFunction.SumIf(NOIDUNG.ResizeToLastRow(), "Số dư đầu kỳ ", SDDK_NO.ResizeToLastRow());
                double DUCO_DK = Globals.ThisAddIn.Application.WorksheetFunction.SumIf(NOIDUNG.ResizeToLastRow(), "Số dư đầu kỳ ", SDDK_CO.ResizeToLastRow());

                ws.UsedRange.DeleteRowContainText("NOIDUNG", "Số dư đầu kỳ ");
                ws.EntireRow(2).Insert();
                NOIDUNG.FirstRow().Value2 = "Số dư đầu kỳ ";
                SDDK_NO.FirstRow().Value2 = Math.Max(0, DUNO_DK - DUCO_DK);
                SDDK_CO.FirstRow().Value2 = Math.Max(0, DUCO_DK - DUNO_DK);

                int LR = ws.LastRow("a");
                if (LR <= 2) return;
                for (int i = LR; i >= 2; i--)//dòng đâu header, 2 dòng tiếp là dư đầu kỳ
                {
                    //cộng dồn số tiền các dòng trùng nhau
                    if (ws.Range[SOCTUGOC.GetColumnLetter() + i].Value2 == ws.Range[SOCTUGOC.GetColumnLetter() + (i + 1)].Value2)
                    {
                        ws.Range[SPSTK_NO.GetColumnLetter() + i].Value2 += ws.Range[SPSTK_NO.GetColumnLetter() + (i + 1)].Value2;
                        ws.Range[SPSTK_CO.GetColumnLetter() + i].Value2 += ws.Range[SPSTK_CO.GetColumnLetter() + (i + 1)].Value2;
                        ws.Range[SPSTK_CO.GetColumnLetter() + (i + 1)].EntireRow.Delete();
                    }

                    //định dạng lại ngày tháng
                    System.DateTime date1;
                    if (DateTime.TryParse(ws.Range[NGAYHACHTOAN.GetColumnLetter() + i].Value2, out date1))
                    {
                        ws.Range[NGAYHACHTOAN.GetColumnLetter() + i].Value2 = "'" + date1.ToString("dd/MM/yyyy");
                    }

                    System.DateTime date2;
                    if (DateTime.TryParse(ws.Range[NGAYCTKT.GetColumnLetter() + i].Value2, out date2))
                    {
                        ws.Range[NGAYCTKT.GetColumnLetter() + i].Value2 = "'" + date2.ToString("dd/MM/yyyy");
                    }

                }
                #endregion

                //tính dư cuối kỳ lũy kế
                SDDK_NO.offset(ExtensionMethod.xlOffset.Down, 2).Formula = "=MAX(0,H2-I2+F3-G3)";
                SDDK_CO.offset(ExtensionMethod.xlOffset.Down, 2).Formula = "=MAX(0,I2-H2+G3-F3)";
                ws.Range[SDDK_NO.offset(ExtensionMethod.xlOffset.Down, 2), SDDK_CO.offset(ExtensionMethod.xlOffset.Down, 2)].ResizeToLastRow().FillDownFormula();

                ws.Columns.AutoFit();
                ws.Rows.RowHeight = 20;
                NOIDUNG.ColumnWidth = 25;
                SOCTUGOC.ColumnWidth = 10;
                ws.Range[SPSTK_NO, SPSTK_CO].EntireColumn.Replace("0", string.Empty, LookAt: XlLookAt.xlWhole, SearchOrder: XlSearchOrder.xlByRows);

                //tô màu để tìm ra các đơn thanh toán ứng với hóa đơn nào
                List<string> GetSet(Range range)
                {
                    List<string> result = new List<string>();
                    foreach (Range r in range)
                    {
                        if (r.Value2 != null && r.Interior.Color == 16777215)//white
                        {
                            result.Add(r.GetValue(ValueType.String));
                        }
                    }
                    return result;
                }

                Range spsNO = SPSTK_NO.FirstRow().ResizeToLastRow("a");
                Range spsCO = SPSTK_CO.FirstRow().ResizeToLastRow("a");

                void setColor(Range range, int color, params string[] values)
                {
                    List<Range> ranges = range.FindAllRangeEqualToValue(values);
                    //if (ranges.Count == 0) Noti.ShowAlertMod("not found");
                    List<double> dupRange = new List<double>();
                    foreach (Range r in ranges)
                    {
                        if (r.Interior.Color == 16777215)//white
                        {
                            if (!dupRange.Contains(r.Value2))
                            {
                                r.Interior.Color = color;
                                dupRange.Add(r.Value2);
                            }
                        }
                    }
                }

                #region subset1     
                int greenColor = 5296274;

                List<string> setthanhtoan1 = GetSet(spsNO);
                foreach (string thanhtoan1 in setthanhtoan1)
                {
                    List<string> sethoadon1 = GetSet(spsCO);
                    foreach (string hoadon1 in sethoadon1)
                    {
                        if (thanhtoan1 == hoadon1)
                        {
                            setColor(spsNO, greenColor, thanhtoan1);
                            setColor(spsCO, greenColor, hoadon1);
                            break;
                        }
                    }
                }
                #endregion


                #region subset2
                List<string> set_thanhtoan2 = GetSet(spsNO);

                foreach (string sum_thanhtoan in set_thanhtoan2)
                {
                    List<string> set_hoadon = GetSet(spsCO);

                    //https://www.geeksforgeeks.org/given-an-array-a-and-a-number-x-check-for-pair-in-a-with-sum-as-x/
                    //HashSet<string> s = new HashSet<string>();
                    for (int i = 0; i < set_hoadon.Count; ++i)
                    {
                        double temp = Convert.ToDouble(sum_thanhtoan) - Convert.ToDouble(set_hoadon[i]);

                        // checking for condition
                        if (set_hoadon.Contains(temp.ToString()))
                        {
                            //     Console.Write("Pair with given sum " +
                            //sum + " is (" + set_hoadon[i] + ", " + temp + ")");
                            //MsgBox.Show($"{sum_thanhtoan} = {temp.ToString()} + {set_hoadon[i]}");

                            int blueColor = 15773696;
                            setColor(spsNO, blueColor, sum_thanhtoan);
                            setColor(spsCO, blueColor, temp.ToString(), set_hoadon[i]);
                            break;//tô màu xong 2 cái thoát luôn, tránh bị tô màu trùng
                        }
                        //s.Add(set_hoadon[i]);
                    }
                }

                #endregion


                #region subset3
                List<string> set_thanhtoan3 = GetSet(spsNO);

                foreach (string sum_thanhtoan in set_thanhtoan3)
                {
                    List<string> set_hoadon3 = GetSet(spsCO);
                    bool isColorSet = false;

                    //https://www.geeksforgeeks.org/given-an-array-a-and-a-number-x-check-for-pair-in-a-with-sum-as-x/
                    //HashSet<string> s = new HashSet<string>();
                    for (int i = 0; i < set_hoadon3.Count - 2; i++)
                    {
                        double currentSum = Convert.ToDouble(sum_thanhtoan) - Convert.ToDouble(set_hoadon3[i]);

                        for (int j = i + 1; j < set_hoadon3.Count; j++)
                        {
                            string remainElement = Convert.ToString(currentSum - Convert.ToDouble(set_hoadon3[j]));
                            if (set_hoadon3.Contains(remainElement))
                            {
                                // Console.Write("Triplet is {0}, {1}, {2}", A[i], A[j], curr_sum - A[j]);
                                int orangeColor = 49407;
                                setColor(spsNO, orangeColor, sum_thanhtoan);
                                setColor(spsCO, orangeColor, set_hoadon3[i], set_hoadon3[j], remainElement);
                                isColorSet = true;
                                break;//tô màu xong 3 cái thoát luôn, tránh bị tô màu trùng
                            }
                            //s.Add(set_hoadon3[i]);
                        }
                        if (isColorSet) break;
                    }
                }
                #endregion
                ws.Range[SPSTK_NO, SDDK_CO].ResizeToLastRow("a").SetAccountingNumberFormat();
            }

            DataTable dt_dmkhachhang = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), "select makhachhang,tentochu from dmkhachhang where manhomkhachang like '%NCC%'");
            Globals.ThisAddIn.Application.ScreenUpdating = false;
            int currentItem = 1;
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();

            string columnSDDK_NO = "H";//ws.GetRangeByText("SDDK_NO").GetColumnLetter();
            string columnSDDK_CO = "I";//ws.GetRangeByText("SDDK_CO").GetColumnLetter();

            foreach (DataRow dr in dt_dmkhachhang.Rows)
            {
                Notification.Wait.Show($"Đang tải dữ liệu: {currentItem}/{dt_dmkhachhang.Rows.Count}");
                currentItem++;

                string maNCC = dr[0].ToString();
                string tenNCC = dr[1].ToString();
                Worksheet ws = wb.GetSheet(maNCC);

                hasData = true;
                string status = string.Empty;
                string duNoCuoiKy = string.Empty;
                string duCoCuoiKy = string.Empty;

                create_SKT_S38DN_BCMAIN(ws, maNCC, pnam, tungay, denngay);
                //if (!hasData)
                //{
                //    status = "Hết công nợ";
                //    goto create_hyperLinks;
                //}

                try
                {
                    duNoCuoiKy = ws.LastRange(columnSDDK_NO).GetValue(ValueType.String);
                    duCoCuoiKy = ws.LastRange(columnSDDK_CO).GetValue(ValueType.String);
                }
                catch { }

                int LR = ws.LastRow("h");//nhỏ nhất là 2
                double saiSoChoPhep = 10;

                if (!hasData)
                {
                    status = "Hết công nợ";
                    goto create_hyperLinks;
                }

                for (int i = LR; i >= 2; i--)//lặp ngược từ dưới lên để tìm điểm hết công nợ cho nhanh
                {
                    if (ws.Range[columnSDDK_NO + LR].Value2 <= saiSoChoPhep && ws.Range[columnSDDK_CO + LR].Value2 <= saiSoChoPhep)
                    {
                        status = "Hết công nợ - Số dư nhỏ";
                        break;
                    }

                    if (ws.Range[columnSDDK_NO + i].Text == "0" && ws.Range[columnSDDK_CO + i].Text == "0")
                    {
                        status = "Công nợ có về 0";
                        break;
                    }
                }

            create_hyperLinks:
                Range newRange = sheetTH.NewRangeAtColumn(sheetTH.GetRangeByText("Mã NCC").GetColumnLetter());
                newRange.SetHeaderText(maNCC, tenNCC, duNoCuoiKy, duCoCuoiKy, status);
                sheetTH.Hyperlinks.Add(newRange, "", ws.Range["a1"].FullAddress(false, true));
                ws.Hyperlinks.Add(ws.Range["a1"], "", newRange.FullAddress(false, true));
            }

            Globals.ThisAddIn.Application.ScreenUpdating = true;
            sheetTH.Columns.AutoFit();
            sheetTH.Range["c1:d1"].EntireColumn.SetAccountingNumberFormat();
            Globals.ThisAddIn.Application.Goto(sheetTH.Range["a1"], true);
            Notification.Wait.Close();
            MsgBox.Show("Thành công" + Environment.NewLine + stopwatch.Elapsed.TotalSeconds);


        }

        private async void button_create_PC_auto_Click(object sender, EventArgs e)
        {

            int[] rows = gridView3_DS_hoadon.GetSelectedRows();
            List<UserClass.Invoice> invoiceInfos = new List<UserClass.Invoice>();

            foreach (int row in rows)
            {
                UserClass.Invoice info = new UserClass.Invoice();
                info.Date = gridView3_DS_hoadon.GetRowCellValue(row, "Ngày HT").ToString();
                info.InvoiceNumber = gridView3_DS_hoadon.GetRowCellValue(row, "Số HĐ").ToString();
                info.CustomerID = gridLookUpEditMaNCC.Text;
                info.CustomerName = gridView3_DS_hoadon.GetRowCellValue(row, "Tên nhà cung cấp").ToString();
                info.TotalAmount = Convert.ToInt32(gridView3_DS_hoadon.GetRowCellValue(row, "Số tiền").ToString());

                var infoFind = invoiceInfos.Find(x => x.Date == gridView3_DS_hoadon.GetRowCellValue(row, "Ngày HT").ToString());
                if (infoFind == null)
                {
                    invoiceInfos.Add(info);
                }
                else
                {
                    infoFind.InvoiceNumber += "+" + gridView3_DS_hoadon.GetRowCellValue(row, "Số HĐ").ToString();
                    infoFind.TotalAmount += Convert.ToInt32(gridView3_DS_hoadon.GetRowCellValue(row, "Số tiền").ToString());
                }

            }


            #region automation part

            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();
                await Task.Run(() =>
                {
                    Core.Start();
                    WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);

                    foreach (var invoiceInfo in invoiceInfos)
                    {
                        WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanPhieuChi(invoiceInfo.Date, invoiceInfo.CustomerID, invoiceInfo.CustomerName, invoiceInfo.InvoiceNumber, invoiceInfo.TotalAmount.ToString());
                    }

                    Notification.Wait.Close();
                    Core.TearDown();
                    MsgBox.Show("Hạch toán PC thành công");

                }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);
            }
            catch (System.OperationCanceledException)
            {
                Notification.Toast.Show("Đã hủy");
            }
            finally
            {
                Core.TearDown();
            }


            #endregion

        }

        private async void button_AutoImport_Click(object sender, EventArgs e)
        {
            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();

                await Task.Run(() =>
                {
                    #region using winappdriver
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
                    WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);
                    WinAppDriver.Vietsea.VietseaAuto.ActiveVIETSEAWindow();

                    //System.Threading.Thread.Sleep(300);
                    //await Task.Delay(300);
                    Notification.Wait.Show("click nghiệp vụ");
                    WinAppDriver.Vietsea.VietseaAuto.menuNghiepVu.MoveAndClick();

                    Notification.Wait.Show("click bán hàng");
                    WinAppDriver.Vietsea.VietseaAuto.menuNghiepVu_dropdown.MoveAndClick(60, 33);//menu bán hàng

                    Notification.Wait.Show("click phiếu xuất bán");
                    WinAppDriver.Vietsea.VietseaAuto.menuNghiepVu_menuItemBanHang_dropdown.MoveAndClick(76, 15);//phiếu xuất bán

                    Notification.Wait.Show("click thêm mới");
                    WinAppDriver.Vietsea.VietseaAuto.buttonThemMoi.Click();

                    Notification.Wait.Show("nhập mã kho xuất");
                    WinAppDriver.Vietsea.VietseaAuto.textboxMaKhoXuat.SetTextByClipboard(PublicVar.last_KhoHang);

                    Notification.Wait.Show("click import");
                    WinAppDriver.Vietsea.VietseaAuto.buttonImportSolomon.Click();

                    WinAppDriver.Vietsea.VietseaAuto.DialogOpen_textboxFileName.Clear();
                    string fileExcel = PublicVar.last_file_import_xuatban_fullname;

                    Notification.Wait.Show("nhập đường dẫn file excel import");
                    WinAppDriver.Vietsea.VietseaAuto.DialogOpen_textboxFileName.SetTextByClipboard(fileExcel);

                    Notification.Wait.Show("click Open");
                    WinAppDriver.Vietsea.VietseaAuto.DialogOpen_buttonOpen.MoveAndClick();

                    ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
                    #endregion

                }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);
            }
            catch (System.OperationCanceledException)
            {
                Notification.Toast.Show("đã hủy");
            }
            finally
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }

        }


        private async void button_load_bangke_muavao_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("bangkemuavao");

            string q = string.Format(@"Select Th.Ngayhachtoan,
                        Th.Kyhieuhoadongd,
                        th.sohoadongd,
                        Th.Ngayhoadongd,
                        Th.Makhachang,
                        dmkh.tentochu,

                        Sum(Ct.Tienhang) As Tienhang,
                        Sum(Ct.Tienvat) As Tienvat,
                        Sum(Ct.Tienhang)+Sum(Ct.Tienvat) as tongtien

                        From Vattugdct Ct
                        Join Vattugd Th
                        On Ct.Mavattugdpk = Th.Mavattugdpk 
                        Join Dmkhachhang Dmkh
                        on th.makhachang = dmkh.makhachhang
                        Where Th.Ngayhachtoan >= '{0}'
                        And Th.Ngayhachtoan <= '{1}'
                        and th.manhomptnx in ('N_MUA','N_DICHVU')
                        and th.trangthai = 10

                        Group By Th.Ngayhachtoan, Th.Kyhieuhoadongd, Th.Sohoadongd, Th.Ngayhoadongd, Th.Makhachang, Dmkh.Tentochu
                        order by Th.Ngayhachtoan,Th.Ngayhoadongd,th.sohoadongd", dtpFromDateVietsea.Value.ToString("dd-MMM-yy"), dtpToDateVietsea.Value.ToString("dd-MMM-yy"));
            DataTable dt = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), q);
            if (dt == null || dt.Rows.Count == 0)
            {
                Notification.Toast.Show("Không có dữ liệu");
                return;
            }

            dt.CopyToRange(ws.Range["a1"]);

            Range kyhieuhoadon_length_header = ws.EntireRow(1).GetHeaderRangeByText("độ dài ký hiệu HĐ");
            kyhieuhoadon_length_header.FirstRow().Formula = $"=LEN({ws.EntireRow(1).GetHeaderRangeByText("KYHIEUHOADONGD").GetColumnLetter()}2)";

            Range sohoadon_makhachhang = ws.EntireRow(1).GetHeaderRangeByText("sohoadon_makhachhang");
            sohoadon_makhachhang.FirstRow().Formula = $"={ws.EntireRow(1).GetHeaderRangeByText("SOHOADONGD").GetColumnLetter()}2&{ws.EntireRow(1).GetHeaderRangeByText("MAKHACHANG").GetColumnLetter()}2";

            Range check_sohoadon_duplicate = ws.EntireRow(1).GetHeaderRangeByText("check sohoadon trùng");
            check_sohoadon_duplicate.FirstRow().Formula = $"=COUNTIF({sohoadon_makhachhang.GetColumnLetter()}:{sohoadon_makhachhang.GetColumnLetter()},{sohoadon_makhachhang.GetColumnLetter()}2)";

            Range makhachhang_tongtien = ws.EntireRow(1).GetHeaderRangeByText("makhachhang_tongtien");
            makhachhang_tongtien.FirstRow().Formula = $"={ws.EntireRow(1).GetHeaderRangeByText("MAKHACHANG").GetColumnLetter()}2&{ws.EntireRow(1).GetHeaderRangeByText("TONGTIEN").GetColumnLetter()}2";

            Range check_tongtien_duplicate = ws.EntireRow(1).GetHeaderRangeByText("check tongtien trùng");
            check_tongtien_duplicate.FirstRow().Formula = $"=COUNTIF({makhachhang_tongtien.GetColumnLetter()}:{makhachhang_tongtien.GetColumnLetter()},{makhachhang_tongtien.GetColumnLetter()}2)";

            ws.Range[kyhieuhoadon_length_header, check_tongtien_duplicate].FirstRow().ResizeToLastRow("a").FillDownFormula(false);

            ws.Columns.AutoFit();
            Notification.Toast.Show("Load bảng kê mua vào thành công");
        }

        private void gridLookUpEditMaNCC_Properties_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (gridLookUpEditMaNCC.EditValue != null)
                {
                    //textBoxMAKHACHHANG.Text = gridLookUpEditMaNCC.EditValue.ToString();
                    DevExpress.XtraEditors.GridLookUpEdit editor = (sender as DevExpress.XtraEditors.GridLookUpEdit);
                    DataRowView row = (DataRowView)gridLookUpEditMaNCC.Properties.GetRowByKeyValue(editor.EditValue);
                    if (row != null)
                    {
                        textBoxMAKHACHHANG.Text = row["TENTOCHU"].ToString();
                    }
                }
            }
            catch { }

        }

        private void gridLookUpEditMaNCC_Click(object sender, EventArgs e)
        {
            gridLookUpEditMaNCC.SelectAll();
        }

        private async void button_Tao_PhieuThu_Auto_Click(object sender, EventArgs e)
        {
            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();
                await Task.Run(() =>
                {
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
                    WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);
                    WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanPhieuThu();
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
                    MsgBox.Show("Hạch toán phiếu thu thành công");
                }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);
            }
            catch (OperationCanceledException)
            {
                Notification.Toast.Show("Đã hủy");
            }
            finally
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }

        }

        private async void button_analyzeStatement_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;

            gridControl_BankStatement.DataSource = Accounting.Banking.BankLib.AnalyzeVietcombankStatement(ws);

            DataTable table = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), "select makhachhang,tentochu from dmkhachhang");

            DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit gridLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            gridLookUpEdit.DataSource = table;
            gridLookUpEdit.EditValueChanged += GridLookUpEdit_EditValueChanged;
            gridLookUpEdit.Popup += GridLookUpEdit_Popup;
            gridLookUpEdit.PopulateViewColumns();
            gridLookUpEdit.DisplayMember = "TENTOCHU";
            gridLookUpEdit.ValueMember = "MAKHACHHANG";
            gridLookUpEdit.SearchMode = DevExpress.XtraEditors.Repository.GridLookUpSearchMode.AutoSuggest;

            gridView_BankStatement.Columns[Accounting.Banking.BankLib.colCustomerName].ColumnEdit = gridLookUpEdit;
            gridControl_BankStatement.RepositoryItems.Add(gridLookUpEdit);

            //gridLookUpEdit.View.Columns.ColumnByName("MAKHACHHANG").Width = 100;
            //gridLookUpEdit.View.Columns.ColumnByName("TENTOCHU").Width = 350;


        }

        private void GridLookUpEdit_Popup(object sender, EventArgs e)
        {
            //DevExpress.XtraEditors.GridLookUpEdit gridLookUpEdit = sender as DevExpress.XtraEditors.GridLookUpEdit;
            ////gridLookUpEdit.Properties.DataSource = (sender as DevExpress.XtraEditors.GridLookUpEdit).Properties.DataSource;
            //DevExpress.XtraEditors.Popup.PopupGridLookUpEditForm form = (gridLookUpEdit as DevExpress.Utils.Win.IPopupControl).PopupWindow as DevExpress.XtraEditors.Popup.PopupGridLookUpEditForm;
            //form.Properties.PopulateViewColumns();
            //form.Properties.View.Columns["MAKHACHHANG"].Width = 300;
            //form.Properties.View.Columns["TENTOCHU"].Width = 1000;
            //// form.Size = new System.Drawing.Size(1300, 300);
        }



        private void GridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            //DevExpress.XtraEditors.GridLookUpEdit view = (DevExpress.XtraEditors.GridLookUpEdit)sender;
            int rowHandle = gridView_BankStatement.FocusedRowHandle;

            var edit = sender as DevExpress.XtraEditors.GridLookUpEdit;
            if (edit.EditValue != null)
            {
                gridView_BankStatement.SetRowCellValue(rowHandle, Accounting.Banking.BankLib.colCustomerID, edit.EditValue.ToString());
                //textBoxMAKHACHHANG.Text = edit.EditValue.ToString();
            }
        }

        private async void button_AutoCreateBankingTransantions_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;

            int[] checkedRows = gridView_BankStatement.GetCheckedRows();
            if (checkedRows.Length == 0)
            {
                MsgBox.Show("Chưa tích chọn dòng nào");
                return;
            }

            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();
                //ExcelAddIn1.Notification.Wait.Show("");
                await Task.Run(() =>
                {

                    ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
                    WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);

                    bool checkEmpty(params string[] itemArray)
                    {
                        foreach (string item in itemArray)
                        {
                            if (string.IsNullOrEmpty(item))
                            {
                                return true;
                            }
                        }
                        return false;
                    }

                    void setColor(string description)
                    {
                        Range foundrange = ws.UsedRange.Find(description);
                        foundrange.EntireRow.Interior.Color = 5296274;
                    }

                    foreach (int checkedrow in checkedRows)
                    {
                        string tranDate = (string)ExcelAddIn1.MyClass.Extensions.DevExpressExtensions.GetControlValue(gridView_BankStatement.GetRowCellValue(checkedrow, Accounting.Banking.BankLib.colDateTime));
                        string tranType = (string)ExcelAddIn1.MyClass.Extensions.DevExpressExtensions.GetControlValue(gridView_BankStatement.GetRowCellValue(checkedrow, Accounting.Banking.BankLib.colType));
                        string desciption = (string)ExcelAddIn1.MyClass.Extensions.DevExpressExtensions.GetControlValue(gridView_BankStatement.GetRowCellValue(checkedrow, Accounting.Banking.BankLib.colDescription));
                        string amount = (string)ExcelAddIn1.MyClass.Extensions.DevExpressExtensions.GetControlValue(gridView_BankStatement.GetRowCellValue(checkedrow, Accounting.Banking.BankLib.colAmount));
                        string fee = (string)ExcelAddIn1.MyClass.Extensions.DevExpressExtensions.GetControlValue(gridView_BankStatement.GetRowCellValue(checkedrow, Accounting.Banking.BankLib.colFee));
                        string customerID = (string)ExcelAddIn1.MyClass.Extensions.DevExpressExtensions.GetControlValue(gridView_BankStatement.GetRowCellValue(checkedrow, Accounting.Banking.BankLib.colCustomerID));
                        string customerName = (string)ExcelAddIn1.MyClass.Extensions.DevExpressExtensions.GetControlValue(gridView_BankStatement.GetRowCellDisplayText(checkedrow, Accounting.Banking.BankLib.colCustomerName));

                        if (tranType == null)
                        {
                            continue;
                        }

                        if (tranType == Accounting.Banking.TransactionType.GiaoDichThe)
                        {
                            if (!checkEmpty(tranDate, fee, amount))
                            {
                                WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanGDThe(Accounting.Banking.Name.Vietcombank, tranDate, Accounting.Banking.AccountNumber.Vietcombank, fee, amount);
                                setColor(desciption);
                            }
                        }

                        if (tranType == Accounting.Banking.TransactionType.KhachTraTien)
                        {
                            if (!checkEmpty(tranDate, customerID, customerName, amount))
                            {
                                WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanKhachHangTraTien(tranDate, Accounting.Banking.AccountNumber.Vietcombank, customerID, customerName, amount);
                                setColor(desciption);
                            }
                        }

                        if (tranType == Accounting.Banking.TransactionType.InternetBankingVCB)
                        {
                            if (!checkEmpty(tranDate, customerID, customerName, amount, fee))
                            {
                                WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanIBVCB(tranDate, Accounting.Banking.AccountNumber.Vietcombank, customerID, customerName, amount, fee);
                                setColor(desciption);
                            }
                        }

                        if (tranType == Accounting.Banking.TransactionType.PhiGiaiNganVCB)
                        {
                            if (!checkEmpty(tranDate, amount))
                            {
                                WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanPhiGiaiNgan(tranDate, "6427", Accounting.Banking.AccountNumber.Vietcombank, amount);
                                setColor(desciption);
                            }
                        }

                    }


                    ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
                    MsgBox.Show("Hạch toán tự động xong!");
                }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);
            }
            catch (System.OperationCanceledException)
            {
                Notification.Toast.Show("Đã hủy");
            }
            finally
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }


        }

        private async void button_AutoCreateBillRetail_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("Bán lẻ");

            List<ExcelAddIn1.UserClass.Good> goods = new List<UserClass.Good>();

            int LR = ws.LastRow("a");
            for (int i = 2; i <= LR; i++)
            {
                ExcelAddIn1.UserClass.Good good = new UserClass.Good();

                double sumStockQty = ws.Range["h" + i].GetValue(ValueType.Double)
                                    + ws.Range["i" + i].GetValue(ValueType.Double)
                                    + ws.Range["j" + i].GetValue(ValueType.Double)
                                    + ws.Range["k" + i].GetValue(ValueType.Double);

                string goodid = ws.Range["c" + i].GetStringValue() == "#N/A" ? string.Empty : ws.Range["c" + i].GetStringValue();
                double expQty = Math.Min(ws.Range["d" + i].GetValue(ValueType.Double), sumStockQty);
                double price = ws.Range["e" + i].GetValue(ValueType.Double);

                if (string.IsNullOrEmpty(goodid) || expQty == 0)
                {
                    continue;
                }

                good.GoodID = goodid;
                good.Quantity = expQty;
                good.Price = price;

                goods.Add(good);
            }

            if (goods.Count == 0)
            {
                MsgBox.Show("Không có mặt hàng thỏa mãn để nhập vào phần mềm");
                return;
            }

            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();

                await Task.Run(() =>
                {
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
                    WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);
                    WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanBillXuatChoKhach(goods);
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();

                    Notification.Toast.Show("Nhập mã hàng thành công");
                }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);

            }
            catch (System.OperationCanceledException)
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
                Notification.Toast.Show("Đã hủy");
            }
            finally
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }
        }

        private void buttonCheckMST_Click(object sender, EventArgs e)
        {
            ExcelAddIn1.AutoTest.Selenium.TraCuuMST.TraCuuMST.TraCuuMSTDoanhNghiep("0106188175");

        }

        private void gridLookUpEditMaNCC_Popup(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.GridLookUpEdit edit = sender as DevExpress.XtraEditors.GridLookUpEdit;
            if (edit != null)
            {
                DevExpress.XtraEditors.Popup.PopupGridLookUpEditForm f = (edit as DevExpress.Utils.Win.IPopupControl).PopupWindow as DevExpress.XtraEditors.Popup.PopupGridLookUpEditForm;
                if (f != null)
                {
                    f.Width = 430;
                    f.Properties.View.Columns["MAKHACHHANG"].Width = 100;
                }
            }
        }

        private void buttonLoadFileImportIhoadon_Click(object sender, EventArgs e)
        {
            string path = System.IO.Path.Combine(PublicVar.AddinLocation, "Resources", "Efy_iHoadon_FileMau.xlsx");
            System.Diagnostics.Process.Start(path);
        }

        private void buttonUploadEinvoiceThanhCong_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> listInvoiceNumber = new List<string>();
                List<UserClass.Invoice> listInvoiceInfo = new List<UserClass.Invoice>();

                int[] checkedRows = gridView_EinvoiceList.GetCheckedRows();

                foreach (int item in checkedRows)
                {
                    UserClass.Invoice info = new UserClass.Invoice();
                    info.InvoiceNumber = gridView_EinvoiceList.GetRowCellValue(item, "Số hóa đơn").ToString();
                    info.CustomerName = gridView_EinvoiceList.GetRowCellValue(item, "Tên khách hàng").ToString();
                    listInvoiceInfo.Add(info);
                }

                Task.Run(() =>
                {
                    try
                    {
                        ExcelAddIn1.AutoTest.Selenium.EFY.EFYAuto efy = new AutoTest.Selenium.EFY.EFYAuto();
                        string mst = AddinConfig.EFYmst;
                        string username = AddinConfig.EFYusername;
                        string password = AddinConfig.EFYpassword;
                        efy.SignIn(mst, username, password);
                        efy.CloseNotification78();
                        string path = folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderPath;
                        efy.ImportInvoiceThanhCong(listInvoiceInfo, path);
                        efy.Exit();
                        MsgBox.Show("Upload hóa đơn điện tử thành công!");
                    }
                    catch (Exception e1)
                    {
                        MsgBox.Show(e1.Message);
                    }
                });
            }
            catch (Exception ex)
            {
                MsgBox.Show(ex.Message);
            }
        }

        private async void buttonCheckMAKHOXUAT_TAIKHOAN_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("Check Kho - TK");

            string fromDate = dtpFromDateVietsea.Value.ToString("dd-MMM-yy");
            string toDate = dtpToDateVietsea.Value.ToString("dd-MMM-yy");

            string sql = string.Format(@"select 
th.ngayhachtoan,
ct.sohoadon,
ct.mavtu,
ct.tenvtu,
ct.makhoxuat,
ct.matkvtu,
ct.makhoxuat || ct.matkvtu as str

from vattugdct ct
join vattugd th
on ct.mavattugdpk = th.mavattugdpk
where
th.manhomptnx = 'X_BAN'
and th.ngayhachtoan >= '{0}'
and th.ngayhachtoan <= '{1}'
and ct.makhoxuat || ct.matkvtu not in ('KHO 01561', 'KHO 051561', 'KHO 101561','KHO 8 LAO CAI1561', 'KHO 0 SA PA1563', 'KHO 05 SA PA1563', 'KHO 10 SA PA1563','KHO 8 SA PA1563', 'KHO 0 BAO THANG1562', 'KHO 05 BAO THANG1562', 'KHO 10 BAO THANG1562','KHO 8 BAO THANG1562')
order by ct.sohoadon,ct.mavtu", fromDate, toDate);
            DataTable dt = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), sql);
            if (dt.HasData())
            {
                dt.CopyToRange(ws.Range["a1"]);
                ws.Columns.AutoFit();
                Notification.Toast.Show("Check thành công");
            }
            else if (!dt.HasData())
            {
                Notification.Toast.Show("Không có sai sót");
            }
        }

        private async void buttonCheckMaKhoNhap_maVAT_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetNewSheet("Check Kho - VAT");

            string fromDate = dtpFromDateVietsea.Value.ToString("dd-MMM-yy");
            string toDate = dtpToDateVietsea.Value.ToString("dd-MMM-yy");

            string sql = string.Format(@"Select 
to_char(th.ngayhachtoan,'dd/MM/yyyy') as ngayhachtoan,
Th.Soctugoc,
to_char(Th.Ngayhoadongd,'dd/MM/yyyy') as ngayhoadon,
kh.makhachhang,
Kh.Tenkhachhang,
Ct.Mavtu,
Ct.Tenvtu,
Ct.Vat||ct.makhonhap

From Vattugdct Ct
Join Vattugd Th
On Ct.Mavattugdpk = Th.Mavattugdpk
Join Dmkhachhang Kh
on th.makhachang = kh.makhachhang
Where
Th.Maptnx = 'NMUA'
and Th.Ngayhoadongd >= '{0}'
And Th.Ngayhoadongd <= '{1}'
And Ct.Vat||Ct.Makhonhap Not In ('5BAN05','0BAN05','10BAN10','10K-HANG01','5K-HANG5','0K-HANG5','8BAN8','8K-HANG8','0K-HANG0')", fromDate, toDate);
            DataTable dt = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), sql);
            if (dt.HasData())
            {
                dt.CopyToRange(ws.Range["a1"]);
                ws.Columns.AutoFit();
                Notification.Toast.Show("Check thành công");
            }
            else if (!dt.HasData())
            {
                Notification.Toast.Show("Không có dữ liệu");
            }

        }

        private async void buttonViewImpHistory_Click(object sender, EventArgs e)
        {
            try
            {
                string startDate = dtpFromDateVietsea.Value.ToString("dd-MMM-yy");
                string endDate = dtpToDateVietsea.Value.ToString("dd-MMM-yy");

                string tableXNT = $"XNT_{dtpFromDateVietsea.Value.ToString("ddMMyyyy")}";
                string mavtu = textBoxProduct_codes.Text;
                string queryTonDauKy = string.Format(@"Select
    Sum({0}.TONDAUKYSL) As Sum_TONDAUKYSL
From
    {0}
Where
    {0}.MAVTU = '{1}'
Group By
    {0}.MAVTU", tableXNT, mavtu);
                var dtTondauky = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), queryTonDauKy);

                double SLtondauky = 0;
                if (dtTondauky != null && dtTondauky.Rows.Count > 0)
                {
                    SLtondauky = Convert.ToDouble(dtTondauky.Rows[0][0]);
                }

                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sh = wb.GetNewSheet(textBoxProduct_codes.Text);
                string[] arr = (((string)textBoxProduct_codes.GetText()).Split(','));
                List<string> list = arr.ToList();
                string productCodeWhereIn = list.ToSQLWhereIn();



                string q = string.Format(@"Select
    To_Char(VATTUGD.NGAYHACHTOAN,'DD-MM-YYYY') as NGAYHACHTOAN,
    VATTUGD.MAPTNX,
    VATTUGD.SOHOADONGD,
    VATTUGDCT.MAVTU,
    DMVTHHDV.TENVTU,
    VATTUGDCT.SOLUONG,
    VATTUGDCT.DONGIA,
    VATTUGDCT.TIENHANG,
    VATTUGDCT.VAT,
    VATTUGD.MAKHACHANG,
    DMKHACHHANG.TENTOCHU
From
    VATTUGD Inner Join
    VATTUGDCT On VATTUGDCT.MAVATTUGDPK = VATTUGD.MAVATTUGDPK Inner Join
    DMVTHHDV On DMVTHHDV.MAVTU = VATTUGDCT.MAVTU Inner Join
    DMKHACHHANG On DMKHACHHANG.MAKHACHHANG = VATTUGD.MAKHACHANG
Where
    VATTUGD.NGAYHACHTOAN >= '{0}' And
    VATTUGD.NGAYHACHTOAN <= '{1}' And
    VATTUGD.MAPTNX In ('NMUA', 'XBAN','XTRA1','XTRA') And
    VATTUGDCT.MAVTU In ({2}) And
    VATTUGD.TRANGTHAI = 10
Order By
    VATTUGD.NGAYHACHTOAN,
    VATTUGD.MAPTNX", startDate, endDate, productCodeWhereIn);


                DataTable dt = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), q);
                if (dt != null && dt.Rows.Count > 0)
                {
                    dt.CopyToRange(sh.Range["a1"], true);
                    //sh.Columns.AutoFit();
                    Globals.ThisAddIn.Application.Goto(sh.Range["a1"], true);
                    ListObject listObject = sh.UsedRange.ToListObject();
                    listObject.GetColumnDataBodyRange("SOLUONG").ConvertToValue();
                    listObject.GetColumnDataBodyRange("SOLUONG").SetAccountingNumberFormat();
                    listObject.GetColumnDataBodyRange("DONGIA").ConvertToValue();
                    listObject.GetColumnDataBodyRange("DONGIA").SetAccountingNumberFormat();
                    listObject.GetColumnDataBodyRange("TIENHANG").ConvertToValue();
                    listObject.GetColumnDataBodyRange("TIENHANG").SetAccountingNumberFormat();
                    listObject.GetColumnDataBodyRange("VAT").ConvertToValue();
                    listObject.GetColumnDataBodyRange("VAT").SetAccountingNumberFormat();

                    sh.NewColumnAtRow(1).Value2 = "SL tồn";
                    sh.NewColumnAtRow(1).Value2 = SLtondauky;
                    sh.Range["M2"].Formula = "=IF(LEFT(B2,1)=\"X\",-1,1)*F2+M1";
                    sh.UsedRange.Columns.AutoFit();
                    listObject.Unlist();
                    Notification.Toast.Show("Thành công");
                }
                else
                {
                    Notification.Toast.Show("Không có dữ liệu");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private async void buttonLoadGoodsTC_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine($"select ");
            sb.AppendLine($"Goods.GoodID, ");
            sb.AppendLine($"Goods.ShortName, ");
            sb.AppendLine($"Goods.ExpRetailPriceVat ");
            sb.AppendLine($"from ");
            sb.AppendLine($"Goods");
            DataTable dataTable = await ADO.GetDataTableFromSQLServerAsync(GetConnectionString(), sb.ToString());
            if (dataTable.HasData())
            {
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet ws = wb.GetSheet("Goods");
                dataTable.CopyToRange(ws.Range["a1"]);
                //ws.Columns.AutoFit();
                ws.Range["c1"].EntireColumn.ConvertToValue().SetAccountingNumberFormat();
                Notification.Toast.Show("Xuất danh mục hàng hóa thành công!");
            }

        }

        private async void buttonLoadDMVTHHvietsea_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine($"select");
            sb.AppendLine($"Dmvthhdv.Mavtu,");
            sb.AppendLine($"dmvthhdv.tenvtu");
            sb.AppendLine($"from");
            sb.AppendLine($"dmvthhdv");

            DataTable dataTable = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), sb.ToString());
            if (dataTable.HasData())
            {
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet ws = wb.GetSheet("DMVTHHDV");
                dataTable.CopyToRange(ws.Range["a1"]);
                //ws.Columns.AutoFit();
                Notification.Toast.Show("Xuất danh mục hàng hóa Vietsea thành công!");
            }
        }

        private async void buttonUploadInvoiceOneByOne_Click(object sender, EventArgs e)
        {
            string dateTime = DateTime.Now.ToString("dd/MM/yyyy");
            string path = folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderPath;

            await Task.Run(() =>
            {

                ExcelAddIn1.AutoTest.Selenium.EFY.EFYAuto eFYAuto = new AutoTest.Selenium.EFY.EFYAuto();
                eFYAuto.Signin();
                eFYAuto.UploadInvoice(dateTime, "KLE000001", path, 40);

                Notification.Toast.Show("Test thành công");
                Notification.Wait.Close();
            });
            //eFYAuto.Exit();
        }

        private async void buttonViewKhoVAT_Click(object sender, EventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("KHO-VAT");
            DataTable dt = await VIETSEA.TBNETERP.DATATABLE_XNT_KHO_VAT(GetConnectionStringOracle(), dtpToDateVietsea.Value);
            if (dt.HasData())
            {
                dt.CopyToRange(ws.Range["a1"]);
                ws.Columns.AutoFit();
                Notification.Toast.Show("Thành công!");

            }
        }

        private async void buttonTaoDonDatHang_Click(object sender, EventArgs e)
        {
            try
            {
                int[] selectedRows = gridView_EinvoiceList.GetSelectedRows();
                if (selectedRows.Length == 0)
                {
                    MessageBox.Show(this, "Chưa chọn hóa đơn nào");
                    return;
                }

                foreach (int row in selectedRows)
                {
                    string MAVATTUGDPK = gridView_EinvoiceList.GetRowCellValue(row, "MAVATTUGDPK").ToString();
                    await CreateDonDatHang(MAVATTUGDPK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }

        }

        private async Task CreateDonDatHang(string MAVATTUGDPK)
        {
            try
            {
                string qDetail = string.Format(@"Select
    RowNum As STT,
    VATTUGDCT.MAVTU As ""Mã hàng"",
    DMVTHHDV.TENVTU As ""Tên hàng"",
    DMDONVITINHDV.DONVILE As ""ĐVT"",
    VATTUGDCT.SOLUONG As ""Số lượng"",
    DMKHACHHANG.TENTOCHU,
    DMKHACHHANG.DIACHITOCHU,
    VATTUGD.NGAYHACHTOAN,
    VATTUGD.SOHOADONGD
From
    VATTUGDCT Inner Join
    VATTUGD On VATTUGD.MAVATTUGDPK = VATTUGDCT.MAVATTUGDPK Inner Join
    DMVTHHDV On DMVTHHDV.MAVTU = VATTUGDCT.MAVTU Inner Join
    DMDONVITINHDV On DMDONVITINHDV.MADVTINH = VATTUGDCT.MADVTINH Inner Join
    DMKHACHHANG On DMKHACHHANG.MAKHACHHANG = VATTUGD.MAKHACHANG
Where
    VATTUGD.MAVATTUGDPK = '{0}'", MAVATTUGDPK);
                DataTable dtDetail = await ADO.GetDataTableFromOracleServerAsync(GetConnectionStringOracle(), qDetail).ConfigureAwait(false);
                if (!dtDetail.HasData())
                {
                    return;
                }

                string tenKhachHang = dtDetail.Rows[0]["TENTOCHU"].ToString();
                string diaChi = dtDetail.Rows[0]["DIACHITOCHU"].ToString();

                int dayStep = new Random().Next(3, 7);
                DateTime ngayHachToan = (DateTime)dtDetail.Rows[0]["NGAYHACHTOAN"];
                DateTime ngayDatHang = ngayHachToan.AddDays(dayStep * (-1));
                string ngayDatHangFull = $"Ngày {ngayDatHang.Day.ToString().PadLeft(2, '0')} tháng {ngayDatHang.Month.ToString().PadLeft(2, '0')} năm {ngayDatHang.Year.ToString()}";
                string sheetName = dtDetail.Rows[0]["SOHOADONGD"].ToString();

                Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet worksheet = workbook.GetNewSheet(sheetName);

                worksheet.Cells.Font.Size = 12;
                worksheet.Cells.Font.Name = "Times New Roman";
                worksheet.Cells.RowHeight = 17;

                worksheet.Range["a1"].Value2 = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM";
                worksheet.Range["a1"].Font.Bold = true;
                worksheet.Range["a1"].Resize[1, 5].MergeAndCenter();

                worksheet.Range["a2"].Value2 = "Độc lập - Tự do - Hạnh Phúc";
                worksheet.Range["a2"].Font.Bold = true;
                worksheet.Range["a2"].Resize[1, 5].MergeAndCenter();
                worksheet.Range["a3"].Value2 = "----------------------";
                worksheet.Range["a3"].Resize[1, 5].MergeAndCenter();

                worksheet.Range["a5"].Value2 = "ĐƠN ĐẶT HÀNG";
                worksheet.Range["a5"].Font.Bold = true;
                worksheet.Range["a5"].Font.Size = 16;
                worksheet.Range["a5"].Resize[1, 5].MergeAndCenter();

                worksheet.Range["a6"].Value2 = $"Số: {new Random().Next(1, 60).ToString().PadLeft(5, '0')}";
                worksheet.Range["a6"].Resize[1, 5].MergeAndCenter();

                worksheet.Range["a8"].Value2 = $"Kính gửi: Công ty Cổ phần Đạt Phát Hà Nội";
                worksheet.Range["a8"].Font.Bold = true;
                worksheet.Range["a8"].Resize[1, 5].MergeAndCenter();
                worksheet.Range["a8"].HorizontalAlignment = XlHAlign.xlHAlignLeft;

                worksheet.Range["a9"].Value2 = $"{tenKhachHang} có nhu cầu đặt hàng tại quý nhà cung cấp theo mẫu yêu cầu.";
                worksheet.Range["a9"].Resize[1, 5].MergeAndCenter();
                worksheet.Range["a9"].HorizontalAlignment = XlHAlign.xlHAlignLeft;
                worksheet.Range["a9"].RowHeight *= 2;
                worksheet.Range["a9"].WrapText = true;

                worksheet.Range["a10"].Value2 = $"Nội dung đặt hàng như sau:";
                worksheet.Range["a10"].HorizontalAlignment = XlHAlign.xlHAlignLeft;

                DataTable dtNoiDung = dtDetail.ToDataTableSelectedColumns("STT", "Mã hàng", "Tên hàng", "ĐVT", "Số lượng");
                dtNoiDung.CopyToRange(worksheet.Range["a12"]);
                worksheet.Range["a12"].EntireRow.Font.Bold = true;
                worksheet.Range["a12"].CurrentRegion.SetBorder();

                worksheet.Range["b1"].EntireColumn.ColumnWidth = 12;
                worksheet.Range["c1"].EntireColumn.ColumnWidth = 60;
                worksheet.Range["d1"].EntireColumn.ColumnWidth = 10;

                int LR = worksheet.LastRow("a");
                Range ngaythangRange = worksheet.Range["e" + (LR + 2)];
                ngaythangRange.Value2 = ngayDatHangFull;
                ngaythangRange.Font.Italic = true;
                ngaythangRange.HorizontalAlignment = XlHAlign.xlHAlignRight;

                int nguoiLapRow = LR + 4;
                worksheet.Range["b" + nguoiLapRow].SetHeaderText("Người lập", "Kế toán trưởng", "Giám đốc");
                worksheet.Range["b" + nguoiLapRow].EntireRow.Font.Bold = true;
                worksheet.Range["b" + nguoiLapRow].EntireRow.HorizontalAlignment = XlHAlign.xlHAlignCenter;

                worksheet.PageSetup.PrintArea = worksheet.UsedRange.Address;

                Globals.ThisAddIn.Application.PrintCommunication = false;
                worksheet.PageSetup.PaperSize = XlPaperSize.xlPaperA4;
                worksheet.PageSetup.Zoom = false;
                worksheet.PageSetup.FitToPagesWide = 1;
                worksheet.PageSetup.FitToPagesTall = false;
                Globals.ThisAddIn.Application.PrintCommunication = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Globals.ThisAddIn.Application.PrintCommunication = true;
            }
        }
    }
}
