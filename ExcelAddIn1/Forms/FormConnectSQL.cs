﻿using System.Windows.Forms;
using VSTOLib;

namespace ExcelAddIn1.Forms
{
    public partial class FormConnectSQL : Form
    {
        public FormConnectSQL()
        {
            InitializeComponent();
            Init();
        }

        void Init()
        {
            comboBoxStation.DataSource = ServerConnection.Server.ServerList;
            comboBoxStation.DisplayMember = "Name";
            textBoxIP.Text = ServerConnection.Server.ServerList.Rows[comboBoxStation.SelectedIndex]["IP"].ToString();
            textBoxUsername.Text = ServerConnection.Server.ServerList.Rows[comboBoxStation.SelectedIndex]["User"].ToString();
            textBoxPassword.Text = ServerConnection.Server.ServerList.Rows[comboBoxStation.SelectedIndex]["Pass"].ToString();
            this.StartPosition = FormStartPosition.Manual;
        }

        static string _connectionString = string.Empty;
        public string GetConnectionString()
        {
            FormConnectSQL formConnectDatabase = new FormConnectSQL();
            formConnectDatabase.Location = System.Windows.Forms.Cursor.Position;
            formConnectDatabase.ShowDialog();
            return _connectionString;

        }

        private async void buttonConnect_Click(object sender, System.EventArgs e)
        {
            System.Data.SqlClient.SqlConnectionStringBuilder stringBuilder = new System.Data.SqlClient.SqlConnectionStringBuilder();
            stringBuilder.UserID = textBoxUsername.Text;
            stringBuilder.Password = textBoxPassword.Text;
            stringBuilder.DataSource = textBoxIP.Text;
            stringBuilder.InitialCatalog = comboBoxDataBaseName.Text;

            _connectionString = stringBuilder.ConnectionString;

            if (await ADO.TestSQLConnection(_connectionString))
            {
                MessageBox.Show(this, "Kết nối thành công", "Thông báo");
                Dispose();
            }
            else
            {
                MessageBox.Show(this,"Kết nối không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void comboBoxStation_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            textBoxIP.Text = ServerConnection.Server.ServerList.Rows[comboBoxStation.SelectedIndex]["IP"].ToString();
            textBoxUsername.Text = ServerConnection.Server.ServerList.Rows[comboBoxStation.SelectedIndex]["User"].ToString();
            textBoxPassword.Text = ServerConnection.Server.ServerList.Rows[comboBoxStation.SelectedIndex]["Pass"].ToString();

            comboBoxDataBaseName.SelectedIndex = -1;
            comboBoxDataBaseName.Items.Clear();
        }

        private void comboBoxDataBaseName_DropDown(object sender, System.EventArgs e)
        {
            System.Data.SqlClient.SqlConnectionStringBuilder sb = new System.Data.SqlClient.SqlConnectionStringBuilder(_connectionString);
            sb.DataSource = textBoxIP.Text;
            sb.UserID = textBoxUsername.Text;
            sb.Password = textBoxPassword.Text;
            ADO.AddDataBaseNameToComboBox(sb.DataSource, sb.UserID, sb.Password, comboBoxDataBaseName);
        }
    }
}
