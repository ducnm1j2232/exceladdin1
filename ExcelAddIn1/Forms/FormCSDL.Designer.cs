﻿
namespace ExcelAddIn1.Forms
{
    partial class FormCSDL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtpToDate = new ExcelAddIn1.CustomDateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFromDate = new ExcelAddIn1.CustomDateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxDBName = new System.Windows.Forms.ComboBox();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.comboBoxZone = new System.Windows.Forms.ComboBox();
            this.ButtonHideForm = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonViewKhoVAT = new System.Windows.Forms.Button();
            this.gridControlKHO = new DevExpress.XtraGrid.GridControl();
            this.gridViewKHO = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.buttonLoadDMVTHHvietsea = new System.Windows.Forms.Button();
            this.buttonLoadGoodsTC = new System.Windows.Forms.Button();
            this.buttonViewImpHistory = new System.Windows.Forms.Button();
            this.buttonCheckMaKhoNhap_maVAT = new System.Windows.Forms.Button();
            this.buttonCheckMAKHOXUAT_TAIKHOAN = new System.Windows.Forms.Button();
            this.button_check_xnt_vietsea = new System.Windows.Forms.Button();
            this.button_LoadDSKhoVietsea = new System.Windows.Forms.Button();
            this.buttonDownloadBANGMA = new System.Windows.Forms.Button();
            this.buttonLoadTonCuoi_v2 = new System.Windows.Forms.Button();
            this.buttonGetDataExportSTLTTAN = new System.Windows.Forms.Button();
            this.buttonViewProductDetail = new System.Windows.Forms.Button();
            this.textBoxProduct_codes = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.checkBoxBoMaDaXuat = new System.Windows.Forms.CheckBox();
            this.buttonLocRaMaHangThua = new System.Windows.Forms.Button();
            this.checkBoxBoHangXuatTra = new System.Windows.Forms.CheckBox();
            this.buttonCalculateGoodsAvailable = new System.Windows.Forms.Button();
            this.buttonExportFileBarcodeToPrint = new System.Windows.Forms.Button();
            this.buttonCheckNegativeStockEveryDay = new System.Windows.Forms.Button();
            this.buttonGetStockInfo = new System.Windows.Forms.Button();
            this.comboBoxSheetName2 = new ExcelAddIn1.MyUserControl.ComboBoxSheetName();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox_RoundExpPrice = new System.Windows.Forms.CheckBox();
            this.button_AutoImport = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.buttonQuickViewRetail = new System.Windows.Forms.Button();
            this.radioButtonKHACHLE = new System.Windows.Forms.RadioButton();
            this.buttonApplyPrice = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.radioButtonMember = new System.Windows.Forms.RadioButton();
            this.buttonGetDataExportInner = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvTiLeLai = new System.Windows.Forms.DataGridView();
            this.customComboBoxVATrate = new ExcelAddIn1.MyUserControl.CustomComboBox();
            this.comboBoxCustomersID = new ExcelAddIn1.MyUserControl.CustomComboBox();
            this.folderBroswerTextBoxSaveImportFileLocation = new ExcelAddIn1.MyUserControl.FolderBroswerTextBox();
            this.comboBox_sheet_tồn = new ExcelAddIn1.MyUserControl.ComboBoxSheetName();
            this.textBox_số_tiền_random = new ExcelAddIn1.CustomTextBox();
            this.textBox_SoCT = new ExcelAddIn1.CustomTextBox();
            this.textBox_CustomerID = new ExcelAddIn1.CustomTextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button_AutoCreateBillRetail = new System.Windows.Forms.Button();
            this.button_TachBill = new System.Windows.Forms.Button();
            this.checkBoxHienTonVIETSEA = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonGetRetailTranDetail = new System.Windows.Forms.Button();
            this.textBoxTypeTranID = new ExcelAddIn1.CustomTextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.buttonTaoDonDatHang = new System.Windows.Forms.Button();
            this.buttonUploadInvoiceOneByOne = new System.Windows.Forms.Button();
            this.buttonUploadEinvoiceThanhCong = new System.Windows.Forms.Button();
            this.buttonLoadFileImportIhoadon = new System.Windows.Forms.Button();
            this.buttonCheckMST = new System.Windows.Forms.Button();
            this.button_load_bangke_muavao = new System.Windows.Forms.Button();
            this.gridControl_EinvoiceList = new DevExpress.XtraGrid.GridControl();
            this.gridView_EinvoiceList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.button3_UploadEinvoiceSelenium = new System.Windows.Forms.Button();
            this.button_download_PDF_einvoice_by_selenium = new System.Windows.Forms.Button();
            this.button_LoadBangKeBanRa = new System.Windows.Forms.Button();
            this.button_DownloadInvoiceInfoToCheck = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.button_CreateFileImport_Einvoice = new System.Windows.Forms.Button();
            this.folderBroswerTextBox_SaveLocation_import_iHoaDon = new ExcelAddIn1.MyUserControl.FolderBroswerTextBox();
            this.button_Load_XBAN = new System.Windows.Forms.Button();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button_load_makhachhang_vietsea = new System.Windows.Forms.Button();
            this.gridLookUpEditMaNCC = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEditMaNCCView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl_DS_Hoadon_vietsea = new DevExpress.XtraGrid.GridControl();
            this.gridView3_DS_hoadon = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.button_create_PC_auto = new System.Windows.Forms.Button();
            this.textBoxMAKHACHHANG = new System.Windows.Forms.TextBox();
            this.button_BC_S38 = new System.Windows.Forms.Button();
            this.button_TH_CONGNO_331 = new System.Windows.Forms.Button();
            this.buttonLoadInvoice = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonGetDataToMakeReceipt = new System.Windows.Forms.Button();
            this.button_Tao_PhieuThu_Auto = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button_AutoCreateBankingTransantions = new System.Windows.Forms.Button();
            this.button_analyzeStatement = new System.Windows.Forms.Button();
            this.gridControl_BankStatement = new DevExpress.XtraGrid.GridControl();
            this.gridView_BankStatement = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.button_download_saoke_vietcombank = new System.Windows.Forms.Button();
            this.button_download_saoke_techcombank = new System.Windows.Forms.Button();
            this.buttonFastReportVCB_RevenueGroupByBranch = new System.Windows.Forms.Button();
            this.buttonFastReportVCB_NXT = new System.Windows.Forms.Button();
            this.dgvBranchIDs = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxNumberOfRowToGet = new System.Windows.Forms.TextBox();
            this.buttonExecute = new System.Windows.Forms.Button();
            this.textBoxOracleQuery = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox_Port_Oracle = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox_IP_Oracle = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpFromDateVietsea = new ExcelAddIn1.CustomDateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpToDateVietsea = new ExcelAddIn1.CustomDateTimePicker();
            this.checkBox_pinForm = new System.Windows.Forms.CheckBox();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.checkBoxGetRetailPrice = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlKHO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewKHO)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTiLeLai)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_EinvoiceList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_EinvoiceList)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMaNCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMaNCCView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_DS_Hoadon_vietsea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3_DS_hoadon)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_BankStatement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_BankStatement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBranchIDs)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtpToDate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpFromDate);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBoxDBName);
            this.groupBox1.Controls.Add(this.textBoxIP);
            this.groupBox1.Controls.Add(this.comboBoxZone);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(182, 163);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thành Công";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(92, 121);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(79, 20);
            this.dtpToDate.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(90, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "đến ngày";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Dữ liệu";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "IP";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(9, 121);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(79, 20);
            this.dtpFromDate.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Từ ngày";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Đơn vị";
            // 
            // comboBoxDBName
            // 
            this.comboBoxDBName.FormattingEnabled = true;
            this.comboBoxDBName.Location = new System.Drawing.Point(50, 68);
            this.comboBoxDBName.Name = "comboBoxDBName";
            this.comboBoxDBName.Size = new System.Drawing.Size(121, 21);
            this.comboBoxDBName.TabIndex = 5;
            this.comboBoxDBName.DropDown += new System.EventHandler(this.comboBoxDBName_DropDown);
            // 
            // textBoxIP
            // 
            this.textBoxIP.Location = new System.Drawing.Point(50, 44);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(121, 20);
            this.textBoxIP.TabIndex = 3;
            // 
            // comboBoxZone
            // 
            this.comboBoxZone.FormattingEnabled = true;
            this.comboBoxZone.Location = new System.Drawing.Point(50, 19);
            this.comboBoxZone.Name = "comboBoxZone";
            this.comboBoxZone.Size = new System.Drawing.Size(121, 21);
            this.comboBoxZone.TabIndex = 1;
            this.comboBoxZone.DropDown += new System.EventHandler(this.comboBoxZone_DropDown);
            this.comboBoxZone.SelectedIndexChanged += new System.EventHandler(this.comboBoxZone_SelectedIndexChanged);
            // 
            // ButtonHideForm
            // 
            this.ButtonHideForm.Location = new System.Drawing.Point(728, 526);
            this.ButtonHideForm.Name = "ButtonHideForm";
            this.ButtonHideForm.Size = new System.Drawing.Size(75, 23);
            this.ButtonHideForm.TabIndex = 3;
            this.ButtonHideForm.Text = "Đóng";
            this.ButtonHideForm.UseVisualStyleBackColor = true;
            this.ButtonHideForm.Click += new System.EventHandler(this.ButtonHideForm_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(200, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(603, 508);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.checkBoxGetRetailPrice);
            this.tabPage2.Controls.Add(this.buttonViewKhoVAT);
            this.tabPage2.Controls.Add(this.gridControlKHO);
            this.tabPage2.Controls.Add(this.buttonLoadDMVTHHvietsea);
            this.tabPage2.Controls.Add(this.buttonLoadGoodsTC);
            this.tabPage2.Controls.Add(this.buttonViewImpHistory);
            this.tabPage2.Controls.Add(this.buttonCheckMaKhoNhap_maVAT);
            this.tabPage2.Controls.Add(this.buttonCheckMAKHOXUAT_TAIKHOAN);
            this.tabPage2.Controls.Add(this.button_check_xnt_vietsea);
            this.tabPage2.Controls.Add(this.button_LoadDSKhoVietsea);
            this.tabPage2.Controls.Add(this.buttonDownloadBANGMA);
            this.tabPage2.Controls.Add(this.buttonLoadTonCuoi_v2);
            this.tabPage2.Controls.Add(this.buttonGetDataExportSTLTTAN);
            this.tabPage2.Controls.Add(this.buttonViewProductDetail);
            this.tabPage2.Controls.Add(this.textBoxProduct_codes);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.checkBoxBoMaDaXuat);
            this.tabPage2.Controls.Add(this.buttonLocRaMaHangThua);
            this.tabPage2.Controls.Add(this.checkBoxBoHangXuatTra);
            this.tabPage2.Controls.Add(this.buttonCalculateGoodsAvailable);
            this.tabPage2.Controls.Add(this.buttonExportFileBarcodeToPrint);
            this.tabPage2.Controls.Add(this.buttonCheckNegativeStockEveryDay);
            this.tabPage2.Controls.Add(this.buttonGetStockInfo);
            this.tabPage2.Controls.Add(this.comboBoxSheetName2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(595, 482);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "NXT";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonViewKhoVAT
            // 
            this.buttonViewKhoVAT.Location = new System.Drawing.Point(199, 380);
            this.buttonViewKhoVAT.Name = "buttonViewKhoVAT";
            this.buttonViewKhoVAT.Size = new System.Drawing.Size(120, 23);
            this.buttonViewKhoVAT.TabIndex = 24;
            this.buttonViewKhoVAT.Text = "Xem kho - vat";
            this.buttonViewKhoVAT.UseVisualStyleBackColor = true;
            this.buttonViewKhoVAT.Click += new System.EventHandler(this.buttonViewKhoVAT_Click);
            // 
            // gridControlKHO
            // 
            this.gridControlKHO.Location = new System.Drawing.Point(6, 252);
            this.gridControlKHO.MainView = this.gridViewKHO;
            this.gridControlKHO.Name = "gridControlKHO";
            this.gridControlKHO.Size = new System.Drawing.Size(187, 218);
            this.gridControlKHO.TabIndex = 23;
            this.gridControlKHO.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewKHO});
            // 
            // gridViewKHO
            // 
            this.gridViewKHO.GridControl = this.gridControlKHO;
            this.gridViewKHO.Name = "gridViewKHO";
            // 
            // buttonLoadDMVTHHvietsea
            // 
            this.buttonLoadDMVTHHvietsea.Location = new System.Drawing.Point(230, 34);
            this.buttonLoadDMVTHHvietsea.Name = "buttonLoadDMVTHHvietsea";
            this.buttonLoadDMVTHHvietsea.Size = new System.Drawing.Size(189, 23);
            this.buttonLoadDMVTHHvietsea.TabIndex = 22;
            this.buttonLoadDMVTHHvietsea.Text = "Xuất DM hàng hóa Vietsea";
            this.buttonLoadDMVTHHvietsea.UseVisualStyleBackColor = true;
            this.buttonLoadDMVTHHvietsea.Click += new System.EventHandler(this.buttonLoadDMVTHHvietsea_Click);
            // 
            // buttonLoadGoodsTC
            // 
            this.buttonLoadGoodsTC.Location = new System.Drawing.Point(230, 7);
            this.buttonLoadGoodsTC.Name = "buttonLoadGoodsTC";
            this.buttonLoadGoodsTC.Size = new System.Drawing.Size(189, 23);
            this.buttonLoadGoodsTC.TabIndex = 21;
            this.buttonLoadGoodsTC.Text = "Xuất DM hàng hóa Thành Công";
            this.buttonLoadGoodsTC.UseVisualStyleBackColor = true;
            this.buttonLoadGoodsTC.Click += new System.EventHandler(this.buttonLoadGoodsTC_Click);
            // 
            // buttonViewImpHistory
            // 
            this.buttonViewImpHistory.Location = new System.Drawing.Point(374, 279);
            this.buttonViewImpHistory.Name = "buttonViewImpHistory";
            this.buttonViewImpHistory.Size = new System.Drawing.Size(215, 23);
            this.buttonViewImpHistory.TabIndex = 20;
            this.buttonViewImpHistory.Text = "Xem lịch sử nhập hàng";
            this.buttonViewImpHistory.UseVisualStyleBackColor = true;
            this.buttonViewImpHistory.Click += new System.EventHandler(this.buttonViewImpHistory_Click);
            // 
            // buttonCheckMaKhoNhap_maVAT
            // 
            this.buttonCheckMaKhoNhap_maVAT.Location = new System.Drawing.Point(374, 447);
            this.buttonCheckMaKhoNhap_maVAT.Name = "buttonCheckMaKhoNhap_maVAT";
            this.buttonCheckMaKhoNhap_maVAT.Size = new System.Drawing.Size(215, 23);
            this.buttonCheckMaKhoNhap_maVAT.TabIndex = 19;
            this.buttonCheckMaKhoNhap_maVAT.Text = "Check kho - mã VAT (Đạt Phát)";
            this.buttonCheckMaKhoNhap_maVAT.UseVisualStyleBackColor = true;
            this.buttonCheckMaKhoNhap_maVAT.Click += new System.EventHandler(this.buttonCheckMaKhoNhap_maVAT_Click);
            // 
            // buttonCheckMAKHOXUAT_TAIKHOAN
            // 
            this.buttonCheckMAKHOXUAT_TAIKHOAN.Location = new System.Drawing.Point(374, 391);
            this.buttonCheckMAKHOXUAT_TAIKHOAN.Name = "buttonCheckMAKHOXUAT_TAIKHOAN";
            this.buttonCheckMAKHOXUAT_TAIKHOAN.Size = new System.Drawing.Size(215, 23);
            this.buttonCheckMAKHOXUAT_TAIKHOAN.TabIndex = 18;
            this.buttonCheckMAKHOXUAT_TAIKHOAN.Text = "Check KHOXUAT - TK (Thành Công)";
            this.buttonCheckMAKHOXUAT_TAIKHOAN.UseVisualStyleBackColor = true;
            this.buttonCheckMAKHOXUAT_TAIKHOAN.Click += new System.EventHandler(this.buttonCheckMAKHOXUAT_TAIKHOAN_Click);
            // 
            // button_check_xnt_vietsea
            // 
            this.button_check_xnt_vietsea.Location = new System.Drawing.Point(374, 419);
            this.button_check_xnt_vietsea.Name = "button_check_xnt_vietsea";
            this.button_check_xnt_vietsea.Size = new System.Drawing.Size(215, 23);
            this.button_check_xnt_vietsea.TabIndex = 17;
            this.button_check_xnt_vietsea.Text = "BC NXT 4 cột";
            this.button_check_xnt_vietsea.UseVisualStyleBackColor = true;
            this.button_check_xnt_vietsea.Click += new System.EventHandler(this.button_check_xnt_vietsea_Click);
            // 
            // button_LoadDSKhoVietsea
            // 
            this.button_LoadDSKhoVietsea.Location = new System.Drawing.Point(199, 252);
            this.button_LoadDSKhoVietsea.Name = "button_LoadDSKhoVietsea";
            this.button_LoadDSKhoVietsea.Size = new System.Drawing.Size(120, 23);
            this.button_LoadDSKhoVietsea.TabIndex = 16;
            this.button_LoadDSKhoVietsea.Text = "1. Tải danh sách kho";
            this.button_LoadDSKhoVietsea.UseVisualStyleBackColor = true;
            this.button_LoadDSKhoVietsea.Click += new System.EventHandler(this.button_LoadDSKhoVietsea_Click);
            // 
            // buttonDownloadBANGMA
            // 
            this.buttonDownloadBANGMA.Location = new System.Drawing.Point(374, 335);
            this.buttonDownloadBANGMA.Name = "buttonDownloadBANGMA";
            this.buttonDownloadBANGMA.Size = new System.Drawing.Size(215, 23);
            this.buttonDownloadBANGMA.TabIndex = 2;
            this.buttonDownloadBANGMA.Text = "Tải bảng mã";
            this.buttonDownloadBANGMA.UseVisualStyleBackColor = true;
            this.buttonDownloadBANGMA.Click += new System.EventHandler(this.buttonDownloadBANGMA_Click);
            // 
            // buttonLoadTonCuoi_v2
            // 
            this.buttonLoadTonCuoi_v2.Location = new System.Drawing.Point(199, 351);
            this.buttonLoadTonCuoi_v2.Name = "buttonLoadTonCuoi_v2";
            this.buttonLoadTonCuoi_v2.Size = new System.Drawing.Size(120, 23);
            this.buttonLoadTonCuoi_v2.TabIndex = 14;
            this.buttonLoadTonCuoi_v2.Text = "2. Tồn cuối v2";
            this.buttonLoadTonCuoi_v2.UseVisualStyleBackColor = true;
            this.buttonLoadTonCuoi_v2.Click += new System.EventHandler(this.buttonLoadTonCuoi_v2_Click);
            // 
            // buttonGetDataExportSTLTTAN
            // 
            this.buttonGetDataExportSTLTTAN.Location = new System.Drawing.Point(425, 33);
            this.buttonGetDataExportSTLTTAN.Name = "buttonGetDataExportSTLTTAN";
            this.buttonGetDataExportSTLTTAN.Size = new System.Drawing.Size(164, 23);
            this.buttonGetDataExportSTLTTAN.TabIndex = 8;
            this.buttonGetDataExportSTLTTAN.Text = "Lấy dữ liệu xuất nội bộ Tấn";
            this.buttonGetDataExportSTLTTAN.UseVisualStyleBackColor = true;
            this.buttonGetDataExportSTLTTAN.Click += new System.EventHandler(this.buttonGetDataExportSTLTTAN_Click);
            // 
            // buttonViewProductDetail
            // 
            this.buttonViewProductDetail.Location = new System.Drawing.Point(374, 307);
            this.buttonViewProductDetail.Name = "buttonViewProductDetail";
            this.buttonViewProductDetail.Size = new System.Drawing.Size(215, 23);
            this.buttonViewProductDetail.TabIndex = 2;
            this.buttonViewProductDetail.Text = "Xem chi tiết vật tư";
            this.buttonViewProductDetail.UseVisualStyleBackColor = true;
            this.buttonViewProductDetail.Click += new System.EventHandler(this.buttonViewProductDetail_Click);
            // 
            // textBoxProduct_codes
            // 
            this.textBoxProduct_codes.Location = new System.Drawing.Point(374, 254);
            this.textBoxProduct_codes.Name = "textBoxProduct_codes";
            this.textBoxProduct_codes.Size = new System.Drawing.Size(215, 20);
            this.textBoxProduct_codes.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(371, 236);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(70, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "Các mã hàng";
            // 
            // checkBoxBoMaDaXuat
            // 
            this.checkBoxBoMaDaXuat.AutoSize = true;
            this.checkBoxBoMaDaXuat.Location = new System.Drawing.Point(199, 306);
            this.checkBoxBoMaDaXuat.Name = "checkBoxBoMaDaXuat";
            this.checkBoxBoMaDaXuat.Size = new System.Drawing.Size(95, 17);
            this.checkBoxBoMaDaXuat.TabIndex = 8;
            this.checkBoxBoMaDaXuat.Text = "Bỏ mã đã xuất";
            this.checkBoxBoMaDaXuat.UseVisualStyleBackColor = true;
            // 
            // buttonLocRaMaHangThua
            // 
            this.buttonLocRaMaHangThua.Location = new System.Drawing.Point(6, 92);
            this.buttonLocRaMaHangThua.Name = "buttonLocRaMaHangThua";
            this.buttonLocRaMaHangThua.Size = new System.Drawing.Size(187, 23);
            this.buttonLocRaMaHangThua.TabIndex = 6;
            this.buttonLocRaMaHangThua.Text = "3. Lọc ra mã hàng thừa";
            this.buttonLocRaMaHangThua.UseVisualStyleBackColor = true;
            this.buttonLocRaMaHangThua.Click += new System.EventHandler(this.buttonLocRaMaHangThua_Click);
            // 
            // checkBoxBoHangXuatTra
            // 
            this.checkBoxBoHangXuatTra.AutoSize = true;
            this.checkBoxBoHangXuatTra.Location = new System.Drawing.Point(199, 284);
            this.checkBoxBoHangXuatTra.Name = "checkBoxBoHangXuatTra";
            this.checkBoxBoHangXuatTra.Size = new System.Drawing.Size(129, 17);
            this.checkBoxBoHangXuatTra.TabIndex = 7;
            this.checkBoxBoHangXuatTra.Text = "Bỏ hàng xuất trả NCC";
            this.checkBoxBoHangXuatTra.UseVisualStyleBackColor = true;
            // 
            // buttonCalculateGoodsAvailable
            // 
            this.buttonCalculateGoodsAvailable.Location = new System.Drawing.Point(6, 63);
            this.buttonCalculateGoodsAvailable.Name = "buttonCalculateGoodsAvailable";
            this.buttonCalculateGoodsAvailable.Size = new System.Drawing.Size(187, 23);
            this.buttonCalculateGoodsAvailable.TabIndex = 4;
            this.buttonCalculateGoodsAvailable.Text = "2. Tính tồn kho khả dụng";
            this.buttonCalculateGoodsAvailable.UseVisualStyleBackColor = true;
            this.buttonCalculateGoodsAvailable.Click += new System.EventHandler(this.buttonCalculateGoodsAvailable_Click);
            // 
            // buttonExportFileBarcodeToPrint
            // 
            this.buttonExportFileBarcodeToPrint.Location = new System.Drawing.Point(425, 7);
            this.buttonExportFileBarcodeToPrint.Name = "buttonExportFileBarcodeToPrint";
            this.buttonExportFileBarcodeToPrint.Size = new System.Drawing.Size(164, 23);
            this.buttonExportFileBarcodeToPrint.TabIndex = 1;
            this.buttonExportFileBarcodeToPrint.Text = "Xuất file Barcode in tem cài kệ";
            this.buttonExportFileBarcodeToPrint.UseVisualStyleBackColor = true;
            this.buttonExportFileBarcodeToPrint.Click += new System.EventHandler(this.buttonExportFileBa_Click);
            // 
            // buttonCheckNegativeStockEveryDay
            // 
            this.buttonCheckNegativeStockEveryDay.Location = new System.Drawing.Point(374, 363);
            this.buttonCheckNegativeStockEveryDay.Name = "buttonCheckNegativeStockEveryDay";
            this.buttonCheckNegativeStockEveryDay.Size = new System.Drawing.Size(215, 23);
            this.buttonCheckNegativeStockEveryDay.TabIndex = 12;
            this.buttonCheckNegativeStockEveryDay.Text = "Check âm từng ngày";
            this.buttonCheckNegativeStockEveryDay.UseVisualStyleBackColor = true;
            this.buttonCheckNegativeStockEveryDay.Click += new System.EventHandler(this.buttonCheckNegativeStockEveryDay_Click);
            // 
            // buttonGetStockInfo
            // 
            this.buttonGetStockInfo.Location = new System.Drawing.Point(6, 7);
            this.buttonGetStockInfo.Name = "buttonGetStockInfo";
            this.buttonGetStockInfo.Size = new System.Drawing.Size(187, 23);
            this.buttonGetStockInfo.TabIndex = 0;
            this.buttonGetStockInfo.Text = "1. Lấy tồn kho Thành Công";
            this.buttonGetStockInfo.UseVisualStyleBackColor = true;
            this.buttonGetStockInfo.Click += new System.EventHandler(this.buttonGetStockInfo_Click);
            // 
            // comboBoxSheetName2
            // 
            this.comboBoxSheetName2.FormattingEnabled = true;
            this.comboBoxSheetName2.Location = new System.Drawing.Point(6, 36);
            this.comboBoxSheetName2.Name = "comboBoxSheetName2";
            this.comboBoxSheetName2.Size = new System.Drawing.Size(187, 21);
            this.comboBoxSheetName2.TabIndex = 3;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.checkBox_RoundExpPrice);
            this.tabPage4.Controls.Add(this.button_AutoImport);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.buttonQuickViewRetail);
            this.tabPage4.Controls.Add(this.radioButtonKHACHLE);
            this.tabPage4.Controls.Add(this.buttonApplyPrice);
            this.tabPage4.Controls.Add(this.label19);
            this.tabPage4.Controls.Add(this.radioButtonMember);
            this.tabPage4.Controls.Add(this.buttonGetDataExportInner);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.dgvTiLeLai);
            this.tabPage4.Controls.Add(this.customComboBoxVATrate);
            this.tabPage4.Controls.Add(this.comboBoxCustomersID);
            this.tabPage4.Controls.Add(this.folderBroswerTextBoxSaveImportFileLocation);
            this.tabPage4.Controls.Add(this.comboBox_sheet_tồn);
            this.tabPage4.Controls.Add(this.textBox_số_tiền_random);
            this.tabPage4.Controls.Add(this.textBox_SoCT);
            this.tabPage4.Controls.Add(this.textBox_CustomerID);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(595, 482);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Xuất hàng";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "5. Thuế suất bán ra";
            // 
            // checkBox_RoundExpPrice
            // 
            this.checkBox_RoundExpPrice.AutoSize = true;
            this.checkBox_RoundExpPrice.Location = new System.Drawing.Point(251, 158);
            this.checkBox_RoundExpPrice.Name = "checkBox_RoundExpPrice";
            this.checkBox_RoundExpPrice.Size = new System.Drawing.Size(105, 17);
            this.checkBox_RoundExpPrice.TabIndex = 29;
            this.checkBox_RoundExpPrice.Text = "4.1. Làm tròn giá";
            this.checkBox_RoundExpPrice.UseVisualStyleBackColor = true;
            // 
            // button_AutoImport
            // 
            this.button_AutoImport.Location = new System.Drawing.Point(12, 333);
            this.button_AutoImport.Name = "button_AutoImport";
            this.button_AutoImport.Size = new System.Drawing.Size(173, 23);
            this.button_AutoImport.TabIndex = 28;
            this.button_AutoImport.Text = "7. Auto import phiếu xuất bán";
            this.button_AutoImport.UseVisualStyleBackColor = true;
            this.button_AutoImport.Click += new System.EventHandler(this.button_AutoImport_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(149, 264);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 27;
            this.label24.Text = "label24";
            // 
            // buttonQuickViewRetail
            // 
            this.buttonQuickViewRetail.Location = new System.Drawing.Point(12, 259);
            this.buttonQuickViewRetail.Name = "buttonQuickViewRetail";
            this.buttonQuickViewRetail.Size = new System.Drawing.Size(120, 23);
            this.buttonQuickViewRetail.TabIndex = 26;
            this.buttonQuickViewRetail.Text = "Doanh thu bán lẻ";
            this.buttonQuickViewRetail.UseVisualStyleBackColor = true;
            this.buttonQuickViewRetail.Click += new System.EventHandler(this.buttonQuickViewRetail_Click);
            // 
            // radioButtonKHACHLE
            // 
            this.radioButtonKHACHLE.AutoSize = true;
            this.radioButtonKHACHLE.Location = new System.Drawing.Point(12, 235);
            this.radioButtonKHACHLE.Name = "radioButtonKHACHLE";
            this.radioButtonKHACHLE.Size = new System.Drawing.Size(112, 17);
            this.radioButtonKHACHLE.TabIndex = 20;
            this.radioButtonKHACHLE.TabStop = true;
            this.radioButtonKHACHLE.Text = "5.1. Xuất khách lẻ";
            this.radioButtonKHACHLE.UseVisualStyleBackColor = true;
            this.radioButtonKHACHLE.CheckedChanged += new System.EventHandler(this.radioButtonKHACHLE_CheckedChanged);
            // 
            // buttonApplyPrice
            // 
            this.buttonApplyPrice.Location = new System.Drawing.Point(468, 158);
            this.buttonApplyPrice.Name = "buttonApplyPrice";
            this.buttonApplyPrice.Size = new System.Drawing.Size(121, 23);
            this.buttonApplyPrice.TabIndex = 17;
            this.buttonApplyPrice.Text = "Tính giá bán";
            this.buttonApplyPrice.UseVisualStyleBackColor = true;
            this.buttonApplyPrice.Click += new System.EventHandler(this.buttonbuttonApplyPrice_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(106, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "1. Thuế suất tính giá";
            // 
            // radioButtonMember
            // 
            this.radioButtonMember.AutoSize = true;
            this.radioButtonMember.Location = new System.Drawing.Point(377, 235);
            this.radioButtonMember.Name = "radioButtonMember";
            this.radioButtonMember.Size = new System.Drawing.Size(100, 17);
            this.radioButtonMember.TabIndex = 22;
            this.radioButtonMember.TabStop = true;
            this.radioButtonMember.Text = "5.2. Xuất nội bộ";
            this.radioButtonMember.UseVisualStyleBackColor = true;
            // 
            // buttonGetDataExportInner
            // 
            this.buttonGetDataExportInner.Location = new System.Drawing.Point(10, 304);
            this.buttonGetDataExportInner.Name = "buttonGetDataExportInner";
            this.buttonGetDataExportInner.Size = new System.Drawing.Size(175, 23);
            this.buttonGetDataExportInner.TabIndex = 21;
            this.buttonGetDataExportInner.Text = "6. Tải dữ liệu nội bộ + tạo import";
            this.buttonGetDataExportInner.UseVisualStyleBackColor = true;
            this.buttonGetDataExportInner.Click += new System.EventHandler(this.buttonGetDataExportInner_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 114);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "4. sheet Tồn";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 456);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(124, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Đường dẫn lưu file import";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "3.1. Số tiền";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "3. Số CT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 36);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "2. Mã khách";
            // 
            // dgvTiLeLai
            // 
            this.dgvTiLeLai.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTiLeLai.Location = new System.Drawing.Point(251, 6);
            this.dgvTiLeLai.Name = "dgvTiLeLai";
            this.dgvTiLeLai.Size = new System.Drawing.Size(338, 146);
            this.dgvTiLeLai.TabIndex = 16;
            this.dgvTiLeLai.Leave += new System.EventHandler(this.dgvTiLeLai_Leave);
            // 
            // customComboBoxVATrate
            // 
            this.customComboBoxVATrate.FormattingEnabled = true;
            this.customComboBoxVATrate.Items.AddRange(new object[] {
            "10",
            "5",
            "0"});
            this.customComboBoxVATrate.Location = new System.Drawing.Point(118, 6);
            this.customComboBoxVATrate.Name = "customComboBoxVATrate";
            this.customComboBoxVATrate.Size = new System.Drawing.Size(127, 21);
            this.customComboBoxVATrate.TabIndex = 1;
            // 
            // comboBoxCustomersID
            // 
            this.comboBoxCustomersID.FormattingEnabled = true;
            this.comboBoxCustomersID.Location = new System.Drawing.Point(377, 258);
            this.comboBoxCustomersID.Name = "comboBoxCustomersID";
            this.comboBoxCustomersID.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCustomersID.TabIndex = 23;
            this.comboBoxCustomersID.DropDown += new System.EventHandler(this.comboBoxCustomersID_DropDown);
            // 
            // folderBroswerTextBoxSaveImportFileLocation
            // 
            this.folderBroswerTextBoxSaveImportFileLocation.FolderPath = "";
            this.folderBroswerTextBoxSaveImportFileLocation.FolderType = ExcelAddIn1.MyUserControl.FolderBroswerTextBox.EnumFolderType.Folder;
            this.folderBroswerTextBoxSaveImportFileLocation.Location = new System.Drawing.Point(152, 453);
            this.folderBroswerTextBoxSaveImportFileLocation.Margin = new System.Windows.Forms.Padding(0);
            this.folderBroswerTextBoxSaveImportFileLocation.Name = "folderBroswerTextBoxSaveImportFileLocation";
            this.folderBroswerTextBoxSaveImportFileLocation.Size = new System.Drawing.Size(429, 20);
            this.folderBroswerTextBoxSaveImportFileLocation.TabIndex = 25;
            this.folderBroswerTextBoxSaveImportFileLocation.Leave += new System.EventHandler(this.folderBroswerTextBoxSaveImportFileLocation_Leave);
            // 
            // comboBox_sheet_tồn
            // 
            this.comboBox_sheet_tồn.FormattingEnabled = true;
            this.comboBox_sheet_tồn.Location = new System.Drawing.Point(118, 110);
            this.comboBox_sheet_tồn.Name = "comboBox_sheet_tồn";
            this.comboBox_sheet_tồn.Size = new System.Drawing.Size(127, 21);
            this.comboBox_sheet_tồn.TabIndex = 15;
            // 
            // textBox_số_tiền_random
            // 
            this.textBox_số_tiền_random.Location = new System.Drawing.Point(118, 84);
            this.textBox_số_tiền_random.Name = "textBox_số_tiền_random";
            this.textBox_số_tiền_random.SelectAllOnClick = false;
            this.textBox_số_tiền_random.Size = new System.Drawing.Size(127, 20);
            this.textBox_số_tiền_random.TabIndex = 7;
            // 
            // textBox_SoCT
            // 
            this.textBox_SoCT.Location = new System.Drawing.Point(118, 58);
            this.textBox_SoCT.Name = "textBox_SoCT";
            this.textBox_SoCT.SelectAllOnClick = false;
            this.textBox_SoCT.Size = new System.Drawing.Size(127, 20);
            this.textBox_SoCT.TabIndex = 5;
            // 
            // textBox_CustomerID
            // 
            this.textBox_CustomerID.Location = new System.Drawing.Point(118, 33);
            this.textBox_CustomerID.Name = "textBox_CustomerID";
            this.textBox_CustomerID.SelectAllOnClick = false;
            this.textBox_CustomerID.Size = new System.Drawing.Size(127, 20);
            this.textBox_CustomerID.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button_AutoCreateBillRetail);
            this.tabPage1.Controls.Add(this.button_TachBill);
            this.tabPage1.Controls.Add(this.checkBoxHienTonVIETSEA);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.buttonGetRetailTranDetail);
            this.tabPage1.Controls.Add(this.textBoxTypeTranID);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(595, 482);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Xuất bill";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button_AutoCreateBillRetail
            // 
            this.button_AutoCreateBillRetail.Location = new System.Drawing.Point(6, 125);
            this.button_AutoCreateBillRetail.Name = "button_AutoCreateBillRetail";
            this.button_AutoCreateBillRetail.Size = new System.Drawing.Size(152, 23);
            this.button_AutoCreateBillRetail.TabIndex = 5;
            this.button_AutoCreateBillRetail.Text = "3. Auto xuất bill Vietsea";
            this.button_AutoCreateBillRetail.UseVisualStyleBackColor = true;
            this.button_AutoCreateBillRetail.Click += new System.EventHandler(this.button_AutoCreateBillRetail_Click);
            // 
            // button_TachBill
            // 
            this.button_TachBill.Location = new System.Drawing.Point(6, 278);
            this.button_TachBill.Name = "button_TachBill";
            this.button_TachBill.Size = new System.Drawing.Size(75, 23);
            this.button_TachBill.TabIndex = 4;
            this.button_TachBill.Text = "Tách bill";
            this.button_TachBill.UseVisualStyleBackColor = true;
            this.button_TachBill.Click += new System.EventHandler(this.button_TachBill_Click);
            // 
            // checkBoxHienTonVIETSEA
            // 
            this.checkBoxHienTonVIETSEA.AutoSize = true;
            this.checkBoxHienTonVIETSEA.Location = new System.Drawing.Point(6, 72);
            this.checkBoxHienTonVIETSEA.Name = "checkBoxHienTonVIETSEA";
            this.checkBoxHienTonVIETSEA.Size = new System.Drawing.Size(183, 17);
            this.checkBoxHienTonVIETSEA.TabIndex = 2;
            this.checkBoxHienTonVIETSEA.Text = "1.1. Hiện tồn VIETSEA khả dụng";
            this.checkBoxHienTonVIETSEA.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(215, 26);
            this.label6.TabIndex = 0;
            this.label6.Text = "1. Nhập mã giao dịch\r\nnhiều giao dịch cách nhau bởi dấu phẩy ( , )";
            // 
            // buttonGetRetailTranDetail
            // 
            this.buttonGetRetailTranDetail.Location = new System.Drawing.Point(6, 96);
            this.buttonGetRetailTranDetail.Name = "buttonGetRetailTranDetail";
            this.buttonGetRetailTranDetail.Size = new System.Drawing.Size(152, 23);
            this.buttonGetRetailTranDetail.TabIndex = 3;
            this.buttonGetRetailTranDetail.Text = "2. Lấy thông tin giao dịch";
            this.buttonGetRetailTranDetail.UseVisualStyleBackColor = true;
            this.buttonGetRetailTranDetail.Click += new System.EventHandler(this.buttonGetRetailTranDetail_Click);
            // 
            // textBoxTypeTranID
            // 
            this.textBoxTypeTranID.Location = new System.Drawing.Point(6, 45);
            this.textBoxTypeTranID.Name = "textBoxTypeTranID";
            this.textBoxTypeTranID.SelectAllOnClick = false;
            this.textBoxTypeTranID.Size = new System.Drawing.Size(583, 20);
            this.textBoxTypeTranID.TabIndex = 1;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.buttonTaoDonDatHang);
            this.tabPage6.Controls.Add(this.buttonUploadInvoiceOneByOne);
            this.tabPage6.Controls.Add(this.buttonUploadEinvoiceThanhCong);
            this.tabPage6.Controls.Add(this.buttonLoadFileImportIhoadon);
            this.tabPage6.Controls.Add(this.buttonCheckMST);
            this.tabPage6.Controls.Add(this.button_load_bangke_muavao);
            this.tabPage6.Controls.Add(this.gridControl_EinvoiceList);
            this.tabPage6.Controls.Add(this.button3_UploadEinvoiceSelenium);
            this.tabPage6.Controls.Add(this.button_download_PDF_einvoice_by_selenium);
            this.tabPage6.Controls.Add(this.button_LoadBangKeBanRa);
            this.tabPage6.Controls.Add(this.button_DownloadInvoiceInfoToCheck);
            this.tabPage6.Controls.Add(this.label20);
            this.tabPage6.Controls.Add(this.button_CreateFileImport_Einvoice);
            this.tabPage6.Controls.Add(this.folderBroswerTextBox_SaveLocation_import_iHoaDon);
            this.tabPage6.Controls.Add(this.button_Load_XBAN);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(595, 482);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Hóa đơn điện tử";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // buttonTaoDonDatHang
            // 
            this.buttonTaoDonDatHang.Location = new System.Drawing.Point(210, 401);
            this.buttonTaoDonDatHang.Name = "buttonTaoDonDatHang";
            this.buttonTaoDonDatHang.Size = new System.Drawing.Size(196, 23);
            this.buttonTaoDonDatHang.TabIndex = 16;
            this.buttonTaoDonDatHang.Text = "Tạo đơn đặt hàng";
            this.buttonTaoDonDatHang.UseVisualStyleBackColor = true;
            this.buttonTaoDonDatHang.Click += new System.EventHandler(this.buttonTaoDonDatHang_Click);
            // 
            // buttonUploadInvoiceOneByOne
            // 
            this.buttonUploadInvoiceOneByOne.Location = new System.Drawing.Point(210, 343);
            this.buttonUploadInvoiceOneByOne.Name = "buttonUploadInvoiceOneByOne";
            this.buttonUploadInvoiceOneByOne.Size = new System.Drawing.Size(196, 23);
            this.buttonUploadInvoiceOneByOne.TabIndex = 15;
            this.buttonUploadInvoiceOneByOne.Text = "Upload hóa đơn đã chọn";
            this.buttonUploadInvoiceOneByOne.UseVisualStyleBackColor = true;
            this.buttonUploadInvoiceOneByOne.Click += new System.EventHandler(this.buttonUploadInvoiceOneByOne_Click);
            // 
            // buttonUploadEinvoiceThanhCong
            // 
            this.buttonUploadEinvoiceThanhCong.Location = new System.Drawing.Point(210, 372);
            this.buttonUploadEinvoiceThanhCong.Name = "buttonUploadEinvoiceThanhCong";
            this.buttonUploadEinvoiceThanhCong.Size = new System.Drawing.Size(196, 23);
            this.buttonUploadEinvoiceThanhCong.TabIndex = 14;
            this.buttonUploadEinvoiceThanhCong.Text = "3.1 Upload hđ điện tử Thành Công";
            this.buttonUploadEinvoiceThanhCong.UseVisualStyleBackColor = true;
            this.buttonUploadEinvoiceThanhCong.Click += new System.EventHandler(this.buttonUploadEinvoiceThanhCong_Click);
            // 
            // buttonLoadFileImportIhoadon
            // 
            this.buttonLoadFileImportIhoadon.Location = new System.Drawing.Point(414, 430);
            this.buttonLoadFileImportIhoadon.Name = "buttonLoadFileImportIhoadon";
            this.buttonLoadFileImportIhoadon.Size = new System.Drawing.Size(166, 23);
            this.buttonLoadFileImportIhoadon.TabIndex = 13;
            this.buttonLoadFileImportIhoadon.Text = "Mở file mẫu import hàng loạt";
            this.buttonLoadFileImportIhoadon.UseVisualStyleBackColor = true;
            this.buttonLoadFileImportIhoadon.Click += new System.EventHandler(this.buttonLoadFileImportIhoadon_Click);
            // 
            // buttonCheckMST
            // 
            this.buttonCheckMST.Location = new System.Drawing.Point(414, 372);
            this.buttonCheckMST.Name = "buttonCheckMST";
            this.buttonCheckMST.Size = new System.Drawing.Size(166, 23);
            this.buttonCheckMST.TabIndex = 12;
            this.buttonCheckMST.Text = "Check MST";
            this.buttonCheckMST.UseVisualStyleBackColor = true;
            this.buttonCheckMST.Click += new System.EventHandler(this.buttonCheckMST_Click);
            // 
            // button_load_bangke_muavao
            // 
            this.button_load_bangke_muavao.Location = new System.Drawing.Point(414, 401);
            this.button_load_bangke_muavao.Name = "button_load_bangke_muavao";
            this.button_load_bangke_muavao.Size = new System.Drawing.Size(166, 23);
            this.button_load_bangke_muavao.TabIndex = 11;
            this.button_load_bangke_muavao.Text = "Load bảng kê mua vào";
            this.button_load_bangke_muavao.UseVisualStyleBackColor = true;
            this.button_load_bangke_muavao.Click += new System.EventHandler(this.button_load_bangke_muavao_Click);
            // 
            // gridControl_EinvoiceList
            // 
            this.gridControl_EinvoiceList.Location = new System.Drawing.Point(9, 35);
            this.gridControl_EinvoiceList.MainView = this.gridView_EinvoiceList;
            this.gridControl_EinvoiceList.Name = "gridControl_EinvoiceList";
            this.gridControl_EinvoiceList.Size = new System.Drawing.Size(571, 302);
            this.gridControl_EinvoiceList.TabIndex = 10;
            this.gridControl_EinvoiceList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_EinvoiceList});
            // 
            // gridView_EinvoiceList
            // 
            this.gridView_EinvoiceList.GridControl = this.gridControl_EinvoiceList;
            this.gridView_EinvoiceList.Name = "gridView_EinvoiceList";
            this.gridView_EinvoiceList.OptionsBehavior.Editable = false;
            this.gridView_EinvoiceList.OptionsSelection.CheckBoxSelectorColumnWidth = 25;
            this.gridView_EinvoiceList.OptionsSelection.MultiSelect = true;
            this.gridView_EinvoiceList.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView_EinvoiceList.OptionsView.ShowAutoFilterRow = true;
            // 
            // button3_UploadEinvoiceSelenium
            // 
            this.button3_UploadEinvoiceSelenium.Location = new System.Drawing.Point(7, 372);
            this.button3_UploadEinvoiceSelenium.Name = "button3_UploadEinvoiceSelenium";
            this.button3_UploadEinvoiceSelenium.Size = new System.Drawing.Size(197, 23);
            this.button3_UploadEinvoiceSelenium.TabIndex = 7;
            this.button3_UploadEinvoiceSelenium.Text = "3. Up hóa đơn điện tử Đạt phát";
            this.button3_UploadEinvoiceSelenium.UseVisualStyleBackColor = true;
            this.button3_UploadEinvoiceSelenium.Click += new System.EventHandler(this.button3_UploadEinvoiceSelenium_Click);
            // 
            // button_download_PDF_einvoice_by_selenium
            // 
            this.button_download_PDF_einvoice_by_selenium.Location = new System.Drawing.Point(414, 343);
            this.button_download_PDF_einvoice_by_selenium.Name = "button_download_PDF_einvoice_by_selenium";
            this.button_download_PDF_einvoice_by_selenium.Size = new System.Drawing.Size(166, 23);
            this.button_download_PDF_einvoice_by_selenium.TabIndex = 6;
            this.button_download_PDF_einvoice_by_selenium.Text = "Selenium tải hóa đơn PDF";
            this.button_download_PDF_einvoice_by_selenium.UseVisualStyleBackColor = true;
            this.button_download_PDF_einvoice_by_selenium.Click += new System.EventHandler(this.button_download_PDF_einvoice_by_selenium_Click);
            // 
            // button_LoadBangKeBanRa
            // 
            this.button_LoadBangKeBanRa.Location = new System.Drawing.Point(7, 401);
            this.button_LoadBangKeBanRa.Name = "button_LoadBangKeBanRa";
            this.button_LoadBangKeBanRa.Size = new System.Drawing.Size(197, 23);
            this.button_LoadBangKeBanRa.TabIndex = 9;
            this.button_LoadBangKeBanRa.Text = "Load bảng kê bán ra";
            this.button_LoadBangKeBanRa.UseVisualStyleBackColor = true;
            this.button_LoadBangKeBanRa.Click += new System.EventHandler(this.button_LoadBangKeBanRa_Click);
            // 
            // button_DownloadInvoiceInfoToCheck
            // 
            this.button_DownloadInvoiceInfoToCheck.Location = new System.Drawing.Point(7, 430);
            this.button_DownloadInvoiceInfoToCheck.Name = "button_DownloadInvoiceInfoToCheck";
            this.button_DownloadInvoiceInfoToCheck.Size = new System.Drawing.Size(197, 23);
            this.button_DownloadInvoiceInfoToCheck.TabIndex = 8;
            this.button_DownloadInvoiceInfoToCheck.Text = "Tải dữ liệu hóa đơn điện từ về check";
            this.button_DownloadInvoiceInfoToCheck.UseVisualStyleBackColor = true;
            this.button_DownloadInvoiceInfoToCheck.Click += new System.EventHandler(this.button_DownloadInvoiceInfoToCheck_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 462);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(82, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "Thư mục lưu file";
            // 
            // button_CreateFileImport_Einvoice
            // 
            this.button_CreateFileImport_Einvoice.Location = new System.Drawing.Point(7, 343);
            this.button_CreateFileImport_Einvoice.Name = "button_CreateFileImport_Einvoice";
            this.button_CreateFileImport_Einvoice.Size = new System.Drawing.Size(197, 23);
            this.button_CreateFileImport_Einvoice.TabIndex = 3;
            this.button_CreateFileImport_Einvoice.Text = "2. Xuất file import hóa đơn điện tử";
            this.button_CreateFileImport_Einvoice.UseVisualStyleBackColor = true;
            this.button_CreateFileImport_Einvoice.Click += new System.EventHandler(this.button_CreateFileImport_Einvoice_Click);
            // 
            // folderBroswerTextBox_SaveLocation_import_iHoaDon
            // 
            this.folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderPath = "";
            this.folderBroswerTextBox_SaveLocation_import_iHoaDon.FolderType = ExcelAddIn1.MyUserControl.FolderBroswerTextBox.EnumFolderType.Folder;
            this.folderBroswerTextBox_SaveLocation_import_iHoaDon.Location = new System.Drawing.Point(91, 459);
            this.folderBroswerTextBox_SaveLocation_import_iHoaDon.Margin = new System.Windows.Forms.Padding(0);
            this.folderBroswerTextBox_SaveLocation_import_iHoaDon.Name = "folderBroswerTextBox_SaveLocation_import_iHoaDon";
            this.folderBroswerTextBox_SaveLocation_import_iHoaDon.Size = new System.Drawing.Size(479, 20);
            this.folderBroswerTextBox_SaveLocation_import_iHoaDon.TabIndex = 4;
            this.folderBroswerTextBox_SaveLocation_import_iHoaDon.Leave += new System.EventHandler(this.folderBroswerTextBox_SaveLocation_import_iHoaDon_Leave);
            // 
            // button_Load_XBAN
            // 
            this.button_Load_XBAN.Location = new System.Drawing.Point(6, 6);
            this.button_Load_XBAN.Name = "button_Load_XBAN";
            this.button_Load_XBAN.Size = new System.Drawing.Size(198, 23);
            this.button_Load_XBAN.TabIndex = 1;
            this.button_Load_XBAN.Text = "1. Tải danh sách phiếu xuất";
            this.button_Load_XBAN.UseVisualStyleBackColor = true;
            this.button_Load_XBAN.Click += new System.EventHandler(this.button_Load_XBAN_Click);
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.groupBox3);
            this.tabPage7.Controls.Add(this.groupBox2);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(595, 482);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Công nợ vietsea";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button_load_makhachhang_vietsea);
            this.groupBox3.Controls.Add(this.gridLookUpEditMaNCC);
            this.groupBox3.Controls.Add(this.gridControl_DS_Hoadon_vietsea);
            this.groupBox3.Controls.Add(this.button_create_PC_auto);
            this.groupBox3.Controls.Add(this.textBoxMAKHACHHANG);
            this.groupBox3.Controls.Add(this.button_BC_S38);
            this.groupBox3.Controls.Add(this.button_TH_CONGNO_331);
            this.groupBox3.Controls.Add(this.buttonLoadInvoice);
            this.groupBox3.Location = new System.Drawing.Point(6, 100);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(583, 376);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Phiếu chi";
            // 
            // button_load_makhachhang_vietsea
            // 
            this.button_load_makhachhang_vietsea.Location = new System.Drawing.Point(6, 19);
            this.button_load_makhachhang_vietsea.Name = "button_load_makhachhang_vietsea";
            this.button_load_makhachhang_vietsea.Size = new System.Drawing.Size(135, 23);
            this.button_load_makhachhang_vietsea.TabIndex = 0;
            this.button_load_makhachhang_vietsea.Text = "1. Tải danh mục NCC";
            this.button_load_makhachhang_vietsea.UseVisualStyleBackColor = true;
            this.button_load_makhachhang_vietsea.Click += new System.EventHandler(this.button_load_makhachhang_vietsea_Click);
            // 
            // gridLookUpEditMaNCC
            // 
            this.gridLookUpEditMaNCC.Location = new System.Drawing.Point(147, 21);
            this.gridLookUpEditMaNCC.Name = "gridLookUpEditMaNCC";
            this.gridLookUpEditMaNCC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMaNCC.Properties.PopupView = this.gridLookUpEditMaNCCView;
            this.gridLookUpEditMaNCC.Properties.SearchMode = DevExpress.XtraEditors.Repository.GridLookUpSearchMode.AutoSuggest;
            this.gridLookUpEditMaNCC.Properties.EditValueChanged += new System.EventHandler(this.gridLookUpEditMaNCC_Properties_EditValueChanged);
            this.gridLookUpEditMaNCC.Size = new System.Drawing.Size(140, 20);
            this.gridLookUpEditMaNCC.TabIndex = 1;
            this.gridLookUpEditMaNCC.Popup += new System.EventHandler(this.gridLookUpEditMaNCC_Popup);
            this.gridLookUpEditMaNCC.Click += new System.EventHandler(this.gridLookUpEditMaNCC_Click);
            // 
            // gridLookUpEditMaNCCView
            // 
            this.gridLookUpEditMaNCCView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEditMaNCCView.Name = "gridLookUpEditMaNCCView";
            this.gridLookUpEditMaNCCView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEditMaNCCView.OptionsView.ShowGroupPanel = false;
            // 
            // gridControl_DS_Hoadon_vietsea
            // 
            this.gridControl_DS_Hoadon_vietsea.Location = new System.Drawing.Point(6, 106);
            this.gridControl_DS_Hoadon_vietsea.MainView = this.gridView3_DS_hoadon;
            this.gridControl_DS_Hoadon_vietsea.Name = "gridControl_DS_Hoadon_vietsea";
            this.gridControl_DS_Hoadon_vietsea.Size = new System.Drawing.Size(571, 264);
            this.gridControl_DS_Hoadon_vietsea.TabIndex = 7;
            this.gridControl_DS_Hoadon_vietsea.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3_DS_hoadon});
            // 
            // gridView3_DS_hoadon
            // 
            this.gridView3_DS_hoadon.GridControl = this.gridControl_DS_Hoadon_vietsea;
            this.gridView3_DS_hoadon.Name = "gridView3_DS_hoadon";
            this.gridView3_DS_hoadon.OptionsSelection.CheckBoxSelectorColumnWidth = 25;
            this.gridView3_DS_hoadon.OptionsSelection.MultiSelect = true;
            this.gridView3_DS_hoadon.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            // 
            // button_create_PC_auto
            // 
            this.button_create_PC_auto.Location = new System.Drawing.Point(147, 48);
            this.button_create_PC_auto.Name = "button_create_PC_auto";
            this.button_create_PC_auto.Size = new System.Drawing.Size(140, 23);
            this.button_create_PC_auto.TabIndex = 5;
            this.button_create_PC_auto.Text = "4. Tạo phiếu chi tự động";
            this.button_create_PC_auto.UseVisualStyleBackColor = true;
            this.button_create_PC_auto.Click += new System.EventHandler(this.button_create_PC_auto_Click);
            // 
            // textBoxMAKHACHHANG
            // 
            this.textBoxMAKHACHHANG.Enabled = false;
            this.textBoxMAKHACHHANG.Location = new System.Drawing.Point(293, 21);
            this.textBoxMAKHACHHANG.Name = "textBoxMAKHACHHANG";
            this.textBoxMAKHACHHANG.ReadOnly = true;
            this.textBoxMAKHACHHANG.Size = new System.Drawing.Size(284, 20);
            this.textBoxMAKHACHHANG.TabIndex = 2;
            // 
            // button_BC_S38
            // 
            this.button_BC_S38.Location = new System.Drawing.Point(6, 48);
            this.button_BC_S38.Name = "button_BC_S38";
            this.button_BC_S38.Size = new System.Drawing.Size(135, 23);
            this.button_BC_S38.TabIndex = 3;
            this.button_BC_S38.Text = "2. Sổ chi tiết 331";
            this.button_BC_S38.UseVisualStyleBackColor = true;
            this.button_BC_S38.Click += new System.EventHandler(this.button_BC_S38_Click);
            // 
            // button_TH_CONGNO_331
            // 
            this.button_TH_CONGNO_331.Location = new System.Drawing.Point(434, 48);
            this.button_TH_CONGNO_331.Name = "button_TH_CONGNO_331";
            this.button_TH_CONGNO_331.Size = new System.Drawing.Size(143, 23);
            this.button_TH_CONGNO_331.TabIndex = 6;
            this.button_TH_CONGNO_331.Text = "Báo cáo tổng hợp 331";
            this.button_TH_CONGNO_331.UseVisualStyleBackColor = true;
            this.button_TH_CONGNO_331.Click += new System.EventHandler(this.button_TH_CONGNO_331_Click);
            // 
            // buttonLoadInvoice
            // 
            this.buttonLoadInvoice.Location = new System.Drawing.Point(6, 77);
            this.buttonLoadInvoice.Name = "buttonLoadInvoice";
            this.buttonLoadInvoice.Size = new System.Drawing.Size(135, 23);
            this.buttonLoadInvoice.TabIndex = 4;
            this.buttonLoadInvoice.Text = "3. Tải ds hóa đơn";
            this.buttonLoadInvoice.UseVisualStyleBackColor = true;
            this.buttonLoadInvoice.Click += new System.EventHandler(this.buttonLoadInvoice_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonGetDataToMakeReceipt);
            this.groupBox2.Controls.Add(this.button_Tao_PhieuThu_Auto);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(583, 79);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Phiếu thu";
            // 
            // buttonGetDataToMakeReceipt
            // 
            this.buttonGetDataToMakeReceipt.Location = new System.Drawing.Point(6, 19);
            this.buttonGetDataToMakeReceipt.Name = "buttonGetDataToMakeReceipt";
            this.buttonGetDataToMakeReceipt.Size = new System.Drawing.Size(171, 23);
            this.buttonGetDataToMakeReceipt.TabIndex = 0;
            this.buttonGetDataToMakeReceipt.Text = "1. Lấy dữ liệu làm phiếu thu";
            this.buttonGetDataToMakeReceipt.UseVisualStyleBackColor = true;
            this.buttonGetDataToMakeReceipt.Click += new System.EventHandler(this.buttonGetDataToMakeReceipt_Click);
            // 
            // button_Tao_PhieuThu_Auto
            // 
            this.button_Tao_PhieuThu_Auto.Location = new System.Drawing.Point(6, 48);
            this.button_Tao_PhieuThu_Auto.Name = "button_Tao_PhieuThu_Auto";
            this.button_Tao_PhieuThu_Auto.Size = new System.Drawing.Size(171, 23);
            this.button_Tao_PhieuThu_Auto.TabIndex = 1;
            this.button_Tao_PhieuThu_Auto.Text = "2. Tạo phiếu thu tự động";
            this.button_Tao_PhieuThu_Auto.UseVisualStyleBackColor = true;
            this.button_Tao_PhieuThu_Auto.Click += new System.EventHandler(this.button_Tao_PhieuThu_Auto_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button_AutoCreateBankingTransantions);
            this.tabPage3.Controls.Add(this.button_analyzeStatement);
            this.tabPage3.Controls.Add(this.gridControl_BankStatement);
            this.tabPage3.Controls.Add(this.button_download_saoke_vietcombank);
            this.tabPage3.Controls.Add(this.button_download_saoke_techcombank);
            this.tabPage3.Controls.Add(this.buttonFastReportVCB_RevenueGroupByBranch);
            this.tabPage3.Controls.Add(this.buttonFastReportVCB_NXT);
            this.tabPage3.Controls.Add(this.dgvBranchIDs);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(595, 482);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Ngân hàng";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button_AutoCreateBankingTransantions
            // 
            this.button_AutoCreateBankingTransantions.Location = new System.Drawing.Point(135, 191);
            this.button_AutoCreateBankingTransantions.Name = "button_AutoCreateBankingTransantions";
            this.button_AutoCreateBankingTransantions.Size = new System.Drawing.Size(132, 23);
            this.button_AutoCreateBankingTransantions.TabIndex = 8;
            this.button_AutoCreateBankingTransantions.Text = "Hạch toán tự động";
            this.button_AutoCreateBankingTransantions.UseVisualStyleBackColor = true;
            this.button_AutoCreateBankingTransantions.Click += new System.EventHandler(this.button_AutoCreateBankingTransantions_Click);
            // 
            // button_analyzeStatement
            // 
            this.button_analyzeStatement.Location = new System.Drawing.Point(135, 162);
            this.button_analyzeStatement.Name = "button_analyzeStatement";
            this.button_analyzeStatement.Size = new System.Drawing.Size(132, 23);
            this.button_analyzeStatement.TabIndex = 7;
            this.button_analyzeStatement.Text = "Phân tích sao kê VCB";
            this.button_analyzeStatement.UseVisualStyleBackColor = true;
            this.button_analyzeStatement.Click += new System.EventHandler(this.button_analyzeStatement_Click);
            // 
            // gridControl_BankStatement
            // 
            this.gridControl_BankStatement.Location = new System.Drawing.Point(6, 223);
            this.gridControl_BankStatement.MainView = this.gridView_BankStatement;
            this.gridControl_BankStatement.Name = "gridControl_BankStatement";
            this.gridControl_BankStatement.Size = new System.Drawing.Size(583, 253);
            this.gridControl_BankStatement.TabIndex = 6;
            this.gridControl_BankStatement.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_BankStatement});
            // 
            // gridView_BankStatement
            // 
            this.gridView_BankStatement.GridControl = this.gridControl_BankStatement;
            this.gridView_BankStatement.Name = "gridView_BankStatement";
            this.gridView_BankStatement.OptionsSelection.CheckBoxSelectorColumnWidth = 25;
            this.gridView_BankStatement.OptionsSelection.MultiSelect = true;
            this.gridView_BankStatement.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            // 
            // button_download_saoke_vietcombank
            // 
            this.button_download_saoke_vietcombank.Location = new System.Drawing.Point(6, 191);
            this.button_download_saoke_vietcombank.Name = "button_download_saoke_vietcombank";
            this.button_download_saoke_vietcombank.Size = new System.Drawing.Size(123, 23);
            this.button_download_saoke_vietcombank.TabIndex = 5;
            this.button_download_saoke_vietcombank.Text = "Tải sao kê VCB";
            this.button_download_saoke_vietcombank.UseVisualStyleBackColor = true;
            this.button_download_saoke_vietcombank.Click += new System.EventHandler(this.button_download_saoke_vietcombank_Click);
            // 
            // button_download_saoke_techcombank
            // 
            this.button_download_saoke_techcombank.Location = new System.Drawing.Point(6, 162);
            this.button_download_saoke_techcombank.Name = "button_download_saoke_techcombank";
            this.button_download_saoke_techcombank.Size = new System.Drawing.Size(123, 23);
            this.button_download_saoke_techcombank.TabIndex = 4;
            this.button_download_saoke_techcombank.Text = "Tải sao kê TECH";
            this.button_download_saoke_techcombank.UseVisualStyleBackColor = true;
            this.button_download_saoke_techcombank.Click += new System.EventHandler(this.button_download_saoke_techcombank_Click);
            // 
            // buttonFastReportVCB_RevenueGroupByBranch
            // 
            this.buttonFastReportVCB_RevenueGroupByBranch.Location = new System.Drawing.Point(336, 35);
            this.buttonFastReportVCB_RevenueGroupByBranch.Name = "buttonFastReportVCB_RevenueGroupByBranch";
            this.buttonFastReportVCB_RevenueGroupByBranch.Size = new System.Drawing.Size(187, 23);
            this.buttonFastReportVCB_RevenueGroupByBranch.TabIndex = 3;
            this.buttonFastReportVCB_RevenueGroupByBranch.Text = "BC doanh thu theo ngành hàng";
            this.buttonFastReportVCB_RevenueGroupByBranch.UseVisualStyleBackColor = true;
            this.buttonFastReportVCB_RevenueGroupByBranch.Click += new System.EventHandler(this.buttonFastReportVCB_RevenueGroupByBranch_Click);
            // 
            // buttonFastReportVCB_NXT
            // 
            this.buttonFastReportVCB_NXT.Location = new System.Drawing.Point(336, 6);
            this.buttonFastReportVCB_NXT.Name = "buttonFastReportVCB_NXT";
            this.buttonFastReportVCB_NXT.Size = new System.Drawing.Size(187, 23);
            this.buttonFastReportVCB_NXT.TabIndex = 2;
            this.buttonFastReportVCB_NXT.Text = "BC NXT theo ngành";
            this.buttonFastReportVCB_NXT.UseVisualStyleBackColor = true;
            this.buttonFastReportVCB_NXT.Click += new System.EventHandler(this.buttonFastReportVCB_NXT_Click);
            // 
            // dgvBranchIDs
            // 
            this.dgvBranchIDs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBranchIDs.Location = new System.Drawing.Point(6, 6);
            this.dgvBranchIDs.Name = "dgvBranchIDs";
            this.dgvBranchIDs.Size = new System.Drawing.Size(324, 150);
            this.dgvBranchIDs.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.textBoxNumberOfRowToGet);
            this.tabPage5.Controls.Add(this.buttonExecute);
            this.tabPage5.Controls.Add(this.textBoxOracleQuery);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(595, 482);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Oracle";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(417, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Records";
            // 
            // textBoxNumberOfRowToGet
            // 
            this.textBoxNumberOfRowToGet.Location = new System.Drawing.Point(470, 6);
            this.textBoxNumberOfRowToGet.Name = "textBoxNumberOfRowToGet";
            this.textBoxNumberOfRowToGet.Size = new System.Drawing.Size(100, 20);
            this.textBoxNumberOfRowToGet.TabIndex = 2;
            // 
            // buttonExecute
            // 
            this.buttonExecute.Location = new System.Drawing.Point(6, 280);
            this.buttonExecute.Name = "buttonExecute";
            this.buttonExecute.Size = new System.Drawing.Size(75, 23);
            this.buttonExecute.TabIndex = 1;
            this.buttonExecute.Text = "Execute";
            this.buttonExecute.UseVisualStyleBackColor = true;
            this.buttonExecute.Click += new System.EventHandler(this.buttonExecute_Click);
            // 
            // textBoxOracleQuery
            // 
            this.textBoxOracleQuery.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOracleQuery.Location = new System.Drawing.Point(5, 32);
            this.textBoxOracleQuery.Multiline = true;
            this.textBoxOracleQuery.Name = "textBoxOracleQuery";
            this.textBoxOracleQuery.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxOracleQuery.Size = new System.Drawing.Size(565, 242);
            this.textBoxOracleQuery.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox_Port_Oracle);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.textBox_IP_Oracle);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.dtpFromDateVietsea);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.dtpToDateVietsea);
            this.groupBox4.Location = new System.Drawing.Point(12, 257);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(182, 133);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "VIETSEA";
            // 
            // textBox_Port_Oracle
            // 
            this.textBox_Port_Oracle.Location = new System.Drawing.Point(76, 45);
            this.textBox_Port_Oracle.Name = "textBox_Port_Oracle";
            this.textBox_Port_Oracle.Size = new System.Drawing.Size(100, 20);
            this.textBox_Port_Oracle.TabIndex = 5;
            this.textBox_Port_Oracle.Leave += new System.EventHandler(this.textBox_Port_Oracle_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(48, 13);
            this.label25.TabIndex = 6;
            this.label25.Text = "Máy chủ";
            // 
            // textBox_IP_Oracle
            // 
            this.textBox_IP_Oracle.Location = new System.Drawing.Point(76, 19);
            this.textBox_IP_Oracle.Name = "textBox_IP_Oracle";
            this.textBox_IP_Oracle.Size = new System.Drawing.Size(100, 20);
            this.textBox_IP_Oracle.TabIndex = 4;
            this.textBox_IP_Oracle.Leave += new System.EventHandler(this.textBox_IP_Oracle_Leave);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 48);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(32, 13);
            this.label26.TabIndex = 7;
            this.label26.Text = "Cổng";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(88, 81);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Đến ngày";
            // 
            // dtpFromDateVietsea
            // 
            this.dtpFromDateVietsea.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDateVietsea.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDateVietsea.Location = new System.Drawing.Point(6, 98);
            this.dtpFromDateVietsea.Name = "dtpFromDateVietsea";
            this.dtpFromDateVietsea.Size = new System.Drawing.Size(79, 20);
            this.dtpFromDateVietsea.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Từ ngày";
            // 
            // dtpToDateVietsea
            // 
            this.dtpToDateVietsea.CustomFormat = "dd/MM/yyyy";
            this.dtpToDateVietsea.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDateVietsea.Location = new System.Drawing.Point(91, 98);
            this.dtpToDateVietsea.Name = "dtpToDateVietsea";
            this.dtpToDateVietsea.Size = new System.Drawing.Size(79, 20);
            this.dtpToDateVietsea.TabIndex = 3;
            // 
            // checkBox_pinForm
            // 
            this.checkBox_pinForm.AutoSize = true;
            this.checkBox_pinForm.Location = new System.Drawing.Point(749, 11);
            this.checkBox_pinForm.Name = "checkBox_pinForm";
            this.checkBox_pinForm.Size = new System.Drawing.Size(50, 17);
            this.checkBox_pinForm.TabIndex = 5;
            this.checkBox_pinForm.Text = "Ghim";
            this.checkBox_pinForm.UseVisualStyleBackColor = true;
            this.checkBox_pinForm.CheckedChanged += new System.EventHandler(this.checkBox_pinForm_CheckedChanged);
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.Location = new System.Drawing.Point(230, 357);
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.buttonEdit1.Size = new System.Drawing.Size(277, 20);
            this.buttonEdit1.TabIndex = 31;
            // 
            // checkBoxGetRetailPrice
            // 
            this.checkBoxGetRetailPrice.AutoSize = true;
            this.checkBoxGetRetailPrice.Location = new System.Drawing.Point(199, 328);
            this.checkBoxGetRetailPrice.Name = "checkBoxGetRetailPrice";
            this.checkBoxGetRetailPrice.Size = new System.Drawing.Size(92, 17);
            this.checkBoxGetRetailPrice.TabIndex = 25;
            this.checkBoxGetRetailPrice.Text = "Lấy giá bán lẻ";
            this.checkBoxGetRetailPrice.UseVisualStyleBackColor = true;
            // 
            // FormCSDL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 561);
            this.Controls.Add(this.checkBox_pinForm);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.ButtonHideForm);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormCSDL";
            this.Text = "FormCSDL";
            this.VisibleChanged += new System.EventHandler(this.FormCSDL_VisibleChanged);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlKHO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewKHO)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTiLeLai)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_EinvoiceList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_EinvoiceList)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMaNCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMaNCCView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_DS_Hoadon_vietsea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3_DS_hoadon)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_BankStatement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_BankStatement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBranchIDs)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxDBName;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.ComboBox comboBoxZone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ButtonHideForm;
        private System.Windows.Forms.Label label5;
        private CustomDateTimePicker dtpToDate;
        private System.Windows.Forms.Label label4;
        private CustomDateTimePicker dtpFromDate;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonDownloadBANGMA;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonGetRetailTranDetail;
        private CustomTextBox textBoxTypeTranID;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label7;
        private CustomDateTimePicker dtpToDateVietsea;
        private System.Windows.Forms.CheckBox checkBoxHienTonVIETSEA;
        private System.Windows.Forms.Label label9;
        private CustomDateTimePicker dtpFromDateVietsea;
        private System.Windows.Forms.Button buttonGetStockInfo;
        private System.Windows.Forms.DataGridView dgvBranchIDs;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button buttonFastReportVCB_NXT;
        private System.Windows.Forms.Button buttonFastReportVCB_RevenueGroupByBranch;
        private System.Windows.Forms.Button buttonExportFileBarcodeToPrint;
        private System.Windows.Forms.Button buttonCalculateGoodsAvailable;
        private MyUserControl.ComboBoxSheetName comboBoxSheetName2;
        private System.Windows.Forms.Button buttonCheckNegativeStockEveryDay;
        private System.Windows.Forms.DataGridView dgvTiLeLai;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private MyUserControl.ComboBoxSheetName comboBox_sheet_tồn;
        private CustomTextBox textBox_số_tiền_random;
        private System.Windows.Forms.Label label13;
        private CustomTextBox textBox_SoCT;
        private CustomTextBox textBox_CustomerID;
        private System.Windows.Forms.Label label14;
        private MyUserControl.FolderBroswerTextBox folderBroswerTextBoxSaveImportFileLocation;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBoxBoHangXuatTra;
        private System.Windows.Forms.Button buttonLocRaMaHangThua;
        private System.Windows.Forms.Button buttonGetDataExportInner;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button buttonExecute;
        private System.Windows.Forms.TextBox textBoxOracleQuery;
        private System.Windows.Forms.RadioButton radioButtonMember;
        private System.Windows.Forms.RadioButton radioButtonKHACHLE;
        private MyUserControl.CustomComboBox comboBoxCustomersID;
        private System.Windows.Forms.CheckBox checkBoxBoMaDaXuat;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxNumberOfRowToGet;
        private System.Windows.Forms.Label label19;
        private MyUserControl.CustomComboBox customComboBoxVATrate;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button button_Load_XBAN;
        private System.Windows.Forms.Button button_CreateFileImport_Einvoice;
        private System.Windows.Forms.Label label20;
        private MyUserControl.FolderBroswerTextBox folderBroswerTextBox_SaveLocation_import_iHoaDon;
        private System.Windows.Forms.Button button_download_PDF_einvoice_by_selenium;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button buttonGetDataToMakeReceipt;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button buttonViewProductDetail;
        private System.Windows.Forms.TextBox textBoxProduct_codes;
        private System.Windows.Forms.Button buttonLoadInvoice;
        private System.Windows.Forms.TextBox textBoxMAKHACHHANG;
        private System.Windows.Forms.Button buttonGetDataExportSTLTTAN;
        private System.Windows.Forms.Button buttonApplyPrice;
        private System.Windows.Forms.Button button3_UploadEinvoiceSelenium;
        private System.Windows.Forms.Button buttonLoadTonCuoi_v2;
        private System.Windows.Forms.Button button_DownloadInvoiceInfoToCheck;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button buttonQuickViewRetail;
        private System.Windows.Forms.Button button_TachBill;
        private System.Windows.Forms.Button button_LoadBangKeBanRa;
        private System.Windows.Forms.TextBox textBox_Port_Oracle;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox_IP_Oracle;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button_LoadDSKhoVietsea;
        private DevExpress.XtraGrid.GridControl gridControl_EinvoiceList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_EinvoiceList;
        private System.Windows.Forms.Button button_BC_S38;
        private System.Windows.Forms.Button button_load_makhachhang_vietsea;
        private System.Windows.Forms.CheckBox checkBox_pinForm;
        private System.Windows.Forms.Button button_download_saoke_techcombank;
        private System.Windows.Forms.Button button_download_saoke_vietcombank;
        private System.Windows.Forms.Button button_check_xnt_vietsea;
        private System.Windows.Forms.Button button_TH_CONGNO_331;
        private DevExpress.XtraGrid.GridControl gridControl_DS_Hoadon_vietsea;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3_DS_hoadon;
        private System.Windows.Forms.Button button_create_PC_auto;
        private System.Windows.Forms.Button button_AutoImport;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMaNCC;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEditMaNCCView;
        private System.Windows.Forms.Button button_load_bangke_muavao;
        private System.Windows.Forms.Button button_Tao_PhieuThu_Auto;
        private System.Windows.Forms.CheckBox checkBox_RoundExpPrice;
        private System.Windows.Forms.Button button_analyzeStatement;
        private DevExpress.XtraGrid.GridControl gridControl_BankStatement;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_BankStatement;
        private System.Windows.Forms.Button button_AutoCreateBankingTransantions;
        private System.Windows.Forms.Button button_AutoCreateBillRetail;
        private System.Windows.Forms.Button buttonCheckMST;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonLoadFileImportIhoadon;
        private System.Windows.Forms.Button buttonUploadEinvoiceThanhCong;
        private System.Windows.Forms.Button buttonCheckMAKHOXUAT_TAIKHOAN;
        private System.Windows.Forms.Button buttonCheckMaKhoNhap_maVAT;
        private System.Windows.Forms.Button buttonViewImpHistory;
        private System.Windows.Forms.Button buttonLoadGoodsTC;
        private System.Windows.Forms.Button buttonLoadDMVTHHvietsea;
        private System.Windows.Forms.Button buttonUploadInvoiceOneByOne;
        //private MyUserControl.ButtonEditBrowser textEditBrowser1;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        private DevExpress.XtraGrid.GridControl gridControlKHO;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewKHO;
        private System.Windows.Forms.Button buttonViewKhoVAT;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonTaoDonDatHang;
        private System.Windows.Forms.CheckBox checkBoxGetRetailPrice;
    }
}