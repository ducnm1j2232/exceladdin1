﻿using Microsoft.Office.Interop.Excel;
using System;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.Forms
{
    public partial class XtraFormSettings : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormSettings()
        {
            InitializeComponent();

            gridControl1.DataSource = null;
            gridControl1.DataSource = Config.DataTables.GlobalSettings;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            DataTable dataTable = gridControl1.DataSource as DataTable;
            if (dataTable != null)
            {
                dataTable.WriteXml(Config.XmlFilename.GlobalSettings,System.Data.XmlWriteMode.WriteSchema);
                System.Windows.Forms.MessageBox.Show(this, "Lưu cài đặt thành công");
            }
        }
    }
}