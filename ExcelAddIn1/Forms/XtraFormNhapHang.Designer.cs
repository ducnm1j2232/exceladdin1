﻿namespace ExcelAddIn1.Forms
{
    partial class XtraFormNhapHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem5 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem6 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageNhapHang = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxPrintType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonStopWorking = new DevExpress.XtraEditors.SimpleButton();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.buttonStartWorking = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCreateImportToPublicFolder = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxKhoNhap = new DevExpress.XtraEditors.ComboBoxEdit();
            this.buttonAutoImport2 = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxTotalAmount = new ExcelAddIn1.CustomTextBox();
            this.textBoxVat = new ExcelAddIn1.CustomTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxImpVat = new DevExpress.XtraEditors.ComboBoxEdit();
            this.buttonClearTempGrid = new DevExpress.XtraEditors.SimpleButton();
            this.labelSerialNumberLength = new System.Windows.Forms.Label();
            this.comboBoxTranType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.buttonSelectAllRecord = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gridTransactionDetails = new DevExpress.XtraGrid.GridControl();
            this.gridViewTransantionDetails = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gridSumaryOfTransactions = new DevExpress.XtraGrid.GridControl();
            this.gridViewSumaryOfTransactions = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gridTemp = new DevExpress.XtraGrid.GridControl();
            this.gridViewTemp = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.buttonSearchTransactions = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxDesiredAmount = new ExcelAddIn1.CustomTextBox();
            this.buttonCreateImportFile = new DevExpress.XtraEditors.SimpleButton();
            this.numericUpDownNumberOfPrint = new System.Windows.Forms.NumericUpDown();
            this.textBoxInvoiceNumber = new ExcelAddIn1.CustomTextBox();
            this.dateEditInvoiceDate = new ExcelAddIn1.MyUserControl.CustomDateEdit();
            this.textBoxSerialNumber = new ExcelAddIn1.CustomTextBox();
            this.xtraTabPageSystem = new DevExpress.XtraTab.XtraTabPage();
            this.gridBangMa = new DevExpress.XtraGrid.GridControl();
            this.gridViewBangMa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.buttonInitBangMa = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditTranDateVietsea = new ExcelAddIn1.MyUserControl.CustomDateEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.buttonLoadInvoiceDataFromExcelSheet = new DevExpress.XtraEditors.SimpleButton();
            this.gridInvoiceData = new DevExpress.XtraGrid.GridControl();
            this.gridViewInvoiceData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPageCongNo = new DevExpress.XtraTab.XtraTabPage();
            this.dropDownButton1 = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem1_HachToanPhieuChi = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.buttonGetInvoiceInfoCopyToBangKeGN = new DevExpress.XtraEditors.SimpleButton();
            this.gridSoChiTietTKVietsea = new DevExpress.XtraGrid.GridControl();
            this.gridViewSoChiTietTKVietsea = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridDetailByOneAccountThanhCong = new DevExpress.XtraGrid.GridControl();
            this.gridViewDetailByOneAccountThanhCong = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.buttonCreatePayment = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.buttonLoadTHCongNo = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlTHCongNo = new DevExpress.XtraGrid.GridControl();
            this.gridViewTHCongNo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPageXuatBill = new DevExpress.XtraTab.XtraTabPage();
            this.buttonAutoCreateRetailTranVietsea = new DevExpress.XtraEditors.SimpleButton();
            this.gridRetailTranDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewRetailTranDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.buttonGetRetailTran = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxRetailTranID = new ExcelAddIn1.CustomTextBox();
            this.xtraTabPageInvoice = new DevExpress.XtraTab.XtraTabPage();
            this.buttonDownloadihoadonPDF = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCheckInvoiceTCT = new DevExpress.XtraEditors.SimpleButton();
            this.buttonDownloadInvoiceListFromHoadondientugdtgovvn = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.buttonExportExcelPL43 = new DevExpress.XtraEditors.SimpleButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonEditHTKKDataFileXml = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCopyDataToXmlFile = new DevExpress.XtraEditors.SimpleButton();
            this.buttonLoadHHBanRa = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.dropDownButton2 = new DevExpress.XtraEditors.DropDownButton();
            this.buttonStockinfo = new System.Windows.Forms.Button();
            this.buttonNXT4COT = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.buttonTongHop131 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditAccountNumber = new DevExpress.XtraEditors.TextEdit();
            this.buttonAutoImport = new DevExpress.XtraEditors.SimpleButton();
            this.searchCustomers = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchCustomersView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelStatusMSSQLConnection = new DevExpress.XtraEditors.LabelControl();
            this.label2 = new System.Windows.Forms.Label();
            this.gridLookUpEditMaNCCVietsea = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEditMaNCCVietseaView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.buttonChangeConnectionMSSQL = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditThanhCongEndDate = new ExcelAddIn1.MyUserControl.CustomDateEdit();
            this.dateEditThanhCongStartDate = new ExcelAddIn1.MyUserControl.CustomDateEdit();
            this.groupBoxVietsea = new System.Windows.Forms.GroupBox();
            this.textBoxMSTVietsea = new System.Windows.Forms.TextBox();
            this.richTextBoxTenNCCVietsea = new System.Windows.Forms.RichTextBox();
            this.labelOracleStatus = new System.Windows.Forms.Label();
            this.buttonChangeConnectionOracle = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditVietseaEndDate = new ExcelAddIn1.MyUserControl.CustomDateEdit();
            this.dateEditVietseaStartDate = new ExcelAddIn1.MyUserControl.CustomDateEdit();
            this.buttonSoChiTiet3311 = new DevExpress.XtraEditors.SimpleButton();
            this.groupBoxThanhCong = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.richTextBoxNewStatusTheoDoiCongNo = new System.Windows.Forms.RichTextBox();
            this.buttonUpdateStatusCongNo331 = new DevExpress.XtraEditors.SimpleButton();
            this.richTextBoxStatusTimeCheck = new System.Windows.Forms.RichTextBox();
            this.buttonToMauDoiChieu = new DevExpress.XtraEditors.SimpleButton();
            this.buttonGetDetailByOneAccount = new DevExpress.XtraEditors.SimpleButton();
            this.textEditCustomerIDs = new DevExpress.XtraEditors.TextEdit();
            this.defaultToolTipController1 = new DevExpress.Utils.DefaultToolTipController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageNhapHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxPrintType.Properties)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxKhoNhap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxImpVat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxTranType.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTransactionDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTransantionDetails)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSumaryOfTransactions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSumaryOfTransactions)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumberOfPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInvoiceDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInvoiceDate.Properties)).BeginInit();
            this.xtraTabPageSystem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBangMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBangMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTranDateVietsea.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTranDateVietsea.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridInvoiceData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInvoiceData)).BeginInit();
            this.xtraTabPageCongNo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSoChiTietTKVietsea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSoChiTietTKVietsea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetailByOneAccountThanhCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetailByOneAccountThanhCong)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTHCongNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTHCongNo)).BeginInit();
            this.xtraTabPageXuatBill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRetailTranDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRetailTranDetail)).BeginInit();
            this.xtraTabPageInvoice.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAccountNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchCustomers.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchCustomersView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMaNCCVietsea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMaNCCVietseaView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditThanhCongEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditThanhCongEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditThanhCongStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditThanhCongStartDate.Properties)).BeginInit();
            this.groupBoxVietsea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVietseaEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVietseaEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVietseaStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVietseaStartDate.Properties)).BeginInit();
            this.groupBoxThanhCong.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomerIDs.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(269, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageNhapHang;
            this.xtraTabControl1.Size = new System.Drawing.Size(1079, 668);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageSystem,
            this.xtraTabPageNhapHang,
            this.xtraTabPage3,
            this.xtraTabPageCongNo,
            this.xtraTabPage5,
            this.xtraTabPageXuatBill,
            this.xtraTabPageInvoice,
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage4,
            this.xtraTabPage6});
            // 
            // xtraTabPageNhapHang
            // 
            this.xtraTabPageNhapHang.Controls.Add(this.labelControl10);
            this.xtraTabPageNhapHang.Controls.Add(this.labelControl9);
            this.xtraTabPageNhapHang.Controls.Add(this.labelControl8);
            this.xtraTabPageNhapHang.Controls.Add(this.labelControl7);
            this.xtraTabPageNhapHang.Controls.Add(this.labelControl6);
            this.xtraTabPageNhapHang.Controls.Add(this.labelControl5);
            this.xtraTabPageNhapHang.Controls.Add(this.labelControl4);
            this.xtraTabPageNhapHang.Controls.Add(this.labelControl3);
            this.xtraTabPageNhapHang.Controls.Add(this.labelControl2);
            this.xtraTabPageNhapHang.Controls.Add(this.comboBoxPrintType);
            this.xtraTabPageNhapHang.Controls.Add(this.groupBox4);
            this.xtraTabPageNhapHang.Controls.Add(this.buttonCreateImportToPublicFolder);
            this.xtraTabPageNhapHang.Controls.Add(this.comboBoxKhoNhap);
            this.xtraTabPageNhapHang.Controls.Add(this.buttonAutoImport2);
            this.xtraTabPageNhapHang.Controls.Add(this.textBoxTotalAmount);
            this.xtraTabPageNhapHang.Controls.Add(this.textBoxVat);
            this.xtraTabPageNhapHang.Controls.Add(this.label8);
            this.xtraTabPageNhapHang.Controls.Add(this.comboBoxImpVat);
            this.xtraTabPageNhapHang.Controls.Add(this.buttonClearTempGrid);
            this.xtraTabPageNhapHang.Controls.Add(this.labelSerialNumberLength);
            this.xtraTabPageNhapHang.Controls.Add(this.comboBoxTranType);
            this.xtraTabPageNhapHang.Controls.Add(this.buttonSelectAllRecord);
            this.xtraTabPageNhapHang.Controls.Add(this.groupBox2);
            this.xtraTabPageNhapHang.Controls.Add(this.groupBox1);
            this.xtraTabPageNhapHang.Controls.Add(this.groupBox3);
            this.xtraTabPageNhapHang.Controls.Add(this.buttonSearchTransactions);
            this.xtraTabPageNhapHang.Controls.Add(this.textBoxDesiredAmount);
            this.xtraTabPageNhapHang.Controls.Add(this.buttonCreateImportFile);
            this.xtraTabPageNhapHang.Controls.Add(this.numericUpDownNumberOfPrint);
            this.xtraTabPageNhapHang.Controls.Add(this.textBoxInvoiceNumber);
            this.xtraTabPageNhapHang.Controls.Add(this.dateEditInvoiceDate);
            this.xtraTabPageNhapHang.Controls.Add(this.textBoxSerialNumber);
            this.xtraTabPageNhapHang.Name = "xtraTabPageNhapHang";
            this.xtraTabPageNhapHang.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPageNhapHang.Text = "Nhập hàng";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(14, 560);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(48, 13);
            this.labelControl10.TabIndex = 38;
            this.labelControl10.Text = "Print Type";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(14, 532);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(45, 13);
            this.labelControl9.TabIndex = 37;
            this.labelControl9.Text = "Số bản in";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(14, 507);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(56, 13);
            this.labelControl8.TabIndex = 36;
            this.labelControl8.Text = "Số hóa đơn";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(14, 481);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(54, 13);
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "Ký hiệu HĐ";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(14, 456);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(44, 13);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Ngày HĐ";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(14, 430);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(45, 13);
            this.labelControl5.TabIndex = 35;
            this.labelControl5.Text = "Tổng tiền";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(14, 405);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(21, 13);
            this.labelControl4.TabIndex = 34;
            this.labelControl4.Text = "VAT";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(14, 378);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(51, 13);
            this.labelControl3.TabIndex = 33;
            this.labelControl3.Text = "Thành tiền";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(14, 353);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(46, 13);
            this.labelControl2.TabIndex = 32;
            this.labelControl2.Text = "Kho nhập";
            // 
            // comboBoxPrintType
            // 
            this.comboBoxPrintType.Location = new System.Drawing.Point(96, 557);
            this.comboBoxPrintType.Name = "comboBoxPrintType";
            this.comboBoxPrintType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxPrintType.Size = new System.Drawing.Size(145, 20);
            this.comboBoxPrintType.TabIndex = 31;
            // 
            // groupBox4
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.groupBox4, DevExpress.Utils.DefaultBoolean.Default);
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.buttonStopWorking);
            this.groupBox4.Controls.Add(this.richTextBox1);
            this.groupBox4.Controls.Add(this.buttonStartWorking);
            this.groupBox4.Location = new System.Drawing.Point(867, 316);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(205, 324);
            this.groupBox4.TabIndex = 29;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Auto Worker";
            // 
            // buttonStopWorking
            // 
            this.buttonStopWorking.Location = new System.Drawing.Point(113, 263);
            this.buttonStopWorking.Name = "buttonStopWorking";
            this.buttonStopWorking.Size = new System.Drawing.Size(92, 23);
            this.buttonStopWorking.TabIndex = 2;
            this.buttonStopWorking.Text = "Dừng";
            this.buttonStopWorking.Click += new System.EventHandler(this.buttonStopWorking_Click);
            // 
            // richTextBox1
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.richTextBox1, DevExpress.Utils.DefaultBoolean.Default);
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(6, 19);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(193, 238);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "Chức năng này tự động thực hiện việc nhập hóa đơn đầu vào";
            // 
            // buttonStartWorking
            // 
            this.buttonStartWorking.Location = new System.Drawing.Point(6, 263);
            this.buttonStartWorking.Name = "buttonStartWorking";
            this.buttonStartWorking.Size = new System.Drawing.Size(95, 23);
            this.buttonStartWorking.TabIndex = 0;
            this.buttonStartWorking.Text = "Bắt đầu làm việc";
            this.buttonStartWorking.Click += new System.EventHandler(this.buttonStartWorking_Click);
            // 
            // buttonCreateImportToPublicFolder
            // 
            this.buttonCreateImportToPublicFolder.Location = new System.Drawing.Point(133, 589);
            this.buttonCreateImportToPublicFolder.Name = "buttonCreateImportToPublicFolder";
            this.buttonCreateImportToPublicFolder.Size = new System.Drawing.Size(109, 23);
            this.buttonCreateImportToPublicFolder.TabIndex = 26;
            this.buttonCreateImportToPublicFolder.Text = "Tạo file import public";
            this.buttonCreateImportToPublicFolder.Click += new System.EventHandler(this.buttonCreateImportToPublicFolder_Click);
            // 
            // comboBoxKhoNhap
            // 
            this.comboBoxKhoNhap.Location = new System.Drawing.Point(96, 350);
            this.comboBoxKhoNhap.Name = "comboBoxKhoNhap";
            this.comboBoxKhoNhap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxKhoNhap.Size = new System.Drawing.Size(144, 20);
            this.comboBoxKhoNhap.TabIndex = 26;
            // 
            // buttonAutoImport2
            // 
            this.buttonAutoImport2.Location = new System.Drawing.Point(14, 617);
            this.buttonAutoImport2.Name = "buttonAutoImport2";
            this.buttonAutoImport2.Size = new System.Drawing.Size(113, 23);
            this.buttonAutoImport2.TabIndex = 25;
            this.buttonAutoImport2.Text = "Auto import tự động lưu";
            this.buttonAutoImport2.Click += new System.EventHandler(this.buttonAutoImport2_Click);
            // 
            // textBoxTotalAmount
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.textBoxTotalAmount, DevExpress.Utils.DefaultBoolean.Default);
            this.textBoxTotalAmount.Location = new System.Drawing.Point(96, 427);
            this.textBoxTotalAmount.Name = "textBoxTotalAmount";
            this.textBoxTotalAmount.SelectAllOnClick = false;
            this.textBoxTotalAmount.Size = new System.Drawing.Size(145, 20);
            this.textBoxTotalAmount.TabIndex = 14;
            // 
            // textBoxVat
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.textBoxVat, DevExpress.Utils.DefaultBoolean.Default);
            this.textBoxVat.Location = new System.Drawing.Point(96, 401);
            this.textBoxVat.Name = "textBoxVat";
            this.textBoxVat.SelectAllOnClick = false;
            this.textBoxVat.Size = new System.Drawing.Size(145, 20);
            this.textBoxVat.TabIndex = 12;
            this.textBoxVat.TextChanged += new System.EventHandler(this.textBoxVat_TextChanged);
            // 
            // label8
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.label8, DevExpress.Utils.DefaultBoolean.Default);
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(248, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Thuế suất";
            // 
            // comboBoxImpVat
            // 
            this.comboBoxImpVat.Location = new System.Drawing.Point(318, 7);
            this.comboBoxImpVat.Name = "comboBoxImpVat";
            this.comboBoxImpVat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxImpVat.Properties.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.comboBoxImpVat_Properties_Closed);
            this.comboBoxImpVat.Size = new System.Drawing.Size(55, 20);
            this.comboBoxImpVat.TabIndex = 5;
            // 
            // buttonClearTempGrid
            // 
            this.buttonClearTempGrid.Location = new System.Drawing.Point(96, 321);
            this.buttonClearTempGrid.Name = "buttonClearTempGrid";
            this.buttonClearTempGrid.Size = new System.Drawing.Size(145, 23);
            this.buttonClearTempGrid.TabIndex = 8;
            this.buttonClearTempGrid.Text = "Xóa bảng tạm";
            this.buttonClearTempGrid.Click += new System.EventHandler(this.buttonClearTempGrid_Click);
            // 
            // labelSerialNumberLength
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.labelSerialNumberLength, DevExpress.Utils.DefaultBoolean.Default);
            this.labelSerialNumberLength.AutoSize = true;
            this.labelSerialNumberLength.Location = new System.Drawing.Point(206, 508);
            this.labelSerialNumberLength.Name = "labelSerialNumberLength";
            this.labelSerialNumberLength.Size = new System.Drawing.Size(35, 13);
            this.labelSerialNumberLength.TabIndex = 19;
            this.labelSerialNumberLength.Text = "label7";
            // 
            // comboBoxTranType
            // 
            this.comboBoxTranType.Location = new System.Drawing.Point(6, 7);
            this.comboBoxTranType.Name = "comboBoxTranType";
            this.comboBoxTranType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxTranType.Properties.SelectedIndexChanged += new System.EventHandler(this.comboBoxTranType_SelectedIndexChanged);
            this.comboBoxTranType.Size = new System.Drawing.Size(137, 20);
            this.comboBoxTranType.TabIndex = 0;
            // 
            // buttonSelectAllRecord
            // 
            this.buttonSelectAllRecord.Location = new System.Drawing.Point(251, 321);
            this.buttonSelectAllRecord.Name = "buttonSelectAllRecord";
            this.buttonSelectAllRecord.Size = new System.Drawing.Size(85, 23);
            this.buttonSelectAllRecord.TabIndex = 3;
            this.buttonSelectAllRecord.Text = "Chọn tất cả";
            this.buttonSelectAllRecord.Click += new System.EventHandler(this.buttonSelectAllRecord_Click);
            // 
            // groupBox2
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.groupBox2, DevExpress.Utils.DefaultBoolean.Default);
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.gridTransactionDetails);
            this.groupBox2.Location = new System.Drawing.Point(248, 34);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(827, 276);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chi tiết giao dịch";
            // 
            // gridTransactionDetails
            // 
            this.gridTransactionDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTransactionDetails.Location = new System.Drawing.Point(3, 16);
            this.gridTransactionDetails.MainView = this.gridViewTransantionDetails;
            this.gridTransactionDetails.Name = "gridTransactionDetails";
            this.gridTransactionDetails.Size = new System.Drawing.Size(821, 257);
            this.gridTransactionDetails.TabIndex = 0;
            this.gridTransactionDetails.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTransantionDetails});
            // 
            // gridViewTransantionDetails
            // 
            this.gridViewTransantionDetails.GridControl = this.gridTransactionDetails;
            this.gridViewTransantionDetails.Name = "gridViewTransantionDetails";
            this.gridViewTransantionDetails.OptionsSelection.MultiSelect = true;
            // 
            // groupBox1
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.groupBox1, DevExpress.Utils.DefaultBoolean.Default);
            this.groupBox1.Controls.Add(this.gridSumaryOfTransactions);
            this.groupBox1.Location = new System.Drawing.Point(3, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(239, 273);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tổng hợp giao dịch";
            // 
            // gridSumaryOfTransactions
            // 
            this.gridSumaryOfTransactions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSumaryOfTransactions.Location = new System.Drawing.Point(3, 16);
            this.gridSumaryOfTransactions.MainView = this.gridViewSumaryOfTransactions;
            this.gridSumaryOfTransactions.Name = "gridSumaryOfTransactions";
            this.gridSumaryOfTransactions.Size = new System.Drawing.Size(233, 254);
            this.gridSumaryOfTransactions.TabIndex = 0;
            this.gridSumaryOfTransactions.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSumaryOfTransactions});
            // 
            // gridViewSumaryOfTransactions
            // 
            this.gridViewSumaryOfTransactions.GridControl = this.gridSumaryOfTransactions;
            this.gridViewSumaryOfTransactions.Name = "gridViewSumaryOfTransactions";
            this.gridViewSumaryOfTransactions.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewSumaryOfTransactions_SelectionChanged);
            // 
            // groupBox3
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.groupBox3, DevExpress.Utils.DefaultBoolean.Default);
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.gridTemp);
            this.groupBox3.Location = new System.Drawing.Point(248, 350);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(613, 293);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Bảng tạm";
            // 
            // gridTemp
            // 
            this.gridTemp.AllowDrop = true;
            this.gridTemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTemp.Location = new System.Drawing.Point(3, 16);
            this.gridTemp.MainView = this.gridViewTemp;
            this.gridTemp.Name = "gridTemp";
            this.gridTemp.Size = new System.Drawing.Size(607, 274);
            this.gridTemp.TabIndex = 0;
            this.gridTemp.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTemp});
            // 
            // gridViewTemp
            // 
            this.gridViewTemp.GridControl = this.gridTemp;
            this.gridViewTemp.Name = "gridViewTemp";
            this.gridViewTemp.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewTemp_CellValueChanged);
            this.gridViewTemp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewTemp_KeyDown);
            // 
            // buttonSearchTransactions
            // 
            this.buttonSearchTransactions.Location = new System.Drawing.Point(149, 5);
            this.buttonSearchTransactions.Name = "buttonSearchTransactions";
            this.buttonSearchTransactions.Size = new System.Drawing.Size(90, 23);
            this.buttonSearchTransactions.TabIndex = 1;
            this.buttonSearchTransactions.Text = "Tìm giao dịch";
            this.buttonSearchTransactions.Click += new System.EventHandler(this.buttonSearchTransactions_Click);
            // 
            // textBoxDesiredAmount
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.textBoxDesiredAmount, DevExpress.Utils.DefaultBoolean.Default);
            this.textBoxDesiredAmount.Location = new System.Drawing.Point(96, 375);
            this.textBoxDesiredAmount.Name = "textBoxDesiredAmount";
            this.textBoxDesiredAmount.SelectAllOnClick = false;
            this.textBoxDesiredAmount.Size = new System.Drawing.Size(145, 20);
            this.textBoxDesiredAmount.TabIndex = 10;
            this.textBoxDesiredAmount.TextChanged += new System.EventHandler(this.textBoxDesiredAmount_TextChanged);
            this.textBoxDesiredAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxDesiredAmount_KeyDown);
            // 
            // buttonCreateImportFile
            // 
            this.buttonCreateImportFile.Location = new System.Drawing.Point(14, 589);
            this.buttonCreateImportFile.Name = "buttonCreateImportFile";
            this.buttonCreateImportFile.Size = new System.Drawing.Size(113, 23);
            this.buttonCreateImportFile.TabIndex = 24;
            this.buttonCreateImportFile.Text = "Tạo file Import v1";
            this.buttonCreateImportFile.Click += new System.EventHandler(this.buttonCreateImportFile_Click);
            // 
            // numericUpDownNumberOfPrint
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.numericUpDownNumberOfPrint, DevExpress.Utils.DefaultBoolean.Default);
            this.numericUpDownNumberOfPrint.Location = new System.Drawing.Point(96, 530);
            this.numericUpDownNumberOfPrint.Name = "numericUpDownNumberOfPrint";
            this.numericUpDownNumberOfPrint.Size = new System.Drawing.Size(145, 20);
            this.numericUpDownNumberOfPrint.TabIndex = 23;
            // 
            // textBoxInvoiceNumber
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.textBoxInvoiceNumber, DevExpress.Utils.DefaultBoolean.Default);
            this.textBoxInvoiceNumber.Location = new System.Drawing.Point(96, 504);
            this.textBoxInvoiceNumber.Name = "textBoxInvoiceNumber";
            this.textBoxInvoiceNumber.SelectAllOnClick = false;
            this.textBoxInvoiceNumber.Size = new System.Drawing.Size(104, 20);
            this.textBoxInvoiceNumber.TabIndex = 21;
            this.textBoxInvoiceNumber.TextChanged += new System.EventHandler(this.TextBoxInvoiceNumber_TextChanged);
            // 
            // dateEditInvoiceDate
            // 
            this.dateEditInvoiceDate.EditValue = null;
            this.dateEditInvoiceDate.Location = new System.Drawing.Point(96, 453);
            this.dateEditInvoiceDate.Name = "dateEditInvoiceDate";
            this.dateEditInvoiceDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditInvoiceDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditInvoiceDate.Properties.MaskSettings.Set("useAdvancingCaret", true);
            this.dateEditInvoiceDate.Properties.MaskSettings.Set("mask", "dd/MM/yyyy");
            this.dateEditInvoiceDate.Properties.UseMaskAsDisplayFormat = true;
            this.dateEditInvoiceDate.Size = new System.Drawing.Size(145, 20);
            this.dateEditInvoiceDate.TabIndex = 16;
            // 
            // textBoxSerialNumber
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.textBoxSerialNumber, DevExpress.Utils.DefaultBoolean.Default);
            this.textBoxSerialNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxSerialNumber.Location = new System.Drawing.Point(96, 478);
            this.textBoxSerialNumber.Name = "textBoxSerialNumber";
            this.textBoxSerialNumber.SelectAllOnClick = false;
            this.textBoxSerialNumber.Size = new System.Drawing.Size(146, 20);
            this.textBoxSerialNumber.TabIndex = 18;
            // 
            // xtraTabPageSystem
            // 
            this.xtraTabPageSystem.Controls.Add(this.gridBangMa);
            this.xtraTabPageSystem.Controls.Add(this.buttonInitBangMa);
            this.xtraTabPageSystem.Controls.Add(this.labelControl1);
            this.xtraTabPageSystem.Controls.Add(this.dateEditTranDateVietsea);
            this.xtraTabPageSystem.Name = "xtraTabPageSystem";
            this.xtraTabPageSystem.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPageSystem.Text = "Hệ thống";
            // 
            // gridBangMa
            // 
            this.gridBangMa.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridBangMa.Location = new System.Drawing.Point(11, 93);
            this.gridBangMa.MainView = this.gridViewBangMa;
            this.gridBangMa.Name = "gridBangMa";
            this.gridBangMa.Size = new System.Drawing.Size(1063, 547);
            this.gridBangMa.TabIndex = 3;
            this.gridBangMa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBangMa});
            // 
            // gridViewBangMa
            // 
            this.gridViewBangMa.GridControl = this.gridBangMa;
            this.gridViewBangMa.Name = "gridViewBangMa";
            // 
            // buttonInitBangMa
            // 
            this.buttonInitBangMa.Location = new System.Drawing.Point(11, 64);
            this.buttonInitBangMa.Name = "buttonInitBangMa";
            this.buttonInitBangMa.Size = new System.Drawing.Size(83, 23);
            this.buttonInitBangMa.TabIndex = 2;
            this.buttonInitBangMa.Text = "Load bảng mã";
            this.buttonInitBangMa.Click += new System.EventHandler(this.buttonInitBangMa_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(11, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(76, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Ngày hạch toán";
            // 
            // dateEditTranDateVietsea
            // 
            this.dateEditTranDateVietsea.EditValue = null;
            this.dateEditTranDateVietsea.Location = new System.Drawing.Point(106, 19);
            this.dateEditTranDateVietsea.Name = "dateEditTranDateVietsea";
            this.dateEditTranDateVietsea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTranDateVietsea.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTranDateVietsea.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dateEditTranDateVietsea.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditTranDateVietsea.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEditTranDateVietsea.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditTranDateVietsea.Properties.MaskSettings.Set("useAdvancingCaret", true);
            this.dateEditTranDateVietsea.Properties.MaskSettings.Set("mask", "dd/MM/yyyy");
            this.dateEditTranDateVietsea.Properties.UseMaskAsDisplayFormat = true;
            this.dateEditTranDateVietsea.Size = new System.Drawing.Size(100, 20);
            this.dateEditTranDateVietsea.TabIndex = 0;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.progressBar1);
            this.xtraTabPage3.Controls.Add(this.buttonLoadInvoiceDataFromExcelSheet);
            this.xtraTabPage3.Controls.Add(this.gridInvoiceData);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPage3.Text = "Hóa đơn TCT";
            // 
            // progressBar1
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.progressBar1, DevExpress.Utils.DefaultBoolean.Default);
            this.progressBar1.Location = new System.Drawing.Point(190, 5);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(309, 23);
            this.progressBar1.TabIndex = 2;
            this.progressBar1.Visible = false;
            // 
            // buttonLoadInvoiceDataFromExcelSheet
            // 
            this.buttonLoadInvoiceDataFromExcelSheet.Location = new System.Drawing.Point(3, 5);
            this.buttonLoadInvoiceDataFromExcelSheet.Name = "buttonLoadInvoiceDataFromExcelSheet";
            this.buttonLoadInvoiceDataFromExcelSheet.Size = new System.Drawing.Size(181, 23);
            toolTipItem13.Text = "Chọn một hoặc nhiều file Danh sách hóa đơn tải về từ Tổng Cục Thuế";
            superToolTip9.Items.Add(toolTipItem13);
            this.buttonLoadInvoiceDataFromExcelSheet.SuperTip = superToolTip9;
            this.buttonLoadInvoiceDataFromExcelSheet.TabIndex = 1;
            this.buttonLoadInvoiceDataFromExcelSheet.Text = "Load dữ liệu hóa đơn lên grid";
            this.buttonLoadInvoiceDataFromExcelSheet.Click += new System.EventHandler(this.buttonLoadInvoiceDataFromExcelSheet_Click);
            // 
            // gridInvoiceData
            // 
            this.gridInvoiceData.Location = new System.Drawing.Point(3, 34);
            this.gridInvoiceData.MainView = this.gridViewInvoiceData;
            this.gridInvoiceData.Name = "gridInvoiceData";
            this.gridInvoiceData.Size = new System.Drawing.Size(1071, 419);
            this.gridInvoiceData.TabIndex = 0;
            this.gridInvoiceData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInvoiceData});
            // 
            // gridViewInvoiceData
            // 
            this.gridViewInvoiceData.GridControl = this.gridInvoiceData;
            this.gridViewInvoiceData.Name = "gridViewInvoiceData";
            // 
            // xtraTabPageCongNo
            // 
            this.xtraTabPageCongNo.Controls.Add(this.dropDownButton1);
            this.xtraTabPageCongNo.Controls.Add(this.buttonGetInvoiceInfoCopyToBangKeGN);
            this.xtraTabPageCongNo.Controls.Add(this.gridSoChiTietTKVietsea);
            this.xtraTabPageCongNo.Controls.Add(this.gridDetailByOneAccountThanhCong);
            this.xtraTabPageCongNo.Controls.Add(this.buttonCreatePayment);
            this.xtraTabPageCongNo.Name = "xtraTabPageCongNo";
            this.xtraTabPageCongNo.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPageCongNo.Text = "Chi tiết công nợ";
            // 
            // dropDownButton1
            // 
            this.dropDownButton1.DropDownControl = this.popupMenu1;
            this.dropDownButton1.Location = new System.Drawing.Point(235, 616);
            this.dropDownButton1.Name = "dropDownButton1";
            this.dropDownButton1.Size = new System.Drawing.Size(135, 23);
            this.dropDownButton1.TabIndex = 28;
            this.dropDownButton1.Text = "dropDownButton1";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1_HachToanPhieuChi),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem1_HachToanPhieuChi
            // 
            this.barButtonItem1_HachToanPhieuChi.Caption = "Tạo phiếu chi auto";
            this.barButtonItem1_HachToanPhieuChi.Id = 0;
            this.barButtonItem1_HachToanPhieuChi.Name = "barButtonItem1_HachToanPhieuChi";
            this.barButtonItem1_HachToanPhieuChi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_HachToanPhieuChi_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1_HachToanPhieuChi,
            this.barButtonItem2});
            this.barManager1.MaxItemId = 2;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1348, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 668);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1348, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 668);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1348, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 668);
            // 
            // buttonGetInvoiceInfoCopyToBangKeGN
            // 
            this.buttonGetInvoiceInfoCopyToBangKeGN.Location = new System.Drawing.Point(95, 616);
            this.buttonGetInvoiceInfoCopyToBangKeGN.Name = "buttonGetInvoiceInfoCopyToBangKeGN";
            this.buttonGetInvoiceInfoCopyToBangKeGN.Size = new System.Drawing.Size(134, 23);
            this.buttonGetInvoiceInfoCopyToBangKeGN.TabIndex = 27;
            this.buttonGetInvoiceInfoCopyToBangKeGN.Text = "Copy thông tin hóa đơn";
            this.buttonGetInvoiceInfoCopyToBangKeGN.Click += new System.EventHandler(this.buttonGetInvoiceInfoCopyToBangKeGN_Click);
            // 
            // gridSoChiTietTKVietsea
            // 
            this.gridSoChiTietTKVietsea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSoChiTietTKVietsea.Location = new System.Drawing.Point(3, 316);
            this.gridSoChiTietTKVietsea.MainView = this.gridViewSoChiTietTKVietsea;
            this.gridSoChiTietTKVietsea.Name = "gridSoChiTietTKVietsea";
            this.gridSoChiTietTKVietsea.Size = new System.Drawing.Size(1071, 295);
            this.gridSoChiTietTKVietsea.TabIndex = 1;
            this.gridSoChiTietTKVietsea.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSoChiTietTKVietsea});
            // 
            // gridViewSoChiTietTKVietsea
            // 
            this.gridViewSoChiTietTKVietsea.GridControl = this.gridSoChiTietTKVietsea;
            this.gridViewSoChiTietTKVietsea.Name = "gridViewSoChiTietTKVietsea";
            // 
            // gridDetailByOneAccountThanhCong
            // 
            this.gridDetailByOneAccountThanhCong.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDetailByOneAccountThanhCong.Location = new System.Drawing.Point(3, 4);
            this.gridDetailByOneAccountThanhCong.MainView = this.gridViewDetailByOneAccountThanhCong;
            this.gridDetailByOneAccountThanhCong.Name = "gridDetailByOneAccountThanhCong";
            this.gridDetailByOneAccountThanhCong.Size = new System.Drawing.Size(1071, 289);
            this.gridDetailByOneAccountThanhCong.TabIndex = 0;
            this.gridDetailByOneAccountThanhCong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDetailByOneAccountThanhCong});
            // 
            // gridViewDetailByOneAccountThanhCong
            // 
            this.gridViewDetailByOneAccountThanhCong.GridControl = this.gridDetailByOneAccountThanhCong;
            this.gridViewDetailByOneAccountThanhCong.Name = "gridViewDetailByOneAccountThanhCong";
            // 
            // buttonCreatePayment
            // 
            this.buttonCreatePayment.Location = new System.Drawing.Point(3, 616);
            this.buttonCreatePayment.Name = "buttonCreatePayment";
            this.buttonCreatePayment.Size = new System.Drawing.Size(86, 23);
            this.buttonCreatePayment.TabIndex = 26;
            this.buttonCreatePayment.Text = "Tạo phiếu chi";
            this.buttonCreatePayment.Click += new System.EventHandler(this.buttonCreatePayment_Click);
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.buttonLoadTHCongNo);
            this.xtraTabPage5.Controls.Add(this.gridControlTHCongNo);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPage5.Text = "TH công nợ";
            // 
            // buttonLoadTHCongNo
            // 
            this.buttonLoadTHCongNo.Location = new System.Drawing.Point(3, 13);
            this.buttonLoadTHCongNo.Name = "buttonLoadTHCongNo";
            this.buttonLoadTHCongNo.Size = new System.Drawing.Size(224, 23);
            this.buttonLoadTHCongNo.TabIndex = 1;
            this.buttonLoadTHCongNo.Text = "Tổng hợp công nợ";
            this.buttonLoadTHCongNo.Click += new System.EventHandler(this.buttonLoadTHCongNo_Click);
            // 
            // gridControlTHCongNo
            // 
            this.gridControlTHCongNo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlTHCongNo.Location = new System.Drawing.Point(3, 60);
            this.gridControlTHCongNo.MainView = this.gridViewTHCongNo;
            this.gridControlTHCongNo.Name = "gridControlTHCongNo";
            this.gridControlTHCongNo.Size = new System.Drawing.Size(1063, 559);
            this.gridControlTHCongNo.TabIndex = 0;
            this.gridControlTHCongNo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTHCongNo});
            // 
            // gridViewTHCongNo
            // 
            this.gridViewTHCongNo.GridControl = this.gridControlTHCongNo;
            this.gridViewTHCongNo.Name = "gridViewTHCongNo";
            this.gridViewTHCongNo.OptionsSelection.MultiSelect = true;
            // 
            // xtraTabPageXuatBill
            // 
            this.xtraTabPageXuatBill.Controls.Add(this.buttonAutoCreateRetailTranVietsea);
            this.xtraTabPageXuatBill.Controls.Add(this.gridRetailTranDetail);
            this.xtraTabPageXuatBill.Controls.Add(this.buttonGetRetailTran);
            this.xtraTabPageXuatBill.Controls.Add(this.label7);
            this.xtraTabPageXuatBill.Controls.Add(this.textBoxRetailTranID);
            this.xtraTabPageXuatBill.Name = "xtraTabPageXuatBill";
            this.xtraTabPageXuatBill.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPageXuatBill.Text = "Xuất bill";
            // 
            // buttonAutoCreateRetailTranVietsea
            // 
            this.buttonAutoCreateRetailTranVietsea.Location = new System.Drawing.Point(837, 11);
            this.buttonAutoCreateRetailTranVietsea.Name = "buttonAutoCreateRetailTranVietsea";
            this.buttonAutoCreateRetailTranVietsea.Size = new System.Drawing.Size(87, 23);
            this.buttonAutoCreateRetailTranVietsea.TabIndex = 3;
            this.buttonAutoCreateRetailTranVietsea.Text = "Auto tạo phiếu";
            this.buttonAutoCreateRetailTranVietsea.Click += new System.EventHandler(this.ButtonAutoCreateRetailTranVietsea_Click);
            // 
            // gridRetailTranDetail
            // 
            this.gridRetailTranDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridRetailTranDetail.Location = new System.Drawing.Point(3, 54);
            this.gridRetailTranDetail.MainView = this.gridViewRetailTranDetail;
            this.gridRetailTranDetail.Name = "gridRetailTranDetail";
            this.gridRetailTranDetail.Size = new System.Drawing.Size(1071, 586);
            this.gridRetailTranDetail.TabIndex = 4;
            this.gridRetailTranDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRetailTranDetail});
            // 
            // gridViewRetailTranDetail
            // 
            this.gridViewRetailTranDetail.GridControl = this.gridRetailTranDetail;
            this.gridViewRetailTranDetail.Name = "gridViewRetailTranDetail";
            // 
            // buttonGetRetailTran
            // 
            this.buttonGetRetailTran.Location = new System.Drawing.Point(756, 11);
            this.buttonGetRetailTran.Name = "buttonGetRetailTran";
            this.buttonGetRetailTran.Size = new System.Drawing.Size(75, 23);
            this.buttonGetRetailTran.TabIndex = 2;
            this.buttonGetRetailTran.Text = "Tải giao dịch";
            this.buttonGetRetailTran.Click += new System.EventHandler(this.buttonGetRetailTran_Click);
            // 
            // label7
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.label7, DevExpress.Utils.DefaultBoolean.Default);
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Nhập mã giao dịch";
            // 
            // textBoxRetailTranID
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.textBoxRetailTranID, DevExpress.Utils.DefaultBoolean.Default);
            this.textBoxRetailTranID.Location = new System.Drawing.Point(121, 13);
            this.textBoxRetailTranID.Name = "textBoxRetailTranID";
            this.textBoxRetailTranID.SelectAllOnClick = false;
            this.textBoxRetailTranID.Size = new System.Drawing.Size(629, 20);
            this.textBoxRetailTranID.TabIndex = 1;
            // 
            // xtraTabPageInvoice
            // 
            this.xtraTabPageInvoice.Controls.Add(this.buttonDownloadihoadonPDF);
            this.xtraTabPageInvoice.Controls.Add(this.buttonCheckInvoiceTCT);
            this.xtraTabPageInvoice.Controls.Add(this.buttonDownloadInvoiceListFromHoadondientugdtgovvn);
            this.xtraTabPageInvoice.Name = "xtraTabPageInvoice";
            this.xtraTabPageInvoice.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPageInvoice.Text = "Hóa đơn điện tử";
            // 
            // buttonDownloadihoadonPDF
            // 
            this.buttonDownloadihoadonPDF.Location = new System.Drawing.Point(4, 404);
            this.buttonDownloadihoadonPDF.Name = "buttonDownloadihoadonPDF";
            this.buttonDownloadihoadonPDF.Size = new System.Drawing.Size(246, 23);
            this.buttonDownloadihoadonPDF.TabIndex = 2;
            this.buttonDownloadihoadonPDF.Text = "Tải hóa đơn điện tử PDF lưu";
            this.buttonDownloadihoadonPDF.Click += new System.EventHandler(this.buttonDownloadihoadonPDF_Click);
            // 
            // buttonCheckInvoiceTCT
            // 
            this.buttonCheckInvoiceTCT.Location = new System.Drawing.Point(3, 356);
            this.buttonCheckInvoiceTCT.Name = "buttonCheckInvoiceTCT";
            this.buttonCheckInvoiceTCT.Size = new System.Drawing.Size(247, 23);
            this.buttonCheckInvoiceTCT.TabIndex = 1;
            this.buttonCheckInvoiceTCT.Text = "Check hóa đơn với tổng cục thuế";
            this.buttonCheckInvoiceTCT.Click += new System.EventHandler(this.buttonCheckInvoiceTCT_Click);
            // 
            // buttonDownloadInvoiceListFromHoadondientugdtgovvn
            // 
            this.buttonDownloadInvoiceListFromHoadondientugdtgovvn.Location = new System.Drawing.Point(3, 327);
            this.buttonDownloadInvoiceListFromHoadondientugdtgovvn.Name = "buttonDownloadInvoiceListFromHoadondientugdtgovvn";
            this.buttonDownloadInvoiceListFromHoadondientugdtgovvn.Size = new System.Drawing.Size(247, 23);
            toolTipItem14.Text = "Chỉ nên chọn khoảng thời gian trong vòng 1 tháng, nếu chọn dài hơn thì sẽ không r" +
    "a đủ kết quả";
            superToolTip10.Items.Add(toolTipItem14);
            this.buttonDownloadInvoiceListFromHoadondientugdtgovvn.SuperTip = superToolTip10;
            this.buttonDownloadInvoiceListFromHoadondientugdtgovvn.TabIndex = 0;
            this.buttonDownloadInvoiceListFromHoadondientugdtgovvn.Text = "Tải danh sách hóa đơn điện tử từ tổng cục thuế";
            this.buttonDownloadInvoiceListFromHoadondientugdtgovvn.Click += new System.EventHandler(this.buttonDownloadInvoiceListFromHoadondientugdtgovvn_Click);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.buttonExportExcelPL43);
            this.xtraTabPage1.Controls.Add(this.label12);
            this.xtraTabPage1.Controls.Add(this.label11);
            this.xtraTabPage1.Controls.Add(this.label10);
            this.xtraTabPage1.Controls.Add(this.label9);
            this.xtraTabPage1.Controls.Add(this.buttonEditHTKKDataFileXml);
            this.xtraTabPage1.Controls.Add(this.buttonCopyDataToXmlFile);
            this.xtraTabPage1.Controls.Add(this.buttonLoadHHBanRa);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPage1.Text = "Tờ khai";
            // 
            // buttonExportExcelPL43
            // 
            this.buttonExportExcelPL43.Location = new System.Drawing.Point(13, 356);
            this.buttonExportExcelPL43.Name = "buttonExportExcelPL43";
            this.buttonExportExcelPL43.Size = new System.Drawing.Size(235, 23);
            this.buttonExportExcelPL43.TabIndex = 7;
            this.buttonExportExcelPL43.Text = "2. Xuất file excel bảng kê phụ lục 43";
            this.buttonExportExcelPL43.Click += new System.EventHandler(this.buttonExportExcelPL43_Click);
            // 
            // label12
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.label12, DevExpress.Utils.DefaultBoolean.Default);
            this.label12.AutoSize = true;
            this.label12.Enabled = false;
            this.label12.Location = new System.Drawing.Point(12, 561);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(383, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "7. Khi mở lại thành công -> Ghi dữ liệu trước rồi mới thực hiện các thao tác khác" +
    "";
            // 
            // label11
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.label11, DevExpress.Utils.DefaultBoolean.Default);
            this.label11.AutoSize = true;
            this.label11.Enabled = false;
            this.label11.Location = new System.Drawing.Point(12, 421);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(444, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "2. Tạo tờ khai GTGT trên HTTK, nhập vài dữ liệu fake vào phụ lục 43 -> Ghi lại ->" +
    " Xuất XML";
            // 
            // label10
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.label10, DevExpress.Utils.DefaultBoolean.Default);
            this.label10.AutoSize = true;
            this.label10.Enabled = false;
            this.label10.Location = new System.Drawing.Point(12, 479);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(455, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "4. Quay lại HTKK, chọn Nhập từ Xml -> chọn file XML -> Ghi -> Thông tin kê khai s" +
    "ai ->Vẫn Ghi";
            // 
            // label9
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.label9, DevExpress.Utils.DefaultBoolean.Default);
            this.label9.AutoSize = true;
            this.label9.Enabled = false;
            this.label9.Location = new System.Drawing.Point(12, 537);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(239, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "6. Đóng giao diện tờ khai HTKK hiện tại rồi mở lại";
            // 
            // buttonEditHTKKDataFileXml
            // 
            this.buttonEditHTKKDataFileXml.Enabled = false;
            this.buttonEditHTKKDataFileXml.Location = new System.Drawing.Point(12, 503);
            this.buttonEditHTKKDataFileXml.Name = "buttonEditHTKKDataFileXml";
            this.buttonEditHTKKDataFileXml.Size = new System.Drawing.Size(235, 23);
            toolTipItem15.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipItem15.Text = "Thư mục mặc định sẽ tìm đến là: C:\\Program Files (x86)\\HTKK\\DataFiles";
            toolTipItem16.Text = "Chọn thư mục mã số thuế của đơn vị";
            toolTipItem17.Text = "Chọn file XML có tên kiểu như là: 01_8_MGT_GTGT_TT80...... chú ý có số 8";
            superToolTip11.Items.Add(toolTipItem15);
            superToolTip11.Items.Add(toolTipSeparatorItem5);
            superToolTip11.Items.Add(toolTipItem16);
            superToolTip11.Items.Add(toolTipSeparatorItem6);
            superToolTip11.Items.Add(toolTipItem17);
            this.buttonEditHTKKDataFileXml.SuperTip = superToolTip11;
            this.buttonEditHTKKDataFileXml.TabIndex = 2;
            this.buttonEditHTKKDataFileXml.Text = "5. Sửa dữ liệu của HTKK data file";
            this.buttonEditHTKKDataFileXml.Click += new System.EventHandler(this.buttonEditHTKKDataFileXml_Click);
            // 
            // buttonCopyDataToXmlFile
            // 
            this.buttonCopyDataToXmlFile.Enabled = false;
            this.buttonCopyDataToXmlFile.Location = new System.Drawing.Point(12, 445);
            this.buttonCopyDataToXmlFile.Name = "buttonCopyDataToXmlFile";
            this.buttonCopyDataToXmlFile.Size = new System.Drawing.Size(235, 23);
            toolTipItem18.Text = "Chọn file XML đã tạo từ HTKK";
            superToolTip12.Items.Add(toolTipItem18);
            this.buttonCopyDataToXmlFile.SuperTip = superToolTip12;
            this.buttonCopyDataToXmlFile.TabIndex = 1;
            this.buttonCopyDataToXmlFile.Text = "3. Copy dữ liệu vào file tờ khai XML vừa xuất";
            this.buttonCopyDataToXmlFile.Click += new System.EventHandler(this.buttonCopyDataToXmlFile_Click);
            // 
            // buttonLoadHHBanRa
            // 
            this.buttonLoadHHBanRa.Location = new System.Drawing.Point(13, 327);
            this.buttonLoadHHBanRa.Name = "buttonLoadHHBanRa";
            this.buttonLoadHHBanRa.Size = new System.Drawing.Size(235, 23);
            this.buttonLoadHHBanRa.TabIndex = 0;
            this.buttonLoadHHBanRa.Text = "1. Load danh sách hàng hóa bán ra";
            this.buttonLoadHHBanRa.Click += new System.EventHandler(this.buttonLoadHHBanRa_Click);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPage2.Text = "Kiểm tra";
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(3, 3);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(465, 257);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.dropDownButton2);
            this.xtraTabPage4.Controls.Add(this.buttonStockinfo);
            this.xtraTabPage4.Controls.Add(this.buttonNXT4COT);
            this.xtraTabPage4.Controls.Add(this.simpleButton2);
            this.xtraTabPage4.Controls.Add(this.simpleButton1);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPage4.Text = "N-X-T";
            // 
            // dropDownButton2
            // 
            this.dropDownButton2.DropDownControl = this.popupMenu1;
            this.dropDownButton2.Location = new System.Drawing.Point(13, 497);
            this.dropDownButton2.MenuManager = this.barManager1;
            this.dropDownButton2.Name = "dropDownButton2";
            this.dropDownButton2.Size = new System.Drawing.Size(135, 23);
            this.dropDownButton2.TabIndex = 4;
            this.dropDownButton2.Text = "dropDownButton2";
            // 
            // buttonStockinfo
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.buttonStockinfo, DevExpress.Utils.DefaultBoolean.Default);
            this.buttonStockinfo.Location = new System.Drawing.Point(13, 28);
            this.buttonStockinfo.Name = "buttonStockinfo";
            this.buttonStockinfo.Size = new System.Drawing.Size(75, 23);
            this.buttonStockinfo.TabIndex = 3;
            this.buttonStockinfo.Text = "Stockinfo";
            this.buttonStockinfo.UseVisualStyleBackColor = true;
            this.buttonStockinfo.Click += new System.EventHandler(this.buttonStockinfo_Click);
            // 
            // buttonNXT4COT
            // 
            this.buttonNXT4COT.Location = new System.Drawing.Point(13, 460);
            this.buttonNXT4COT.Name = "buttonNXT4COT";
            this.buttonNXT4COT.Size = new System.Drawing.Size(168, 23);
            this.buttonNXT4COT.TabIndex = 2;
            this.buttonNXT4COT.Text = "Báo cáo NXT 4 cột";
            this.buttonNXT4COT.Click += new System.EventHandler(this.buttonNXT4COT_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(13, 431);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(168, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Load NXT";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(13, 327);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(168, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Load DT-GV quyết toán";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.buttonTongHop131);
            this.xtraTabPage6.Controls.Add(this.label1);
            this.xtraTabPage6.Controls.Add(this.textEditAccountNumber);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(1077, 643);
            this.xtraTabPage6.Text = "BC VCB";
            // 
            // buttonTongHop131
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.buttonTongHop131, DevExpress.Utils.DefaultBoolean.Default);
            this.buttonTongHop131.Location = new System.Drawing.Point(189, 29);
            this.buttonTongHop131.Name = "buttonTongHop131";
            this.buttonTongHop131.Size = new System.Drawing.Size(97, 23);
            this.buttonTongHop131.TabIndex = 14;
            this.buttonTongHop131.Text = "Tổng hợp 131";
            this.buttonTongHop131.UseVisualStyleBackColor = true;
            this.buttonTongHop131.Click += new System.EventHandler(this.ButtonTongHop131_Click);
            // 
            // label1
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.label1, DevExpress.Utils.DefaultBoolean.Default);
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Tài khoản";
            // 
            // textEditAccountNumber
            // 
            this.textEditAccountNumber.EditValue = "131";
            this.textEditAccountNumber.Location = new System.Drawing.Point(83, 31);
            this.textEditAccountNumber.MenuManager = this.barManager1;
            this.textEditAccountNumber.Name = "textEditAccountNumber";
            this.textEditAccountNumber.Size = new System.Drawing.Size(100, 20);
            this.textEditAccountNumber.TabIndex = 12;
            // 
            // buttonAutoImport
            // 
            this.buttonAutoImport.Location = new System.Drawing.Point(134, 287);
            this.buttonAutoImport.Name = "buttonAutoImport";
            this.buttonAutoImport.Size = new System.Drawing.Size(120, 23);
            this.buttonAutoImport.TabIndex = 7;
            this.buttonAutoImport.Text = "Import auto không lưu";
            this.buttonAutoImport.Click += new System.EventHandler(this.buttonAutoImport_Click);
            // 
            // searchCustomers
            // 
            this.searchCustomers.EditValue = "Click để tìm kiếm";
            this.searchCustomers.Location = new System.Drawing.Point(6, 81);
            this.searchCustomers.Name = "searchCustomers";
            this.searchCustomers.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchCustomers.Properties.NullText = "[Click để tìm kiếm]";
            this.searchCustomers.Properties.PopupView = this.searchCustomersView;
            this.searchCustomers.Properties.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.searchCustomers_Properties_Closed);
            this.searchCustomers.Properties.Popup += new System.EventHandler(this.searchCustomers_Properties_Popup);
            this.searchCustomers.Properties.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchCustomers_Properties_KeyDown);
            this.searchCustomers.Size = new System.Drawing.Size(247, 20);
            this.searchCustomers.TabIndex = 4;
            // 
            // searchCustomersView
            // 
            this.searchCustomersView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchCustomersView.Name = "searchCustomersView";
            this.searchCustomersView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchCustomersView.OptionsSelection.MultiSelect = true;
            this.searchCustomersView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.searchCustomersView.OptionsView.ShowGroupPanel = false;
            // 
            // labelStatusMSSQLConnection
            // 
            this.labelStatusMSSQLConnection.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelStatusMSSQLConnection.Appearance.Options.UseForeColor = true;
            this.labelStatusMSSQLConnection.Location = new System.Drawing.Point(104, 25);
            this.labelStatusMSSQLConnection.Name = "labelStatusMSSQLConnection";
            this.labelStatusMSSQLConnection.Size = new System.Drawing.Size(60, 13);
            this.labelStatusMSSQLConnection.TabIndex = 1;
            this.labelStatusMSSQLConnection.Text = "Chưa kết nối";
            // 
            // label2
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.label2, DevExpress.Utils.DefaultBoolean.Default);
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Mã NCC";
            // 
            // gridLookUpEditMaNCCVietsea
            // 
            this.gridLookUpEditMaNCCVietsea.Location = new System.Drawing.Point(61, 134);
            this.gridLookUpEditMaNCCVietsea.Name = "gridLookUpEditMaNCCVietsea";
            this.gridLookUpEditMaNCCVietsea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMaNCCVietsea.Properties.PopupView = this.gridLookUpEditMaNCCVietseaView;
            this.gridLookUpEditMaNCCVietsea.Properties.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.gridLookUpEditMaNCCVietsea_Properties_Closed);
            this.gridLookUpEditMaNCCVietsea.Properties.Popup += new System.EventHandler(this.gridLookUpEditMaNCCVietsea_Popup);
            this.gridLookUpEditMaNCCVietsea.Properties.EditValueChanged += new System.EventHandler(this.gridLookUpEditMaNCCVietsea_EditValueChanged);
            this.gridLookUpEditMaNCCVietsea.Properties.Click += new System.EventHandler(this.gridLookUpEditMaNCCVietsea_Properties_Click);
            this.gridLookUpEditMaNCCVietsea.Size = new System.Drawing.Size(192, 20);
            this.gridLookUpEditMaNCCVietsea.TabIndex = 5;
            // 
            // gridLookUpEditMaNCCVietseaView
            // 
            this.gridLookUpEditMaNCCVietseaView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEditMaNCCVietseaView.Name = "gridLookUpEditMaNCCVietseaView";
            this.gridLookUpEditMaNCCVietseaView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEditMaNCCVietseaView.OptionsView.ShowGroupPanel = false;
            // 
            // buttonChangeConnectionMSSQL
            // 
            this.buttonChangeConnectionMSSQL.Location = new System.Drawing.Point(6, 20);
            this.buttonChangeConnectionMSSQL.Name = "buttonChangeConnectionMSSQL";
            this.buttonChangeConnectionMSSQL.Size = new System.Drawing.Size(90, 23);
            this.buttonChangeConnectionMSSQL.TabIndex = 0;
            this.buttonChangeConnectionMSSQL.Text = "Thay đổi kết nối";
            this.buttonChangeConnectionMSSQL.Click += new System.EventHandler(this.buttonChangeConnectionMSSQL_Click);
            // 
            // dateEditThanhCongEndDate
            // 
            this.dateEditThanhCongEndDate.EditValue = null;
            this.dateEditThanhCongEndDate.Location = new System.Drawing.Point(134, 55);
            this.dateEditThanhCongEndDate.Name = "dateEditThanhCongEndDate";
            this.dateEditThanhCongEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditThanhCongEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditThanhCongEndDate.Properties.MaskSettings.Set("useAdvancingCaret", true);
            this.dateEditThanhCongEndDate.Properties.MaskSettings.Set("mask", "dd/MM/yyyy");
            this.dateEditThanhCongEndDate.Properties.UseMaskAsDisplayFormat = true;
            this.dateEditThanhCongEndDate.Size = new System.Drawing.Size(119, 20);
            this.dateEditThanhCongEndDate.TabIndex = 3;
            // 
            // dateEditThanhCongStartDate
            // 
            this.dateEditThanhCongStartDate.EditValue = null;
            this.dateEditThanhCongStartDate.Location = new System.Drawing.Point(6, 55);
            this.dateEditThanhCongStartDate.Name = "dateEditThanhCongStartDate";
            this.dateEditThanhCongStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditThanhCongStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditThanhCongStartDate.Properties.MaskSettings.Set("useAdvancingCaret", true);
            this.dateEditThanhCongStartDate.Properties.MaskSettings.Set("mask", "dd/MM/yyyy");
            this.dateEditThanhCongStartDate.Size = new System.Drawing.Size(119, 20);
            this.dateEditThanhCongStartDate.TabIndex = 2;
            // 
            // groupBoxVietsea
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.groupBoxVietsea, DevExpress.Utils.DefaultBoolean.Default);
            this.groupBoxVietsea.Controls.Add(this.textBoxMSTVietsea);
            this.groupBoxVietsea.Controls.Add(this.richTextBoxTenNCCVietsea);
            this.groupBoxVietsea.Controls.Add(this.labelOracleStatus);
            this.groupBoxVietsea.Controls.Add(this.buttonChangeConnectionOracle);
            this.groupBoxVietsea.Controls.Add(this.dateEditVietseaEndDate);
            this.groupBoxVietsea.Controls.Add(this.dateEditVietseaStartDate);
            this.groupBoxVietsea.Controls.Add(this.buttonSoChiTiet3311);
            this.groupBoxVietsea.Controls.Add(this.label2);
            this.groupBoxVietsea.Controls.Add(this.gridLookUpEditMaNCCVietsea);
            this.groupBoxVietsea.Controls.Add(this.buttonAutoImport);
            this.groupBoxVietsea.Location = new System.Drawing.Point(4, 340);
            this.groupBoxVietsea.Name = "groupBoxVietsea";
            this.groupBoxVietsea.Size = new System.Drawing.Size(260, 316);
            this.groupBoxVietsea.TabIndex = 2;
            this.groupBoxVietsea.TabStop = false;
            this.groupBoxVietsea.Text = "VIETSEA";
            // 
            // textBoxMSTVietsea
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.textBoxMSTVietsea, DevExpress.Utils.DefaultBoolean.Default);
            this.textBoxMSTVietsea.Location = new System.Drawing.Point(8, 160);
            this.textBoxMSTVietsea.Name = "textBoxMSTVietsea";
            this.textBoxMSTVietsea.Size = new System.Drawing.Size(245, 20);
            this.textBoxMSTVietsea.TabIndex = 9;
            // 
            // richTextBoxTenNCCVietsea
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.richTextBoxTenNCCVietsea, DevExpress.Utils.DefaultBoolean.Default);
            this.richTextBoxTenNCCVietsea.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxTenNCCVietsea.Location = new System.Drawing.Point(11, 186);
            this.richTextBoxTenNCCVietsea.Name = "richTextBoxTenNCCVietsea";
            this.richTextBoxTenNCCVietsea.Size = new System.Drawing.Size(239, 49);
            this.richTextBoxTenNCCVietsea.TabIndex = 8;
            this.richTextBoxTenNCCVietsea.Text = "";
            // 
            // labelOracleStatus
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.labelOracleStatus, DevExpress.Utils.DefaultBoolean.Default);
            this.labelOracleStatus.AutoSize = true;
            this.labelOracleStatus.ForeColor = System.Drawing.Color.Red;
            this.labelOracleStatus.Location = new System.Drawing.Point(103, 21);
            this.labelOracleStatus.Name = "labelOracleStatus";
            this.labelOracleStatus.Size = new System.Drawing.Size(67, 13);
            this.labelOracleStatus.TabIndex = 1;
            this.labelOracleStatus.Text = "Chưa kết nối";
            // 
            // buttonChangeConnectionOracle
            // 
            this.buttonChangeConnectionOracle.Location = new System.Drawing.Point(8, 16);
            this.buttonChangeConnectionOracle.Name = "buttonChangeConnectionOracle";
            this.buttonChangeConnectionOracle.Size = new System.Drawing.Size(88, 23);
            this.buttonChangeConnectionOracle.TabIndex = 0;
            this.buttonChangeConnectionOracle.Text = "Thay đổi kết nối";
            this.buttonChangeConnectionOracle.Click += new System.EventHandler(this.buttonChangeConnectionOracle_Click);
            // 
            // dateEditVietseaEndDate
            // 
            this.dateEditVietseaEndDate.EditValue = null;
            this.dateEditVietseaEndDate.Location = new System.Drawing.Point(134, 57);
            this.dateEditVietseaEndDate.Name = "dateEditVietseaEndDate";
            this.dateEditVietseaEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditVietseaEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditVietseaEndDate.Properties.MaskSettings.Set("useAdvancingCaret", true);
            this.dateEditVietseaEndDate.Properties.MaskSettings.Set("mask", "dd/MM/yyyy");
            this.dateEditVietseaEndDate.Properties.UseMaskAsDisplayFormat = true;
            this.dateEditVietseaEndDate.Size = new System.Drawing.Size(119, 20);
            this.dateEditVietseaEndDate.TabIndex = 3;
            // 
            // dateEditVietseaStartDate
            // 
            this.dateEditVietseaStartDate.EditValue = null;
            this.dateEditVietseaStartDate.Location = new System.Drawing.Point(6, 57);
            this.dateEditVietseaStartDate.Name = "dateEditVietseaStartDate";
            this.dateEditVietseaStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditVietseaStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditVietseaStartDate.Properties.MaskSettings.Set("useAdvancingCaret", true);
            this.dateEditVietseaStartDate.Properties.MaskSettings.Set("mask", "dd/MM/yyyy");
            this.dateEditVietseaStartDate.Properties.UseMaskAsDisplayFormat = true;
            this.dateEditVietseaStartDate.Size = new System.Drawing.Size(119, 20);
            this.dateEditVietseaStartDate.TabIndex = 2;
            // 
            // buttonSoChiTiet3311
            // 
            this.buttonSoChiTiet3311.Location = new System.Drawing.Point(6, 247);
            this.buttonSoChiTiet3311.Name = "buttonSoChiTiet3311";
            this.buttonSoChiTiet3311.Size = new System.Drawing.Size(75, 23);
            this.buttonSoChiTiet3311.TabIndex = 6;
            this.buttonSoChiTiet3311.Text = "Xem công nợ";
            this.buttonSoChiTiet3311.Click += new System.EventHandler(this.buttonSoChiTiet3311_Click);
            // 
            // groupBoxThanhCong
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.groupBoxThanhCong, DevExpress.Utils.DefaultBoolean.Default);
            this.groupBoxThanhCong.Controls.Add(this.groupBox5);
            this.groupBoxThanhCong.Controls.Add(this.buttonUpdateStatusCongNo331);
            this.groupBoxThanhCong.Controls.Add(this.richTextBoxStatusTimeCheck);
            this.groupBoxThanhCong.Controls.Add(this.buttonToMauDoiChieu);
            this.groupBoxThanhCong.Controls.Add(this.buttonGetDetailByOneAccount);
            this.groupBoxThanhCong.Controls.Add(this.buttonChangeConnectionMSSQL);
            this.groupBoxThanhCong.Controls.Add(this.textEditCustomerIDs);
            this.groupBoxThanhCong.Controls.Add(this.labelStatusMSSQLConnection);
            this.groupBoxThanhCong.Controls.Add(this.dateEditThanhCongStartDate);
            this.groupBoxThanhCong.Controls.Add(this.dateEditThanhCongEndDate);
            this.groupBoxThanhCong.Controls.Add(this.searchCustomers);
            this.groupBoxThanhCong.Location = new System.Drawing.Point(4, 0);
            this.groupBoxThanhCong.Name = "groupBoxThanhCong";
            this.groupBoxThanhCong.Size = new System.Drawing.Size(259, 317);
            this.groupBoxThanhCong.TabIndex = 0;
            this.groupBoxThanhCong.TabStop = false;
            this.groupBoxThanhCong.Text = "THÀNH CÔNG";
            // 
            // groupBox5
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.groupBox5, DevExpress.Utils.DefaultBoolean.Default);
            this.groupBox5.Controls.Add(this.richTextBoxNewStatusTheoDoiCongNo);
            this.groupBox5.Location = new System.Drawing.Point(6, 208);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(247, 72);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ghi chú";
            // 
            // richTextBoxNewStatusTheoDoiCongNo
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.richTextBoxNewStatusTheoDoiCongNo, DevExpress.Utils.DefaultBoolean.Default);
            this.richTextBoxNewStatusTheoDoiCongNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxNewStatusTheoDoiCongNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxNewStatusTheoDoiCongNo.Location = new System.Drawing.Point(3, 16);
            this.richTextBoxNewStatusTheoDoiCongNo.Name = "richTextBoxNewStatusTheoDoiCongNo";
            this.richTextBoxNewStatusTheoDoiCongNo.Size = new System.Drawing.Size(241, 53);
            this.richTextBoxNewStatusTheoDoiCongNo.TabIndex = 10;
            this.richTextBoxNewStatusTheoDoiCongNo.Text = "";
            // 
            // buttonUpdateStatusCongNo331
            // 
            this.buttonUpdateStatusCongNo331.Location = new System.Drawing.Point(148, 286);
            this.buttonUpdateStatusCongNo331.Name = "buttonUpdateStatusCongNo331";
            this.buttonUpdateStatusCongNo331.Size = new System.Drawing.Size(105, 23);
            this.buttonUpdateStatusCongNo331.TabIndex = 9;
            this.buttonUpdateStatusCongNo331.Text = "Cập nhật trạng thái";
            this.buttonUpdateStatusCongNo331.Click += new System.EventHandler(this.buttonUpdateStatusCongNo331_Click);
            // 
            // richTextBoxStatusTimeCheck
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.richTextBoxStatusTimeCheck, DevExpress.Utils.DefaultBoolean.Default);
            this.richTextBoxStatusTimeCheck.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxStatusTimeCheck.Location = new System.Drawing.Point(6, 180);
            this.richTextBoxStatusTimeCheck.Name = "richTextBoxStatusTimeCheck";
            this.richTextBoxStatusTimeCheck.Size = new System.Drawing.Size(247, 22);
            this.richTextBoxStatusTimeCheck.TabIndex = 8;
            this.richTextBoxStatusTimeCheck.Text = "";
            // 
            // buttonToMauDoiChieu
            // 
            this.buttonToMauDoiChieu.Location = new System.Drawing.Point(154, 133);
            this.buttonToMauDoiChieu.Name = "buttonToMauDoiChieu";
            this.buttonToMauDoiChieu.Size = new System.Drawing.Size(99, 23);
            this.buttonToMauDoiChieu.TabIndex = 7;
            this.buttonToMauDoiChieu.Text = "Tô màu đối chiếu";
            this.buttonToMauDoiChieu.Click += new System.EventHandler(this.buttonToMauDoiChieu_Click);
            // 
            // buttonGetDetailByOneAccount
            // 
            this.buttonGetDetailByOneAccount.BackgroundImage = global::ExcelAddIn1.Properties.Resources.icons8_ok_48px;
            this.buttonGetDetailByOneAccount.Location = new System.Drawing.Point(6, 133);
            this.buttonGetDetailByOneAccount.Name = "buttonGetDetailByOneAccount";
            this.buttonGetDetailByOneAccount.Size = new System.Drawing.Size(75, 23);
            this.buttonGetDetailByOneAccount.TabIndex = 6;
            this.buttonGetDetailByOneAccount.Text = "Xem công nợ";
            this.buttonGetDetailByOneAccount.Click += new System.EventHandler(this.buttonGetDetailByOneAccount_Click);
            // 
            // textEditCustomerIDs
            // 
            this.textEditCustomerIDs.Location = new System.Drawing.Point(6, 107);
            this.textEditCustomerIDs.Name = "textEditCustomerIDs";
            this.textEditCustomerIDs.Size = new System.Drawing.Size(247, 20);
            this.textEditCustomerIDs.TabIndex = 5;
            // 
            // defaultToolTipController1
            // 
            // 
            // 
            // 
            this.defaultToolTipController1.DefaultController.AutoPopDelay = 50000;
            this.defaultToolTipController1.DefaultController.InitialDelay = 1;
            this.defaultToolTipController1.DefaultController.KeepWhileHovered = true;
            // 
            // XtraFormNhapHang
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 668);
            this.Controls.Add(this.groupBoxThanhCong);
            this.Controls.Add(this.groupBoxVietsea);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "XtraFormNhapHang";
            this.Text = "Donate: NGUYEN MINH DUC 0451000415954 Vietcombank CN Thành Công";
            this.Load += new System.EventHandler(this.XtraFormNhapHang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageNhapHang.ResumeLayout(false);
            this.xtraTabPageNhapHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxPrintType.Properties)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxKhoNhap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxImpVat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxTranType.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTransactionDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTransantionDetails)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSumaryOfTransactions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSumaryOfTransactions)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumberOfPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInvoiceDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInvoiceDate.Properties)).EndInit();
            this.xtraTabPageSystem.ResumeLayout(false);
            this.xtraTabPageSystem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBangMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBangMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTranDateVietsea.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTranDateVietsea.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridInvoiceData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInvoiceData)).EndInit();
            this.xtraTabPageCongNo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSoChiTietTKVietsea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSoChiTietTKVietsea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetailByOneAccountThanhCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetailByOneAccountThanhCong)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTHCongNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTHCongNo)).EndInit();
            this.xtraTabPageXuatBill.ResumeLayout(false);
            this.xtraTabPageXuatBill.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRetailTranDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRetailTranDetail)).EndInit();
            this.xtraTabPageInvoice.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraTabPage6.ResumeLayout(false);
            this.xtraTabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAccountNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchCustomers.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchCustomersView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMaNCCVietsea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMaNCCVietseaView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditThanhCongEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditThanhCongEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditThanhCongStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditThanhCongStartDate.Properties)).EndInit();
            this.groupBoxVietsea.ResumeLayout(false);
            this.groupBoxVietsea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVietseaEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVietseaEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVietseaStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVietseaStartDate.Properties)).EndInit();
            this.groupBoxThanhCong.ResumeLayout(false);
            this.groupBoxThanhCong.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomerIDs.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageNhapHang;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageSystem;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxTranType;
        private DevExpress.XtraEditors.SimpleButton buttonClearTempGrid;
        private DevExpress.XtraEditors.SimpleButton buttonAutoImport;
        private DevExpress.XtraEditors.SimpleButton buttonSelectAllRecord;
        private DevExpress.XtraEditors.SimpleButton buttonCreateImportFile;
        private System.Windows.Forms.NumericUpDown numericUpDownNumberOfPrint;
        private CustomTextBox textBoxInvoiceNumber;
        private CustomTextBox textBoxSerialNumber;
        private MyUserControl.CustomDateEdit dateEditInvoiceDate;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMaNCCVietsea;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEditMaNCCVietseaView;
        private CustomTextBox textBoxDesiredAmount;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraGrid.GridControl gridTransactionDetails;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTransantionDetails;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraGrid.GridControl gridSumaryOfTransactions;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSumaryOfTransactions;
        private DevExpress.XtraEditors.SimpleButton buttonChangeConnectionMSSQL;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraGrid.GridControl gridTemp;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTemp;
        private DevExpress.XtraEditors.SimpleButton buttonSearchTransactions;
        private MyUserControl.CustomDateEdit dateEditThanhCongEndDate;
        private MyUserControl.CustomDateEdit dateEditThanhCongStartDate;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private MyUserControl.CustomDateEdit dateEditTranDateVietsea;
        private DevExpress.XtraEditors.LabelControl labelStatusMSSQLConnection;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCongNo;
        private DevExpress.XtraGrid.GridControl gridDetailByOneAccountThanhCong;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetailByOneAccountThanhCong;
        private DevExpress.XtraGrid.GridControl gridSoChiTietTKVietsea;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSoChiTietTKVietsea;
        private System.Windows.Forms.GroupBox groupBoxVietsea;
        private System.Windows.Forms.GroupBox groupBoxThanhCong;
        private DevExpress.XtraEditors.SimpleButton buttonGetDetailByOneAccount;
        private DevExpress.XtraEditors.SimpleButton buttonSoChiTiet3311;
        private MyUserControl.CustomDateEdit dateEditVietseaEndDate;
        private MyUserControl.CustomDateEdit dateEditVietseaStartDate;
        private DevExpress.XtraEditors.SimpleButton buttonCreatePayment;
        private DevExpress.XtraEditors.SearchLookUpEdit searchCustomers;
        private DevExpress.XtraGrid.Views.Grid.GridView searchCustomersView;
        private DevExpress.XtraEditors.TextEdit textEditCustomerIDs;
        private System.Windows.Forms.Label labelSerialNumberLength;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageInvoice;
        private DevExpress.XtraEditors.SimpleButton buttonDownloadInvoiceListFromHoadondientugdtgovvn;
        private DevExpress.XtraEditors.SimpleButton buttonCheckInvoiceTCT;
        private DevExpress.XtraEditors.SimpleButton buttonInitBangMa;
        private DevExpress.XtraEditors.SimpleButton buttonToMauDoiChieu;
        private DevExpress.XtraGrid.GridControl gridBangMa;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBangMa;
        private DevExpress.XtraEditors.SimpleButton buttonChangeConnectionOracle;
        private System.Windows.Forms.Label labelOracleStatus;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageXuatBill;
        private System.Windows.Forms.Label label7;
        private CustomTextBox textBoxRetailTranID;
        private DevExpress.XtraEditors.SimpleButton buttonGetRetailTran;
        private DevExpress.XtraGrid.GridControl gridRetailTranDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRetailTranDetail;
        private DevExpress.XtraEditors.SimpleButton buttonAutoCreateRetailTranVietsea;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxImpVat;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.SimpleButton buttonLoadHHBanRa;
        private DevExpress.XtraEditors.SimpleButton buttonCopyDataToXmlFile;
        private DevExpress.XtraEditors.SimpleButton buttonEditHTKKDataFileXml;
        private DevExpress.Utils.DefaultToolTipController defaultToolTipController1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private CustomTextBox textBoxTotalAmount;
        private CustomTextBox textBoxVat;
        private DevExpress.XtraEditors.SimpleButton buttonAutoImport2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxKhoNhap;
        private System.Windows.Forms.RichTextBox richTextBoxTenNCCVietsea;
        private DevExpress.XtraEditors.SimpleButton buttonCreateImportToPublicFolder;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private DevExpress.XtraEditors.SimpleButton buttonStartWorking;
        private DevExpress.XtraEditors.SimpleButton buttonStopWorking;
        private DevExpress.XtraEditors.SimpleButton buttonDownloadihoadonPDF;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.SimpleButton buttonLoadInvoiceDataFromExcelSheet;
        private DevExpress.XtraGrid.GridControl gridInvoiceData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInvoiceData;
        private System.Windows.Forms.TextBox textBoxMSTVietsea;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxPrintType;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton buttonUpdateStatusCongNo331;
        private System.Windows.Forms.RichTextBox richTextBoxStatusTimeCheck;
        private System.Windows.Forms.RichTextBox richTextBoxNewStatusTheoDoiCongNo;
        private System.Windows.Forms.ProgressBar progressBar1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton buttonNXT4COT;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraGrid.GridControl gridControlTHCongNo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTHCongNo;
        private DevExpress.XtraEditors.SimpleButton buttonLoadTHCongNo;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button buttonStockinfo;
        private DevExpress.XtraEditors.SimpleButton buttonGetInvoiceInfoCopyToBangKeGN;
        private DevExpress.XtraEditors.SimpleButton buttonExportExcelPL43;
        private DevExpress.XtraEditors.DropDownButton dropDownButton1;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1_HachToanPhieuChi;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.DropDownButton dropDownButton2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private System.Windows.Forms.Button buttonTongHop131;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEditAccountNumber;
    }
}