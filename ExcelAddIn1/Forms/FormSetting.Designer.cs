﻿
namespace ExcelAddIn1.Forms
{
    partial class FormSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvServerList = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ButtonBrowserFolderImport = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxSaveImport = new System.Windows.Forms.TextBox();
            this.textBoxOpenFileDialog_TargetTHANHCONG = new ExcelAddIn1.MyUserControl.TextBoxOpenFileDialog();
            this.textBoxOpenFileDialog_TargetVIETSEA = new ExcelAddIn1.MyUserControl.TextBoxOpenFileDialog();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxTransferFeeDifferentSystemVCB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTransferFeeSameSystemVCB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dtpStartDateSaleReturn = new ExcelAddIn1.CustomDateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxEFYmst = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxEFYpassword = new System.Windows.Forms.TextBox();
            this.textBoxEFYusername = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxPasswordVIETSEA = new System.Windows.Forms.TextBox();
            this.textBoxUserNameVIETSEA = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ButtonSaveSettings = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServerList)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(553, 296);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvServerList);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(545, 270);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Server";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvServerList
            // 
            this.dgvServerList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServerList.Location = new System.Drawing.Point(6, 6);
            this.dgvServerList.Name = "dgvServerList";
            this.dgvServerList.Size = new System.Drawing.Size(240, 240);
            this.dgvServerList.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.ButtonBrowserFolderImport);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.TextBoxSaveImport);
            this.tabPage2.Controls.Add(this.textBoxOpenFileDialog_TargetTHANHCONG);
            this.tabPage2.Controls.Add(this.textBoxOpenFileDialog_TargetVIETSEA);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(545, 270);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Đường dẫn";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(339, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Đường dẫn phần mềm Thành Công (ClickOnce: đường dẫn appref-ms)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Đường dẫn phần mềm VIETSEA";
            // 
            // ButtonBrowserFolderImport
            // 
            this.ButtonBrowserFolderImport.Location = new System.Drawing.Point(345, 43);
            this.ButtonBrowserFolderImport.Name = "ButtonBrowserFolderImport";
            this.ButtonBrowserFolderImport.Size = new System.Drawing.Size(75, 23);
            this.ButtonBrowserFolderImport.TabIndex = 2;
            this.ButtonBrowserFolderImport.Text = "Chọn folder";
            this.ButtonBrowserFolderImport.UseVisualStyleBackColor = true;
            this.ButtonBrowserFolderImport.Click += new System.EventHandler(this.ButtonBrowserFolderImport_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Đường dẫn lưu file import nhập hàng";
            // 
            // TextBoxSaveImport
            // 
            this.TextBoxSaveImport.Location = new System.Drawing.Point(9, 45);
            this.TextBoxSaveImport.Name = "TextBoxSaveImport";
            this.TextBoxSaveImport.Size = new System.Drawing.Size(330, 20);
            this.TextBoxSaveImport.TabIndex = 0;
            // 
            // textBoxOpenFileDialog_TargetTHANHCONG
            // 
            this.textBoxOpenFileDialog_TargetTHANHCONG.Location = new System.Drawing.Point(9, 154);
            this.textBoxOpenFileDialog_TargetTHANHCONG.Name = "textBoxOpenFileDialog_TargetTHANHCONG";
            this.textBoxOpenFileDialog_TargetTHANHCONG.Size = new System.Drawing.Size(411, 20);
            this.textBoxOpenFileDialog_TargetTHANHCONG.TabIndex = 5;
            // 
            // textBoxOpenFileDialog_TargetVIETSEA
            // 
            this.textBoxOpenFileDialog_TargetVIETSEA.Location = new System.Drawing.Point(9, 99);
            this.textBoxOpenFileDialog_TargetVIETSEA.Name = "textBoxOpenFileDialog_TargetVIETSEA";
            this.textBoxOpenFileDialog_TargetVIETSEA.Size = new System.Drawing.Size(411, 20);
            this.textBoxOpenFileDialog_TargetVIETSEA.TabIndex = 3;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(545, 270);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Ngân hàng";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxTransferFeeDifferentSystemVCB);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxTransferFeeSameSystemVCB);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 82);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vietcombank";
            // 
            // textBoxTransferFeeDifferentSystemVCB
            // 
            this.textBoxTransferFeeDifferentSystemVCB.Location = new System.Drawing.Point(259, 44);
            this.textBoxTransferFeeDifferentSystemVCB.Name = "textBoxTransferFeeDifferentSystemVCB";
            this.textBoxTransferFeeDifferentSystemVCB.Size = new System.Drawing.Size(149, 20);
            this.textBoxTransferFeeDifferentSystemVCB.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(235, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Phí chuyển tiền cùng hệ thống VCB (chưa VAT)";
            // 
            // textBoxTransferFeeSameSystemVCB
            // 
            this.textBoxTransferFeeSameSystemVCB.Location = new System.Drawing.Point(259, 22);
            this.textBoxTransferFeeSameSystemVCB.Name = "textBoxTransferFeeSameSystemVCB";
            this.textBoxTransferFeeSameSystemVCB.Size = new System.Drawing.Size(149, 20);
            this.textBoxTransferFeeSameSystemVCB.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(235, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Phí chuyển tiền khác hệ thống VCB (chưa VAT)";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dtpStartDateSaleReturn);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(545, 270);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Xuất trả";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dtpStartDateSaleReturn
            // 
            this.dtpStartDateSaleReturn.CustomFormat = "dd/MM/yyyy";
            this.dtpStartDateSaleReturn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDateSaleReturn.Location = new System.Drawing.Point(9, 42);
            this.dtpStartDateSaleReturn.Name = "dtpStartDateSaleReturn";
            this.dtpStartDateSaleReturn.Size = new System.Drawing.Size(107, 20);
            this.dtpStartDateSaleReturn.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Ngày bắt đầu xuất trả";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox4);
            this.tabPage5.Controls.Add(this.groupBox3);
            this.tabPage5.Controls.Add(this.groupBox2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(545, 270);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Đăng nhập";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(230, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(190, 100);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxEFYmst);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.textBoxEFYpassword);
            this.groupBox3.Controls.Add(this.textBoxEFYusername);
            this.groupBox3.Location = new System.Drawing.Point(6, 81);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(209, 104);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "EFY";
            // 
            // textBoxEFYmst
            // 
            this.textBoxEFYmst.Location = new System.Drawing.Point(93, 15);
            this.textBoxEFYmst.Name = "textBoxEFYmst";
            this.textBoxEFYmst.Size = new System.Drawing.Size(100, 20);
            this.textBoxEFYmst.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Mã số thuế";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Mật khẩu";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Tên đăng nhập";
            // 
            // textBoxEFYpassword
            // 
            this.textBoxEFYpassword.Location = new System.Drawing.Point(93, 69);
            this.textBoxEFYpassword.Name = "textBoxEFYpassword";
            this.textBoxEFYpassword.PasswordChar = '*';
            this.textBoxEFYpassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxEFYpassword.TabIndex = 1;
            // 
            // textBoxEFYusername
            // 
            this.textBoxEFYusername.Location = new System.Drawing.Point(93, 42);
            this.textBoxEFYusername.Name = "textBoxEFYusername";
            this.textBoxEFYusername.Size = new System.Drawing.Size(100, 20);
            this.textBoxEFYusername.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textBoxPasswordVIETSEA);
            this.groupBox2.Controls.Add(this.textBoxUserNameVIETSEA);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(209, 69);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vietsea";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Tên đăng nhập";
            // 
            // textBoxPasswordVIETSEA
            // 
            this.textBoxPasswordVIETSEA.Location = new System.Drawing.Point(93, 37);
            this.textBoxPasswordVIETSEA.Name = "textBoxPasswordVIETSEA";
            this.textBoxPasswordVIETSEA.PasswordChar = '*';
            this.textBoxPasswordVIETSEA.Size = new System.Drawing.Size(100, 20);
            this.textBoxPasswordVIETSEA.TabIndex = 3;
            // 
            // textBoxUserNameVIETSEA
            // 
            this.textBoxUserNameVIETSEA.Location = new System.Drawing.Point(93, 13);
            this.textBoxUserNameVIETSEA.Name = "textBoxUserNameVIETSEA";
            this.textBoxUserNameVIETSEA.Size = new System.Drawing.Size(100, 20);
            this.textBoxUserNameVIETSEA.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Mật khẩu";
            // 
            // ButtonSaveSettings
            // 
            this.ButtonSaveSettings.Location = new System.Drawing.Point(486, 314);
            this.ButtonSaveSettings.Name = "ButtonSaveSettings";
            this.ButtonSaveSettings.Size = new System.Drawing.Size(75, 23);
            this.ButtonSaveSettings.TabIndex = 1;
            this.ButtonSaveSettings.Text = "Lưu";
            this.ButtonSaveSettings.UseVisualStyleBackColor = true;
            this.ButtonSaveSettings.Click += new System.EventHandler(this.ButtonSaveSettings_Click);
            // 
            // FormSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 345);
            this.Controls.Add(this.ButtonSaveSettings);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSetting";
            this.Text = "Cài đặt";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvServerList)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvServerList;
        private System.Windows.Forms.Button ButtonSaveSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxSaveImport;
        private System.Windows.Forms.Button ButtonBrowserFolderImport;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxTransferFeeDifferentSystemVCB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxTransferFeeSameSystemVCB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage4;
        private CustomDateTimePicker dtpStartDateSaleReturn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private MyUserControl.TextBoxOpenFileDialog textBoxOpenFileDialog_TargetVIETSEA;
        private System.Windows.Forms.Label label6;
        private MyUserControl.TextBoxOpenFileDialog textBoxOpenFileDialog_TargetTHANHCONG;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox textBoxPasswordVIETSEA;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxUserNameVIETSEA;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxEFYpassword;
        private System.Windows.Forms.TextBox textBoxEFYusername;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxEFYmst;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}