﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelAddIn1.Forms
{
    public partial class FormConnectOracle : Form
    {
        static string _connectionString;

        public FormConnectOracle()
        {
            InitializeComponent();
            Init();
        }


        void Init()
        {
            StartPosition = FormStartPosition.Manual;
            textBoxIP.Text = MyClass.AddinConfig.Oralce_IP;
            textBoxPort.Text = MyClass.AddinConfig.Oralce_Port;
            buttonConnect.Click += ButtonConnect_Click;
        }

        private async void ButtonConnect_Click(object sender, EventArgs e)
        {
            Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder sb = new Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder();
            string ip = textBoxIP.Text;
            string port = textBoxPort.Text;
            sb.DataSource = $"{ip}:{port}/TBNETERP";
            sb.UserID = "TBNETERP";
            sb.Password = "TBNETERP";

            _connectionString = sb.ConnectionString;

            if (await ADO.TestOracleConnection(_connectionString))
            {
                MessageBox.Show("Kết nối thành công", "Thông báo");
                MyClass.AddinConfig.Oralce_IP = ip;
                MyClass.AddinConfig.Oralce_Port = port;
                Dispose();
            }
            else
            {
                MessageBox.Show("Kết nối không thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public string GetConnectionString()
        {
            FormConnectOracle formConnectOracle = new FormConnectOracle();
            formConnectOracle.Location = System.Windows.Forms.Cursor.Position;
            formConnectOracle.ShowDialog();
            return _connectionString;
        }


    }
}
