﻿using ExcelAddIn1.MyClass;
using System;
using System.Data;
using System.Windows.Forms;

namespace ExcelAddIn1.Forms
{
    public partial class FormSetting : Form
    {
        public FormSetting()
        {
            InitializeComponent();

            //load server list
            dgvServerList.DataSource = PublicVar.DT_ServerList();

            //load save import folder location
            TextBoxSaveImport.Text = AddinConfig.SaveLocationImportPurchaseInvoiceVIETSEA;

            //load transfer free vietcombank
            textBoxTransferFeeSameSystemVCB.Text = AddinConfig.TransferFeeSameBankSystem;
            textBoxTransferFeeDifferentSystemVCB.Text = AddinConfig.TransferFeeDifferentBankSystem;

            //load start date sale return
            dtpStartDateSaleReturn.Value = System.DateTime.Parse(AddinConfig.StartDateSaleReturn);

            textBoxOpenFileDialog_TargetVIETSEA.Text = AddinConfig.TargetVIETSEA;
            textBoxOpenFileDialog_TargetTHANHCONG.Text = AddinConfig.TargetTHANHCONG;

            textBoxUserNameVIETSEA.Text = AddinConfig.UsernameVIETSEA;
            textBoxPasswordVIETSEA.Text = AddinConfig.PasswordVIETSEA;

            textBoxEFYmst.Text = AddinConfig.EFYmst;
            textBoxEFYusername.Text = AddinConfig.EFYusername;
            textBoxEFYpassword.Text = AddinConfig.EFYpassword;

            this.CenterToScreen();
        }

        private void ButtonSaveSettings_Click(object sender, EventArgs e)
        {
            //tab server
            DataTable dataTable = (DataTable)dgvServerList.DataSource;
            dataTable.WriteXml(PublicVar.xmlFileServerList, XmlWriteMode.WriteSchema);

            //tab save folder
            AddinConfig.SaveLocationImportPurchaseInvoiceVIETSEA = TextBoxSaveImport.Text;

            //tab Bank
            AddinConfig.TransferFeeSameBankSystem = textBoxTransferFeeSameSystemVCB.Text;
            AddinConfig.TransferFeeDifferentBankSystem = textBoxTransferFeeDifferentSystemVCB.Text;

            //tab sale return
            AddinConfig.StartDateSaleReturn = dtpStartDateSaleReturn.Value.ToString();

            AddinConfig.TargetVIETSEA = textBoxOpenFileDialog_TargetVIETSEA.Text;
            AddinConfig.TargetTHANHCONG = textBoxOpenFileDialog_TargetTHANHCONG.Text;

            AddinConfig.UsernameVIETSEA = textBoxUserNameVIETSEA.Text;
            AddinConfig.PasswordVIETSEA = textBoxPasswordVIETSEA.Text;

            AddinConfig.EFYmst = textBoxEFYmst.Text;
            AddinConfig.EFYusername = textBoxEFYusername.Text;
            AddinConfig.EFYpassword = textBoxEFYpassword.Text;

            MessageBox.Show("Lưu cài đặt thành công!", PublicVar.title);
        }

        private void ButtonBrowserFolderImport_Click(object sender, EventArgs e)
        {
            string path = Module1.FolderBrowser("Chọn folder lưu file import nhập hàng");
            if (!string.IsNullOrEmpty(path))
            {
                TextBoxSaveImport.Text = path;
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
