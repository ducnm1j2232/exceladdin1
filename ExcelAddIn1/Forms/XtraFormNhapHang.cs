﻿using ExcelAddIn1.AutoTest.WinAppDriver.Vietsea;
using ExcelAddIn1.MyClass;
using ExcelAddIn1.MyClass.Extensions;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.Forms
{
    public partial class XtraFormNhapHang : DevExpress.XtraEditors.XtraForm
    {
        #region static var
        //static XtraFormNhapHang _xtraFormNhapHang = new XtraFormNhapHang();
        static string _connectionStringSQL = string.Empty;
        static string _connectionStringOracle = string.Empty;

        static System.Data.DataTable _bangma = new System.Data.DataTable();
        static System.Data.DataTable _dtTransDetail = null;
        static System.Data.DataTable _dtTemp = null;
        static DataTable _dataProductInfo = null;

        System.Collections.Generic.List<string> _getCustomerIDs;
        System.Collections.Generic.List<UserClass.DevExpressGridCellinfo> listCellVietsea;
        System.Collections.Generic.List<UserClass.DevExpressGridCellinfo> listCellThanhCong;

        static bool _allowCellValueChange = true;
        bool isColorSet = false;

        string _trancode;
        string _impExpID;
        string _priceVat;
        string _totalVat;

        private static string _AccessFileOneDriveDatabase
        {
            get
            {
                return ExcelAddIn1.Config.Manager.GetValue(Config.ID.OneDriveDatabaseAccessFile);
            }
        }
        #endregion

        public XtraFormNhapHang()
        {
            InitializeComponent();

            InitComponentValues_1();

        }

        private async void XtraFormNhapHang_Load(object sender, EventArgs e)
        {
            Task task1 = InitBangMa();
            Task task2 = InitComponentValues_2Async();

            await Task.WhenAll(task1, task2);
        }

        void InitComponentValues_1()
        {
            dateEditThanhCongStartDate.EditValue = System.DateTime.Today;
            dateEditThanhCongEndDate.EditValue = System.DateTime.Today;
            dateEditInvoiceDate.EditValue = System.DateTime.Today;
            dateEditVietseaStartDate.EditValue = System.DateTime.Today;
            dateEditVietseaEndDate.EditValue = System.DateTime.Today;

            comboBoxTranType.Properties.Items.AddRange(new string[] { "Nhập hàng", "Xuất trả" });
            comboBoxTranType.SelectedIndex = 0;

            numericUpDownNumberOfPrint.Value = 1;
            labelStatusMSSQLConnection.Text = "Chưa kết nối";
            labelStatusMSSQLConnection.ForeColor = System.Drawing.Color.Red;
            gridLookUpEditMaNCCVietsea.Enabled = false;

            textBoxDesiredAmount.SelectAllOnClick = true;
            textBoxVat.SelectAllOnClick = true;
            textBoxTotalAmount.SelectAllOnClick = true;

            comboBoxPrintType.Properties.Items.AddRange(new string[] { Public.PrintType.Print, Public.PrintType.PDF, Public.PrintType.None });
            comboBoxPrintType.SelectedIndex = 0;

            Binding binding = new Binding("Text", textBoxInvoiceNumber, "Text.Length");
            labelSerialNumberLength.DataBindings.Add(binding);


        }

        async Task InitComponentValues_2Async()
        {
            #region main

            //get trandate vietsea
            Task<DateTime> GetTrandateVietseaFromGoogleSheetTask = GetTrandateVietseaFromGoogleSheet();

            _connectionStringSQL = ThanhCong.Database.SQLServer.DefaultConnectionStringTONGKHO;
            Task<bool> TestSQLConnectionTask = ADO.TestSQLConnection(_connectionStringSQL);

            _connectionStringOracle = VIETSEA.OracleServer.GetConnectionString;
            Task<bool> TestOracleConnectionTask = ADO.TestOracleConnection(_connectionStringOracle);

            dateEditTranDateVietsea.SafeInvoke(async () =>
            {
                dateEditTranDateVietsea.EditValue = await GetTrandateVietseaFromGoogleSheetTask;
            });


            bool isSQLConnected = await TestSQLConnectionTask;
            if (isSQLConnected)
            {
                await LoadCustomerData(_connectionStringSQL);
                System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder(_connectionStringSQL);
                labelStatusMSSQLConnection.SafeInvoke(() =>
                {
                    labelStatusMSSQLConnection.Text = $"{builder.DataSource} : {builder.InitialCatalog}";
                    labelStatusMSSQLConnection.ForeColor = System.Drawing.Color.Green;
                });
            }


            bool isOracleConnected = await TestOracleConnectionTask;
            if (isOracleConnected)
            {
                Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder sb = new Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder(_connectionStringOracle);
                await InitMaKhachHangVietsea();

                comboBoxKhoNhap.SafeInvoke(async () =>
                {
                    DataTable dt = await VIETSEA.OracleServer.GetDMKHO(_connectionStringOracle).ConfigureAwait(false);
                    comboBoxKhoNhap.Properties.Items.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        comboBoxKhoNhap.Properties.Items.Add(row["MAKHO"].ToString());
                    }
                    comboBoxKhoNhap.SelectedIndex = -1;
                });


                labelOracleStatus.SafeInvoke(() =>
                {
                    labelOracleStatus.Text = sb.DataSource;
                    labelOracleStatus.ForeColor = System.Drawing.Color.Green;
                });
            }


            gridViewRetailTranDetail.SetMultiSelectCheckbox();

            comboBoxImpVat.Properties.Items.Clear();
            comboBoxImpVat.Properties.Items.AddRange(Public.VatRate.VatList.ToArray<object>());
            comboBoxImpVat.SelectedIndex = 0;
            #endregion


        }

        private async void ButtonAutoCreateRetailTranVietsea_Click(object sender, EventArgs e)
        {
            List<UserClass.Good> goodList = new List<UserClass.Good>();
            int[] checkedRows = gridViewRetailTranDetail.GetSelectedRows();

            if (checkedRows.Length == 0)
            {
                MessageBox.Show(this, "Chưa chọn mã hàng nào");
                return;
            }

            foreach (int rowHandle in checkedRows)
            {
                UserClass.Good good = new UserClass.Good();

                string slTon = gridViewRetailTranDetail.GetRowCellValue(rowHandle, "SL tồn").ToString();
                if (string.IsNullOrEmpty(slTon)) continue;//skip

                good.GoodID = gridViewRetailTranDetail.GetRowCellValue(rowHandle, "Mã vietsea").ToString();
                good.Stock = gridViewRetailTranDetail.GetRowCellValue(rowHandle, "Kho").ToString();
                good.Quantity = Convert.ToDouble(gridViewRetailTranDetail.GetRowCellValue(rowHandle, "Số lượng"));
                good.Price = Convert.ToDouble(gridViewRetailTranDetail.GetRowCellValue(rowHandle, "Đơn giá"));

                goodList.Add(good);
            }

            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();

                await Task.Run(() =>
                {
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
                    WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);
                    WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanBill2(goodList);
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();

                    Notification.Toast.Show("Tạo phiếu thành công");
                }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);

            }
            catch (System.OperationCanceledException)
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
                Notification.Toast.Show("Đã hủy");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
            }

        }


        private void RepositoryItemComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var combobox = sender as DevExpress.XtraEditors.ComboBoxEdit;
            if (combobox.SelectedIndex != -1)
            {
                double rateValue = System.Convert.ToDouble(combobox.Properties.Items[combobox.SelectedIndex]);
                int rowHandle = gridViewRetailTranDetail.FocusedRowHandle;
                double quantity = System.Convert.ToDouble(gridViewRetailTranDetail.GetRowCellValue(rowHandle, "Số lượng"));
                double priceVAT = System.Convert.ToDouble(gridViewRetailTranDetail.GetRowCellValue(rowHandle, "Thành tiền VAT"));
                double newAmount = priceVAT / (1 + rateValue / 100);
                double newPrice = newAmount / quantity;
                gridViewRetailTranDetail.SetRowCellValue(rowHandle, "Thành tiền", newAmount);
                gridViewRetailTranDetail.SetRowCellValue(rowHandle, "Đơn giá", newPrice);
            }
        }

        private void RepoSearchLookUpEdit_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit = (DevExpress.XtraEditors.SearchLookUpEdit)sender;
            DevExpress.XtraGrid.Views.Grid.GridView gridView = searchLookUpEdit.Properties.View;
            object slTon = searchLookUpEdit.GetColumnValueFromLookupEdit(gridView.FocusedRowHandle, "TONCUOIKYSL");
            gridViewRetailTranDetail.SetRowCellValue(gridViewRetailTranDetail.FocusedRowHandle, "SL tồn", slTon);

            object kho = searchLookUpEdit.GetColumnValueFromLookupEdit(gridView.FocusedRowHandle, "MAKHOHANG");
            gridViewRetailTranDetail.SetRowCellValue(gridViewRetailTranDetail.FocusedRowHandle, "Kho", kho);

            gridViewRetailTranDetail.RowCellStyle += GridViewRetailTranDetail_RowCellStyle;
            gridViewRetailTranDetail.FocusedRowHandle += 1;
            gridViewRetailTranDetail.FocusedRowHandle -= 1;

        }

        private void GridViewRetailTranDetail_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gridView = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            if (gridView == null) return;

            if (e.Column.FieldName == "SL tồn")
            {
                object cellVaue = DevExpressExtensions.GetControlValue(gridView.GetRowCellValue(e.RowHandle, e.Column.FieldName));
                if (string.IsNullOrEmpty(cellVaue.ToString()))
                {
                    cellVaue = 0;
                }
                double slTon = System.Convert.ToDouble(cellVaue);
                double slBan = System.Convert.ToDouble(gridView.GetRowCellValue(e.RowHandle, "Số lượng"));

                e.Appearance.BackColor = System.Drawing.Color.Transparent;
                if (slTon < slBan)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Orange;
                }
            }
        }

        private void GridViewRetailTranDetail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //DevExpress.XtraGrid.Views.Grid.GridView gridView = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            //if (gridView == null) return;

            //if (e.Column.FieldName == "Mã vietsea")
            //{
            //    string mavtu = gridView.GetRowCellValue(e.RowHandle, e.Column.FieldName).ToString();
            //    DataRow[] dataRow = _dataProductInfo.Select($"MAVTU = '{mavtu}'", "TONCUOIKYSL DESC");
            //    if (dataRow.Length > 0)
            //    {
            //        string slTon = dataRow[0]["TONCUOIKYSL"].ToString();
            //        string kho = dataRow[0]["MAKHOHANG"].ToString();
            //        gridViewRetailTranDetail.SetRowCellValue(e.RowHandle, "SL tồn", slTon);
            //        gridViewRetailTranDetail.SetRowCellValue(e.RowHandle, "Kho", kho);
            //    }
            //}

            //if (e.Column.FieldName == "SL tồn")
            //{
            //    object cellVaue = DevExpressExtensions.GetControlValue(e.Value);
            //    if (string.IsNullOrEmpty(cellVaue.ToString()))
            //    {
            //        cellVaue = 0;
            //    }
            //    double slTon = System.Convert.ToDouble(cellVaue);
            //    double slBan = System.Convert.ToDouble(gridView.GetRowCellValue(e.RowHandle, "Số lượng"));

            //    gridView.SetCellColor(System.Drawing.Color.Transparent, e.RowHandle, e.Column.FieldName);
            //    if (slTon < slBan)
            //    {
            //        gridView.SetCellColor(System.Drawing.Color.Orange, e.RowHandle, e.Column.FieldName);
            //    }
            //}


        }

        private void RepoSearchLookUpEdit_Popup(object sender, EventArgs e)
        {
            var edit = sender as DevExpress.XtraEditors.SearchLookUpEdit;
            var popupForm = edit.GetPopupEditForm();
            popupForm.Left = gridRetailTranDetail.PointToScreen(System.Drawing.Point.Empty).X;
            popupForm.Width = gridRetailTranDetail.Width;
            DevExpress.XtraGrid.Views.Grid.GridView view = popupForm.OwnerEdit.Properties.View;
            view.SetBestFitColumns(0, "MAKHOHANG", "MAVTU", "TONCUOIKYSL", "GIAVON");
            view.Columns["GIAVON"].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
        }


        public string VlookupMaVietsea(DataTable bangma, string goodID)
        {
            System.Data.DataRow[] dataRows = _bangma.Select($"ma = '{goodID}'");
            return dataRows.Length > 0 ? dataRows[0][Mvvm.Models.TableColumns.GridTransDetail.VietseaCode].ToString() : "#N/A";
        }

        private void GridViewDetailByOneAccountThanhCong_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            try
            {
                if (!isColorSet)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Transparent;
                }
                else
                {
                    if (listCellThanhCong == null)
                    {
                        return;
                    }

                    for (int i = 0; i < listCellThanhCong.Count; i++)
                    {
                        UserClass.DevExpressGridCellinfo cell = listCellThanhCong[i];
                        if (e.RowHandle == cell.RowHandle && e.Column.FieldName == cell.ColumnName)
                        {
                            e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                            //listCellThanhCong.Remove(cell);
                            //break;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void GridViewSoChiTietTKVietsea_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            try
            {
                if (!isColorSet)
                {
                    e.Appearance.BackColor = System.Drawing.Color.Transparent;
                }
                else
                {
                    if (listCellVietsea == null)
                    {
                        return;
                    }

                    for (int i = 0; i < listCellVietsea.Count; i++)
                    {
                        UserClass.DevExpressGridCellinfo cell = listCellVietsea[i];
                        if (e.RowHandle == cell.RowHandle && e.Column.FieldName == cell.ColumnName)
                        {
                            e.Appearance.BackColor = System.Drawing.Color.LightGreen;
                            //listCellVietsea.Remove(cell);
                            //break;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void TextBoxInvoiceNumber_TextChanged(object sender, System.EventArgs e)
        {
            //labelSerialNumberLength.Text = $"Độ dài: {textBoxInvoiceNumber.Text.Length}";
            if (textBoxInvoiceNumber.Text.Length != 8)
            {
                labelSerialNumberLength.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                labelSerialNumberLength.ForeColor = System.Drawing.Color.Green;
            }
        }

        public void HideForm()
        {
            this.Hide();
        }

        void SetParam()
        {
            _trancode = PublicVar.DT_ImportOrExportTranCode().Rows[comboBoxTranType.SelectedIndex]["TranCode"].ToString();
            _impExpID = PublicVar.DT_ImportOrExportTranCode().Rows[comboBoxTranType.SelectedIndex]["ExportIDImportID"].ToString();
            _priceVat = PublicVar.DT_ImportOrExportTranCode().Rows[comboBoxTranType.SelectedIndex]["Price"].ToString();
            _totalVat = PublicVar.DT_ImportOrExportTranCode().Rows[comboBoxTranType.SelectedIndex]["Total"].ToString();
        }

        async Task<DateTime> GetTrandateVietseaFromGoogleSheet()
        {
            var data = await GoogleApis.SheetsAPI.GetValuesAsync(GoogleApis.SheetsAPI.spreadSheetID, "CloudParams!A1:B").ConfigureAwait(false);
            data.ToDataTale();
            object result = data.ToDataTale().Select("Name = 'trandateVietsea'").CopyToDataTable().Rows[0]["Value"];

            System.IFormatProvider formatProvider = new System.Globalization.CultureInfo("vi-VN", true);
            string formatString = "dd/MM/yyyy";
            System.DateTime dateTime = System.DateTime.ParseExact(result.ToString(), formatString, formatProvider);
            return dateTime;
        }

        async Task InitBangMa()
        {
            var dt = await GoogleApis.SheetsAPI.GetValuesAsync(GoogleApis.SheetsAPI.spreadSheetID, $"{PublicVar.BangMaSheetName}!A1:C").ConfigureAwait(false);
            _bangma = dt.ToDataTale();
            gridBangMa.SafeBeginInvoke(() =>
              {
                  gridBangMa.DataSource = _bangma;
                  gridViewBangMa.OptionsView.ShowAutoFilterRow = true;
                  gridViewBangMa.OptionsView.ShowGroupPanel = false;
                  gridViewBangMa.BestFitColumns();
              });
        }

        private async Task InitMaKhachHangVietsea()
        {
            //try
            //{

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            #region sb
            sb.AppendLine($"select");
            sb.AppendLine($"makhachhang,");
            sb.AppendLine($"masothue,");
            sb.AppendLine($"diengiai,");
            sb.AppendLine($"tentochu");
            sb.AppendLine($"from");
            sb.AppendLine($"dmkhachhang");
            sb.AppendLine($"where");
            sb.AppendLine($"manhomkhachang like '%NCC%'");
            #endregion

            string query = sb.ToString();
            System.Data.DataTable dt = await ADO.GetDataTableFromOracleServerAsync(_connectionStringOracle, query).ConfigureAwait(false);
            if (dt.HasData())
            {
                gridLookUpEditMaNCCVietsea.SafeBeginInvoke(() =>
                {
                    gridLookUpEditMaNCCVietsea.Enabled = false;
                    gridLookUpEditMaNCCVietsea.Properties.SearchMode = DevExpress.XtraEditors.Repository.GridLookUpSearchMode.AutoSearch;
                    gridLookUpEditMaNCCVietsea.Enabled = true;
                    gridLookUpEditMaNCCVietsea.Properties.DataSource = dt;
                    gridLookUpEditMaNCCVietsea.Text = "Nhập tên NCC";
                    gridLookUpEditMaNCCVietsea.Properties.DisplayMember = "MAKHACHHANG";
                    gridLookUpEditMaNCCVietsea.Properties.ValueMember = "MAKHACHHANG";

                    gridLookUpEditMaNCCVietsea.Properties.PopulateViewColumns();
                    //gridLookUpEditMaNCCVietseaView.Columns["MAKHACHHANG"].Width = 50;
                    gridLookUpEditMaNCCVietseaView.SetBestFitColumns(0, "MAKHACHHANG", "MASOTHUE", "DIENGIAI");
                    gridLookUpEditMaNCCVietsea.Enabled = true;
                });
            }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}

        }

        async Task LoadCustomerData(string connectionString)
        {
            string q = "select CustomerID,Taxcode, CustomerName from Customers";
            DataTable dt = await ADO.GetDataTableFromSQLServerAsync(connectionString, q).ConfigureAwait(false);
            if (dt != null && dt.Rows.Count > 0)
            {
                searchCustomers.SafeInvoke(() =>
                {
                    searchCustomers.Properties.DataSource = dt;
                    searchCustomers.Properties.DisplayMember = "CustomerID";
                    searchCustomers.Properties.ValueMember = "CustomerID";
                });
            }
        }

        async Task<System.Data.DataTable> GetSumaryOfTransactions()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            #region sb
            sb.AppendLine($"select");
            sb.AppendLine($"RIGHT(Transactions.TransDate,2)+'/'+ Substring(Transactions.TransDate,5,2)+'/'+ LEFT(Transactions.TransDate,4) as \"Ngày tháng\",");
            sb.AppendLine($"Sum(TransDetail.{_totalVat}) as \"Số tiền\",");
            sb.AppendLine($"Transactions.TransactionID,");
            sb.AppendLine($"Transactions.TransDate");

            sb.AppendLine($"from");
            sb.AppendLine($"Transactions");

            sb.AppendLine($"join");
            sb.AppendLine($"TransDetail");
            sb.AppendLine($"on");
            sb.AppendLine($"Transactions.TransactionID = TransDetail.TransactionID");

            sb.AppendLine($"join");
            sb.AppendLine($"Customers");
            sb.AppendLine($"on");
            sb.AppendLine($"Transactions.{_impExpID} = Customers.CustomerID");

            sb.AppendLine($"where");
            sb.AppendLine($"Transactions.Status = '1'");
            sb.AppendLine($"and Transactions.TransCode = '{_trancode}'");
            sb.AppendLine($"and Transactions.TransDate >= '{Module1.ConvertDateToTransDate((System.DateTime)dateEditThanhCongStartDate.EditValue)}'");
            sb.AppendLine($"and Transactions.TransDate <= '{Module1.ConvertDateToTransDate((System.DateTime)dateEditThanhCongEndDate.EditValue)}'");
            sb.AppendLine($"and Transactions.{_impExpID} in ({GetCustomerIDsWhereIn()})");

            sb.AppendLine($"group by");
            sb.AppendLine($"RIGHT(Transactions.TransDate,2)+'/'+ Substring(Transactions.TransDate,5,2)+'/'+ LEFT(Transactions.TransDate,4),");
            sb.AppendLine($"Transactions.TransactionID,");
            sb.AppendLine($"Transactions.TransDate");

            sb.AppendLine($"order by");
            sb.AppendLine($"Transactions.TransDate,");
            sb.AppendLine($"Transactions.TransactionID");
            #endregion
            string query = sb.ToString();
            return await ADO.GetDataTableFromSQLServerAsync(_connectionStringSQL, query).ConfigureAwait(false);
        }

        async Task<System.Data.DataTable> GetTransDetail(int vatRate)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            #region sb
            sb.AppendLine($"select");
            sb.AppendLine($"Transactions.TransactionID,");
            sb.AppendLine($"TransDetail.GoodID as \"{Mvvm.Models.TableColumns.GridTransDetail.GoodID}\",");
            sb.AppendLine($"'' as \"{Mvvm.Models.TableColumns.GridTransDetail.VietseaCode}\",");
            sb.AppendLine($"'' as \"{Mvvm.Models.TableColumns.GridTransDetail.VietseaName}\",");
            sb.AppendLine($"'' as \"{Mvvm.Models.TableColumns.GridTransDetail.IsEqual}\",");
            sb.AppendLine($"Goods.ShortName as \"{Mvvm.Models.TableColumns.GridTransDetail.GoodName}\",");
            sb.AppendLine($"TransDetail.Quantity as \"{Mvvm.Models.TableColumns.GridTransDetail.Quantity}\",");
            //sb.AppendLine($"format(TransDetail.{_priceVat}/1.1,'N0') as \"Đơn giá\",");
            //sb.AppendLine($"format(TransDetail.{_totalVat}/1.1,'N0') as \"Thành tiền\",");
            //sb.AppendLine($"format(TransDetail.{_totalVat},'N0') as \"Thành tiền VAT\"");
            sb.AppendLine($"TransDetail.{_priceVat}/(1+{vatRate}/100.0) as \"{Mvvm.Models.TableColumns.GridTransDetail.Price}\",");
            sb.AppendLine($"TransDetail.{_totalVat}/(1+{vatRate}/100.0) as \"{Mvvm.Models.TableColumns.GridTransDetail.Amount}\",");
            sb.AppendLine($"{vatRate} as \"{Mvvm.Models.TableColumns.GridTransDetail.Vat}\",");
            sb.AppendLine($"TransDetail.{_totalVat} as \"{Mvvm.Models.TableColumns.GridTransDetail.AmountVAT}\"");
            sb.AppendLine($"from");
            sb.AppendLine($"Transactions");
            sb.AppendLine($"join");
            sb.AppendLine($"TransDetail");
            sb.AppendLine($"on");
            sb.AppendLine($"TransDetail.TransactionID = Transactions.TransactionID");
            sb.AppendLine($"join");
            sb.AppendLine($"Customers");
            sb.AppendLine($"on");
            sb.AppendLine($"Transactions.{_impExpID} = Customers.CustomerID");
            sb.AppendLine($"join");
            sb.AppendLine($"Goods");
            sb.AppendLine($"on TransDetail.GoodID = Goods.GoodID");
            sb.AppendLine($"where");
            sb.AppendLine($"Transactions.Status = '1'");
            sb.AppendLine($"and Transactions.{_impExpID} in ({GetCustomerIDsWhereIn()})");
            sb.AppendLine($"and Transactions.TransDate >= '{Module1.ConvertDateToTransDate((System.DateTime)dateEditThanhCongStartDate.EditValue)}'");
            sb.AppendLine($"and Transactions.TransDate <= '{Module1.ConvertDateToTransDate((System.DateTime)dateEditThanhCongEndDate.EditValue)}'");
            #endregion

            string query = sb.ToString();
            DataTable dt = await ADO.GetDataTableFromSQLServerAsync(_connectionStringSQL, query).ConfigureAwait(false);
            if (dt.HasData())
            {
                foreach (DataRow row in dt.Rows)
                {
                    string goodID = row[Mvvm.Models.TableColumns.GridTransDetail.GoodID].ToString();
                    DataRow[] dataRows = _bangma.Select($"ma = '{goodID}'");

                    string maVietSea = dataRows.Length > 0 ? _bangma.Select($"ma = '{goodID}'")[0][Mvvm.Models.TableColumns.GridTransDetail.VietseaCode].ToString() : "#N/A";
                    row[Mvvm.Models.TableColumns.GridTransDetail.VietseaCode] = maVietSea;

                    string tenVietsea = await VIETSEA.OracleServer.GetTenVatTu(_connectionStringOracle, maVietSea);
                    row[Mvvm.Models.TableColumns.GridTransDetail.VietseaName] = tenVietsea;

                    string goodName = row[Mvvm.Models.TableColumns.GridTransDetail.GoodName].ToString();
                    row[Mvvm.Models.TableColumns.GridTransDetail.IsEqual] = goodName == tenVietsea;
                }
            }
            return dt;

        }

        string GetCustomerIDsWhereIn()
        {
            string input = textEditCustomerIDs.EditValue != null ? textEditCustomerIDs.EditValue.ToString() : string.Empty;
            string[] arr = input.Split(',');

            return arr.ToSQLWhereIn();
        }

        string GetTransactionIDs()
        {
            string result = string.Empty;
            int[] checkedRowIndexes = gridViewSumaryOfTransactions.GetCheckedRows();
            List<string> list = new List<string>();
            foreach (int i in checkedRowIndexes)
            {
                list.Add(gridViewSumaryOfTransactions.GetRowCellValue(i, "TransactionID").ToString());
            }
            return list.ToSQLWhereIn();
        }

        void CopyRecordToGridViewTemp()
        {
            gridTemp.DataSource = _dtTemp;
            gridViewTemp.OptionsView.ShowGroupPanel = false;
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.TransactionID].Visible = false;
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.VietseaName].Visible = false;
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.IsEqual].Visible = false;

            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.GoodID].Visible = false;
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.AmountVAT].Visible = false;
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.Vat].Visible = false;

            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.Quantity].SetNumberFormat();
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.Price].SetNumberFormat();
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.Amount].SetNumberFormat();

            gridViewTemp.BestFitColumns();
            gridViewTemp.FocusedRowHandle = gridViewTemp.RowCount - 1;

            gridViewTemp.OptionsView.ShowFooter = true;
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.Amount].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.Amount].SummaryItem.FieldName = Mvvm.Models.TableColumns.GridTransDetail.Amount;
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.Amount].SummaryItem.Mode = DevExpress.Data.SummaryMode.Mixed;
            gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.Amount].SummaryItem.DisplayFormat = "{0:N2}";

        }

        void RecalculateAmount()
        {
            double oldAmount = System.Convert.ToDouble(gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.Amount].SummaryItem.SummaryValue);
            double newAmount = System.Convert.ToDouble(textBoxDesiredAmount.Text);
            double rate = newAmount / oldAmount;

            for (int i = 0; i < gridViewTemp.RowCount; i++)
            {
                double currentCellValue = System.Convert.ToDouble(gridViewTemp.GetRowCellValue(i, Mvvm.Models.TableColumns.GridTransDetail.Amount));
                double newCellValue = Math.Round(currentCellValue * rate, 0);
                gridViewTemp.SetRowCellValue(i, Mvvm.Models.TableColumns.GridTransDetail.Amount, newCellValue.ToString("N2"));
            }

            //check sum if equal to desireAmount
            double currentSum = System.Convert.ToDouble(gridViewTemp.Columns[Mvvm.Models.TableColumns.GridTransDetail.Amount].SummaryText);
            double smallChange = newAmount - currentSum;

            double currentCell1Value = System.Convert.ToDouble(gridViewTemp.GetRowCellValue(0, Mvvm.Models.TableColumns.GridTransDetail.Amount));
            double newCell1Value = currentCell1Value + smallChange;

            gridViewTemp.SetRowCellValue(0, Mvvm.Models.TableColumns.GridTransDetail.Amount, newCell1Value.ToString("N2"));

            gridViewTemp.BestFitColumns();

        }

        private async Task<DataTable> GetDetailByOneAccountAsync(string connectionString, string taiKhoan, List<string> customers)
        {
            string ids = string.Empty;
            foreach (string item in customers)
            {
                ids += "," + item;
            }

            ids = ids.Trim(',');

            System.Collections.Generic.List<System.Data.SqlClient.SqlParameter> sqlParameters = new System.Collections.Generic.List<System.Data.SqlClient.SqlParameter>();
            sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@FromDate", ADO.ConvertDateToTransDate(dateEditThanhCongStartDate.DateTime)));
            sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@EndDate", ADO.ConvertDateToTransDate(dateEditThanhCongEndDate.DateTime)));
            sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@AccountNumber", taiKhoan));
            sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@SupplyID", ids));

            System.Data.DataTable dt = await ADO.GetDataTableFromStoredProcedureAsync(_connectionStringSQL, "[dbo].[Accounting.Report.DetailByOneAccount]", sqlParameters).ConfigureAwait(false);
            if (!dt.HasData())
            {
                return null;
            }

            return dt;
        }

        private async Task<DataTable> GetDetailByOneAccountAsync(string connectionString, string taiKhoan, string customerID)
        {
            string ids = string.Empty;
            List<string> customerIDs = new List<string>();
            customerIDs.Add(customerID);

            return await GetDetailByOneAccountAsync(connectionString, taiKhoan, customerIDs);
        }

        private async Task ShowDetailByOneAccountAsync()
        {
            string paramCustomerIDs = textEditCustomerIDs.EditValue != null ? textEditCustomerIDs.EditValue.ToString() : string.Empty;
            if (paramCustomerIDs.IsEmpty())
            {
                this.SafeInvoke(() => { MessageBox.Show(this, "Không có dữ liệu mã nhà cung cấp (CustomerID)"); });
                return;
            }

            List<string> list = paramCustomerIDs.Split(',').ToList();

            System.Data.DataTable dt = await GetDetailByOneAccountAsync(_connectionStringSQL, "331", list).ConfigureAwait(false);
            if (!dt.HasData())
            {
                this.SafeInvoke(() => { MessageBox.Show(this, "Không có dữ liệu"); });
                return;
            }

            System.Data.DataTable dataSource = dt.ToDataTableSelectedColumns("Transdate", "TransactionID", "Description", "AppAccount", "DebitValue", "CreditValue", "SurplusDebit", "SurplusCredit");

            decimal DebitStartValue = System.Convert.ToDecimal(dt.Compute("AVG(DebitStartValue)", ""));
            decimal CreditStartValue = System.Convert.ToDecimal(dt.Compute("AVG(CreditStartValue)", ""));

            DataRow dataRow = dataSource.NewRow();
            dataRow.ItemArray = new object[] { "", "", "Số dư đầu kỳ", 0, 0, 0, DebitStartValue, CreditStartValue };
            dataSource.Rows.InsertAt(dataRow, 0);

            gridDetailByOneAccountThanhCong.SafeBeginInvoke(() =>
            {
                gridDetailByOneAccountThanhCong.DataSource = dataSource;
                SetVisual();
                SetNumberFormat();
                AddSumaryItem();
            });

            void SetVisual()
            {
                gridViewDetailByOneAccountThanhCong.SetBestFitColumns(0, "Transdate", "TransactionID");
                gridViewDetailByOneAccountThanhCong.SetBestFitColumns(80, "AppAccount");
                gridViewDetailByOneAccountThanhCong.SetBestFitColumns(80, "DebitValue", "CreditValue");
                gridViewDetailByOneAccountThanhCong.SetBestFitColumns(80, "SurplusDebit", "SurplusCredit");
                gridViewDetailByOneAccountThanhCong.OptionsView.ShowGroupPanel = false;
                gridViewDetailByOneAccountThanhCong.OptionsView.ShowAutoFilterRow = true;
                gridViewDetailByOneAccountThanhCong.SetMultiSelectCheckbox();

                //isColorSet = false;
                //gridViewDetailByOneAccountThanhCong.RowCellStyle += GridViewDetailByOneAccountThanhCong_RowCellStyle;
            }

            void SetNumberFormat()
            {
                gridViewDetailByOneAccountThanhCong.Columns["DebitValue"].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                gridViewDetailByOneAccountThanhCong.Columns["CreditValue"].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                gridViewDetailByOneAccountThanhCong.Columns["SurplusDebit"].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                gridViewDetailByOneAccountThanhCong.Columns["SurplusCredit"].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
            }

            void AddSumaryItem()
            {
                gridViewDetailByOneAccountThanhCong.Columns["DebitValue"].AddSumaryItem(DevExpress.Data.SummaryItemType.Sum);
                gridViewDetailByOneAccountThanhCong.Columns["CreditValue"].AddSumaryItem(DevExpress.Data.SummaryItemType.Sum);
            }



        }

        private async Task<DataTable> GetDataChiTietCongNo(DateTime fromDate, DateTime toDate, string ncc)
        {
            string tu_ngay = fromDate.ToString("dd/MM/yyyy");
            string den_ngay = toDate.ToString("dd/MM/yyyy");
            int p_nam = fromDate.Year;

            System.Data.DataTable data = await VIETSEA.TBNETERP.BC_SKT_S38DN_BCMAIN(_connectionStringOracle, "", p_nam, tu_ngay, den_ngay, "MATK LIKE '331%' ", $"CTDOITUONG LIKE '%DMNHACUNGCAP:{ncc};%' ", "", "", "");
            if (data != null && data.Rows.Count > 0)
            {
                RutGonVaTinhSoDuLuyKe(data);


            }

            return data;

            //local
            void RutGonVaTinhSoDuLuyKe(DataTable dataTable)
            {
                //rút gọn
                for (int i = dataTable.Rows.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        string soCT1 = dataTable.Rows[i]["SOCTUGOC"].ToString();
                        string soCT0 = dataTable.Rows[i + 1]["SOCTUGOC"].ToString();
                        if (soCT1 == soCT0)
                        {
                            dataTable.Rows[i]["SDDK_NO"] = System.Convert.ToDouble(dataTable.Rows[i]["SDDK_NO"].ToString()) + System.Convert.ToDouble(dataTable.Rows[i + 1]["SDDK_NO"].ToString());
                            dataTable.Rows[i]["SDDK_CO"] = System.Convert.ToDouble(dataTable.Rows[i]["SDDK_CO"].ToString()) + System.Convert.ToDouble(dataTable.Rows[i + 1]["SDDK_CO"].ToString());

                            dataTable.Rows[i]["SPSTK_NO"] = System.Convert.ToDouble(dataTable.Rows[i]["SPSTK_NO"].ToString()) + System.Convert.ToDouble(dataTable.Rows[i + 1]["SPSTK_NO"].ToString());
                            dataTable.Rows[i]["SPSTK_CO"] = System.Convert.ToDouble(dataTable.Rows[i]["SPSTK_CO"].ToString()) + System.Convert.ToDouble(dataTable.Rows[i + 1]["SPSTK_CO"].ToString());

                            dataTable.Rows.RemoveAt(i + 1);
                        }
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                    }
                }

                //tính số dư đầu kỳ
                double SUM_SDDK_NO = System.Convert.ToDouble(dataTable.Compute("SUM(SDDK_NO)", ""));
                double SUM_SDDK_CO = System.Convert.ToDouble(dataTable.Compute("SUM(SDDK_CO)", ""));
                double SDDK_NO = System.Math.Max(0, SUM_SDDK_NO - SUM_SDDK_CO);
                double SDDK_CO = System.Math.Max(0, SUM_SDDK_CO - SUM_SDDK_NO);
                dataTable.Rows[0]["SDDK_NO"] = SDDK_NO;
                dataTable.Rows[0]["SDDK_CO"] = SDDK_CO;

                //tính số dư lũy kế
                for (int i = 1; i < dataTable.Rows.Count; i++)
                {
                    double dkNO = System.Convert.ToDouble(dataTable.Rows[i - 1]["SDDK_NO"]);
                    double dkCO = System.Convert.ToDouble(dataTable.Rows[i - 1]["SDDK_CO"]);
                    double psNO = System.Convert.ToDouble(dataTable.Rows[i]["SPSTK_NO"]);
                    double psCO = System.Convert.ToDouble(dataTable.Rows[i]["SPSTK_CO"]);
                    dataTable.Rows[i]["SDDK_NO"] = System.Math.Max(0, dkNO - dkCO + psNO - psCO);
                    dataTable.Rows[i]["SDDK_CO"] = System.Math.Max(0, dkCO - dkNO + psCO - psNO);
                }
            }
        }



        private async Task GetSoChiTiet3311()
        {
            string tu_ngay = dateEditVietseaStartDate.DateTime.ToString("dd/MM/yyyy");
            string den_ngay = dateEditVietseaEndDate.DateTime.ToString("dd/MM/yyyy");
            int p_nam = dateEditVietseaStartDate.DateTime.Year;

            string ncc = gridLookUpEditMaNCCVietsea.Text;
            System.Data.DataTable data = await VIETSEA.TBNETERP.BC_SKT_S38DN_BCMAIN(_connectionStringOracle, "", p_nam, tu_ngay, den_ngay, "MATK LIKE '331%' ", $"CTDOITUONG LIKE '%DMNHACUNGCAP:{ncc};%' ", "", "", "");
            if (data != null && data.Rows.Count > 0)
            {
                RutGonVaTinhSoDuLuyKe();

                gridSoChiTietTKVietsea.SafeBeginInvoke(() =>
                {
                    gridSoChiTietTKVietsea.DataSource = data;
                    SetNumberFormatAndVisual();
                });
            }


            await GetDataTheoDoiCongNo331();


            void RutGonVaTinhSoDuLuyKe()
            {
                for (int i = data.Rows.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        string soCT1 = data.Rows[i]["SOCTKTPK"].ToString();
                        string soCT0 = data.Rows[i + 1]["SOCTKTPK"].ToString();
                        if (soCT1 == soCT0)
                        {
                            data.Rows[i]["SDDK_NO"] = System.Convert.ToDouble(data.Rows[i]["SDDK_NO"].ToString()) + System.Convert.ToDouble(data.Rows[i + 1]["SDDK_NO"].ToString());
                            data.Rows[i]["SDDK_CO"] = System.Convert.ToDouble(data.Rows[i]["SDDK_CO"].ToString()) + System.Convert.ToDouble(data.Rows[i + 1]["SDDK_CO"].ToString());

                            data.Rows[i]["SPSTK_NO"] = System.Convert.ToDouble(data.Rows[i]["SPSTK_NO"].ToString()) + System.Convert.ToDouble(data.Rows[i + 1]["SPSTK_NO"].ToString());
                            data.Rows[i]["SPSTK_CO"] = System.Convert.ToDouble(data.Rows[i]["SPSTK_CO"].ToString()) + System.Convert.ToDouble(data.Rows[i + 1]["SPSTK_CO"].ToString());

                            data.Rows.RemoveAt(i + 1);
                        }
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                    }
                }

                //tính số dư đầu kỳ
                double SUM_SDDK_NO = System.Convert.ToDouble(data.Compute("SUM(SDDK_NO)", ""));
                double SUM_SDDK_CO = System.Convert.ToDouble(data.Compute("SUM(SDDK_CO)", ""));
                double SDDK_NO = System.Math.Max(0, SUM_SDDK_NO - SUM_SDDK_CO);
                double SDDK_CO = System.Math.Max(0, SUM_SDDK_CO - SUM_SDDK_NO);
                data.Rows[0]["SDDK_NO"] = SDDK_NO;
                data.Rows[0]["SDDK_CO"] = SDDK_CO;

                //tính số dư lũy kế
                for (int i = 1; i < data.Rows.Count; i++)
                {
                    double dkNO = System.Convert.ToDouble(data.Rows[i - 1]["SDDK_NO"]);
                    double dkCO = System.Convert.ToDouble(data.Rows[i - 1]["SDDK_CO"]);
                    double psNO = System.Convert.ToDouble(data.Rows[i]["SPSTK_NO"]);
                    double psCO = System.Convert.ToDouble(data.Rows[i]["SPSTK_CO"]);
                    data.Rows[i]["SDDK_NO"] = System.Math.Max(0, dkNO - dkCO + psNO - psCO);
                    data.Rows[i]["SDDK_CO"] = System.Math.Max(0, dkCO - dkNO + psCO - psNO);
                }
            }

            void SetNumberFormatAndVisual()
            {
                #region number format
                gridViewSoChiTietTKVietsea.Columns["NGAYHACHTOAN"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                gridViewSoChiTietTKVietsea.Columns["NGAYHACHTOAN"].DisplayFormat.FormatString = "{0:dd/MM/yyyy}";
                gridViewSoChiTietTKVietsea.Columns["NGAYCTKT"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                gridViewSoChiTietTKVietsea.Columns["NGAYCTKT"].DisplayFormat.FormatString = "{0:dd/MM/yyyy}";

                gridViewSoChiTietTKVietsea.Columns["SPSTK_NO"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                gridViewSoChiTietTKVietsea.Columns["SPSTK_NO"].DisplayFormat.FormatString = "{0:N0}";
                gridViewSoChiTietTKVietsea.Columns["SPSTK_CO"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                gridViewSoChiTietTKVietsea.Columns["SPSTK_CO"].DisplayFormat.FormatString = "{0:N0}";
                gridViewSoChiTietTKVietsea.Columns["SDDK_NO"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                gridViewSoChiTietTKVietsea.Columns["SDDK_NO"].DisplayFormat.FormatString = "{0:N0}";
                gridViewSoChiTietTKVietsea.Columns["SDDK_CO"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                gridViewSoChiTietTKVietsea.Columns["SDDK_CO"].DisplayFormat.FormatString = "{0:N0}";
                #endregion

                #region visual
                gridViewSoChiTietTKVietsea.OptionsView.ShowGroupPanel = false;
                gridViewSoChiTietTKVietsea.OptionsView.ShowAutoFilterRow = true;

                gridViewSoChiTietTKVietsea.Columns["SOCTKTPK"].Visible = false;

                gridViewSoChiTietTKVietsea.SetMultiSelectCheckbox();

                gridViewSoChiTietTKVietsea.SetBestFitColumns(80, "MATKDU", "SPSTK_NO", "SPSTK_CO", "SDDK_NO", "SDDK_CO");
                gridViewSoChiTietTKVietsea.SetBestFitColumns(0, "NGAYHACHTOAN", "NGAYCTKT");
                gridViewSoChiTietTKVietsea.SetBestFitColumns(80, "SOCTUGOC");

                isColorSet = false;
                gridViewSoChiTietTKVietsea.RowCellStyle += GridViewSoChiTietTKVietsea_RowCellStyle;

                #endregion

                //add sumary item
                gridViewSoChiTietTKVietsea.OptionsView.ShowFooter = true;
                gridViewSoChiTietTKVietsea.Columns["SPSTK_NO"].AddSumaryItem(DevExpress.Data.SummaryItemType.Sum);
                gridViewSoChiTietTKVietsea.Columns["SPSTK_CO"].AddSumaryItem(DevExpress.Data.SummaryItemType.Sum);

            }
        }


        private async Task GetDataTheoDoiCongNo331()
        {
            try
            {
                string accessFile = _AccessFileOneDriveDatabase;
                if (accessFile.IsEmpty())
                {
                    return;
                }

                string MANHACUNGCAP = gridLookUpEditMaNCCVietsea.GetText();

                string checkIfExist = $"SELECT * FROM {Public.OneDrive.CongNoNCC.TableName} WHERE {Public.OneDrive.CongNoNCC.MANHACUNGCAP} = '{MANHACUNGCAP}'";
                DataTable dt = await ADO.GetDataFromAccessFileAsync(accessFile, checkIfExist).ConfigureAwait(false);

                richTextBoxStatusTimeCheck.SafeInvoke(() => richTextBoxStatusTimeCheck.Clear());
                richTextBoxNewStatusTheoDoiCongNo.SafeInvoke(() => richTextBoxNewStatusTheoDoiCongNo.Clear());

                if (dt.HasData())
                {
                    string time = $"Kiểm tra lần cuối: {dt.Rows[0]["THOIGIANCHECK"]}";
                    string status = $"{dt.Rows[0]["GHICHU"]}";

                    richTextBoxStatusTimeCheck.SafeInvoke(() => richTextBoxStatusTimeCheck.Text = time);
                    richTextBoxNewStatusTheoDoiCongNo.SafeInvoke(() => richTextBoxNewStatusTheoDoiCongNo.Text = status);
                }

            }
            catch
            {

            }
        }

        private async void gridViewSumaryOfTransactions_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            int vatRate = System.Convert.ToInt32(comboBoxImpVat.SelectedItem.ToString());
            _dtTransDetail = await GetTransDetail(vatRate);

            gridTransactionDetails.SafeBeginInvoke(() =>
            {
                gridTransactionDetails.DataSource = null;
                gridViewTransantionDetails.Columns.Clear();
            });

            //_dtTransDetail = await getTransDetailTask;
            if (_dtTemp == null) _dtTemp = _dtTransDetail.Clone();
            if (_dtTransDetail.HasData())
            {
                System.Data.DataRow[] dataRows = _dtTransDetail.Select($"TransactionID in ({GetTransactionIDs()})");
                if (dataRows.Length > 0)
                {
                    System.Data.DataTable dataTable = dataRows.CopyToDataTable();
                    gridTransactionDetails.SafeBeginInvoke(() =>
                    {
                        gridTransactionDetails.DataSource = dataTable;


                        #region modifi
                        //add button 
                        AddButtonEditCopyRecordToTempTable();
                        AddButtonEditSearchGoodID();
                        AddButtonEditSearchVietseaProductCode();

                        //add combobox vat
                        var combobox = gridViewTransantionDetails.AddComboboxButton("VAT", Public.VatRate.VatList);
                        combobox.EditValueChanged += Combobox_EditValueChanged;
                        void Combobox_EditValueChanged(object s, EventArgs e1)
                        {
                            var combo = s as DevExpress.XtraEditors.ComboBoxEdit;
                            int vat = System.Convert.ToInt32(combo.EditValue);
                            int rowHandle = DevExpressExtensions.GetGridView(s).FocusedRowHandle;
                            double amountVAT = System.Convert.ToDouble(gridViewTransantionDetails.GetRowCellValue(rowHandle, "Thành tiền VAT"));
                            double quantity = System.Convert.ToDouble(gridViewTransantionDetails.GetRowCellValue(rowHandle, "Số lượng"));
                            double newAmount = amountVAT / (1 + vat / 100.00);
                            double newPrice = newAmount / quantity;

                            gridViewTransantionDetails.SetRowCellValue(rowHandle, "Thành tiền", newAmount);
                            gridViewTransantionDetails.SetRowCellValue(rowHandle, "Đơn giá", newPrice);
                        }

                        //footer
                        gridViewTransantionDetails.Columns[Mvvm.Models.TableColumns.GridTransDetail.Price].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                        gridViewTransantionDetails.Columns[Mvvm.Models.TableColumns.GridTransDetail.Amount].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                        gridViewTransantionDetails.Columns[Mvvm.Models.TableColumns.GridTransDetail.AmountVAT].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");

                        //midifi columns
                        gridViewTransantionDetails.Columns["TransactionID"].Visible = false;
                        gridViewTransantionDetails.Columns["Tên Vietsea"].Visible = false;
                        gridViewTransantionDetails.OptionsView.ShowGroupPanel = false;
                        gridViewTransantionDetails.OptionsView.ShowAutoFilterRow = true;
                        gridViewTransantionDetails.SetBestFitColumns(0, "Mã hàng", "Mã Vietsea", "IsEqual", "Số lượng", "Đơn giá", "Thành tiền", "VAT", "Thành tiền VAT");

                        //sort column to see #N/A error
                        gridViewTransantionDetails.Columns[Mvvm.Models.TableColumns.GridTransDetail.VietseaCode].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;

                        AddSumaryItem();

                        CheckVietseaNameAndSetColor();
                        #endregion
                    });
                }
            }

            #region local
            void AddSumaryItem()
            {
                gridViewTransantionDetails.Columns[Mvvm.Models.TableColumns.GridTransDetail.GoodID].AddSumaryItem(DevExpress.Data.SummaryItemType.Count);
            }

            void CheckVietseaNameAndSetColor()
            {
                gridViewTransantionDetails.RowCellStyle += GridViewTransantionDetails_RowCellStyle;
                gridViewTransantionDetails.LayoutChanged();
            }
            #endregion

        }

        private void GridViewTransantionDetails_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gridView = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            if (gridView == null) return;

            var obj = gridView.GetRowCellValue(e.RowHandle, "IsEqual");
            if (obj == null) return;
            string equal = obj.ToString();

            if (e.Column.FieldName == "IsEqual")
            {
                if (equal == "True")
                {
                    e.Appearance.BackColor = System.Drawing.Color.LightCyan;
                }
            }
        }

        private void AddButtonEditSearchVietseaProductCode()
        {
            DevExpress.XtraEditors.Controls.EditorButton editorButton = new DevExpress.XtraEditors.Controls.EditorButton();
            editorButton.Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Search;
            gridViewTransantionDetails.AddButtonEdit("Mã Vietsea", out DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repository, editorButton);
            repository.ButtonClick += async (sender, e) =>
            {
                DevExpress.XtraGrid.Views.Grid.GridView gridView = DevExpressExtensions.GetGridView(sender);
                DataRow row = gridView.GetDataRow(gridView.FocusedRowHandle);
                string maVietSea = row["Mã Vietsea"].ToString();
                string tenVatTuVietsea = await VIETSEA.OracleServer.GetTenVatTu(_connectionStringOracle, maVietSea);
                MessageBox.Show(this, tenVatTuVietsea);
            };
        }

        private void AddButtonEditCopyRecordToTempTable()
        {
            DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reposityItemButtonEditCopyRecord = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            reposityItemButtonEditCopyRecord.Buttons.Clear();
            DevExpress.XtraEditors.Controls.EditorButton button = new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus);
            reposityItemButtonEditCopyRecord.Buttons.Add(button);
            reposityItemButtonEditCopyRecord.ButtonClick += reposityItemButtonEditCopyRecord_ButtonClick;
            gridViewTransantionDetails.Columns[Mvvm.Models.TableColumns.GridTransDetail.GoodName].ColumnEdit = reposityItemButtonEditCopyRecord;
            gridViewTransantionDetails.Columns[Mvvm.Models.TableColumns.GridTransDetail.GoodName].ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            gridViewTransantionDetails.GridControl.RepositoryItems.Add(reposityItemButtonEditCopyRecord);
        }

        private void AddButtonEditSearchGoodID()
        {
            DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditSearchGoodID = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            repositoryItemButtonEditSearchGoodID.Buttons.Clear();
            DevExpress.XtraEditors.Controls.EditorButton editorButton = new DevExpress.XtraEditors.Controls.EditorButton();
            editorButton.Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Search;
            repositoryItemButtonEditSearchGoodID.Buttons.Add(editorButton);
            repositoryItemButtonEditSearchGoodID.ButtonClick += RepositoryItemButtonEditSearchGoodID_ButtonClick;

            gridTransactionDetails.SafeInvoke(() =>
            {
                DevExpress.XtraGrid.Columns.GridColumn column = gridViewTransantionDetails.Columns[Mvvm.Models.TableColumns.GridTransDetail.GoodID];
                column.ColumnEdit = repositoryItemButtonEditSearchGoodID;
                column.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
                gridViewTransantionDetails.GridControl.RepositoryItems.Add(repositoryItemButtonEditSearchGoodID);
                column.BestFit();
                column.OptionsColumn.FixedWidth = true;
            });

        }

        private async void RepositoryItemButtonEditSearchGoodID_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.BaseEdit editor = (DevExpress.XtraEditors.BaseEdit)sender;
            DevExpress.XtraGrid.GridControl grid = editor.Parent as DevExpress.XtraGrid.GridControl;
            DevExpress.XtraGrid.Views.Grid.GridView view = grid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var row = view.GetDataRow(view.FocusedRowHandle);
            string goodID = row[Mvvm.Models.TableColumns.GridTransDetail.GoodID].ToString();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            #region sb
            sb.AppendLine($"Select");
            sb.AppendLine($"Goods.GoodID,");
            sb.AppendLine($"Goods.ShortName,");
            sb.AppendLine($"Goods.SupplierID,");
            sb.AppendLine($"Customers.CustomerName,");
            sb.AppendLine($"Units.UnitName,");
            sb.AppendLine($"Goods.ConvertUnit,");
            sb.AppendLine($"Goods.LastImpPriceVat,");
            sb.AppendLine($"Goods.ExpRetailPriceVat,");
            sb.AppendLine($"Goods.Status");
            sb.AppendLine($"From");
            sb.AppendLine($"Goods Inner Join");
            sb.AppendLine($"Units On Goods.UnitID = Units.UnitID Inner Join");
            sb.AppendLine($"Customers On Goods.SupplierID = Customers.CustomerID");
            sb.AppendLine($"Where");
            sb.AppendLine($"Goods.GoodID= '{goodID}'");
            #endregion

            string query = sb.ToString();
            System.Data.DataTable dt = await ADO.GetDataTableFromSQLServerAsync(_connectionStringSQL, query);
            if (dt.HasData())
            {
                UserClass.Good good = new UserClass.Good();

                good.GoodID = dt.Rows[0]["GoodID"].ToString();
                good.ShortName = dt.Rows[0]["ShortName"].ToString();
                good.SupplierID = dt.Rows[0]["SupplierID"].ToString();
                good.CustomerName = dt.Rows[0]["CustomerName"].ToString();
                good.UnitName = dt.Rows[0]["UnitName"].ToString();
                good.ConvertUnit = int.Parse(dt.Rows[0]["ConvertUnit"].ToString());
                good.LastImpPriceVat = dt.Rows[0]["LastImpPriceVat"].ToString();
                good.ExpRetailPriceVat = dt.Rows[0]["ExpRetailPriceVat"].ToString();
                good.Status = dt.Rows[0]["Status"].ToString();

                string commentText = $"Mã vật tư: {good.GoodID}" + Environment.NewLine
                                        + $"Tên vật tư: {good.ShortName}" + Environment.NewLine
                                        + $"Mã NCC: {good.SupplierID}" + Environment.NewLine
                                        + $"Tên NCC: {good.CustomerName}" + Environment.NewLine
                                        + $"Đơn vị tính: {good.UnitName}" + Environment.NewLine
                                        + $"Quy cách: {good.ConvertUnit}" + Environment.NewLine
                                        + $"Giá nhập gần nhất: {Convert.ToDouble(good.LastImpPriceVat).ToString("N2")}" + Environment.NewLine
                                        + $"Giá bán lẻ: {Convert.ToDouble(good.ExpRetailPriceVat).ToString("N2")}" + Environment.NewLine
                                        + $"Tình trạng: {good.Status}";

                this.SafeInvoke(() =>
                {
                    MessageBox.Show(this, commentText, "Thông tin vật tư");
                    //this.BringToFront();
                });
            }
        }


        private void reposityItemButtonEditCopyRecord_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraEditors.BaseEdit editor = (DevExpress.XtraEditors.BaseEdit)sender;
            DevExpress.XtraGrid.GridControl grid = editor.Parent as DevExpress.XtraGrid.GridControl;
            DevExpress.XtraGrid.Views.Grid.GridView gridView = grid.FocusedView as DevExpress.XtraGrid.Views.Grid.GridView;
            var row = gridView.GetDataRow(gridView.FocusedRowHandle);
            _dtTemp.Rows.Add(row.ItemArray);

            CopyRecordToGridViewTemp();

            System.Windows.Forms.SendKeys.SendWait("{ESC}");
        }

        private void SetRowColor(DevExpress.XtraGrid.Views.Grid.GridView gridView, System.Drawing.Color color, params int[] rows)
        {
            gridView.RowStyle += (sender1, e1) =>
            {
                foreach (int row in rows)
                {
                    if (e1.RowHandle == row)
                    {
                        e1.Appearance.BackColor = System.Drawing.Color.Yellow;
                        e1.HighPriority = true;
                    }
                }
            };
            gridView.LayoutChanged();
        }

        private void gridViewTemp_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (!_allowCellValueChange)
            {
                _allowCellValueChange = !_allowCellValueChange;
                return;
            }
            _allowCellValueChange = !_allowCellValueChange;

            DevExpress.XtraGrid.Views.Grid.GridView gridView = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            decimal quantity = System.Convert.ToDecimal(gridView.GetRowCellValue(e.RowHandle, Mvvm.Models.TableColumns.GridTransDetail.Quantity));
            decimal price = System.Convert.ToDecimal(gridView.GetRowCellValue(e.RowHandle, Mvvm.Models.TableColumns.GridTransDetail.Price));
            decimal amount = System.Convert.ToDecimal(gridView.GetRowCellValue(e.RowHandle, Mvvm.Models.TableColumns.GridTransDetail.Amount));

            if (e.Column.GetTextCaption() == Mvvm.Models.TableColumns.GridTransDetail.Quantity || e.Column.GetTextCaption() == Mvvm.Models.TableColumns.GridTransDetail.Price)
            {
                gridView.SetRowCellValue(e.RowHandle, Mvvm.Models.TableColumns.GridTransDetail.Amount, (quantity * price).ToString("N2"));
            }

            if (e.Column.GetTextCaption() == Mvvm.Models.TableColumns.GridTransDetail.Amount)
            {
                gridView.SetRowCellValue(e.RowHandle, Mvvm.Models.TableColumns.GridTransDetail.Price, (amount / quantity).ToString());
            }

            DevExpress.XtraGrid.Columns.GridColumn gridColumn = gridView.Columns[e.Column.GetTextCaption()];
            gridColumn.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gridColumn.DisplayFormat.FormatString = "{0:N0}";

            //recalculate sum using up down button
            gridView.FocusedRowHandle = e.RowHandle + 1;
            gridView.FocusedRowHandle = e.RowHandle;

        }

        private void gridViewTemp_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Delete)
            {
                DevExpress.XtraGrid.Views.Grid.GridView gridView = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                gridView.DeleteRow(gridView.FocusedRowHandle);
            }
        }

        private void gridLookUpEditMaNCCVietsea_Popup(object sender, System.EventArgs e)
        {
            try
            {
                DevExpress.XtraEditors.GridLookUpEdit edit = sender as DevExpress.XtraEditors.GridLookUpEdit;
                if (edit != null)
                {
                    DevExpress.XtraEditors.Popup.PopupGridLookUpEditForm f = edit.GetPopupEditForm();
                    if (f != null)
                    {
                        f.Width = 1100;
                        gridLookUpEditMaNCCVietseaView.SetBestFitColumns();
                    }
                }
            }
            catch { }
        }

        private async void gridLookUpEditMaNCCVietsea_EditValueChanged(object sender, System.EventArgs e)
        {
            try
            {
                string MAKH = gridLookUpEditMaNCCVietsea.EditValue as string;
                string query = string.Format(@"Select * From
(Select kyhieuhoadongd
From Vattugd
Where 
Maptnx = 'NMUA'
And Makhachang = '{0}'
Order By Ngayhachtoan Desc)
where rownum = 1", MAKH);
                var kyhieuHD = await ADO.GetDataTableFromOracleServerAsync(PublicVar.ConnectionStringOracleDatPhat, query);
                if (kyhieuHD.HasData())
                {
                    textBoxSerialNumber.SetTextSafeThread(kyhieuHD.Rows[0][0].ToString());
                }
            }
            catch { }
            await GetSoChiTiet3311();
        }

        private void textBoxDesiredAmount_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter) RecalculateAmount();
        }

        private void comboBoxTranType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            SetParam();
        }

        private void searchCustomers_Properties_Popup(object sender, System.EventArgs e)
        {
            var edit = sender as DevExpress.XtraEditors.SearchLookUpEdit;
            var popupForm = edit.GetPopupEditForm();
            popupForm.Width = this.Width - 50;
            DevExpress.XtraGrid.Views.Grid.GridView view = popupForm.OwnerEdit.Properties.View;
            view.BestFitColumns();

        }

        private void PopupForm_KeyUp(object sender, KeyEventArgs e)
        {
            DevExpress.XtraEditors.Popup.PopupSearchLookUpEditForm popupForm = sender as DevExpress.XtraEditors.Popup.PopupSearchLookUpEditForm;
            if (e.KeyData == System.Windows.Forms.Keys.Enter)
            {
                DevExpress.XtraGrid.Views.Grid.GridView view = popupForm.OwnerEdit.Properties.View;
                popupForm.OwnerEdit.ClosePopup();
                System.Collections.Generic.List<string> values = view.GetSelectedValue("CustomerID");
                string aa = string.Join(",", values);
                searchCustomers.EditValue = aa;
                MessageBox.Show($"{aa}");
            }
        }

        private void searchCustomers_Properties_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView view = ((DevExpress.XtraEditors.SearchLookUpEdit)sender).Properties.View;
                _getCustomerIDs = view.GetSelectedValue("CustomerID");
                string selectedCustomerIDs = string.Join(",", _getCustomerIDs);
                textEditCustomerIDs.EditValue = selectedCustomerIDs;
            }
            catch { }
        }

        private async void buttonChangeConnectionMSSQL_Click(object sender, EventArgs e)
        {
            labelStatusMSSQLConnection.Text = "Chưa kết nối";
            labelStatusMSSQLConnection.ForeColor = System.Drawing.Color.Red;

            Forms.FormConnectSQL formConnectDatabase = new FormConnectSQL();
            string connectionString = formConnectDatabase.GetConnectionString();
            _connectionStringSQL = connectionString;

            if (string.IsNullOrEmpty(connectionString)) return;

            if (await ADO.TestSQLConnection(connectionString))
            {
                _ = LoadCustomerData(connectionString);
                System.Data.SqlClient.SqlConnectionStringBuilder sb = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionString);
                labelStatusMSSQLConnection.Text = $"{sb.DataSource} : {sb.InitialCatalog}";
                labelStatusMSSQLConnection.ForeColor = System.Drawing.Color.Green;
            }
        }

        private async void buttonGetDetailByOneAccount_Click(object sender, EventArgs e)
        {
            try
            {
                buttonGetDetailByOneAccount.InvokeSetEnable(false);
                await Task.Run(ShowDetailByOneAccountAsync);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                buttonGetDetailByOneAccount.InvokeSetEnable(true);
                xtraTabControl1.SafeInvoke(() => { xtraTabControl1.SelectedTabPage = xtraTabPageCongNo; });
            }

        }

        private void buttonToMauDoiChieu_Click(object sender, EventArgs e)
        {
            #region get list cell
            listCellVietsea = new System.Collections.Generic.List<UserClass.DevExpressGridCellinfo>();
            listCellThanhCong = new System.Collections.Generic.List<UserClass.DevExpressGridCellinfo>();

            bool comparativeEqual(object a, object b, int saiSo)
            {
                double number1 = Convert.ToDouble(a);
                double number2 = Convert.ToDouble(b);

                if (number1 > 0 && number2 > 0 && Math.Abs(number1 - number2) <= saiSo)
                {
                    return true;
                }
                return false;
            }

            string VSpsNo = "SPSTK_NO";
            string VSpsCo = "SPSTK_CO";
            string TCpsNo = "DebitValue";
            string TCpsCo = "CreditValue";

            bool IsListContainsCell(System.Collections.Generic.List<UserClass.DevExpressGridCellinfo> list, UserClass.DevExpressGridCellinfo cellIfno)
            {
                foreach (var cell in list)
                {
                    if (cell.RowHandle == cellIfno.RowHandle && cell.ColumnName == cellIfno.ColumnName)
                    {
                        return true;
                    }
                }
                return false;
            }

            for (int VSrow = 0; VSrow < gridViewSoChiTietTKVietsea.RowCount; VSrow++)
            {
                for (int TCrow = 0; TCrow < gridViewDetailByOneAccountThanhCong.RowCount; TCrow++)
                {
                    if (comparativeEqual(gridViewSoChiTietTKVietsea.GetRowCellValue(VSrow, VSpsNo), gridViewDetailByOneAccountThanhCong.GetRowCellValue(TCrow, TCpsNo), 1000))
                    {
                        UserClass.DevExpressGridCellinfo VScellNO = new UserClass.DevExpressGridCellinfo(VSrow, VSpsNo);
                        UserClass.DevExpressGridCellinfo TCcellNO = new UserClass.DevExpressGridCellinfo(TCrow, TCpsNo);

                        if (!IsListContainsCell(listCellVietsea, VScellNO) && !IsListContainsCell(listCellThanhCong, TCcellNO))
                        {
                            listCellVietsea.Add(VScellNO);
                            listCellThanhCong.Add(TCcellNO);
                        }
                    }

                    if (comparativeEqual(gridViewSoChiTietTKVietsea.GetRowCellValue(VSrow, VSpsCo), gridViewDetailByOneAccountThanhCong.GetRowCellValue(TCrow, TCpsCo), 1000))
                    {
                        UserClass.DevExpressGridCellinfo VScellCO = new UserClass.DevExpressGridCellinfo(VSrow, VSpsCo);
                        UserClass.DevExpressGridCellinfo TCcellCO = new UserClass.DevExpressGridCellinfo(TCrow, TCpsCo);

                        if (!IsListContainsCell(listCellVietsea, VScellCO) && !IsListContainsCell(listCellThanhCong, TCcellCO))
                        {
                            listCellVietsea.Add(VScellCO);
                            listCellThanhCong.Add(TCcellCO);
                        }
                    }

                }
            }
            #endregion

            isColorSet = true;
            gridViewSoChiTietTKVietsea.RowCellStyle += GridViewSoChiTietTKVietsea_RowCellStyle;
            gridViewSoChiTietTKVietsea.LayoutChanged();

            gridViewDetailByOneAccountThanhCong.RowCellStyle += GridViewDetailByOneAccountThanhCong_RowCellStyle;
            gridViewDetailByOneAccountThanhCong.LayoutChanged();
        }

        private async void buttonChangeConnectionOracle_Click(object sender, EventArgs e)
        {
            labelOracleStatus.Text = "Chưa kết nối";
            labelOracleStatus.ForeColor = System.Drawing.Color.Red;

            FormConnectOracle formConnectOracle = new FormConnectOracle();
            _connectionStringOracle = formConnectOracle.GetConnectionString();
            Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder sb = new Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder(_connectionStringOracle);
            if (await ADO.TestOracleConnection(_connectionStringOracle))
            {
                _ = InitMaKhachHangVietsea();
                labelOracleStatus.Text = sb.DataSource;
                labelOracleStatus.ForeColor = System.Drawing.Color.Green;
            }
        }

        private async void buttonCreateImportFile_Click(object sender, EventArgs e)
        {
            string serialNumber = textBoxSerialNumber.Text;
            string invoiceNumber = serialNumber.Contains("/") ? textBoxInvoiceNumber.Text.PadLeft(7, '0') : textBoxInvoiceNumber.Text;
            string makhachhang = gridLookUpEditMaNCCVietsea.EditValue.ToString();
            string masothue = textBoxMSTVietsea.Text;
            System.DateTime trandate = dateEditTranDateVietsea.DateTime >= dateEditInvoiceDate.DateTime ? dateEditTranDateVietsea.DateTime : dateEditInvoiceDate.DateTime;
            string date = "'" + trandate.ToString("dd/MM/yyyy");

            int numberOfInvoice = await VIETSEA.TBNETERP.InvoiceExist(_connectionStringOracle, serialNumber, masothue, invoiceNumber);
            if (numberOfInvoice > 0)
            {
                System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
                stringBuilder.AppendLine($"Đã tồn tại {numberOfInvoice} hóa đơn");
                stringBuilder.AppendLine($"Mã khách hàng: {makhachhang}");
                stringBuilder.AppendLine($"Số hóa đơn: {invoiceNumber}");
                stringBuilder.AppendLine($"Ký hiệu hóa đơn: {serialNumber}");
                string text = stringBuilder.ToString();
                MessageBox.Show(text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            Module1.SpeedUpCode(true);
            Workbook importWorkbook = VIETSEA.TBNETERP.FileExcelImportNHAPHANG();
            Worksheet sheet = importWorkbook.GetSheet("MAUHOADON");

            for (int rowHandle = 0; rowHandle < gridViewTemp.RowCount; rowHandle++)
            {
                int excelRow = rowHandle + 2;
                sheet.Range["a" + excelRow].Value = "'" + invoiceNumber;
                sheet.Range["b" + excelRow].Value = gridViewTemp.GetRowCellValue(rowHandle, Mvvm.Models.TableColumns.GridTransDetail.VietseaCode).ToString();
                sheet.Range["c" + excelRow].Value = gridViewTemp.GetRowCellValue(rowHandle, Mvvm.Models.TableColumns.GridTransDetail.GoodName).ToString();
                sheet.Range["d" + excelRow].Value = gridViewTemp.GetRowCellValue(rowHandle, Mvvm.Models.TableColumns.GridTransDetail.Quantity).ToString();
                sheet.Range["e" + excelRow].Value = gridViewTemp.GetRowCellValue(rowHandle, Mvvm.Models.TableColumns.GridTransDetail.Price).ToString();
                sheet.Range["f" + excelRow].Value = string.Empty;
                sheet.Range["g" + excelRow].Value = makhachhang;
                sheet.Range["h" + excelRow].Value = date;
                sheet.Range["i" + excelRow].Value = serialNumber;
            }

            importWorkbook.SaveOverWrite(Public.Variables.ImportNMUAFileName, 56);
            importWorkbook.Close();

            Module1.SpeedUpCode(false);
            Notification.Toast.Show("Tạo file import thành công!");
            BringToFront();
        }

        private async void buttonAutoImport_Click(object sender, EventArgs e)
        {
            string tranDate = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(ExcelAddIn1.PublicVar.fileImportNhapHang_fullPath, "MAUHOADON", "H2");
            string sohoadon = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(ExcelAddIn1.PublicVar.fileImportNhapHang_fullPath, "MAUHOADON", "A2");
            string defaultMaNCC = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(ExcelAddIn1.PublicVar.fileImportNhapHang_fullPath, "MAUHOADON", "G2");
            string invoiceDate = dateEditInvoiceDate.DateTime.ToString("dd/MM/yyyy");
            string vat = comboBoxImpVat.Text;

            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();

                await System.Threading.Tasks.Task.Run(() =>
                {
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.Start();
                    WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);
                    WinAppDriver.Vietsea.VietseaAuto.Auto_ImportPhieuNhapHang(tranDate, sohoadon, invoiceDate, defaultMaNCC, vat, numericUpDownNumberOfPrint.Value.ToString());
                    ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();

                    //ws.Activate();
                    //ExcelAddIn1.AutoTest.WinAppDriver.Method.SetForegroundWindow(new System.IntPtr(Globals.ThisAddIn.Application.Hwnd));
                    Notification.Toast.Show("Nhập phiếu thành công");
                }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);
            }
            catch (System.OperationCanceledException)
            {
                Notification.Toast.Show("Macro was canceled");
            }
            catch (System.Exception ex)
            {
                MsgBox.Show(ex.Message + System.Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                await GetSoChiTiet3311();
                ExcelAddIn1.AutoTest.WinAppDriver.Core.TearDown();
                this.SafeInvoke(() => { this.BringToFront(); });
                xtraTabControl1.SafeInvoke(() => { xtraTabControl1.SelectedTabPage = xtraTabPageCongNo; });
            }
        }

        private void buttonClearTempGrid_Click(object sender, EventArgs e)
        {
            _dtTemp.Rows.Clear();
            gridTemp.DataSource = _dtTemp;
        }

        private void buttonSoChiTiet3311_Click(object sender, EventArgs e)
        {
            try
            {
                _ = Task.Run(GetSoChiTiet3311);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
            finally
            {
                xtraTabControl1.SafeBeginInvoke(() =>
                {
                    xtraTabControl1.SelectedTabPage = xtraTabPageCongNo;
                });
            }
        }

        private async void buttonCreatePayment_Click(object sender, EventArgs e)
        {
            List<UserClass.Invoice> invoiceInfos = new List<UserClass.Invoice>();
            foreach (int rowHandle in gridViewSoChiTietTKVietsea.GetSelectedRows())
            {
                UserClass.Invoice invoiceInfo = new UserClass.Invoice();

                System.DateTime ngayHachToan = (System.DateTime)gridViewSoChiTietTKVietsea.GetRowCellValue(rowHandle, "NGAYHACHTOAN");
                System.DateTime ngayCTKT = (System.DateTime)gridViewSoChiTietTKVietsea.GetRowCellValue(rowHandle, "NGAYCTKT");
                System.DateTime date = ngayHachToan > ngayCTKT ? ngayHachToan : ngayCTKT;
                //phải lấy ngày lớn hơn vì trường hợp 1388 nhập hàng trước hóa đơn về sau,
                //không thể thanh toán trước được

                invoiceInfo.Date = date.ToString("dd/MM/yyyy");
                invoiceInfo.InvoiceNumber = gridViewSoChiTietTKVietsea.GetRowCellValue(rowHandle, "SOCTUGOC").ToString();
                invoiceInfo.CustomerID = gridLookUpEditMaNCCVietsea.Text;
                invoiceInfo.CustomerName = richTextBoxTenNCCVietsea.Text;
                invoiceInfo.TotalAmount = System.Convert.ToDouble(gridViewSoChiTietTKVietsea.GetRowCellValue(rowHandle, "SPSTK_CO"));

                var infoFind = invoiceInfos.Find(x => x.Date == ((System.DateTime)gridViewSoChiTietTKVietsea.GetRowCellValue(rowHandle, "NGAYHACHTOAN")).ToString("dd/MM/yyyy"));
                if (infoFind == null)
                {
                    invoiceInfos.Add(invoiceInfo);
                }
                else
                {
                    infoFind.InvoiceNumber += "+" + gridViewSoChiTietTKVietsea.GetRowCellValue(rowHandle, "SOCTUGOC").ToString();
                    infoFind.TotalAmount += System.Convert.ToInt32(gridViewSoChiTietTKVietsea.GetRowCellValue(rowHandle, "SPSTK_CO").ToString());
                }
            }

            #region automation
            try
            {
                ExcelAddIn1.AutoTest.WinAppDriver.Core.InitToken();
                await Task.Run(() =>
                {
                    AutoTest.WinAppDriver.Core.Start();
                    WinAppDriver.Vietsea.VietseaAuto.OpenVietsea(AddinConfig.UsernameVIETSEA, AddinConfig.PasswordVIETSEA, false);

                    foreach (var invoiceInfo in invoiceInfos)
                    {
                        WinAppDriver.Vietsea.VietseaAuto.Auto_HachToanPhieuChi(invoiceInfo.Date, invoiceInfo.CustomerID, invoiceInfo.CustomerName, invoiceInfo.InvoiceNumber, invoiceInfo.TotalAmount.ToString());
                    }

                    this.SafeInvoke(() =>
                    {
                        this.BringToFront();
                        MessageBox.Show(this, "Hạch toán PC thành công");
                    });

                }, ExcelAddIn1.AutoTest.WinAppDriver.Core.cancellationToken);
            }
            catch (System.OperationCanceledException)
            {
                Notification.Toast.Show("Đã hủy");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                AutoTest.WinAppDriver.Core.TearDown();
            }
            #endregion
        }

        private async void buttonInitBangMa_Click(object sender, EventArgs e)
        {
            await InitBangMa();
            Notification.Toast.Show("Load bảng mã thành công");
        }

        private async void buttonSearchTransactions_Click(object sender, EventArgs e)
        {
            try
            {
                buttonSearchTransactions.InvokeSetEnable(false);

                await Task.Run(async () =>
                {
                    #region MAIN

                    Task<DataTable> GetSumaryOfTransactionsTask = GetSumaryOfTransactions();

                    _ = ShowDetailByOneAccountAsync();
                    _ = Task.Run(() =>
                    {
                        gridTransactionDetails.SafeBeginInvoke(() =>
                        {
                            gridTransactionDetails.DataSource = null;
                            gridViewTransantionDetails.Columns.Clear();
                        });
                    });

                    System.Data.DataTable dtSumaryOfTransactions = await GetSumaryOfTransactionsTask;
                    if (dtSumaryOfTransactions != null && dtSumaryOfTransactions.Rows.Count > 0)
                    {
                        gridSumaryOfTransactions.SafeBeginInvoke(() =>
                        {
                            gridSumaryOfTransactions.DataSource = dtSumaryOfTransactions;

                            #region modifi gridSumaryOfTransactions
                            gridViewSumaryOfTransactions.SetMultiSelectCheckbox();
                            gridViewSumaryOfTransactions.OptionsView.ShowGroupPanel = false;
                            gridViewSumaryOfTransactions.OptionsBehavior.Editable = false;
                            gridViewSumaryOfTransactions.Columns["TransactionID"].Visible = false;
                            gridViewSumaryOfTransactions.Columns["TransDate"].Visible = false;

                            gridViewSumaryOfTransactions.Columns["Số tiền"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                            gridViewSumaryOfTransactions.Columns["Số tiền"].DisplayFormat.FormatString = "{0:N0}";

                            gridViewSumaryOfTransactions.OptionsView.ShowFooter = true;
                            gridViewSumaryOfTransactions.Columns["Số tiền"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
                            gridViewSumaryOfTransactions.Columns["Số tiền"].SummaryItem.FieldName = "Số tiền";
                            gridViewSumaryOfTransactions.Columns["Số tiền"].SummaryItem.Mode = DevExpress.Data.SummaryMode.Selection;
                            gridViewSumaryOfTransactions.Columns["Số tiền"].SummaryItem.DisplayFormat = "{0:N0}";
                            #endregion
                        });
                    }
                    else
                    {
                        gridSumaryOfTransactions.SafeInvoke(() =>
                        {
                            gridViewSumaryOfTransactions.Columns.Clear();
                        });
                        this.SafeInvoke(() => { MessageBox.Show(this, "Không có dữ liệu"); });
                    }
                    #endregion
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.SafeInvoke(() => { this.BringToFront(); });
                buttonSearchTransactions.InvokeSetEnable(true);
            }

        }

        private void buttonSelectAllRecord_Click(object sender, EventArgs e)
        {
            for (int rowHandle = 0; rowHandle < gridViewTransantionDetails.RowCount; rowHandle++)
            {
                DataRow row = gridViewTransantionDetails.GetDataRow(rowHandle);
                _dtTemp.Rows.Add(row.ItemArray);
            }

            CopyRecordToGridViewTemp();
        }

        private async void buttonDownloadInvoiceListFromHoadondientugdtgovvn_Click(object sender, EventArgs e)
        {
            try
            {
                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet ws = wb.GetSheet("HDDT");

                await Task.Run(() =>
                {
                    DataTable table = AutoTest.Selenium.HoadondientuGdt.Flows.GetDataTable(dateEditVietseaStartDate.DateTime, dateEditVietseaEndDate.DateTime);
                    if (table != null && table.Rows.Count > 0)
                    {
                        table.CopyToRange(ws.Range["a1"]);
                        var listObject = ws.UsedRange.ToListObject();
                        listObject.GetColumnRange(AutoTest.Selenium.HoadondientuGdt.ColumnName.TongTienChuaThue).Replace(".", "");
                        listObject.GetColumnRange(AutoTest.Selenium.HoadondientuGdt.ColumnName.TongTienChuaThue).ConvertToValue();

                        listObject.GetColumnRange(AutoTest.Selenium.HoadondientuGdt.ColumnName.TongTienThue).Replace(".", "");
                        listObject.GetColumnRange(AutoTest.Selenium.HoadondientuGdt.ColumnName.TongTienThue).ConvertToValue();

                        listObject.GetColumnRange(AutoTest.Selenium.HoadondientuGdt.ColumnName.TongTienChietKhauThuongMai).Replace(".", "");
                        listObject.GetColumnRange(AutoTest.Selenium.HoadondientuGdt.ColumnName.TongTienChietKhauThuongMai).ConvertToValue();

                        listObject.GetColumnRange(AutoTest.Selenium.HoadondientuGdt.ColumnName.TongTienPhi).Replace(".", "");
                        listObject.GetColumnRange(AutoTest.Selenium.HoadondientuGdt.ColumnName.TongTienPhi).ConvertToValue();

                        listObject.GetColumnRange(AutoTest.Selenium.HoadondientuGdt.ColumnName.TongTienThanhToan).Replace(".", "");
                        listObject.GetColumnRange(AutoTest.Selenium.HoadondientuGdt.ColumnName.TongTienThanhToan).ConvertToValue();

                        listObject.Unlist();
                    }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }

            //autoBrowser.GoTo()
        }

        private void comboBoxImpVat_Properties_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            try
            {
                DevExpress.XtraEditors.Repository.RepositoryItemComboBox comboBox = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)gridViewTransantionDetails.Columns["VAT"].ColumnEdit;

                double vat = System.Convert.ToDouble(comboBoxImpVat.EditValue);
                if (vat == 0) comboBoxKhoNhap.EditValue = "BAN05";
                if (vat == 5) comboBoxKhoNhap.EditValue = "BAN05";
                if (vat == 8) comboBoxKhoNhap.EditValue = "BAN8";
                if (vat == 10) comboBoxKhoNhap.EditValue = "BAN10";

                for (int i = 0; i < gridViewTransantionDetails.RowCount; i++)
                {
                    gridViewTransantionDetails.SetRowCellValue(i, "VAT", vat);

                    double amountVAT = System.Convert.ToDouble(gridViewTransantionDetails.GetRowCellValue(i, "Thành tiền VAT"));
                    double quantity = System.Convert.ToDouble(gridViewTransantionDetails.GetRowCellValue(i, "Số lượng"));
                    double newAmount = amountVAT / (1 + vat / 100.00);
                    double newPrice = newAmount / quantity;

                    gridViewTransantionDetails.SetRowCellValue(i, "Thành tiền", newAmount);
                    gridViewTransantionDetails.SetRowCellValue(i, "Đơn giá", newPrice);
                }
            }
            catch { }
        }

        private async void buttonCheckInvoiceTCT_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_connectionStringOracle))
            {
                MsgBox.Show("Chưa chọn địa chỉ kết nối Oracle");
                return;
            }
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.ActiveSheet;
            try
            {
                await VIETSEA.TBNETERP.CheckEinvoiceTCT(ws, _connectionStringOracle);
            }
            catch
            {

            }
            finally
            {

            }

        }

        private async void buttonLoadHHBanRa_Click(object sender, EventArgs e)
        {
            Task<DataTable> getDataTask = VIETSEA.TBNETERP.Data_ToKhai_BanRa(dateEditVietseaStartDate.DateTime, dateEditVietseaEndDate.DateTime);

            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet ws = wb.GetSheet("banra");

            DataTable dataTable = await getDataTask;
            if (dataTable.HasData())
            {
                dataTable.CopyToRange(ws.Range["a1"]);
                ws.Columns.AutoFit();

                ListObject listObject = ws.UsedRange.ToListObject();
                listObject.GetColumnRange("TIENHANG").ConvertToValue();
                listObject.GetColumnRange("MAVAT").ConvertToValue();
                listObject.GetColumnRange("TIENVAT").ConvertToValue();
                listObject.GetColumnRange("TONGTIEN").ConvertToValue();

                ws.UsedRange.CopyFilterData("MAVAT", "=8", wb.GetSheet("vat8").Range["a1"]);

                PivotTable pivotTable = VSTOLib.Excel.Pivot.CreatePivotTable(listObject.Range, "pivotBanRa");

                PivotField fMAVAT = pivotTable.PivotFields("MAVAT");
                fMAVAT.Orientation = XlPivotFieldOrientation.xlRowField;
                fMAVAT.Position = 1;

                pivotTable.AddDataField(pivotTable.PivotFields("TIENHANG"), "Tiền hàng", XlConsolidationFunction.xlSum);
                pivotTable.AddDataField(pivotTable.PivotFields("TIENVAT"), "Tiền vat", XlConsolidationFunction.xlSum);

                pivotTable.DataBodyRange.SetAccountingNumberFormat();

                listObject.Unlist();
                Notification.Toast.Show("Tải dữ liệu thành công!");
            }

            #region local

            #endregion
        }

        private void buttonCopyDataToXmlFile_Click(object sender, EventArgs e)
        {
            try
            {
                string xmlFile = string.Empty;
                using (OpenFileDialog openFileDialog = new OpenFileDialog()
                {
                    Filter = "XML Files (*.xml)|*.xml",
                    Title = "Chọn file tờ khai XML"
                })
                {
                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        xmlFile = openFileDialog.FileName;
                    }
                    else
                    {
                        return;
                    }
                }

                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(xmlFile);
                var PL_NQ43_GTGTNode = doc.GetElementsByTagName("PL_NQ43_GTGT");
                foreach (System.Xml.XmlNode node in PL_NQ43_GTGTNode)
                {
                    node.RemoveAll();
                }

                Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sheetVat8 = wb.GetSheet("vat8");
                ListObject table8 = sheetVat8.UsedRange.ToListObject();

                foreach (ListRow row in table8.ListRows)
                {
                    int rowNum = row.Index;
                    string tenvtu = table8.GetCell(row.Index, "TENVTU").Value2;
                    double tienhang = table8.GetCell(row.Index, "TIENHANG").Value2;

                    System.Xml.XmlElement xmlElementBangKe = doc.CreateElement("BangKeTenHHDV");
                    xmlElementBangKe.SetAttribute($"ID", $"ID_{rowNum}");

                    System.Xml.XmlElement tenHHDV = doc.CreateElement("tenHHDV");
                    tenHHDV.InnerText = tenvtu.Trim();

                    System.Xml.XmlElement giaTriHHDV = doc.CreateElement("giaTriHHDV");
                    giaTriHHDV.InnerText = tienhang.ToString();

                    System.Xml.XmlElement thueSuatTheoQuyDinh = doc.CreateElement("thueSuatTheoQuyDinh");
                    thueSuatTheoQuyDinh.InnerText = "10";

                    System.Xml.XmlElement thueSuatSauGiam = doc.CreateElement("thueSuatSauGiam");
                    thueSuatSauGiam.InnerText = "8";

                    System.Xml.XmlElement thueGTGTDuocGiam = doc.CreateElement("thueGTGTDuocGiam");
                    thueGTGTDuocGiam.InnerText = "0";

                    xmlElementBangKe.AppendChild(tenHHDV);
                    xmlElementBangKe.AppendChild(giaTriHHDV);
                    xmlElementBangKe.AppendChild(thueSuatTheoQuyDinh);
                    xmlElementBangKe.AppendChild(thueSuatSauGiam);
                    xmlElementBangKe.AppendChild(thueGTGTDuocGiam);

                    PL_NQ43_GTGTNode[0].AppendChild(xmlElementBangKe);

                }
                System.Xml.XmlElement tongCongGiaTriHHDV = doc.CreateElement("tongCongGiaTriHHDV");
                tongCongGiaTriHHDV.InnerText = "0";

                System.Xml.XmlElement tongCongThueGTGTDuocGiam = doc.CreateElement("tongCongThueGTGTDuocGiam");
                tongCongThueGTGTDuocGiam.InnerText = "0";

                PL_NQ43_GTGTNode[0].AppendChild(tongCongGiaTriHHDV);
                PL_NQ43_GTGTNode[0].AppendChild(tongCongThueGTGTDuocGiam);

                doc.Save(xmlFile);
                string xmlString = System.IO.File.ReadAllText(xmlFile);
                string newXmlString = xmlString.Replace(" xmlns=\"\"", "");

                System.Xml.XmlDocument xmlDocument = new System.Xml.XmlDocument();
                xmlDocument.LoadXml(newXmlString);
                xmlDocument.Save(xmlFile);

                table8.Unlist();
                Notification.Toast.Show("Copy dữ liệu vào file XML thành công!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }
            finally
            {

            }


        }

        private void buttonEditHTKKDataFileXml_Click(object sender, EventArgs e)
        {
            try
            {
                string directory = @"C:\Program Files (x86)\HTKK\DataFiles";
                if (!System.IO.Directory.Exists(directory))
                {
                    directory = string.Empty;
                }

                string xmlFile = string.Empty;
                using (OpenFileDialog openFileDialog = new OpenFileDialog()
                {
                    Filter = "XML Files (*.xml)|*.xml",
                    Title = "Chọn data file tờ khai XML",
                    InitialDirectory = directory
                })
                {
                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        xmlFile = openFileDialog.FileName;
                    }
                    else
                    {
                        return;
                    }
                }

                string xmlContent = System.IO.File.ReadAllText(xmlFile);
                string newContent = xmlContent.Replace("######0###", "###10###8###");
                System.Xml.XmlDocument xmlDocument = new System.Xml.XmlDocument();
                xmlDocument.LoadXml(newContent);
                xmlDocument.Save(xmlFile);
                Notification.Toast.Show("Sửa file data xml thành công!");

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }
            finally
            {

            }
        }

        private async void buttonGetRetailTran_Click(object sender, EventArgs e)
        {
            #region local funtion

            object GetValueFromTonKho(DataTable table, string mavtu, string columnName)
            {
                if (table == null)
                {
                    MessageBox.Show("null");
                }
                DataRow[] dataRows = table.Select($"[MAVTU] = '{mavtu}'", "TONCUOIKYSL DESC");
                if (dataRows.Length > 0)
                {
                    return dataRows[0][columnName];
                }
                return null;
            }

            #endregion


            string retailTranIDs = textBoxRetailTranID.GetText();
            if (string.IsNullOrEmpty(retailTranIDs))
            {
                MessageBox.Show(this, "Chưa nhập mã giao dịch");
                return;
            }

            _dataProductInfo = await ExcelAddIn1.VIETSEA.OracleServer.GetProductCodeInfo(_connectionStringOracle, (DateTime)dateEditVietseaEndDate.EditValue);
            string tranIDs = retailTranIDs.Split(',').ToSQLWhereIn();
            DataTable dataRetail = await ADO.GetDataTableFromSQLServerAsync(_connectionStringSQL, SQLquery.GetRetailTranDetail2(tranIDs));

            if (dataRetail.HasData())
            {
                gridRetailTranDetail.SafeBeginInvoke(() =>
                {
                    gridRetailTranDetail.DataSource = dataRetail;

                    gridViewRetailTranDetail.OptionsView.ShowGroupPanel = false;
                    gridViewRetailTranDetail.Columns["Đơn giá"].SetNumberFormat();
                    gridViewRetailTranDetail.Columns["Thành tiền"].SetNumberFormat();
                    gridViewRetailTranDetail.Columns["Thành tiền VAT"].SetNumberFormat();

                    gridViewRetailTranDetail.Columns["Thành tiền"].Summary.Clear();
                    gridViewRetailTranDetail.Columns["Thành tiền"].Summary.Add(new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Thành tiền", "{0:N0}"));
                    gridViewRetailTranDetail.Columns["Thành tiền VAT"].Summary.Clear();
                    gridViewRetailTranDetail.Columns["Thành tiền VAT"].Summary.Add(new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Thành tiền VAT", "{0:N0}"));
                    gridViewRetailTranDetail.OptionsView.ShowFooter = true;

                    gridViewRetailTranDetail.BestFitColumns();

                    #region conditional formating
                    gridViewRetailTranDetail.OptionsMenu.ShowConditionalFormattingItem = true;
                    gridViewRetailTranDetail.ApplyDuplicateFormating("Mã vietsea", System.Drawing.Color.LightPink);
                    #endregion

                    //add in line button gridlookupedit 
                    //set data source
                    gridViewRetailTranDetail.CellValueChanged += GridViewRetailTranDetail_CellValueChanged;

                    gridRetailTranDetail.RepositoryItems.Clear();
                    DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit repoSearchLookUpEdit = new DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit();

                    repoSearchLookUpEdit.DataSource = _dataProductInfo;
                    repoSearchLookUpEdit.DisplayMember = "MAVTU";
                    repoSearchLookUpEdit.ValueMember = "MAVTU";
                    //repoSearchLookUpEdit.EditValueChanged += RepoSearchLookUpEdit_EditValueChanged;
                    repoSearchLookUpEdit.Closed += RepoSearchLookUpEdit_Closed;
                    repoSearchLookUpEdit.Popup += RepoSearchLookUpEdit_Popup;
                    gridRetailTranDetail.RepositoryItems.Add(repoSearchLookUpEdit);
                    gridViewRetailTranDetail.Columns["Mã vietsea"].ColumnEdit = repoSearchLookUpEdit;

                    //vlookup
                    for (int i = 0; i < gridViewRetailTranDetail.RowCount; i++)
                    {
                        string goodid = gridViewRetailTranDetail.GetRowCellValue(i, "GoodID").ToString();
                        string maVietsea = VlookupMaVietsea(_bangma, goodid);

                        gridViewRetailTranDetail.SetRowCellValue(i, "Mã vietsea", maVietsea);
                        object maKho = GetValueFromTonKho(_dataProductInfo, maVietsea, "MAKHOHANG");
                        object slTon = GetValueFromTonKho(_dataProductInfo, maVietsea, "TONCUOIKYSL");
                        gridViewRetailTranDetail.SetRowCellValue(i, "Kho", maKho);
                        gridViewRetailTranDetail.SetRowCellValue(i, "SL tồn", slTon);
                    }

                    //button edit VAT
                    DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
                    repositoryItemComboBox.Items.AddRange(Public.VatRate.VatList.ToArray());
                    repositoryItemComboBox.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
                    repositoryItemComboBox.SelectedIndexChanged += RepositoryItemComboBox_SelectedIndexChanged;
                    gridRetailTranDetail.RepositoryItems.Add(repositoryItemComboBox);
                    gridViewRetailTranDetail.Columns["VAT"].ColumnEdit = repositoryItemComboBox;



                    gridViewRetailTranDetail.RowCellStyle += GridViewRetailTranDetail_RowCellStyle;
                    gridViewRetailTranDetail.BestFitColumns();
                });
            }
            else
            {
                MessageBox.Show(this, "Không có dữ liệu");
            }
        }

        private void textBoxDesiredAmount_TextChanged(object sender, EventArgs e)
        {
            if (textBoxDesiredAmount.Text.Length > 0)
            {
                double amount = System.Convert.ToDouble(textBoxDesiredAmount.Text);
                double vat = System.Convert.ToDouble(comboBoxImpVat.EditValue);
                textBoxVat.Text = Math.Round(amount * vat / 100.00, 0).ToString();
            }
            else
            {
                textBoxVat.Text = "0";
            }

        }

        private void textBoxVat_TextChanged(object sender, EventArgs e)
        {
            if (textBoxVat.Text.Length > 0)
            {
                double amount = System.Convert.ToDouble(textBoxDesiredAmount.Text);
                double vat = System.Convert.ToDouble(textBoxVat.Text);
                textBoxTotalAmount.Text = (amount + vat).ToString();
            }
        }

        private async void buttonAutoImport2_Click(object sender, EventArgs e)
        {
            string tranDate = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(ExcelAddIn1.PublicVar.fileImportNhapHang_fullPath, "MAUHOADON", "H2");
            string sohoadon = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(ExcelAddIn1.PublicVar.fileImportNhapHang_fullPath, "MAUHOADON", "A2");
            string maNCC = ExcelAddIn1.MyClass.ExcelGodClass.Func.GetValueFromWorkbookWithoutOpening(ExcelAddIn1.PublicVar.fileImportNhapHang_fullPath, "MAUHOADON", "G2");
            string invoiceDate = dateEditInvoiceDate.DateTime.ToString("dd/MM/yyyy");
            string vat = comboBoxImpVat.Text;
            string invoiceNumber = textBoxInvoiceNumber.Text.Length > 0 ? textBoxInvoiceNumber.Text : "";
            int invoiceAmount = textBoxDesiredAmount.Text.Length > 0 ? Convert.ToInt32(textBoxDesiredAmount.Text) : 0;
            int invoiceVat = textBoxVat.Text.Length > 0 ? Convert.ToInt32(textBoxVat.Text) : 0;
            string copies = numericUpDownNumberOfPrint.Text;
            string makho = comboBoxKhoNhap.Text;
            string printType = comboBoxPrintType.Text;

            try
            {
                await Task.Run(async () =>
                {
                    AutoTest.WinAppDriver.Vietsea.Flows.OpenVietsea();
                    AutoTest.WinAppDriver.Vietsea.Flows.ImportInputInvoices(tranDate, invoiceNumber, invoiceDate, maNCC, vat, invoiceAmount, invoiceVat, copies, makho, @"C:\ExcelVSTOAddin\FileImportNhapHang.xls", printType);

                    await GetSoChiTiet3311();
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }
            finally
            {
                WAD.Cmd.TearDown();
                this.SafeInvoke(() => this.BringToFront());
                xtraTabControl1.SafeInvoke(() => { xtraTabControl1.SelectedTabPage = xtraTabPageCongNo; });
            }

        }

        private void gridLookUpEditMaNCCVietsea_Properties_Click(object sender, EventArgs e)
        {
            gridLookUpEditMaNCCVietsea.SelectAll();
        }

        private void gridLookUpEditMaNCCVietsea_Properties_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gridView = gridLookUpEditMaNCCVietsea.Properties.View;
                int rowHandle = gridView.FocusedRowHandle;
                if (rowHandle == int.MinValue)
                {
                    return;
                }

                richTextBoxTenNCCVietsea.Text = gridView.GetRowCellValue(rowHandle, "TENTOCHU").ToString();
                textBoxMSTVietsea.Text = gridView.GetRowCellValue(rowHandle, "MASOTHUE").ToString();
            }
            catch
            {
            }
        }

        private async Task FillTenToChuMaSoThue(UserClass.Invoice invoiceInfo)
        {
            try
            {
                UserClass.Supplier vietseaKhachHang = await VIETSEA.TBNETERP.GetVietseaKhachHang(_connectionStringOracle, invoiceInfo.MST).ConfigureAwait(false);
                gridLookUpEditMaNCCVietsea.Text = vietseaKhachHang.ID;
                richTextBoxTenNCCVietsea.Text = vietseaKhachHang.Name;
                textBoxMSTVietsea.Text = vietseaKhachHang.MST;

                textBoxDesiredAmount.Text = invoiceInfo.Subtotal.ToString();
                textBoxVat.Text = invoiceInfo.VatAmount.ToString();
                //dateEditInvoiceDate.EditValue = DateTime.ParseExact(invoiceInfo.Date, "dd/MM/yyyy", new System.Globalization.CultureInfo("vi-VN"));
                dateEditInvoiceDate.EditValue = invoiceInfo.Date;
                textBoxSerialNumber.Text = invoiceInfo.SerialNumber;
                textBoxInvoiceNumber.Text = invoiceInfo.InvoiceNumber;
            }
            catch (Exception ex)
            {

            }
        }

        private async Task<bool> FillCustomerID(string mst)
        {
            List<string> customerIDs = await ThanhCong.Database.Data.GetCustomerIDList(_connectionStringSQL, mst).ConfigureAwait(false);
            if (customerIDs == null)
            {
                MessageBox.Show($"Không tìm được customerID tương ứng với mst {mst}");
                return false;
            }

            textEditCustomerIDs.SafeInvoke(() =>
            {
                textEditCustomerIDs.EditValue = string.Join(",", customerIDs.ToArray());
            });

            return true;
        }

        private void searchCustomers_Properties_KeyDown(object sender, KeyEventArgs e)
        {
            DevExpress.XtraEditors.Popup.PopupSearchLookUpEditForm popup = sender as DevExpress.XtraEditors.Popup.PopupSearchLookUpEditForm;
            if (e.KeyCode == Keys.Enter)
            {
                popup.OwnerEdit.ClosePopup();
            }
        }

        private void buttonCreateImportToPublicFolder_Click(object sender, EventArgs e)
        {
            if (!CheckMaVietseaBeforeCreateImport())
            {
                MessageBox.Show(this, "Có dòng chưa có mã hàng", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string serialNumber = textBoxSerialNumber.Text;
            string invoiceNumber = serialNumber.Contains("/") ? textBoxInvoiceNumber.Text.PadLeft(7, '0') : textBoxInvoiceNumber.Text;
            string makhachhang = gridLookUpEditMaNCCVietsea.EditValue.ToString();
            System.DateTime trandate = dateEditTranDateVietsea.DateTime >= dateEditInvoiceDate.DateTime ? dateEditTranDateVietsea.DateTime : dateEditInvoiceDate.DateTime;
            string date = "'" + trandate.ToString("dd/MM/yyyy");

            if (VIETSEA.OracleServer.CheckIfInvoiceExist(makhachhang, serialNumber, invoiceNumber, out string userTao))
            {
                System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
                stringBuilder.AppendLine($"Đã tồn tại hóa đơn");
                stringBuilder.AppendLine($"Mã khách hàng: {makhachhang}");
                stringBuilder.AppendLine($"Số hóa đơn: {invoiceNumber}");
                stringBuilder.AppendLine($"Ký hiệu hóa đơn: {serialNumber}");
                stringBuilder.AppendLine($"Người tạo: {userTao}");
                string text = stringBuilder.ToString();
                MessageBox.Show(text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            UserClass.InvoiceInfoPackage package = new UserClass.InvoiceInfoPackage();
            package.TranDate = date;
            package.MaNCC = gridLookUpEditMaNCCVietsea.EditValue.ToString();
            package.VatRate = comboBoxImpVat.Text;
            package.MaKhoNhap = comboBoxKhoNhap.Text;
            package.InvoiceAmount = System.Convert.ToDouble(textBoxDesiredAmount.Text);
            package.InvoiceVat = System.Convert.ToDouble(textBoxVat.Text);
            package.InvoiceDate = dateEditInvoiceDate.DateTime.ToString("dd/MM/yyyy");
            package.InvoiceSerialNumber = textBoxSerialNumber.Text;
            package.InvoiceNumber = textBoxInvoiceNumber.Text;
            package.PrintCopies = numericUpDownNumberOfPrint.Value.ToString();
            package.PrintType = comboBoxPrintType.Text;

            Module1.SpeedUpCode(true);
            //Workbook excelFile = VIETSEA.ExcelFile.ImportNhapHang.CreateExcelWorkbook(package);
            Workbook excelFile = VIETSEA.ExcelFile.ImportNhapHang.CreateImportWorkbook(package);

            Worksheet sheetData = excelFile.GetSheet("MAUHOADON");
            for (int rowHandle = 0; rowHandle < gridViewTemp.RowCount; rowHandle++)
            {
                int excelRow = rowHandle + 2;
                sheetData.Range["a" + excelRow].Value = "'" + invoiceNumber;
                sheetData.Range["b" + excelRow].Value = gridViewTemp.GetRowCellValue(rowHandle, Mvvm.Models.TableColumns.GridTransDetail.VietseaCode).ToString();
                sheetData.Range["c" + excelRow].Value = gridViewTemp.GetRowCellValue(rowHandle, Mvvm.Models.TableColumns.GridTransDetail.GoodName).ToString();
                sheetData.Range["d" + excelRow].Value = gridViewTemp.GetRowCellValue(rowHandle, Mvvm.Models.TableColumns.GridTransDetail.Quantity).ToString();
                sheetData.Range["e" + excelRow].Value = gridViewTemp.GetRowCellValue(rowHandle, Mvvm.Models.TableColumns.GridTransDetail.Price).ToString();
                sheetData.Range["f" + excelRow].Value = string.Empty;
                sheetData.Range["g" + excelRow].Value = makhachhang;
                //sheetData.Range["h" + excelRow].Value = date;
                sheetData.Range["h" + excelRow].Value = "'" + Flows._dumbDate;
                sheetData.Range["i" + excelRow].Value = serialNumber;
            }

            string path = Config.Manager.GetValue(Config.ID.PublicImportFolder);
            string filename = System.IO.Path.Combine(path, $"{invoiceNumber} - {makhachhang}.xls");
            excelFile.SaveOverWrite(filename, 56);
            excelFile.Close();

            Module1.SpeedUpCode(false);
            Notification.Toast.Show($"Tạo file import thành công! Đường dẫn: {path}");
            this.BringToFront();

        }

        private bool CheckMaVietseaBeforeCreateImport()
        {
            for (int i = 0; i < gridViewTemp.RowCount; i++)
            {
                string ma = gridViewTemp.GetRowCellValue(i, Mvvm.Models.TableColumns.GridTransDetail.VietseaCode).ToString();
                if (ma == "#N/A")
                {
                    return false;
                }
            }

            return true;
        }

        private bool _stopWorking = false;

        private async void buttonStartWorking_Click(object sender, EventArgs e)
        {
            buttonStartWorking.Enabled = false;
            _stopWorking = false;
            string fileToWork = string.Empty;

            try
            {
                await Task.Run(async () =>
                {
                    Notification.Wait.Show("Working...");
                    while (!_stopWorking)
                    {
                        fileToWork = GetFileNameToWork();
                        if (!string.IsNullOrEmpty(fileToWork))
                        {
                            Notification.Wait.Show($"Working...{fileToWork}");
                            AutoTest.WinAppDriver.Vietsea.Flows.OpenVietsea();
                            await AutoTest.WinAppDriver.Vietsea.Flows.ImportInputInvoicesByAutoWorker(fileToWork);

                            System.IO.File.Delete(fileToWork);
                            Notification.Toast.Show($"Xóa thành công file: {fileToWork}");
                        }

                        await System.Threading.Tasks.Task.Delay(TimeSpan.FromSeconds(5));
                    }
                    Notification.Wait.Close();
                    buttonStartWorking.InvokeSetEnable(true);
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                VSTOLib.IO.FileAndFolder.Delete(fileToWork);
                Notification.Wait.Close();
                buttonStartWorking.InvokeSetEnable(true);
            }
        }
        private string GetFileNameToWork()
        {
            string path = Config.Manager.GetValue(Config.ID.PublicImportFolder);
            List<string> files = new List<string>(System.IO.Directory.EnumerateFiles(path));

            List<string> excelFiles = new List<string>();
            foreach (var item in files)
            {
                if (isXlsFile(item))
                {
                    excelFiles.Add(item);
                }
                else
                {
                    System.IO.File.Delete(item);
                }
            }

            if (files.Count == 0)
            {
                Notification.Wait.Show($"{path} no file found");
                return string.Empty;
            }

            DateTime minCreationTime = DateTime.MaxValue;
            string minFilename = string.Empty;

            foreach (string filename in excelFiles)
            {
                //MessageBox.Show(filename);
                DateTime creationTime = System.IO.File.GetCreationTime(filename);
                if (minCreationTime == DateTime.MaxValue)
                {
                    minCreationTime = creationTime;
                }
                if (creationTime <= minCreationTime)
                {
                    minFilename = filename;
                }
            }

            return minFilename;
        }

        private bool isXlsFile(string path)
        {
            return Path.GetExtension(path) == ".xls";
        }

        private void buttonStopWorking_Click(object sender, EventArgs e)
        {
            buttonStartWorking.InvokeSetEnable(true);
            _stopWorking = true;
            Notification.Toast.Show("Đã dừng");
            WAD.Cmd.TearDown();
        }

        private async void buttonDownloadihoadonPDF_Click(object sender, EventArgs e)
        {
            string downloadFolder = VSTOLib.IO.FolderBrowser.SelectFolder("Chọn thư mục lưu file hóa đơn điện tử PDF");
            if (string.IsNullOrEmpty(downloadFolder))
            {
                return;
            }

            try
            {

                await Task.Run(() =>
                {
                    AutoTest.Selenium.EFY.ihoadon.DownloadInvoicePDF(dateEditVietseaStartDate.DateTime, dateEditVietseaEndDate.DateTime, downloadFolder);
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }




        }

        private DataTable CreateTable()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(Public.TableCheckInvoice.NgayHoaDon, typeof(string));
            dataTable.Columns.Add(Public.TableCheckInvoice.KyHieu, typeof(string));
            dataTable.Columns.Add(Public.TableCheckInvoice.SoHD, typeof(string));
            dataTable.Columns.Add(Public.TableCheckInvoice.MST, typeof(string));
            dataTable.Columns.Add(Public.TableCheckInvoice.Ten, typeof(string));
            dataTable.Columns.Add(Public.TableCheckInvoice.TienHang, typeof(double));
            dataTable.Columns.Add(Public.TableCheckInvoice.TienThue, typeof(double));
            dataTable.Columns.Add(Public.TableCheckInvoice.TongTien, typeof(double));
            dataTable.Columns.Add(Public.TableCheckInvoice.TrangThai, typeof(string));
            dataTable.Columns.Add(Public.TableCheckInvoice.DaNhap, typeof(string));
            dataTable.Columns.Add(Public.TableCheckInvoice.GhiChu, typeof(string));
            dataTable.Columns.Add(Public.TableCheckInvoice.ChinhXac, typeof(string));

            return dataTable;
        }
        private void FillDataToGrid(ListObject listobject, DataTable dataTable)
        {
            foreach (ListRow row in listobject.ListRows)
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow[Public.TableCheckInvoice.NgayHoaDon] = row.GetCell(UserClass.DanhSachHoaDon.NgayLap).GetValue<string>();
                dataRow[Public.TableCheckInvoice.KyHieu] = row.GetCell(UserClass.DanhSachHoaDon.KyHieuMauSo).GetValue<string>() + row.GetCell(UserClass.DanhSachHoaDon.KyHieuHoaDon).GetValue<string>();
                dataRow[Public.TableCheckInvoice.SoHD] = row.GetCell(UserClass.DanhSachHoaDon.SoHoaDon).GetValue<string>();
                dataRow[Public.TableCheckInvoice.MST] = row.GetCell(UserClass.DanhSachHoaDon.MSTNguoiBan).GetValue<string>();
                dataRow[Public.TableCheckInvoice.Ten] = row.GetCell(UserClass.DanhSachHoaDon.TenNguoiBan).GetValue<string>();
                dataRow[Public.TableCheckInvoice.TienThue] = row.GetCell(UserClass.DanhSachHoaDon.TongTienThue).GetValue<string>().ConvertFromVietnameseFormatToNumber<double>();
                dataRow[Public.TableCheckInvoice.TongTien] = row.GetCell(UserClass.DanhSachHoaDon.TongTienThanhToan).GetValue<string>().ConvertFromVietnameseFormatToNumber<double>();
                dataRow[Public.TableCheckInvoice.TienHang] = Convert.ToDouble(dataRow[Public.TableCheckInvoice.TongTien].ToString()) - Convert.ToDouble(dataRow[Public.TableCheckInvoice.TienThue].ToString());
                dataRow[Public.TableCheckInvoice.TrangThai] = row.GetCell(UserClass.DanhSachHoaDon.TrangThaiHoaDon).GetValue<string>();

                dataTable.Rows.Add(dataRow);
            }

            gridInvoiceData.DataSource = dataTable;
            gridViewInvoiceData.Columns[Public.TableCheckInvoice.TienHang].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
            gridViewInvoiceData.Columns[Public.TableCheckInvoice.TienThue].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
            gridViewInvoiceData.Columns[Public.TableCheckInvoice.TongTien].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
            gridViewInvoiceData.SetBestFitColumns(0,
                //Global.TableCheckInvoice.NgayHoaDon,
                //Global.TableCheckInvoice.KyHieu,
                //Global.TableCheckInvoice.SoHD,
                //Global.TableCheckInvoice.MST,
                Public.TableCheckInvoice.TienHang,
                Public.TableCheckInvoice.TienThue,
                Public.TableCheckInvoice.TongTien,
                //Global.TableCheckInvoice.TrangThai,
                //Global.TableCheckInvoice.DaNhap,
                Public.TableCheckInvoice.GhiChu,
                Public.TableCheckInvoice.ChinhXac);

            gridViewInvoiceData.BestFitColumns();

            gridViewInvoiceData.OptionsView.ShowAutoFilterRow = true;

            gridViewInvoiceData.OptionsView.ShowFooter = true;
            DevExpress.XtraGrid.GridColumnSummaryItem summaryItem = new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, Public.TableCheckInvoice.TienThue, "{0:N0}");
            gridViewInvoiceData.Columns[Public.TableCheckInvoice.TienThue].Summary.Clear();
            gridViewInvoiceData.Columns[Public.TableCheckInvoice.TienThue].Summary.Add(summaryItem);


        }
        private void CheckInvoice()
        {
            progressBar1.Visible = true;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = gridViewInvoiceData.RowCount;
            progressBar1.Value = 0;

            gridInvoiceData.SafeInvoke(async () =>
            {
                for (int rowHandle = 0; rowHandle < gridViewInvoiceData.RowCount; rowHandle++)
                {
                    string kyhieu = gridViewInvoiceData.GetRowCellValue(rowHandle, Public.TableCheckInvoice.KyHieu).ToString();
                    string mst = gridViewInvoiceData.GetRowCellValue(rowHandle, Public.TableCheckInvoice.MST).ToString();
                    string soHD = gridViewInvoiceData.GetRowCellValue(rowHandle, Public.TableCheckInvoice.SoHD).ToString();

                    await CheckDaNhap(kyhieu, mst, soHD, rowHandle);
                    await CheckGhiChuHoaDon(kyhieu, mst, soHD, rowHandle);

                    progressBar1.Value = rowHandle;
                }
            });

            progressBar1.Visible = false;

            async Task CheckGhiChuHoaDon(string kyhieu, string mst, string soHD, int rowHandle)
            {
                try
                {
                    //check ghi chú hóa đơn
                    string q = $"SELECT * FROM {Public.OneDrive.HoaDonMuaVao.TableName} " +
                    $"WHERE {Public.OneDrive.HoaDonMuaVao.KYHIEU} = '{kyhieu}' " +
                    $"AND {Public.OneDrive.HoaDonMuaVao.MST} = '{mst}' " +
                    $"AND {Public.OneDrive.HoaDonMuaVao.SOHOADON} = '{soHD}' ";

                    DataTable ghiChu = await ADO.GetDataFromAccessFileAsync(_AccessFileOneDriveDatabase, q).ConfigureAwait(false);
                    if (ghiChu.HasData())
                    {
                        gridViewInvoiceData.SetRowCellValue(rowHandle, Public.TableCheckInvoice.GhiChu, ghiChu.Rows[0]["GHICHU"]);
                    }
                }
                catch
                {

                }
            }

            async Task CheckDaNhap(string kyhieu, string mst, string soHD, int rowHandle)
            {
                double tienHang = Convert.ToDouble(gridViewInvoiceData.GetRowCellValue(rowHandle, Public.TableCheckInvoice.TienHang).ToString());
                double tienThue = Convert.ToDouble(gridViewInvoiceData.GetRowCellValue(rowHandle, Public.TableCheckInvoice.TienThue).ToString());
                double tongTien = Convert.ToDouble(gridViewInvoiceData.GetRowCellValue(rowHandle, Public.TableCheckInvoice.TongTien).ToString());

                DataTable invoice = await VIETSEA.TBNETERP.GetInvoice(_connectionStringOracle, kyhieu, mst, soHD).ConfigureAwait(false);
                if (invoice != null && invoice.Rows.Count > 0)
                {
                    gridViewInvoiceData.SetRowCellValue(rowHandle, Public.TableCheckInvoice.DaNhap, invoice.Rows.Count.ToString());

                    if (invoice.Rows.Count == 1)
                    {
                        if (tienHang == Convert.ToDouble(invoice.Rows[0]["SUMTIENHANG"].ToString())
                        && tienThue == Convert.ToDouble(invoice.Rows[0]["SUMTIENVAT"].ToString())
                        && tongTien == Convert.ToDouble(invoice.Rows[0]["TOTAL"].ToString()))
                        {
                            gridViewInvoiceData.SetRowCellValue(rowHandle, Public.TableCheckInvoice.ChinhXac, "Yes");
                        }
                        else
                        {
                            gridViewInvoiceData.SetRowCellValue(rowHandle, Public.TableCheckInvoice.ChinhXac, "No");
                        }
                    }
                }
                else
                {
                    gridViewInvoiceData.SetRowCellValue(rowHandle, Public.TableCheckInvoice.DaNhap, 0);
                }
            }
        }

        private void buttonLoadInvoiceDataFromExcelSheet_Click(object sender, EventArgs e)
        {
            try
            {
                #region Main
                buttonLoadInvoiceDataFromExcelSheet.InvokeSetEnable(false);
                string[] files = GetFiles();

                if (files == null || files.Length == 0)
                {
                    MessageBox.Show("Chưa chọn file danh sách hóa đơn");
                    return;
                }

                gridInvoiceData.SafeInvoke(() =>
                {
                    gridViewInvoiceData.OptionsSelection.MultiSelect = true;
                    gridViewInvoiceData.ClearColumnsFilter();
                    gridInvoiceData.DataSource = null;
                });

                DataTable dataTable = CreateTable();

                foreach (string file in files)
                {
                    Workbook wb = Globals.ThisAddIn.Application.Workbooks.Open(file);
                    Worksheet sheet = wb.GetSheet("sheet 1");

                    DataTable dataSource = GetMainDataRange(sheet).ConvertToDataTable();
                    FillExcelDataToDataTable(dataSource, dataTable);

                    wb.Close(SaveChanges: false);

                    this.SafeInvoke(() =>
                    {
                        this.BringToFront();
                    });
                }

                SetDataSource(dataTable);
                CheckInvoice();
                AddButtonEdit();
                #endregion

            }
            catch (Exception ex)
            {
                this.SafeInvoke(() =>
                {
                    MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                });
            }
            finally
            {
                buttonLoadInvoiceDataFromExcelSheet.InvokeSetEnable(true);
            }

            #region local

            Range GetMainDataRange(Worksheet worksheet)
            {
                Range foundRange = worksheet.Cells.Find("Ký hiệu mẫu số", LookIn: XlFindLookIn.xlValues, LookAt: XlLookAt.xlPart);
                if (foundRange == null)
                {
                    return null;
                }

                Range firstCell = worksheet.Cells[foundRange.Row, 1];
                Range output = worksheet.GetRectangleRange(firstCell, "a");

                return output;
            }

            string[] GetFiles()
            {
                string[] result = null;
                using (OpenFileDialog openFileDialog = new OpenFileDialog()
                {
                    Multiselect = true
                })
                {
                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        result = openFileDialog.FileNames;
                    }
                }

                return result;
            }
            void FillExcelDataToDataTable(DataTable dataSource, DataTable destinationDataTable)
            {
                //destinationDataTable.Columns[Public.TableCheckInvoice.NgayHoaDon].DataType = typeof(DateTime);

                foreach (DataRow dataSourceRow in dataSource.Rows)
                {
                    DataRow dataRow = destinationDataTable.NewRow();
                    //dataRow[Public.TableCheckInvoice.NgayHoaDon] = row.GetCell(UserClass.DanhSachHoaDon.NgayLap).GetValue<string>();
                    //dataRow[Public.TableCheckInvoice.KyHieu] = row.GetCell(UserClass.DanhSachHoaDon.KyHieuMauSo).GetValue<string>() + row.GetCell(UserClass.DanhSachHoaDon.KyHieuHoaDon).GetValue<string>();
                    //dataRow[Public.TableCheckInvoice.SoHD] = row.GetCell(UserClass.DanhSachHoaDon.SoHoaDon).GetValue<string>();
                    //dataRow[Public.TableCheckInvoice.MST] = row.GetCell(UserClass.DanhSachHoaDon.MSTNguoiBan).GetValue<string>();
                    //dataRow[Public.TableCheckInvoice.Ten] = row.GetCell(UserClass.DanhSachHoaDon.TenNguoiBan).GetValue<string>();
                    //dataRow[Public.TableCheckInvoice.TienThue] = row.GetCell(UserClass.DanhSachHoaDon.TongTienThue).GetValue<string>().ConvertFromVietnameseFormatToNumber<double>();
                    //dataRow[Public.TableCheckInvoice.TongTien] = row.GetCell(UserClass.DanhSachHoaDon.TongTienThanhToan).GetValue<string>().ConvertFromVietnameseFormatToNumber<double>();
                    //dataRow[Public.TableCheckInvoice.TienHang] = Convert.ToDouble(dataRow[Public.TableCheckInvoice.TongTien].ToString()) - Convert.ToDouble(dataRow[Public.TableCheckInvoice.TienThue].ToString());
                    //dataRow[Public.TableCheckInvoice.TrangThai] = row.GetCell(UserClass.DanhSachHoaDon.TrangThaiHoaDon).GetValue<string>();

                    dataRow[Public.TableCheckInvoice.NgayHoaDon] = DateTime.ParseExact(dataSourceRow[UserClass.DanhSachHoaDon.NgayLap].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dataRow[Public.TableCheckInvoice.KyHieu] = dataSourceRow[UserClass.DanhSachHoaDon.KyHieuMauSo].ToString() + dataSourceRow[UserClass.DanhSachHoaDon.KyHieuHoaDon].ToString();
                    dataRow[Public.TableCheckInvoice.SoHD] = dataSourceRow[UserClass.DanhSachHoaDon.SoHoaDon].ToString();
                    dataRow[Public.TableCheckInvoice.MST] = dataSourceRow[UserClass.DanhSachHoaDon.MSTNguoiBan].ToString();
                    dataRow[Public.TableCheckInvoice.Ten] = dataSourceRow[UserClass.DanhSachHoaDon.TenNguoiBan].ToString();
                    dataRow[Public.TableCheckInvoice.TienHang] = VSTOLib.Converter.General.ConvertTo<double>(dataSourceRow[UserClass.DanhSachHoaDon.TongTienThanhToan]) - VSTOLib.Converter.General.ConvertTo<double>(dataSourceRow[UserClass.DanhSachHoaDon.TongTienThue]);
                    dataRow[Public.TableCheckInvoice.TienThue] = VSTOLib.Converter.General.ConvertTo<double>(dataSourceRow[UserClass.DanhSachHoaDon.TongTienThue]);
                    dataRow[Public.TableCheckInvoice.TongTien] = VSTOLib.Converter.General.ConvertTo<double>(dataSourceRow[UserClass.DanhSachHoaDon.TongTienThanhToan]);
                    dataRow[Public.TableCheckInvoice.TrangThai] = dataSourceRow[UserClass.DanhSachHoaDon.TrangThaiHoaDon].ToString();

                    destinationDataTable.Rows.Add(dataRow);
                }
            }

            void SetDataSource(DataTable dataTable)
            {
                gridInvoiceData.DataSource = dataTable;
                gridViewInvoiceData.Columns[Public.TableCheckInvoice.TienHang].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                gridViewInvoiceData.Columns[Public.TableCheckInvoice.TienThue].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                gridViewInvoiceData.Columns[Public.TableCheckInvoice.TongTien].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                gridViewInvoiceData.SetBestFitColumns(0,
                    //Global.TableCheckInvoice.NgayHoaDon,
                    //Global.TableCheckInvoice.KyHieu,
                    //Global.TableCheckInvoice.SoHD,
                    //Global.TableCheckInvoice.MST,
                    Public.TableCheckInvoice.TienHang,
                    Public.TableCheckInvoice.TienThue,
                    Public.TableCheckInvoice.TongTien,
                    //Global.TableCheckInvoice.TrangThai,
                    //Global.TableCheckInvoice.DaNhap,
                    Public.TableCheckInvoice.GhiChu,
                    Public.TableCheckInvoice.ChinhXac);

                gridViewInvoiceData.BestFitColumns();
                gridViewInvoiceData.OptionsView.ShowAutoFilterRow = true;
                gridViewInvoiceData.OptionsView.ShowFooter = true;
                gridViewInvoiceData.Columns[Public.TableCheckInvoice.TienThue].AddSumaryItem(DevExpress.Data.SummaryItemType.Sum);
                gridViewInvoiceData.Columns[Public.TableCheckInvoice.SoHD].AddSumaryItem(DevExpress.Data.SummaryItemType.Count);

            }

            void AddButtonEdit()
            {
                DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit buttonEditAdd = gridViewInvoiceData.AddButtonEdit(
                    Public.TableCheckInvoice.DaNhap,
                    new DevExpress.XtraEditors.Controls.EditorButton() { Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Plus });
                buttonEditAdd.ButtonClick += ButtonEditAdd_ButtonClick;

                DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit buttonEditSubmitGhiChu = gridViewInvoiceData.AddButtonEdit(
                   Public.TableCheckInvoice.GhiChu,
                   new DevExpress.XtraEditors.Controls.EditorButton() { Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Up });

                buttonEditSubmitGhiChu.ButtonClick += ButtonEditSubmitGhiChu_ButtonClick;
            }
            #endregion
        }

        private async void ButtonEditSubmitGhiChu_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gridView = DevExpressExtensions.GetGridView(sender);
            DataRow row = gridView.GetDataRow(gridView.FocusedRowHandle);
            gridView.FocusedRowHandle -= 1;
            gridView.FocusedRowHandle += 1;

            UserClass.Invoice invoice = new UserClass.Invoice()
            {
                Date = row[Public.TableCheckInvoice.NgayHoaDon].ToString(),
                SerialNumber = row[Public.TableCheckInvoice.KyHieu].ToString(),
                InvoiceNumber = row[Public.TableCheckInvoice.SoHD].ToString(),
                MST = row[Public.TableCheckInvoice.MST].ToString(),
                CustomerName = row[Public.TableCheckInvoice.Ten].ToString(),
                Subtotal = Convert.ToDouble(row[Public.TableCheckInvoice.TienHang].ToString()),
                VatAmount = Convert.ToDouble(row[Public.TableCheckInvoice.TienThue].ToString()),
                TotalAmount = Convert.ToDouble(row[Public.TableCheckInvoice.TongTien].ToString()),
                GhiChu = row[Public.TableCheckInvoice.GhiChu].ToString()
            };

            await UpdateGhiChuHoaDon(invoice);


        }

        private async Task UpdateGhiChuHoaDon(UserClass.Invoice invoice)
        {
            try
            {
                string accessFile = _AccessFileOneDriveDatabase;
                if (accessFile.IsEmpty())
                {
                    return;
                }

                //string update = $"UPDATE {Global.OneDrive.HoaDonMuaVao.TableName} SET" +
                //        $" {Global.OneDrive.HoaDonMuaVao.NGAYHOADON} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.KYHIEU} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.SOHOADON} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.MST} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.TEN} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.TIENHANG} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.TIENTHUE} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.TONGTIEN} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.TRANGTHAI} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.DANHAP} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.GHICHU} = NULL," +
                //        $" {Global.OneDrive.HoaDonMuaVao.CHINHXAC} = NULL" +

                //        $" WHERE {Global.OneDrive.HoaDonMuaVao.SOHOADON} = '{invoice.InvoiceNumber}'" +
                //        $" AND {Global.OneDrive.HoaDonMuaVao.KYHIEU} = '{invoice.SerialNumber}'" +
                //        $" AND {Global.OneDrive.HoaDonMuaVao.MST} = '{invoice.MST}'";

                string delete = $"DELETE * FROM {Public.OneDrive.HoaDonMuaVao.TableName} " +
                        $" WHERE {Public.OneDrive.HoaDonMuaVao.SOHOADON} = '{invoice.InvoiceNumber}'" +
                        $" AND {Public.OneDrive.HoaDonMuaVao.KYHIEU} = '{invoice.SerialNumber}'" +
                        $" AND {Public.OneDrive.HoaDonMuaVao.MST} = '{invoice.MST}'";
                await ADO.ExecuteQueryAccessFileAsync(accessFile, delete);

                string insert = $"INSERT INTO {Public.OneDrive.HoaDonMuaVao.TableName} VALUES " +
                    $"(" +
                    $"'{invoice.Date}'," +
                    $"'{invoice.SerialNumber}'," +
                    $"'{invoice.InvoiceNumber}'," +
                    $"'{invoice.MST}'," +
                    $"'{invoice.CustomerName}'," +
                    $"'{invoice.Subtotal}'," +
                    $"'{invoice.VatAmount}'," +
                    $"'{invoice.TotalAmount}'," +
                    $"'{string.Empty}'," +
                    $"'{string.Empty}'," +
                    $"'{invoice.GhiChu}'," +
                    $"'{string.Empty}'" +
                    $");";

                await ADO.ExecuteQueryAccessFileAsync(accessFile, insert).ConfigureAwait(false);

                Notification.Toast.Show("Cập nhật ghi chú hóa đơn thành công");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {

            }
        }

        private async void ButtonEditAdd_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gridView = DevExpressExtensions.GetGridView(sender);
            DataRow row = gridView.GetDataRow(gridView.FocusedRowHandle);

            UserClass.Invoice invoiceInfo = new UserClass.Invoice()
            {
                MST = row[Public.TableCheckInvoice.MST].ToString(),
                Date = row[Public.TableCheckInvoice.NgayHoaDon].ToString(),
                SerialNumber = row[Public.TableCheckInvoice.KyHieu].ToString(),
                InvoiceNumber = row[Public.TableCheckInvoice.SoHD].ToString(),
                Subtotal = Convert.ToDouble(row[Public.TableCheckInvoice.TienHang].ToString()),
                VatAmount = Convert.ToDouble(row[Public.TableCheckInvoice.TienThue].ToString()),
                TotalAmount = Convert.ToDouble(row[Public.TableCheckInvoice.TongTien].ToString()),
            };

            await FillInvoiceInfoToFormNhapHang(invoiceInfo);
        }

        private async Task FillInvoiceInfoToFormNhapHang(UserClass.Invoice invoiceInfo)
        {
            xtraTabControl1.SelectedTabPage = xtraTabPageNhapHang;

            try
            {
                UserClass.Supplier vietseaKhachHang = await VIETSEA.TBNETERP.GetVietseaKhachHang(_connectionStringOracle, invoiceInfo.MST).ConfigureAwait(false);
                if (vietseaKhachHang == null)
                {
                    this.SafeInvoke(() =>
                    {
                        MessageBox.Show(this, "Không tồn tại mã số thuế này");
                    });
                    return;
                }

                await FillTenToChuMaSoThue(invoiceInfo);
                await FillCustomerID(invoiceInfo.MST);

                buttonSearchTransactions_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"{ex}");
            }
            finally
            {
                //string ID = ThanhCong.Database.Tables.Customers.Fields.CustomerID;
            }

        }

        private async void buttonUpdateStatusCongNo331_Click(object sender, EventArgs e)
        {
            try
            {
                string excelFile = _AccessFileOneDriveDatabase;
                if (excelFile.IsEmpty())
                {
                    return;
                }

                string MANHACUNGCAP = gridLookUpEditMaNCCVietsea.Text;
                string TENNHACUNGCAP = richTextBoxTenNCCVietsea.Text;
                string THOIGIANCHECK = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                string GHICHU = richTextBoxNewStatusTheoDoiCongNo.Text;
                DateTime NGAYSODU = System.Convert.ToDateTime(gridViewSoChiTietTKVietsea.GetRowCellValue(gridViewSoChiTietTKVietsea.RowCount - 1, "NGAYHACHTOAN"));
                double DUNO = System.Convert.ToDouble(gridViewSoChiTietTKVietsea.GetRowCellValue(gridViewSoChiTietTKVietsea.RowCount - 1, "SDDK_NO"));
                double DUCO = System.Convert.ToDouble(gridViewSoChiTietTKVietsea.GetRowCellValue(gridViewSoChiTietTKVietsea.RowCount - 1, "SDDK_CO"));


                string delete = $"DELETE * FROM {Public.OneDrive.CongNoNCC.TableName} " +
                    $"WHERE {Public.OneDrive.CongNoNCC.MANHACUNGCAP} = '{MANHACUNGCAP}';";
                await ADO.ExecuteQueryAccessFileAsync(excelFile, delete);

                if (!GHICHU.IsEmpty())
                {
                    string insert = $"INSERT INTO {Public.OneDrive.CongNoNCC.TableName} " +
                        $"VALUES ('{MANHACUNGCAP}','{TENNHACUNGCAP}','{THOIGIANCHECK}','{GHICHU}','{NGAYSODU}','{DUNO}','{DUCO}');";
                    await ADO.ExecuteQueryAccessFileAsync(excelFile, insert);
                }

                Notification.Toast.Show("Cập nhật trạng thái công nợ thành công");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void simpleButton1_Click(object sender, EventArgs e)
        {
            ListObject listObject = null;
            try
            {
                Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
                DataTable dt = await VIETSEA.TBNETERP.GetDTGV_QUYETTOAN(_connectionStringOracle, dateEditVietseaStartDate.DateTime, dateEditVietseaEndDate.DateTime);
                if (dt == null)
                {
                    MessageBox.Show("Không có dữ liệu");
                }

                Worksheet worksheet = workbook.GetNewSheet($"DT-GV {dateEditVietseaStartDate.DateTime.ToString("dd-MM-yyyy")}--{dateEditVietseaEndDate.DateTime.ToString("dd-MM-yyyy")}");
                dt.CopyToRange(worksheet.Range["a1"]);
                listObject = worksheet.UsedRange.ToListObject();
                listObject.GetColumnDataBodyRange("SUM_SOLUONG").ConvertToValue();
                listObject.GetColumnDataBodyRange("SUM_SOLUONG").SetAccountingNumberFormat();

                listObject.GetColumnDataBodyRange("SUM_TIENHANG").ConvertToValue();
                listObject.GetColumnDataBodyRange("SUM_TIENHANG").SetAccountingNumberFormat();

                listObject.GetColumnDataBodyRange("SUM_GIAVONHANGBAN").ConvertToValue();
                listObject.GetColumnDataBodyRange("SUM_GIAVONHANGBAN").SetAccountingNumberFormat();

                listObject.GetColumnDataBodyRange("CHENH_LECH").ConvertToValue();
                listObject.GetColumnDataBodyRange("CHENH_LECH").SetAccountingNumberFormat();

                Notification.Toast.Show("Load dữ liệu thành công");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                listObject.Unlist();
            }
        }

        private async void simpleButton2_Click(object sender, EventArgs e)
        {
            Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
            DataTable dt = await VIETSEA.TBNETERP.GetNXT(_connectionStringOracle, dateEditVietseaEndDate.DateTime);
            if (dt == null)
            {
                MessageBox.Show("Không có dữ liệu");
            }

            Worksheet worksheet = workbook.GetNewSheet($"XNT_{dateEditVietseaEndDate.DateTime.ToString("ddMMyyyy")}");
            dt.CopyToRange(worksheet.Range["a1"]);
            Notification.Toast.Show("Load dữ liệu thành công");
        }

        private async void buttonNXT4COT_Click(object sender, EventArgs e)
        {
            try
            {
                Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;

                var taskDauky = VIETSEA.TBNETERP.GetNXT_4COT_DAUKY(_connectionStringOracle, dateEditVietseaStartDate.DateTime, dateEditVietseaEndDate.DateTime);
                var taskTrongKy = VIETSEA.TBNETERP.GetNXT_4COT_TRONGKY(_connectionStringOracle, dateEditVietseaStartDate.DateTime, dateEditVietseaEndDate.DateTime);
                var taskCuoiKy = VIETSEA.TBNETERP.GetNXT_4COT_CUOIKY(_connectionStringOracle, dateEditVietseaStartDate.DateTime, dateEditVietseaEndDate.DateTime);
                var taskDMVTHHDV = VIETSEA.TBNETERP.GET_DMVTHHDV(_connectionStringOracle);
                var taskDMDONVITINHDV = VIETSEA.TBNETERP.GET_DMDONVITINHDV(_connectionStringOracle);

                List<Task> tasks = new List<Task>
                {
                    taskDauky,
                    taskTrongKy,
                    taskCuoiKy,
                    taskDMVTHHDV,
                    taskDMDONVITINHDV
                };

                while (tasks.Count > 0)
                {
                    Task finishedTask = await Task.WhenAny(tasks);
                    if (finishedTask == taskDauky)
                    {
                        DataTable dtDauKy = await taskDauky;
                        dtDauKy.CopyToRange(workbook.GetNewSheet("dauky").Range["a1"]);
                    }
                    else if (finishedTask == taskTrongKy)
                    {
                        DataTable dtTrongKy = await taskTrongKy;
                        dtTrongKy.CopyToRange(workbook.GetNewSheet("trongky").Range["a1"]);
                    }
                    else if (finishedTask == taskCuoiKy)
                    {
                        DataTable dtCuoiKy = await taskCuoiKy;
                        dtCuoiKy.CopyToRange(workbook.GetNewSheet("cuoiky").Range["a1"]);
                    }
                    else if (finishedTask == taskDMVTHHDV)
                    {
                        DataTable dtDMVTHHDV = await taskDMVTHHDV;
                        dtDMVTHHDV.CopyToRange(workbook.GetNewSheet("DMVTHHDV").Range["a1"]);
                    }
                    else if (finishedTask == taskDMDONVITINHDV)
                    {
                        DataTable dtDMDONVITINHDV = await taskDMDONVITINHDV;
                        dtDMDONVITINHDV.CopyToRange(workbook.GetNewSheet("DMDONVITINHDV").Range["a1"]);
                    }

                    tasks.Remove(finishedTask);


                }

                Worksheet sheetDauky = workbook.GetSheet("dauky");
                Worksheet sheetTrongky = workbook.GetSheet("trongky");
                Worksheet sheetCuoiky = workbook.GetSheet("cuoiky");

                sheetTrongky.Range["b1:c1"].EntireColumn.Insert();
                sheetTrongky.EntireRow(1).Delete();
                sheetTrongky.GetRange("a1:g", "a").Copy(sheetDauky.NewRangeAtColumn("a"));


                #region pivot field name
                string MAVTU = "MAVTU";
                string SLDAUKY = "SLDAUKY";
                string GTDAUKY = "GTDAUKY";
                string SLNHAP = "SLNHAP";
                string GTNHAP = "GTNHAP";
                string SLXUAT = "SLXUAT";
                string GTXUAT = "GTXUAT";
                string SLCUOIKY = "SLCUOIKY";
                string GTCUOIKY = "GTCUOIKY";
                #endregion


                sheetDauky.Range["a1"].SetHeaderText(MAVTU, SLDAUKY, GTDAUKY, SLNHAP, GTNHAP, SLXUAT, GTXUAT, SLCUOIKY, GTCUOIKY);
                ListObject listObject = sheetDauky.UsedRange.ToListObject();
                listObject.GetColumnRange(SLDAUKY).ConvertToValue().SetAccountingNumberFormat();
                listObject.GetColumnRange(GTDAUKY).ConvertToValue().SetAccountingNumberFormat();
                listObject.GetColumnRange(SLNHAP).ConvertToValue().SetAccountingNumberFormat();
                listObject.GetColumnRange(GTNHAP).ConvertToValue().SetAccountingNumberFormat();
                listObject.GetColumnRange(SLXUAT).ConvertToValue().SetAccountingNumberFormat();
                listObject.GetColumnRange(GTXUAT).ConvertToValue().SetAccountingNumberFormat();

                listObject.GetColumnDataBodyRange(SLCUOIKY).Cell(1, 1).Formula =
                    "=" + listObject.GetColumnDataBodyRange(SLDAUKY).Cell(1, 1).Address2()
                    + "+" + listObject.GetColumnDataBodyRange(SLNHAP).Cell(1, 1).Address2()
                    + "-" + listObject.GetColumnDataBodyRange(SLXUAT).Cell(1, 1).Address2();

                listObject.GetColumnDataBodyRange(GTCUOIKY).Cell(1, 1).Formula =
                    "=" + listObject.GetColumnDataBodyRange(GTDAUKY).Cell(1, 1).Address2()
                    + "+" + listObject.GetColumnDataBodyRange(GTNHAP).Cell(1, 1).Address2()
                    + "-" + listObject.GetColumnDataBodyRange(GTXUAT).Cell(1, 1).Address2();

                listObject.GetColumnDataBodyRange(SLCUOIKY).FillDownFormula(false);
                listObject.GetColumnDataBodyRange(GTCUOIKY).FillDownFormula(false);

                PivotTable PT = VSTOLib.Excel.Pivot.CreatePivotTable(sheetDauky.UsedRange, "pivot1");
                PivotField fMAVTU = PT.PivotFields(MAVTU);
                fMAVTU.Orientation = XlPivotFieldOrientation.xlRowField;
                fMAVTU.Position = 1;

                PT.AddDataField(PT.PivotFields(SLDAUKY), "sl tồn đk", XlConsolidationFunction.xlSum);
                PT.AddDataField(PT.PivotFields(GTDAUKY), "gt tồn đk", XlConsolidationFunction.xlSum);
                PT.AddDataField(PT.PivotFields(SLNHAP), "sl nhập", XlConsolidationFunction.xlSum);
                PT.AddDataField(PT.PivotFields(GTNHAP), "gt nhập", XlConsolidationFunction.xlSum);
                PT.AddDataField(PT.PivotFields(SLXUAT), "sl xuất", XlConsolidationFunction.xlSum);
                PT.AddDataField(PT.PivotFields(GTXUAT), "gt xuất", XlConsolidationFunction.xlSum);
                PT.AddDataField(PT.PivotFields(SLCUOIKY), "sl tồn ck", XlConsolidationFunction.xlSum);
                PT.AddDataField(PT.PivotFields(GTCUOIKY), "gt tồn ck", XlConsolidationFunction.xlSum);

                PT.DataBodyRange.SetAccountingNumberFormat();

                listObject.Unlist();
                this.SafeInvoke(() => { MessageBox.Show(this, "Load thành công"); });

            }
            catch (Exception ex)
            {
                this.SafeInvoke(() => { MessageBox.Show(this, ex.Message); });
            }
            finally
            {

            }

        }

        private async void buttonLoadTHCongNo_Click(object sender, EventArgs e)
        {
            string oldText = buttonLoadTHCongNo.Text;
            string accessFile = Config.Manager.GetValue(Config.ID.OneDriveDatabaseAccessFile);

            try
            {
                buttonLoadTHCongNo.InvokeSetEnable(false);

                gridControlTHCongNo.DataSource = null;
                DataTable khachHangs = await ADO.GetDataTableFromOracleServerAsync(_connectionStringOracle, "select makhachhang,masothue,tentochu from dmkhachhang where manhomkhachang like '%NCC%' order by makhachhang");
                if (!khachHangs.HasData())
                {
                    return;
                }

                await Task.Run(() => Main(khachHangs));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                buttonLoadTHCongNo.InvokeSetEnable(true);
                buttonLoadTHCongNo.SetTextSafeThread(oldText);
            }

            //local
            void Main(DataTable table)
            {
                DataTable dataSource = CreateDataSource();
                int loopCounter = 0;

                Parallel.For(0, table.Rows.Count - 1, new ParallelOptions { MaxDegreeOfParallelism = 6 }, async i =>
                {
                    DataRow eachRow = table.Rows[i];
                    await FillData(eachRow, dataSource);
                    loopCounter++;
                    buttonLoadTHCongNo.SafeInvoke(() =>
                    {
                        buttonLoadTHCongNo.Text = $"{loopCounter}/{table.Rows.Count}";
                    });
                });

                gridControlTHCongNo.SafeInvoke(() =>
                {
                    gridControlTHCongNo.DataSource = null;
                    gridViewTHCongNo.OptionsView.ShowAutoFilterRow = true;
                    gridViewTHCongNo.OptionsView.ShowFooter = true;
                    gridViewTHCongNo.OptionsView.ShowGroupPanel = false;
                    gridControlTHCongNo.DataSource = dataSource;


                    gridViewTHCongNo.Columns["Dư Nợ"].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                    gridViewTHCongNo.Columns["Dư Có"].SetNumberFormat(DevExpress.Utils.FormatType.Numeric, "{0:N0}");
                    //gridViewTHCongNo.SetBestFitColumns(0, "Mã", "Dư Nợ", "Dư Có", "Ghi chú");


                });

                Addbuttonedit();

                Notification.Wait.Close();
                this.SafeInvoke(() => { MessageBox.Show(this, "Xong"); });

            }

            DataTable CreateDataSource()
            {
                DataTable source = new DataTable("a");
                source.Columns.Add("Mã", typeof(string));
                source.Columns.Add("MST", typeof(string));
                source.Columns.Add("Tên", typeof(string));
                source.Columns.Add("Dư Nợ", typeof(double));
                source.Columns.Add("Dư Có", typeof(double));
                source.Columns.Add("Ghi chú", typeof(string));
                source.Columns.Add("Phân tích", typeof(string));

                return source;
            }

            async Task FillData(DataRow row, DataTable source)
            {
                string ncc = row["MAKHACHHANG"].ToString();
                string mst = row["MASOTHUE"].ToString();
                string ten = row["TENTOCHU"].ToString();

                DataTable dataChiTietCongNo = await GetDataChiTietCongNo(dateEditVietseaStartDate.DateTime, dateEditVietseaEndDate.DateTime, ncc).ConfigureAwait(false);
                if (!dataChiTietCongNo.HasData())
                {
                    return;
                }

                DataRow dataRow = source.NewRow();
                string ghichu = string.Empty;
                string phanTich = string.Empty;
                int saiSoChoPhep = Public.Accounting.SaiSoChoPhep;
                double SDDK_NO = Convert.ToDouble(dataChiTietCongNo.Rows[dataChiTietCongNo.Rows.Count - 1]["SDDK_NO"]);
                double SDDK_CO = Convert.ToDouble(dataChiTietCongNo.Rows[dataChiTietCongNo.Rows.Count - 1]["SDDK_CO"]);
                bool addRow = true;

                if (SDDK_NO < 1 && SDDK_CO < 1)
                {
                    addRow = false;
                }

                if (SDDK_NO <= saiSoChoPhep && SDDK_CO <= saiSoChoPhep)
                {
                    ghichu = "Hết công nợ, số dư nhỏ";
                }
                else
                {
                    ghichu = await GetGhiChu(ncc, SDDK_NO, SDDK_CO);
                    phanTich = await GetPhanTichCongNo(mst);
                }



                if (addRow)
                {
                    dataRow.ItemArray = new object[] { ncc, mst, ten, SDDK_NO, SDDK_CO, ghichu, phanTich };
                    source.Rows.Add(dataRow);
                }
            }

            async Task<string> GetGhiChu(string ncc, double duNo, double duCo)
            {
                string result = string.Empty;

                string query = $"SELECT * FROM {Public.OneDrive.CongNoNCC.TableName} WHERE {Public.OneDrive.CongNoNCC.MANHACUNGCAP} = '{ncc}'";
                DataTable dt = await ADO.GetDataFromAccessFileAsync(accessFile, query).ConfigureAwait(false);
                if (!dt.HasData())
                {
                    return string.Empty;
                }

                double DUNOFROMACCESS = dt.Rows[0]["DUNO"] is DBNull ? 0 : Convert.ToDouble(dt.Rows[0]["DUNO"]);
                double DUCOFROMACCESS = dt.Rows[0]["DUCO"] is DBNull ? 0 : Convert.ToDouble(dt.Rows[0]["DUCO"]);

                if (DUNOFROMACCESS == duNo && DUCOFROMACCESS == duCo)
                {
                    result = dt.Rows[0]["GHICHU"] is DBNull ? string.Empty : dt.Rows[0]["GHICHU"].ToString();
                }

                return result;
            }

            async Task<string> GetPhanTichCongNo(string mst)
            {
                string result = string.Empty;

                DataTable dt = await ThanhCong.Database.SQLServer.GetDetailByOneAccountByMSTAsync(_connectionStringSQL, "331", dateEditThanhCongStartDate.DateTime, dateEditThanhCongEndDate.DateTime, mst);


                return result;
            }

            void Addbuttonedit()
            {
                gridControlTHCongNo.SafeInvoke(() =>
                {
                    DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit buttonEdit = gridViewTHCongNo.AddButtonEdit("Ghi chú", new DevExpress.XtraEditors.Controls.EditorButton() { Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Search });
                    buttonEdit.ButtonClick += ButtonEdit_ButtonClick;
                });
            }

        }

        private async void ButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView gridView = DevExpressExtensions.GetGridView(sender);
                DataRow row = gridView.GetDataRow(gridView.FocusedRowHandle);
                string ncc = row["Mã"].ToString();
                gridLookUpEditMaNCCVietsea.EditValue = ncc;
                gridLookUpEditMaNCCVietsea.ShowPopup();
                gridLookUpEditMaNCCVietsea.ClosePopup();

                string mst = textBoxMSTVietsea.Text;
                await FillCustomerID(mst);
                _ = ShowDetailByOneAccountAsync();

                _ = GetSoChiTiet3311();


            }
            catch (Exception ex)
            {
                this.SafeInvoke(() => { MessageBox.Show(this, ex.Message); });
            }
            finally
            {
                xtraTabControl1.SafeBeginInvoke(() =>
                {
                    xtraTabControl1.SelectedTabPage = xtraTabPageCongNo;
                });
            }

            #region local

            #endregion
        }

        private async void buttonStockinfo_Click(object sender, EventArgs e)
        {
            Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
            string dbname = labelStatusMSSQLConnection.Text.GetStringAfter(": ");

            List<System.Data.SqlClient.SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@Fromdate", dateEditThanhCongStartDate.DateTime.ToString("yyyyMMdd")));
            sqlParameters.Add(new SqlParameter("@Todate", dateEditThanhCongEndDate.DateTime.ToString("yyyyMMdd")));
            DataTable data = await ADO.GetDataTableFromStoredProcedureAsync(_connectionStringSQL, "[dbo].[System.Stock.Stockinfo]", sqlParameters);

            if (!data.HasData())
            {
                MessageBox.Show("Không có dữ liệu");
                return;
            }

            DataTable modData = data.ToDataTableSelectedColumns("GoodID", "FullName", "QtyStart", "AmountStartVat", "ImpQty", "ImpAmountVat", "ExpQty", "ExpAmountVat", "EndQty", "EndAmountVat");
            Worksheet worksheet = workbook.GetNewSheet(dbname);
            modData.CopyToRange(worksheet.Range["a1"]);

            ListObject listObject = worksheet.UsedRange.ToListObject();
            listObject.GetColumnRange("QtyStart").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("AmountStartVat").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("ImpQty").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("ImpAmountVat").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("ExpQty").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("ExpAmountVat").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("EndQty").ConvertToValue().SetAccountingNumberFormat();
            listObject.GetColumnRange("EndAmountVat").ConvertToValue().SetAccountingNumberFormat();

            listObject.Unlist();
            MessageBox.Show("Tải dữ liệu stockinfo thành công");
        }

        private void buttonGetInvoiceInfoCopyToBangKeGN_Click(object sender, EventArgs e)
        {

            #region field name
            string SOCTUGOC = "SOCTUGOC";
            string NGAYCTKT = "NGAYCTKT";
            string SPSTK_CO = "SPSTK_CO";

            #endregion

            Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet worksheet = workbook.GetNewSheet("infocopy");

            foreach (int row in gridViewSoChiTietTKVietsea.GetSelectedRows())
            {
                string invoiceNumber = "'" + gridViewSoChiTietTKVietsea.GetRowCellValue(row, SOCTUGOC).ToString();
                string invoiceDate = "'" + ((DateTime)gridViewSoChiTietTKVietsea.GetRowCellValue(row, NGAYCTKT)).ToString("dd/MM/yyyy");
                int invoiceTotalAmount = Convert.ToInt32(gridViewSoChiTietTKVietsea.GetRowCellValue(row, SPSTK_CO));

                worksheet.NewRangeAtColumn("a").SetHeaderText(invoiceNumber, invoiceDate, invoiceTotalAmount);
            }

            worksheet.Range["a2"].CurrentRegion.Copy();
            Notification.Toast.Show("Copy thông tin hóa đơn thành công, hãy paste value vào bảng kê giải ngân");
        }

        private async void buttonExportExcelPL43_Click(object sender, EventArgs e)
        {
            try
            {
                Task<DataTable> getDataTask = VIETSEA.TBNETERP.Data_ToKhai_BanRa(dateEditVietseaStartDate.DateTime, dateEditVietseaEndDate.DateTime);

                string excelFile = @"Accounting\HTKK\Bang_Ke_01_ND15_GTGT_TT80.xls";
                string excelFullPath = System.IO.Path.Combine(ExcelAddIn1.Public.Addin.StartupPath, excelFile);
                Workbook workbook = Globals.ThisAddIn.Application.Workbooks.Open(excelFullPath);

                DataTable dataTable = await getDataTask;
                DataTable tableVat8 = dataTable.Select("MAVAT = '8'").CopyToDataTable().ToDataTableSelectedColumns("TENVTU", "TIENHANG");
                Worksheet bangke = workbook.GetSheet("Bang ke");
                bangke.Range["a40", "a" + bangke.Rows.Count].EntireRow.ClearContents();

                tableVat8.CopyToRange(bangke.NewRangeAtColumn("c"), false);
                bangke.NewRangeAtColumn("b").SetRowNum(tableVat8.Rows.Count);
                bangke.Range["d" + (bangke.LastRow("e") + 1), "d" + bangke.Rows.Count].ConvertToValue();

                Globals.ThisAddIn.Application.DisplayAlerts = false;
                SaveFileDialog saveFileDialog = new SaveFileDialog
                {
                    Filter = "Excel |*.xls",
                    FileName = workbook.Name,
                    Title = "Lưu file"

                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filename = saveFileDialog.FileName;
                    workbook.SaveAs(filename);
                    workbook.Close(false);
                    this.SafeInvoke(() => { MessageBox.Show(this, "Kết xuất file bảng kê phụ lục 43 thành công!"); });
                }
            }
            catch (Exception ex)
            {
                this.SafeInvoke(() => { MessageBox.Show(this, ex.Message); });
            }
            finally
            {
                Globals.ThisAddIn.Application.DisplayAlerts = true;
            }

        }

        private void barButtonItem1_HachToanPhieuChi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private async void ButtonTongHop131_Click(object sender, EventArgs e)
        {
            try
            {
                string accountNumber = textEditAccountNumber.Text;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@FromDate", ADO.ConvertDateToTransDate(dateEditThanhCongStartDate.DateTime)));
                sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@EndDate", ADO.ConvertDateToTransDate(dateEditThanhCongEndDate.DateTime)));
                sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@AccountNumber", accountNumber));
                //sqlParameters.Add(new System.Data.SqlClient.SqlParameter("@SupplyID", ids));

                DataTable dataTable = await ADO.GetDataTableFromStoredProcedureAsync(_connectionStringSQL, "[dbo].[Accounting.Report.TotalByOneAccount]", sqlParameters);

                double startDebitValue = Convert.ToDouble(dataTable.Compute("AVG(DebitStartValue)", string.Empty));
                double startCreditValue = Convert.ToDouble(dataTable.Compute("AVG(CreditStartValue)", string.Empty));
                double debitValue = Convert.ToDouble(dataTable.Compute("SUM(DebitValue)", string.Empty));
                double creditValue = Convert.ToDouble(dataTable.Compute("SUM(CreditValue)", string.Empty));
                double endDebitValue = Convert.ToDouble(dataTable.Compute("AVG(DebitEndValue)", string.Empty));
                double endCreditValue = Convert.ToDouble(dataTable.Compute("AVG(CreditEndValue)", string.Empty));


                SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(_connectionStringSQL);
                string dbName = sqlConnectionStringBuilder.InitialCatalog;
                Workbook activeWorkbook = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet newWorksheet = activeWorkbook.GetNewSheet(accountNumber + "-" + dbName);
                newWorksheet.Range["a1"].SetHeaderText("Đơn vị", "Nợ đầu kỳ", "Có đầu kỳ", "Phát sinh Nợ", "Phát sinh Có", "Nợ cuối kỳ", "Có cuối kỳ");
                newWorksheet.Range["a1"].EntireRow.Font.Bold = true;

                newWorksheet.Range["a2"].SetHeaderText(dbName, startDebitValue, startCreditValue, debitValue, creditValue, endDebitValue, endCreditValue);
                newWorksheet.Range["a2"].EntireRow.SetAccountingNumberFormat();
                newWorksheet.Columns.AutoFit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}