﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExcelAddIn1.GoogleApis
{
    class SheetsAPI
    {
        public const string applicationName = "VSTOaddin";
        public const string spreadSheetID = "1o9N0xb1FdSWLQS_rlPcwp1bocYU4Z1R6p3-HyzNxjKw";

        public static async Task<SheetsService> GetSheetsServiceAsync()
        {
            GoogleCredential googleCredential = await ExcelAddIn1.GoogleApis.Credential.GetServiceAccountCredentialAsync().ConfigureAwait(false);
            if (googleCredential.IsCreateScopedRequired)
            {
                googleCredential = googleCredential.CreateScoped(SheetsService.Scope.Spreadsheets);
            }
            SheetsService sheetsService = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = googleCredential,
                ApplicationName = applicationName
            });

            return sheetsService;
        }

        public static async Task<IList<IList<object>>> GetValuesAsync(string spreadSheetID, string range)
        {
            var service = await GetSheetsServiceAsync();
            SpreadsheetsResource.ValuesResource.GetRequest getRequest = service.Spreadsheets.Values.Get(spreadSheetID, range);
            ValueRange valueRange = await getRequest.ExecuteAsync().ConfigureAwait(false);
            IList<IList<object>> result = valueRange.Values;
            return result;
        }

        public static async Task SetValuesAsync(string spreadSheetID, string range, IList<IList<object>> values)
        {
            var service = await GetSheetsServiceAsync();
            ValueRange valueRange = new ValueRange();
            valueRange.Values = values;
            SpreadsheetsResource.ValuesResource.UpdateRequest updateRequest = service.Spreadsheets.Values.Update(valueRange, spreadSheetID, range);
            updateRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;
            await updateRequest.ExecuteAsync().ConfigureAwait(false);

        }

        public static async Task AppendValueAsync(string spreadSheetID, string range, IList<IList<object>> values)
        {
            SheetsService sheetsService = await GetSheetsServiceAsync();

            ValueRange requestBody = new ValueRange();
            requestBody.MajorDimension = "ROWS";
            //var oblist = new List<object>() { "New value","hihi" };
            //var oblist2 = new List<object>() { "New value2","hihi2" };
            //requestBody.Values = new List<IList<object>> { oblist,oblist2 };
            requestBody.Values = (IList<IList<object>>)values;
            requestBody.Range = range;

            //SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum valueInputOptionEnum = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            //SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum insertDataOptionEnum = SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;
            
            SpreadsheetsResource.ValuesResource.AppendRequest request = sheetsService.Spreadsheets.Values.Append(requestBody, spreadSheetID, range);
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            request.InsertDataOption = SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;

            AppendValuesResponse response = await request.ExecuteAsync().ConfigureAwait(false);

        }
    }
}
