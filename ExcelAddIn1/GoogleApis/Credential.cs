﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.GoogleApis
{
    class Credential
    {
        public static async Task<Google.Apis.Auth.OAuth2.GoogleCredential> GetServiceAccountCredentialAsync()
        {
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream(ExcelAddIn1.Properties.Resources.serviceAccount))
            {
                return await Google.Apis.Auth.OAuth2.GoogleCredential.FromStreamAsync(stream, System.Threading.CancellationToken.None).ConfigureAwait(false);
            }
        }
    }
}
