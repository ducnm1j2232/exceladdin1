﻿using ExcelAddIn1.MyClass;
using ExcelAddIn1.MyClass.Extensions;
using Microsoft.Office.Interop.Excel;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;
using OracleCommand = Oracle.ManagedDataAccess.Client.OracleCommand;
using OracleConnection = Oracle.ManagedDataAccess.Client.OracleConnection;
using OracleDataAdapter = Oracle.ManagedDataAccess.Client.OracleDataAdapter;
using OracleParameter = Oracle.ManagedDataAccess.Client.OracleParameter;

namespace ExcelAddIn1.VIETSEA
{
    public static class TBNETERP
    {
        public static string BAN10 = "BAN10";
        public static string BAN05 = "BAN05";
        public static string KHOHANG01 = "K-HANG01";
        public static string KHOHANG5 = "K-HANG5";

        public static string ConnectionString_DATPHAT
        {
            get
            {
                OracleConnectionStringBuilder stringBuilder = new OracleConnectionStringBuilder();
                stringBuilder.DataSource = $"192.168.0.50:1522/TBNETERP";
                stringBuilder.UserID = "TBNETERP";
                stringBuilder.Password = "TBNETERP";
                return stringBuilder.ConnectionString;
            }
        }

        public static Worksheet FileImportXUATBAN_Empty()
        {
            Workbook wb = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet ws = wb.Worksheets[1];
            ws.Name = "4";
            ws.Range["a1"].SetHeaderText(new string[] { "So_ct", "Ma_kh", "Ma_vt", "So_luong", "Gia2", "Tien2", "Thue" });
            ws.Range["f2"].Formula = "=D2*E2";
            ws.Range["f2:f3000"].FillDownFormula(false);
            return ws;
        }

        public static Worksheet FileImportXUATBAN(string so_ct = null, string ma_kh = null, Range ma_vt = null, Range so_luong = null, Range gia2 = null, int thue = 10)
        {
            Worksheet result = FileImportXUATBAN_Empty();
            if (ma_vt != null) ma_vt.CopyValueTo(result.Range["c2"]);
            if (so_luong != null) so_luong.CopyValueTo(result.Range["d2"]);
            if (gia2 != null) gia2.CopyValueTo(result.Range["e2"]);

            result.GetRange("a2:a", "c").Value2 = so_ct;
            result.GetRange("b2:b", "c").Value2 = ma_kh;
            result.GetRange("g2:g", "c").Value2 = thue;

            return result;
        }

        public static void Create_workbook_import_XuatBan(string path, string so_ct = null, string ma_kh = null, Range ma_vt = null, Range so_luong = null, Range gia2 = null, int thue = 10)
        {
            Worksheet sheet4 = FileImportXUATBAN_Empty();
            if (ma_vt != null) ma_vt.CopyValueTo(sheet4.Range["c2"]);
            if (so_luong != null) so_luong.CopyValueTo(sheet4.Range["d2"]);
            if (gia2 != null) gia2.CopyValueTo(sheet4.Range["e2"]);

            sheet4.GetRange("a2:a", "c").Value2 = so_ct;
            sheet4.GetRange("b2:b", "c").Value2 = ma_kh;
            sheet4.GetRange("g2:g", "c").Value2 = thue;

            Workbook workbook = (Workbook)sheet4.Parent;
            string fillname = $"{path}\\Import XBAN {ma_kh} {so_ct}.xls";
            PublicVar.last_file_import_xuatban_fullname = fillname;
            workbook.SaveOverWrite(fillname, 56);
            workbook.Close();
        }

        public static async System.Threading.Tasks.Task<System.Data.DataTable> DATATABLE_HOADON_NHAPMUA_NHAPDICHVU(string connString, System.DateTime startDate, System.DateTime endDate, string makhachhang)
        {
            string fromDate = startDate.ToString("dd-MMM-yy");
            string toDate = endDate.ToString("dd-MMM-yy");

            string q = string.Format(@"Select 
                                        to_char(Th.Ngayhachtoan,'DD/MM/YYYY') As ""Ngày HT"",
                                        Th.Sohoadongd As ""Số HĐ"",
                                        to_char(Th.Ngayhoadongd,'DD/MM/YYYY') As ""Ngày HĐ"",
                                        Dmkh.Tenkhachhang As ""Tên nhà cung cấp"",
                                        sum(ct.tienhang + ct.tienvat) as ""Số tiền"",
                                        Th.Ngayhachtoan
                                        From
                                        Vattugd Th
                                        Join Vattugdct Ct
                                        On Th.Mavattugdpk = Ct.Mavattugdpk
                                        Join Dmkhachhang Dmkh
                                        on th.makhachang = dmkh.makhachhang
                                        Where Th.Ngayhachtoan >= '{0}'
                                        And Th.Ngayhachtoan <= '{1}'
                                        And Th.Maptnx in ('NMUA','NDV')
                                        And Th.Makhachang = '{2}'
                                        GROUP BY Th.Ngayhachtoan, Th.Sohoadongd, Th.Ngayhoadongd, Dmkh.Tenkhachhang
                                        order by 6, 2", fromDate, toDate, makhachhang);
            System.Data.DataTable result = await ADO.GetDataTableFromOracleServerAsync(connString, q).ConfigureAwait(false);
            if (result.Rows.Count > 0) return result;
            else return null;

        }

        public enum XNTtype
        {
            Full, Available

        }

        public enum MAKHOHANG
        {
            ALL, BAN10, BAN05, KHOHANG01, KHOHANG5
        }

        //public static string MAKHOHANG
        //{
        //    get { "ALL","BAN10","BAN05","KHOHANG01","KHOHANG5" }
        //}


        /// <summary>
        /// bao gồm 6 cột: kho, mavattu, tenvattu, soluong, giavon, giatri
        /// </summary>
        /// <param name="date"></param>
        /// <param name="kho"></param>
        /// <returns></returns>
        public static async Task<DataTable> DATATABLE_XNT_VIETSEA_CORE(DateTime date, MAKHOHANG makhohang = MAKHOHANG.ALL, XNTtype xntType = XNTtype.Full, IList<string> mavattu = null)
        {
            string xntDate = date.ToString("ddMMyyyy");
            string oracleDate = date.ToString("dd-MMM-yy");
            string q = string.Empty;
            _ = @"With Xnt As
                                    (Select
                                    makhohang || mavtu as a,
                                      Makhohang,
                                      Mavtu,
                                      Tenvtu,
                                      toncuoikysl,
                                      Giavon,
                                      toncuoikygt
                                    From Xnt_01062021
                                    Where
                                      Toncuoikysl >= 1
                                      And Giavon > 0
                                      and toncuoikygt > 0)
                                    ,
                                    Dataxuat As
                                    (Select
                                      Vattugdct.Makhoxuat || Vattugdct.Mavtu as a,
                                      Vattugdct.Makhoxuat As Kho,
                                      Vattugdct.Mavtu As Mavattu,
                                      sum(vattugdct.soluong) as soluong
                                    From
                                      Vattugdct
                                    Join
                                      Vattugd
                                    On
                                      vattugdct.mavattugdpk = vattugd.mavattugdpk
                                    Where
                                      Vattugd.Maptnx In('XBAN', 'XTRA1', 'XKHAC')
                                      And Vattugd.Ngayhachtoan > '01-JUN-21'
                                    Group By Vattugdct.Makhoxuat || Vattugdct.Mavtu, Vattugdct.Makhoxuat, Vattugdct.Mavtu)

                                    Select
                                      Xnt.Makhohang As Kho,
                                      Xnt.Mavtu As Mavattu,
                                      Xnt.Tenvtu as tenvattu,
                                      Xnt.Toncuoikysl - Nvl(Dataxuat.Soluong, 0) As Slton,
                                       Xnt.Giavon As Giavon,
                                      xnt.toncuoikygt as giatri
                                    From
                                      Xnt
                                    Left Join
                                      Dataxuat
                                    On
                                      xnt.a = dataxuat.a";
            string kho = string.Empty;

            switch (makhohang)
            {
                case MAKHOHANG.ALL:
                    kho = " and makhohang in ('BAN10','BAN05','K-HANG01','K-HANG5')";
                    break;
                case MAKHOHANG.BAN10:
                    kho = " and makhohang = 'BAN10'";
                    break;
                case MAKHOHANG.BAN05:
                    kho = " and makhohang = BAN05";
                    break;
                case MAKHOHANG.KHOHANG01:
                    kho = " and makhohang = K-HANG01";
                    break;
                case MAKHOHANG.KHOHANG5:
                    kho = " and makhohang = K-HANG5";
                    break;
                default:
                    break;
            }

            string mavtu = string.Empty;
            if (mavattu != null)
            {
                mavtu = $" and mavtu in ({mavattu.ToSQLWhereIn()})";
            }

            string querry_get_xnt_core = string.Format(@"Select
                                                          makhohang || mavtu as a,
                                                          Makhohang as kho,
                                                          Mavtu as mavattu,
                                                          Tenvtu as tenvattu,
                                                          toncuoikysl as soluong,
                                                          Giavon as giavon, 
                                                          toncuoikygt as giatri
                                                        From Xnt_{0}
                                                        Where
                                                          Toncuoikysl >= 1
                                                          And Giavon > 0
                                                          and toncuoikygt > 0
                                                          {1}
                                                          {2}
                                                        order by mavtu", xntDate, kho, mavtu);

            switch (xntType)
            {
                case XNTtype.Full:
                    q = querry_get_xnt_core.Replace("makhohang || mavtu as a,", string.Empty);
                    //loại bỏ đi vì lấy tồn full không cần đoạn nỗi chuỗi này, nối chuỗi này chỉ để join khi dùng XNTtype.Available
                    break;
                case XNTtype.Available:
                    q = string.Format(@"With Xnt As
                                        ({0})
                                        ,
                                        Dataxuat As
                                        (Select
                                          Vattugdct.Makhoxuat||Vattugdct.Mavtu as a,
                                          Vattugdct.Makhoxuat As Kho,
                                          Vattugdct.Mavtu As Mavattu,
                                          sum(vattugdct.soluong)as soluong
                                        From
                                          Vattugdct
                                        Join
                                          Vattugd
                                        On
                                          vattugdct.mavattugdpk = vattugd.mavattugdpk
                                        Where
                                          Vattugd.Maptnx In ('XBAN','XTRA1','XKHAC')
                                          And Vattugd.Ngayhachtoan > '{1}'
                                        Group By Vattugdct.Makhoxuat||Vattugdct.Mavtu, Vattugdct.Makhoxuat, Vattugdct.Mavtu)

                                        Select
                                          Xnt.kho As Kho,
                                          Xnt.mavattu As Mavattu,
                                          Xnt.tenvattu as tenvattu,
                                          to_char(to_number(Xnt.soluong) - to_number(NVL(Dataxuat.soluong,0))) As soluong,
                                          Xnt.giavon As giavon,
                                          to_char((to_number(Xnt.soluong) - to_number(NVL(Dataxuat.soluong,0))) * to_number(Xnt.giavon)) as giatri
                                        From
                                          Xnt
                                        Left Join
                                          Dataxuat
                                        On
                                          xnt.a = dataxuat.a", querry_get_xnt_core, oracleDate);
                    break;
                default:
                    break;
            }

            DataTable dataTable = await ADO.GetDataTableFromOracleServerAsync(q).ConfigureAwait(false);
            if (dataTable.Rows.Count > 0)
            {
                return dataTable;
            }
            else
            {
                return null;
            }



            //return dataTable;
        }

        public static async Task<DataTable> DATATABLE_XNT_VIETSEA_CORE(string connString, DateTime date, IList<string> KHOHANG = null, XNTtype xntType = XNTtype.Full, IList<string> mavattu = null)
        {
            string xntDate = date.ToString("ddMMyyyy");
            string oracleDate = date.ToString("dd-MMM-yy");
            string q = string.Empty;
            _ = @"With Xnt As
                                    (Select
                                    makhohang || mavtu as a,
                                      Makhohang,
                                      Mavtu,
                                      Tenvtu,
                                      toncuoikysl,
                                      Giavon,
                                      toncuoikygt
                                    From Xnt_01062021
                                    Where
                                      Toncuoikysl >= 1
                                      And Giavon > 0
                                      and toncuoikygt > 0)
                                    ,
                                    Dataxuat As
                                    (Select
                                      Vattugdct.Makhoxuat || Vattugdct.Mavtu as a,
                                      Vattugdct.Makhoxuat As Kho,
                                      Vattugdct.Mavtu As Mavattu,
                                      sum(vattugdct.soluong) as soluong
                                    From
                                      Vattugdct
                                    Join
                                      Vattugd
                                    On
                                      vattugdct.mavattugdpk = vattugd.mavattugdpk
                                    Where
                                      Vattugd.Maptnx In('XBAN', 'XTRA1', 'XKHAC')
                                      And Vattugd.Ngayhachtoan > '01-JUN-21'
                                    Group By Vattugdct.Makhoxuat || Vattugdct.Mavtu, Vattugdct.Makhoxuat, Vattugdct.Mavtu)

                                    Select
                                      Xnt.Makhohang As Kho,
                                      Xnt.Mavtu As Mavattu,
                                      Xnt.Tenvtu as tenvattu,
                                      Xnt.Toncuoikysl - Nvl(Dataxuat.Soluong, 0) As Slton,
                                       Xnt.Giavon As Giavon,
                                      xnt.toncuoikygt as giatri
                                    From
                                      Xnt
                                    Left Join
                                      Dataxuat
                                    On
                                      xnt.a = dataxuat.a";

            string kho = string.Empty;
            if (KHOHANG != null)
            {
                kho = $" and makhohang in ({KHOHANG.ToSQLWhereIn()})";
            }

            string mavtu = string.Empty;
            if (mavattu != null)
            {
                mavtu = $" and mavtu in ({mavattu.ToSQLWhereIn()})";
            }

            string querry_get_xnt_core = string.Format(@"Select
                                                          makhohang || mavtu as a,
                                                          Makhohang as kho,
                                                          Mavtu as mavattu,
                                                          Tenvtu as tenvattu,
                                                          toncuoikysl as soluong,
                                                          Giavon as giavon, 
                                                          toncuoikygt as giatri
                                                        From Xnt_{0}
                                                        Where
                                                          Toncuoikysl >= 1
                                                          And Giavon > 0
                                                          and toncuoikygt > 0
                                                          {1}
                                                          {2}
                                                        order by mavtu", xntDate, kho, mavtu);

            switch (xntType)
            {
                case XNTtype.Full:
                    q = querry_get_xnt_core.Replace("makhohang || mavtu as a,", string.Empty);
                    //loại bỏ đi vì lấy tồn full không cần đoạn nỗi chuỗi này, nối chuỗi này chỉ để join khi dùng XNTtype.Available
                    break;
                case XNTtype.Available:
                    q = string.Format(@"With Xnt As
                                        ({0})
                                        ,
                                        Dataxuat As
                                        (Select
                                          Vattugdct.Makhoxuat||Vattugdct.Mavtu as a,
                                          Vattugdct.Makhoxuat As Kho,
                                          Vattugdct.Mavtu As Mavattu,
                                          sum(vattugdct.soluong)as soluong
                                        From
                                          Vattugdct
                                        Join
                                          Vattugd
                                        On
                                          vattugdct.mavattugdpk = vattugd.mavattugdpk
                                        Where
                                          Vattugd.Manhomptnx like 'X_%'
                                          --And Vattugd.Manhomptnx <> 'X_CHUYENKHO'
                                          And Vattugd.Ngayhachtoan > '{1}'
                                        Group By Vattugdct.Makhoxuat||Vattugdct.Mavtu, Vattugdct.Makhoxuat, Vattugdct.Mavtu)

                                        Select
                                          Xnt.kho As Kho,
                                          Xnt.mavattu As Mavattu,
                                          Xnt.tenvattu as tenvattu,
                                          to_char(to_number(Xnt.soluong) - to_number(NVL(Dataxuat.soluong,0))) As soluong,
                                          Xnt.giavon As giavon,
                                          to_char((to_number(Xnt.soluong) - to_number(NVL(Dataxuat.soluong,0))) * to_number(Xnt.giavon)) as giatri
                                        From
                                          Xnt
                                        Left Join
                                          Dataxuat
                                        On
                                          xnt.a = dataxuat.a
                                        where
                                           to_char(to_number(Xnt.soluong) - to_number(NVL(Dataxuat.soluong,0))) >= 1", querry_get_xnt_core, oracleDate);
                    break;
                default:
                    break;
            }

            DataTable dataTable = await ADO.GetDataTableFromOracleServerAsync(connString, q).ConfigureAwait(false);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable;
            }
            else
            {
                return null;
            }



            //return dataTable;
        }

        public static async Task<DataTable> DATATABLE_XNT_KHO_VAT(string connString, DateTime date)
        {
            string dateString = date.ToString("ddMMyyyy");
            string queryXNT = string.Format(@"Select
Makhohang,
Mavtu,
tenvtu,
1 as vat
From
Xnt_{0}
Where
Toncuoikysl>0
order by mavtu", dateString);
            DataTable dtXNT = await ADO.GetDataTableFromOracleServerAsync(connString, queryXNT).ConfigureAwait(false);

            if (dtXNT == null) return null;


            for (int i = 0; i < dtXNT.Rows.Count; i++)
            {
                string mavtu = dtXNT.Rows[i]["MAVTU"].ToString();
                if (string.IsNullOrEmpty(mavtu)) continue;

                string q = string.Format(@"Select
Vattugdct.Vat
From
Vattugd
Join
Vattugdct
on vattugd.mavattugdpk = vattugdct.mavattugdpk
Where
Vattugd.Maptnx = 'NMUA'
And Vattugdct.Mavtu = '{0}'
and ROWNUM = 1
Order By Vattugd.Ngayhachtoan Desc", mavtu);
                var vat = await ADO.GetDataTableFromOracleServerAsync(connString, q);
                dtXNT.Rows[i]["VAT"] = vat.Rows[0][0];
            }

            return dtXNT;
        }

        public static async Task<DataTable> DATATABLE_BangKeBanRa(string connString, System.DateTime fromDate, System.DateTime toDate)
        {
            string startDate = fromDate.ToString("dd-MMM-yy");
            string endDate = toDate.ToString("dd-MMM-yy");
            string q = string.Format(@"Select 
                                        Vattugd.Soctugoc As Sohoadon,
                                        Vattugd.Kyhieuhoadongd As Kyhieu,
                                        to_char(Vattugd.Ngayhachtoan,'dd/MM/yyyy') as ngaythang,
                                        Vattugd.Masothue As Mst,
                                        dmkhachhang.Tenkhachhang As Tenkhachhang,
                                        sum(Vattugdct.tienhang + vattugdct.tienvat) as thanhtien
                                    From Vattugdct
                                    Join Vattugd
                                    On Vattugdct.Mavattugdpk = Vattugd.Mavattugdpk
                                    Join Dmkhachhang
                                    on vattugd.makhachang = dmkhachhang.makhachhang
                                    Where  VATTUGD.MAPTNX in('XBAN','XTRA1','XDV')
                                    and Vattugd.Ngayhachtoan >= '{0}'
                                    And Vattugd.Ngayhachtoan <= '{1}'
                                    and Vattugd.trangthai = '10'
                                    GROUP BY Vattugd.Soctugoc, Vattugd.Kyhieuhoadongd, to_char(Vattugd.Ngayhachtoan,'dd/MM/yyyy'), Vattugd.Masothue, dmkhachhang.Tenkhachhang
                                    order by Vattugd.Soctugoc", startDate, endDate);
            DataTable result = await ADO.GetDataTableFromOracleServerAsync(connString, q).ConfigureAwait(false);
            if (result != null && result.Rows.Count > 0)
            {
                return result;
            }
            else return null;
        }

        public static DataTable BC_XNT_TUNGAYDENNGAY_4COT(string connStr, string P_TUNGAY, string P_DENNGAY, string P_MADONVI, string P_MAKHO, string P_MAVTU, string P_MANHOMVTU, string P_MALOAIVTU, string P_MANCC, string P_MATKVTU)
        {
            string commandText = "PK_XNT_TUNGAYDENNGAY.BIEU4COT";
            Oracle.ManagedDataAccess.Client.OracleConnection oracleConnection = new Oracle.ManagedDataAccess.Client.OracleConnection(connStr);
            Oracle.ManagedDataAccess.Client.OracleCommand oracleCommand = new Oracle.ManagedDataAccess.Client.OracleCommand(commandText, oracleConnection);
            oracleCommand.CommandType = System.Data.CommandType.StoredProcedure;
            oracleCommand.BindByName = true;

            oracleCommand.Parameters.Add("P_TUNGAY", OracleDbType.Varchar2).Value = P_TUNGAY;
            oracleCommand.Parameters.Add("P_DENGAY", OracleDbType.Varchar2).Value = P_DENNGAY;
            oracleCommand.Parameters.Add("P_MALOAIVTU", OracleDbType.Varchar2).Value = P_MALOAIVTU;
            oracleCommand.Parameters.Add("P_MANHOMVTU", OracleDbType.Varchar2).Value = P_MANHOMVTU;
            oracleCommand.Parameters.Add("P_MADONVI", OracleDbType.Varchar2).Value = P_MADONVI;
            oracleCommand.Parameters.Add("P_MAKHO", OracleDbType.Varchar2).Value = P_MAKHO;
            oracleCommand.Parameters.Add("P_MAVTU", OracleDbType.Varchar2).Value = P_MAVTU;
            oracleCommand.Parameters.Add("P_MANCC", OracleDbType.Varchar2).Value = P_MANCC;
            oracleCommand.Parameters.Add("P_MATKVTU", OracleDbType.Varchar2).Value = P_MATKVTU;

            Oracle.ManagedDataAccess.Client.OracleParameter oracleParameter = new Oracle.ManagedDataAccess.Client.OracleParameter("C", OracleDbType.RefCursor);
            oracleParameter.Direction = System.Data.ParameterDirection.Output;
            oracleCommand.Parameters.Add(oracleParameter);
            oracleConnection.Open();

            Noti.ShowToast(oracleConnection.State.ToString());
            Oracle.ManagedDataAccess.Client.OracleDataAdapter oracleDataAdapter = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(oracleCommand);
            System.Data.DataSet dataSet = new System.Data.DataSet();
            try
            {
                oracleDataAdapter.Fill(dataSet);
            }
            catch
            {
                oracleConnection.Close();
                return null;
            }
            finally
            {
                oracleConnection.Close();
                oracleConnection.Dispose();
            }
            return dataSet.Tables[0];
        }

        public static async Task<DataTable> BC_SKT_S38DN_BCMAIN(string connStr, string P_TABLENAME, int P_NAM, string P_TUNGAY, string P_DENNGAY, string P_MATK, string P_CTDT, string P_MADONVI, string GROUP_BY, string P_MATKDU)
        {
            string commandText = "PK_BC_S38_DN_MAIN.EXEC_S38DN";
            using (OracleConnection oracleConnection = new OracleConnection(connStr))
            {
                using (OracleCommand oracleCommand = new OracleCommand(commandText, oracleConnection))
                {
                    oracleCommand.BindByName = true;
                    oracleCommand.CommandType = CommandType.StoredProcedure;
                    oracleCommand.Parameters.Add("P_TABLENAME", OracleDbType.Varchar2).Value = "BC_S38_DN";
                    oracleCommand.Parameters.Add("P_NAM", OracleDbType.Varchar2).Value = P_NAM - 1;
                    oracleCommand.Parameters.Add("P_GROUP_BY", OracleDbType.Varchar2).Value = GROUP_BY;
                    oracleCommand.Parameters.Add("P_TUNGAY", OracleDbType.Varchar2).Value = P_TUNGAY;
                    oracleCommand.Parameters.Add("P_DENNGAY", OracleDbType.Varchar2).Value = P_DENNGAY;
                    oracleCommand.Parameters.Add("P_MADONVI", OracleDbType.Varchar2).Value = "TH0302";
                    oracleCommand.Parameters.Add("P_MATK", OracleDbType.Varchar2).Value = P_MATK;
                    if (string.IsNullOrEmpty(P_MATKDU))
                    {
                        oracleCommand.Parameters.Add("P_MATKNO", OracleDbType.Varchar2).Value = P_MATK.Replace("MATK", "MATKNO");
                        oracleCommand.Parameters.Add("P_MATKCO", OracleDbType.Varchar2).Value = P_MATK.Replace("MATK", "MATKCO");
                    }
                    else
                    {
                        string value = P_MATK.Replace("MATK", "MATKNO") + " AND MATKCO LIKE '" + P_MATKDU + "%'";
                        string value2 = P_MATK.Replace("MATK", "MATKCO") + " AND MATKNO LIKE '" + P_MATKDU + "%'";
                        oracleCommand.Parameters.Add("P_MATKNO", OracleDbType.Varchar2).Value = value;
                        oracleCommand.Parameters.Add("P_MATKCO", OracleDbType.Varchar2).Value = value2;
                    }
                    oracleCommand.Parameters.Add("P_CTDT", OracleDbType.Varchar2).Value = P_CTDT;
                    oracleCommand.Parameters.Add("P_CTDTNO", OracleDbType.Varchar2).Value = P_CTDT.Replace("CTDOITUONG", "CTDOITUONGNO");
                    oracleCommand.Parameters.Add("P_CTDTCO", OracleDbType.Varchar2).Value = P_CTDT.Replace("CTDOITUONG", "CTDOITUONGCO");
                    using (OracleParameter oracleParameter = new OracleParameter("C", OracleDbType.RefCursor))
                    {
                        oracleParameter.Direction = ParameterDirection.Output;
                        oracleCommand.Parameters.Add(oracleParameter);
                        await oracleConnection.OpenAsync().ConfigureAwait(false);
                        using (OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(oracleCommand))
                        {
                            DataSet dataSet = new DataSet();
                            try
                            {
                                oracleDataAdapter.Fill(dataSet);
                            }
                            catch
                            {
                                //oracleConnection.Close();
                                return null;
                            }
                            finally
                            {
                                //oracleConnection.Close();
                            }
                            DataTable table = dataSet.Tables[0];

                            //thêm dòng số dư đầu kỳ nếu kết quả trả về ko có
                            //vì những ncc mới, tính toán số dư đầu kỳ mà ko có thì sẽ ko có dòng dữ liệu số dư đầu kỳ
                            bool isContainsSDDK = table.AsEnumerable().Any(row => row.Field<decimal>("LOAIDULIEU") == 1);
                            if (!isContainsSDDK)
                            {
                                DataRow dr = table.NewRow();
                                dr["NOIDUNG"] = "Số dư đầu kỳ ";
                                dr["SDDK_NO"] = 0;
                                dr["SDDK_CO"] = 0;
                                dr["SPSTK_NO"] = 0;
                                dr["SPSTK_CO"] = 0;

                                table.Rows.InsertAt(dr, 0);
                            }
                            return table.ToDataTableSelectedColumns("NGAYHACHTOAN", "SOCTKTPK", "SOCTUGOC", "NGAYCTKT", "NOIDUNG", "MATKDU", "SPSTK_NO", "SPSTK_CO", "SDDK_NO", "SDDK_CO");
                        }
                    }
                }
            }
        }

        public static async Task<DataTable> BC_SKT_S38DN_BCMAIN_CORE(string connStr, DateTime startDate, DateTime endDate, string taiKhoan, string doiTuong)
        {
            int P_NAM = startDate.Year;
            string P_TUNGAY = startDate.ToString("dd/MM/yyyy");
            string P_DENNGAY = endDate.ToString("dd/MM/yyyy");
            string P_MATKDU = string.Empty;
            string P_MATK = taiKhoan;
            string P_CTDT = doiTuong;

            string commandText = "PK_BC_S38_DN_MAIN.EXEC_S38DN";
            using (OracleConnection oracleConnection = new OracleConnection(connStr))
            {
                using (OracleCommand oracleCommand = new OracleCommand(commandText, oracleConnection))
                {
                    oracleCommand.BindByName = true;
                    oracleCommand.CommandType = CommandType.StoredProcedure;
                    oracleCommand.Parameters.Add("P_TABLENAME", OracleDbType.Varchar2).Value = "BC_S38_DN";
                    oracleCommand.Parameters.Add("P_NAM", OracleDbType.Varchar2).Value = P_NAM - 1;
                    oracleCommand.Parameters.Add("P_GROUP_BY", OracleDbType.Varchar2).Value = "";
                    oracleCommand.Parameters.Add("P_TUNGAY", OracleDbType.Varchar2).Value = P_TUNGAY;
                    oracleCommand.Parameters.Add("P_DENNGAY", OracleDbType.Varchar2).Value = P_DENNGAY;
                    oracleCommand.Parameters.Add("P_MADONVI", OracleDbType.Varchar2).Value = "TH0302";
                    oracleCommand.Parameters.Add("P_MATK", OracleDbType.Varchar2).Value = P_MATK;

                    if (string.IsNullOrEmpty(P_MATKDU))
                    {
                        oracleCommand.Parameters.Add("P_MATKNO", OracleDbType.Varchar2).Value = P_MATK.Replace("MATK", "MATKNO");
                        oracleCommand.Parameters.Add("P_MATKCO", OracleDbType.Varchar2).Value = P_MATK.Replace("MATK", "MATKCO");
                    }
                    else
                    {
                        string value = P_MATK.Replace("MATK", "MATKNO") + " AND MATKCO LIKE '" + P_MATKDU + "%'";
                        string value2 = P_MATK.Replace("MATK", "MATKCO") + " AND MATKNO LIKE '" + P_MATKDU + "%'";
                        oracleCommand.Parameters.Add("P_MATKNO", OracleDbType.Varchar2).Value = value;
                        oracleCommand.Parameters.Add("P_MATKCO", OracleDbType.Varchar2).Value = value2;
                    }

                    oracleCommand.Parameters.Add("P_CTDT", OracleDbType.Varchar2).Value = P_CTDT;
                    oracleCommand.Parameters.Add("P_CTDTNO", OracleDbType.Varchar2).Value = P_CTDT.Replace("CTDOITUONG", "CTDOITUONGNO");
                    oracleCommand.Parameters.Add("P_CTDTCO", OracleDbType.Varchar2).Value = P_CTDT.Replace("CTDOITUONG", "CTDOITUONGCO");
                    using (OracleParameter oracleParameter = new OracleParameter("C", OracleDbType.RefCursor))
                    {
                        oracleParameter.Direction = ParameterDirection.Output;
                        oracleCommand.Parameters.Add(oracleParameter);
                        await oracleConnection.OpenAsync().ConfigureAwait(false);
                        using (OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(oracleCommand))
                        {
                            DataSet dataSet = new DataSet();
                            try
                            {
                                oracleDataAdapter.Fill(dataSet);
                            }
                            catch
                            {
                                return null;
                            }

                            DataTable table = dataSet.Tables[0];

                            //thêm dòng số dư đầu kỳ nếu kết quả trả về ko có
                            //vì những ncc mới, tính toán số dư đầu kỳ mà ko có thì sẽ ko có dòng dữ liệu số dư đầu kỳ
                            bool isContainsSDDK = table.AsEnumerable().Any(row => row.Field<decimal>("LOAIDULIEU") == 1);
                            if (!isContainsSDDK)
                            {
                                DataRow dr = table.NewRow();
                                dr["NOIDUNG"] = "Số dư đầu kỳ ";
                                dr["SDDK_NO"] = 0;
                                dr["SDDK_CO"] = 0;
                                dr["SPSTK_NO"] = 0;
                                dr["SPSTK_CO"] = 0;

                                table.Rows.InsertAt(dr, 0);
                            }
                            return table.ToDataTableSelectedColumns("NGAYHACHTOAN", "SOCTUGOC", "NGAYCTKT", "NOIDUNG", "MATKDU", "SPSTK_NO", "SPSTK_CO", "SDDK_NO", "SDDK_CO");
                        }
                    }
                }
            }
        }

        public static async Task<DataTable> BC_SO_CHI_TIET_TAI_KHOAN(string connectionString, DateTime startDate, DateTime endDate, string taiKhoan, string doiTuong)
        {
            return await BC_SKT_S38DN_BCMAIN_CORE(connectionString, startDate, endDate, taiKhoan, doiTuong).ConfigureAwait(false);
        }

        public static DataTable BC_TH_CONGNO_331(string connStr, string P_TUNGAY, string P_DENNGAY, string P_TABLENAME = "BC_S38_DN_V2", int P_NAM = 2012, string P_MATK = "MATK LIKE '331%' ", string P_CTDT = "1=1", string P_MADONVI = "TH0302", string GROUP_BY = "DMKHACHHANG_DMNHACUNGCAP_DMNHANVIEN", int TH_CT = 1)
        {

            string commandText = "PK_BC_S38_DN_V2.EXE_PK_S38_DN_V2";
            OracleConnection oracleConnection = new OracleConnection("Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.0.50)(PORT=1522)))(CONNECT_DATA=(SID=TBNETERP)(SERVER=DEDICATED)));User ID=TBNETERP;Password=TBNETERP");
            OracleCommand oracleCommand = new OracleCommand(commandText, oracleConnection);
            oracleCommand.CommandType = CommandType.StoredProcedure;
            oracleCommand.Parameters.Add("P_TABLENAME", OracleDbType.NVarchar2).Value = P_TABLENAME;
            oracleCommand.Parameters.Add("P_NAM", OracleDbType.NVarchar2).Value = P_NAM;
            oracleCommand.Parameters.Add("P_TUNGAY", OracleDbType.NVarchar2).Value = P_TUNGAY;
            oracleCommand.Parameters.Add("P_DENNGAY", OracleDbType.NVarchar2).Value = P_DENNGAY;
            oracleCommand.Parameters.Add("P_MADONVI", OracleDbType.NVarchar2).Value = P_MADONVI;
            oracleCommand.Parameters.Add("P_MATK", OracleDbType.NVarchar2).Value = P_MATK;
            oracleCommand.Parameters.Add("TH_CT", OracleDbType.NVarchar2).Value = TH_CT;
            oracleCommand.Parameters.Add("P_GROUP_BY", OracleDbType.NVarchar2).Value = GROUP_BY;
            oracleCommand.Parameters.Add("P_MATKNO", OracleDbType.NVarchar2).Value = P_MATK.Replace("MATK", "MATKNO");
            oracleCommand.Parameters.Add("P_MATKCO", OracleDbType.NVarchar2).Value = P_MATK.Replace("MATK", "MATKCO");
            oracleCommand.Parameters.Add("P_CTDT", OracleDbType.NVarchar2).Value = P_CTDT;
            oracleCommand.Parameters.Add("P_CTDTNO", OracleDbType.NVarchar2).Value = P_CTDT.Replace("CTDOITUONG", "CTDOITUONGNO");
            oracleCommand.Parameters.Add("P_CTDTCO", OracleDbType.NVarchar2).Value = P_CTDT.Replace("CTDOITUONG", "CTDOITUONGCO");
            OracleParameter oracleParameter = new OracleParameter("C", OracleDbType.RefCursor);
            oracleParameter.Direction = ParameterDirection.Output;
            oracleCommand.Parameters.Add(oracleParameter);
            oracleConnection.Open();
            OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(oracleCommand);
            DataSet dataSet = new DataSet();
            try
            {
                oracleDataAdapter.Fill(dataSet);
            }
            catch
            {
                oracleConnection.Close();
                return null;
            }
            finally
            {
                oracleConnection.Close();
                oracleConnection.Dispose();
            }
            return dataSet.Tables[0];
        }

        public static DataTable BC_SOCHU_T(string connStr, string P_TUNGAY, string P_DENNGAY, string P_CTDT, string P_TABLENAME = "BC_S38_DN", string P_NAM = "0", string P_MATK = "MATK LIKE '331%' ", string P_MADONVI = "TH0302")
        {
            P_CTDT = string.Format("CTDOITUONG LIKE '%DMNHACUNGCAP:{0};%' ", P_CTDT);

            string commandText = "PK_BC_S38_DN.EXEC_S38DN";
            OracleConnection oracleConnection = new OracleConnection(connStr);
            OracleCommand oracleCommand = new OracleCommand(commandText, oracleConnection);
            oracleCommand.CommandType = CommandType.StoredProcedure;
            oracleCommand.Parameters.Add("P_TABLENAME", OracleDbType.Varchar2).Value = P_TABLENAME;
            oracleCommand.Parameters.Add("P_NAM", OracleDbType.Varchar2).Value = 2012;
            oracleCommand.Parameters.Add("P_TUNGAY", OracleDbType.Varchar2).Value = P_TUNGAY;
            oracleCommand.Parameters.Add("P_DENNGAY", OracleDbType.Varchar2).Value = P_DENNGAY;
            oracleCommand.Parameters.Add("P_MADONVI", OracleDbType.Varchar2).Value = P_MADONVI;
            oracleCommand.Parameters.Add("P_MATK", OracleDbType.Varchar2).Value = P_MATK;
            oracleCommand.Parameters.Add("P_MATKNO", OracleDbType.Varchar2).Value = P_MATK.Replace("MATK", "MATKNO");
            oracleCommand.Parameters.Add("P_MATKCO", OracleDbType.Varchar2).Value = P_MATK.Replace("MATK", "MATKCO");
            oracleCommand.Parameters.Add("P_CTDT", OracleDbType.Varchar2).Value = P_CTDT;
            oracleCommand.Parameters.Add("P_CTDTNO", OracleDbType.Varchar2).Value = P_CTDT.Replace("CTDOITUONG", "CTDOITUONGNO");
            oracleCommand.Parameters.Add("P_CTDTCO", OracleDbType.Varchar2).Value = P_CTDT.Replace("CTDOITUONG", "CTDOITUONGCO");
            OracleParameter oracleParameter = new OracleParameter("C", OracleDbType.RefCursor);
            oracleParameter.Direction = ParameterDirection.Output;
            oracleCommand.Parameters.Add(oracleParameter);
            oracleConnection.Open();
            OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(oracleCommand);
            DataSet dataSet = new DataSet();
            try
            {
                oracleDataAdapter.Fill(dataSet);
            }
            catch
            {
                oracleConnection.Close();
                return null;
            }
            finally
            {
                oracleConnection.Close();
                oracleConnection.Dispose();
            }
            return dataSet.Tables[0];
        }

        public static Workbook FileExcelImportNHAPHANG()
        {
            Workbook workbook = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet sheet1 = workbook.Worksheets[1];
            sheet1.Name = "MAUHOADON";
            sheet1.Range["a1"].SetHeaderText("SOHOADON", "MAVTU", "TENVTU", "TONCUOIKYSL", "DON GIA", "TONCUOIKYGT", "MANCC", "NGAYHD", "KYHIEUHD");

            Worksheet sheet2 = workbook.Worksheets.Add();
            sheet2.Name = "info";


            return workbook;
        }

        public static async Task<DataTable> DataCheckInvoiceExist(string connectionString, string serialNumer, string taxIdentificationNumber, string invoiceNumber)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            #region sb
            sb.AppendLine($"Select");
            sb.AppendLine($"Vattugd.Kyhieuhoadongd,");
            sb.AppendLine($"Vattugd.Sohoadongd,");
            sb.AppendLine($"Vattugd.Ngayhoadongd,");
            sb.AppendLine($"dmkhachhang.masothue,");
            sb.AppendLine($"dmkhachhang.Tenkhachhang,");
            sb.AppendLine($"Vattugd.Sumtienhang,");
            sb.AppendLine($"Vattugd.Sumtienvat,");
            sb.AppendLine($"Vattugd.Sumtienhang+Vattugd.Sumtienvat as Total");
            sb.AppendLine($"From");
            sb.AppendLine($"Vattugd");
            sb.AppendLine($"Join Dmkhachhang");
            sb.AppendLine($"On");
            sb.AppendLine($"vattugd.makhachang = dmkhachhang.makhachhang");
            sb.AppendLine($"Where");
            sb.AppendLine($"Vattugd.Maptnx in ('NMUA','NDV')");
            sb.AppendLine($"And Vattugd.Kyhieuhoadongd = '{serialNumer}'");
            sb.AppendLine($"And dmkhachhang.Masothue = '{taxIdentificationNumber}'");
            //sb.AppendLine($"And Vattugd.Sohoadongd Like '%{invoiceNumber}%'");
            sb.AppendLine($"And TRIM(LEADING '0' from Vattugd.Sohoadongd) = '{invoiceNumber}'");
            sb.AppendLine($"and vattugd.trangthai = '10'");
            #endregion

            return await ADO.GetDataTableFromOracleServerAsync(connectionString, sb.ToString()).ConfigureAwait(false);

        }

        public static async Task<int> InvoiceExist(string connectionString, string serialNumer, string taxIdentificationNumber, string invoiceNumber)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            #region sb
            sb.AppendLine($"Select");
            sb.AppendLine($"Vattugd.Kyhieuhoadongd,");
            sb.AppendLine($"Vattugd.Sohoadongd,");
            sb.AppendLine($"Vattugd.Ngayhoadongd,");
            sb.AppendLine($"dmkhachhang.masothue,");
            sb.AppendLine($"dmkhachhang.Tenkhachhang,");
            sb.AppendLine($"Vattugd.Sumtienhang,");
            sb.AppendLine($"Vattugd.Sumtienvat,");
            sb.AppendLine($"Vattugd.Sumtienhang+Vattugd.Sumtienvat as Total");
            sb.AppendLine($"From");
            sb.AppendLine($"Vattugd");
            sb.AppendLine($"Join Dmkhachhang");
            sb.AppendLine($"On");
            sb.AppendLine($"vattugd.makhachang = dmkhachhang.makhachhang");
            sb.AppendLine($"Where");
            sb.AppendLine($"Vattugd.Maptnx in ('NMUA','NDV')");
            sb.AppendLine($"And Vattugd.Kyhieuhoadongd = '{serialNumer}'");
            sb.AppendLine($"And dmkhachhang.Masothue = '{taxIdentificationNumber}'");
            //sb.AppendLine($"And Vattugd.Sohoadongd Like '%{invoiceNumber}%'");
            sb.AppendLine($"And TRIM(LEADING '0' from Vattugd.Sohoadongd) = '{invoiceNumber.TrimStart(new Char[] { '0' })}'");
            sb.AppendLine($"and vattugd.trangthai = '10'");
            #endregion

            DataTable dataTable = await ADO.GetDataTableFromOracleServerAsync(connectionString, sb.ToString()).ConfigureAwait(false);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable.Rows.Count;
            }

            return 0;
        }

        public static async Task<DataTable> GetInvoice(string connectionString, string serialNumer, string taxIdentificationNumber, string invoiceNumber)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            #region sb
            sb.AppendLine($"Select");
            sb.AppendLine($"Vattugd.Kyhieuhoadongd,");
            sb.AppendLine($"Vattugd.Sohoadongd,");
            sb.AppendLine($"Vattugd.Ngayhoadongd,");
            sb.AppendLine($"dmkhachhang.masothue,");
            sb.AppendLine($"dmkhachhang.Tenkhachhang,");
            sb.AppendLine($"Vattugd.Sumtienhang,");
            sb.AppendLine($"Vattugd.Sumtienvat,");
            sb.AppendLine($"Vattugd.Sumtienhang+Vattugd.Sumtienvat as Total");
            sb.AppendLine($"From");
            sb.AppendLine($"Vattugd");
            sb.AppendLine($"Join Dmkhachhang");
            sb.AppendLine($"On");
            sb.AppendLine($"vattugd.makhachang = dmkhachhang.makhachhang");
            sb.AppendLine($"Where");
            sb.AppendLine($"Vattugd.Maptnx in ('NMUA','NDV')");
            sb.AppendLine($"And Vattugd.Kyhieuhoadongd = '{serialNumer}'");
            sb.AppendLine($"And dmkhachhang.Masothue = '{taxIdentificationNumber}'");
            sb.AppendLine($"And TRIM(LEADING '0' from Vattugd.Sohoadongd) = '{invoiceNumber.TrimStart(new Char[] { '0' })}'");
            sb.AppendLine($"and vattugd.trangthai = '10'");
            #endregion

            DataTable dataTable = await ADO.GetDataTableFromOracleServerAsync(connectionString, sb.ToString()).ConfigureAwait(false);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable;
            }

            return null;
        }
        public static async Task CheckEinvoiceTCT(Worksheet worksheet, string connectionString)
        {
            var table1 = worksheet.UsedRange.ToListObject();
            table1.ListColumns.Add(table1.ListColumns["Tổng tiềnchiết khấuthương mại"].Index + 1).Name = "TienSauChietKhau";
            table1.GetCell(1, "TienSauChietKhau").Formula = $"={table1.GetCell(1, "Tổng tiềnchưa thuế").Address2()}-{table1.GetCell(1, "Tổng tiềnchiết khấuthương mại").Address2()}";
            table1.ListColumns["TienSauChietKhau"].DataBodyRange.FillDownFormula(false);

            table1.ListColumns.AddRange("Kết quả", "SoHoaDon", "NgayHoaDon", "Mst", "TenNCC", "SoTien", "TienVat", "TongTienThanhToan", "CheckSoTienChuaThue1", "CheckSoTienSauChietKhau2", "CheckSoTien", "CheckVat", "CheckTongTienThanhToan");

            for (int i = 1; i <= table1.ListRows.Count; i++)
            {
                Notification.Wait.Show($"{i}/{table1.ListRows.Count}");

                string kyhieu = table1.GetCell(i, "Ký hiệumẫu số").GetStringValue() + table1.GetCell(i, "Ký hiệuhóa đơn").GetStringValue();
                string masothue = table1.GetCell(i, "Thông tin hóa đơn").GetStringValue().GetStringBetween("MST người bán: ", "Tên người bán");
                string sohoadon = table1.GetCell(i, "Số hóa đơn").GetStringValue();

                DataTable dt = await VIETSEA.TBNETERP.DataCheckInvoiceExist(connectionString, kyhieu, masothue, sohoadon);
                if (dt.HasData())
                {
                    if (dt.Rows.Count == 1)
                    {
                        dt.CopyToRange(table1.GetCell(i, "Kết quả"), false);

                    }
                    if (dt.Rows.Count > 1)
                    {
                        System.Windows.Forms.MessageBox.Show($"Có nhiều hơn 1 dòng dữ liệu thỏa mãn hóa đơn {kyhieu}-{sohoadon}");
                    }
                }
                else
                {
                    table1.GetCell(i, "Kết quả").Value2 = "Không có dữ liệu";
                }
            }

            Notification.Wait.Close();

            table1.ListColumns["SoTien"].DataBodyRange.ConvertToValue();
            table1.ListColumns["TienVat"].DataBodyRange.ConvertToValue();
            table1.ListColumns["TongTienThanhToan"].DataBodyRange.ConvertToValue();

            table1.GetCell(1, "CheckSoTienChuaThue1").Formula = $"={table1.GetCell(1, "Tổng tiềnchưa thuế").Address2()}={table1.GetCell(1, "SoTien").Address2()}";
            table1.GetCell(1, "CheckSoTienSauChietKhau2").Formula = $"={table1.GetCell(1, "TienSauChietKhau").Address2()}={table1.GetCell(1, "SoTien").Address2()}";
            table1.GetCell(1, "CheckSoTien").Formula = $"=OR({table1.GetCell(1, "CheckSoTienChuaThue1").Address2()},{table1.GetCell(1, "CheckSoTienSauChietKhau2").Address2()})";
            table1.GetCell(1, "CheckVat").Formula = $"={table1.GetCell(1, "Tổng tiền thuế").Address2()}={table1.GetCell(1, "TienVat").Address2()}";
            table1.GetCell(1, "CheckTongTienThanhToan").Formula = $"={table1.GetCell(1, "Tổng tiềnthanh toán").Address2()}={table1.GetCell(1, "TongTienThanhToan").Address2()}";

            table1.ListColumns["CheckSoTienChuaThue1"].DataBodyRange.FillDownFormula(false);
            table1.ListColumns["CheckSoTienSauChietKhau2"].DataBodyRange.FillDownFormula(false);
            table1.ListColumns["CheckSoTien"].DataBodyRange.FillDownFormula(false);
            table1.ListColumns["CheckVat"].DataBodyRange.FillDownFormula(false);
            table1.ListColumns["CheckTongTienThanhToan"].DataBodyRange.FillDownFormula(false);

            //table1.Range.Columns.AutoFit();
            table1.Unlist();
            MsgBox.Show("Kiểm tra xong!");

        }


        public static async Task<DataTable> Data_ToKhai_BanRa(DateTime startDate, DateTime endDate)
        {
            string q = string.Format(@"Select
--Vattugd.soctugoc,
Vattugdct.Mavtu,
Dmvthhdv.Tenvtu,
vattugdct.mavat,
Round(Sum(Vattugdct.Tienhang),0) as tienhang,
Round(Sum(Vattugdct.Tienvat),0) As Tienvat,
Round(Sum(Vattugdct.Tienhang)+Sum(Vattugdct.Tienvat),0) as tongtien
From
Vattugdct
Join
Vattugd
On
Vattugdct.Mavattugdpk = Vattugd.Mavattugdpk
Join
Dmvthhdv
On
vattugdct.mavtu = dmvthhdv.mavtu
Where
Vattugd.Maptnx In ('XBAN','XDV','XBAN','XBANSAPA','XBANBT')
--and Vattugdct.Mavat = '8'
And Vattugd.Ngayhachtoan >= '{0}'
And Vattugd.Ngayhachtoan <= '{1}'
and vattugd.trangthai = '10'
GROUP BY 
--Vattugd.soctugoc, 
Vattugdct.Mavtu, Dmvthhdv.Tenvtu, vattugdct.mavat
order by dmvthhdv.tenvtu", startDate.ToString("dd-MMM-yy"), endDate.ToString("dd-MMM-yy"));

            return await ADO.GetDataTableFromOracleServerAsync(VIETSEA.OracleServer.GetConnectionString, q).ConfigureAwait(false);

        }


        public static async Task<string> GetMaKhachHang(string connectionString, string masothue)
        {
            string q = @"Select
    DMKHACHHANG.MAKHACHHANG
From
    DMKHACHHANG
Where
    DMKHACHHANG.MANHOMKHACHANG Like '%NCC%'";

            DataTable dataTable = await ADO.GetDataTableFromOracleServerAsync(connectionString, q).ConfigureAwait(false);
            if (dataTable == null || dataTable.Rows.Count == 0)
            {
                return string.Empty;
            }

            return dataTable.Rows[0][0].ToString();
        }

        public static async Task<UserClass.Supplier> GetVietseaKhachHang(string connectionString, string masothue)
        {
            string q = $"Select * From DMKHACHHANG Where DMKHACHHANG.MASOTHUE = '{masothue}' and MANHOMKHACHANG LIKE '%NCC_01%'";

            DataTable dataTable = await ADO.GetDataTableFromOracleServerAsync(connectionString, q).ConfigureAwait(false);
            if (dataTable == null || dataTable.Rows.Count == 0)
            {
                return null;
            }

            UserClass.Supplier vietseaKhachHang = new UserClass.Supplier()
            {
                ID = dataTable.Rows[0][VIETSEA.DataBase.Tables.MAKHACHHANG.Columns.MAKHACHHANG].ToString(),
                Name = dataTable.Rows[0][VIETSEA.DataBase.Tables.MAKHACHHANG.Columns.TENTOCHU].ToString(),
                MST = dataTable.Rows[0][VIETSEA.DataBase.Tables.MAKHACHHANG.Columns.MASOTHUE].ToString(),
            };

            return vietseaKhachHang;
        }

        public static async Task<DataTable> GetDTGV_QUYETTOAN(string connectionString, DateTime startDate, DateTime endDate)
        {
            //https://stackoverflow.com/questions/23935716/specified-cast-is-not-valid-when-populating-datatable-from-oracledataadapter-f

            string q = string.Format(@"Select
    VATTUGD.NGAYHACHTOAN,
    VATTUGD.SOCTUGOC,
    Sum(round(VATTUGDCT.SOLUONG,8)) As Sum_SOLUONG,
    Sum(round(VATTUGDCT.TIENHANG,8)) As Sum_TIENHANG,
    Sum(round(VATTUGDCT.GIAVONHANGBAN * VATTUGDCT.SOLUONG,8)) As Sum_GIAVONHANGBAN,
    Sum(round(VATTUGDCT.TIENHANG,8)) - Sum(round(VATTUGDCT.GIAVONHANGBAN * VATTUGDCT.SOLUONG,8)) As CHENH_LECH
From
    VATTUGD Inner Join
    VATTUGDCT On VATTUGD.MAVATTUGDPK = VATTUGDCT.MAVATTUGDPK
Where
    VATTUGD.NGAYHACHTOAN >= '{0}' And
    VATTUGD.NGAYHACHTOAN <= '{1}' And
    VATTUGD.MAPTNX In ('XBAN', 'XTRA1', 'XDV') And
    VATTUGD.TRANGTHAI = 10
Group By
    VATTUGD.NGAYHACHTOAN,
    VATTUGD.SOCTUGOC
Order By
    VATTUGD.NGAYHACHTOAN,
    VATTUGD.SOCTUGOC", startDate.ToString("dd-MMM-yy"), endDate.ToString("dd-MMM-yy"));
            DataTable dataTable = await ADO.GetDataTableFromOracleServerAsync(connectionString, q).ConfigureAwait(false);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable;
            }

            return null;

        }

        public static async Task<DataTable> GetNXT(string connectionString, DateTime date)
        {
            string q = $"SELECT * FROM XNT_{date.ToString("ddMMyyyy")}";
            DataTable dataTable = await ADO.GetDataTableFromOracleServerAsync(connectionString, q).ConfigureAwait(false);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                return dataTable;
            }

            return null;
        }

        public static async Task<DataTable> GetNXT_4COT_DAUKY(string connectionString, DateTime startDate, DateTime endDate)
        {
            string tableNameDauKy = $"XNT_{startDate:ddMMyyyy}";
            string query_DauKy = string.Format(@"Select
    {0}.MAVTU,
    Sum({0}.TONDAUKYSL) As TONDAUKYSL,
    Sum({0}.TONDAUKYGT) As TONDAUKYGT
From
    {0}
Group By
    {0}.MAVTU
Order By
    {0}.MAVTU", tableNameDauKy);

            DataTable dtDauKy = await ADO.GetDataTableFromOracleServerAsync(connectionString, query_DauKy).ConfigureAwait(false);
            return dtDauKy;
        }
        //nhap xuat
        public static async Task<DataTable> GetNXT_4COT_TRONGKY(string connectionString, DateTime startDate, DateTime endDate)
        {
#pragma warning disable CS0219 // The variable 'query_Nhap' is assigned but its value is never used
            string query_Nhap = @"Select
    {0}.MAVTU,
    Sum(({0}.NHAPMUASL) + ({0}.NKHAUSL) + ({0}.NKHACSL) + ({0}.NCHUYENKHOSL) +
    ({0}.NTHANHPHAMSL) + ({0}.NDODANGSL) + ({0}.NTRALAISL) + ({0}.NDIEUCHINHSL)) As
    NHAPMUASL,
    Sum(({0}.NHAPMUAGT) + ({0}.NKHAUGT) + ({0}.NKHACGT) + ({0}.NCHUYENKHOGT) +
    ({0}.NTHANHPHAMGT) + ({0}.NDODANGGT) + ({0}.NTRALAIGT) + ({0}.NDIEUCHINHGT)) As
    NHAPMUAGT,
    Sum(({0}.XCHUYENKHOSL) + ({0}.XKHACSL) + ({0}.XBANSL) + ({0}.XSXSL) +
    ({0}.XTAICHESL) + ({0}.XDICHDANHSL) + ({0}.XTRALAISL) + ({0}.XDIEUCHINHSL)) As
    XBANSL,
    Sum(({0}.XCHUYENKHOGT) + ({0}.XKHACGT) + ({0}.XBANGT) + ({0}.XSXGT) +
    ({0}.XTAICHEGT) + ({0}.XDICHDANHGT) + ({0}.XTRALAIGT) + ({0}.XDIEUCHINHGT)) As
    XBANGT
From
    {0}
Group By
    {0}.MAVTU
Having
    Sum(({0}.NHAPMUASL) + ({0}.NKHAUSL) + ({0}.NKHACSL) + ({0}.NCHUYENKHOSL) +
    ({0}.NTHANHPHAMSL) + ({0}.NDODANGSL) + ({0}.NTRALAISL) + ({0}.NDIEUCHINHSL)) <>
    0 And
    Sum(({0}.XCHUYENKHOSL) + ({0}.XKHACSL) + ({0}.XBANSL) + ({0}.XSXSL) +
    ({0}.XTAICHESL) + ({0}.XDICHDANHSL) + ({0}.XTRALAISL) + ({0}.XDIEUCHINHSL)) <> 0
Order By
    {0}.MAVTU";
#pragma warning restore CS0219 // The variable 'query_Nhap' is assigned but its value is never used

            List<string> queryUnionAllSumNhapXuat = new List<string>();
            for (DateTime eachDate = startDate; eachDate <= endDate; eachDate = eachDate.AddDays(1))
            {
                string tablename = $"XNT_{eachDate:ddMMyyyy}";
                string q_sum = String.Format(@"Select
    {0}.MAVTU,
    Sum(({0}.NHAPMUASL) + ({0}.NKHAUSL) + ({0}.NKHACSL) + ({0}.NCHUYENKHOSL) +
    ({0}.NTHANHPHAMSL) + ({0}.NDODANGSL) + ({0}.NTRALAISL) + ({0}.NDIEUCHINHSL)) As
    NHAPMUASL,
    Sum(({0}.NHAPMUAGT) + ({0}.NKHAUGT) + ({0}.NKHACGT) + ({0}.NCHUYENKHOGT) +
    ({0}.NTHANHPHAMGT) + ({0}.NDODANGGT) + ({0}.NTRALAIGT) + ({0}.NDIEUCHINHGT)) As
    NHAPMUAGT,
    Sum(({0}.XCHUYENKHOSL) + ({0}.XKHACSL) + ({0}.XBANSL) + ({0}.XSXSL) +
    ({0}.XTAICHESL) + ({0}.XDICHDANHSL) + ({0}.XTRALAISL) + ({0}.XDIEUCHINHSL)) As
    XBANSL,
    Sum(({0}.XCHUYENKHOGT) + ({0}.XKHACGT) + ({0}.XBANGT) + ({0}.XSXGT) +
    ({0}.XTAICHEGT) + ({0}.XDICHDANHGT) + ({0}.XTRALAIGT) + ({0}.XDIEUCHINHGT)) As
    XBANGT
From
    {0}
Group By
    {0}.MAVTU", tablename);
                queryUnionAllSumNhapXuat.Add(q_sum);
            }

            string query_sum_nhap_xuat = string.Join(" union all ", queryUnionAllSumNhapXuat);

            string finalQuery = $"select A.MAVTU,SUM(A.NHAPMUASL),SUM(A.NHAPMUAGT),SUM(XBANSL),SUM(XBANGT) " +
                $"from ({query_sum_nhap_xuat}) A " +
                $"GROUP BY A.MAVTU";
            DataTable dtSumNhapXuat = await ADO.GetDataTableFromOracleServerAsync(connectionString, finalQuery).ConfigureAwait(false);
            return dtSumNhapXuat;
        }

        public static async Task<DataTable> GetNXT_4COT_CUOIKY(string connectionString, DateTime startDate, DateTime endDate)
        {

            string tableNameCuoiKy = $"XNT_{endDate:ddMMyyyy}";
            string query_CuoiKy = string.Format(@"Select
    {0}.MAVTU,
    Sum({0}.TONCUOIKYSL) As TONCUOIKYSL,
    Sum({0}.TONCUOIKYGT) As TONCUOIKYGT
From
    {0}
Group By
    {0}.MAVTU
Order By
    {0}.MAVTU", tableNameCuoiKy);

            DataTable dtCuoiKy = await ADO.GetDataTableFromOracleServerAsync(connectionString, query_CuoiKy).ConfigureAwait(false);
            return dtCuoiKy;

        }

        public static async Task<DataTable> GET_DMVTHHDV(string connectionString)
        {
            string q = @"Select
    DMVTHHDV.MAVTU,
    DMVTHHDV.TENVTU,
    DMVTHHDV.MADVTINH
From
    DMVTHHDV";

            return await ADO.GetDataTableFromOracleServerAsync(connectionString, q).ConfigureAwait(false);
        }

        public static async Task<DataTable> GET_DMDONVITINHDV(string connectionString)
        {
            string q = @"Select
    DMDONVITINHDV.MADVTINH,
    DMDONVITINHDV.DONVILE
From
    DMDONVITINHDV";

            return await ADO.GetDataTableFromOracleServerAsync(connectionString, q).ConfigureAwait(false);
        }



    }
}
