﻿using System.Threading.Tasks;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.VIETSEA
{
    public static class OracleServer
    {
        public static string GetConnectionString
        {
            get
            {
                Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder sb = new Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder();
                string ip = MyClass.AddinConfig.Oralce_IP;
                string port = MyClass.AddinConfig.Oralce_Port;
                sb.DataSource = $"{ip}:{port}/TBNETERP";
                sb.UserID = "TBNETERP";
                sb.Password = "TBNETERP";
                return sb.ConnectionString;
            }
        }


        public static bool CheckIfInvoiceExist(string MaNCC, string kyhieuHD, string SoHoaDon, out string userTao)
        {
            userTao = string.Empty;
            bool result = false;
            string query = string.Format(@"Select *
                                From Vattugd
                                Where Maptnx ='NMUA'
                                And Soctugoc = '{0}'
                                And Makhachang = '{1}'
                                and kyhieuhoadongd = '{2}'", SoHoaDon, MaNCC, kyhieuHD);
            DataTable dataTable = ADO.GetDataTableFromOracleServerAsync(ExcelAddIn1.VIETSEA.TBNETERP.ConnectionString_DATPHAT, query).Result;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                userTao = dataTable.Rows[0]["USERTAO"].ToString() + "|" + dataTable.Rows[0]["USERSUA"].ToString();
                result = true;
            }
            return result;
        }

        public static async Task<DataTable> GetProductCodeInfo(string connectionString, System.DateTime date)
        {
            string dateString = date.ToString("ddMMyyyy");
            string kho = $"XNT_{dateString}";

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine($"select");
            sb.AppendLine($"{kho}.makhohang,");
            sb.AppendLine($"{kho}.mavtu,");
            sb.AppendLine($"dmvthhdv.tenvtu,");
            sb.AppendLine($"{kho}.toncuoikysl,");
            sb.AppendLine($"{kho}.giavon");
            sb.AppendLine($"from");
            sb.AppendLine($"{kho}");
            sb.AppendLine($"Join Dmvthhdv on dmvthhdv.mavtu = {kho}.mavtu");
            sb.AppendLine($"where");
            sb.AppendLine($"{kho}.toncuoikysl>0");
            sb.AppendLine($"order by {kho}.mavtu asc, {kho}.toncuoikysl desc");

            return await ADO.GetDataTableFromOracleServerAsync(connectionString, sb.ToString()).ConfigureAwait(false);
        }

        public static async Task<DataTable> GetDMKHO(string connectionString)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append($"select makho from dmkhodv");
            return await ADO.GetDataTableFromOracleServerAsync(connectionString, sb.ToString()).ConfigureAwait(false);
        }

        public static async Task<string> GetTenVatTu(string connectionString, string Mavtu)
        {
            string q = $"Select DMVTHHDV.TENVTU From DMVTHHDV Where DMVTHHDV.MAVTU = '{Mavtu}'";
            DataTable dataTable = await ADO.GetDataTableFromOracleServerAsync(connectionString, q).ConfigureAwait(false);
            if (!dataTable.HasData())
            {
                return string.Empty;
            }

            string output = dataTable.Rows[0][0].ToString();
            return output;
        }









    }
}
