﻿using ExcelAddIn1.AutoTest.WinAppDriver.Vietsea;
using Microsoft.Office.Interop.Excel;
using VSTOLib.Extensions;

namespace ExcelAddIn1.VIETSEA.ExcelFile
{
    public static class ImportNhapHang
    {

        public const string MANCC = "MANCC";
        public const string NGAYHACHTOAN = "NGAYHACHTOAN";
        public const string KHO = "KHO";
        public const string THANHTIEN = "THANHTIEN";
        public const string VAT = "VAT";
        public const string TONGTIEN = "TONGTIEN";
        public const string TIENVAT = "TIENVAT";
        public const string NGAYHOADON = "NGAYHOADON";
        public const string KYHIEU = "KYHIEU";
        public const string SOHOADON = "SOHOADON";
        public const string SOBANIN = "SOBANIN";
        public const string PRINTTYPE = "PRINTTYPE";


        private static Workbook CreateBlankImportWorkbook()
        {
            Workbook workbook = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet sheetData = workbook.Worksheets[1];
            sheetData.Name = "MAUHOADON";
            sheetData.Range["a1"].SetHeaderText("SOHOADON", "MAVTU", "TENVTU", "TONCUOIKYSL", "DON GIA", "TONCUOIKYGT", "MANCC", "NGAYHD", "KYHIEUHD");

            return workbook;
        }

        public static Workbook CreateExcelWorkbook(UserClass.InvoiceInfoPackage package)
        {
            Workbook workbook = CreateBlankImportWorkbook();

            workbook.GetSheet(MANCC).Range["a1"].Value2 = package.MaNCC;
            workbook.GetSheet(NGAYHACHTOAN).Range["a1"].Value2 = package.TranDate;
            workbook.GetSheet(KHO).Range["a1"].Value2 = package.MaKhoNhap;
            workbook.GetSheet(THANHTIEN).Range["a1"].Value2 = package.InvoiceAmount.ToString();
            workbook.GetSheet(TIENVAT).Range["a1"].Value2 = package.InvoiceVat.ToString();
            workbook.GetSheet(VAT).Range["a1"].Value2 = package.VatRate.ToString();
            workbook.GetSheet(NGAYHOADON).Range["a1"].Value2 = "'" + package.InvoiceDate;
            workbook.GetSheet(KYHIEU).Range["a1"].Value2 = package.InvoiceSerialNumber;
            workbook.GetSheet(SOHOADON).Range["a1"].Value2 = "'" + package.InvoiceNumber;
            workbook.GetSheet(SOBANIN).Range["a1"].Value2 = package.PrintCopies;
            workbook.GetSheet(PRINTTYPE).Range["a1"].Value2 = package.PrintType.ToString();

            return workbook;
        }

        public static Workbook CreateImportWorkbook(UserClass.InvoiceInfoPackage package)
        {
            Workbook workbook = CreateBlankImportWorkbook();

            Worksheet worksheet = workbook.GetNewSheet("info");

            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, MANCC, package.MaNCC);
            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, NGAYHACHTOAN, package.TranDate);
            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, KHO, package.MaKhoNhap);
            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, THANHTIEN, package.InvoiceAmount.ToString());
            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, TIENVAT, package.InvoiceVat.ToString());
            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, VAT, package.VatRate.ToString());
            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, NGAYHOADON, "'" + package.InvoiceDate);
            //worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, NGAYHOADON, "'" + Flows._dumbDate);
            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, KYHIEU, package.InvoiceSerialNumber);
            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, SOHOADON, "'" + package.InvoiceNumber);
            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, SOBANIN, package.PrintCopies);
            worksheet.NewRightCellAtColumn(1).SetTextArray(VSTOLib.SetTextDirection.Column, PRINTTYPE, package.PrintType.ToString());


            return workbook;
        }


    }
}
