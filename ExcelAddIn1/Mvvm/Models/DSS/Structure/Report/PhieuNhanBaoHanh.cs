﻿namespace ExcelAddIn1.Mvvm.Models.DSS.Structure.Report
{
    public class PhieuNhanBaoHanh
    {
        public string STT { get; set; }
        public  string MaHang { get; set; }
        public  string TenHang { get; set; }
        public  string DonViTinh { get; set; }
        public  double SoLuongXuat { get; set; }

    }
}
