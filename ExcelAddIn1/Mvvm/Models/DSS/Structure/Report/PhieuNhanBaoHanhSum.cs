﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.Mvvm.Models.DSS.Structure.Report
{
    public class PhieuNhanBaoHanhSum
    {
        public string STT { get; set; }
        public string MaHang { get; set; }
        public string TenHang { get; set; }
        public string DonViTinh { get; set; }
        public double TongSoLuongXuat { get; set; }
    }
}
