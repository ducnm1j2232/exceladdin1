﻿namespace ExcelAddIn1.Mvvm.Models.DSS.Structure.WSheet
{
    public class SheetOutputRandomData
    {
        public const string SheetName = "outputRandomData";
        public const string STT = "STT";
        public const string SoChungTuSource = "SoChungTuSource";
        public const string NgayChungTuSource = "NgayChungTuSource";
        public const string MaKhachHang = "MaKhachHang";

        public const string MaHang = "MaHang";
        public const string TenHang = "TenHang";
        public const string DonViTinh = "DonViTinh";
        public const string SoLuongXuat = "SoLuongXuat";
        public const string Loi = "Loi";
        public const string Note = "Note";




    }
}
