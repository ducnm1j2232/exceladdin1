﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.Mvvm.Models.DSS.Structure.WSheet
{
    public static class SheetBangMa
    {
        public const string MAHANG = "MAHANG";
        public const string TENHANG = "TENHANG";
        public const string LOAIVT = "LOAIVT";
        public const string DONVITINH = "DONVITINH";
    }
}
