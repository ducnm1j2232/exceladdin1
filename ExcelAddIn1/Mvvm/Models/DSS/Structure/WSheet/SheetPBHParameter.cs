﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.Mvvm.Models.DSS.Structure.WSheet
{
    public class SheetPBHParameter
    {
        public const string SheetName = "PBHparameter";
        public const string SoPhieuAddress = "B1";
        public const string SoChungTuAddress= "B2";
        public const string NgayChungTuAddress = "B3";
        public const string MaKhachHangAddress = "B4";

    }
}
