﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.Mvvm.Models.DSS.Structure.WSheet
{
    public class SheetDMKhachHang
    {
        public const string MAKHACHHANG = "MAKHACHHANG";
        public const string TENKHACHHANG = "TENKHACHHANG";
        public const string MST = "MST";
        public const string DIACHI = "DIACHI";
        public const string SODIENTHOAI = "SODIENTHOAI";
    }
}
