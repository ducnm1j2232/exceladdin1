﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.Mvvm.Models.DSS.Structure.WBook
{
    public class WorkbookBaoHanhSheetNames
    {
        public const string BangMa = "BANG MA";
        public const string LoiSP = "LOI SP";
        public const string DMKhachHang = "DM khach hang";
        public const string DMNote = "DMNote";
    }
}
