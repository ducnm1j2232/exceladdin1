﻿namespace ExcelAddIn1.Mvvm.Models.DSS
{
    public class PhieuBaoHanhItemMaHang
    {
        private string _MaHang;
        private string _TenHang;
        private string _DonViTinh;
        private int _SoLuongXuat;
        private string _Loi;
        private string _Note;
        public enum _CreatedBy { Original, NewRandom, AddQuantity }


        public string MaHang
        {
            get { return _MaHang; }
            set { _MaHang = value; }
        }

        public string TenHang
        {
            get { return _TenHang; }
            set { _TenHang = value; }
        }

        public string DonViTinh
        {
            get { return _DonViTinh; }
            set { _DonViTinh = value; }
        }

        public int SoLuongXuat
        {
            get { return _SoLuongXuat; }
            set { _SoLuongXuat = value; }
        }

        public _CreatedBy CreatedBy
        {
            get;
            set;
        }

        public string Loi
        {
            get { return _Loi; }
            set { _Loi = value; }
        }

        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }



    }
}
