﻿using System;
using System.Collections.Generic;
using System.Data;

namespace ExcelAddIn1.Mvvm.Models.DSS
{
    public class PhieuBaoHanh
    {

        private int _STT;
        private string _SoChungTuSource;
        private int _MaxItemCount = 5;
        private bool _HasRandomNewMaHang;
        private string _MaKhachHang;
        private DateTime _NgayChungTuSource;
        private DateTime _NgayXacNhanLoiBaoHanh;
        private DateTime _NgayDeNghiXuatKho;
        private DateTime _NgayTiepNhanBaoHanh;
        private List<PhieuBaoHanhItemMaHang> _Items = new List<PhieuBaoHanhItemMaHang>();

        public int STT
        {
            get { return _STT; }
            set { _STT = value; }
        }

        public string SoChungTuSource
        {
            get { return _SoChungTuSource; }
            set { _SoChungTuSource = value; }
        }

        public int MaxItemCount
        {
            get { return _MaxItemCount; }
            set { _MaxItemCount = value; }
        }

        public bool HasRandomNewMaHang
        {
            get { return _HasRandomNewMaHang; }
            set { _HasRandomNewMaHang = value; }
        }

        public string MaKhachHang
        {
            get { return _MaKhachHang; }
            set { _MaKhachHang = value; }
        }

        public DateTime NgayChungTuSource
        {
            get { return _NgayChungTuSource; }
            set { _NgayChungTuSource = value; }
        }

        public DateTime NgayXacNhanLoiBaoHanh
        {
            get { return NgayChungTuSource.AddDays(-3); }
        }

        public DateTime NgayDeNghiXuatKho
        {
            get { return NgayChungTuSource.AddDays(-2); }
        }

        public DateTime NgayTiepNhanBaoHanh
        {
            get { return NgayChungTuSource.AddDays(-7); }
        }

        public DateTime NgayTraHangBaoHanh
        {
            get { return NgayChungTuSource.AddDays(3); }
        }

        public List<PhieuBaoHanhItemMaHang> Items
        {
            get { return _Items; }
            set { _Items = value; }
        }


        public bool ContainItem(PhieuBaoHanhItemMaHang itemMaHang)
        {
            foreach (PhieuBaoHanhItemMaHang item in Items)
            {
                if (item.MaHang == itemMaHang.MaHang)
                {
                    return true;
                }
            }
            return false;
        }

        public void AddItem(PhieuBaoHanhItemMaHang phieuBaoHanhItemMaHang)
        {
            _Items.Add(phieuBaoHanhItemMaHang);
        }

        public DataTable ConvertToDataTable
        {
            get
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(nameof(STT), typeof(int));
                dt.Columns.Add(nameof(SoChungTuSource), typeof(string));
                dt.Columns.Add(nameof(MaKhachHang), typeof(string));
                dt.Columns.Add(nameof(NgayChungTuSource), typeof(DateTime));
                dt.Columns.Add(nameof(NgayXacNhanLoiBaoHanh), typeof(DateTime));
                dt.Columns.Add(nameof(NgayTiepNhanBaoHanh), typeof(DateTime));
                dt.Columns.Add(nameof(NgayDeNghiXuatKho), typeof(DateTime));
                dt.Columns.Add(nameof(NgayTraHangBaoHanh), typeof(DateTime));
                dt.Columns.Add(nameof(PhieuBaoHanhItemMaHang.MaHang), typeof(string));
                dt.Columns.Add(nameof(PhieuBaoHanhItemMaHang.TenHang), typeof(string));
                dt.Columns.Add(nameof(PhieuBaoHanhItemMaHang.DonViTinh), typeof(string));
                dt.Columns.Add(nameof(PhieuBaoHanhItemMaHang.SoLuongXuat), typeof(int));
                dt.Columns.Add(nameof(PhieuBaoHanhItemMaHang.Loi), typeof(string));
                dt.Columns.Add(nameof(PhieuBaoHanhItemMaHang.Note), typeof(string));
                dt.Columns.Add(nameof(PhieuBaoHanhItemMaHang.CreatedBy), typeof(string));

                foreach (PhieuBaoHanhItemMaHang itemMaHang in Items)
                {
                    object[] itemArray = { STT,
                        SoChungTuSource,
                        MaKhachHang,
                        NgayChungTuSource,
                        NgayXacNhanLoiBaoHanh,
                        NgayTiepNhanBaoHanh,
                        NgayDeNghiXuatKho,
                        NgayTraHangBaoHanh,
                        itemMaHang.MaHang,
                        itemMaHang.TenHang,
                        itemMaHang.DonViTinh,
                        itemMaHang.SoLuongXuat,
                        itemMaHang.Loi,
                        itemMaHang.Note,
                        itemMaHang.CreatedBy };
                    DataRow newRow = dt.NewRow();
                    newRow.ItemArray = itemArray;
                    dt.Rows.Add(newRow);
                }

                return dt;
            }

        }



    }
}
