﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.Mvvm.Models.DSS
{
    public static class DataSourceSCTVTHH
    {
        public const string MaHang = "MaHang";
        public const string TenHang = "TenHang";
        public const string NgayChungTu = "NgayChungTu";
        public const string SoChungTu = "SoChungTu";
        public const string DonViTinh = "DonViTinh";
        public const string SoLuongXuat = "SoLuongXuat";
    }
}
