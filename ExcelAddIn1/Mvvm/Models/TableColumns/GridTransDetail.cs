﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.Mvvm.Models.TableColumns
{
    public static class GridTransDetail
    {
        public const string TransactionID = "TransactionID";
        public const string GoodID = "Mã hàng";
        public const string GoodName = "Tên hàng";
        public const string VietseaCode = "Mã Vietsea";
        public const string VietseaName = "Tên Vietsea";
        public const string IsEqual = "IsEqual";
        public const string Quantity = "Số lượng";
        public const string Price = "Đơn giá";
        public const string Amount = "Thành tiền";
        public const string Vat = "VAT";
        public const string AmountVAT = "Thành tiền VAT";
    }
}
