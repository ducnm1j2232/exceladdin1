﻿using Microsoft.Office.Interop.Excel;
using VSTOLib.Extensions;

namespace ExcelAddIn1.Mvvm.Commands.DSSBaoHanh
{
    public class LoadDMCommand : MvvmBase.CommandBase
    {
        private ViewModels.DSSBaoHanhViewModel _DSSBaoHanhViewModel;
        private Workbook _MainWorkbook;

        public LoadDMCommand(ViewModels.DSSBaoHanhViewModel dSSBaoHanhViewModel)
        {
            _DSSBaoHanhViewModel = dSSBaoHanhViewModel;
            _MainWorkbook = dSSBaoHanhViewModel.MainWorkbook;
        }

        public override void Execute(object parameter)
        {
            Worksheet BangMa = _MainWorkbook.GetSheet(Mvvm.Models.DSS.Structure.WBook.WorkbookBaoHanhSheetNames.BangMa);
            _DSSBaoHanhViewModel.BangMa = BangMa.UsedRange.ConvertToDataTable();

            Worksheet DMKhachHang = _MainWorkbook.GetSheet(Mvvm.Models.DSS.Structure.WBook.WorkbookBaoHanhSheetNames.DMKhachHang);
            _DSSBaoHanhViewModel.DMKhachHang = DMKhachHang.UsedRange.ConvertToDataTable();

            Worksheet LoiSP = _MainWorkbook.GetSheet(Mvvm.Models.DSS.Structure.WBook.WorkbookBaoHanhSheetNames.LoiSP);
            _DSSBaoHanhViewModel.DMSPLoi = LoiSP.UsedRange.ConvertToDataTable();
        }

    }
}
