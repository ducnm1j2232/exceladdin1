﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

namespace ExcelAddIn1.Mvvm.Commands.DSSBaoHanh
{
    public class RunRandomCommand : MvvmBase.CommandBase
    {
        private Random _random = new Random();

        private ViewModels.DSSBaoHanhViewModel _DSSBaoHanhViewModel;
        private List<DataTable> _PXKDataTableList;
        private DataTable _tempPXKData;

        private DataTable _BangMaDataSourceForOriginalMethod;
        private DataTable _DMLoiSPForOriginalMethod;
        private DataTable _DMLoiSPForRandomlMethod;
        private DataTable _BangMaNotContainMaHangInPXK;
        private DataTable _dtBangMaNotContainPXKDataAndContainOnlyNoteDaSua;

        public RunRandomCommand(ViewModels.DSSBaoHanhViewModel dSSBaoHanhViewModel)
        {
            _DSSBaoHanhViewModel = dSSBaoHanhViewModel;

            _DSSBaoHanhViewModel.PropertyChanged += _DSSBaoHanhViewModel_PropertyChanged;
        }

        private void _DSSBaoHanhViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_DSSBaoHanhViewModel.IsBangMaLoaded)
                || e.PropertyName == nameof(_DSSBaoHanhViewModel.IsDMKhachHangLoaded)
                || e.PropertyName == nameof(_DSSBaoHanhViewModel.IsLoiSPLoaded)
                || e.PropertyName == nameof(_DSSBaoHanhViewModel.IsSCTVTHHLoaded))
            {
                OnCanExecuteChange();
            }
        }

        public override bool CanExecute(object parameter)
        {
            return _DSSBaoHanhViewModel.IsBangMaLoaded
                && _DSSBaoHanhViewModel.IsDMKhachHangLoaded
                && _DSSBaoHanhViewModel.IsLoiSPLoaded
                && _DSSBaoHanhViewModel.IsSCTVTHHLoaded
                && base.CanExecute(parameter);
        }

        public override void Execute(object parameter)
        {
            _PXKDataTableList = GetPXKDatatableList(_DSSBaoHanhViewModel.DataSCTVTHH);
            _BangMaNotContainMaHangInPXK = GetBangMaNotContainMaHangInPXK();
            _BangMaDataSourceForOriginalMethod = GetBangMaDataSourceForOriginalMethod();
            _DMLoiSPForOriginalMethod = GetDMLoiSPForOriginalMethod();
            _DMLoiSPForRandomlMethod = GetDMLoiSPForRandomMethod();
            _dtBangMaNotContainPXKDataAndContainOnlyNoteDaSua = GetdtBangMaNotContainPXKDataAndContainOnlyNoteDaSua(_BangMaNotContainMaHangInPXK);

            List<List<Models.DSS.PhieuBaoHanh>> list = new List<List<Models.DSS.PhieuBaoHanh>>();
            foreach (DataTable eachPXKData in _PXKDataTableList)
            {
                //dùng tempPXKData vì sẽ trừ tồn dần trong bảng
                _tempPXKData = eachPXKData.Copy();
                int sumSLXuatSource = System.Convert.ToInt32(_tempPXKData.Compute($"SUM({Models.DSS.DataSourceSCTVTHH.SoLuongXuat})", ""));

                List<Models.DSS.PhieuBaoHanh> phieuBaoHanhList = CreatePhieuBaoHanhFromPXKDataTable(_tempPXKData);
                list.Add(phieuBaoHanhList);
            }

            DataTable outputRandomDataTable = ListToDataTable(list);
            _DSSBaoHanhViewModel.OutputRandomTable = outputRandomDataTable;

            int sumSoLuongxuatSource = System.Convert.ToInt32(_DSSBaoHanhViewModel.DataSCTVTHH.Compute($"SUM({Models.DSS.DataSourceSCTVTHH.SoLuongXuat})", ""));
            int sumSoLuongXuat = System.Convert.ToInt32(outputRandomDataTable.Compute($"SUM({Models.DSS.DataSourceSCTVTHH.SoLuongXuat})", ""));

            System.Windows.Forms.MessageBox.Show($"Tổng số lượng xuất random: {sumSoLuongXuat}, so với trong bảng VTHH: {sumSoLuongxuatSource}, lệch {sumSoLuongXuat - sumSoLuongxuatSource}");
        }

        private DataTable GetBangMaNotContainMaHangInPXK()
        {
            DataTable output = new DataTable();
            var pxkData = from pxk in _DSSBaoHanhViewModel.DataSCTVTHH.AsEnumerable() select pxk.Field<string>(Mvvm.Models.DSS.DataSourceSCTVTHH.MaHang);
            var q = _DSSBaoHanhViewModel.BangMa.AsEnumerable().Where(x => !pxkData.Contains(x.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetBangMa.MAHANG)));
            output = q.CopyToDataTable();

            return output;
        }


        private DataTable GetBangMaDataSourceForOriginalMethod()
        {
            DataTable output = new DataTable();

            var filterDmLoiDoiMoi = from dmloisp in _DSSBaoHanhViewModel.DMSPLoi.AsEnumerable()
                                    where dmloisp.Field<string>(Models.DSS.Structure.WSheet.SheetLoiSP.NOTE) == "Đổi mới"
                                    select dmloisp;

            var joinTable = from bangma in _DSSBaoHanhViewModel.BangMa.AsEnumerable()
                            join dmLoi in filterDmLoiDoiMoi
                            on bangma.Field<string>(Models.DSS.Structure.WSheet.SheetBangMa.LOAIVT)
                            equals dmLoi.Field<string>(Models.DSS.Structure.WSheet.SheetLoiSP.LOAIVT)
                            where dmLoi.Field<string>(Models.DSS.Structure.WSheet.SheetLoiSP.NOTE) == "Đổi mới"
                            select bangma;
            output = joinTable.CopyToDataTable();

            return output;
        }

        private DataTable GetDMLoiSPForOriginalMethod()
        {
            DataTable output = new DataTable();
            var query = from dmloi in _DSSBaoHanhViewModel.DMSPLoi.AsEnumerable()
                        where dmloi.Field<string>(Models.DSS.Structure.WSheet.SheetLoiSP.NOTE) == "Đổi mới"
                        select dmloi;
            output = query.CopyToDataTable();
            return output;
        }

        private DataTable GetDMLoiSPForRandomMethod()
        {
            DataTable output = new DataTable();
            var query = from dmloi in _DSSBaoHanhViewModel.DMSPLoi.AsEnumerable()
                        where dmloi.Field<string>(Models.DSS.Structure.WSheet.SheetLoiSP.NOTE) == "Đã sửa"
                        select dmloi;
            output = query.CopyToDataTable();
            return output;
        }

        private List<DataTable> GetPXKDatatableList(DataTable source)
        {
            List<DataTable> output = new List<DataTable>();

            DataTable uniqueDatasource = source.DefaultView.ToTable(true, Mvvm.Models.DSS.DataSourceSCTVTHH.SoChungTu);
            List<string> soChungTuList = new List<string>();
            foreach (DataRow row in uniqueDatasource.Rows)
            {
                string soChungTu = row[Mvvm.Models.DSS.DataSourceSCTVTHH.SoChungTu].ToString();
                DataTable item = source.Select($"{Mvvm.Models.DSS.DataSourceSCTVTHH.SoChungTu} = '{soChungTu}'").CopyToDataTable();
                output.Add(item);
            }

            return output;
        }

        private List<Models.DSS.PhieuBaoHanh> CreatePhieuBaoHanhFromPXKDataTable(DataTable PXKDataTable)
        {
            List<Models.DSS.PhieuBaoHanh> phieuBaoHanhList = new List<Models.DSS.PhieuBaoHanh>();

            int stt = 1;
            while (PXKDataTable.Rows.Count > 0)
            {
                Models.DSS.PhieuBaoHanh phieuBaoHanh = CreateNewPhieuBaoHanh(stt, PXKDataTable);
                AddItemCreatedByOriginalToPhieuBaoHanh(phieuBaoHanh, PXKDataTable);
                AddItemCreateByRandomToPhieuBaoHanh(phieuBaoHanh, _BangMaNotContainMaHangInPXK);
                AddItemCreateByAddQuantityToPhieuBanhHanh(phieuBaoHanh);

                phieuBaoHanhList.Add(phieuBaoHanh);
                //await Task.Delay(0);
                stt++;
            }

            int phieuBaoHanhCount = phieuBaoHanhList.Count;

            return phieuBaoHanhList;
        }

        private Models.DSS.PhieuBaoHanh CreateNewPhieuBaoHanh(int STT, DataTable PxkDataTable)
        {
            Models.DSS.PhieuBaoHanh phieuBaoHanh = new Models.DSS.PhieuBaoHanh();
            phieuBaoHanh.STT = STT;

            string soChungTuSource = PxkDataTable.Rows[0][Mvvm.Models.DSS.DataSourceSCTVTHH.SoChungTu].ToString();
            phieuBaoHanh.SoChungTuSource = soChungTuSource;

            string maKhachHang = GetRandomMaKhachHang();
            phieuBaoHanh.MaKhachHang = maKhachHang;

            DateTime ngayChungTu = (DateTime)PxkDataTable.Rows[0][Mvvm.Models.DSS.DataSourceSCTVTHH.NgayChungTu];
            phieuBaoHanh.NgayChungTuSource = ngayChungTu;

            return phieuBaoHanh;
        }

        private void AddItemCreatedByOriginalToPhieuBaoHanh(Models.DSS.PhieuBaoHanh phieuBaoHanh, DataTable dataPXK)
        {
            int numOfItems = _random.Next(_DSSBaoHanhViewModel.NumOfItemMin, Math.Min(dataPXK.Rows.Count, _DSSBaoHanhViewModel.NumOfItemMax));

            while (phieuBaoHanh.Items.Count < numOfItems && dataPXK.Rows.Count > 0)
            {
                AddItemMaHangCore(phieuBaoHanh, dataPXK, numOfItems);
            }
        }

        private void AddItemMaHangCore(Models.DSS.PhieuBaoHanh phieuBaoHanh, DataTable source, int numOfItems)
        {
            List<string> SelectedMaHang = new List<string>();
            while (phieuBaoHanh.Items.Count < numOfItems)
            {
                Models.DSS.PhieuBaoHanhItemMaHang itemMaHang = new Models.DSS.PhieuBaoHanhItemMaHang();

            startAgain:
                int maxDataSourceRowCount = source.Rows.Count;
                System.Diagnostics.Debug.WriteLine(maxDataSourceRowCount);
                int randomRowIndex = _random.Next(0, maxDataSourceRowCount);

                string maHang = source.Rows[randomRowIndex][Models.DSS.DataSourceSCTVTHH.MaHang].ToString();
                if (SelectedMaHang.Contains(maHang))
                {
                    goto startAgain;
                }

                itemMaHang.MaHang = maHang;
                var query = from d in source.AsEnumerable()
                            where d.Field<string>(Mvvm.Models.DSS.DataSourceSCTVTHH.MaHang) == maHang
                            select d;
                itemMaHang.TenHang = query.CopyToDataTable().Rows[0][Mvvm.Models.DSS.DataSourceSCTVTHH.TenHang].ToString();

                int soLuongTon = System.Convert.ToInt32(source.Rows[randomRowIndex][Models.DSS.DataSourceSCTVTHH.SoLuongXuat]);
                int maxSoLuongXuatKhaDung = Math.Min(_DSSBaoHanhViewModel.SLXuatMax, soLuongTon);
                int randomSoLuongXuat = _random.Next(_DSSBaoHanhViewModel.SLXuatMin, maxSoLuongXuatKhaDung);
                itemMaHang.SoLuongXuat = randomSoLuongXuat;

                itemMaHang.DonViTinh = GetDonViTinhFromPXK(itemMaHang);
                GetRandomLoiAndNote(itemMaHang, _DMLoiSPForOriginalMethod);
                phieuBaoHanh.AddItem(itemMaHang);

                SelectedMaHang.Add(maHang);

                //update lại source để trừ tồn dần
                DataRow dataRow = source.Rows[randomRowIndex];
                int soLuongConLai = soLuongTon - randomSoLuongXuat;
                source.Rows[randomRowIndex][Models.DSS.DataSourceSCTVTHH.SoLuongXuat] = soLuongConLai;
                if (soLuongConLai == 0)
                {
                    dataRow.Delete();
                    source.AcceptChanges();
                }

                Debug.WriteLine($"SL hàng trong phiếu: {phieuBaoHanh.Items.Count}, numofItems:{numOfItems}");

            }
        }
        private void AddItemCreateByAddQuantityToPhieuBanhHanh(Models.DSS.PhieuBaoHanh phieuBaoHanh)
        {
            List<Models.DSS.PhieuBaoHanhItemMaHang> list = new List<Models.DSS.PhieuBaoHanhItemMaHang>();

            foreach (Models.DSS.PhieuBaoHanhItemMaHang itemMaHang in phieuBaoHanh.Items)
            {
                if (!CanRandom(_DSSBaoHanhViewModel.AddItemByIncreaseQuantityRate))
                {
                    continue;
                }

                if (CanAddQuantity(itemMaHang))
                {
                    itemMaHang.Note = "Đổi mới";

                    Models.DSS.PhieuBaoHanhItemMaHang newItemMaHang = new Models.DSS.PhieuBaoHanhItemMaHang();
                    newItemMaHang.MaHang = itemMaHang.MaHang;
                    newItemMaHang.TenHang = itemMaHang.TenHang;
                    newItemMaHang.DonViTinh = itemMaHang.DonViTinh;
                    newItemMaHang.SoLuongXuat = _random.Next(_DSSBaoHanhViewModel.AddItemByIncreaseQuantityMin, _DSSBaoHanhViewModel.AddItemByIncreaseQuantityMax);
                    newItemMaHang.CreatedBy = Models.DSS.PhieuBaoHanhItemMaHang._CreatedBy.AddQuantity;
                    newItemMaHang.Loi = itemMaHang.Loi;
                    newItemMaHang.Note = "Đã sửa";

                    list.Add(newItemMaHang);
                    //phieuBaoHanh.AddItem(newItemMaHang);
                }
            }

            foreach(var item in list)
            {
                phieuBaoHanh.AddItem(item);
            }
        }

        private bool CanAddQuantity(Mvvm.Models.DSS.PhieuBaoHanhItemMaHang itemMaHang)
        {
            string maHang = itemMaHang.MaHang;
            var query = from bangma in _DSSBaoHanhViewModel.BangMa.AsEnumerable()
                        join dmloi in _DSSBaoHanhViewModel.DMSPLoi.AsEnumerable()
                        on bangma.Field<string>(Models.DSS.Structure.WSheet.SheetBangMa.LOAIVT)
                        equals dmloi.Field<string>(Models.DSS.Structure.WSheet.SheetLoiSP.LOAIVT)
                        where dmloi.Field<string>(Models.DSS.Structure.WSheet.SheetLoiSP.NOTE) == "Đã sửa"
                        select bangma;

            if (query.Any())
            {
                var dt = query.CopyToDataTable();
                if (itemMaHang.CreatedBy == Models.DSS.PhieuBaoHanhItemMaHang._CreatedBy.Original)
                {
                    return true;
                }
            }

            return false;
        }

        private void AddItemCreateByRandomToPhieuBaoHanh(Models.DSS.PhieuBaoHanh phieuBaoHanh, DataTable dataSourceBangMaNotContainPXKData)
        {
            if (!CanRandom(_DSSBaoHanhViewModel.AddNewItemByRandomRate))
            {
                return;
            }

            List<string> maHangList = new List<string>();

            int numOfItems = _random.Next(_DSSBaoHanhViewModel.NumOfNewItemMin, _DSSBaoHanhViewModel.NumOfNewItemMax);
            for (int i = 0; i < numOfItems; i++)
            {
                Mvvm.Models.DSS.PhieuBaoHanhItemMaHang itemMaHang = new Models.DSS.PhieuBaoHanhItemMaHang();

            GetRandom:
                string maHangRandom = GetRandomValueFromTable(_dtBangMaNotContainPXKDataAndContainOnlyNoteDaSua, Mvvm.Models.DSS.Structure.WSheet.SheetBangMa.MAHANG).ToString();
                if (maHangList.Contains(maHangRandom))
                {
                    goto GetRandom;
                }

                itemMaHang.MaHang = maHangRandom;
                itemMaHang.TenHang = GetTenHang(itemMaHang.MaHang, _dtBangMaNotContainPXKDataAndContainOnlyNoteDaSua);
                itemMaHang.DonViTinh = GetDonViTinhFromBangMa(itemMaHang);
                itemMaHang.SoLuongXuat = _random.Next(_DSSBaoHanhViewModel.NewRandomItemSLXuatMin, _DSSBaoHanhViewModel.NewRandomItemSLXuatMax);
                itemMaHang.CreatedBy = Models.DSS.PhieuBaoHanhItemMaHang._CreatedBy.NewRandom;
                GetRandomLoiAndNote(itemMaHang, _DMLoiSPForRandomlMethod);

                phieuBaoHanh.AddItem(itemMaHang);

                maHangList.Add(maHangRandom);
            }

            #region local
            string GetTenHang(string maHang, DataTable source)
            {
                var query = from d in source.AsEnumerable() where d.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetBangMa.MAHANG) == maHang select d;
                return query.CopyToDataTable().Rows[0][Mvvm.Models.DSS.Structure.WSheet.SheetBangMa.TENHANG].ToString();
            }
            #endregion
        }


        private DataTable GetdtBangMaNotContainPXKDataAndContainOnlyNoteDaSua(DataTable bangmaNotContainPXKData)
        {
            DataTable output = new DataTable();

            var query = from bangma in bangmaNotContainPXKData.AsEnumerable()
                        join dmloi in _DMLoiSPForRandomlMethod.AsEnumerable()
                        on bangma.Field<string>(Models.DSS.Structure.WSheet.SheetBangMa.LOAIVT)
                        equals dmloi.Field<string>(Models.DSS.Structure.WSheet.SheetLoiSP.LOAIVT)
                        select bangma;

            output = query.CopyToDataTable();

            return output;
        }

        private bool CanRandom(int rate)
        {
            int randomNumber = _random.Next(0, 100);
            if (randomNumber <= rate)
            {
                return true;
            }

            return false;
        }

        private bool CanUseAddNewRandomItemMethod(Models.DSS.PhieuBaoHanh phieuBaoHanh)
        {
            foreach (var item in phieuBaoHanh.Items)
            {

            }
            return true;
        }

        private string GetDonViTinhFromPXK(Models.DSS.PhieuBaoHanhItemMaHang itemMaHang)
        {
            string maHang = itemMaHang.MaHang;

            var query = from data in _DSSBaoHanhViewModel.DataSCTVTHH.AsEnumerable()
                        where data.Field<string>(Mvvm.Models.DSS.DataSourceSCTVTHH.MaHang) == maHang
                        select data;
            if (!query.Any())
            {
                return string.Empty;
            }

            string output = query.CopyToDataTable().Rows[0][Mvvm.Models.DSS.DataSourceSCTVTHH.DonViTinh].ToString();
            return output;
        }

        private string GetDonViTinhFromBangMa(Models.DSS.PhieuBaoHanhItemMaHang itemMaHang)
        {
            string maHang = itemMaHang.MaHang;

            var query = from data in _DSSBaoHanhViewModel.BangMa.AsEnumerable()
                        where data.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetBangMa.MAHANG) == maHang
                        select data;
            if (!query.Any())
            {
                return string.Empty;
            }

            string output = query.CopyToDataTable().Rows[0][Mvvm.Models.DSS.Structure.WSheet.SheetBangMa.DONVITINH].ToString();
            return output;
        }


        private string GetLoaiVT(Models.DSS.PhieuBaoHanhItemMaHang itemMaHang)
        {
            string maHang = itemMaHang.MaHang;
            //cách 2: LinQ
            var query = from bangma in _DSSBaoHanhViewModel.BangMa.AsEnumerable()
                        where bangma.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetBangMa.MAHANG) == maHang
                        select bangma;

            if (!query.Any())
            {
                return string.Empty;
            }
            var table = query.CopyToDataTable();
            var output = table.Rows[0][Mvvm.Models.DSS.Structure.WSheet.SheetBangMa.LOAIVT].ToString();
            return output;
        }

        private void GetRandomLoiAndNote(Models.DSS.PhieuBaoHanhItemMaHang itemMaHang, DataTable DMLoiDataSource)
        {
            string loaiVT = GetLoaiVT(itemMaHang);
            if (string.IsNullOrEmpty(loaiVT))
            {
                itemMaHang.Loi = string.Empty;
                itemMaHang.Note = string.Empty;
                return;
            }

            var query = from dmloi in DMLoiDataSource.AsEnumerable()
                        where dmloi.Field<string>(Models.DSS.Structure.WSheet.SheetLoiSP.LOAIVT) == loaiVT
                        select dmloi;

            if (!query.Any())
            {
                itemMaHang.Loi = string.Empty;
                itemMaHang.Note = string.Empty;
                return;
            }

            DataTable filterSource = query.CopyToDataTable();

            int randomRowIndex = _random.Next(0, filterSource.Rows.Count - 1);
            itemMaHang.Loi = filterSource.Rows[randomRowIndex][Mvvm.Models.DSS.Structure.WSheet.SheetLoiSP.LOI].ToString();
            itemMaHang.Note = filterSource.Rows[randomRowIndex][Mvvm.Models.DSS.Structure.WSheet.SheetLoiSP.NOTE].ToString();
            //GetRandomValueFromTable(filterSource, Models.DSS.Structure.WSheet.SheetLoiSP.LOI).ToString();
        }

        private string GetNote(Models.DSS.PhieuBaoHanhItemMaHang itemMaHang)
        {
            return string.Empty;
        }

        private string GetRandomMaKhachHang()
        {
            //DataTable dataTable = _DSSBaoHanhViewModel.DMKhachHang;
            //int maxCount = dataTable.Rows.Count;
            //int randomIndex = _random.Next(0, maxCount);

            //string maKhachHangRandom = dataTable.Rows[randomIndex][Models.DSS.Structure.WSheet.SheetDMKhachHang.Ma].ToString();

            //return maKhachHangRandom;
            return GetRandomValueFromTable(_DSSBaoHanhViewModel.DMKhachHang, Models.DSS.Structure.WSheet.SheetDMKhachHang.MAKHACHHANG).ToString();
        }

        private int GetRandomRowIndexFromDataTable(DataTable dataTable)
        {
            return _random.Next(0, dataTable.Rows.Count);
        }

        private object GetRandomValueFromTable(DataTable source, string columnName)
        {
            int max = source.Rows.Count;
            int randomIndex = _random.Next(0, max);

            object output = source.Rows[randomIndex][columnName];
            if (output == null)
            {
                throw new Exception("null");
            }

            return output;
        }


        private DataTable ListToDataTable(List<List<Models.DSS.PhieuBaoHanh>> input)
        {
            DataTable output = new DataTable();
            DataTable temp = new DataTable();
            foreach (List<Models.DSS.PhieuBaoHanh> item in input)
            {
                foreach (Models.DSS.PhieuBaoHanh phieu in item)
                {
                    temp = phieu.ConvertToDataTable;
                    foreach (DataRow row in temp.Rows)
                    {
                        if (output.Columns.Count == 0)
                        {
                            output = temp.Clone();
                        }
                        output.Rows.Add(row.ItemArray);
                    }
                }
            }

            return output;
        }



    }

}
