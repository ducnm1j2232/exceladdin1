﻿using ExcelAddIn1.Mvvm.ViewModels;
using Microsoft.Office.Interop.Excel;
using System.Collections.ObjectModel;
using System.Data;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.Mvvm.Commands.DSSBaoHanh
{
    public class LoadDanhSachPhieuRandomCommand : MvvmBase.CommandBase
    {
        private DSSBaoHanhReportViewModel _DSSBaoHanhReportViewModel;


        private Workbook _workbookDataSource;

        public LoadDanhSachPhieuRandomCommand(DSSBaoHanhReportViewModel dSSBaoHanhReportViewModel)
        {
            _DSSBaoHanhReportViewModel = dSSBaoHanhReportViewModel;
        }

        public override void Execute(object parameter)
        {
            _workbookDataSource = _DSSBaoHanhReportViewModel.MainWorkbook;
            Worksheet worksheet = _workbookDataSource.GetSheet(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.SheetName);

            DataTable source = worksheet.UsedRange.ConvertToDataTable();
            _DSSBaoHanhReportViewModel.OutputRandomData = source;
            var uniqueRows = source.DefaultView.ToTable(true, "STT", "SoChungTuSource", "NgayChungTuSource","MaKhachHang");

            ObservableCollection<Models.DSS.PBH> list = new ObservableCollection<Models.DSS.PBH>();
            foreach (DataRow row in uniqueRows.Rows)
            {
                Models.DSS.PBH pBH = new Models.DSS.PBH()
                {
                    STT = row["STT"].ToString(),
                    SoChungTu = row["SoChungTuSource"].ToString(),
                    NgayChungTu = System.Convert.ToDateTime(row["NgayChungTuSource"]),
                    MaKhachHang = row["MaKhachHang"].ToString()
                };

                list.Add(pBH);
            }

            _DSSBaoHanhReportViewModel.PBHCollection = list;

        }
    }
}
