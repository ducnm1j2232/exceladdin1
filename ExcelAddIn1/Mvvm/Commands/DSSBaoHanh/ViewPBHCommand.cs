﻿using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Linq;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.Mvvm.Commands.DSSBaoHanh
{
    public class ViewPBHCommand : MvvmBase.CommandBase
    {
        private Mvvm.ViewModels.DSSBaoHanhReportViewModel _DSSBaoHanhReportViewModel;

        private Workbook _mainWorkbook;
        private DataTable detailItemMaHangDataSource;

        public ViewPBHCommand(Mvvm.ViewModels.DSSBaoHanhReportViewModel dSSBaoHanhReportViewModel)
        {
            _DSSBaoHanhReportViewModel = dSSBaoHanhReportViewModel;
        }

        public override void Execute(object parameter)
        {
            SetPBHParameter();
            View1PhieuNhanBaoHanh();
            View2BBXacNhanLoiBH();
            View3PhieuDeNghiXuatKho();
            View5PhieuTraHangBH();
        }

        private void SetPBHParameter()
        {
            _mainWorkbook = _DSSBaoHanhReportViewModel.MainWorkbook;
            DataTable outputRandomData = _DSSBaoHanhReportViewModel.OutputRandomData;
            var dtDetailItemMaHang = from detail in outputRandomData.AsEnumerable()
                                     where detail.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.STT) == _DSSBaoHanhReportViewModel.SelectedPBH.STT
                                     && detail.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.SoChungTuSource) == _DSSBaoHanhReportViewModel.SelectedPBH.SoChungTu
                                     select detail;

            //new Mvvm.Models.DSS.PBH
            //{
            //    STT = pbh.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.STT),
            //    NgayChungTu = pbh.Field<DateTime>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.NgayChungTuSource),
            //    SoChungTu = pbh.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.SoChungTuSource),
            //    MaKhachHang = pbh.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.MaKhachHang),
            //};

            detailItemMaHangDataSource = dtDetailItemMaHang.CopyToDataTable();
            detailItemMaHangDataSource.DefaultView.Sort = "MaHang ASC";
            detailItemMaHangDataSource = detailItemMaHangDataSource.DefaultView.ToTable();

            Worksheet worksheetPBHParameter = _mainWorkbook.GetSheet(Mvvm.Models.DSS.Structure.WSheet.SheetPBHParameter.SheetName);
            worksheetPBHParameter.Range[Mvvm.Models.DSS.Structure.WSheet.SheetPBHParameter.SoPhieuAddress].Value = _DSSBaoHanhReportViewModel.SelectedPBH.STT;
            worksheetPBHParameter.Range[Mvvm.Models.DSS.Structure.WSheet.SheetPBHParameter.SoChungTuAddress].Value = _DSSBaoHanhReportViewModel.SelectedPBH.SoChungTu;
            worksheetPBHParameter.Range[Mvvm.Models.DSS.Structure.WSheet.SheetPBHParameter.NgayChungTuAddress].Value = _DSSBaoHanhReportViewModel.SelectedPBH.NgayChungTu;
            worksheetPBHParameter.Range[Mvvm.Models.DSS.Structure.WSheet.SheetPBHParameter.MaKhachHangAddress].Value = _DSSBaoHanhReportViewModel.SelectedPBH.MaKhachHang;
        }

        private void View1PhieuNhanBaoHanh()
        {
            //string[] columns = new string[] {Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.STT,
            //    Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.MaHang,
            //    Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.TenHang,
            //    Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.DonViTinh,
            //    Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.SoLuongXuat,
            //};

            //DataTable data = detailItemMaHangDataSource.SelectColumn(columns);//phải lấy cả cột STT để sau còn đánh số lại

            //group
            var query = from eachRow in detailItemMaHangDataSource.AsEnumerable()
                        group eachRow by new //Models.DSS.Structure.Report.PhieuNhanBaoHanh
                        {
                            STT = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.STT),
                            MaHang = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.MaHang),
                            TenHang = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.TenHang),
                            DonViTinh = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.DonViTinh)
                        }
                        into grp

                        select new //Models.DSS.Structure.Report.PhieuNhanBaoHanhSum
                        {
                            STT = grp.Key.STT,
                            MaHang = grp.Key.MaHang,
                            TenHang = grp.Key.TenHang,
                            DonViTinh = grp.Key.DonViTinh,
                            TongSoLuongXuat = grp.Sum(x => x.Field<double>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.SoLuongXuat))
                        };

            var toList = query.ToList();

            DataTable dataSource = toList.ToDataTable();

            Worksheet worksheetReport = _mainWorkbook.GetSheet(Mvvm.Models.DSS.Structure.WSheet.Sheet1PhieuNhanBaoHanh.SheetName);
            WriteDataToSheet(dataSource, worksheetReport);
        }

        private void View2BBXacNhanLoiBH()
        {
            string[] columns = new string[] {Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.STT,
                Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.MaHang,
                Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.TenHang,
                Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.SoLuongXuat,
                Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.Loi,
                Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.Note,
            };

            DataTable data = detailItemMaHangDataSource.SelectColumn(columns);//phải lấy cả cột STT để sau còn đánh số lại

            Worksheet worksheetReport = _mainWorkbook.GetSheet(Mvvm.Models.DSS.Structure.WSheet.Sheet2BBXacNhanHangLoiBH.SheetName);
            WriteDataToSheet(data, worksheetReport);

        }

        private void View3PhieuDeNghiXuatKho()
        {
            //group
            var query = from eachRow in detailItemMaHangDataSource.AsEnumerable()
                        group eachRow by new //chỉ để new không được định danh, nếu ko thì ko group được
                        {
                            STT = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.STT),
                            MaHang = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.MaHang),
                            DonViTinh = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.DonViTinh)
                        }
                        into grp

                        select new //chỉ để new
                        {
                            STT = grp.Key.STT,
                            MaHang = grp.Key.MaHang,
                            DonViTinh = grp.Key.DonViTinh,
                            SoLuongXuat = grp.Sum(x => x.Field<double>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.SoLuongXuat))
                        };

            var toList = query.ToList();

            DataTable dataSource = toList.ToDataTable();

            //duplicate column mã hàng
            DataColumn dataColumn = new DataColumn("MaHangThayThe");
            dataColumn.Expression = Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.MaHang;
            dataSource.Columns.Add(dataColumn);

            string[] selectColumns = new string[]
            {
                Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.STT,
                Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.MaHang,
                "MaHangThayThe",
                Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.DonViTinh,
                Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.SoLuongXuat,
            };
            dataSource = dataSource.SelectColumn(selectColumns);

            Worksheet worksheetReport = _mainWorkbook.GetSheet(Mvvm.Models.DSS.Structure.WSheet.Sheet3PhieuDeNghiXuatKho.SheetName);
            WriteDataToSheet(dataSource, worksheetReport);

            //convert to value to calculate sum
            ListObject table = worksheetReport.ListObjects[1];
            table.ListColumns["Số lượng"].DataBodyRange.ConvertToValue();

        }

        private void View5PhieuTraHangBH()
        {
            //group
            var query = from eachRow in detailItemMaHangDataSource.AsEnumerable()
                        group eachRow by new //chỉ để new không được định danh, nếu ko thì ko group được
                        {
                            STT = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.STT),
                            MaHang = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.MaHang),
                            TenHang = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.TenHang),
                            DonViTinh = eachRow.Field<string>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.DonViTinh)
                        }
                        into grp

                        select new //chỉ để new
                        {
                            STT = grp.Key.STT,
                            MaHang = grp.Key.MaHang,
                            TenHang = grp.Key.TenHang,
                            DonViTinh = grp.Key.DonViTinh,
                            SoLuongXuat = grp.Sum(x => x.Field<double>(Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.SoLuongXuat)),
                            TinhTrang = "Sử dụng tốt",
                            ChuY = ""
                        };

            var toList = query.ToList();
            DataTable dataSource = toList.ToDataTable();

            Worksheet worksheetReport = _mainWorkbook.GetSheet(Mvvm.Models.DSS.Structure.WSheet.Sheet5PhieuTraHangBH.SheetName);
            WriteDataToSheet(dataSource, worksheetReport);

        }
        private void WriteDataToSheet(DataTable data, Worksheet worksheet)
        {
            EditSTTinto12345(data);

            ListObject listObject = worksheet.ListObjects.Item[1];
            listObject.DeleteRows();
            listObject.AddRows(data.Rows.Count);

            data.CopyToRange(listObject.GetCell(1, Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.STT), false);
        }

        private void EditSTTinto12345(DataTable dataTable)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                DataRow row = dataTable.Rows[i];
                row[Mvvm.Models.DSS.Structure.WSheet.SheetOutputRandomData.STT] = i + 1;
            }
        }




    }
}
