﻿using Microsoft.Office.Interop.Excel;
using System;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.Mvvm.Commands.DSSBaoHanh
{
    public class GetSoChiTietXuatKhoFileCommand : MvvmBase.CommandBase
    {
        private ViewModels.DSSBaoHanhViewModel _DSSBaoHanhViewModel;
        public GetSoChiTietXuatKhoFileCommand(Mvvm.ViewModels.DSSBaoHanhViewModel dSSBaoHanhViewModel)
        {
            _DSSBaoHanhViewModel = dSSBaoHanhViewModel;
            _DSSBaoHanhViewModel.PropertyChanged += _DSSBaoHanhViewModel_PropertyChanged;
        }

        private void _DSSBaoHanhViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_DSSBaoHanhViewModel.IsBangMaLoaded)
               || e.PropertyName == nameof(_DSSBaoHanhViewModel.IsDMKhachHangLoaded)
               || e.PropertyName == nameof(_DSSBaoHanhViewModel.IsLoiSPLoaded))
            {
                OnCanExecuteChange();
            }
        }

        public override bool CanExecute(object parameter)
        {
            return _DSSBaoHanhViewModel.IsBangMaLoaded
                && _DSSBaoHanhViewModel.IsDMKhachHangLoaded
                && _DSSBaoHanhViewModel.IsLoiSPLoaded
                && base.CanExecute(parameter);
        }

        public override void Execute(object parameter)
        {
            string filename = GetFileName();
            if (string.IsNullOrEmpty(filename))
            {
                return;
            }

            Workbook wb = Globals.ThisAddIn.Application.Workbooks.Open(filename);

            DataTable dataTable = GetDataSource(wb);
            _DSSBaoHanhViewModel.DataSCTVTHH = dataTable;
        }

        private static string GetFileName()
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog()
            {
                Title = "Chọn file Sổ chi tiết vật tư hàng hóa từ Misa",
                Filter = "Excel Files | *.xls; *.xlsx; *.xlsm"
            };

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return ofd.FileName;
            }

            return string.Empty;
        }

        private static DataTable GetDataSource(Workbook workbook)
        {
            Worksheet worksheet = workbook.GetSheet("Báo cáo");
            int LR = worksheet.LastRow(Mvvm.Models.DSS.SheetSCTVTHHColumns.SoChungTu);
            int firstDataRow = 5;

            DataTable output = CreateDataTable();

            for (int i = firstDataRow; i <= LR; i++)
            {
                string mahang = worksheet.Range[Mvvm.Models.DSS.SheetSCTVTHHColumns.MaHang + i].Value;
                string tenhang = worksheet.Range[Mvvm.Models.DSS.SheetSCTVTHHColumns.TenHang + i].Value;
                DateTime ngaychungtu = worksheet.Range[Mvvm.Models.DSS.SheetSCTVTHHColumns.NgayChungTu + i].Value;
                string sochungtu = worksheet.Range[Mvvm.Models.DSS.SheetSCTVTHHColumns.SoChungTu + i].Value;
                string donvitinh = worksheet.Range[Mvvm.Models.DSS.SheetSCTVTHHColumns.DVT + i].Value;
                double soluongxuat = worksheet.Range[Mvvm.Models.DSS.SheetSCTVTHHColumns.SoLuongXuat + i].Value;

                output.Rows.Add(mahang, tenhang, ngaychungtu, sochungtu, donvitinh, soluongxuat);

            }
            workbook.Close(SaveChanges: false);

            output.DefaultView.Sort = Mvvm.Models.DSS.DataSourceSCTVTHH.NgayChungTu + "," + Mvvm.Models.DSS.DataSourceSCTVTHH.SoChungTu;

            return output = output.DefaultView.ToTable();

            #region local
            DataTable CreateDataTable()
            {
                DataTable table = new DataTable();
                table.Columns.Add(Mvvm.Models.DSS.DataSourceSCTVTHH.MaHang, typeof(string));
                table.Columns.Add(Mvvm.Models.DSS.DataSourceSCTVTHH.TenHang, typeof(string));
                table.Columns.Add(Mvvm.Models.DSS.DataSourceSCTVTHH.NgayChungTu, typeof(DateTime));
                table.Columns.Add(Mvvm.Models.DSS.DataSourceSCTVTHH.SoChungTu, typeof(string));
                table.Columns.Add(Mvvm.Models.DSS.DataSourceSCTVTHH.DonViTinh, typeof(string));
                table.Columns.Add(Mvvm.Models.DSS.DataSourceSCTVTHH.SoLuongXuat, typeof(double));

                return table;
            }
            #endregion
        }
    }
}
