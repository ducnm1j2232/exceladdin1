﻿using ExcelAddIn1.Mvvm.ViewModels;
using Microsoft.Office.Interop.Excel;
using System.Threading.Tasks;
using System.Windows;
using VSTOLib.Extensions;

namespace ExcelAddIn1.Mvvm.Commands.DSSBaoHanh
{
    public class ExportReportCommand : MvvmBase.AsyncCommandBase2
    {
        private Mvvm.ViewModels.DSSBaoHanhReportViewModel _DSSBaoHanhReportViewModel;
        private Workbook _workbook;
        private Microsoft.Office.Interop.Excel.Application _excelApp;

        public ExportReportCommand(DSSBaoHanhReportViewModel dSSBaoHanhReportViewModel)
        {
            _DSSBaoHanhReportViewModel = dSSBaoHanhReportViewModel;

            _workbook = dSSBaoHanhReportViewModel.MainWorkbook;
            _excelApp = dSSBaoHanhReportViewModel.MainWorkbook.Application;
        }

        public override bool CanExecute()
        {
            return true;
        }

        public override async Task ExecuteAsync()
        {
            string saveFolder = VSTOLib.IO.FolderBrowser.SelectFolder("Chọn thư mục lưu file excel");
            if (string.IsNullOrEmpty(saveFolder))
            {
                return;
            }


            await ExportToFile(saveFolder);
            MessageBox.Show("Kết xuất file thành công");

        }

        private async Task ExportToFile(string saveFolder)
        {
            try
            {

                for (int i = 0; i < _DSSBaoHanhReportViewModel.PBHCollection.Count; i++)
                {
                    Models.DSS.PBH pBH = _DSSBaoHanhReportViewModel.PBHCollection[i];

                    _DSSBaoHanhReportViewModel.SelectedPBH = pBH;
                    _DSSBaoHanhReportViewModel.ViewPBH.Execute(null);
                    int percentage = 100 * (i + 1) / _DSSBaoHanhReportViewModel.PBHCount;
                    _DSSBaoHanhReportViewModel.CurrentPercentage = percentage;

                    SaveFile(pBH, saveFolder);
                    await Task.Delay(1);
                }

            }
            finally
            {
                _excelApp.ScreenUpdating = true;
            }

        }

        private void SaveFile(Models.DSS.PBH pBH, string folder)
        {
            _excelApp.ScreenUpdating = false;
            Workbook newWorkbook = _excelApp.Workbooks.Add();

            Worksheet worksheet1 = _workbook.GetSheet(Models.DSS.Structure.WSheet.Sheet1PhieuNhanBaoHanh.SheetName);
            worksheet1.Copy(newWorkbook.Worksheets[1]);

            string newFileName = $"{pBH.STT}-{pBH.NgayChungTu.ToString("dd-MM-yyyy")}-{pBH.MaKhachHang}.xlsx";
            string savePath = System.IO.Path.Combine(folder, newFileName);

            newWorkbook.SaveOverWrite(savePath);
            newWorkbook.Close();
            _excelApp.ScreenUpdating = true;

        }
    }
}

