﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelAddIn1.Mvvm.Commands.DSSBaoHanh
{
    public class TestUpdateUICommand : MvvmBase.AsyncCommandBase
    {
        Mvvm.ViewModels.DSSBaoHanhViewModel _DSSBaoHanhViewModel;
        public TestUpdateUICommand(ViewModels.DSSBaoHanhViewModel dSSBaoHanhViewModel,Action<Exception> onException):base(onException)
        {
            _DSSBaoHanhViewModel = dSSBaoHanhViewModel;
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            for (int i = 0; i < 100; i++)
            {
                await Task.Delay(60);
                //System.Threading.Thread.Sleep(100);
                System.Diagnostics.Debug.WriteLine($"running.....  {i}");
            }

            System.Windows.Forms.MessageBox.Show("Test");
        }

       
    }
}
