﻿using Microsoft.Office.Interop.Excel;
using System.Collections.ObjectModel;
using System.Windows.Input;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.Mvvm.ViewModels
{
    public class DSSBaoHanhReportViewModel : MvvmBase.ViewModelBase
    {
        ObservableCollection<Mvvm.Models.DSS.PBH> _PBHColection = new ObservableCollection<Models.DSS.PBH>();

        public DSSBaoHanhReportViewModel()
        {
            MainWorkbook = Globals.ThisAddIn.Application.ActiveWorkbook;

            LoadDanhSachPhieuRandom = new Mvvm.Commands.DSSBaoHanh.LoadDanhSachPhieuRandomCommand(this);
            ViewPBH = new Mvvm.Commands.DSSBaoHanh.ViewPBHCommand(this);
            ExportReport = new Mvvm.Commands.DSSBaoHanh.ExportReportCommand(this);
        }



        private Workbook _MainWorkbook;
        public Workbook MainWorkbook
        {
            get { return _MainWorkbook; }
            set { _MainWorkbook = value; OnPropertyChanged(); }
        }


        private ObservableCollection<Mvvm.Models.DSS.PBH> _PBHCollection = new ObservableCollection<Models.DSS.PBH>();
        public ObservableCollection<Mvvm.Models.DSS.PBH> PBHCollection
        {
            get { return _PBHCollection; }
            set
            {
                _PBHCollection = value;
                OnPropertyChanged();
            }
        }

        private Mvvm.Models.DSS.PBH _SelectedPBH;
        public Mvvm.Models.DSS.PBH SelectedPBH
        {
            get { return _SelectedPBH; }
            set
            {
                _SelectedPBH = value;
                OnPropertyChanged();
            }
        }

        private DataTable _OutputRandomData;
        public DataTable OutputRandomData
        {
            get { return _OutputRandomData; }
            set
            {
                _OutputRandomData = value;
                OnPropertyChanged();
            }
        }

        public int PBHCount
        {
            get { return PBHCollection.Count; }
        }

        private int _CurrentProgress;
        public int CurrentPBHExported
        {
            get { return _CurrentProgress; }
            set
            {
                _CurrentProgress = value;
                OnPropertyChanged();
            }
        }

        private int _CurrentPercentage;
        public int CurrentPercentage
        {
            get
            {
                return _CurrentPercentage;
            }
            set
            {
                _CurrentPercentage = value;
                OnPropertyChanged();
            }
        }







        public ICommand LoadDanhSachPhieuRandom { get; set; }
        public ICommand ViewPBH { get; set; }

        public MvvmBase.IAsyncCommand ExportReport { get; set; }

    }
}
