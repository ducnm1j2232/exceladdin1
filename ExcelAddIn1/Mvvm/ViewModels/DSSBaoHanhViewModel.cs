﻿using Microsoft.Office.Interop.Excel;
using System.Windows.Input;
using VSTOLib.Extensions;
using DataTable = System.Data.DataTable;

namespace ExcelAddIn1.Mvvm.ViewModels
{
    public class DSSBaoHanhViewModel : MvvmBase.ViewModelBase
    {
        private Workbook _MainWorkbook;
        private bool _IsBangMaLoaded;
        private bool _IsDMKhachHangLoaded;
        private bool _IsLoiSPLoaded;
        private bool _IsSCTVTHHLoaded;
        private DataTable _DataSCTVTHH;
        private DataTable _BangMa;
        private DataTable _DMKhachHang;
        private DataTable _DMSPLoi;
        private DataTable _OutputRandomTable;
        
        private int _NumOfItemMin = 1;
        private int _NumOfItemMax = 7;
        private int _SLXuatMin = 1;
        private int _SLXuatMax = 20;
        
        private int _AddNewItemByRandomRate = 50;
        private int _NumOfNewItemMin = 1;
        private int _NumOfNewItemMax = 3;
        private int _NewRandomItemSLXuatMin = 1;
        private int _NewRandomItemSLXuatMax = 10;

        private int _AddItemByIncreaseQuantityRate = 30;
        private int _AddItemByIncreaseQuantityMin = 1;
        private int _AddItemByIncreaseQuantityMax = 3;


        public DSSBaoHanhViewModel()
        {
            MainWorkbook = Globals.ThisAddIn.Application.ActiveWorkbook;

            LoadDataBase = new Mvvm.Commands.DSSBaoHanh.LoadDMCommand(this);
            GetSCTVTHH = new Mvvm.Commands.DSSBaoHanh.GetSoChiTietXuatKhoFileCommand(this);
            RunRandom = new Mvvm.Commands.DSSBaoHanh.RunRandomCommand(this);
            TestUpdateUI = new Mvvm.Commands.DSSBaoHanh.TestUpdateUICommand(this, (ex) => { System.Windows.Forms.MessageBox.Show(ex.Message); });
        }


        public Workbook MainWorkbook { get { return _MainWorkbook; } set { _MainWorkbook = value; OnPropertyChanged(); } }
        public bool IsBangMaLoaded { get { return _IsBangMaLoaded; } set { _IsBangMaLoaded = value; OnPropertyChanged(); } }
        public bool IsDMKhachHangLoaded { get { return _IsDMKhachHangLoaded; } set { _IsDMKhachHangLoaded = value; OnPropertyChanged(); } }
        public bool IsLoiSPLoaded { get { return _IsLoiSPLoaded; } set { _IsLoiSPLoaded = value; OnPropertyChanged(); } }
        public bool IsSCTVTHHLoaded { get { return _IsSCTVTHHLoaded; } set { _IsSCTVTHHLoaded = value; OnPropertyChanged(); } }

        public DataTable BangMa
        {
            get { return _BangMa; }
            set
            {
                _BangMa = value;
                if (_BangMa != null && _BangMa.Rows.Count > 0)
                    IsBangMaLoaded = true;
                OnPropertyChanged();
            }
        }

        public DataTable DMKhachHang
        {
            get { return _DMKhachHang; }
            set
            {
                _DMKhachHang = value;
                if (_DMKhachHang != null && _DMKhachHang.Rows.Count > 0)
                    IsDMKhachHangLoaded = true;
                OnPropertyChanged();
            }
        }

        public DataTable DMSPLoi
        {
            get { return _DMSPLoi; }
            set
            {
                _DMSPLoi = value;
                if (_DMSPLoi != null && _DMSPLoi.Rows.Count > 0)
                    IsLoiSPLoaded = true;
                OnPropertyChanged();
            }
        }

        public DataTable DataSCTVTHH
        {
            get { return _DataSCTVTHH; }
            set
            {
                _DataSCTVTHH = value;
                if (_DataSCTVTHH != null && _DataSCTVTHH.Rows.Count > 0)
                    IsSCTVTHHLoaded = true;
                OnPropertyChanged();
            }
        }

        public DataTable OutputRandomTable
        {
            get { return _OutputRandomTable; }
            set
            {
                _OutputRandomTable = value;
                ExportOutPutRandomDataToNewWorksheet(value);
                OnPropertyChanged();
            }
        }


        public int NumOfItemMin
        {
            get { return _NumOfItemMin; }
            set { _NumOfItemMin = value; OnPropertyChanged(); }
        }

        public int NumOfItemMax
        {
            get { return _NumOfItemMax; }
            set { _NumOfItemMax = value; OnPropertyChanged(); }
        }

        public int SLXuatMin
        {
            get { return _SLXuatMin; }
            set { _SLXuatMin = value; OnPropertyChanged(); }
        }

        public int SLXuatMax
        {
            get { return _SLXuatMax; }
            set { _SLXuatMax = value; OnPropertyChanged(); }
        }

        public int AddNewItemByRandomRate
        {
            get { return _AddNewItemByRandomRate; }
            set { _AddNewItemByRandomRate = value; OnPropertyChanged(); }
        }

        public int NumOfNewItemMin
        {
            get { return _NumOfNewItemMin; }
            set { _NumOfNewItemMin = value; OnPropertyChanged(); }
        }

        public int NumOfNewItemMax
        {
            get { return _NumOfNewItemMax; }
            set { _NumOfNewItemMax = value; OnPropertyChanged(); }
        }

        public int AddItemByIncreaseQuantityRate
        {
            get { return _AddItemByIncreaseQuantityRate; }
            set { _AddItemByIncreaseQuantityRate = value; OnPropertyChanged(); }
        }

        public int AddItemByIncreaseQuantityMin
        {
            get { return _AddItemByIncreaseQuantityMin; }
            set { _AddItemByIncreaseQuantityMin = value; OnPropertyChanged(); }
        }

        public int AddItemByIncreaseQuantityMax
        {
            get { return _AddItemByIncreaseQuantityMax; }
            set { _AddItemByIncreaseQuantityMax = value; OnPropertyChanged(); }
        }

        public int NewRandomItemSLXuatMin
        {
            get { return _NewRandomItemSLXuatMin; }
            set { _NewRandomItemSLXuatMin = value; OnPropertyChanged(); }
        }

        public int NewRandomItemSLXuatMax
        {
            get { return _NewRandomItemSLXuatMax; }
            set { _NewRandomItemSLXuatMax = value; OnPropertyChanged(); }
        }


        private void ExportOutPutRandomDataToNewWorksheet(DataTable dataTable)
        {
            //Microsoft.Office.Interop.Excel.Application excelApp = _MainWorkbook.Application;
            //Workbook reportWorkbook = excelApp.Workbooks.Add();
            string sheetName = Models.DSS.Structure.WSheet.SheetOutputRandomData.SheetName;
            Worksheet worksheet = MainWorkbook.GetNewSheet(sheetName);
            dataTable.CopyToRange(worksheet.Range["a1"]);
            ListObject listObject = worksheet.UsedRange.ConvertToListObject();
            listObject.GetCell(1, Models.DSS.Structure.WSheet.SheetOutputRandomData.SoLuongXuat).EntireColumn.ConvertToValue();
            listObject.Unlist();
        }

        public ICommand LoadDataBase { get; set; }

        public ICommand GetSCTVTHH { get; set; }
        public ICommand RunRandom { get; set; }

        public ICommand TestUpdateUI { get; set; }

    }
}
