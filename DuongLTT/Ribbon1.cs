﻿using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Tools.Ribbon;
using System.Collections.Generic;
using VSTOLib.Extensions;

namespace DuongLTT
{
    public partial class Ribbon1
    {
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            Workbook wb = Globals.ThisAddIn.Application.ActiveWorkbook;
            Worksheet activeSheet = wb.ActiveSheet;

            activeSheet.RemoveAutoFilter();
            ListObject tableSource = activeSheet.GetRange("a8:v", "a").ConvertToListObject();

            Range columnRange = tableSource.GetColumnDataBodyRange(ExcelTemplate.Zalo.SoDienThoai);
            List<string> phoneNumbers = columnRange.GetUniqueValue();

            Workbook customerReportWorkbook = ExcelTemplate.Zalo.ZaloCustomer();
            Worksheet customerReportSheet1 = customerReportWorkbook.Worksheets[1];
            void CopyDataToSheetCustomer(string customer)
            {
                string criteria = customer;
                tableSource.Range.SetFilter(ExcelTemplate.Zalo.SoDienThoai, criteria);

                Range visibleRows = tableSource.DataBodyRange.SpecialCells(XlCellType.xlCellTypeVisible).Rows;
                List<string> tagsList = new List<string>();
                string firstName = "", lastName = "", phone = "", address = "", email = "", gender = "", birthday = "", tags = "";

                foreach (Range eachRow in visibleRows)
                {
                    int rowIndex = eachRow.Row;
                    firstName = ((Range)activeSheet.Cells[rowIndex,tableSource.ListColumns[ExcelTemplate.Zalo.KhachHang].Index]).GetValue<string>();
                    lastName = ".";
                    phone = ((Range)activeSheet.Cells[rowIndex, tableSource.ListColumns[ExcelTemplate.Zalo.SoDienThoai].Index]).GetValue<string>();
                    address = ((Range)activeSheet.Cells[rowIndex, tableSource.ListColumns[ExcelTemplate.Zalo.TenCuaHang].Index]).GetValue<string>();
                    email = string.Empty;
                    gender = "1";
                    birthday = string.Empty;
                    tagsList.AddUnique(((Range)activeSheet.Cells[rowIndex, tableSource.ListColumns[ExcelTemplate.Zalo.NhomHangHoa].Index]).GetValue<string>());
                }

                //foreach (ListRow row in tableSource.ListRows)
                //{
                //    int rowIndex = row.Index;
                //    System.Diagnostics.Debug.WriteLine(rowIndex);

                //    firstName = tableSource.GetCell(rowIndex, ExcelTemplate.Zalo.KhachHang).GetValue<string>();
                //    lastName = ".";
                //    phone = tableSource.GetCell(rowIndex, ExcelTemplate.Zalo.SoDienThoai).GetValue<string>();
                //    address = tableSource.GetCell(rowIndex, ExcelTemplate.Zalo.TenCuaHang).GetValue<string>();
                //    email = string.Empty;
                //    gender = "1";
                //    birthday = string.Empty;
                //    tagsList.AddUnique(tableSource.GetCell(rowIndex, ExcelTemplate.Zalo.NhomHangHoa).GetValue<string>());

                //}

                tags = string.Join("|", tagsList);
                List<string> valueArray = new List<string>();
                valueArray.AddRange(new string[] { firstName, lastName, phone, address, email, gender, birthday, tags });

                customerReportSheet1.NewRangeAtColumn("a").SetHeaderText(valueArray.ToArray());
            }

            foreach (string phoneNumber in phoneNumbers)
            {
                System.Diagnostics.Debug.WriteLine(phoneNumber);
                CopyDataToSheetCustomer(phoneNumber);
            }

            tableSource.Unlist();
            System.Windows.Forms.MessageBox.Show("Xong");

        }
    }
}
