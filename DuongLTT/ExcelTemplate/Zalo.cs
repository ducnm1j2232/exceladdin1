﻿using Microsoft.Office.Interop.Excel;
using VSTOLib.Extensions;

namespace DuongLTT.ExcelTemplate
{
    public static class Zalo
    {
        internal const string customerSheetName = "customer";
        internal static string[] header = new string[] { "first_name", "last_name", "phone", "address", "email", "gender", "birthday", "tags", "points", "total_amount" };
        internal const string KhachHang = "Khách hàng";
        internal const string SoDienThoai = "Số điện thoại";
        internal const string TenCuaHang = "Tên cửa hàng";
        internal const string NhomHangHoa = "Nhóm hàng hóa";

        public static Workbook ZaloCustomer()
        {
            Workbook workbook = Globals.ThisAddIn.Application.Workbooks.Add();
            Worksheet customerSheet = workbook.Worksheets[1];
            customerSheet.Name = customerSheetName;
            customerSheet.Range["a1"].SetHeaderText(header);

            return workbook;

        }


    }
}
