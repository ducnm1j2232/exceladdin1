﻿using System;
using System.Windows.Forms;

namespace WAD.Forms
{
    public partial class frmCancel : Form
    {
        protected override bool ShowWithoutActivation => true;

        protected override CreateParams CreateParams
        {
            get
            {
                //make sure Top Most property on form is set to false
                //otherwise this doesn't work
                int WS_EX_TOPMOST = 0x00000008;
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= WS_EX_TOPMOST;
                return cp;
            }
        }

        public frmCancel()
        {
            InitializeComponent();

            var handle = this.Handle;
            //this.TopMost = true;
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            WAD.Token.TokenCancel.CancellationTokenSource?.Cancel();
            this.Dispose();
        }

        public void Start()
        {
            this.BeginInvoke((MethodInvoker)delegate
            {
                Screen screen = Screen.FromPoint(Cursor.Position);
                Location = new System.Drawing.Point(screen.WorkingArea.Left + (screen.WorkingArea.Width - this.Width)/2, screen.WorkingArea.Top);
                this.TopMost = true;
                this.Show();
            });
        }
    }
}
