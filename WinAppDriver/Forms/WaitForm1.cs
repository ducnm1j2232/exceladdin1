﻿using DevExpress.XtraWaitForm;
using System;
using System.Windows.Forms;

namespace WAD.Forms
{
    public partial class WaitForm1 : WaitForm
    {
        public WaitForm1()
        {
            InitializeComponent();
            this.progressPanel1.AutoHeight = true;

            ShowOnTopMode = ShowFormOnTopMode.AboveAll;
        }

        #region Overrides

        public override void SetCaption(string caption)
        {
            base.SetCaption(caption);
            this.progressPanel1.Caption = caption;
        }
        public override void SetDescription(string description)
        {
            base.SetDescription(description);
            this.progressPanel1.Description = description;
        }
        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        public enum WaitFormCommand
        {
        }

        private void progressPanel1_Click(object sender, EventArgs e)
        {
            Cancel();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {
            Cancel();
        }

        private void Cancel()
        {
            WAD.Token.TokenCancel.CancellationTokenSource?.Cancel();
            
        }
    }
}