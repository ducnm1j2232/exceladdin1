﻿using OpenQA.Selenium.Appium.Windows;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WAD
{
    public class WADCore
    {
        public static Element LocatorCore(object parent, WADEnum.FindBy findBy, string findByValue, string classNameKey, string classNameValue, int timeoutSecond = WAD.Wait.TimeoutSecond)
        {
            Type type = parent.GetType();
            WindowsDriver<WindowsElement> driver = null;
            WindowsElement element1 = null;

            WindowsElement elementFound = null;

            if (type == typeof(Driver))
            {
                driver = ((Driver)parent).WindowsDriver;
            }

            if (type == typeof(Element))
            {
                element1 = ((Element)parent).WindowsElement;
            }

            TimeSpan timeout = timeoutSecond == 0 ? TimeSpan.FromHours(6) : TimeSpan.FromSeconds(timeoutSecond);
            bool isTimeout = false;
            Task t = Task.Run(() =>
            {
                while (!isTimeout)
                {
                    try
                    {
                        WAD.Token.TokenCancel.CancellationToken.ThrowIfCancellationRequested();
                        switch (findBy)
                        {
                            case WADEnum.FindBy.ID:
                                if (driver != null) elementFound = driver.FindElementByAccessibilityId(findByValue);
                                if (element1 != null) elementFound = (WindowsElement)element1.FindElementByAccessibilityId(findByValue);
                                break;

                            case WADEnum.FindBy.Name:
                                if (string.IsNullOrEmpty(classNameKey) && string.IsNullOrEmpty(classNameValue))
                                {
                                    if (driver != null) elementFound = driver.FindElementByName(findByValue);
                                    if (element1 != null) elementFound = (WindowsElement)element1.FindElementByName(findByValue);
                                    break;
                                }

                                if (!string.IsNullOrEmpty(classNameKey) && !string.IsNullOrEmpty(classNameValue))
                                {
                                    ReadOnlyCollection<WindowsElement> elementFoundList1 = null;
                                    if (driver != null) elementFoundList1 = driver.FindElementsByName(findByValue);
                                    if (elementFoundList1 != null && elementFoundList1.Count > 0)
                                    {
                                        foreach (var element in elementFoundList1)
                                        {
                                            if (element.GetAttribute(classNameKey).Equals(classNameValue))
                                            {
                                                elementFound = element;
                                                break;
                                            }
                                        }
                                    }

                                    ReadOnlyCollection<OpenQA.Selenium.Appium.AppiumWebElement> elementFoundList2 = null;
                                    if (element1 != null) elementFoundList2 = element1.FindElementsByName(findByValue);
                                    if (elementFoundList2 != null && elementFoundList2.Count > 0)
                                    {
                                        foreach (var element in elementFoundList2)
                                        {
                                            if (element.GetAttribute(classNameKey) == (classNameValue))
                                            {
                                                elementFound = (WindowsElement)element;
                                                break;
                                            }
                                        }
                                    }
                                }
                                break;

                            case WADEnum.FindBy.ClassName:
                                if (driver != null) elementFound = driver?.FindElementByClassName(findByValue);
                                if (element1 != null) elementFound = (WindowsElement)element1?.FindElementByClassName(findByValue);
                                break;
                        }
                    }
                    catch (OpenQA.Selenium.WebDriverException)
                    {
                        //An element could not be located on the page using the given search parameters
                    }

                    if (elementFound != null)
                    {
                        break;
                    }
                }

            });

            if (!t.Wait(timeout))
            {
                isTimeout = true;
                throw new Exception($"time out when locator {findByValue}");
            }

            if (driver is null)
            {
                driver = ((Element)parent).Driver;
            }
            return new Element(driver, elementFound);
        }

        public static void ClickCore(Element element, WADEnum.ClickPosition clickPosition, int offsetX = 0, int offsetY = 0, WADEnum.ClickType clickType = WADEnum.ClickType.Click)
        {
            MoveMouse(element, clickPosition, offsetX, offsetY);
            ClickByWindowMessage(clickType);
        }

        public static void MoveMouse(Element element, WADEnum.ClickPosition clickPosition, int offsetX, int offsetY)
        {
            //winappdriver element location return local postion on a screen;

            bool isTimeout = false;

            Task t = Task.Run(() =>
            {
                System.Drawing.Point elementLocation = GetClickPointLocation(element, clickPosition, offsetX, offsetY);

                while (Cursor.Position != elementLocation && !isTimeout)
                {
                    //System.Diagnostics.Debug.WriteLine($"move mouse: cursor: {Cursor.Position.X}:{Cursor.Position.Y}, screen: {cursorScreen} -- {elementLocation.X}:{elementLocation.Y}, screen: {elementScreen}");
                    Cursor.Position = elementLocation;
                    System.Threading.Thread.Sleep(300);
                }
            });

            TimeSpan timeSpan = TimeSpan.FromSeconds(Wait.TimeoutSecond);
            if (!t.Wait(timeSpan))
            {
                isTimeout = true;
                throw new Exception("timeout move mouse");
            }
        }

        public static System.Drawing.Point GetClickPointLocation(Element element, WADEnum.ClickPosition clickPosition, int offsetX, int offsetY)
        {
            string rect = element.GetAttribute("BoundingRectangle");
            string[] array = rect.Split(' ');
            int left = System.Convert.ToInt32(array[0].Replace("Left:", ""));
            int top = System.Convert.ToInt32(array[1].Replace("Top:", ""));
            int width = System.Convert.ToInt32(array[2].Replace("Width:", ""));
            int height = System.Convert.ToInt32(array[3].Replace("Height:", ""));

            int centerX = 0;
            int centerY = 0;

            switch (clickPosition)
            {
                case WADEnum.ClickPosition.TopLeft:
                    centerX = 0;
                    centerY = 0;
                    break;
                case WADEnum.ClickPosition.Center:
                    centerX = width / 2;
                    centerY = height / 2;
                    break;
            }

            int X = left + offsetX + centerX;
            int Y = top + offsetY + centerY;

            return new System.Drawing.Point(X, Y);
        }

        private static void ClickByWindowMessage(WADEnum.ClickType clickType)
        {
            switch (clickType)
            {
                case WADEnum.ClickType.Click:
                    Win32Functions.MouseClickByWM(0, 0);
                    break;
                case WADEnum.ClickType.DoubleClick:
                    Win32Functions.MouseClickByWM(0, 0);
                    Win32Functions.MouseClickByWM(0, 0);
                    break;
            }
            System.Windows.Forms.Application.DoEvents();
        }


    }
}
