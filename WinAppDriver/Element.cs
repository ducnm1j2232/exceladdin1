﻿using System;
using System.Threading.Tasks;

namespace WAD
{
    public class Element : WAD.Interfaces.ILocator
    {
        private OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> _driver { get; set; }
        private OpenQA.Selenium.Appium.Windows.WindowsElement _element { get; set; }

        public OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> Driver
        {
            get { return _driver; }
            set { _driver = value; }
        }

        public OpenQA.Selenium.Appium.Windows.WindowsElement WindowsElement
        {
            get { return _element; }
            set { _element = value; }
        }

        public Element()
        {

        }

        public Element(OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> driver = null, OpenQA.Selenium.Appium.Windows.WindowsElement element = null)
        {
            if (driver != null)
            {
                _driver = driver;
            }

            if (element != null)
            {
                _element = element;
            }
        }

        public Element Locator(WADEnum.FindBy findBy, string findByValue, string classNameKey = "", string classNameValue = "", int timeoutSecond = Wait.TimeoutSecond)
        {
            return WADCore.LocatorCore(this, findBy, findByValue, classNameKey, classNameValue, timeoutSecond);
        }

        public void Click(int offsetX = 0, int offsetY = 0, WADEnum.ClickType clickType = WADEnum.ClickType.Click)
        {
            WADCore.ClickCore(this, WADEnum.ClickPosition.TopLeft, offsetX, offsetY, clickType);
        }

        public void Click(WADEnum.ClickType clickType = WADEnum.ClickType.Click)
        {
            WADCore.ClickCore(this, WADEnum.ClickPosition.Center, 0, 0, clickType);
        }
        public void Click()
        {
            WADCore.ClickCore(this, WADEnum.ClickPosition.Center, 0, 0, WADEnum.ClickType.Click);
        }

        public System.Drawing.Point Location
        {
            get
            {
                string rect = this.GetAttribute("BoundingRectangle");
                string[] array = rect.Split(' ');
                int left = System.Convert.ToInt32(array[0].Replace("Left:", ""));
                int top = System.Convert.ToInt32(array[1].Replace("Top:", ""));

                return new System.Drawing.Point(left, top);
            }
        }

        public string GetAttribute(string attributeName)
        {
            return _element.GetAttribute(attributeName);
        }

        public void WaitFor(WADEnum.State state, string text = "")
        {
            bool isTimeout = false;
            bool conditionIsMet = false;

            Task t = Task.Run(() =>
            {
                while (!conditionIsMet && !isTimeout)
                {
                    switch (state)
                    {
                        case WADEnum.State.Enable:
                            if (Enable) conditionIsMet = true;
                            break;
                        case WADEnum.State.Disable:
                            if (!Enable) conditionIsMet = true;
                            break;
                        case WADEnum.State.Display:
                            if (Display) conditionIsMet = true;
                            break;
                        case WADEnum.State.Disappear:
                            if (!Display) conditionIsMet = true;
                            break;
                        case WADEnum.State.Editable:
                            if (Editable) conditionIsMet = true;
                            break;
                        case WADEnum.State.Text:
                            if (Text == text) conditionIsMet = true;
                            break;

                    }
                }
            });

            TimeSpan timeSpan = TimeSpan.FromSeconds(Wait.TimeoutSecond);
            if (!t.Wait(timeSpan))
            {
                isTimeout = true;
                throw new Exception($"timeout wait for {state}");
            }

        }

        public string Text => _element.Text;

        public bool Enable => _element.Enabled;

        public bool Display => _element.Displayed;

        public bool Editable
        {
            get
            {
                if (_element.GetAttribute("Value.IsReadOnly") == "False")
                {
                    return true;
                }
                return false;
            }
        }

        public void HighLight()
        {
            string rect = _element.GetAttribute("BoundingRectangle");
            string[] array = rect.Split(' ');
            int left = System.Convert.ToInt32(array[0].Replace("Left:", ""));
            int top = System.Convert.ToInt32(array[1].Replace("Top:", ""));
            int width = System.Convert.ToInt32(array[2].Replace("Width:", ""));
            int height = System.Convert.ToInt32(array[3].Replace("Height:", ""));

            System.Windows.Forms.Form form = new System.Windows.Forms.Form();
            form.Top = top;
            form.Left = left;
            form.Width = width;
            form.Height = height;
            form.BackColor = System.Drawing.Color.Red;
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            form.TopMost = true;

            form.StartPosition = System.Windows.Forms.FormStartPosition.Manual;

            while (true)
            {
                form.Show();
                System.Threading.Thread.Sleep(500);
                form.Hide();
                System.Threading.Thread.Sleep(500);
            }

            //graphics.DrawRectangle(new System.Drawing.Pen(System.Drawing.Color.Red), rectangle);


        }

        public void SetText(string text, WADEnum.SetTextType setTextType = WADEnum.SetTextType.Clipboard)
        {
            bool textIsSet = false;

            while (!textIsSet)
            {
                switch (setTextType)
                {
                    case WADEnum.SetTextType.Clipboard:
                        this.Click();
                        this.Clear();
                        VSTOLib.Clipboard.ClipboardSafeInvoke.SetText(text);
                        Actions.Keyboard.CtrlV();
                        System.Windows.Forms.Application.DoEvents();
                        break;

                    case WADEnum.SetTextType.WindowMessage:
                        this.WaitFor(WADEnum.State.Editable);
                        int WM_SETTEXT = 0X000C;
                        user32dll.SendMessage(this.WindowHandle, WM_SETTEXT, 0, text);
                        break;
                }

                try
                {
                    this.WaitFor(WADEnum.State.Text, text);
                    textIsSet = true;
                }
                catch { }

            }

        }

        public void SetTextComboboxVietsea(string text)
        {
            this.Locator(WADEnum.FindBy.ClassName, "Edit").SetText(text, WADEnum.SetTextType.WindowMessage);
            this.Locator(WADEnum.FindBy.Name, "Open").Click(WADEnum.ClickType.DoubleClick);
            Actions.Keyboard.Tab();
        }

        public void SetTextDateTimePickerVietsea(string text)
        {
            _element.Click();
            _element.SendKeys(text);
        }

        public void Clear()
        {
            _element.Clear();
        }

        public System.IntPtr WindowHandle
        {
            get
            {
                return new System.IntPtr(System.Convert.ToInt32(_element.GetAttribute("NativeWindowHandle")));
            }
        }

        public void Active()
        {
            for (int i = 1; i <= 5; i++)
            {
                user32dll.SetForegroundWindow(this.WindowHandle);
            }
            this.WaitFor(WADEnum.State.Display);
        }

        public void Close()
        {
            Win32Functions.CloseWindowByWM(WindowHandle);
            //WaitFor(WADEnum.State.Disappear);
        }
    }
}
