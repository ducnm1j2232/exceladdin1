﻿namespace WAD
{
    public class WADEnum
    {
        public enum FindBy { ID, Name, ClassName }

        public enum ClickPosition { TopLeft, Center }
        public enum ClickType { Click, DoubleClick }

        public enum State { Enable, Disable, Display, Disappear, Editable,Text }

        public enum SetTextType { Clipboard, WindowMessage }

    }
}
