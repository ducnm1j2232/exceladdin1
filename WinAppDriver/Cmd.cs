﻿namespace WAD
{
    public static class Cmd
    {
        public static void Start()
        {
            //https://robindotnet.wordpress.com/2010/07/11/how-do-i-programmatically-find-the-deployed-files-for-a-vsto-add-in/
            System.Reflection.Assembly assemblyInfo = System.Reflection.Assembly.GetExecutingAssembly();

            System.Uri uriCodeBase = new System.Uri(assemblyInfo.CodeBase);
            string ClickOnceLocation = System.IO.Path.GetDirectoryName(uriCodeBase.LocalPath.ToString());

            string winappdriverEXE = System.IO.Path.Combine(ClickOnceLocation, Const.ExeLocation);

            System.Diagnostics.ProcessStartInfo processStartInfo = new System.Diagnostics.ProcessStartInfo(winappdriverEXE);
            processStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;
            System.Diagnostics.Process.Start(processStartInfo);

            while (true)
            {
                if (System.Diagnostics.Process.GetProcessesByName(Const.WADProcessName).Length > 0)
                {
                    break;
                }
            }
        }

        public static void TearDown()
        {
            try
            {
                foreach (var process in System.Diagnostics.Process.GetProcessesByName(Const.WADProcessName))
                {
                    process.Kill();
                }
            }
            catch { }
        }
    }
}
