﻿namespace WAD.Actions
{
    public static class Keyboard
    {
        const int timeoutMilisecond = 50;

        private static void SendCore(string keys)
        {
            //System.Threading.Thread.Sleep(timeoutMilisecond);
            System.Windows.Forms.Application.DoEvents();
            System.Windows.Forms.SendKeys.SendWait(keys);
            System.Windows.Forms.Application.DoEvents();
            System.Threading.Thread.Sleep(timeoutMilisecond);
        }

        public static void CtrlC()
        {
            SendCore("^{c}");
        }

        public static void CtrlV()
        {
            SendCore("^{v}");
        }


        public static void Tab()
        {
            SendCore("{TAB}");
        }

        public static void SendKeys(string keys, int repeat = 1, int timeout = timeoutMilisecond)
        {
            for (int i = 0; i < repeat; i++)
            {
                SendCore(keys);
            }
        }
    }
}
