﻿namespace WAD.Interfaces
{
    interface ILocator
    {
        WAD.Element Locator(WADEnum.FindBy findBy, string selector, string classNameKey, string classNameValue, int timeoutSecond);
    }
}
