﻿using System;
using System.Threading;

namespace WAD
{
    public class Driver : WAD.Interfaces.ILocator
    {

        private OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> _driver { get; set; }

        public OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement> WindowsDriver
        {
            get { return _driver; }
            set { _driver = value; }
        }

        /// <summary>
        /// Attach driver to existing process
        /// </summary>
        /// <param name="processName"></param>
        /// <exception cref="Exception"></exception>
        public Driver(string processName)
        {
            StartCmd();

            System.IntPtr appTopLevelWindowHandle = new IntPtr();
            var process = System.Diagnostics.Process.GetProcessesByName(processName);

            if (process.Length > 1)
            {
                throw new Exception($"có nhiều hơn 1 process {processName}");
            }

            if (process.Length == 1)
            {
                appTopLevelWindowHandle = process[0].MainWindowHandle;
            }

            InitDriver(appTopLevelWindowHandle);
            InitWaitFormAndToken();
        }

        public Driver(System.IntPtr intPtr)
        {
            StartCmd();
            InitDriver(intPtr);
            InitWaitFormAndToken();
        }

        public Driver(Enums.SessionType sessionType = Enums.SessionType.Desktop)
        {
            OpenQA.Selenium.Appium.AppiumOptions options = new OpenQA.Selenium.Appium.AppiumOptions();
            options.AddAdditionalCapability("app", "Root");

            Uri uri = new Uri(Const.DefaultPort);
            var driver = new OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>(uri, options);
            _driver = driver;
        }

        private void InitDriver(IntPtr intPtr)
        {
            var appWindowHandle = intPtr.ToString("x");
            OpenQA.Selenium.Appium.AppiumOptions options = new OpenQA.Selenium.Appium.AppiumOptions();
            options.AddAdditionalCapability("appTopLevelWindow", appWindowHandle);

            Uri uri = new Uri(Const.DefaultPort);
            var driver = new OpenQA.Selenium.Appium.Windows.WindowsDriver<OpenQA.Selenium.Appium.Windows.WindowsElement>(uri, options);
            _driver = driver;
        }

        private void StartCmd()
        {
            //https://robindotnet.wordpress.com/2010/07/11/how-do-i-programmatically-find-the-deployed-files-for-a-vsto-add-in/
            System.Reflection.Assembly assemblyInfo = System.Reflection.Assembly.GetExecutingAssembly();

            Uri uriCodeBase = new Uri(assemblyInfo.CodeBase);
            string ClickOnceLocation = System.IO.Path.GetDirectoryName(uriCodeBase.LocalPath.ToString());

            string winappdriverEXE = System.IO.Path.Combine(ClickOnceLocation, Const.ExeLocation);

            System.Diagnostics.ProcessStartInfo processStartInfo = new System.Diagnostics.ProcessStartInfo(winappdriverEXE);
            processStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;
            System.Diagnostics.Process.Start(processStartInfo);

            while (true)
            {
                if (System.Diagnostics.Process.GetProcessesByName(Const.WADProcessName).Length > 0)
                {
                    break;
                }
            }

        }

        private void InitWaitFormAndToken()
        {
            //WAD.Forms.XtraForm1 xtraForm1 = new Forms.XtraForm1();
            //xtraForm1.ShowWaitForm();
            //WAD.Token.TokenCancel.CancellationTokenSource = new CancellationTokenSource();
            //WAD.Token.TokenCancel.CancellationToken = WAD.Token.TokenCancel.CancellationTokenSource.Token;

        }

        public void TearDown()
        {
            try
            {
                foreach (var process in System.Diagnostics.Process.GetProcessesByName(Const.WADProcessName))
                {
                    process.Kill();
                }
            }
            catch
            {
            }
            finally
            {
            }
        }

        public Element Locator(WADEnum.FindBy findBy, string selector, string classNameKey = "", string classNameValue = "", int timeoutSecond = Wait.TimeoutSecond)
        {
            return WADCore.LocatorCore(this, findBy, selector, classNameKey, classNameValue, timeoutSecond);
        }

        public void BringToFront()
        {
            try
            {
                _driver.Manage().Window.Maximize();
            }
            catch
            {

            }
        }



    }
}
