﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WAD.RedRectangle
{
    public partial class OverlayForm : Form
    {
        public OverlayForm()
        {
            InitializeComponent();

            FormBorderStyle = FormBorderStyle.None;
            ShowInTaskbar = false;

        }

        public void ShowRectangle(int top, int left, int width, int height)
        {
            Top = top;
            Left = left;
            Width = width;
            Height = height;

            Show();
        }

        protected override bool ShowWithoutActivation => true;
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        var createParams = base.CreateParams;
        //        createParams.ExStyle |= (int)WindowStyles.WS_EX_TRANSPARENT;
        //        createParams.ExStyle |= (int)WindowStyles.WS_EX_TOOLWINDOW;
        //        createParams.ExStyle |= (int)WindowStyles.WS_EX_TOPMOST;
        //        return createParams;
        //    }
        //}
    }
}
