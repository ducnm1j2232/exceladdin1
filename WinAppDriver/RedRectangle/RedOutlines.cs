﻿using System.Drawing;


namespace WAD.RedRectangle
{
    public class RedOutlines
    {
        public void DrawOutLine(int left, int top,int width,int height)
        {
            Bitmap bmp = WAD.Properties.Resources.facebook_48px;
            Bitmap tempBmp = new Bitmap(bmp.Width,bmp.Height);

            Graphics g = Graphics.FromImage(tempBmp);
            Pen pen = new Pen(Color.Red);
            g.DrawImage(bmp, 0, 0);
            g.DrawRectangle(pen, new Rectangle(left, top, width, height));
        }

    }
}
