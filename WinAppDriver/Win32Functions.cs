﻿namespace WAD
{
    public static class Win32Functions
    {
        public static void MouseClickByWM(uint X, uint Y)
        {
            user32dll.mouse_event(user32dll.MOUSEEVENTF_LEFTDOWN | user32dll.MOUSEEVENTF_LEFTUP, X, Y, 0, 0);
        }

        public static System.IntPtr FindWindow(string lpClassName, string lpWindowName)
        {
            while (true)
            {
                if (user32dll.FindWindow(lpClassName, lpWindowName) != System.IntPtr.Zero)
                {
                    return user32dll.FindWindow(lpClassName, lpWindowName);
                }
            }
        }

        public static void CloseWindowByWM(System.IntPtr hWnd)
        {
            System.UInt32 WM_CLOSE = 0x0010;
            user32dll.SendMessage(hWnd, WM_CLOSE, System.IntPtr.Zero, System.IntPtr.Zero);
        }


    }
}
