﻿using Microsoft.Identity.Client;
using System;
using System.Threading.Tasks;

namespace MicrosoftGraphAPI
{
    public static class Class1
    {
        private static IPublicClientApplication _clientApp;
        public static IPublicClientApplication PublicClientApp
        {
            get
            {
                return _clientApp;
            }
        }

        public static async Task<AuthenticationResult> GetAuthenticationResultAsync()
        {
            _clientApp = PublicClientApplicationBuilder
                .Create(Info.ClientID)

                //.WithRedirectUri("https://login.microsoftonline.com/common/oauth2/nativeclient")
                .WithAuthority(Info.authority)
                .Build();

            AuthenticationResult authResult = null;

            try
            {
                authResult = await _clientApp.AcquireTokenByUsernamePassword(Info.Scopes, Info.Username, Info.Password).ExecuteAsync();
            }
            catch (MsalUiRequiredException)
            {
                //try
                //{
                //    authResult = await _clientApp.AcquireTokenInteractive(Info.scopes)
                //        .WithAccount(accounts.FirstOrDefault())
                //        .WithPrompt(Prompt.SelectAccount)
                //        .ExecuteAsync();
                //}
                //catch (MsalException msalex)
                //{
                //    System.Windows.Forms.MessageBox.Show($"Error Acquiring Token:{System.Environment.NewLine}{msalex}");
                //}
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"Error Acquiring Token Silently:{System.Environment.NewLine}{ex}");
                return null;
            }

            if (authResult != null)
            {
                return authResult;
            }

            return null;

        }
    }
}
